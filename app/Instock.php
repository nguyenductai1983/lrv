<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instock extends Model
{
    protected $table = 'instocks';
    protected $fillable = [
        'user_id',
        'warehouse_id',
    ];
    //
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
