<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customer';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'area_id',
        'customer_group_id',
        'image_1_file_id',
        'image_2_file_id',
        'image_3_file_id',
        'first_name',
        'middle_name',
        'last_name',
        'address_1',
        'address_2',
        'telephone',
        'cellphone',
        'postal_code',
        'id_card',
        'card_expire',
        'date_issued',
        'birthday',
        'career',
        'country_id',
        'province_id',
        'city_id',
        'email',
        'password',
        'agency_id',
        'provider',
        'provider_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'card_expire',
        'birthday',
        'date_issued'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Belong to Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    /**
     * Belong to CustomerGroup
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(CustomerGroup::class, 'customer_group_id', 'id');
    }

    /**
     * Belong to Country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * Belong to Province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /**
     * Belong to City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * Belong to image 1 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image1()
    {
        return $this->belongsTo(File::class, 'image_1_file_id', 'id');
    }

    /**
     * Belong to image 2 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image2()
    {
        return $this->belongsTo(File::class, 'image_2_file_id', 'id');
    }

    /**
     * Belong to image 3 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image3()
    {
        return $this->belongsTo(File::class, 'image_3_file_id', 'id');
    }

    /**
     * Has many transport
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transports()
    {
        return $this->hasMany(Transport::class, 'customer_id', 'id');
    }

    /**
     * Set image 1 file id attribute
     *
     * @param $value
     */
    public function setImage1FileIdAttribute($value)
    {
        if ($this->image_1_file_id) {
            $this->image1()->update(['valid' => config('file.invalid')]);
        }

        if ($value) {
            File::where('id', '=', $value)->update(['valid' => config('file.valid')]);
        }

        $this->attributes['image_1_file_id'] = $value;
    }

    /**
     * Set image 2 file id attribute
     *
     * @param $value
     */
    public function setImage2FileIdAttribute($value)
    {
        if ($this->image_2_file_id) {
            $this->image2()->update(['valid' => config('file.invalid')]);
        }

        if ($value) {
            File::where('id', '=', $value)->update(['valid' => config('file.valid')]);
        }

        $this->attributes['image_2_file_id'] = $value;
    }

    /**
     * Set image 1 file id attribute
     *
     * @param $value
     */
    public function setImage3FileIdAttribute($value)
    {
        if ($this->image_3_file_id) {
            $this->image3()->update(['valid' => config('file.invalid')]);
        }

        if ($value) {
            File::where('id', '=', $value)->update(['valid' => config('file.valid')]);
        }

        $this->attributes['image_3_file_id'] = $value;
    }

    /**
     * Set birthday attribute
     *
     * @param $value
     */
    public function setBirthdayAttribute($value)
    {
        if ($value) {
            $value = Carbon::createFromFormat(config('app.date_format'), $value);
        }
        $this->attributes['birthday'] = $value;
    }

    /**
     * Get birthday date issued
     *
     * @return mixed
     */
    public function getDateIssuedAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('app.date_format'));
        }
        return $value;
    }
/**
     * Set birthday date issued
     *
     * @param $value
     */
    public function setDateIssuedAttribute($value)
    {
        if ($value) {
            $value = Carbon::createFromFormat(config('app.date_format'), $value);
        }
        $this->attributes['date_issued'] = $value;
    }

    /**
     * Get birthday attribute
     *
     * @return mixed
     */
    public function getBirthdayAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('app.date_format'));
        }
        return $value;
    }

    /**
     * Set card expire attribute
     *
     * @param $value
     */

    public function setCardExpireAttribute($value)
    {
        if ($value) {
            $value = Carbon::createFromFormat(config('app.date_format'), $value);
        }
        $this->attributes['card_expire'] = $value;
    }

    /**
     * Get card expire
     *
     * @return mixed
     */
    public function getCardExpireAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('app.date_format'));
        }
        return $value;
    }
    /**
     * Get full_name attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $name = $this->last_name;
        if ($this->middle_name != '') {
            $name .= (' ' . $this->middle_name);
        }
        return ($name . ' ' . $this->first_name);
    }
}
