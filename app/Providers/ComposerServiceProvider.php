<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use App\Notification;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            [
                'layouts.home.index',
                'layouts.home.user',
                // 'layouts.home.menu-header',
                // 'layouts.home.menu-header-mobile',
                // 'layouts.home.footer'
            ],
            'App\Http\ViewComposers\PagesComposer'
        );
        view()->composer([
                'layouts.home.index',
                'layouts.home.user',
            ], function ($view) {
            $view->with('cart_qty', Cart::content()->count());
        });

        view()->composer([
                'layouts.admin.index'
            ], function ($view) {
            $view->with('notifi_qty', Notification::where('to_user_id', '=', isset(Auth::user()->id) && !empty(Auth::user()->id) ? Auth::user()->id : -1)->where('is_read', '=', 0)->count());
            $view->with('notifi_top_contents', Notification::where('to_user_id', '=', isset(Auth::user()->id) && !empty(Auth::user()->id) ? Auth::user()->id : -1)->where('is_read', '=', 0)->orderBy('id', 'desc')->skip(0)->take(10)->get());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
