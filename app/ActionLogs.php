<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionLogs extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_action_logs';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        
    ];
}
