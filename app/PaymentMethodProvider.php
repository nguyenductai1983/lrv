<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethodProvider extends Model
{
    //
    protected $table = 'tbl_payment_method_provider';
}
