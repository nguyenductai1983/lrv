<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $table = 'wards';
    protected $fillable = [
        'country_id',
        'province_id',
        'city_id',
        'name',
        'vnpost_alias'
    ];
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * Relationship with province
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
    /**
     * Has many Customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    //
}
