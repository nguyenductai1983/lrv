<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyProduct extends Model
{
    /**
     * Table to use
     */
    protected $table = 'agency_product';
    
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
    ];
}
