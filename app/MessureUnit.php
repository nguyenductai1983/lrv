<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\MessureUnitEnum;

class MessureUnit extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_messure_unit';

    protected $fillable = [
        'code',
        'name',
        'display_order',
        'is_active',
    ];
    
    public function getIsActiveNameAttribute() {
        switch ($this->attributes['is_active']) {
            case MessureUnitEnum::IS_ACTIVE_TRUE:
                return __('messure.is_active_true');
            default:
                return __('Messure.is_active_fail');
        }
    }
}
