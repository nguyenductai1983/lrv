<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\DimensionUnitEnum;

class DimensionUnit extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_dimension_unit';

    protected $fillable = [
        'code',
        'name',
        'display_order',
        'is_default',
        'radio'
    ];
    
    public function getIsDefaultNameAttribute() {
        switch ($this->attributes['is_default']) {
            case DimensionUnitEnum::IS_DEFAULE_TRUE:
                return __('label.yes');
            default:
                return __('label.no');
        }
    }
}
