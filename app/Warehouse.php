<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\WarehouseEnum;
use App\Province;
use App\City;
use App\Ward;
class Warehouse extends Model {

    protected $fillable = ['code', 'name', 'country_id',
    'province_id', 'city_id', 'ward_id', 'description', 'address',
    'telephone', 'email', 'contact_person', 'post_code',
    'is_default', 'type', 'type_pickup', 'currency_id', 'weight_unit_id',
    'dimension_unit_id','stockin'];
    /**
     * Table to use
     */
    protected $table = 'tbl_warehouse';
    public function products()
    {
        return $this->belongsTo('App\User', 'product_group_id',3);
    }
    /**
     * Belong to Province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
       public function province() {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /**
     * Belong to City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city() {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function ward() {
        return $this->belongsTo(Ward::class, 'ward_id', 'id');
    }
    /**
     * Belong to Country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * Belong to Dimension Unit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dimension_unit() {
        return $this->belongsTo(DimensionUnit::class, 'dimension_unit_id', 'id');
    }

    /**
     * Belong to Weight Unit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function weight_unit() {
        return $this->belongsTo(WeightUnit::class, 'weight_unit_id', 'id');
    }

    /**
     * Belong to Currency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    public function getWarehouseAddressAttribute() {
        $address = "{$this->attributes['contact_person']}; {$this->attributes['telephone']}; {$this->attributes['address']}";
        $city = City::find($this->attributes['city_id']);
        $province = Province::find($this->attributes['province_id']);
        $ward = Ward::find($this->attributes['ward_id']);
        if (!empty($ward)) {
            $address = $address . ', ' . $ward->name;
        }
        if (!empty($city)) {
            $address = $address . ', ' . $city->name;
        }
        if (!empty($province)) {
            $address = $address . ', ' . $province->name;
        }
        return $address;
    }

    public function getTypeNameAttribute() {
        switch ($this->attributes['type']) {
            case WarehouseEnum::TYPE_LOCAL:
                return __('warehouse.type_local');
            case WarehouseEnum::TYPE_FOREIGN:
                return __('warehouse.type_global');
            default:
                return __('warehouse.type_local');
        }
    }
    public function product()
    {
        return $this->belongsToMany(product::class, 'warehouse_products', 'warehouse_id','product_id' );
    }

}
