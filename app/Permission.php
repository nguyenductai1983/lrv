<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * Table to use
     */
    protected $table = 'permissions';

    /**
     * Attributes are guarded
     */
    protected $guarded = [];

    /**
     * Relationship with roles
     */
    public function roles() {
        return $this->belongsToMany(Role::class, 'permission_role', 'permission_id', 'role_id');
    }
}
