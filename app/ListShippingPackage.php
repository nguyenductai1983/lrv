<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListShippingPackage extends Model
{
    protected $table = 'list_shipping_packages';
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    //
}
