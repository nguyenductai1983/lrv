<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerContact extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_customer_contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
    
}
