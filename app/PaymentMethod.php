<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\PaymentMethodEnum;

class PaymentMethod extends Model {

    //
    protected $table = 'tbl_payment_method';
    
    protected $fillable = [
        'code',
        'name',
        'description',
        'display_order',
        'card_holder',
        'account_number',
        'bank_branch',
        'type',
        'image_file_id'
    ];

    /**
     * Belong to image File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(File::class, 'image_file_id', 'id');
    }

    public function method_providers() {
        return $this->hasMany(PaymentMethodProvider::class, 'payment_method_id', 'id');
    }
    
    public function getTypeNameAttribute() {
        switch ($this->attributes['type']) {
            case PaymentMethodEnum::TYPE_GLOBAL:
                return __('payment.type_global');
            default:
                return __('payment.type_local');
        }
    }

    
}
