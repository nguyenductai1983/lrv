<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * Table to use
     */
    protected $table = 'countries';

    /**
     * Attributes are guarded
     */
    protected $guarded = [];

    /**
     * Relationship with province
     */
    public function provinces()
    {
        return $this->hasMany(Province::class, 'country_id', 'id');
    }

    /**
     * Relationship with city
     */
    public function cities()
    {
        return $this->hasMany(City::class, 'country_id', 'id');
    }
     //thêm
     public function wards()
     {
         return $this->hasMany(Ward::class, 'country_id', 'id');
     }
    /**
     * Has many Customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers()
    {
        return $this->hasMany(Customer::class, 'country_id', 'id');
    }
}
