<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Exceptions\NoFilenameGivenException;
class ExportQuery implements FromQuery, WithHeadings
{
    use Exportable;
    public function __construct($query)
    {
       $queryfirst = $query->first();
        $header=null;
        if (empty($queryfirst)){
            $header=null;
        }
        else
        {
            $header = $queryfirst->attributesToArray();
        }
        $this->header = array_keys($header);
        $this->query = $query;

    }
    public function headings(): array
    {
        if (!empty($this->header))
        {
        return [$this->header];
        }
        return [0];
    }

    public function query()
    {
        if (empty($this->query))
        {
            return 'select *';
        }
        $query=$this->query;
        $query->get();
        return $query;

    }

}
