<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_currency_rate';
    
    public function fromCurrencys()
    {
        return $this->belongsTo(Currency::class, 'currency_from_id', 'id');
    }

    public function toCurrencys(){
        return $this->belongsTo(Currency::class,'currency_to_id', 'id');
    }
}
