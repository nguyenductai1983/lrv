<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignLog extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_campaign_log';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'customer_id',
        'order_id',
        'coupon_code',
        'coupon_amount',
        'weight'
    ];
}
