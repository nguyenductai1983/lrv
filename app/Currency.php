<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currencies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'exchange_vnd'
    ];
    public function MTSSurcharge()
    {
        return $this->hasMany(MTSSurcharge::class, 'currencie_id', 'id');
    }
}
