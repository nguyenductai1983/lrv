<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\ClaimEnum;

class CustomerClaim extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_customer_claim';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
    
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function order() {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
    
    public function getStatusNameAttribute() {
        switch ($this->attributes['status']) {
            case ClaimEnum::PROCESSING:
                return __('order.status_processing');
            case ClaimEnum::CANCEL:
                return __('order.status_cancel');
            case ClaimEnum::COMPLETE:
                return __('order.status_complete');
            default:
                return __('order.status_new');
        }
    }

    public function getStatusLabelAttribute() {
        if ($this->attributes['status'] == ClaimEnum::NORMAL) {
            return "badge";
        } else if ($this->attributes['status'] == ClaimEnum::COMPLETE) {
            return "badge badge-success";
        } else if ($this->attributes['status'] == ClaimEnum::CANCEL) {
            return "badge badge-danger";
        } else {
            return "badge badge-warning";
        }
    }
}
