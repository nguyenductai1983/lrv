<?php

namespace App;
use App\CustomerGroup;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_campaign';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'create_user_id',
        'code',
        'amount',
        'from_date',
        'to_date',
        'customer_group_id',
        'weight',
        'apply_per_customer',
        'num_of_use',
        'status',
        'Transport',
        'Express',
        'YHL',
        'Quote',
        'MTS',
        
    ];
    public function group()
    {
        return $this->belongsTo(CustomerGroup::class, 'customer_group_id', 'id');
    }
    public function getCustomerGroupNameAttribute() {
        
        if ($this->attributes['customer_group_id'] == 1) {
            return "Ship";
        } else if ($this->attributes['customer_group_id'] == 2) {
            return "Default";
        } else {
            return "All";
        }
    }

    public function getStatusNameAttribute() {
        switch ($this->attributes['status']) {
            case 1:
                return __('campaign.status_active');
            case 2:
                return __('campaign.status_deactive');
            default:
                return __('campaign.status_deactive');
        }
    }

    public function getStatusLabelAttribute() {
        if ($this->attributes['status'] == 1) {
            return "badge badge-success";
        } else {
            return "badge badge-danger";
        }
    }

    public function getIsUsedNameAttribute() {
        if ($this->attributes['is_used'] == 1) {
            return __('campaign.is_used');
        } else {
            return __('campaign.is_used_no');;
        }
    }

    public function getIsUsedLabelAttribute() {
        if ($this->attributes['is_used'] == 1) {
            return "badge badge-danger";
        } else {
            return "badge badge-success";
        }
    }
}
