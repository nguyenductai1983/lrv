<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\MtsTransactionTypeEnum;
use App\Enum\MtsStatusEnum;
use App\Country;
use App\Province;
use App\City;

class Mts extends Model
{
    //
    /**
     * Table to use
     */
    protected $table = 'tbl_mts';

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function receiver() {
        return $this->belongsTo(\App\Address::class, 'shipping_address_id', 'id');
    }

    public function agency() {
        return $this->belongsTo(Agency::class, 'agency_id', 'id');
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }
    
    public function sender_city() {
        return $this->belongsTo(City::class, 'sender_city_id', 'id');
    }
    
    public function sender_province() {
        return $this->belongsTo(Province::class, 'sender_province_id', 'id');
    }
    
    public function sender_country() {
        return $this->belongsTo(Country::class, 'sender_country_id', 'id');
    }
    
    public function receiver_city() {
        return $this->belongsTo(City::class, 'receiver_city_id', 'id');
    }
    
    public function receiver_province() {
        return $this->belongsTo(Province::class, 'receiver_province_id', 'id');
    }
    
    public function receiver_country() {
        return $this->belongsTo(Country::class, 'receiver_country_id', 'id');
    }
    
    public function getCreatedDateAttribute() {
        return $this->created_at->format(config('app.date_format'));
    }

    public function getSenderFullNameAttribute() {
        return "{$this->attributes['sender_last_name']} {$this->attributes['sender_middle_name']} {$this->attributes['sender_first_name']}";
    }
    public function getReceiveFullNameAttribute() {
        return "{$this->attributes['receiver_last_name']} {$this->attributes['receiver_middle_name']} {$this->attributes['receiver_first_name']}";
    }
    public function getTransactionTypeNameAttribute() {
        switch ($this->attributes['transaction_type']) {
            case MtsTransactionTypeEnum::TYPE_BANK:
                return __('mts.transaction_type_bank');
            case MtsTransactionTypeEnum::TYPE_ACCOUNT:
                return __('mts.transaction_type_account');
            default:
                return __('mts.transaction_type_home');
        }
    }
    
    public function getStatusNormalAttribute() {
        return MtsStatusEnum::NORMAL;
    }
    
    public function getStatusNameAttribute() {
        switch ($this->attributes['mts_status']) {
            case MtsStatusEnum::PROCESSING:
                return __('mts.status_processing');
            case MtsStatusEnum::CANCEL:
                return __('mts.status_cancel');
            case MtsStatusEnum::COMPLETE:
                return __('mts.status_complete');
            default:
                return __('mts.status_normal');
        }
    }

    public function getStatusLabelAttribute() {
        if ($this->attributes['mts_status'] == MtsStatusEnum::NORMAL) {
            return "badge";
        } else if ($this->attributes['mts_status'] == MtsStatusEnum::PROCESSING) {
            return "badge badge-warning";
        } else if ($this->attributes['mts_status'] == MtsStatusEnum::COMPLETE) {
            return "badge badge-success";
        } else if ($this->attributes['mts_status'] == MtsStatusEnum::CANCEL) {
            return "badge badge-danger";
        }
    }
}
