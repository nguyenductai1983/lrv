<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
;
class OrderItem extends Model
{
    use SoftDeletes;
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_order_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
    ];

    /**
     * Has one customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orders()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function messure(){
        return $this->belongsTo(MessureUnit::class,'messure_unit_id','id');
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
    /**
     * Get created at attribute
     *
     * @return string
     */
    public function getCreatedDateAttribute()
    {
        return $this->created_at->format(config('app.date_format'));
    }
    public function getProductdangerAttribute() {
        $product = Product::find($this->attributes['product_id']);
        return $product->danger;
    }
}
