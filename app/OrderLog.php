<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_order_log';

    public function user() {
        return $this->belongsTo(User::class, 'create_user_id', 'id');
    }
}
