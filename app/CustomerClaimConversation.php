<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerClaimConversation extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_customer_claim_conversation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
