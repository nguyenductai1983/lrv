<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'receivers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'address',
        'telephone',
        'cellphone',
        'postal_code',
        'country_id',
        'province_id',
        'city_id',
        'relationship'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name'
    ];

    /**
     * Has one transport
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transport()
    {
        return $this->hasOne(Transport::class, 'receiver_id', 'id');
    }

    /**
     * Belong to Country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * Belong to Province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /**
     * Belong to City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * Get full_name attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $name = $this->first_name;
        if ($this->middle_name != '') {
            $name .= (' ' . $this->middle_name);
        }
        return ($name . ' ' . $this->last_name);
    }
}
