<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Components\ConvertsUtil;
use App\Components\TextUtil;
//  use Appstract\Stock\HasStock;
use App\Http\Controllers\Admin\stockMutations\HasStock;
// D:\Program Files\xampp\htdocs\Laravel7\app\Http\Controllers\Admin\stockMutations\HasStock.php


class Product extends Model
{
    use HasStock;
    /**
     * Table to use
     */
    protected $table = 'products';

    /**
     * Attributes are guarded
     */
    protected $guarded = [];
    /**
     * Relationship with product groups
     */
    public function group()
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id', 'id');
    }

    /**
     * Relationship with agency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function agencies()
    {
        return $this->belongsToMany(Agency::class, 'agency_product', 'product_id', 'agency_id')
                ->withPivot(['pickup_fee', 'price', 'weight', 'currency_id', 'weight_unit_id', 'messure_unit_id', 'messure_unit_code', 'currency_code']);

    }

    public function thumbnails()
    {
        return $this->belongsTo(File::class, 'thumbnail_id', 'id');
    }

    public function currencies(){
        return $this->belongsTo(Currency::class, 'currency_id','id');
    }

    public function messureunits(){
        return $this->belongsTo(MessureUnit::class, 'messure_unit_id','id');
    }

    public function weightunits(){
        return $this->belongsTo(WeightUnit::class, 'weight_unit_id','id');
    }

    public function getProductNameAttribute() {
        return $this->attributes['name'];
    }

    public function getPerDiscountAttribute() {
        if($this->attributes['price'] > 0 && $this->attributes['price'] > $this->attributes['sale_price']){
            return ConvertsUtil::numberFormat(100 * (1 - ($this->attributes['sale_price'] / $this->attributes['price'])));
        }
        return 0;
    }

    public function getOriginPriceFormatAttribute() {
        return $this->attributes['price'];
    }

    public function getSalePriceFormatAttribute() {
        return $this->attributes['sale_price'];
    }

    public function getFrontendUrlAttribute() {
        $path = "/product/detail/" . TextUtil::getUrlAlias($this->attributes['name']) . "/" . $this->attributes['id'];
        return url($path);
    }
    public function warehouse()
    {
        return $this->belongsToMany(warehouse::class, 'warehouse_products', 'product_id', 'warehouse_id');
    }
}
