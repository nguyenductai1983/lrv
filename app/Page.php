<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * Table to use
     */
    protected $table = 'pages';

    /**
     * Attributes are guarded
     */
    protected $guarded = [];

    /**
     * Relationship with menu page translate
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function translate()
    {
        return $this->hasOne(PageTranslate::class, 'page_id', 'id')
                    ->where('lang', '=', app()->getLocale());
    }

    /**
     * Relationship with menu page translate
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translates()
    {
        return $this->hasMany(PageTranslate::class, 'page_id', 'id');
    }
    public function agencies() {
        return $this->belongsToMany(Agency::class, 'agencie_page', 'page_id', 'agencie_id');
    }
}
