<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\TransportStatusEnum;
use App\Enum\TransportPaymentStatusEnum;
use App\Enum\EshiperEnum;

class Transport extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_transport';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
    ];

    /**
     * Has one customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function orders() {
        return $this->hasMany(Order::class, 'transport_id', 'id');
    }

    public function sender_city() {
        return $this->belongsTo(City::class, 'sender_city_id', 'id');
    }

    public function sender_province() {
        return $this->belongsTo(Province::class, 'sender_province_id', 'id');
    }

    public function sender_country() {
        return $this->belongsTo(Country::class, 'sender_country_id', 'id');
    }

    public function receiver_city() {
        return $this->belongsTo(City::class, 'receiver_city_id', 'id');
    }
    public function ward_city() {
        return $this->belongsTo(Ward::class, 'receiver_ward_id', 'id');
    }
    public function receiver_province() {
        return $this->belongsTo(Province::class, 'receiver_province_id', 'id');
    }

    public function receiver_country() {
        return $this->belongsTo(Country::class, 'receiver_country_id', 'id');
    }

    public function getSenderFullNameAttribute() {
        return "{$this->attributes['sender_last_name']} {$this->attributes['sender_middle_name']} {$this->attributes['sender_first_name']}";
    }

    public function getReceiveFullNameAttribute() {
        return "{$this->attributes['receive_last_name']} {$this->attributes['receiver_middle_name']} {$this->attributes['receive_first_name']}";
    }

    public function getSenderCompanyNameAttribute()
    {
        return "{$this->attributes['sender_first_name']} {$this->attributes['sender_middle_name']} {$this->attributes['sender_last_name']}";
    }

    public function getReceiverCompanyNameAttribute()
    {
        return "{$this->attributes['receive_first_name']} {$this->attributes['receiver_middle_name']} {$this->attributes['receive_last_name']}";
    }

    public function getPartnerNameAttribute()
    {
        if(!empty($this->attributes['carrer_quote'])){
            return __('transport.carrier_eshipper');
        }
        return __('transport.carrier_me');
    }

    public function getCreatedDateAttribute()
    {
        return $this->created_at->format(config('app.date_format'));
    }

    public function getStatusNameAttribute() {
        switch ($this->attributes['transport_status']) {
            case TransportStatusEnum::STATUS_PROCESSING:
                return __('order.status_processing');
            case TransportStatusEnum::STATUS_CANCEL:
                return __('order.status_cancel');
            case TransportStatusEnum::STATUS_COMPLETE:
                return __('order.status_complete');
            default:
                return __('transport.status_new');
        }
    }

    public function getStatusLabelAttribute() {
        switch ($this->attributes['transport_status']) {
            case TransportStatusEnum::STATUS_PROCESSING:
                return "badge badge-warning";
            case TransportStatusEnum::STATUS_CANCEL:
                return "badge badge-danger";
            case TransportStatusEnum::STATUS_COMPLETE:
                return "badge badge-success";
            default:
                return "badge";
        }
    }

    public function getEshipperStatusNameAttribute() {
        switch ($this->attributes['eshiper_shipping_status']) {
            case EshiperEnum::READY_FOR_SHIPPING:
                return __('order.eshiper_status_ready_shipping');
            case EshiperEnum::IN_TRANSIT:
                return __('order.eshiper_status_in_transit');
            case EshiperEnum::DELIVERED:
                return __('order.eshiper_status_delivered');
            case EshiperEnum::CANCELLED:
                return __('order.eshiper_status_cancel');
            case EshiperEnum::EXCEPTION:
                return __('order.eshiper_status_exception');
            case EshiperEnum::CLOSED:
                return __('order.eshiper_status_closed');
            case EshiperEnum::READY_FOR_CHECKOUT_DHLEC:
                return __('order.eshiper_status_ready_checkout_dhlec');
            case EshiperEnum::READY_TO_PROCESS_DHLEC:
                return __('order.eshiper_status_ready_process_dhlec');
            default:
                return __('order.eshiper_status_undefine');
        }
    }

    public function getEshipperStatusLabelAttribute() {
        switch ($this->attributes['eshiper_shipping_status']) {
            case EshiperEnum::READY_FOR_SHIPPING:
                return "badge badge-warning";
            case EshiperEnum::IN_TRANSIT:
                return "badge badge-warning";
            case EshiperEnum::DELIVERED:
                return "badge badge-success";
            case EshiperEnum::CANCELLED:
                return "badge badge-danger";
            case EshiperEnum::EXCEPTION:
                return "badge badge-danger";
            case EshiperEnum::CLOSED:
                return "badge badge-success";
            case EshiperEnum::READY_FOR_CHECKOUT_DHLEC:
                return "badge badge-warning";
            case EshiperEnum::READY_TO_PROCESS_DHLEC:
                return "badge badge-warning";
            default:
                return "badge";
        }
    }

    public function getPaymentStatusLabelAttribute() {
        if ($this->attributes['payment_status'] == TransportPaymentStatusEnum::NOT_PAID) {
            return "badge";
        } else if ($this->attributes['payment_status'] == TransportPaymentStatusEnum::PART_PAID) {
            return "badge badge-warning";
        } else if ($this->attributes['payment_status'] == TransportPaymentStatusEnum::FULL_PAID) {
            return "badge badge-success";
        } else {
            return "badge badge-warning";
        }
    }

    public function getPaymentStatusNameAttribute() {
        switch ($this->attributes['payment_status']) {
            case TransportPaymentStatusEnum::PART_PAID:
                return __('order.payment_status_part_paid');
            case TransportPaymentStatusEnum::FULL_PAID:
                return __('order.payment_status_full_paid');
            case TransportPaymentStatusEnum::NOT_PAID:
                return __('order.payment_status_not_paid');
            default:
                return __('order.payment_status_not_paid');
        }
    }
}
