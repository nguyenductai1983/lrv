<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_voucher';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
