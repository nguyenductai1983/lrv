<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\OrderTrackingStatusEnum;

class OrderTracking extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_order_tracking';

    public function user() {
        return $this->belongsTo(User::class, 'create_user_id', 'id');
    }

    public function getShippingStatusNameAttribute() {
        switch ($this->attributes['status']) {
            case OrderTrackingStatusEnum::STATUS_STOCK_IN:
                return __('order.shipping_status_stockin');
            case OrderTrackingStatusEnum::STATUS_SHIPMENT:
                return __('order.shipping_status_shipment');
            case OrderTrackingStatusEnum::STATUS_STOCK_OUT:
                return __('order.shipping_status_stockout');
            case OrderTrackingStatusEnum::STATUS_REVICER:
                return __('order.shipping_status_local');
            case OrderTrackingStatusEnum::STATUS_READY_TO_SHIP:
                return __('order.shipping_status_ready');
            case OrderTrackingStatusEnum::STATUS_CARRIER:
                return __('order.shipping_status_carrier');
            case OrderTrackingStatusEnum::STATUS_DONE:
                return __('order.shipping_status_done');
            default:
                return __('order.shipping_status_new');
        }
    }
}
