<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    /**
     * Table to use
     */
    protected $table = 'provinces';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'country_id',
        'name',
        'postal_code'
    ];

    /**
     * Relationship with country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * Relationship with cities
     */
    public function cities()
    {
        return $this->hasMany(City::class, 'province_id', 'id');
    }
    //thêm
    public function wards()
    {
        return $this->hasMany(Ward::class, 'province_id', 'id');
    }


    /**
     * Has many Customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers()
    {
        return $this->hasMany(Customer::class, 'province_id', 'id');
    }
}
