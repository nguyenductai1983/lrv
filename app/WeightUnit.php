<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\WeightUnitEnum;

class WeightUnit extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_weight_unit';
    
    protected $fillable = [
        'code',
        'name',
        'display_order',
        'is_default',
        'radio'
    ];
    
    public function getIsDefaultNameAttribute() {
        switch ($this->attributes['is_default']) {
            case WeightUnitEnum::IS_DEFAULE_TRUE:
                return __('label.yes');
            default:
                return __('label.no');
        }
    }
}
