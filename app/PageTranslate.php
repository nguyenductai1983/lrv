<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslate extends Model
{
    /**
     * Table to use
     */
    protected $table = 'page_translates';

    /**
     * Attributes are guarded
     */
    protected $guarded = [];

    /**
     * Relationship with page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }
    
    public function getThumnailAttribute($value)
    {
//        return Storage::url($value);
        return $value ? ('/storage' . $value) : $value;
    }
}
