<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * Table to use
     */
    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'country_id',
        'province_id',
        'name',
        'shipping_fee',
        'postal_code',
        'pickup_fee'
    ];

    /**
     * Relationship with country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * Relationship with province
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }
    //thêm
    public function wards()
    {
        return $this->hasMany(Ward::class, 'city_id', 'id');
    }

    /**
     * Has many Customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers()
    {
        return $this->hasMany(Customer::class, 'city_id', 'id');
    }
}
