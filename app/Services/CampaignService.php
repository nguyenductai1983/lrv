<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Campaign;
use App\Customer;
use App\Order;
use App\CampaignLog;

class CampaignService {
    public static function setCouponByOrder($couponCode, $orderId){
        if(empty($couponCode)){
            return ["success" => false, "message" => 'Vui lòng nhập mã khuyến mại', "data" => []];
        }
        $campaign = Campaign::where(['code' => $couponCode])->first();
        if (empty($campaign)) {
            return ["success" => false, "message" => 'Mã khuyến mại không tồn tại', "data" => []];
        }
        // Kiểm tra tình trạng của campaign
        if($campaign->status != 1){
            return ["success" => false, "message" => 'Chương trình khuyến mại không thỏa mãn', "data" => []];
        }
        // Kiểm tra tồn tại của đơn hàng
        $order = Order::where(['id' => $orderId])->first();
        if (empty($order)) {
            return ["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []];
        }
        if(!empty($order->coupon_code)){
            return ["success" => false, "message" => 'Đơn hàng đã sử dụng mã khuyến mại', "data" => []];
        }

        // Kiểm tra nhóm khách hàng
        $customer = Customer::where(['id' => $order->customer_id])->first();
        if (empty($customer)) {
            return ["success" => false, "message" => 'Khách hàng không tồn tại', "data" => []];
        }
        if(!empty($campaign->customer_id) && $campaign->customer_id != $customer->id){
            return ["success" => false, "message" => 'Khách hàng không thuộc đối tượng áp dụng', "data" => []];
        }
        if(!empty($campaign->customer_group_id) && $campaign->customer_group_id != $customer->customer_group_id){
            return ["success" => false, "message" => 'Khách hàng không thuộc đối tượng áp dụng', "data" => []];
        }

        // Kiểm tra trọng lượng
        if(!empty($campaign->weight) && $campaign->weight > $order->total_weight){
            return ["success" => false, "message" => 'Trọng lượng đơn hàng chưa thỏa mãn dd', "data" => []];
        }

        // Kiểm tra thời gian
        $currentTime = date("Y-m-d H:i:s");
        if(!empty($campaign->from_date) && $campaign->from_date > $currentTime){
            return ["success" => false, "message" => 'Mã khuyến mại đã hết hạn', "data" => []];
        }
        if(!empty($campaign->to_date) && $campaign->to_date < $currentTime){
            return ["success" => false, "message" => 'Mã khuyến mại đã hết hạn', "data" => []];
        }

        // Kiểm tra tổng lượt dùng campaign
        $totalUse = Order::where(['coupon_code' => $couponCode])->count();
        if ($totalUse >= $campaign->num_of_use) {
            return ["success" => false, "message" => 'Mã khuyến mại hết lượt sử dụng', "data" => []];
        }
        // Kiểm tra lượt dùng với KH
        $totalCustomerUse = Order::where(['coupon_code' => $couponCode, 'customer_id' => $customer->id])->count();
        if ($totalCustomerUse >= $campaign->apply_per_customer) {
            return ["success" => false, "message" => 'Khách hàng sử dụng vượt lượt sử dụng', "data" => []];
        }

        if($campaign->amount > 0){
            $order->coupon_code = $couponCode;
            $order->coupon_amount = $campaign->amount;
            $order->coupon_time = date("Y-m-d H:i:s");

            $order->total_final -= $order->coupon_amount;
            if($order->total_final < 0){
                $order->total_final = 0;
            }
            $order->update();

            $campaignLog = new CampaignLog();
            $campaignLog->customer_id = $order->customer_id;
            $campaignLog->order_id = $order->id;
            $campaignLog->coupon_code = $order->coupon_code;
            $campaignLog->coupon_amount = $campaign->amount;
            $campaignLog->weight = $order->total_weight;
            $campaignLog->save();

            $numberUse = Order::where('coupon_code', '=', $campaign['coupon_code'])->count();
            if($numberUse + 1 >= $campaign->num_of_use){
                $campaign->is_used = 1;
                $campaign->update();
            }
        }

        return ["success" => true, "message" => 'Successful', "data" => ['coupon_amount' => number_format($campaign->amount, 2, '.', ','), 'total_final' => number_format($order->total_final, 2, '.', ',')]];
    }


    public static function removeCouponByOrder($orderId){
        // Kiểm tra tồn tại của đơn hàng
        $order = Order::where(['id' => $orderId])->first();
        if (empty($order)) {
            return ["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []];
        }
        if(empty($order->coupon_code)){
            return ["success" => false, "message" => 'Đơn hàng chưa sử dụng mã khuyến mại', "data" => []];
        }

        $campaign = Campaign::where(['code' => $order->coupon_code])->first();
        if (empty($campaign)) {
            return ["success" => false, "message" => 'Mã khuyến mại không hợp lệ', "data" => []];
        }

        if($campaign->amount > 0){
            $order->coupon_code = null;
            $order->coupon_amount = null;
            $order->coupon_time = null;
            $order->total_final += $campaign->amount;
            if($order->total_final < 0){
                $order->total_final = 0;
            }
            $order->update();

            CampaignLog::where('order_id', '=', $order->id)->delete();

            $campaign->is_used = 0;
            $campaign->update();
        }

        return ["success" => true, "message" => 'Successful', "data" => ['coupon_amount' => number_format($campaign->amount, 2, '.', ','), 'total_final' => number_format($order->total_final, 2, '.', ',')]];
    }

    public static function getAmountCoupon($couponCode, $customerId, $weight) {
        $campaign = Campaign::where(['code' => $couponCode])->first();
        if (empty($campaign)) {
            $result= __('campaign.coupon_not_active');
            return ["amount" => false, "message" =>$result];
        }
        // Kiểm tra trọng lượng
        if(!empty($campaign->weight) && $campaign->weight > $weight){
            $result= __('campaign.weight_required');
            return ["amount" => false, "message" =>$result];
        }
        // Kiểm tra tình trạng của campaign
        if($campaign->status != 1){
            $result= __('campaign.not_active');
            return ["amount" => false, "message" =>$result];
        }
        // Kiểm tra nhóm khách hàng
        $customer = Customer::where(['id' => $customerId])->first();
        if (empty($customer)) {
            $result= __('campaign.customer_group_is_not');
            return ["amount" => false, "message" =>$result];
        }

        if(!empty($campaign->customer_id) && $campaign->customer_id != $customer->id){
           $result= __('campaign.customer_group_is_not');
            return ["amount" => false, "message" =>$result];
        }

        if(!empty($campaign->customer_group_id) && $campaign->customer_group_id != $customer->customer_group_id){
            $result= __('campaign.customer_group_is_not');
            return ["amount" => false, "message" =>$result];
        }

        // Kiểm tra thời gian
        $currentTime = date("Y-m-d H:i:s");
        if(!empty($campaign->from_date) && $campaign->from_date > $currentTime){
            $result= __('campaign.date_invalid');
            return ["amount" => false, "message" =>$result];
        }
        if(!empty($campaign->to_date) && $campaign->to_date < $currentTime){
            $result= __('campaign.date_invalid');
            return ["amount" => false, "message" =>$result];
        }
        // Kiểm tra tổng lượt dùng campaign
        $totalUse = Order::where(['coupon_code' => $couponCode])->count();
        if ($totalUse >= $campaign->num_of_use) {
            $result= __('campaign.out_coupon');
            return ["amount" => false, "message" =>$result];
        }
        // Kiểm tra lượt dùng với KH
        $totalCustomerUse = Order::where(['coupon_code' => $couponCode, 'customer_id' => $customerId])->count();
        if ($totalCustomerUse >= $campaign->apply_per_customer) {
           $result= __('campaign.coupon_used');
            return ["amount" => false, "message" =>$result];
        }

        return $campaign->amount;
    }

}
