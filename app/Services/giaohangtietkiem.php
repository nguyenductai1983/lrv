<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use App\ShippingPackageLocal;
use App\WeightUnit;
use App\Enum\WeightUnitEnum ;
use App\Order;
use App\Warehouse;
use App\CurrencyRate;
class giaohangtietkiem
{
    private static function curl_header($url,$method,$data)
    {
        $token=config('giaohangtietkiem.token');
        $result = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS =>  $data,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "token: ".$token
            ),
        );
        return $result;
    }
    private static function Run_curl($url,$method,$data=null)
    {
        $data_curl = giaohangtietkiem::curl_header($url,$method,$data);
        $curl = curl_init();
        curl_setopt_array($curl,$data_curl);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
        # code...
    }
    public static function createOrder($pick_session, ShippingPackageLocal $shippingPackageLocal, Order $Order, Warehouse $warehouse)
    // public static function createOrder()
    {
        $data = [];
        $weight_unit=WeightUnit::find(WeightUnitEnum::KG);
        $weight_unit=$weight_unit->radio;
        $no = 0;
        foreach ($Order->order_items as $item) {
            $data["products"][$no]["name"] = $item->name;
            $data["products"][$no]["weight"] = $item->sub_total_weight*$weight_unit;
            $data["products"][$no]["quantity"] = $item->quantity;
            $data["products"][$no]["product_code"] =  $item->product->code;
            $no++;
        }
        // $data["order"] = [];
        $data["order"]['id'] = $shippingPackageLocal->shipping_code; //String - mã đơn hàng thuộc hệ thống của đối tác
        $data["order"]['pick_name'] = $warehouse->contact_person; //String - Tên người liên hệ lấy hàng hóa
        $data["order"]['pick_address'] =  $warehouse->address; // String - Địa chỉ ngắn gọn để lấy nhận hàng hóa.
        $data["order"]['pick_province'] = $warehouse->province->name; //String - Tên tỉnh/thành phố nơi lấy hàng hóa
        $data["order"]['pick_district'] = $warehouse->city->name; // String - Tên quận/huyện nơi lấy hàng hóa
        $data["order"]['pick_ward'] = $warehouse->ward->name; // String - Tên quận/huyện nơi lấy hàng hóa
        $data["order"]['pick_tel'] = $warehouse->telephone; //String - Số điện thoại liên hệ nơi lấy hàng hóa

        $data["order"]['tel'] = $Order->receiver_phone; //String - Số điện thoại người nhận hàng hóa
        $data["order"]['name'] = $Order->ReceiveFullName; //String - tên người nhận hàng
        $data["order"]['address'] = $Order->receiver_address; //String - Địa chỉ chi tiết của người nhận hàng,
        $data["order"]['province'] = $Order->receiver_province_name; //String - Tên tỉnh/thành phố của người nhận hàng hóa
        $data["order"]['district'] = $Order->receiver_city_name; //String - Tên quận/huyện của người nhận hàng hóa
        $data["order"]['ward'] = $Order->receiver_ward_name; //String - Tên phường/xã của người nhận hàng hóa (Bắt buộc khi không có đường/phố)
        $data["order"]['hamlet'] = 'Khác'; //String - Tên thôn/ấp/xóm/tổ/… của người nhận hàng hóa. Nếu không có, vui lòng điền “Khác”
        $data["order"]['is_freeship'] = '1';
        // Integer - Freeship cho người nhận hàng. Nếu bằng 1 COD sẽ chỉ thu người nhận hàng số tiền bằng pick_money,
        //nếu bằng 0 COD sẽ thu tiền người nhận số tiền bằng pick_money + phí ship của đơn hàng, giá trị mặc định bằng 0
        // $data["order"]['pick_date'] = '2021-04-04"'; //String YYYY/MM/DD - Hẹn ngày lấy hàng -
        //mặc định không sử dụng được field này, cấu hình riêng cho từng gói dịch vụ
        $currency = $Order->currency_id;
        $convertcurrency =CurrencyRate::where([['currency_from_id','=', $currency],['currency_to_id','=',$shippingPackageLocal->currency_id]])->first();
        if(!empty($convertcurrency ))
        {
            $money=(int)$Order->total_remain_amount*(int)$convertcurrency->rate;
        }
        else
        {
            $money=(int)$Order->total_remain_amount*20000;
        }

        if($money<1000)
        {
            $money=0;
        }
        $data["order"]['pick_money'] = $money; //Integer - Số tiền CoD. Nếu bằng 0 thì không thu tiền CoD. Tính theo VNĐ
        $data["order"]['note'] =  substr($Order->user_note, 0, 119);;
        //String - Ghi chú đơn hàng. Vd: Khối lượng tính cước tối đa: 1.00 kgTừ 24/2/2020 ghi chú tối đa cho phép là 120 kí tự
        $data["order"]['value'] = $Order->total_declare_price;
        //Interger (VNĐ) - Giá trị đóng bảo hiểm, là căn cứ để tính phí bảo hiểm và bồi thường khi có sự cố.
        $data["order"]['transport'] = 'fly'; //String - Phương thức vâng chuyển road ( bộ ) , fly (bay).
        //Nếu phương thức vận chuyển không hợp lệ thì GHTK sẽ tự động nhảy về PTVC mặc định
        $data["order"]['pick_option'] = 'cod';
        //String - Nhận một trong hai giá trị cod và post, mặc định là cod, biểu thị lấy hàng bởi COD
        //hoặc Shop sẽ gửi tại bưu cục
        $data["order"]['deliver_option'] = 'xteam'; //String - Gía trị là xteam nếu lựa chọn phương thức vận chuyển xfast
         $data["order"]['pick_session'] = $pick_session; //String - Giá trị lấy từ response của API check dịch vụ XFAST (phiên lấy xfast)
        // 2	Lấy trước 8h30 & Giao trước 10h30
        // 5	Lấy trước 10h30 & Giao trước 12h30
        // 8	Lấy trước 13h30 & Giao trước 15h30
        // 11	Lấy trước 15h30 & Giao trước 17h30
        // 2_mai	Lấy trước 8h30 & Giao trước 10h30 ngày mai
        // 5_mai	Lấy trước 10h30 & Giao trước 12h30 ngày mai
        // 8_mai	Lấy trước 13h30 & Giao trước 15h30 ngày mai
        // 11_mai	Lấy trước 15h30 & Giao trước 17h30 ngày mai
        $data = json_encode($data);
        $url=config('giaohangtietkiem.url_create_order');
        $response= giaohangtietkiem::Run_curl($url,'POST',$data);
        return $response;
    }
    public static function cancelOrder($tracking_code)
    {
        $url=config('giaohangtietkiem.url_cancel');
        $url=$url.$tracking_code;
        $response= giaohangtietkiem::Run_curl($url,'POST');
        return $response;
    }
    public static function cancelOrderGHTK($ghtk_tracking_code)
    {
        $url=config('giaohangtietkiem.url_cancel_GHTK');
        $url= $url.$ghtk_tracking_code;
        $response= giaohangtietkiem::Run_curl($url,'POST');
        return $response;
    }

    public static function getLabel($ghtk_tracking_code)
    {
        $url=config('giaohangtietkiem.url_label_pdf');
        $url= $url.$ghtk_tracking_code;
        $response= giaohangtietkiem::Run_curl($url,'GET');
        return $response;
    }
}
