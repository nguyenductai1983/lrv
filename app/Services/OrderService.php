<?php
namespace App\Services;
use App\Agency;
use App\MTSSurcharge;
class OrderService {
    public static function getMinFee($cusProvinceId, $fromProvinceId) {
        $caPriority = config('app.ca_priority');
        $vnPriority = config('app.vn_priority');
        $caVnPriorityFee = config('app.ca_priority_vn_priority_fee');
        $caVnPriorityDefaultFee = config('app.ca_priority_vn_default_fee');
        $caVnDefaultPriorityFee = config('app.ca_default_vn_priority_fee');
        $caVnDefaultFee = config('app.ca_default_vn_default_fee');
        $caPriorityFlag = false;
        $vnPriorityFlag = false;
        $minFee = 0;
        if(in_array($cusProvinceId, $caPriority, true) || $cusProvinceId === true){
            $caPriorityFlag = true;
        }
        if(in_array($fromProvinceId, $vnPriority, true)){
            $vnPriorityFlag = true;
        }
        if($caPriorityFlag){
            if($vnPriorityFlag){
                $minFee = $caVnPriorityFee;
            }else{
                $minFee = $caVnPriorityDefaultFee;
            }
        }else{
            if($vnPriorityFlag){
                $minFee = $caVnDefaultPriorityFee;
            }else{
                $minFee = $caVnDefaultFee;
            }
        }
        return $minFee;
    }
    public static function getMinFeeadmin($Agency, $fromProvinceId) {
        $vnPriority = config('app.vn_priority');
        $vn_hcm_priority = config('app.vn_hcm_priority');
        $vnPriorityFlag = false;
        $agencyfee = Agency::where('id',$Agency)->first(['min_fee']);
        $minFee = $agencyfee['min_fee'];
        if(in_array($fromProvinceId, $vnPriority, true)){
            $vnPriorityFlag = true;
        }
            if(!$vnPriorityFlag){
                $minFee = $minFee+$vn_hcm_priority;
            }
        return $minFee;
    }
    public static function getPerFeeMts($fromProvinceId) {
        $vnPriority = config('app.vn_priority');
        $mtsPerFee = config('mts.per_fee');
        $mtsMaxPerFee = config('mts.max_per_fee');
        $vnPriorityFlag = false;
        $perFee = $mtsMaxPerFee;
        if(in_array($fromProvinceId, $vnPriority, true)){
            $vnPriorityFlag = true;
        }
        if($vnPriorityFlag){
            $perFee = $mtsPerFee;
        }
        return $perFee;
    }
    public static function Surcharge(float $amount,int $discount,$type_discount)
    {
        $result =[];
        $query = MTSSurcharge::where([]);
        $query->where('amount', '<=', $amount);
        $query->orderByDesc('amount');
        $query->limit(1);
        $amount=(int)$amount;
        $discount=(int)$discount;
        $MTSSurcharge=$query->Get();
        $rate_number=$MTSSurcharge[0]->rate*$amount/100;
        $fee_number= $MTSSurcharge[0]->fee;
        $surcharge_number = $MTSSurcharge[0]->surcharge*$amount/100;
        $total_fee= $rate_number+$fee_number+ $surcharge_number;
        // nếu chiết phần trăm thì tính phần trăm phí
        if($discount > 0 && $type_discount == 1){
            $discount= ($amount * $discount) / 100;
        }
        // nếu chiết khấu là số tiền cụ thể
        else if($discount > 0 && $type_discount == 2){
            $discount =  $discount;
        }
        // kiem tra lai chiet khau
        if($discount>$total_fee)
        {
            $discount=$total_fee;
        }
        $result['discount'] = $discount;
        $total_fee=$total_fee - $discount;
        // tính huê hồng cho đại lý
        $result['commission']= $total_fee*config('mts.per_agency_fee');
        $result['total_fee'] = $total_fee;
        $result['total_final'] = $amount + $total_fee;
        return $result;
    }
}
