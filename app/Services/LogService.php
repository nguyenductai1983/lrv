<?php
namespace App\Services;

use Illuminate\Support\Facades\Log;

class LogService
{
    /**
     * @param \Exception $e
     *
     * @return string
     */
    public static function error($e) {
        $msg = "File: {$e->getFile()} | Line: {$e->getLine()} | Code: {$e->getCode()} | Msg: {$e->getMessage()}";

        return Log::error($msg);
    }
}
