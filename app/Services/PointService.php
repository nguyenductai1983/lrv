<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Order;
use App\Customer;
use App\CustomerPointLog;
use App\PointConfig;
use App\PointConvert;
use App\Campaign;
use App\Transport;
use App\Enum\PointEnum;
use Exception;
class PointService {

    public static function addPointToCustomer($customerId, $type) {
        $customer = Customer::where(['id' => $customerId])->first();
        if (empty($customer)) {
            return;
        }
        if(empty($customer->ref_code) || $customer->is_used){
           return;
        }
        $customerTo = Customer::where(['code' => $customer->ref_code])->first();
        if (empty($customerTo)) {
            return;
        }
        $pointConfig = PointConfig::where(['type' => $type, 'is_active' => PointEnum::IS_ACTIVE])->first();
        if (empty($pointConfig)) {
            return;
        }
        $customerPointLogExist = CustomerPointLog::where(['use_ref_code' => $customer->ref_code])->first();
        if(!empty($customerPointLogExist)){
            return;
        }
        $customerPointLog = new CustomerPointLog();
        $customerPointLog->use_ref_code = $customer->ref_code;
        $customerPointLog->customer_id = $customerTo->id;
        $customerPointLog->point = $pointConfig->point;
        $customerPointLog->type = PointEnum::TYPE_ADD_POINT;
        $customerPointLog->save();

        $customerTo->point += $pointConfig->point;
        $customerTo->update();

        $customer->is_used = 1;
        $customer->update();

        return;
    }

    /**
     * Cộng điểm khi thùng hàng của KH xuất Shipment thành công
     */
    public static function addPointOrder(Order $order, $type) {
        $total_weight = isset($order->total_weight) ? $order->total_weight : 0;
        if($total_weight <= 0){
            return;
        }

        $customer = Customer::where(['id' =>$order->customer_id])->first();
        if (empty($customer)) {
            return;
        }

        $pointConfig = PointConfig::where(['type' => $type, 'is_active' => PointEnum::IS_ACTIVE])->first();
        if (empty($pointConfig)) {
            return;
        }

        $customerPointLogExist = CustomerPointLog::where(['use_ref_code' => $order->code])->first();
        if(!empty($customerPointLogExist)){
            return;
        }

        $point = $total_weight * $pointConfig->point;
        if($pointConfig->min_point > 0 && $point < $pointConfig->min_point){
            $point = $pointConfig->min_point;
        }
        if($pointConfig->max_point > 0 && $point > $pointConfig->max_point){
            $point = $pointConfig->max_point;
        }

        $customerPointLog = new CustomerPointLog();
        $customerPointLog->use_ref_code = $order->code;
        $customerPointLog->customer_id = $customer->id;
        $customerPointLog->point = $point;
        $customerPointLog->type = PointEnum::TYPE_ADD_POINT;
        $customerPointLog->save();

        $customer->point += $point;
        $customer->update();

        return;
    }

    /**
     * Cộng điểm khi thùng hàng của KH xuất Shipment thành công
     */
    public static function addPointTransport(Transport $transport, $type) {
        $total_weight = isset($transport->total_weight) ? $transport->total_weight : 0;
        if($total_weight <= 0){
            return;
        }

        $customer = Customer::where(['id' =>$transport->customer_id])->first();
        if (empty($customer)) {
            return;
        }

        $pointConfig = PointConfig::where(['type' => $type, 'is_active' => PointEnum::IS_ACTIVE])->first();
        if (empty($pointConfig)) {
            return;
        }

        $customerPointLogExist = CustomerPointLog::where(['use_ref_code' => $transport->code])->first();
        if(!empty($customerPointLogExist)){
            return;
        }

        $point = $total_weight * $pointConfig->point;
        if($pointConfig->min_point > 0 && $point < $pointConfig->min_point){
            $point = $pointConfig->min_point;
        }
        if($pointConfig->max_point > 0 && $point > $pointConfig->max_point){
            $point = $pointConfig->max_point;
        }

        $customerPointLog = new CustomerPointLog();
        $customerPointLog->use_ref_code = $transport->code;
        $customerPointLog->customer_id = $customer->id;
        $customerPointLog->point = $point;
        $customerPointLog->type = PointEnum::TYPE_ADD_POINT;
        $customerPointLog->save();

        $customer->point += $point;
        $customer->update();

        return;
    }

    public static function convertPointToCoupon(Customer $customer) {
        $pointConvert = PointConvert::where(['is_active' => PointEnum::IS_ACTIVE])->first();
        if (empty($pointConvert)) {
            return false;
        }
        if($customer->point < $pointConvert->point){
            return false;
        }
        try {
            DB::beginTransaction();
            //Tạo Coupon Code
            $campaign = new Campaign();
            $campaign->customer_id = $customer->id;
            $campaign->name = __('campaign.convert_point');
            $campaign->amount = $pointConvert->amount;
            $campaign->num_of_use = 1;
            $campaign->apply_per_customer = 1;
            $campaign->save();

            $campaign->update(['code' =>  'CP' . rand(10000, 99999) . $campaign->id]);

            // Lưu log trừ
            $customerPointLog = new CustomerPointLog();
            $customerPointLog->use_ref_code = $campaign->code;
            $customerPointLog->customer_id = $customer->id;
            $customerPointLog->point = $pointConvert->point;
            $customerPointLog->type = PointEnum::TYPE_SUB_POINT;
            $customerPointLog->save();

            $customer->point -= $pointConvert->point;
            $customer->update();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            return false;
        }
    }

}
