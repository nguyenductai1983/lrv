<?php

/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/10/2018
 * Time: 5:16 PM
 */

namespace App\Services;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Enum\EshiperEnum;
use App\Helpers\Eshiper\EshiperQuoteRequestBuilder;
use App\Components\EshiperRestClient;
use App\Helpers\Eshiper\EshiperShippingRequestBuilder;
use Orchestra\Parser\Xml\Facade as XmlParser;
use App\Components\TextUtil;

class EshiperService {

    public static function cancelOrder($id) {
        $client = new Client();
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <EShipper xmlns:es="http://www.eshipper.net/XMLSchema" username="' . env('ESHIPER_USERNAME') . '" password="' . env('ESHIPER_PASSWORD') . '" version="3.0.0">
                <ShipmentCancelRequest>
                <Order orderId="' . $id . '"/>
                </ShipmentCancelRequest>
                </EShipper>';
        $request = new Request('POST', env('ESHIPER_API_TEST_URL'), ['Content-Type' => 'text/xml; charset=UTF8'], $xml);
        $response = $client->send($request);

        return self::parseRespone($response->getBody()->getContents(), EshiperEnum::CANCEL_TYPE);
    }
// hàm cập nhật trang thái đơn hàng
    public static function getOrder($id) {
        $client = new Client();
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <EShipper xmlns:es="http://www.eshipper.net/XMLSchema" username="' . env('ESHIPER_USERNAME') . '" password="' . env('ESHIPER_PASSWORD') . '" version="3.0.0">
                <OrderInformationRequest>
                <Order orderId="' . $id . '" detailed="true"/>
                </OrderInformationRequest>
                </EShipper>';
        $request = new Request('POST', env('ESHIPER_API_TEST_URL'), ['Content-Type' => 'text/xml; charset=UTF8'], $xml);
        $response = $client->send($request);
        return self::parseRespone($response->getBody()->getContents(), EshiperEnum::INFO_TYPE);
    }

    public static function createOrderByPackage($params){
        $client = new Client();
        $pickup = '';
        if(isset($params['pickup']) && !empty($params['pickup'])){
            $pickup = '<Pickup contactName="'. TextUtil::removeDiacritical($params['pickup']['contact_name']) .'" phoneNumber="'. $params['pickup']['phone_number'] .'" pickupDate="'. $params['pickup']['pickup_date'] .'" pickupTime="'. $params['pickup']['pickup_time'] .'" closingTime="'. $params['pickup']['closing_time'] .'" location="'. TextUtil::removeDiacritical($params['pickup']['location']) .'" />';
        }
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <EShipper xmlns="http://www.eshipper.net/XMLSchema" username="' . env('ESHIPER_USERNAME') . '" password="' . env('ESHIPER_PASSWORD') . '" version="3.0.0">
                    <ShippingRequest serviceId="'. $params['serviceId'] .'" scheduledShipDate="'. $params['scheduledShipDate'] .'" stackable="true" >
                        <From company="'. TextUtil::removeDiacritical($params['addressFrom']['company']) .'" address1="'. TextUtil::removeDiacritical($params['addressFrom']['address1']) .'" city="'. TextUtil::removeDiacritical($params['addressFrom']['city']) .'" state="'. TextUtil::removeDiacritical($params['addressFrom']['state']) .'" zip="'. $params['addressFrom']['zip'] .'" country="'. TextUtil::removeDiacritical($params['addressFrom']['country']) .'" phone="'. $params['addressFrom']['phone'] .'" attention="'. TextUtil::removeDiacritical($params['addressFrom']['attention']) .'" email="'. $params['addressFrom']['email'] .'" residential="'. TextUtil::removeDiacritical($params['addressFrom']['residential']) .'" />
                        <To company="'. TextUtil::removeDiacritical($params['addressTo']['company']) .'" address1="'. TextUtil::removeDiacritical($params['addressTo']['address1']) .'" city="'. TextUtil::removeDiacritical($params['addressTo']['city']) .'" state="'. TextUtil::removeDiacritical($params['addressTo']['state']) .'" zip="'. $params['addressTo']['zip'] .'" country="'. TextUtil::removeDiacritical($params['addressTo']['country']) .'" phone="'. $params['addressTo']['phone'] .'" attention="'. TextUtil::removeDiacritical($params['addressTo']['attention']) .'" email="'. $params['addressTo']['email'] .'"/>
                        <Packages type="Package">
                            '. self::buildPackage($params['packages']) .'
                        </Packages>'.$pickup.'<Payment type="3rd Party"/>
                        <Reference name="'. $params['code'] .'" code="'. $params['code'] .'"/>
                    </ShippingRequest>
                </EShipper>';

        $request = new Request('POST', env('ESHIPER_API_TEST_URL'), ['Content-Type' => 'text/xml; charset=UTF8'], $xml);
        $response = $client->send($request);
        return self::parseRespone($response->getBody()->getContents(), EshiperEnum::SHIPPING_TYPE);
    }
// hàm lấy danh sách đơn vị vận chuyển
    public static function getQuoteEshiper($params) {
        $eshiper_quote_helper = new EshiperQuoteRequestBuilder();
        $eshiper_quote_helper->setRequestType(EshiperEnum::QUOTE_REQUEST, $params['typeAttrs']);
        $eshiper_quote_helper->setAddressFrom($params['addressFrom']);
        $eshiper_quote_helper->setAddressTo($params['addressTo']);
        $eshiper_quote_helper->setPackages($params['packages']);
        if (!empty($params['cod'])) {
            $eshiper_quote_helper->setCOD($params['cod']);
        }
        $eshiper_quote_helper->setFooter();
        if (isset($params['pickup']['is_schedule']) && $params['pickup']['is_schedule'] == EshiperEnum::IS_PICKUP) {
            $eshiper_quote_helper->setPickup($params['pickup']);
        }
        if (!empty($params['cod'])) {
            $eshiper_quote_helper->setCOD($params['cod']);
        }
        $xmlRequest = $eshiper_quote_helper->buildRequest();
        $xmlResponse = EshiperRestClient::callEshiper($xmlRequest);
        if ($xmlResponse===false)
        {
            Log::warning($xmlRequest);
            Log::error($xmlResponse);
        }
        return self::parseRespone($xmlResponse, EshiperEnum::QUOTE_TYPE);
    }

    public static function getShippingEshiper($params) {
        $eshiper_shipping_helper = new EshiperShippingRequestBuilder();
        $eshiper_shipping_helper->setRequestType(EshiperEnum::SHIPPING_REQUEST, $params['typeAttrs']);
        $eshiper_shipping_helper->setAddressFrom($params['addressFrom'], true);
        $eshiper_shipping_helper->setAddressTo($params['addressTo'], true);
        $eshiper_shipping_helper->setPaymentType($params['paymentType']);
        $eshiper_shipping_helper->setPackages($params['packages']);
        $eshiper_shipping_helper->setFooter();
        if (isset($params['pickup']['is_schedule']) && $params['pickup']['is_schedule'] == EshiperEnum::IS_PICKUP) {
            $eshiper_shipping_helper->setPickup($params['pickup']);
        }
        if (!empty($params['cod'])) {
            $eshiper_shipping_helper->setCOD($params['cod']);
        }
        if (!empty($params['reference'])) {
            $reference = $params['reference'];
            $eshiper_shipping_helper->setReference($reference['name'], $reference['code']);
        }
        if (!empty($params['custom'])) {
            $eshiper_shipping_helper->setCustom($params['custom'], $params['countries']);
        }
        $xmlRequest = $eshiper_shipping_helper->buildRequest();
        // Log::error($xmlRequest);
        $xmlResponse = EshiperRestClient::callEshiper($xmlRequest);
        // Log::error($xmlResponse);
        return self::parseRespone($xmlResponse, EshiperEnum::SHIPPING_TYPE);
    }
// xử lý dữ liệu trước khi hiện thị ngày 2020-02-21
    public static function parseRespone($xmlResponse, $type, $ship_date = null) {
        $output['success'] = false;
        $output['message'] = null;
        $output['data'] = [];
        $multiplier = config('express.multiplier');
        if(!$xmlResponse){
            return $output;
        }
        $xml = preg_replace("/(<EShipper(.+)>|<\/EShipper>)/", "", $xmlResponse);
        $xml = XmlParser::extract($xml);
        $errors = $xml->parse([
            'message' => ['uses' => 'Error::Message', 'default' => null]
        ]);
        if ($errors['message']) {
            $output['message'] = $errors['message'];
            return $output;
        }

        switch ($type) {
            case "QUOTE" : $data = $xml->parse([
                    'QUOTE' => ['uses' => 'Quote[::carrierId>carrierId,::carrierName>carrierName,::serviceId>serviceId,::serviceName>serviceName,::modeTransport>modeTransport,::transitDays>transitDays,::deliveryDate>deliveryDate,::currency>currency,::baseCharge>baseCharge,::totalTariff>totalTariff,::baseChargeTariff>baseChargeTariff,::fuelSurchargeTariff>fuelSurchargeTariff,::fuelSurcharge>fuelSurcharge,::totalCharge>totalCharge,Surcharge(::name=@)>surcharge.name,Surcharge(::amount=@)>surcharge.amount]', 'default' => null]
                ]);
                foreach ($data['QUOTE'] as $key => $val) {
                    $data['QUOTE'][$key]['baseCharge'] =  $multiplier* $val['baseCharge'];
                   // $data['QUOTE'][$key]['totalCharge']  = $multiplier *  $val['totalCharge'];
                    //mình chỉ chia cho "BASE CHARGE", "HST" và "GST"
                    $data['QUOTE'][$key]['fuelSurcharge']  =  $val['fuelSurcharge'];
                    $data['QUOTE'][$key]['totalCharge']= ($multiplier* $val['baseCharge']) +  $val['fuelSurcharge'];
                    $surcharge = [];
                    if (!empty($val['surcharge']['name']) && !empty($val['surcharge']['amount'])) {
                       $nameList = array_keys($val['surcharge']['name']);
                       $amountList = array_keys($val['surcharge']['amount']);
                       foreach ($nameList as $k => $v) {
                        if(!empty($amountList[$k]))
                        {
                           $temp['id'] = null;
                           $temp['name'] = $v;
                           if($v==='HST' || $v==='GST')
                           {
                               $temp['amount'] = $amountList[$k]* $multiplier;
                                $data['QUOTE'][$key]['totalCharge']= $data['QUOTE'][$key]['totalCharge']+$temp['amount'];
                           }
                           else
                           {
                               $temp['amount'] = $amountList[$k] ;//* $multiplier;
                               $data['QUOTE'][$key]['totalCharge'] =$data['QUOTE'][$key]['totalCharge'] +$amountList[$k];
                           }
                           $surcharge[] = $temp;
                       }
                    }
                    }
                    $data['QUOTE'][$key]['surcharge'] = $surcharge;
                }
                //het hàm từng nhà vận cuyển
                break;
            case "SHIPPING" : $data['SHIPPING'] = $xml->parse([
                    'orderId' => ['uses' => 'Order::id'],
                    'carrierName' => ['uses' => 'Carrier::carrierName'],
                    'serviceName' => ['uses' => 'Carrier::serviceName'],
                    'referenceCode' => ['uses' => 'Reference::code'],
                    'referenceName' => ['uses' => 'Reference::name'],
                    'pickupNumber' => ['uses' => 'Pickup::confirmationNumber'],
                    'package' => ['uses' => 'Package[::trackingNumber>trackingNumber]'],
                    'statusId' => ['uses' => 'Status::statusId'],
                    'trackingUrl' => ['uses' => 'TrackingURL'],
                    'label' => ['uses' => 'Labels'],
                    'customLabel' => ['uses' => 'CustomsInvoice'],
                ]);
                break;
            case "CANCEL" :
                $data['CANCEL'] = $xml->parse([
                    'orderId' => ['uses' => 'Order::orderId'],
                    'message' => ['uses' => 'Order::message'],
                    'orderStatus' => ['uses' => 'Status::statusId']
                ]);
                break;
            case "INFO" :
                $data['INFO'] = $xml->parse([
                    'orderId' => ['uses' => 'Order::id'],
                    'carrierName' => ['uses' => 'Carrier::carrierName'],
                    'serviceName' => ['uses' => 'Carrier::serviceName'],
                    'package' => ['uses' => 'Package[::trackingNumber>trackingNumber]'],
                    'tracking' => ['uses' => 'TrackingURL'],
                    'statusId' => ['uses' => 'Status::statusId'],
                    'statusName' => ['uses' => 'Status::statusName'],
                    'currency' => ['uses' => 'Quote::currency'],
                    'baseCharge' => ['uses' => 'Quote::baseCharge'],
                    'totalCharge' => ['uses' => 'Quote::totalCharge'],
                    'fuelSurcharge' => ['uses' => 'Quote::fuelSurcharge'],
                    'surcharge' => ['uses' => 'Quote.Surcharge[::id>id,::name>name,::amount>amount]'],
                    'history'   => ['uses' => 'OrderDetails.OrderStatusHistory.Status[::Name>name,::Date>date,::AssignedBy>assigned_by,::Comments>comments]'],
                    'customLabel'   => ['uses' => 'CustomsInvoice']
                ]);
               $surcharge=0;
                if (!empty($data['INFO']['surcharge'])) {
                       foreach ($data['INFO']['surcharge'] as $k => $v) {
                           $temp=$v;
                           // anh Giang hướng dẫn chỉ nhân 0.45 các HST GST và BaseChar
                           if($v['name']==='HST' || $v['name']==='GST')
                           {
                               $temp['amount'] = $v['amount']* $multiplier;
                                $surcharge= $surcharge+$temp['amount'];
                           }
                           else
                           {
                               $temp['amount'] = $v['amount'] ;//* $multiplier;
                               $surcharge =$surcharge +$temp['amount'];
                           }
                           $data['INFO']['surcharge'][$k]=$temp;
                       }
                    }
               $data['INFO']['baseCharge'] = $multiplier * $data['INFO']['baseCharge'];
               $data['INFO']['totalCharge'] = $data['INFO']['baseCharge'];
               $data['INFO']['totalCharge'] += $surcharge;
               $data['INFO']['totalCharge'] += $data['INFO']['fuelSurcharge'];
                break;
            default : $data = null;
        }
        if (empty($data[$type])) {
            $output['message'] = 'Get data error, please F5!';
            return $output;
        }
        $output['success'] = true;
        $output['message'] = null;
        $output['data'] = $data[$type];
        return $output;
    }

    private static function buildPackage($items){
        $xml = '';
        foreach ($items as $item){
           $xml .= '<Package length="'. $item['length'] .'" width="'. $item['width'] .'" height="'. $item['height'] .'" weight="'. $item['total_weight'] .'" type="Package" insuranceAmount="'. $item['total_insurrance_fee'] .'" description=""/>';
        }
        return $xml;
    }
}
