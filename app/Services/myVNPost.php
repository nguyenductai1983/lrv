<?php

/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/10/2018
 * Time: 5:16 PM
 */

namespace App\Services;

use Illuminate\Support\Facades\Log;
use App\Components\myVNPostComponents;
use App\ShippingPackageLocal;
use App\Order;
use App\Warehouse;

class myVNPost {
    private static function curl_header($url,$data,$header="") {
        $http_header= array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        );
        if(!empty($header))
        {
        array_unshift( $http_header,"h-token:".$header);
        }
        $result= array(
         CURLOPT_URL => $url,
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_ENCODING => "",
         CURLOPT_MAXREDIRS => 10,
         CURLOPT_TIMEOUT => 30000,
         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         CURLOPT_CUSTOMREQUEST => "POST",
         CURLOPT_POSTFIELDS =>  $data,
         CURLOPT_HTTPHEADER => $http_header,
         );
         return $result;
         }
    public static function GetAccessToken() {
        $result='';
        $data = myVNPostComponents::GetAccessToken();
        $url=config('vnptpost.GetAccessToken.url');
        $array_curl = myVNPost::curl_header($url,$data);
        $curl = curl_init();
        curl_setopt_array($curl, $array_curl);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $result= $err;
        } else {
            if(json_decode($response)->ErrorMessage)
            {
                return json_decode($response)->ErrorMessage;
            }
            $result=json_decode($response)->Token;
        }
        return $result;
    }
    public static function CreateOrder($typeService, ShippingPackageLocal $shippingPackageLocal, Order $shippingAddress, Warehouse $warehouse)
    {
        $header=self::GetAccessToken();
        $data =myVNPostComponents::CreateOrder($typeService, $shippingPackageLocal, $shippingAddress, $warehouse);
        $data=json_encode($data);
        $url=config('vnptpost.CreateOrder_url');
        $array_curl = self::curl_header($url,$data,$header);
        $curl = curl_init();
        curl_setopt_array($curl, $array_curl);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $result= $err;
            Log::error($err.$shippingPackageLocal->shipping_code);
        } else {
            $result=json_decode($response);

        }
return $result;


    }
}
