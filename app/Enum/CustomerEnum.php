<?php
namespace App\Enum;

abstract class CustomerEnum {
    const STATUS_ACTIVE = 1; // Bình thường
    const STATUS_DEACTIVE = 2; // Chưa xác thực
}
