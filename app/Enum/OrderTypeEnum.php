<?php
namespace App\Enum;

abstract class OrderTypeEnum {
    const TRANSPORT = 1; // Loại hàng transport
    const EXPRESS = 2; // Express
    const YHL = 3; // Yhl
    const QUOTE = 4; // Mua hộ
}
