<?php
namespace App\Enum;

abstract class TransportStatusEnum {
    const STATUS_NEW = 1; // Mới tạo
    const STATUS_PROCESSING = 2; // Dang xử lý
    const STATUS_CANCEL = 3; // Bị hủy
    const STATUS_COMPLETE = 4; // Hoàn tất
}
