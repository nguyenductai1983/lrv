<?php
namespace App\Enum;
// quan ly phieu thu
abstract class VoucherStatusEnum {
    const TYPE_TRANSPORT = 0;
    const TYPE_MTS = 1;
    const TYPE_EXTRA = 2;
}
