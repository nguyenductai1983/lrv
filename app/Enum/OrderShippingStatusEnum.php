<?php
namespace App\Enum;

abstract class OrderShippingStatusEnum {
    const SHIPPING_STATUS_WAILT = 0; // Chờ kiểm tra
    const SHIPPING_STATUS_NEW = 1; // Mới tạo
    const SHIPPING_STATUS_STOCK_IN = 2; // Đã nhập kho
    const SHIPPING_STATUS_SHIPMENT = 3; // Đã cho vào shipment
    const SHIPPING_STATUS_STOCK_OUT = 4; // Đã xuất kho về VN
    const SHIPPING_IN_THE_CUSTOMS = 5; // Hàng đang tạm giữ tại hải quan
    const SHIPPING_STATUS_REVICER = 6; // Đã nhận hàng ở VN
    const SHIPPING_STATUS_READY_TO_SHIP = 7; // Sẵn sàng xuất cho đơn vị ship
    const SHIPPING_STATUS_DELAY = 8; // Giao sau tết 9
    const SHIPPING_STATUS_CARRIER = 9; // Đã giao cho hãng vận chuyển
    const SHIPPING_STATUS_DONE = 10; // Giao hàng thành công
}
