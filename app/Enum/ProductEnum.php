<?php
namespace App\Enum;

abstract class ProductEnum {
    const DANGER_BATTERY = 1; // Hàng co pin
    const DANGER_PERFUME = 2; // Hàng nước hoa perfume
    const DANGER_BATTERY_PERFUME = 3; // Hàng nước hoa perfume
}
