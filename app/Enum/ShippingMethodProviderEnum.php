<?php
namespace App\Enum;

abstract class ShippingMethodProviderEnum {
    const ABS = 6; // Người của Igreen vận chuyển
    const DROPOFF = 5; // Vận chuyển bằng dropoff
    const VNPOST_FAST = 4; // Vận chuyển bằng vnpost nhanh
    const VNPOST_SLOW = 3; // Vận chuyển bằng vnpost chậm
    const GHTK = 7; // Vận chuyển bằng giao hang tiet kiem
    const IS_ACTIVE_TRUE = 1;

    const SHIPPING_PROVIDER_VIETTEL = 1; // VIETTEL
    const SHIPPING_PROVIDER_VNPOST = 2; // VNPOST
    const SHIPPING_PROVIDER_GMN = 3; // GMN
    const SHIPPING_PROVIDER_GHTK = 4; // GHTK
}
