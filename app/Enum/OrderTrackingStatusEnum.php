<?php
namespace App\Enum;

abstract class OrderTrackingStatusEnum {
    // final class OrderTrackingStatusEnum {
    const STATUS_NEW = 1; // Mới tạo
    const STATUS_STOCK_IN = 2; // Đã nhập kho
    const STATUS_SHIPMENT = 3; // Đã cho vào shipment
    const STATUS_STOCK_OUT = 4; // Đã xuất kho về VN
    const STATUS_CUSTOMS = 5; // Hàng đang tạm giữ tại hải quan new
    const STATUS_REVICER = 6; // Đã nhận hàng ở VN 5
    const STATUS_READY_TO_SHIP = 7; // Sẵn sàng xuất 6
    const STATUS_DELAY = 8; // Giao sau tết 9 new
    const STATUS_CARRIER = 9; // Đã giao cho hãng vận chuyển 7
    const STATUS_DONE = 10; // Giao hàng thành công 8
}
