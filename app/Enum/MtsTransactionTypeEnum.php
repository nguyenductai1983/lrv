<?php
namespace App\Enum;

abstract class MtsTransactionTypeEnum {
    const TYPE_HOME = 1;
    const TYPE_BANK = 2;
    const TYPE_ACCOUNT = 3;
}
