<?php
namespace App\Enum;

abstract class WarehouseEnum {
    const TYPE_FOREIGN = 1; // Kho nước ngoài
    const TYPE_LOCAL = 2; // Kho nội địa việt nam
    
    const IS_DEFAULT_TRUE = 1; // Là kho mặc định
    const IS_DEFAULT_FALSE = 2; // Kho phải kho mặc định
}
