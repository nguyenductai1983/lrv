<?php
namespace App\Enum;

abstract class CurrencyEnum {
    const CAD = 1; // cad
    const USD = 2; // usd
    const VND = 3; // vnd
}
