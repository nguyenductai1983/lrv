<?php
namespace App\Enum;

abstract class WarehousePackageEnum {
    const STATUS_NORMAL = 1; // Gói bình thường
    const STATUS_CANCEL = 2; // Gói bị hủy
}
