<?php
namespace App\Enum;

abstract class TransactionTypeEnum {
    const RECEIPT = 1; // Thu tiền
    const SLIP = 2; // Chi tiền
}
