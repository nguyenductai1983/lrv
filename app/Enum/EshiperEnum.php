<?php
namespace App\Enum;

abstract class EshiperEnum {
    const READY_FOR_SHIPPING = 1;
    const IN_TRANSIT = 2;
    const DELIVERED = 3;
    const CANCELLED = 4;
    const EXCEPTION = 5;
    const CLOSED = 7;
    const READY_FOR_CHECKOUT_DHLEC = 9;
    const READY_TO_PROCESS_DHLEC = 10;

    const PAYMENT_ALL = -1;
    const PAYMENT_UNPAID = 0;
    const PAYMENT_PAID = 1;
    const PAYMENT_PARTIAL_PAID = 2;
    const PAYMENT_RELEASED = 3;
    const PAYMENT_CANCELLED = 4;

    const CARRIER_CANADA_POST = 5;
    const CARRIER_CANADA_WORLDWIDE_INC =  3;
    const CARRIER_DHL =  4;
    const CARRIER_ESHIPPER_TRUCKING =  15;
    const CARRIER_FEDERAL_EXPRESS = 1 ;
    const CARRIER_PUROLATOR = 2 ;
    const CARRIER_TST = 11 ;
    const CARRIER_UPS = 6 ;
    const CARRIER_E_SHIPPER_TRUCKING = 18 ;
    const CARRIER_DHL_ECOMMERCE = 35 ;

    const INSURANCE_TYPE_ESHIPER = 'Carrier';
    const INSURANCE_TYPE_CARRIER = 'Carrier';
    const PACKAGE_INSURANCE_COVERAGE = 'CA,US';
    const QUOTE_REQUEST = 'QuoteRequest';
    const SHIPPING_REQUEST = 'ShippingRequest';
    const QUOTE_TYPE = 'QUOTE';
    const SHIPPING_TYPE = 'SHIPPING';
    const CANCEL_TYPE = 'CANCEL';
    const INFO_TYPE = 'INFO';
    const DIM_RATE = 2.5400;
    const WEIGHT_RATE = 2.204;
    const PACKAGE_TYPE = [
        1 => 'Envelope',
        2 => 'Pak',
        3 => 'Package',
        4 => 'Pallet',
        5 => 'SmarteHome'
    ];
    const COD_PAYMENT_TYPE = [
        1 => 'Check',
        2 => 'Certified Check',
    ];

    const IS_PICKUP = "2";
    const IS_NOT_PICKUP = "1";
}
