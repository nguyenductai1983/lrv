<?php
namespace App\Enum;

abstract class PaymentMethodEnum {
    const STATUS_ACTIVE = 1; 
    const STATUS_INACTIVE = 2;
    
    const TYPE_GLOBAL = 1;
    const TYPE_LOCAL = 2;
}
