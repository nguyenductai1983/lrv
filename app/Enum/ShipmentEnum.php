<?php
namespace App\Enum;

abstract class ShipmentEnum {
    const STATUS_NORMAL = 1; // Shipment bình thường
    const STATUS_CANCEL = 2; // Shipment bị hủy
    
    const SHIPPING_STATUS_NORMAL = 1; // Chưa xuất hết
    const SHIPPING_STATUS_STOCKOUT = 2; // Đã xuất hết về VN
    const SHIPPING_STATUS_RECIVER = 3; // Đã nhận hàng hết về VN
}
