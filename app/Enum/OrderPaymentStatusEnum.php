<?php
namespace App\Enum;

abstract class OrderPaymentStatusEnum {
    const NOT_PAID = 1; // Chưa thanh toán
    const PART_PAID = 2; // Thanh toán 1 phần
    const FULL_PAID = 3; // Thanh toán toàn bộ
}
