<?php
namespace App\Enum;

abstract class OrderStatusEnum {
    const STATUS_NEW = 1; // Mới tạo
    const STATUS_PROCESSING = 2; // Đơn hàng đang xử lý
    const STATUS_CANCEL = 0; // Đơn hàng bị hủy
    const STATUS_COMPLETE = 4; // Hoàn tất đơn hàng
}
