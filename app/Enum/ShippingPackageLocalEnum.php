<?php
namespace App\Enum;

abstract class ShippingPackageLocalEnum {
    const CANCEL = 0; // Hủy
    const WAITING_APPROVE = 1; // Chờ duyệt
    const APPROVED = 2; // Duyệt
    const DELIVERY_CARRIER = 3; // Giao cho HVC
    const CARRIER_PICKUP_FAIL = 4; // HVC pickup thất bại
    const CARRIER_DELIVERY_SUCCESS = 5; // HVC giao thành công
    const CARRIER_DELIVERY_FAIL = 6; // HVC giao thất bại
    const CARRIER_OTHER = 100; // HVC giao thất bại
    const CHANNEL_OFFLINE = 1;
    const CHANNEL_ONLINE = 2;
}
