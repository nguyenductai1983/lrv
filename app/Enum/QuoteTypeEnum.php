<?php
namespace App\Enum;

abstract class QuoteTypeEnum {
    const SHOP_FOR_ME = 1; // Mua hàng hộ
    const SHIP_FOR_ME = 2; // Tự mua hàng
}
