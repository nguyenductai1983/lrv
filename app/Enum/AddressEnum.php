<?php
/**
 * Created by IntelliJ IDEA.
 * User: pc
 * Date: 22/04/2018
 * Time: 01:27 PM
 */

namespace App\Enum;


abstract class AddressEnum
{
    const SENDER = 1; // Địa chỉ gửi
    const RECEIVER = 2; // Địa chỉ nhận
}
