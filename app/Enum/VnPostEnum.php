<?php
namespace App\Enum;

abstract class VnPostEnum {
    const TYPE_GOODS_WH = "1"; // Hàng kho
    const TYPE_GOODS_DROPOFF = "2"; // Hàng drop off

    const TYPE_SEVICE_EMS = "1"; // Chuyển nhanh
    const TYPE_SEVICE_NORMAL = "2"; // Chuyển thường

    const TYPE_PICKUP_WH = "1"; // Lấy tại kho
    const TYPE_PICKUP_UP = "2"; // Hàng picking-up
    const TYPE_PICKUP_DROPOFF = "3"; // Hàng drop off

    const SHIPPING_STATUS_DELETE = 10; // buu điện đã xóa
    const SHIPPING_STATUS_PICKUP = 20; // buu điện đã nhận tin
    const SHIPPING_STATUS_CANCEL = 60; // VNPT hủy
    const SHIPPING_STATUS_CANCEL_MESSENGER = 61; // vnpt báo hủy
    const SHIPPING_STATUS_CANCEL_ACCEPT = 62; //nhận báo hủy
    const SHIPPING_STATUS_APPROVED = 70; // bưu điện đã nhận đơn hàng
    const SHIPPING_STATUS_FAIL = 91; // bưu điện đã nhận đơn hàng
    const SHIPPING_STATUS_DELIVERY = 100; // phát thành công
    const SHIPPING_STATUS_PAYPOST = 110; // buu ta đã thu tiền người nhận
    const SHIPPING_STATUS_COD = 120; // VNPT đã trả tiền cho người gửi
    const SHIPPING_STATUS_RETURN_FAIL = 161; //Returns
    const SHIPPING_STATUS_RETURN = 170; //Returns




    const SHIPPING_STATUS_DELIVERY_AGAIN = 500;

    const SERVICE_NAME_BK ='BK';  // - BK: chuyển phát thường
    const SERVICE_NAME_EMS ='EMS'; // - EMS: chuyển phát nhanh
    const SERVICE_NAME_ECOD ='ECOD'; // - ECOD: chuyển phát tiết kiệm (chỉ dành cho các đơn hàng liên tỉnh có áp dụng COD)
    const SERVICE_NAME_DONG_GIA ='DONG_GIA'; // - DONG_GIA: dịch vụ thỏa thuận riêng giữa khách hàng và VNPost (cần liên hệ bưu cục phục vụ của khách hàng nếu chưa rõ)
}
