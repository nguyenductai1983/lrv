<?php
namespace App\Enum;

abstract class DangerTypeEnum {
    const battery = 1; // Loại pin
    const perfuner = 2; // Nước hoa
    const tonic = 4; // Thuốc bổ
}
