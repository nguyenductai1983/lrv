<?php

namespace App\Enum;

abstract class ClaimEnum {

    const NORMAL = 1; // Mới tạo
    const PROCESSING = 2; // Đang xử lý
    const CANCEL = 3; // Hủy
    const COMPLETE = 4; // Hoàn thành

}
