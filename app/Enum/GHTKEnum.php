<?php
namespace App\Enum;

abstract class GHTKEnum {
    const TYPE_GOODS_WH = "1"; // Hàng kho
    const TYPE_GOODS_DROPOFF = "2"; // Hàng drop off

    const TYPE_SEVICE_EMS = "1"; // Chuyển nhanh
    const TYPE_SEVICE_NORMAL = "2"; // Chuyển thường

    const TYPE_PICKUP_WH = "1"; // Lấy tại kho
    const TYPE_PICKUP_UP = "2"; // Hàng picking-up
    const TYPE_PICKUP_DROPOFF = "3"; // Hàng drop off

    const STATUS_CANCEL = -1; //-1	Hủy đơn hàng
    const STATUS_NOT_APPROVED= 1; // 1	Chưa tiếp nhận
    const STATUS_APPROVED= 2 ;// 2	Đã tiếp nhận
    const STATUS_WAREHOURE= 3 ;// 3	Đã lấy hàng/Đã nhập kho
    const STATUS_PROCESS= 4 ;// 4	Đã điều phối giao hàng/Đang giao hàng
    const STATUS_DELIVERY_NOT_LIABILITIE=  5;// 5	Đã giao hàng/Chưa đối soát
    const STATUS_DELIVERY= 6;// 6	Đã đối soát
    const STATUS_NOT_GET_GOODS = 7;// 7	Không lấy được hàng
    const STATUS_DELETION_GOODS= 8;// 8	Hoãn lấy hàng
    const STATUS_NOT_DELIVERY= 9;// 9	Không giao được hàng
    const STATUS_DELAY= 10;// 10	Delay giao hàng
    const STATUS_LIABILITIE= 11;// 11	Đã đối soát công nợ trả hàng
    const STATUS_GET_GOODS = 12;// 12	Đã điều phối lấy hàng/Đang lấy hàng
    const STATUS_REIMBURSEMENT_ORDERS = 13;// 13	Đơn hàng bồi hoàn
    const SHIPPING_STATUS_RETURN = 20;// 20	Đang trả hàng (COD cầm hàng đi trả)
    const SHIPPING_STATUS_RETURN_COMPLETE = 21;// 21	Đã trả hàng (COD đã trả xong hàng)
    const STATUS_SHIPPER_GET_GOODS = 123;// 123	Shipper báo đã lấy hàng
    const STATUS_SHIPPER_NOT_GET_GOODS = 127;// 127	Shipper (nhân viên lấy/giao hàng) báo không lấy được hàng
    const STATUS_SHIPPER_DELAY = 128; // 128	Shipper báo delay lấy hàng
    const STATUS_SHIPPER_DELIVERY = 45; // 45	Shipper báo đã giao hàng
    const STATUS_SHIPPER_NOT_DELIVERY = 49;// 49	Shipper báo không giao được giao hàng
    const STATUS_SHIPPER_DELAY_DELIVERY = 410;// 410	Shipper báo delay giao hàng
}
