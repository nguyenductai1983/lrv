<?php
namespace App\Enum;

abstract class MtsStatusEnum {
    const NORMAL = 1;
    const PROCESSING = 2;
    const CANCEL = 3;
    const COMPLETE = 4;
}
