<?php
namespace App\Enum;

abstract class OrderLogStatusEnum {
    const STATUS_CANCEL = 0; // huy don hang
    const STATUS_NEW = 1; // Mới tạo
    const STATUS_STOCK_IN = 2; // Đã nhập kho
    const STATUS_SHIPMENT = 3; // Đã cho vào shipment
    const STATUS_STOCK_OUT = 4; // Đã xuất kho về VN
    const STATUS_IN_THE_CUSTOMS = 5; // Hàng đang tạm giữ tại hải quan 7 -> 5
    const STATUS_REVICER = 6; // Đã nhận hàng ở VN
    const STATUS_READY_TO_SHIP = 7; // Sẵn sàng xuất
    const STATUS_OTHER = 8; // Giao hàng thành công
    const STATUS_CARRIER = 9; // Đã giao cho hãng vận chuyển
    const STATUS_DONE = 10; // Giao hàng thành công
    const STATUS_RETURN = 11; // trả hoàn thành công
}
