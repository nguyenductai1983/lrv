<?php
namespace App\Enum;
// quan ly phieu thu
abstract class ExtraRefundStatusEnum {
    const TYPE_EXTRA = 1;
    const TYPE_REFUND = 2;

    const STATUS_CANCEL = 0;
    const STATUS_NEW = 1;
    const STATUS_APPROVE = 2; //approve
    const STATUS_COMPLETE = 4;

}
