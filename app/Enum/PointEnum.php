<?php

namespace App\Enum;

abstract class PointEnum {

    const IS_ACTIVE = 1;
    const NOT_ACTIVE = 0;
    const TYPE_ADD_POINT = 0;
    const TYPE_SUB_POINT = 1;
    
    const TYPE_CONFIG_CUSTOMER = 1;
    const TYPE_CONFIG_ORDER = 2;
}
