<?php
function redirect_success($url, $message)
{
    return redirect()->action($url)->withSuccess($message);
}

function redirect_errors($message)
{
    return redirect()->back()->withErrors($message);
}

function getPathFile($url)
{
    if (strpos($url, '/../')) {
        $arrayPath = explode('/../', $url);
        return str_replace('/', '\\', $arrayPath[1]);
    }else{
        $arrayPath = explode('/', $url);
        $arrayNeed = array_slice($arrayPath, -2,2);
        return (implode('/', $arrayNeed));
    }

}