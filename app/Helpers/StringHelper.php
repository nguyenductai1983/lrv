<?php

function convert_vi_to_en($str, $flag = false) {
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));

    return ($flag) ? str_slug($str, '-') : $str;
}

function unicode_str_filter($str) {
    $unicode = array(
        'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|@',
        'd' => 'đ',
        'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        'i' => 'í|ì|ỉ|ĩ|ị',
        'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
        'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'D' => 'Đ',
        'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
        'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
    );
    foreach ($unicode as $nonUnicode => $uni) {
        $str = preg_replace("/($uni)/i", $nonUnicode, $str);
    }
    return $str;
}

function urlNormalization($str, $id = null) {
    $string = unicode_str_filter($str);
    $re = "/\W+/";
    $re2 = "/^\W+|\W+$/";
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
    $newString = preg_replace($re, "-", $string);
    $done = !empty($id) ? preg_replace($re2, "", $newString) . '-' . $id : preg_replace($re2, "", $newString);
    return strtolower($done);
}

function getNextNumber($number, $new_number = null) {
    if (!isset($new_number)) {
        $new_number = $number + 1;
        $width = strlen($number);
    } else {
        $widthNew = strlen($new_number);
        $widthOld = strlen($number);
        if ($widthNew > $widthOld) {
            $width = $widthNew;
        } else {
            $width = $widthOld;
        }
    }
    if ($width < 1) {
        $width = 2;
    }
    $padded = str_pad((string) $new_number, $width, "0", STR_PAD_LEFT);
    return $padded;
}

function getSpecialString($string, $symbol = '[]|{}|()|" "') {
    switch ($symbol) {
        case '[]':
            $regex = "/\[([^\]]*)\]/";
            break;
        case '{}':
            $regex = '/{(.*?)}/';
            break;
        case '" "':
            $regex = '/"([^"]+)"/';
            break;
        case '()':
            $regex = '#\((([^()]+|(?R))*)\)#';
            break;
        case '\' \'':
            $regex = '/".*?"|\'.*?\'/';
            break;
        default:
            return $string;
            break;
    }
    if (preg_match_all($regex, $string, $matches)) {
        return $matches;
    } else {
        //no parenthesis
        return false;
    }
}

function getStringBeweenSpec($needle, $symbol = '[]|{}|()|" "') {
    $specical_character = explode('|', $symbol);
    $string = [];
    foreach ($specical_character as $key => $value) {
        $string[$value]['after'] = getSpecialString($needle, $value)[1];
        $string[$value]['before'] = getSpecialString($needle, $value)[0];
        if (isset($string[$value]['before']) && !empty($string[$value]['before']))
            foreach ($string[$value]['before'] as $delete) {
                $needle = str_replace($delete, '', $needle);
            }
    }
    $string['main'] = trim($needle);
    return $string;
}

function stringToDate($string) {
    $time = strtotime($string);

    $newformat = date('Y-m-d', $time);
    return $newformat;
}
