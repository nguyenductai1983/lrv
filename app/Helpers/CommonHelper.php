<?php

function getURLPictureForGal($url)
{
    $urlAfterGet = NULL;
    if (strpos($url, '|') && preg_match('/^(0|1){1}.{10,}$/', $url)) {
        $picture_get = explode('|', $url);
        if (in_array(S_INTERNAL, $picture_get)) {
            $urlAfterGet = asset(end($picture_get));
        } else {
            $urlAfterGet = end($picture_get);
        }
        return $urlAfterGet;
    } else {
        return $url;
    }
}

function deleteFolderWithFiles($dir){
    $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new RecursiveIteratorIterator($it,
        RecursiveIteratorIterator::CHILD_FIRST);
    foreach($files as $file) {
        if ($file->isDir()){
            rmdir($file->getRealPath());
        } else {
            unlink($file->getRealPath());
        }
    }
    rmdir($dir);
}

function fixErrorEncode($string){
    $text = utf8_decode($string);
    if(mb_check_encoding($text) == 'ISO-8859-1'){
        return $text;
    }else{
        return $string;
    }
}

function getMimeType($filename, $checkImage = false)
{
    $mimetype = false;
    if(!file_exists ($filename)){
        return false;
    }
    $imagetype = array('image/gif', 'image/jpeg', 'image/png', 'image/bmp');
    if (function_exists('finfo_fopen')) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        if ($checkImage) {
            if (in_array($type, $imagetype))
                return true;
            else
                return false;
        }
        return $type;
        // open with FileInfo
    } elseif (function_exists('getimagesize') && $checkImage) {
        $type = getimagesize($filename);
        if(in_array($type['mime'],$imagetype)){
            return true;
        }else {
            return false;
        }
        // open with GD
    } elseif (function_exists('exif_imagetype') && $checkImage) {
        $type = exif_imagetype($filename);
        if($type){
            switch($type){
                case 1:
                    return 'image/gif';
                case 2:
                    return 'image/jpeg';
                case 3:
                    return 'image/png';
                case 6:
                    return 'image/bmp';
                default:
                    return false;
            }
        }
        return $type;
        // open with EXIF
    } elseif (function_exists('mime_content_type')) {
        $mimetype = mime_content_type($filename);
        if ($checkImage) {
            if (in_array($mimetype, $imagetype))
                return true;
            else
                return false;
        }
    }
    return $mimetype;
}

function openZip($file_to_open,$target,$delete = false) {
    $zip = new ZipArchive();
    $x = $zip->open($file_to_open);
    if($x === true) {
        $zip->extractTo($target);
        $zip->close();
        if($delete)
        unlink($file_to_open);
    } else {
        return false;
    }
    return $target;
}

function quickSort($array = [], $left=0, $right=0)
{
    $i = $left;
    $j = $right;
    $pivot = $array[($left + $right) / 2];
    do {
        while ($array[$i] < $pivot && $i < $right) {
            $i++;
        }
        while ($array[$j] > $pivot && $j > $left) {
            $j--;
        }
        if ($i <= $j) {
            $array = swap($array, $i, $j);
            $i++;
            $j--;
        }
    } while ($i <= $j);
    if ($left < $j) quickSort($array, $left, $j);
    if ($i < $right) quickSort($array, $i, $right);
    return $array;
//    var_dump($array);
}

function swap($array, $i, $j)
{
    $temp = $array[$i];
    $array[$i] = $array[$j];
    $array[$j] = $temp;
    return $array;
//    die;
}

function setURLPictureForGal($url, $mode)
{
    return $mode . '|' . $url;
}


function showMenuMultileLevel($aCats, $parentid = 0)
{
    $menu_tmp = array();
    foreach ($aCats as $k => $item) {
        if ($item['parent_id'] == $parentid) {
            $menu_tmp[] = $item;
            unset($aCats[$k]);
        }
    }
    if ($menu_tmp) {
        if ($parentid == 0) {
            echo '<ol class="tree">';
        } else {
            echo '<ol>';
        }
        foreach ($menu_tmp as $li) {
            echo '<li>';
            echo '<label for="' . $li['cat_id'] . '">' . $li['cat_name'] . "&nbsp;&nbsp;&nbsp;" . create_field_action('admin/category', $li['cat_id']) . '</label>';
            echo '<input type="checkbox" checked id="' . $li['cat_id'] . '" />';
            showMenuMultileLevel($aCats, $li['cat_id']);
            echo '</li>';
        }
        echo '</ol>';
    }
}

function get_data($data, $field)
{
    $field_array = explode(',', $field);
    $temp = array();
    foreach ($field_array as $k => $v) {
        foreach ($data as $k2 => $v2) {
            if ($k2 == trim(strtolower($v))) {
                $temp[$k2] = $v2;
            }
        }
    }
    return $temp;
}


function autoAssignDataToProperty($model, $data)
{
    $properties = $model->properties;
    foreach ($properties as $property) {
        foreach ($data as $key => $value) {
            if ($key == $property) {
                $model->$property = $data[$key];
            } else {

            }
        }
    }
    //return $model;
}

function autoAssignDataToBuilder($model, $data, $option = false)
{
    $result = [];
    if (!$option)
        $properties = array_keys(get_object_vars($model));
    else
        $properties = $model;
    foreach ($properties as $property) {
        foreach ($data as $key => $value) {
            if ($key == $property) {
                $result[$property] = $data[$key];
            }
        }
    }
    return $result;
    //return $model;
}

function  getConfigPagination($dataRequest, $sortColum)
{
    // FOR PAGINATION CONFIG
    $pageCurrent = 1;
    $dtaPerPage = 15;
    if (isset($dataRequest['current'])) {
        $pageCurrent = $dataRequest['current'];
    }
    if (isset($dataRequest['rowCount'])) {
        $dtaPerPage = $dataRequest['rowCount'];
    }
    $start = ($pageCurrent - 1) * $dtaPerPage;
    // FOR SEARCH CONFIG
    $search = '';
    if (isset($dataRequest['searchPhrase'])) {
        $search = $dataRequest['searchPhrase'];
    }
    // FOR SORT CONFIG
    $sortBy = $sortColum[0];
    $sortOrder = "ASC";
    if (isset($dataRequest['sort'])) {
        $sort = $dataRequest['sort'];
        $sortBy = (in_array(key($sort), $sortColum)) ? key($sort) : $sortBy;
        $sortOrder = current($sort);
    }
    return array(
        'start' => $start,
        'limit' => $dtaPerPage,
        'current' => $pageCurrent,
        'sortBy' => $sortBy,
        'sortOrder' => $sortOrder,
        'search' => $search
    );
}

function create_field_action($controller, $id, $other_id = null)
{
    if (!empty($other_id)) {
        return '<a href="' . asset($controller . '/' . $id . '/edit/' . $other_id) . '"><i class="glyphicon glyphicon-edit"></i></a>
            <a href="' . asset($controller . '/' . $id . '/del/' . $other_id) . '"><i class="glyphicon glyphicon-remove"></i></a>';
    } else
        return '<a href="' . asset($controller . '/' . $id . '/edit') . '"><i class="glyphicon glyphicon-edit"></i></a>
            <a href="' . asset($controller . '/' . $id . '/del') . '"><i class="glyphicon glyphicon-remove"></i></a>';
}

function create_field_action_other($controller, $id)
{
    return '<a href="' . asset($controller . '/' . $id . '/edit/plus') . '"><i class="glyphicon glyphicon-plus"></i></a>';
}

function del_action($controller, $id)
{
    return '<a href="' . asset($controller . '/' . $id . '/del') . '"><i class="glyphicon glyphicon-remove"></i></a>';
}

function create_field_image($src, $attr = '')
{
    return '<img src="' . asset($src) . '" ' . $attr . '>';
}

function create_field_status($status)
{
    $str = '<span class="label label-sm label-success">Approved</span>';
    if ($status == 0) {
        $str = '<span class="label label-sm label-warning">Pending</span>';
    }
    return $str;
}

function price_formate($price)
{
    return number_format($price, 0, ",", ".");
}

function isValidImage($imageExtension)
{
    $allowed = array(
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/x-png',
        'image/gif',
        '.jpg',
        '.jpeg',
        '.gif',
        '.png',
    );
    return in_array($imageExtension, $allowed);
}

/**
 * dump variable and exit
 * @param $var
 */
function adump($var)
{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
    exit;
}

function get_client_ip_server()
{
    $ipaddress = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (!empty($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (!empty($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (!empty($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (!empty($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}


function RecursiveWrite($array, $html = null)
{
    if (is_array($array)) {
        foreach ($array as $vals) {
            $html .= '<a href="javascript:" li-id="' . $vals->sort_comment . '-' . $vals->link_id . '-' . $vals->nav_id . '"
                                                   class="expand-a"><i
                                                            class="expand-link">' . !empty($vals->link_title) ? $vals->link_title : $vals->sort_comment . '</i></a>';
            $html .= '<div id="' . $vals->sort_comment . '-' . $vals->link_id . '-' . $vals->nav_id . '">' . RecursiveWrite($vals->links, $html) . '</div>';
        }
    }
    return $html;
}

function addParamToURL($url, $param = array())
{
    $parmString = http_build_query($param);
    $url = $url . "?" . $parmString;
    return $url;
}

if (!function_exists('cached_asset')) {
    function cached_asset($path, $bustQuery = false)
    {
        // Get the full path to the asset.
        $realPath = public_path($path);

        if (!file_exists($realPath)) {
            throw new LogicException("File not found at [{$realPath}]");
        }

        // Get the last updated timestamp of the file.
        $timestamp = filemtime($realPath);

        if (!$bustQuery) {
            // Get the extension of the file.
            $extension = pathinfo($realPath, PATHINFO_EXTENSION);

            // Strip the extension off of the path.
            $stripped = substr($path, 0, -(strlen($extension) + 1));

            // Put the timestamp between the filename and the extension.
            $path = implode('.', array($stripped, $timestamp, $extension));
        } else {
            // Append the timestamp to the path as a query string.
            $path .= '?' . $timestamp;
        }

        return asset($path);
    }
}

class Sanitiser
{
    public static function trimInput($input)
    {
        if (is_string($input))
            return trim($input);
        else
            return $input;
    }

    public static function generateHtml($needlesArr, $haystackBlob, $inputName)
    {
        $haystack = [];
        if (is_object($haystackBlob)) {
            foreach ($haystackBlob as $value) {
                $haystack[] = $value->id;
            }
        } else {
            $haystack = (array)$haystackBlob;
        }

        $html = '';

        foreach ($needlesArr as $key => $needle) {
            if (in_array($key, $haystack)) {
                $html .= "<div class='checkbox'>
                                <label><input type='checkbox' name='" . $inputName . "' value='" . $key . "' checked='checked'>"
                    . $needle .
                    "</label>
                            </div>";
            } else {
                $html .= "<div class='checkbox'>
                                <label><input type='checkbox' name='" . $inputName . "' value='" . $key . "'>"
                    . $needle .
                    "</label>
                            </div>";
            }
        }

        return $html;
    }
}

?>
