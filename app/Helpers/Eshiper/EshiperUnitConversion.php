<?php
/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/10/2018
 * Time: 10:59 AM
 */
namespace App\Helpers\Eshiper;
class EshiperUnitConversion
{
    public static function cmToInch ($cm)
    {
        $inch = (float)$cm * 0.3937;
        return ceil($inch);
    }
    
    public static function kgToLib ($kg)
    {
        $lib = (float)$kg / 0.45359;
        return ceil($lib);
    }
}