<?php
namespace App\Helpers\Eshiper;
use App\Enum\EshiperEnum;

/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/10/2018
 * Time: 10:42 AM
 */

class EshiperHelper
{
    var $customs_coverage;
    
    public function readCustomsCoverage(){
        $this->customs_coverage = array();
        $countries_coverage = explode(',', EshiperEnum::PACKAGE_INSURANCE_COVERAGE);
        
        foreach($countries_coverage as $key => $coverage){
            $country_coverage = explode(':', $coverage);
            $this->customs_coverage[$country_coverage[0]] = $country_coverage[1];
        }
    }
    
    public function getMaxCoverage($country, $current_value){
        $coverage = $current_value;
        if (array_key_exists($country, $this->customs_coverage) && $current_value > $this->customs_coverage[$country]){
            $coverage = $this->customs_coverage[$country];
        }
        return $coverage;
    }
    
    public function getNextBusinessDay() {
        $next_business_day = date('Y-m-d');
        $day_of_week = date('N');
        if($day_of_week == 5) {
            $next_business_day = date('Y-m-d',strtotime('+0 day'));
        } elseif($day_of_week == 6) {
            $next_business_day = date('Y-m-d',strtotime('+2 day'));
        } elseif($day_of_week == 7) {
            $next_business_day = date('Y-m-d',strtotime('+1 day'));
        }
        return $next_business_day;
    }
}