<?php

/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/10/2018
 * Time: 10:54 AM
 */
namespace App\Helpers\Eshiper;
use App\Enum\EshiperEnum;
use App\Components\TextUtil;

class EshiperRequestBuilder {

    public $header,
            $request_tag_name,
            $request_type,
            $address_from,
            $address_to,
            $packages,
            $customs_coverage,
            $country_dest,
            $eshiper_helper,
            $footer,
            $pickup = '',
            $cod = '';

    function __construct() {
        $this->eshiper_helper = new EshiperHelper();
        $this->setHeader();
    }

    function setHeader() {
        $this->header = "<?xml version=\"1.0\"?>\n" .
                "<EShipper xmlns=\"http://www.eshipper.net/XMLSchema\" username=\"" . env('ESHIPER_USERNAME','igreen_eshipper') . "\" password=\"" . env('ESHIPER_PASSWORD','eshipper2010') . "\" version=\"3.0.0\">\n";
    }

    function getHeader() {
        return $this->header;
    }

    function setRequestType($request_tag_name, $attrs) {
        $this->request_tag_name = $request_tag_name;
        $this->request_type = '<' . $this->request_tag_name;
        foreach ($attrs as $attr_name => $attr_value) {
            $this->request_type .= " $attr_name=\"$attr_value\"";
        }
        $this->request_type .= ">\n";
    }

    function getRequestType() {
        return $this->request_type;
    }

    function setAddressFrom($address, $isFull = false) {
        $this->address_from = "<From " . $this->addAddress($address, $isFull) . "/>\n";
    }

    function getAddressFrom() {
        return $this->address_from;
    }

    function setAddressTo($address, $isFull = false) {
        $this->address_to = "<To " . $this->addAddress($address, $isFull) . "/>\n";
        $this->country_dest = $address['country'];
    }

    function getAddressTo() {
        return $this->address_to;
    }

    function setPackages($packages) {
        $dimRate = 1;
        $weightRate = 1;
        $boxesToShip = !empty($packages['boxes']) ? $packages['boxes'] : [];
        $package_type_name = EshiperEnum::PACKAGE_TYPE[$packages['type']];
        $this->packages = "<Packages type=\"$package_type_name\">\n";
        $num_of_boxes = count($boxesToShip);
        if ($packages['dimType'] == 1) {
            $dimRate = EshiperEnum::DIM_RATE;
            $weightRate = EshiperEnum::WEIGHT_RATE;
        }
        for ($i = 0; $i < $num_of_boxes; $i++) {
            $this->packages .= " <Package" .
                    " length=\"" . $boxesToShip[$i]['length'] / $dimRate . "\"" .
                    " width=\"" . $boxesToShip[$i]['width'] / $dimRate . "\"" .
                    " height=\"" . $boxesToShip[$i]['height'] / $dimRate . "\"" .
                    " weight=\"" . $boxesToShip[$i]['weight'] * $weightRate . "\"";
            if(isset($boxesToShip[$i]['description']) && !empty($boxesToShip[$i]['description'])){
                $this->packages .= " description=\"" . $boxesToShip[$i]['description'] . "\"";
            }else{
                $boxesToShip[$i]['description'] = 'iGreenLink';
                $this->packages .= " description=\"" . $boxesToShip[$i]['description'] . "\"";
            }
            if ($packages['type'] == 4) {
                $this->packages .= " freightClass=\"" . $boxesToShip[$i]['freightClass'] . "\"";
                $this->packages .= !empty($boxesToShip[$i]['nmfcCode']) ? " nmfcCode = \"" . $boxesToShip[$i]['nmfcCode'] . "\"" : '';
            }
            $this->packages .= " type=\"" . $package_type_name . "\"" .
                    " insuranceAmount=\"" . $boxesToShip[$i]['insuranceAmount'] . "\"/>";
        }

        $this->packages .= "</Packages>\n";
    }

    function getPackages() {
        return $this->packages;
    }

    function setFooter() {
       // $rrr = App::environment('ESHIPER_USERNAME');
        $this->footer = "</" . $this->request_tag_name . ">\n" .
                "</EShipper>\n";
    }

    function getFooter() {
        return $this->footer;
    }

    function addAddress($address, $isFull = false, $type = 1) {
        $address_line = "company=\"" . TextUtil::removeDiacritical($address['company']) . "\"" .
                " address1=\"" . TextUtil::removeDiacritical($address['address_1']) . "\"";
        $address_line .= !empty($address['address_2']) ? " address2=\"" . TextUtil::removeDiacritical($address['address_2']) . "\"" : '';

        $address_line .= " city=\"" . TextUtil::removeDiacritical($address['city']) . "\"" .
                " state=\"" . TextUtil::removeDiacritical($address['state']) . "\"" .
                " country=\"" . TextUtil::removeDiacritical($address['country']) . "\"" .
                " zip=\"" . $address['postal_code'] . "\"";
        $address_line .= !empty($address['residential']) && $address['residential'] ? " residential=\"true\"" : '';
        if ($type == 1) {
            $address_line .= !empty($address['confirm_delivery']) && $address['confirm_delivery'] ? " confirmDelivery=\"true\"" : '';
        }
        if ($type == 2) {
            $address_line .= !empty($address['notify_recipient']) && $address['notify_recipient'] ? " notifyRecipient=\"true\"" : '';
        }
        if ($isFull) {
            $address_line .= " phone=\"" . $address['telephone'] . "\"";
            if (!empty($address['attention'])) {
                $address_line .= " attention=\"" . TextUtil::removeDiacritical($address['attention']) . "\"";
            } else {
                $address_line .= " attention=\"" . TextUtil::removeDiacritical($address['company']) . "\"";
            }
            $address_line .= " email=\"" . $address['email'] . "\"";
            $address_line .= !empty($address['instruction']) ? " instructions=\"" . TextUtil::removeDiacritical($address['instruction']) . "\"" : "";
        }
        return $address_line;
    }

    function setCOD($cod) {
        $payment_type = $cod['payment_type'];
        $cod_data = $cod['cod_data'];
        $this->cod = "<COD paymentType=\"$payment_type\">\n";
        $this->cod .= " <CODReturnAddress" .
                " codCompany=\"" . TextUtil::removeDiacritical($cod_data['company']) . "\"" .
                " codName=\"" . TextUtil::removeDiacritical($cod_data['company']) . "\"" .
                " codAddress1=\"" . TextUtil::removeDiacritical($cod_data['address_1']) . "\"" .
                " codAddress2=\"" . TextUtil::removeDiacritical($cod_data['address_2']) . "\"" .
                " codPhone=\"" . $cod_data['telephone'] . "\"" .
                " codStateCode=\"" . TextUtil::removeDiacritical($cod_data['state']) . "\"" .
                " codCity=\"" . TextUtil::removeDiacritical($cod_data['city']) . "\"" .
                " codZip=\"" . $cod_data['postal_code'] . "\"" .
                " codCountry=\"" . TextUtil::removeDiacritical($cod_data['country']) . "\"/>";

        $this->cod .= "</COD>\n";
    }

    function getCOD() {
        return $this->cod;
    }

    function setPickup($pickup_data) {
        $this->pickup = "<Pickup";
        $this->pickup .= " contactName=\"" . TextUtil::removeDiacritical($pickup_data['contact_name']) . "\"" .
                " phoneNumber=\"" . $pickup_data['phone_number'] . "\"" .
                " pickupDate=\"" . $pickup_data['pickup_date'] . "\"" .
                " pickupTime=\"" . $pickup_data['pickup_time'] . "\"" .
                " closingTime=\"" . $pickup_data['closing_time'] . "\"";
        if (!empty($pickup_data['location'])) {
            $this->pickup .= " location=\"" . TextUtil::removeDiacritical($pickup_data['location']) . "\"";
        } else {
            $this->pickup .= " location=\"" . 'Front Door' . "\"";
        }
        $this->pickup .= " />";
    }

    function getPickup() {
        return $this->pickup;
    }

}
