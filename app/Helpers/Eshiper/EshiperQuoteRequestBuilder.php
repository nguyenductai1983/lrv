<?php
/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/10/2018
 * Time: 11:34 AM
 */
namespace App\Helpers\Eshiper;
class EshiperQuoteRequestBuilder extends EshiperRequestBuilder
{
    
    public $request, $header, $insurance, $from, $to, $packages, $footer, $cod , $pickup;
    
    function __construct(){
        parent::__construct();
        $this->pickup = '';
        $this->cod = '';
    }
    
    function buildRequest() {
        return parent::getHeader() . parent::getRequestType() . parent::getAddressFrom() . parent::getAddressTo()
            . parent::getCOD() . parent::getPackages() .  parent::getPickup() . parent::getFooter();
    }
}