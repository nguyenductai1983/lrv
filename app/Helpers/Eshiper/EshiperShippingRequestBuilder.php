<?php
/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/19/2018
 * Time: 10:05 AM
 */

namespace App\Helpers\Eshiper;

use App\Components\TextUtil;


class EshiperShippingRequestBuilder extends EshiperRequestBuilder
{
    var $request, $header, $from, $to, $payment_type, $options, $references, $packages, $customs, $footer,$pickup, $cod;
    
    function __construct(){
        parent::__construct();
        $this->references = array();
        $this->custom = "";
        $this->pickup = "";
        $this->cod = "";
        $this->options = "";
    }
    
    function setPaymentType($paymentType) {
        if(!empty($paymentType)){
            $this->payment_type = "<Payment type=\"$paymentType\"/>\n";
        }else{
            $this->payment_type = "<Payment type=\"3rd Party\"/>\n";
        }
    }
    
    function getPaymentType() {
       return $this->payment_type;
    }
    
    function setOptions($options) {
        $this->options = '';
        foreach($options as $tag => $value) {
            $this->options .= "<" . $tag . ">" . $value . "</" . $tag . ">\n";
        }
    }
    
    function getOptions() {
        return $this->options;
    }
    
    function setReference($name, $code) {
        $this->references[] = "<Reference name=\"$name\" code=\"$code\"/>\n";
    }
    
    // function setCustom($custom, $countries, $provinces, $cities) {
    //     $items = !empty($custom['items']) ? $custom['items']:[];
    //     $brokerName = TextUtil::removeDiacritical($custom['brokerName']);
    //     $taxId = TextUtil::removeDiacritical($custom['taxId']);
    //     $receiptsTaxId = TextUtil::removeDiacritical($custom['receiptsTaxId']);
    //     $company = TextUtil::removeDiacritical($custom['company']);
    //     $name = TextUtil::removeDiacritical($custom['name']);
    //     $phone = TextUtil::removeDiacritical($custom['phone']);
        
    //     $billTo = TextUtil::removeDiacritical($custom['dutiesTaxes']['billTo']);
    //     $dutiable = TextUtil::removeDiacritical($custom['dutiesTaxes']['dutiable']);
    //     $consigneeAccount = TextUtil::removeDiacritical($custom['dutiesTaxes']['consigneeAccount']);
    //     $sedNumber = TextUtil::removeDiacritical($custom['dutiesTaxes']['sedNumber']);
        
    //     $billToCompany = TextUtil::removeDiacritical($custom['billTo']['company']);
    //     $billToName = TextUtil::removeDiacritical($custom['billTo']['name']);
    //     $billToAddress = TextUtil::removeDiacritical($custom['billTo']['address_1']);
        
    //     $billToCountry = '';
    //     if(isset($countries[$custom['billTo']['country_id']])){
    //         $billToCountry = TextUtil::removeDiacritical($countries[$custom['billTo']['country_id']]);
    //     }
    //     $billToCity = '';
    //     if(isset($cities[$custom['billTo']['city_id']])){
    //         $billToCity = TextUtil::removeDiacritical($cities[$custom['billTo']['city_id']]);
    //     }

    //     $billToProvince = '';
    //     if(isset($provinces[$custom['billTo']['province_id']])){
    //         $billToProvince = TextUtil::removeDiacritical($provinces[$custom['billTo']['province_id']]);
    //     }
    //     $billToPostalCode = '';
    //     if(isset($custom['billTo']['postal_code'])){
    //         $billToPostalCode = TextUtil::removeDiacritical($custom['billTo']['postal_code']);
    //     }
        
    //     $this->custom = "<CustomsInvoice brokerName=\"$brokerName\" shipperTaxID=\"$taxId\" receiverTaxID=\"$receiptsTaxId\" contactCompany=\"$company\" contactName=\"$name\" contactPhone=\"$phone\">\n";
    //     $this->custom .= "<BillTo company=\"$billToCompany\" name=\"$billToName\" address1=\"$billToAddress\" city=\"$billToCity\" state=\"$billToProvince\" zip=\"$billToPostalCode\" country=\"$billToCountry\"/>\n";
    //     $this->custom .= "<Contact name=\"$name\" phone=\"$phone\"/>\n";
    //     $num_of_boxes = count($items);
    //     for ($i = 0; $i < $num_of_boxes; $i++) {
    //         $this->custom .=
    //             " <Item" .
    //             " code=\"" . TextUtil::removeDiacritical($items[$i]['code'])  . "\"" .
    //             " description=\""  . TextUtil::removeDiacritical($items[$i]['description'])  . "\"" .
    //             " originCountry=\"" . TextUtil::removeDiacritical($countries[$items[$i]['country_id']]) . "\"" .
    //             " quantity=\"" . $items[$i]['quantity'] . "\"" .
    //             " unitPrice=\"" . $items[$i]['unitPrice'] . "\"" .
    //             " skuCode=\"" . TextUtil::removeDiacritical($items[$i]['code']) . "\"/>";
    //     }
    //     $this->custom .= "<DutiesTaxes billTo=\"$billTo\" dutiable=\"$dutiable\" consigneeAccount=\"$consigneeAccount\" sedNumber=\"$sedNumber\"/>\n";
    //     $this->custom .="</CustomsInvoice>\n";
    // }

    function setCustom($custom, $countries) {
        $items = !empty($custom['items']) ? $custom['items']:[];
        
        $this->custom = '<CustomsInvoice brokerName="" shipperTaxID="" receiverTaxID="" contactCompany="IGREENLINK" contactName="GARY" contactPhone="16478876869">';
        $this->custom .= '<BillTo company="IGREENLINK" name="GARY" address1="10 TIFFANY GATE" city="Richmond Hill Southwest" state="Ontario" zip="L4C6W8" country="CA"/>';
        $this->custom .= '<Contact name="GARY" phone="16478876869"/>';
        $num_of_boxes = count($items);
        for ($i = 0; $i < $num_of_boxes; $i++) {
            $this->custom .=
                " <Item" .
                " code=\"" . TextUtil::removeDiacritical($items[$i]['code'])  . "\"" .
                " description=\""  . TextUtil::removeDiacritical($items[$i]['description'])  . "\"" .
                " originCountry=\"" . TextUtil::removeDiacritical($countries[$items[$i]['country_id']]) . "\"" .
                " quantity=\"" . $items[$i]['quantity'] . "\"" .
                " unitPrice=\"" . $items[$i]['unitPrice'] . "\"" .
                " skuCode=\"" . TextUtil::removeDiacritical($items[$i]['code']) . "\"/>";
        }
        $this->custom .= '<DutiesTaxes billTo="receiver" dutiable="false" consigneeAccount="" sedNumber=""/>';
        $this->custom .="</CustomsInvoice>";
    }
    
    function getCustoms() {
        return $this->customs;
    }
    
    function buildRequest() {
        $request = parent::getHeader() . parent::getRequestType() . parent::getAddressFrom() . parent::getAddressTo();
        if ( $this->cod != "" ) {
            $request .= $this->cod;
        }
        $request .= parent::getPackages();
        if ( $this->pickup != "") {
            $request .= $this->pickup;
        }
        $request .= $this->payment_type;
        if (!empty($this->references)) {
            foreach($this->references as $reference) {
                $request .= $reference;
            }
        }
        if($this->custom != "") {
            $request .= $this->custom;
        }
        return $request.parent::getFooter();
    }
}