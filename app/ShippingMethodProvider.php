<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingMethodProvider extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_shipping_method_provider';
    protected $fillable = [
        'name',
        'shipping_provider_id',
        'shipping_method_id',
        'min_weight',
        'min_fee',
        'display_order',
        'is_active',
        'image_file_id'
    ];
    public function file()
    {
        return $this->belongsTo(File::class, 'image_file_id', 'id');
    }
    public function method() {
        return $this->hasMany(ShippingMethod::class, 'shipping_method_id', 'id');
    }
    public function provider() {
        return $this->hasMany(ShippingProvider::class, 'shipping_provider_id', 'id');
    }

}
