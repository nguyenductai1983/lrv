<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\PointEnum;

class PointConfig extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_point_config';

    protected $fillable = [
        'point',
        'type',
        'is_active'
    ];
    
    public function getTypeNameAttribute() {
        switch ($this->attributes['type']) {
            case PointEnum::TYPE_CONFIG_CUSTOMER:
                return __('point.type_config_customer');
            case PointEnum::TYPE_CONFIG_ORDER:
                return __('point.type_config_order');
            default:
                return __('point.type_config_customer');
        }
    }
    
    public function getIsActiveNameAttribute() {
        switch ($this->attributes['is_active']) {
            case PointEnum::IS_ACTIVE:
                return __('point.is_active');
            default:
                return __('point.no_active');
        }
    }
}
