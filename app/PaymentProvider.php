<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentProvider extends Model
{
    //
    protected $table = 'tbl_payment_provider';
}
