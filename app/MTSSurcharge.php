<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MTSSurcharge extends Model
{
    protected $table = 'mts_surcharges';
    //
    public function Currency()
    {
        return $this->belongsTo(Currency::class, 'currencie_id', 'id');
    }
}
