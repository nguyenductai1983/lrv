<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\PointEnum;

class PointConvert extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_point_convert';

    protected $fillable = [
        'point',
        'amount',
        'is_active'
    ];
    
    public function getIsActiveNameAttribute() {
        switch ($this->attributes['is_active']) {
            case PointEnum::IS_ACTIVE:
                return __('point.is_active');
            default:
                return __('point.no_active');
        }
    }
}
