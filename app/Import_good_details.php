<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import_good_details extends Model
{
    //
    protected $table = 'import_good_details';
    public function Import_goods() {
        return $this->belongsTo(Import_goods::class, 'import_good_id', 'id');
    }
    public function Product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
