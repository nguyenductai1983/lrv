<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerMemberConfig extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_customer_member_config';

    protected $fillable = [
        'customer_id',
        'discount',
        'type'
    ];
}
