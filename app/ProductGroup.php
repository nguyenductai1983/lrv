<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\File;

class ProductGroup extends Model
{
    /**
     * Table to use
     */
    protected $table = 'product_groups';

    /**
     * Attributes are guarded
     */
    protected $guarded = [];

    /**
     * Relationship with product
     */
    public function products() {
        return $this->hasMany(Product::class, 'product_group_id', 'id');
    }
    
    /**
     * Belong to image 2 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thumbnails()
    {
        return $this->belongsTo(File::class, 'thumbnail_id', 'id');
    }
    
    public function getFrontendUrlAttribute() {
        return url("/product/category/{$this->attributes['id']}");
    }
}
