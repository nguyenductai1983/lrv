<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import_goods extends Model
{
    //
    protected $table = 'import_goods';
    public function User() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function Product_suppliers() {
        return $this->belongsTo(Product_suppliers::class, 'product_supplier_id', 'id');
    }
    public function Import_good_details() {
        return $this->hasMany(Import_good_details::class, 'import_good_id', 'id');
    }
}
