<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'code',
        'first_name',
        'middle_name',
        'last_name',
        'username',
        'email',
        'password',
        'address',
        'gender',
        'mobile_phone',
        'home_phone',
        'office_phone',
        'fax',
        'bank_account',
        'bank_code',
        'tax_code',
        'id_card',
        'date_issued',
        'place_issued',
        'birthday',
        'family_allowance',
        'mts_percent',
        'agency_id',
        'role_id',
        'transport_count',
        'remember_token',
        'warehouse_id',
        'country_id',
        'province_id',
        'city_id',
        'postal_code',
        'updated_at',
        'display_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_issued',
        'birthday',
        'deleted_at'
    ];

    /**
     * Get name attribute
     */
    public function getFullNameAttribute()
    {
        $name = $this->first_name;
        if (!empty($this->middle_name)) {
            $name .= ' ' . $this->middle_name;
        }
        if (!empty($this->last_name)) {
            $name .= ' ' . $this->last_name;
        }
        return $name;
    }

    /**
     * Relationship with role
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    /**
     * Relationship with agency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agency_id', 'id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'id');
    }

    /**
     * Set date issued attribute
     *
     * @param $value
     */
    public function setDateIssuedAttribute($value)
    {
        if ($value) {
            $value = Carbon::createFromFormat(config('app.date_format'), $value);
        }
        $this->attributes['date_issued'] = $value;
    }

    /**
     * Get date issued formatted
     *
     * @return mixed
     */
    public function getDateIssuedFormattedAttribute()
    {
        if ($this->date_issued) {
            return $this->date_issued->format(config('app.date_format'));
        }
        return $this->date_issued;
    }

    /**
     * Set birthday attribute
     *
     * @param $value
     */
    public function setBirthdayAttribute($value)
    {
        if ($value) {
            $value = Carbon::createFromFormat(config('app.date_format'), $value);
        }
        $this->attributes['birthday'] = $value;
    }

    /**
     * Get birthday formatted
     *
     * @return mixed
     */
    public function getBirthdayFormattedAttribute()
    {
        if ($this->birthday) {
            return $this->birthday->format(config('app.date_format'));
        }
        return $this->birthday;
    }

    /**
     * Has many transport
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transports()
    {
        return $this->hasMany(Transport::class, 'created_user_id', 'id');
    }
}
