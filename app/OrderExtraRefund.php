<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderExtraRefund extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_extra_refund';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    public function order() {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function getStatusNameAttribute() {
        switch ($this->attributes['status']) {
            case 0:
                return __('order.status_cancel');
            case 2:
                return __('order.status_processing');
            case 4:
                return __('order.status_complete');
            default:
                return __('order.status_new');
        }
    }

    public function getStatusLabelAttribute() {
        if ($this->attributes['status'] == 0) {
            return "badge badge-danger";
        } else if ($this->attributes['status'] == 1) {
            return "badge";
        } else if ($this->attributes['status'] == 4) {
            return "badge badge-success";
        } else {
            return "badge badge-warning";
        }
    }

    /**
     * Belong to image 1 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image1()
    {
        return $this->belongsTo(File::class, 'image_1_file_id', 'id');
    }

    /**
     * Belong to image 2 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image2()
    {
        return $this->belongsTo(File::class, 'image_2_file_id', 'id');
    }

    /**
     * Belong to image 3 File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image3()
    {
        return $this->belongsTo(File::class, 'image_3_file_id', 'id');
    }

}
