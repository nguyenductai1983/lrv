<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Components\ConvertsUtil;

class Shipment extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_shipment';

    public function warehouse() {
        return $this->belongsTo(Warehouse::class, 'to_warehouse_id', 'id');
    }
    
    public function getEstimateDeliveryAttribute() {
        return ConvertsUtil::strToDate($this->attributes['estimate_delivery']);
    }
}
