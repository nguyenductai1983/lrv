<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'size',
        'name',
        'path',
        'valid',
        'created_user_id'
    ];

    /**
     * Get path attribute
     *
     * @param string $value
     * @return string
     */
    public function getPathAttribute($value)
    {
//        return Storage::url($value);
        return $value ? ('/storage' . $value) : $value;
    }

    /**
     * Get path origin attribute
     *
     * @return string mixed
     */
    public function getPathOriginAttribute() {
        return ($this->path ? str_replace('/storage', '', $this->path) : $this->path);
    }
    
    public function getValidNameAttribute() {
        switch ($this->attributes['valid']) {
            case 0:
                return __('banner.active');
            case 1:
                return __('banner.deactive');
            default:
                return __('banner.active');
        }
    }
}
