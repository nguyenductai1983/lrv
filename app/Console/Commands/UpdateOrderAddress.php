<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Country;
use App\Province;
use App\City;

class UpdateOrderAddress extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:address-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $orders = Order::where([])->get();
        if(empty($orders)){
            return;
        }
        foreach($orders as $order){
            $senderCountry = Country::where('id', $order->sender_country_id)->first();
            $order->sender_country_name = isset($senderCountry->name) ? $senderCountry->name : '';
            $senderProvince = Province::where('id', $order->sender_province_id)->first();
            $order->sender_province_name = isset($senderProvince->name) ? $senderProvince->name : '';
            $senderCity = City::where('id', $order->sender_city_id)->first();
            $order->sender_city_name = isset($senderCity->name) ? $senderCity->name : '';
            
            $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
            $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
            $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
            $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
            $receiverCity = City::where('id', $order->receiver_city_id)->first();
            $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';

            $order->update();
        }
    }

}
