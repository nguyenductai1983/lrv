<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ActionLogs;

class LogHourCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'action:logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //
    }
    public static function test()
    {
        $actionlogs = new ActionLogs();
        $actionlogs->body='nguyen van CALL';
        $actionlogs->save();
    }
}
