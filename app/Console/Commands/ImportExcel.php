<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Customer;
use App\Address;
use App\Province;
use App\City;
use Maatwebsite\Excel\Excel;

class ImportExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        Excel::load('nhan.xlsx', function($reader) {
            $results = $reader->all()->toArray();
            foreach($results as $key => $item){
                $customer = Customer::where('reset_password_token', '=', $item['nn_khncc_id'])->first();
                if(empty($customer)){
                    continue;
                }
                if(empty($item['nn_tel'])){
                    continue;
                }
                $address = new Address();
                $address->customer_id = $customer->id;
                $address->first_name = $item['nn_f_name'];
                $address->middle_name = $item['nn_m_name'];
                $address->last_name = $item['nn_l_name'];
                $address->address_1 = $item['nn_diachi'];
                $address->telephone = $item['nn_tel'];
                $address->cellphone = $item['nn_phone'];
                $address->postal_code = $item['nn_postcode'];

                if($item['nn_nuoc'] == 'VN'){
                    $address->country_id = 47;
                    $city = City::where('name', '=', $item['nn_thanhpho_ten'])->first();
                    if(!empty($city)){
                        $address->city_id = $city->id;
                        $address->province_id = $city->province_id;
                    }else{
                        continue;
                    }
                }else{
                    $post_code = substr($item['nn_postcode'], 0, 3);
                    if(!empty($post_code)){
                        // Tìm quận huyện
                        $city = City::where('postal_code', '=', $post_code)->first();
                        if(!empty($city)){
                            $address->country_id = $city->country_id;
                            $address->province_id = $city->province_id;
                            $address->city_id = $city->id;
                        }else{
                            continue;
                        }
                    }else{
                        continue;
                    }
                }
                //dd($address);
                $address->save();
                echo "Create new address => id:{$address->id}/n";
            }
        })->get();

    }
}
