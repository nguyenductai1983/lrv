<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mts;
use App\User;

class UserMts extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mts:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        $arrs = ['CA010003MCAVN', 'CA010002MCAVN'];
        foreach($arrs as $code){
            $mts = Mts::where(['code' => $code])->first();
            if(empty($mts)){
                return [];
            }
            if($code == 'CA010003MCAVN'){
                $mts->code = 'ADM0003MCAVN';
            }
            if($code == 'CA010002MCAVN'){
                $mts->code = 'ADM0002MCAVN';
            }
            $mts->user_id = 1;
            $mts->save();
        }
    }

}
