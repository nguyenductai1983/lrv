<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Country;
use App\Province;
use App\City;
use App\Exports\ExportQuery;

class ImportExcelUs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:usa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        Excel::load('8.xlsx', function($reader) {
            $country_id = 48;
            $results = $reader->all()->toArray();
            foreach($results as $key => $item){
                $province = Province::where('name', '=', $item['state'])->where('country_id', '=', $country_id)->first();
                if(empty($province)){
                    $province = new Province();
                    $province->country_id = $country_id;
                    $province->name = $item['state'];
                    $province->code = $item['abbreviation'];
                    $province->is_active = 1;

                    $province->save();
                }
                $city = City::where('name', '=', $item['city'])->where('country_id', '=', $country_id)->where('province_id', '=', $province->id)->first();
                if(empty($city)){
                    $city = new City();
                    $city->country_id = $country_id;
                    $city->province_id = $province->id;
                    $city->name = $item['city'];
                    $city->code = $item['city'];
                    $city->is_active = 1;
                    $city->postal_code = $item['zip_code'];

                    $city->save();
                }
            }
        })->get();
    }
}
