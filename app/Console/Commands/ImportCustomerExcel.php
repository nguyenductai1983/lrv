<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Agency;
use App\Customer;
use App\City;
use Excel;

class ImportCustomerExcel extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        Excel::load('gui.xlsx', function($reader) {
            $results = $reader->all()->toArray();
            foreach($results as $key => $item){
                $customerExist = Customer::where('code', '=', $item['khncc_ma'])->first();
                if(!empty($customerExist)){
                    continue;
                }
                if(empty($item['khncc_tel'])){
                    continue;
                }
                $customer = new Customer();
                $old = explode("_",$item['khncc_ma']);
                if(isset($old[0]) && !empty($old[0])){
                    // Đại lý
                    $agency = Agency::where('code', '=', $old[0])->first();
                    if(empty($agency)){
                        $agency = new Agency();
                        $agency->name = $old[0];
                        $agency->code = $old[0];

                        $agency->save();
                    }
                    $customer->agency_id = $agency->id;
                }
                $customer->code = $item['khncc_ma'];
                $customer->customer_group_id = 1;
                $customer->first_name = $item['khncc_f_name'];
                $customer->first_name = $item['khncc_m_name'];
                $customer->first_name = $item['khncc_l_name'];
                $customer->address_1 = $item['khncc_diachi'];
                $customer->address_2 = $item['khncc_diachi2'];
                $customer->telephone = $item['khncc_tel'];
                $customer->cellphone = $item['khncc_mobile'];
                $customer->id_card = $item['khncc_theid'];
                $customer->postal_code = $item['khncc_post_code'];
                $customer->reset_password_token = $item['khncc_id'];

                $post_code = substr($item['khncc_post_code'], 0, 3);
                if(!empty($post_code)){
                    // Tìm quận huyện
                    $city = City::where('postal_code', '=', $post_code)->first();
                    if(!empty($city)){
                        $customer->country_id = $city->country_id;
                        $customer->province_id = $city->province_id;
                        $customer->city_id = $city->id;
                    }
                }
                $customer->save();
                echo "Create new customer => id:{$customer->id}/n";
            }
        })->get();

    }

}
