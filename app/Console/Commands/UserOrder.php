<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\User;
use App\Transport;

class UserOrder extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $orders = Order::where([])->get();
        if(empty($orders)){
            return [];
        }
        foreach($orders as $order){
            $userCode = substr($order->code, 0, 3);
            if(empty($userCode)){
                continue;
            }
            $user = User::where(['code' => $userCode])->first();
            if(empty($user)){
                continue;
            }
            if($order->user_id != $user->id){
                $order->user_id = $user->id;
                $order->save();
                if(!empty($order->transport_id)){
                    Transport::where('id', $order->transport_id)->update(['user_id' =>$user->id]);
                }
            }
        }
    }

}
