<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use App\Agency;
use App\AgencyProduct;

class ProductAgency extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:agency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $products = Product::where([])->get();
        if(empty($products)){
            return [];
        }
        $agencys = Agency::where([])->get();
        if(empty($agencys)){
            return [];
        }
        foreach($products as $product){
            foreach ($agencys as $agency){
                $agencyProduct = AgencyProduct::where(['agency_id' => $agency->id, 'product_id' => $product->id])->first();
                if(empty($agencyProduct)){
                    $newAgencyProduct = new AgencyProduct();
                    $newAgencyProduct->agency_id = $agency->id;
                    $newAgencyProduct->product_id = $product->id;
                    $newAgencyProduct->messure_unit_id = $product->messure_unit_id;
                    $newAgencyProduct->currency_id = $product->currency_id;
                    $newAgencyProduct->price = $product->sale_price;
                    $newAgencyProduct->messure_unit_code = $product->messureunits->code;
                    $newAgencyProduct->currency_code = $product->currencies->code;
                    $newAgencyProduct->weight_unit_id = $product->weight_unit_id;
                    $newAgencyProduct->weight = $product->weight;
                    $newAgencyProduct->save();
                }
            }
        }
    }

}
