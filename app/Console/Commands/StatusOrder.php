<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Address;

class StatusOrder extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        Order::where('shipping_status', '=', 8)->update(['order_status' => 4]);
    }

}
