<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Address;

class ShippingAddressOrder extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:shipaddress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $orders = Order::where([])->get();
        if(empty($orders)){
            return [];
        }
        foreach($orders as $order){
            $address = Address::where(['id' => $order->shipping_address_id])->first();
            if(empty($address)){
                continue;
            }
            if(!empty($order->receiver_address) && $order->receiver_address != $address->address_1){
                $newAddress = new Address();
                $newAddress->customer_id = $order->customer_id;
                $newAddress->first_name = $order->receive_first_name;
                $newAddress->middle_name = $order->receiver_middle_name;
                $newAddress->last_name = $order->receive_last_name;
                $newAddress->address_1 = $order->receiver_address;
                $newAddress->address_2 = $order->receiver_address_2;
                $newAddress->country_id = $order->receiver_country_id;
                $newAddress->province_id = $order->receiver_province_id;
                $newAddress->city_id = $order->receiver_city_id;
                $newAddress->postal_code = $order->receiver_post_code;
                $newAddress->telephone = $order->receiver_phone;
                $newAddress->email = $order->receiver_email;
                $newAddress->attention = $order->receiver_attention;
                $newAddress->instruction = $order->receiver_instruction;
                $newAddress->notify_recipient = $order->receiver_notify_recipient;
                $newAddress->residential = $order->receiver_residential;
                $newAddress->save();

                $order->shipping_address_id = $newAddress->id;
                $order->save();
            }
        }
    }

}
