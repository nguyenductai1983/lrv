<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_transaction';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
    
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
