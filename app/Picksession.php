<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picksession extends Model
{
    /**
     * Table to use
     */
    protected $table = 'pick_session';

    /**
     * The attributes that are mass assignable.
     */

}
