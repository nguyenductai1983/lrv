<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests\AddressRequest;
use App\Http\Controllers\Controller;
use App\Components\LanguageUtil;
use App\Services\PointService;
use App\Country;
use App\Province;
use App\City;
use App\Customer;
use App\Address;
use App\CustomerPointLog;
use App\Campaign;
use Exception;
use App\Services\LogService;

class CustomerController extends Controller {

    public function index() {
        $this->data = ['menu' => '1.1'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['countries'] = Country::where('is_active', '=', 1)->orderBy('name', 'asc')->get();
        $customer = Auth::user();
        $this->data['customer'] = $customer;

        return view('customer.profile', $this->data);
    }

    public function changePassword() {
        $this->data = ['menu' => '1.2'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('customer.change_password', $this->data);
    }

    public function address() {
        $customer = Auth::user();

        $query = Address::where(['customer_id' => $customer->id, 'is_deleted' => 0]);

        $this->data = ['menu' => '1.3'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['addresses'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['customer'] = $customer;

        return view('customer.address', $this->data);
    }

    public function point() {
        $customer = Auth::user();

        $query = CustomerPointLog::where(['customer_id' => $customer->id]);

        $this->data = ['menu' => '1.4'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['points'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['customer'] = $customer;

        return view('customer.point', $this->data);
    }

    public function coupon() {
        $customer = Auth::user();

        $query = Campaign::where(['customer_id' => $customer->id]);

        $this->data = ['menu' => '1.5'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['coupons'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['customer'] = $customer;

        return view('customer.coupon', $this->data);
    }

    public function getCouponCode() {
        $customer = Auth::user();

        $flag = PointService::convertPointToCoupon($customer);
        if($flag){
            return response(["success" => true, "message" => "Success.", "errors" => [], "data" => []]);
        }
        return response(["success" => false, "message" => __('message.get_coupon_code_fail')]);
    }

    public function addressEdit($id) {
        $customer = Auth::user();
        $address = Address::where(['id' => $id, 'customer_id' => $customer->id, 'is_deleted' => 0])->first();
        if (empty($address)) {
            abort(404);
        }
        $this->data = ['menu' => '1.3'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['address'] = $address;
        $this->data['countries'] = Country::where('is_active', '=', 1)->orderBy('name', 'asc')->get();

        return view('customer.address_edit', $this->data);
    }

    public function addressCreate() {
        $customer = Auth::user();
        $this->data = ['menu' => '1.3'];
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['countries'] = Country::where('is_active', '=', 1)->orderBy('name', 'asc')->get();

        return view('customer.address_create', $this->data);
    }

    public function addressStore(AddressRequest $request) {
        $params = $request->all();

        try {
            DB::beginTransaction();
            $customer = Auth::user();
            $address = new Address();
            $address->customer_id = $customer->id;
            $address->first_name = $params['first_name'];
            $address->middle_name = $params['middle_name'];
            $address->last_name = $params['last_name'];
            $address->address_1 = $params['address_1'];
            $address->address_2 = $params['address_2'];
            $address->telephone = $params['telephone'];
            $address->cellphone = $params['cellphone'];
            $address->postal_code = $params['postal_code'];
            $address->email = $params['email'];
            $address->country_id = $params['country_id'];
            $address->province_id = $params['province_id'];
            $address->city_id = $params['city_id'];
            $address->ward_id = null;
            if(!empty($params['ward_id']))
            {$address->ward_id = $params['ward_id'];
            }
            $address->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }
        return redirect()->route('profile.address', $address->customer_id);
    }

    public function addressUpdate(AddressRequest $request, $id) {
        if (!$address = Address::find($id)) {
            abort(404);
        }
        $params = $request->all();

        try {
            DB::beginTransaction();
            $address->first_name = $params['first_name'];
            $address->middle_name = $params['middle_name'];
            $address->last_name = $params['last_name'];
            $address->address_1 = $params['address_1'];
            $address->address_2 = $params['address_2'];
            $address->telephone = $params['telephone'];
            $address->cellphone = $params['cellphone'];
            $address->postal_code = $params['postal_code'];
            $address->email = $params['email'];
            $address->country_id = $params['country_id'];
            $address->province_id = $params['province_id'];
            $address->city_id = $params['city_id'];
            $address->ward_id = null;
            if(!empty($params['ward_id']))
            {$address->ward_id = $params['ward_id'];
            }
            $address->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }
        return redirect()->route('profile.address', $address->customer_id);
    }

    public function addressDestroy($id) {
        $address = Address::find($id);
        if (empty($address)) {
            return response(["success" => false, "message" => __('message.del_data_fail')]);
        }
        try {
            DB::beginTransaction();

            $address->is_deleted = 1;
            $address->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => __('message.del_data_fail')]);
        }

        return response(["success" => true, "message" => __('message.del_data_success'), "data" => []]);
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'middle_name' => 'nullable|string',
            'last_name' => 'required|string',
            'telephone' => 'required|string|max:20',
            'birthday' => 'required|string',
            'country_id' => 'required|integer',
            'province_id' => 'required|integer',
            'city_id' => 'required|integer',
            'address_1' => 'required|string',
            'address_2' => 'nullable|string',
            'postal_code' => 'nullable|string',
            'company' => 'nullable|string',
            'id_card' => 'nullable|string',
            'card_expire' => 'nullable|string',
            'career' => 'nullable|string',
            'image_1_file_id' => 'nullable|integer',
            'image_2_file_id' => 'nullable|integer',
            'image_3_file_id' => 'nullable|integer',
        ], [], [
            'first_name' => __('customer.first_name'),
            'middle_name' => __('customer.middle_name'),
            'last_name' => __('customer.last_name'),
            'telephone' => __('customer.telephone'),
            'birthday' => __('customer.birthday'),
            'country_id' => __('customer.country_id'),
            'province_id' => __('customer.province_id'),
            'city_id' => __('customer.city_id'),
            'address_1' => __('customer.address_1'),
            'address_2' => __('customer.address_2'),
            'postal_code' => __('label.postal_code'),
            'company' => __('label.company'),
            'id_card' => __('customer.id_card'),
            'card_expire' => __('customer.card_expire'),
            'career' => __('customer.career'),
            'image_1_file_id' => __('customer.image_1_file_id'),
            'image_2_file_id' => __('customer.image_2_file_id'),
            'image_3_file_id' => __('customer.image_3_file_id'),
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.check_data_error'), "errors" => $validator->errors(), "data" => []]);
        }
        $user = Auth::user();
        try {
            DB::beginTransaction();
            $user->first_name = $request->get('first_name');
            $user->middle_name = $request->get('middle_name');
            $user->last_name = $request->get('last_name');
            $user->telephone = $request->get('telephone');
            $user->birthday = $request->get('birthday');
            $user->country_id = $request->get('country_id');
            $user->province_id = $request->get('province_id');
            $user->city_id = $request->get('city_id');
            $user->address_1 = $request->get('address_1');
            $user->address_2 = $request->get('address_2');
            $user->postal_code = $request->get('postal_code');
            $user->company = $request->get('company');
            $user->id_card = $request->get('id_card');
            $user->card_expire = $request->get('card_expire');
            $user->career = $request->get('career');
            $user->image_1_file_id = $request->get('image_1_file_id');
            $user->image_2_file_id = $request->get('image_2_file_id');
            $user->image_3_file_id = $request->get('image_3_file_id');
            $user->update();
            DB::commit();
            return response(["success" => true, "message" => __('message.success_sys'), "errors" => [], "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => __('message.data_error'), "errors" => [], "data" => []]);
        }
    }

    public function updateChangePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ],[],[
            'old_password' => __('customer.old_password'),
            'password' => __('customer.password')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => "Lỗi truyền dữ liệu.", "errors" => $validator->errors(), "data" => []]);
        }
        $user = Auth::user();
        $current_password = $user->password;
        if(Hash::check($request->get('old_password'), $current_password)){
            try {
                DB::beginTransaction();
                $user->password = Hash::make($request->get('password'));
                $user->update;

                DB::commit();
                return response(["success" => true, "message" => "Thay đổi mật khẩu thành công.", "errors" => [], "data" => []]);
            } catch (Exception $e) {
                DB::rollBack();
                return response(["success" => false, "message" => "Lỗi thay đổi mật khẩu.", "errors" => [], "data" => []]);
            }
        }else{
            return response(["success" => false, "message" => "Lỗi truyền dữ liệu.", "errors" => $request->only('old_password'), "data" => []]);
        }
    }

}
