<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Components\LanguageUtil;
use App\Order;
use App\OrderTracking;
use App\Services\EshiperService;


class OrderController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orderCode = $request->input('order-code');
        $this->data['trackings'] = [];
        $this->data['eshippers'] = [];
        $this->data['order'] = [];
        if(!empty($orderCode)){
            $order = Order::where(['code' => $orderCode])->first();
            if(!empty($order)){
                $this->data['order'] = $order;
                if (!empty($order->express_order_id)) {
                    $eshiperResponse = EshiperService::getOrder($order->express_order_id);
                    $this->data['eshippers'] = isset($eshiperResponse['data']['history']) ? $eshiperResponse['data']['history'] : [];
                }else{
                    $this->data['trackings'] = OrderTracking::where([
                        ['order_id', '=', $order->id]
                    ])->orderBy('id', 'asc')->get();
                }
            }
        }
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        
        return view('tracking', $this->data);
    }
}
