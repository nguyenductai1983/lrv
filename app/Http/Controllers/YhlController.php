<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Enum\OrderTypeEnum;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
use App\Order;

class YhlController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '4.0'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $customer = Auth::user();
        $query->where('customer_id', '=', $customer->id);
        $query->whereNull('agency_id');
        $query->where('type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl.index', $this->data);
    }
}
