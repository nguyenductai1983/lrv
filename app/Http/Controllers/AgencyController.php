<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Components\LanguageUtil;
use App\Agency;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['agencies'] = Agency::where('is_deleted', 0)->select()->paginate(config('app.items_per_page'));

        return view('agency.index', $this->data);
    }
}
