<?php

namespace App\Http\Controllers;

use App\Components\LanguageUtil;
use App\ProductGroup;
use App\Page;
use App\Enum\ProductGroupEnum;

class NewsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['news'] = Page::with('translates')->where('type', '=', 3)
                        ->orderBy('level', 'asc')
                        ->select()->paginate(config('app.items_per_page'));
        return view('news.index', $this->data);
    }
    
    public function detail($slug)
    {
        if (!$page = Page::with('translate')->where('slug', '=', $slug)->first()) {
            abort(404);
        }

        return view('news.detail', [
            'page' => $page
        ]);
    }
}
