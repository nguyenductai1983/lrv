<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use Illuminate\Http\Request;
use App\ShippingPackageLocal;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\GHTKEnum;
use App\Http\Controllers\Controller;
use Exception;
use PhpParser\Node\Stmt\Else_;

class callbackGHTK extends Controller
{
    public function updateShipment(Request $request)
    {
        $param = $request->all();
        $user = auth()->user();
        if (!empty($user)) {
            $user = $user->id;
        }
        // $apiKey = $request->get('key');
        // $partner_id = $request->get('partner_id');
        // $label_id = $request->get('label_id');
        // $status_id = $request->get('status_id');
        // $action_time = $request->get('action_time');
        // $reason_code = $request->get('reason_code');
        // $reason = $request->get('reason');
        // $weight = $request->get('weight');
        // $fee = $request->get('fee');
        // $pick_money = $request->get('pick_money');
        // $return_part_package = $request->get('return_part_package');
        if (!isset($param['partner_id']) || empty($param['partner_id'])) {
            return response('partner_id Not Found', 500);
        }
        if (!isset($param['partner_id']) || empty($param['partner_id'])) {
            return response('partner_id Not Found', 500);
        }
        if (!isset($param['label_id']) || empty($param['label_id'])) {
            return response('label_id Not Found', 500);
        }
        // thử tìm shippingPackageLocal bằng mã của hệ thống
        $shippingPackageLocal = ShippingPackageLocal::where(['shipping_code' => $param['partner_id']])->first();
        // nếu không tìm thấy tìm kiếm bằng mã của GHTK
        if(empty($shippingPackageLocal))
        {
            $shippingPackageLocal = ShippingPackageLocal::where(['tracking_code' => $param['label_id']])->first();
        }
        if (empty($shippingPackageLocal->order_id)) {
            $orderData = Order::where(['code' => $param['partner_id']])->first();
        } else {
            $orderData = Order::find($shippingPackageLocal->order_id);
        }
        if (empty($orderData)) {
            DB::rollBack();
            return response('partner_id Not Found', 501);
        }
        $GHTK_Status = $this->mappingGHTK_Status($param['status_id']);
        $oder_status = $GHTK_Status['oder_status'];
        $shipping_status = $GHTK_Status['shipping_status'];
        $shipping_local_status = $GHTK_Status['shipping_local_status'];
        $tracking_status = $GHTK_Status['tracking_status'];
        $log_status = $GHTK_Status['log_status'];
        $log_api='';
        try {
            DB::beginTransaction();

            if (!empty($shipping_local_status)) {
                if (empty($shippingPackageLocal)) {
                    $log_api = 'PO not Select shipping';
                } else {
                    $shippingPackageLocal->tracking_code = $param['label_id'];
                    $shippingPackageLocal->shipping_code = $param['partner_id'];
                    $shippingPackageLocal->status = $shipping_local_status;
                    $shippingPackageLocal->updated_at = date('Y-m-d H:i:s');
                    if (is_array($param)) {
                        $shippingPackageLocal->last_response_api = json_encode($param);
                    }
                    $shippingPackageLocal->update();
                }
                if (!empty($oder_status)) {
                    $orderData->order_status = $oder_status;
                }
                if (!empty($shipping_status)) {
                    $orderData->shipping_status = $shipping_status;
                }
                if (!empty($oder_status) || !empty($shipping_status)) {
                    $orderData->update();
                }
                if (!empty($tracking_status)) {
                    $orderTracking = new OrderTracking();
                    $orderTracking->order_id = $orderData->id;
                    $orderTracking->tracking_code = $param['label_id'];
                    $orderTracking->order_code = $param['partner_id'];
                    $orderTracking->status = $tracking_status;
                    $orderTracking->note = isset($param['reason']) ? $param['reason'] : '';
                    $orderTracking->log_api = $log_api;
                    if (is_array($param)) {
                        if (!empty($param['_token'])) {
                            array_shift($param);
                        }
                        $orderTracking->response_api = json_encode($param);
                    }
                    $orderTracking->create_user_id = $user;
                    $orderTracking->save();
                }
                if (!empty($log_status)) {
                    $orderLog = new OrderLog();
                    $orderLog->order_id = $orderData->id;
                    $orderLog->status = $log_status;
                    $orderLog->description = isset($param['reason']) ? $param['reason'] : '';
                    $orderLog->save();
                }
            } else if ($log_status === OrderLogStatusEnum::STATUS_OTHER) {
                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->status = 1;
                $orderLog->description = isset($param['reason']) ? $param['reason'] : '';
                $orderLog->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('FAIL', 501);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }
    private function mappingGHTK_Status($GHTKStatus)
    {
        $result = [];

        switch ($GHTKStatus) {
            case GHTKEnum::STATUS_CANCEL: //Hủy đơn hàng
                $oder_status = OrderStatusEnum::STATUS_PROCESSING;
                $shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                $shipping_local_status = ShippingPackageLocalEnum::CANCEL;
                $tracking_status = OrderTrackingStatusEnum::STATUS_REVICER;
                $log_status = OrderLogStatusEnum::STATUS_CANCEL;
                break;
            case GHTKEnum::STATUS_NOT_APPROVED:  // 1	Chưa tiếp nhận
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_APPROVED: // 2	Đã tiếp nhận
                $oder_status = OrderStatusEnum::STATUS_PROCESSING;
                $shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
                $shipping_local_status = ShippingPackageLocalEnum::APPROVED;
                $tracking_status = OrderTrackingStatusEnum::STATUS_CARRIER;
                $log_status = OrderLogStatusEnum::STATUS_READY_TO_SHIP;
                break;
            case GHTKEnum::STATUS_WAREHOURE: // 3	Đã lấy hàng/Đã nhập kho
                $oder_status = OrderStatusEnum::STATUS_PROCESSING;
                $shipping_status =  OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
                $shipping_local_status = ShippingPackageLocalEnum::DELIVERY_CARRIER;
                $tracking_status = OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;
                $log_status = OrderLogStatusEnum::STATUS_CARRIER;
                break;
            case GHTKEnum::STATUS_PROCESS: // 4	Đã điều phối giao hàng/Đang giao hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_DELIVERY_NOT_LIABILITIE: // 5	Đã giao hàng/Chưa đối soát
                $oder_status = OrderStatusEnum::STATUS_COMPLETE;
                $shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
                $shipping_local_status = ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
                $tracking_status = OrderTrackingStatusEnum::STATUS_DONE;
                $log_status = OrderLogStatusEnum::STATUS_DONE;
                break;
            case GHTKEnum::STATUS_DELIVERY: // 6 Đã đối soát
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_NOT_GET_GOODS:  // 7	Không lấy được hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = ShippingPackageLocalEnum::CARRIER_PICKUP_FAIL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_DELETION_GOODS: // 8	Hoãn lấy hàng
                $oder_status = OrderStatusEnum::STATUS_PROCESSING;
                $shipping_status =  OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN;
                $shipping_local_status = ShippingPackageLocalEnum::CARRIER_PICKUP_FAIL;
                $tracking_status =  OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;
                $log_status = OrderLogStatusEnum::STATUS_CARRIER;
                break;
            case GHTKEnum::STATUS_NOT_DELIVERY: // 9 Không giao được hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_DELAY: // 10 Delay giao hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_LIABILITIE: // 11	Đã đối soát công nợ trả hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_GET_GOODS: // 12	Đã điều phối lấy hàng/Đang lấy hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_REIMBURSEMENT_ORDERS: // 13	Đơn hàng bồi hoàn
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::SHIPPING_STATUS_RETURN: // 20	Đang trả hàng (COD cầm hàng đi trả)
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::SHIPPING_STATUS_RETURN_COMPLETE: // 21	Đã trả hàng (COD đã trả xong hàng)
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = OrderLogStatusEnum::STATUS_RETURN;
                break;
            case GHTKEnum::STATUS_SHIPPER_GET_GOODS: // 123	Shipper báo đã lấy hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_SHIPPER_NOT_GET_GOODS: // 127	Shipper (nhân viên lấy/giao hàng) báo không lấy được hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_SHIPPER_DELAY: // 128	Shipper báo delay lấy hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_SHIPPER_DELIVERY: // 45	Shipper báo đã giao hàng
                $oder_status = OrderStatusEnum::STATUS_COMPLETE;
                $shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
                $shipping_local_status = ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
                $tracking_status = OrderTrackingStatusEnum::STATUS_DONE;
                $log_status = OrderLogStatusEnum::STATUS_DONE;
                break;

            case GHTKEnum::STATUS_SHIPPER_NOT_DELIVERY: // 49	Shipper báo không giao được giao hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = ShippingPackageLocalEnum::CARRIER_DELIVERY_FAIL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            case GHTKEnum::STATUS_SHIPPER_DELAY_DELIVERY: // 410	Shipper báo delay giao hàng
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = NULL;
                break;
            default:
                $oder_status = NULL;
                $shipping_status = NULL;
                $shipping_local_status = NULL;
                $tracking_status = NULL;
                $log_status = OrderLogStatusEnum::STATUS_OTHER;
        }
        $result = [
            'oder_status' => $oder_status,
            'shipping_status' => $shipping_status,
            'shipping_local_status' => $shipping_local_status,
            'tracking_status' => $tracking_status,
            'log_status' => $log_status,
        ];
        return $result;
    }
}
