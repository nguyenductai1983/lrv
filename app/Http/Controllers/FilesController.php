<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\File;

class FilesController extends Controller
{
    public function upload(Request $request)
    {
        $rules = [
            'file' => 'required'
        ];
        $rules['file'] .= '|mimes:' . config('file.img_mimes') . '|max:' . config('file.img_max_size');
        $this->validate($request, $rules, [], [
            'files' => __('label.file_invalid')
        ]);

        $file = $request->file('file');
        $path = date('Y') . '/' . date('m');
        $name = $file->hashName();
        if (!Storage::putFileAs('public/' . $path, $file, $name)) {
            return response('Internal Server Error', 500);
        }
        $attrs = [
            'type'            => $file->getMimeType(),
            'size'            => $file->getSize(),
            'name'            => $file->getClientOriginalName(),
            'path'            => '/' . $path . '/' . $name,
            'created_user_id' => isset(Auth::guard('admin')->user()->id) ? Auth::guard('admin')->user()->id : null
        ];

        if (!$result = File::create($attrs)) {
            return response('Internal Server Error', 500);
        }
        return $result;
    }
}
