<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
// use App\Enum\OrderTypeEnum;
// use App\Enum\CurrencyEnum;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
use App\Components\RealtimeUtil;
use App\Services\OrderService;
use App\MTSSurcharge;
use App\Mts;
use App\Address;
use App\Customer;
use App\Notification;
use App\User;
use Exception;
class MtsController extends Controller
{
    protected $realtimeUtil;
    /**
     * Assign data
     */
    private $data;

    /**
     * MtsController constructor.
     */
    public function __construct(RealtimeUtil $realtimeUtil)
    {
        $this->realtimeUtil = $realtimeUtil;
        $this->data = [
            'menu' => '5.0'
        ];
    }

    private function _validate(Request $request)
    {
        $this->validate($request, [
            'sender' => 'required|array',
            'sender.id' => 'nullable|exists:customers,id',
            'sender.first_name' => 'required|required_without:sender.id|string|max:50',
            'sender.middle_name' => 'nullable|string|max:50',
            'sender.last_name' => 'required|required_without:sender.id|string|max:50',
            'sender.address_1' => 'required|required_without:sender.id|string|max:35',
            'sender.telephone' => 'required|required_without:sender.id|string|max:20',
            'sender.postal_code' => 'nullable|required_without:sender.id|string|max:20',
            'sender.country_id' => 'required|required_without:sender.id|exists:countries,id',
            'sender.province_id' => 'required|required_without:sender.id|exists:provinces,id',
            'sender.city_id' => 'required|required_without:sender.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:35',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',

            'container' => 'required|array',
            'container.reason'  => 'nullable|string',
            'container.transaction_type' => 'required|in:' . implode(',', config('mts.transaction_type')),
            'container.payment_by' => 'required|in:' . implode(',', config('mts.payment_by')),
            'container.amount' => 'required|numeric|min:0',
            'container.transfer_fee' => 'required|numeric|min:0',
            'container.total' => 'required|numeric|min:0',
        ], [], [
            'sender.id' => __('label.sender'),
            'sender.first_name' => __('customer.first_name'),
            'sender.middle_name' => __('customer.middle_name'),
            'sender.last_name' => __('customer.last_name'),
            'sender.address_1' => __('customer.address_1'),
            'sender.telephone' => __('customer.telephone'),
            'sender.postal_code' => __('customer.postal_code'),
            'sender.country_id' => __('customer.country_id'),
            'sender.province_id' => __('customer.province_id'),
            'sender.city_id' => __('customer.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),

            'container.reason' => __('mts.reason'),
            'container.transaction_type' => __('mts.transaction_type'),
            'container.payment_by' => __('mts.payment_by'),
            'container.amount' => __('mts.amount'),
            'container.transfer_fee' => __('mts.transfer_fee'),
            'container.total' => __('mts.total'),
        ]);
    }



    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Mts::where([]);
        $customer = Auth::user();
        $query->where('customer_id', '=', $customer->id);
        $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('complete'))) {
            $this->data['complete'] = 'alert alert-success';
        }
        else
        {
            $this->data['complete'] = 'hidden';
        }
        $this->data['mtss'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('mts.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $customerData = Customer::where('id', auth()->user()->id)->first();
        if(empty($customerData->country_id) || empty($customerData->province_id) || empty($customerData->city_id)){
            return redirect()->route('profile.index');
        }
        $this->data['customer'] = $customerData;
        $MTSSurcharge=MTSSurcharge::get();
        $this->data['MTSSurcharge']=$MTSSurcharge;
        $this->data['language'] = LanguageUtil::getMTSKeyJavascript();
        return view('mts.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function Check_info_Invalid($customer,$amount,$relationship)
    {
        $list_errors=[];
        $status=0;
        $arr=['id_card','card_expire','birthday','career'];
            $check_card_expire =false;
            if($customer['card_expire']){
                $today = date('Y-m-d H:i:s');
                $ngay=explode('/',$customer['card_expire']);
                $varDate = Date($ngay[2].'-'.$ngay[1].'-'.$ngay[0].': 0:0:0');
                if($varDate >= $today) {
                    $check_card_expire = true ;}
                }
                foreach ($arr as & $value) {
                    if (!$customer[$value]) {
                        $status=2;
                        $list_errors+=array('customer.'.$value =>[__('customer.check').__('customer.'.$value)]);
                    }
                }
            if(!$check_card_expire){
                $status=2;
                $list_errors+=array('customer.card_expire' => [__('customer.check').__('customer.card_expire')]);
            }
            if(!$relationship)
            {
                $status=2;
                $list_errors+=array('receiver.relationship' => [__('receiver.check').__('receiver.relationship')]);
            }
        if($amount>=3000){
            if (!$customer['image_1_file_id'] && !$customer['image_2_file_id'] && !$customer['image_2_file_id']) {
                $status=1;
                $list_errors+=array('customer.image_1_file_id' => [__('customer.check').__('customer.image_1_file_id')]);
            }
        }
            $result= [$status,$list_errors];
            return $result;
    }
    private function Check_info_amount($amount, $customer,$relationship)
    {
        $result = ['status' => true,''];
        if($amount >= 3000)
            {
                $customer= $this->Check_info_Invalid($customer,$amount,$relationship);
                if($customer[0]>=1){
                return ['status' => false,$customer[1]];
            }
            }elseif($amount >= 850 )
            {
                $customer= $this->Check_info_Invalid($customer,$amount,$relationship);
                if($customer[0]>=2){
                return ['status' => false,$customer[1]];
                }
            }
        return $result;
    }
    public function store(Request $request)
    {
        $this->_validate($request);
        $data = $request->all();
        $check_info_amount = $this->Check_info_amount($data['container']['amount'], $data['sender'],
        $data['receiver']['relationship']) ;
        if(!$check_info_amount['status'])
        {
            return response(['errors'=>$check_info_amount[0]],422)->header('Content-Type', 'text/plain');
        }
        try {
            DB::beginTransaction();
            $sender = $data['sender'];
            $receiver = $data['receiver'];
            $customer = auth()->user();
            if (isset($data['container']) && !empty($data['container'])) {
                $mts = new Mts();

                $mts->currency_id = $data['container']['currency'];
                $mts->customer_id = $customer->id;

                $customerData = Customer::where('id', $customer->id)->first();
                if(!empty($customerData)){
                    if(empty($customerData->country_id)){
                        $customerData->country_id = $sender['country_id'];
                    }
                    if(empty($customerData->province_id)){
                        $customerData->province_id = $sender['province_id'];
                    }
                    if(empty($customerData->city_id)){
                        $customerData->city_id = $sender['city_id'];
                    }
                    if(empty($customerData->postal_code)){
                        $customerData->postal_code = $sender['postal_code'];
                    }
                    $customerData->update();
                }
                $mts->sender_first_name = $sender['first_name'];
                $mts->sender_middle_name = $sender['middle_name'];
                $mts->sender_last_name = $sender['last_name'];
                $mts->sender_email = $customer->email;
                $mts->sender_phone = $sender['telephone'];
                $mts->sender_address = $sender['address_1'];
                $mts->sender_country_id = $sender['country_id'];
                $mts->sender_province_id = $sender['province_id'];
                $mts->sender_city_id = $sender['city_id'];
                $mts->sender_post_code = $sender['postal_code'];

                if (isset($receiver['id']) && !empty($receiver['id'])) {
                    $receiverObj = Address::where('id', $receiver['id'])->first();
                    if (empty($receiverObj)) {
                        $receiverObj = new Address();
                        $properties = array_keys($receiver);
                        foreach ($properties as $property) {
                            if (isset($receiver[$property]) && !empty($receiver[$property]))
                                $receiverObj->$property = $receiver[$property];
                        }
                        $receiverObj->save();
                    }
                    else
                    {
                        if (empty( $receiverObj->relationship))
                        {
                            $receiverObj->relationship=$data['receiver']['relationship'];
                            $receiverObj->save();
                        }
                    }
                } else {
                    $receiverObj = new Address();
                    $properties = array_keys($receiver);
                    foreach ($properties as $property) {
                        if (isset($receiver[$property]) && !empty($receiver[$property]))
                            $receiverObj->$property = $receiver[$property];
                    }
                    $receiverObj->customer_id = $customer->id;
                    $receiverObj->save();
                }

                $mts->shipping_address_id = $receiverObj->id;
                $mts->receiver_last_name = $receiver['last_name'];
                $mts->receiver_first_name = $receiver['first_name'];
                $mts->receiver_middle_name = $receiver['middle_name'];
                $mts->receiver_phone = $receiver['telephone'];
                $mts->receiver_address = $receiver['address_1'];
                $mts->receiver_country_id = $receiver['country_id'];
                $mts->receiver_province_id = $receiver['province_id'];
                $mts->receiver_city_id = $receiver['city_id'];
                $mts->receiver_post_code = $receiver['postal_code'];

                $mts->reason = $data['container']['reason'];
                $mts->relationship = $data['receiver']['relationship'];
                $mts->transaction_type = $data['container']['transaction_type'];
                $mts->payment_by = $data['container']['payment_by'];
                $mts->total_goods = $data['container']['amount'];
                $mts->discount_type = $data['container']['discount_type'];
                $mts->discount_number = $data['container']['discount_number'];
                $mts->total_discount = 0;
                // if($mts->discount_number > 0 && $mts->discount_type == 1){
                //     $mts->total_discount = ($mts->total_goods * $mts->discount_number) / 100;
                // }else if($mts->discount_number > 0 && $mts->discount_type == 2){
                //     $mts->total_discount = $mts->discount_number;
                // }
                $surcharge= OrderService::Surcharge($mts->total_goods,$mts->discount_number,$mts->discount_type);
                $mts->transfer_fee=$surcharge['total_fee'];
                $mts->total_final = $surcharge['total_final'];
                $mts->total_discount = $surcharge['discount'];
                // $mts->transfer_fee = ((OrderService::getPerFeeMts($mts->receiver_province_id) * $mts->total_goods) + config('mts.fix_fee'))- $mts->total_discount;
                // /* bổ sung phi tien gui nhiều*/
                // if($mts->total_goods > config('mts.mts3000'))
                // {
                //     $mts->transfer_fee += config('mts.fix_fee3000');
                // }
                //     else if($mts->total_goods > config('mts.mts1500'))
                //     {
                //        $mts->transfer_fee += config('mts.fix_fee1500');
                //     }
                // $mts->total_final = $mts->total_goods + $mts->transfer_fee;
                $mts->save();

                $mts->code = "KH" . sprintf('%04d', $mts->id) . "MCAVN";
                $mts->update();

                //Đẩy vào notifi
                $users = User::where('role_id', '=', 1)->get();
                if(!empty($users)){
                    $this->realtimeUtil->registerChannel('notification');
                    foreach($users as $user){
                        $notification = new Notification();
                        $notification->to_user_id = $user->id;
                        $notification->name = $mts->code;
                        $notification->is_read = 0;
                        $notification->save();

                        $notification->path = route('mts.quote.edit', $mts->id) . '?notification_id='.$notification->id;
                        $notification->update();
                        // Đẩy vào 1 channel có sẵn
                        $this->realtimeUtil->publish(['name' => $notification->name, 'to_user_id' => $notification->to_user_id, 'path' => $notification->path], 'notification');
                    }
                }
            }else{
                DB::rollBack();
                return response('Không tìm thấy config giá', 500);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $mts = Mts::where('id', '=', $id)
            ->with([
                'customer' => function ($query){
                    $query->with('image1');
                    $query->with('image2');
                    $query->with('image3');
                },
                'receiver',
                'currency'
            ])
            ->first();

        if (!$mts) {
            return response('Not Found', 404);
        }
        $mts->receiver->first_name = $mts->receiver_first_name;
        $mts->receiver->middle_name = $mts->receiver_middle_name;
        $mts->receiver->last_name = $mts->receiver_last_name;
        $mts->receiver->address_1 = $mts->receiver_address;
        $mts->receiver->telephone = $mts->receiver_phone;
        $mts->receiver->postal_code = $mts->receiver_post_code;
        $mts->receiver->country_id = $mts->receiver_country_id;
        $mts->receiver->province_id= $mts->receiver_province_id;
        $mts->receiver->city_id = $mts->receiver_city_id;
        if($mts->pay_date){
            $mts->pay_date = ConvertsUtil::strToDate($mts->pay_date);
        }
        return response([
            'mts' => $mts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        $MTSSurcharge=MTSSurcharge::get();
        $this->data['MTSSurcharge']=$MTSSurcharge;
        return view('mts.edit', $this->data);
    }
}
