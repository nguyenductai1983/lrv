<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Ward;
use App\City;
use App\Country;
use App\Currency;
use App\DimensionUnit;
use App\Province;
use App\Warehouse;
use App\PaymentMethod;
use App\Enum\WarehouseEnum;
use App\Enum\PaymentMethodEnum;
use App\Services\CampaignService;
use App\Http\Controllers\Controller;
class ApiController extends Controller
{
    public function usedCoupon(Request $request){
        $data = $request->all();
        $result = CampaignService::setCouponByOrder($data['coupon_code'], $data['order_id']);
        return response($result);
    }

    public function removedCoupon(Request $request){
        $data = $request->all();
        $result = CampaignService::removeCouponByOrder($data['order_id']);
        return response($result);
    }

    public function couponAmount(Request $request) {
        $data = $request->all();
        $amount = CampaignService::getAmountCoupon($data['container']['coupon_code'], $data['customer']['id'], $data['container']['total_weight']);
        return response([
            'amount' => $amount
        ]);
    }

    public function getWarehouses() {
        $warehouses = Warehouse::where("type", WarehouseEnum::TYPE_FOREIGN)->get();
        return response([
            'warehouses' => $warehouses
        ]);
    }

    public function getCountries() {
        $countries = Country::where("is_active", 1)->orderBy("name", 'asc')->get(['id', 'name', 'code']);
        return response([
            'countries' => $countries
        ]);
    }
    public function getProvinces(Request $request) {
        $countryId =  $request->query('country_id');
        $provinces = Province::where("is_active", 1)->where("country_id", $countryId)->orderBy("name", 'asc')->get();
        return response([
            'provinces' => $provinces
        ]);
    }
    public function getCities(Request $request) {
        $provinceId = $request->query('province_id');
        $countryId = $request->query('country_id');
        $cities = City::where("is_active", 1)->where("country_id", $countryId)
            ->where("province_id", $provinceId)->orderBy("name", 'asc')->get();
        return response([
            'cities' => $cities
        ]);
    }
    public function getWards(Request $request) {
        $cityId = $request->query('city_id');
        $Wards='';
        if($cityId >0){
            $Wards = Ward::where("is_active", 1)->where("city_id", $cityId)
           ->orderBy("name", 'asc')->get();
        }
        return response([
            'wards' => $Wards
        ]);
    }
    public function getDimTypes() {
        $dimTypes = DimensionUnit::where([])->orderBy("display_order", 'asc')->get();
        return response([
            'dimTypes' => $dimTypes
        ]);
    }

    public function getPaymentMethods() {
        $col = [
            'id',
            'name',
            'code'
        ];
        $methods = PaymentMethod::where("is_active", PaymentMethodEnum::STATUS_ACTIVE)->get($col);
        return response([
            'methods' => $methods
        ]);
    }

    public function getAddress(Request $request) {
        $address = Address::where(["customer_id" => $request->customer_id, 'is_deleted' => 0])->orderBy("id", 'desc')->get();
        return response([
            'address' => $address
        ]);
    }

    public function getProducts(Request $request)
    {

        $columns = [
            'products.id',
            'product_groups.name as group_name',
            'product_groups.code as group_code',
            'tbl_messure_unit.code as unit',
            'products.code',
            'products.name',
            'products.by_weight',
            DB::raw('(CASE WHEN products.weight > 0 THEN products.weight ELSE 1 END) AS weight'),
            'products.sale_price',
           'products.price',
            'products.description',
            'products.status',
            'products.separate',
        ];

        $query = DB::table('products')->leftJoin('product_groups', 'products.product_group_id', '=', 'product_groups.id');
        $query->leftJoin('tbl_messure_unit', 'products.messure_unit_id', '=', 'tbl_messure_unit.id');
        if (!empty($request->query('product_group'))) {
            $query->where('products.product_group_id', '=', $request->query('product_group'));
        }
        if (!empty($request->query('service'))) {
            $query->where('product_groups.service', '=', $request->query('service'));
        }
        if (!empty($request->query('status'))) {
            $query->where('products.status', '=', $request->query('status'));
        }
        $query->where('products.is_deleted', '=', 0);
        $products = $query->groupBy([
            'products.id',
            'group_name',
            'group_code',
            'unit',
            'products.code',
            'products.name',
            'products.by_weight',
            'weight',
            'products.sale_price',
            'products.price',
            'products.description',
            'products.status',
            'products.separate',
        ])->orderBy('products.display_order', 'asc')->get($columns);
        return response([
            'products' => $products
        ]);
    }
    public function getCurrencies() {
        $currencies = Currency::where("is_active", 1)->orderBy("name", 'asc')->get(['id', 'name', 'code']);
        return response([
            'currencies' => $currencies
        ]);
    }
}
