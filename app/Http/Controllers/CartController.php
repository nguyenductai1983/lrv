<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Enum\OrderTypeEnum;
use App\Enum\CustomerEnum;
use App\Enum\WeightUnitEnum;
use App\Enum\DimensionUnitEnum;
use App\Enum\CurrencyEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
use App\Components\RealtimeUtil;
use App\Product;
use App\Country;
use App\Province;
use App\City;
use App\Ward;
use App\Customer;
use App\Address;
use App\Order;
use App\OrderItem;
use App\PaymentMethod;
use App\Notification;
use App\User;
use Illuminate\Http\Request;
use Exception;
class CartController extends Controller
{
    protected $realtimeUtil;

    /**
     * CartController constructor.
     */
    public function __construct(RealtimeUtil $realtimeUtil)
    {
        $this->realtimeUtil = $realtimeUtil;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function step1()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['cart'] = Cart::content();

        return view('cart.step1', $this->data);
    }

    public function step2()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['cart'] = Cart::content();
        $this->data['countries'] = Country::where('is_active', '=', 1)->orderBy('name', 'asc')->get();
        $customer = Auth::user();
        $this->data['customer'] = $customer;

        return view('cart.step2', $this->data);
    }

    public function step3($code)
    {
        $order = Order::where('code', '=', $code)->where('type', '=', OrderTypeEnum::YHL)->first();
        if(empty($order)){
            return redirect()->route('home.index');
        }
        if($order->payment_status == OrderPaymentStatusEnum::FULL_PAID){
            return redirect()->route('cart.step4', $code);
        }
        $this->data['order'] = $order;
        $this->data['payment_methods'] = PaymentMethod::where('is_deleted', '=', 0)->where('type', '=', 2)->orderBy('display_order')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('cart.step3', $this->data);
    }

    public function step4($code)
    {
        $order = Order::where('code', '=', $code)->where('type', '=', OrderTypeEnum::YHL)->first();
        if(empty($order)){
            return redirect()->route('home.index');
        }
        $this->data['order'] = $order;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('cart.step4', $this->data);
    }

    public function confirm($code)
    {
        $order = Order::where('code', '=', $code)->where('type', '=', OrderTypeEnum::YHL)->first();
        if(empty($order)){
            return redirect()->route('home.index');
        }
        $payment_method = PaymentMethod::where('code', '=', $order->payment_method)->where('type', '=', 2)->first();
        if(empty($payment_method)){
            return redirect()->route('home.index');
        }
        $this->data['order'] = $order;
        $this->data['payment_method'] = $payment_method;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('cart.confirm', $this->data);
    }

    public function addItem(Request $request){
       $validator = Validator::make($request->input(), [
            'id' => 'required|integer',
            'quantity' => 'required|integer',
        ],[],[
            'id' => __('product.id'),
            'quantity' => __('product.quantity')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $product = Product::find($request->input('id'));
        if(empty($product)){
            return response(["success" => false, "message" => __('message.cart-no-product'), "errors" => [],"data" => []]);
        }
        $count = $this->addItemUpdateCart($product, $request->input('quantity'));

        return response(["success" => true, "message" => __('message.cart-add-item-susscess'), "errors" => [],"data" => $count]);
    }

    public function removeItem(Request $request){
        $validator = Validator::make($request->input(), [
            'rowId' => 'required|string'
        ],[],[
            'rowId' => __('product.rowId')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        Cart::remove($request->input('rowId'));

        return response(["success" => true, "message" => __('message.cart-del-item-susscess'), "errors" => [],"data" => Cart::content()->count()]);
    }


    public function updateItem(Request $request){
        $validator = Validator::make($request->input(), [
            'rowId' => 'required|string',
            'quantity' => 'required|integer|min:1',
        ],[],[
            'rowId' => __('product.rowId'),
            'quantity' => __('product.quantity')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $cartItem = Cart::get($request->input('rowId'));
        if(empty($cartItem)){
            return response(["success" => false, "message" => __('message.cart-no-exist'), "errors" => [],"data" => []]);
        }
        $product = Product::find($cartItem->id);
        if(empty($product)){
            return response(["success" => false, "message" => __('message.cart-no-product'), "errors" => [],"data" => []]);
        }
        Cart::update($request->input('rowId'), ['price' => ConvertsUtil::roundNumber($product->sale_price), 'qty' => $request->input('quantity')]);

        $data['price'] = ConvertsUtil::roundNumber($product->sale_price);
        $data['sub_total'] = ConvertsUtil::roundNumber($data['price'] * $request->input('quantity'));
        $data['total'] = $this->getTotalCart();

        return response(["success" => true, "message" => __('message.cart-update-susscess'), "errors" => [],"data" => $data]);
    }

    public function shippingLocal(Request $request){
        $validator = Validator::make($request->input(), [
            'districtId' => 'required|integer'
        ],[],[
            'districtId' => __('cart.districtId')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $city = City::where('id', '=', $request->input('districtId'))->get()->first();
        if(empty($city)){
            return response(["success" => false, "message" => __('message.cart-no-district'), "errors" => [],"data" => []]);
        }
        $cart = Cart::content();
        if(empty($cart)){
            return response(["success" => false, "message" => __('message.cart-empty'), "errors" => [],"data" => []]);
        }
        $productIds = [];
        foreach($cart as $item){
           $productIds[] = $item->id;
        }
        $products = Product::whereIn('id', $productIds)->get();
        if(empty($products)){
            return response(["success" => false, "message" => __('message.cart-no-product'), "errors" => [],"data" => []]);
        }
        $totalWeight = 0;
        foreach($products as $product){
            $totalWeight += $product->weight;
        }
        $data['shipping_fee'] = ConvertsUtil::roundNumber($totalWeight * $city->yhl_fee);
        $data['total'] = $this->getTotalCart() + $data['shipping_fee'];

        return response(["success" => true, "message" => __('message.cart-get-shippingfee-susscess'), "errors" => [],"data" => $data]);
    }

    public function createOrder(Request $request){
        $validator = Validator::make($request->input(), [
            "BuyerName" => "required|string",
            "BuyerPhone" => "required|string",
            "BuyerEmail" => "required|string",
            "BuyerAddress" => "required|string",
            "BuyerCountryId" => "required|integer",
            "BuyerProvinceId" => "required|integer",
            "BuyerDistrictId" => "required|integer",
            "ReceiverName" => "required|string",
            "ReceiverPhone" => "required|string",
            "ReceiverEmail" => "required|string",
            "ReceiverAddress" => "required|string",
            "ReceiverCountryId" => "required|integer",
            "ReceiverProvinceId" => "required|integer",
            "ReceiverDistrictId" => "required|integer",
            "ReceiverWardId" => 'requiredIf:ReceiverCountryId,91',
        ],[],[
            'BuyerName' => __('cart.BuyerName'),
            'BuyerPhone' => __('cart.BuyerPhone'),
            'BuyerEmail' => __('cart.BuyerEmail'),
            'BuyerAddress' => __('cart.BuyerAddress'),
            'BuyerCountryId' => __('cart.BuyerCountryId'),
            'BuyerProvinceId' => __('cart.BuyerProvinceId'),
            'BuyerDistrictId' => __('cart.BuyerDistrictId'),
            'ReceiverName' => __('receiver.first_name') . ' '. __('receiver.first_name'),
            'ReceiverPhone' => __('receiver.telephone'),
            'ReceiverEmail' => __('receiver.email'),
            'ReceiverAddress' => __('receiver.address'),
            'ReceiverCountryId' => __('receiver.country_id'),
            'ReceiverProvinceId' => __('receiver.province_id'),
            'ReceiverDistrictId' => __('receiver.city_id'),
            'ReceiverWardId' => __('receiver.ward_id')
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->messages();
            $message='';
            foreach ($errors as $value){
                $message.=$value[0];
              }
            return response(["success" => false, "message" => $message, "errors" => $validator->errors(),"data" => []]);
        }
        $cart = Cart::content();
        if(count($cart) == 0){
            return response(["success" => false, "message" => __('message.cart-empty'), "errors" => [],"data" => []]);
        }
        $receiverDistrict = City::find($request->input('ReceiverDistrictId'));
        if(empty($receiverDistrict)){
            return response(["success" => false, "message" => __('message.cart-empty'), "errors" => [],"data" => []]);
        }
        // Kh
        try {
            DB::beginTransaction();
            $customer = Auth::user();
            if(empty($customer)){
                $customer = Customer::where('email', '=', $request->input('BuyerEmail'))->get()->first();
                if(empty($customer)){
                    $customer = new Customer();
                    $customer->email = $request->input('BuyerEmail');
                    $customer->telephone = $request->input('BuyerPhone');
                    $customer->address_1 = $request->input('BuyerAddress');
                    $customer->city_id = $request->input('BuyerDistrictId');
                    $customer->province_id = $request->input('BuyerProvinceId');
                    $customer->country_id = $request->input('BuyerCountryId');
                    $customer->customer_group_id = 1;
                    $customer->status = CustomerEnum::STATUS_DEACTIVE;
                    $customer->save();

                    $customer->code = 'KH_' . $customer->id;
                    $customer->update();
                }
            }
            //  Địa chỉ mua
            $queryBuyer = Address::where('customer_id', '=', $customer->id);
            $queryBuyer->where('telephone', '=', $customer->telephone);
            $queryBuyer->where('address_1', '=', $customer->address_1);
            $queryBuyer->where('city_id', '=', $customer->city_id);
            $queryBuyer->where('province_id', '=', $customer->province_id);
            $queryBuyer->where('country_id', '=', $customer->country_id);

            $addressBuyer = $queryBuyer->get()->first();
            if(empty($addressBuyer)){
                $addressBuyer = new Address();
                $addressBuyer->customer_id = $customer->id;
                $addressBuyer->telephone = $customer->telephone;
                $addressBuyer->address_1 = $customer->address_1;
                $addressBuyer->city_id = $customer->city_id;
                $addressBuyer->province_id = $customer->province_id;
                $addressBuyer->country_id = $customer->country_id;
                $addressBuyer->save();
            }
            //  Địa chỉ nhận
            $queryReceiver = Address::where('customer_id', '=', $customer->id);
            $queryReceiver->where('telephone', '=', $customer->telephone);
            $queryReceiver->where('address_1', '=', $customer->address_1);
            $queryReceiver->where('city_id', '=', $customer->city_id);
            $queryReceiver->where('province_id', '=', $customer->province_id);
            $queryReceiver->where('country_id', '=', $customer->country_id);

            $addressReceiver = $queryReceiver->get()->first();
            if(empty($addressReceiver)){
                $addressReceiver = new Address();
                $addressReceiver->customer_id = $customer->id;
                $addressReceiver->telephone = $customer->telephone;
                $addressReceiver->address_1 = $customer->address_1;
                $addressReceiver->city_id = $customer->city_id;
                $addressReceiver->province_id = $customer->province_id;
                $addressReceiver->country_id = $customer->country_id;
                $addressReceiver->save();
            }
            $order = new Order();
            $order->customer_id = $customer->id;
            $order->sender_country_id = $request->input('BuyerCountryId');
            $order->sender_province_id = $request->input('BuyerProvinceId');
            $order->sender_city_id = $request->input('BuyerDistrictId');
            $order->sender_first_name = $request->input('BuyerName');
            $order->sender_email = $request->input('BuyerEmail');
            $order->sender_phone = $request->input('BuyerPhone');
            $order->sender_address = $request->input('BuyerAddress');
            $senderCountry = Country::where('id', $order->sender_country_id)->first();
            $order->sender_country_name = isset($senderCountry->name) ? $senderCountry->name : '';
            $senderProvince = Province::where('id', $order->sender_province_id)->first();
            $order->sender_province_name = isset($senderProvince->name) ? $senderProvince->name : '';
            $senderCity = City::where('id', $order->sender_city_id)->first();
            $order->sender_city_name = isset($senderCity->name) ? $senderCity->name : '';
            $order->receive_first_name = $request->input('ReceiverName');
            $order->receiver_email = $request->input('ReceiverEmail');
            $order->receiver_phone = $request->input('ReceiverPhone');
            $order->receiver_address = $request->input('ReceiverAddress');
            $order->receiver_country_id = $request->input('ReceiverCountryId');
            $order->receiver_province_id = $request->input('ReceiverProvinceId');
            $order->receiver_city_id = $request->input('ReceiverDistrictId');
            $order->receiver_ward_id = $request->input('ReceiverWardId');
            $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
            $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
            $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
            $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
            $receiverCity = City::where('id', $order->receiver_city_id)->first();
            $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';
            $receiverWard = Ward::where('id', $order->sender_ward_id)->first();
            $order->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
            $order->sender_address_id = $addressBuyer->id;
            $order->shipping_address_id = $addressReceiver->id; //id người nhận
            $order->dimension_unit_id = DimensionUnitEnum::INCH;
            $order->weight_unit_id = WeightUnitEnum::LBS;
            $order->currency_id = CurrencyEnum::VND;
            $order->type = OrderTypeEnum::YHL;
            $order->total_weight = 0;
            $order->total_goods_fee = 0;
            $order->total_final = 0;
            $order->shipping_fee = 0;
            $order->save();

            foreach($cart as $item){
                $product = Product::find($item->id);
                if(empty($product)){
                    continue;
                }
                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->product_id = $product->id;
                $orderItem->name = $item->name;
                $orderItem->quantity = $item->qty;
                $orderItem->unit_goods_fee = ConvertsUtil::roundNumber($product->sale_price);
                $orderItem->sub_total_weight = $item->qty * $product->weight;
                $orderItem->sub_total_goods = $orderItem->unit_goods_fee * $orderItem->quantity;
                $orderItem->sub_total_final = $orderItem->sub_total_goods;

                $orderItem->save();

                $order->total_goods_fee += $orderItem->sub_total_final;
                $order->total_weight += $orderItem->sub_total_weight;
            }
            $order->shipping_fee = ConvertsUtil::roundNumber($receiverDistrict->yhl_fee * $order->total_weight);
            $order->total_final = $order->total_goods_fee + $order->shipping_fee;
            $receiverCountry = Country::find($customer['country_id']);
            $receiverCountryName = isset($receiverCountry->code) ? $receiverCountry->code : '';
            $order->code ="P". $receiverCountryName. sprintf('%05d', $order->id) ;

            $order->update();

            Mail::send('email.create-yhl', ['order' => $order], function ($message) use ($order) {
                $message->to($order->sender_email, $order->sender_email)->subject(__('email.create-yhl'));
            });

            Cart::destroy();

            //Đẩy vào notifi
            $users = User::where('role_id', '=', 1)->get();
            if(!empty($users)){
                $this->realtimeUtil->registerChannel('notification');
                foreach($users as $user){
                    $notification = new Notification();
                    $notification->to_user_id = $user->id;
                    $notification->name = $order->code;
                    $notification->is_read = 0;
                    $notification->save();

                    $notification->path = route('yhl.customer.edit', $order->id) . '?notification_id='.$notification->id;
                    $notification->update();
                    // Đẩy vào 1 channel có sẵn
                    $this->realtimeUtil->publish(['name' => $notification->name, 'to_user_id' => $notification->to_user_id, 'path' => $notification->path], 'notification');
                }
            }

            DB::commit();
            return response(["success" => true, "message" => __('message.cart-create-order-success'), "errors" => [],"data" => route('cart.step3', $order->code)]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => __('message.cart-err-create-order'), "errors" => [],"data" => []]);
        }
    }

    public function approveOrder(Request $request){
        $validator = Validator::make($request->input(), [
            "order_code" => "required|string",
            "CustomerNote" => "nullable|string",
            "payment_method" => "required|string"
        ],[],[
            "order_code" => __('cart.order_code'),
            'CustomerNote' => __('cart.CustomerNote'),
            'payment_method' => __('cart.payment_method')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => $validator->errors(),"data" => []]);
        }
        $order = Order::where('code', '=', $request->input('order_code'))->first();
        if(empty($order)){
            return response(["success" => false, "message" => __('message.cart-no-exist-order'), "errors" => [],"data" => []]);
        }
        try {
            DB::beginTransaction();
            $order->payment_method = $request->input('payment_method');
            $order->customer_note = $request->input('CustomerNote');

            $order->update();
            DB::commit();
            return response(["success" => true, "message" => __('message.cart-choose-payment'), "errors" => [],"data" => route('cart.confirm', $order->code)]);
        }catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => __('message.cart-err-choose-payment'), "errors" => [],"data" => []]);
        }
    }

    private function addItemUpdateCart(Product $product, $quantity){
        $cart = Cart::content();
        if(!empty($cart)){
            foreach($cart as $item){
                if($item->id == $product->id){
                    $qty = $item->qty + $quantity;
                    Cart::update($item->rowId, ['price' => ConvertsUtil::roundNumber($product->sale_price), 'qty' => $qty]);
                }
            }
        }
        Cart::add($product->id, $product->name, $quantity, ConvertsUtil::roundNumber($product->sale_price),$product->weight, ['image' => $product->thumbnail, 'url' => $product->frontend_url, 'code' => $product->code]);

        return Cart::content()->count();
    }

    private function getTotalCart(){
        $cart = Cart::content();
        $total = 0;
        if(!empty($cart)){
            foreach($cart as $item){
                $sub_total = ConvertsUtil::roundNumber($item->price * $item->qty);
                $total += $sub_total;
            }
        }
        return $total;
    }

}
