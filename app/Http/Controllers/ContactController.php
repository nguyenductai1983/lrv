<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Components\LanguageUtil;
use App\CustomerContact;

class ContactController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('contact', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'address' => 'nullable|string|max:255',
            'phone_number' => 'required|string|max:20',
            'email' => 'required|email',
            'content' => 'nullable|string'
        ], [], [
            'full_name' => __('label.full_name'),
            'address' => __('label.address'),
            'phone_number' => __('label.phone_number'),
            'email' => __('label.email'),
            'content' => __('label.content')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customerContact = new CustomerContact();
            $customerContact->full_name = $data['full_name'];
            $customerContact->address = isset($data['address']) ? $data['address'] : '';
            $customerContact->phone_number = $data['phone_number'];
            $customerContact->email = $data['email'];
            $customerContact->content = isset($data['content']) ? $data['content'] : '';
            $customerContact->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        return response(["success" => true, "message" => __('message.success_sys'), "errors" => [],"data" => []]);
    }
}
