<?php

namespace App\Http\Controllers;

use App\Components\LanguageUtil;
use App\ProductGroup;
use App\Product;
use App\Enum\ProductGroupEnum;

class ProductController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['groups'] = ProductGroup::where(['service' => ProductGroupEnum::SERVICE_YHL, 'is_deleted' => 0])->orderBy('id', 'asc')->get();
        
        return view('product.index', $this->data);
    }
    
    public function category($id)
    {
        if(empty($id)){
            return redirect()->route('product.index');
        }
        $productGroup = ProductGroup::find($id);
        if(empty($productGroup)){
            return redirect()->route('product.index');
        }
        if($productGroup->is_deleted){
            return redirect()->route('product.index');
        }
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['products'] = Product::where(['product_group_id' => $id, 'is_deleted' => 0])->orderBy('id', 'asc')->get();
        
        return view('product.category', $this->data);
    }
    
    public function detail($name, $id)
    {
        if(empty($id)){
            return redirect()->route('product.index');
        }
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $product = Product::find($id);
        if(empty($product)){
            return redirect()->route('product.index');
        }
        if($product->is_deleted){
            return redirect()->route('product.index');
        }
        $this->data['groups'] = ProductGroup::where(['service' => ProductGroupEnum::SERVICE_YHL, 'is_deleted' => 0])->orderBy('id', 'asc')->get();
        $groupIds = [];
        foreach($this->data['groups'] as $group){
            $groupIds[] = $group->id;
        }
        $this->data['product'] = $product;
        $this->data['other_products'] = Product::where('product_group_id', '!=', $id)->where('is_deleted', '=', 0)->whereIn('product_group_id', $groupIds)->limit(10)->orderBy('id', 'asc')->get();
        return view('product.detail', $this->data);
    }
}
