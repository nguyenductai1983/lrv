<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config;


class EstimateController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['customer'] = auth()->user();
        
        return view('estimate', $this->data);
    }
}
