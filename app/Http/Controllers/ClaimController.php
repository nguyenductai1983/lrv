<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Components\RealtimeUtil;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\CustomerClaim;
use App\CustomerClaimConversation;
use App\Notification;
use App\User;

class ClaimController extends Controller {

    protected $realtimeUtil;

    private $data;

    public function __construct(RealtimeUtil $realtimeUtil) {
        $this->realtimeUtil = $realtimeUtil;
        $this->data = [
            'menu' => '7.0'
        ];
    }

    public function index(Request $request) {
        $query = CustomerClaim::where([]);
        $customer = auth()->user();
        $query->where('customer_id', '=', $customer->id);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        $this->data['claims'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('claim.index', $this->data);
    }

    public function view($id) {
        $claim = CustomerClaim::where('id', $id)->where('customer_id', auth()->id())->first();
        if (empty($claim)) {
            return redirect()->route('claim.frontend.index');
        }
        $this->data['claimConversations'] = CustomerClaimConversation::where('customer_claim_id', $id)->where('customer_id', auth()->id())->orderBy('id', 'asc')->get();
        $this->data['claim'] = $claim;
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('claim.view', $this->data);
    }

    public function createConversation (Request $request)
    {
        $validator = Validator::make($request->get(), [
            'id' => 'required|integer',
            'comment' => 'required|string',
        ],[],[
            'id' => __('claim.id'),
            'comment' => __('claim.comment')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $customerClaimConversation = new CustomerClaimConversation();
        $customerClaimConversation->customer_claim_id = $request->get('id');
        $customerClaimConversation->customer_id = auth()->id();
        $customerClaimConversation->content = $request->get('comment');

        $customerClaimConversation->save();

        return response(["success" => true, "message" => __('message.claim-susscess'), "errors" => [],"data" => $customerClaimConversation]);
    }

    public function open (Request $request)
    {
        $validator = Validator::make($request->get(), [
            'order_id' => 'required|integer',
            'title' => 'required|string',
            'content' => 'required|string',
        ],[],[
            'order_id' => __('claim.order_id'),
            'title' => __('claim.title'),
            'content' => __('claim.content'),
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $customerClaim = new CustomerClaim();
        $customerClaim->order_id = $request->get('order_id');
        $customerClaim->customer_id = auth()->id();
        $customerClaim->title = $request->get('title');
        $customerClaim->content = $request->get('content');
        $customerClaim->save();

        $customerClaim->code = "CLAIM_". $customerClaim->id;
        $customerClaim->update();
        //Đẩy vào notifi
        $users = User::where('role_id', '=', 1)->get();
        if(!empty($users)){
            $this->realtimeUtil->registerChannel('notification');
            foreach($users as $user){
                $notification = new Notification();
                $notification->to_user_id = $user->id;
                $notification->name = $customerClaim->code;
                $notification->is_read = 0;
                $notification->save();

                $notification->path = route('admin.claim.view', $customerClaim->id) . '?notification_id='.$notification->id;
                $notification->update();
                // Đẩy vào 1 channel có sẵn
                $this->realtimeUtil->publish(['name' => $notification->name, 'to_user_id' => $notification->to_user_id, 'path' => $notification->path], 'notification');
            }
        }
        return response(["success" => true, "message" => __('message.claim-susscess'), "errors" => [],"data" => $customerClaim]);
    }
}
