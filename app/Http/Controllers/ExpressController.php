<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Modules\Express\Http\Requests\ExpressRequest;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Components\RealtimeUtil;
use App\Services\EshiperService;
// use App\Services\PointService;
use App\Services\CampaignService;
use App\Enum\EshiperEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
// use App\Enum\PointEnum;
use App\Address;
use App\City;
use App\Currency;
use App\Order;
use App\OrderItem;
use App\Province;
use App\Country;
use App\Customer;
use App\Page;
use App\Notification;
use App\User;
use App\CampaignLog;
use App\Campaign;

class ExpressController extends Controller {

    protected $realtimeUtil;

    public function __construct(RealtimeUtil $realtimeUtil) {
        $this->realtimeUtil = $realtimeUtil;
        $this->data = [
            'menu' => '3.0'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::EXPRESS);
        $query->where('customer_id', '=', auth()->id());
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('express::quote.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
}
