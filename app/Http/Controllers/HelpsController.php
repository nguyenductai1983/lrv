<?php

namespace App\Http\Controllers;

use App\Components\LanguageUtil;
use App\Page;

class HelpsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['pages'] = Page::with('translates')->where('type', '=', 4)
                        ->orderBy('level', 'asc')
                        ->get();
        return view('helps.index', $this->data);
    }
    
    public function detail($slug)
    {
        if (!$page = Page::with('translate')->where('slug', '=', $slug)->first()) {
            abort(404);
        }

        return view('helps.detail', [
            'page' => $page
        ]);
    }
}
