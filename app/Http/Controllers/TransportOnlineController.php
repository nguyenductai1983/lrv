<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ExpressRequest;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Components\RealtimeUtil;
use App\Enum\CurrencyEnum;
use App\Enum\WeightUnitEnum;
use App\Enum\DimensionUnitEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\EshiperEnum;
use App\Enum\TransportStatusEnum;
use App\Services\CampaignService;
use App\Services\OrderService;
use App\Order;
use App\OrderItem;
use App\Transport;
use App\Config;
use App\Country;
use App\Province;
use App\City;
use App\Warehouse;
use App\Address;
use App\Product;
use Exception;
// use App\CampaignLog;
// use App\Campaign;
// use App\Customer;
use App\Services\EshiperService;
use App\Page;
use App\Notification;
// use App\User;

class TransportOnlineController extends Controller
{
    protected $realtimeUtil;

    /**
     * Assign data
     */
    private $data;

    /**
     * TransportController constructor.
     */
    public function __construct(RealtimeUtil $realtimeUtil)
    {
        $this->realtimeUtil = $realtimeUtil;
        $this->data = [
            'menu' => '2.1'
        ];
    }

    private function _validate(Request $request)
    {
        $this->validate($request, [
            'sender' => 'required|array',
            //'sender.first_name' => 'required|required_without:sender.id|string|max:50',
            'sender.first_name' => 'required|string|max:50',
            'sender.middle_name' => 'nullable|string|max:50',
            'sender.last_name' => 'required|required_without:sender.id|string|max:50',
            'sender.address_1' => 'required|required_without:sender.id|string|max:35',
            'sender.telephone' => 'required|required_without:sender.id|string|max:20',
            'sender.postal_code' => 'required|required_without:sender.id|string|max:20',
            'sender.country_id' => 'required|required_without:sender.id|exists:countries,id',
            'sender.province_id' => 'required|required_without:sender.id|exists:provinces,id',
            'sender.city_id' => 'required|required_without:sender.id|exists:cities,id',
            'sender.shipping_date' => 'nullable|string',
            'sender.email' => 'sometimes|required|email',

            'pickup' => 'required|array',
            'pickup.is_schedule' => 'required|string|in:1,2',
            'pickup.contact_name' => 'required_if:pickup.is_schedule,2',
            'pickup.phone_number' => 'required_if:pickup.is_schedule,2',
            'pickup.location' => 'required_if:pickup.is_schedule,2',
            'pickup.date_time' => 'required_if:pickup.is_schedule,2',
            'pickup.start_hour_time' => 'required_if:pickup.is_schedule,2',
            'pickup.start_minute_time' => 'required_if:pickup.is_schedule,2',
            'pickup.closing_hour_time' => 'required_if:pickup.is_schedule,2',
            'pickup.closing_minute_time' => 'required_if:pickup.is_schedule,2',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:35',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|',
            'receiver.cellphone' => 'nullable|string|',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',
            'receiver.email' => 'nullable|email|max:50',
            'warehouse_id' => 'required|integer|exists:tbl_warehouse,id',

            'containers' => 'required|array',
            'containers.*.length' => 'nullable|numeric|min:0',
            'containers.*.width' => 'nullable|numeric|min:0',
            'containers.*.height' => 'nullable|numeric|min:0',
            'containers.*.shipping_fee' => 'nullable|numeric|min:0',
            'containers.*.total_weight' => 'required|numeric|min:0',
            'containers.*.total_shipping_fee' => 'required|numeric|min:0',
            'containers.*.total_declared_value' => 'required|numeric|min:0',
            'containers.*.total_surcharge' => 'required|numeric|min:0',
            'containers.*.total_insurance' => 'required|numeric|min:0',
            'containers.*.total' => 'required|numeric|min:0',
            'containers.*.products' => 'required|array',
            'containers.*.products.*.code' => 'required|string',
            'containers.*.products.*.name' => 'required|string',
            'containers.*.products.*.by_weight' => 'required|in:0,1',
            'containers.*.products.*.quantity' => 'required|numeric|min:0',
            'containers.*.products.*.weight' => 'required|numeric|min:0',
            'containers.*.products.*.price' => 'required|numeric|min:0',
            'containers.*.products.*.declared_value' => 'required|numeric|min:0',
            'containers.*.products.*.surcharge' => 'nullable|numeric|min:0',
            'containers.*.products.*.insurance' => 'nullable|numeric|min:0',
            'containers.*.products.*.total' => 'required|numeric|min:0',
            'containers.*.products.*.note' => 'nullable|string|max:255'
        ], [
            'required' => __('validation.required'),
            'string'  => __('validation.string'),
            'numeric'  => __('validation.numeric'),
            'alpha_num' => __('validation.alpha_num')
        ], [
            'sender.first_name' => __('receiver.first_name'),
            'sender.middle_name' => __('receiver.middle_name'),
            'sender.last_name' => __('receiver.last_name'),
            'sender.address_1' => __('receiver.address'),
            'sender.telephone' => __('receiver.telephone'),
            'sender.postal_code' => __('receiver.postal_code'),
            'sender.country_id' => __('receiver.country_id'),
            'sender.province_id' => __('receiver.province_id'),
            'sender.city_id' => __('receiver.city_id'),
            'sender.shipping_date' => __('receiver.shipping_date'),
            'sender.shipping_date' => __('customer.email'),

            'pickup' => __('express.pickup_info'),
            'pickup.contact_name' =>  __('express.contact_name'),
            'pickup.phone_number' => __('express.phone_number'),
            'pickup.location' => __('express.pickup_location'),
            'pickup.date_time' => __('express.pickup_date'),
            'pickup.start_hour_time' => __('express.pickup_hour'),
            'pickup.start_minute_time' => __('express.pickup_minute'),
            'pickup.closing_hour_time' => __('express.closing_hour'),
            'pickup.closing_minute_time' => __('express.closing_minute'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),
            'receiver.email' => __('express.email'),
            'warehouse_id' => __('label.warehouse_id'),

            'containers' => __('label.containers'),
            'containers.*.length' => __('label.length'),
            'containers.*.width' => __('label.width'),
            'containers.*.height' => __('label.height'),
            'containers.*.shipping_fee' => __('label.shipping_fee'),
            'containers.*.total_weight' => __('label.total_weight'),
            'containers.*.total_shipping_fee' => __('label.total_shipping_fee'),
            'containers.*.total_declared_value' => __('label.total_declared_value'),
            'containers.*.total_surcharge' => __('label.total_surcharge'),
            'containers.*.total_insurance' => __('label.total_insurance'),
            'containers.*.total' => __('label.total'),

            'containers.*.products' => __('label.products'),
            'containers.*.products.*.product_id' => __('label.product_id'),
            'containers.*.products.*.code' => __('product.code'),
            'containers.*.products.*.name' => __('product.name'),
            'containers.*.products.*.by_weight' => __('product.by_weight'),
            'containers.*.products.*.quantity' => __('label.quantity_short'),
            'containers.*.products.*.weight' => __('label.physical_weight'),
            'containers.*.products.*.price' => __('label.price'),
            'containers.*.products.*.declared_value' => __('label.declared_value_short'),
            'containers.*.products.*.surcharge' => __('label.surcharge'),
            'containers.*.products.*.insurance' => __('label.insurance_short'),
            'containers.*.products.*.total' => __('label.total'),
            'containers.*.products.*.note' => __('label.note')
        ]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Transport::where([]);
        $customer = auth()->user();
        $query->where('customer_id', '=', $customer->id);
        $query->where('transport_status', '=', TransportStatusEnum::STATUS_NEW);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('complete'))) {
            $this->data['complete'] = 'alert alert-success';
        }
        else
        {
            $this->data['complete'] = 'hidden';
        }
        $this->data['transports'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['caPriority'] = config('app.ca_priority');
        $this->data['vnPriority'] = config('app.vn_priority');
        $this->data['caVnPriorityFee'] = config('app.ca_priority_vn_priority_fee');
        $this->data['caVnPriorityDefaultFee'] = config('app.ca_priority_vn_default_fee');
        $this->data['caVnDefaultPriorityFee'] = config('app.ca_default_vn_priority_fee');
        $this->data['caVnDefaultFee'] = config('app.ca_default_vn_default_fee');
        $this->data['senderCountryId'] = config('app.sender_country_id');
        $this->data['receiverCountryId'] = config('app.receiver_country_id');
        $this->data['payMethod'] = config('app.pay_method');
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['discountLevel'] = config('app.discount_level');
        $slug = 66;
        if (!$page = Page::with('translate')->where('id', '=', $slug)->first()) {
            $page='';
        }
         $this->data['termagree']=$page;
        return view('transport.online.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);
         $code_custom='ONL';
        try {
            DB::beginTransaction();
            $data = $request->all();
            $sender = $data['sender'];
            $receiver = $data['receiver'];
            if (isset($data['containers']) && !empty($data['containers'])) {
                $configSurcharge = Config::find(config('app.config_surcharge'));
                $configInsurance = Config::find(config('app.config_insurance'));
                $transport = new Transport();
                $transport->weight_unit_id = WeightUnitEnum::LBS;
                $transport->currency_id = CurrencyEnum::CAD;
                $transport->warehouse_id = $data['warehouse_id'];
                $transport->transport_status = 1;

                $transport->sender_first_name = $sender['first_name'];
                $transport->sender_middle_name = $sender['middle_name'];
                $transport->sender_last_name = $sender['last_name'];
                $transport->sender_phone = $sender['telephone'];
                $transport->sender_cellphone = $sender['cellphone'];
                $transport->sender_address = $sender['address_1'];
                $transport->sender_address_2 = $sender['address_2'];
                $transport->sender_email = $sender['email'];
                $transport->sender_post_code = $sender['postal_code'];
                $transport->sender_city_id = $sender['city_id'];
                $transport->sender_province_id = $sender['province_id'];
                $transport->sender_country_id = $sender['country_id'];
                if(!empty($sender['shipping_date'])){
                    $transport->shipping_date = ConvertsUtil::strToDateFormat($sender['shipping_date']);
                }
                $transport->receive_first_name = $receiver['first_name'];
                $transport->receiver_middle_name = $receiver['middle_name'];
                $transport->receive_last_name = $receiver['last_name'];
                $transport->receiver_phone = $receiver['telephone'];
                $transport->receiver_cellphone = $receiver['cellphone'];
                $transport->receiver_address = $receiver['address_1'];
                $transport->receiver_address_2 = $receiver['address_2'];
                $transport->receiver_post_code = $receiver['postal_code'];
                $transport->receiver_ward_id = $receiver['ward_id'];
                $transport->receiver_city_id = $receiver['city_id'];
                $transport->receiver_province_id = $receiver['province_id'];
                $transport->receiver_country_id = $receiver['country_id'];
                $transport->carrer_quotes = json_encode($data['quotes']);
                $transport->carrer_quote = !empty($data['quote']) ? json_encode($data['quote']) : '';
                $transport->foreign_fee = isset($data['quote']['totalCharge']) ? $data['quote']['totalCharge'] : 0;
                $transport->pickup = json_encode($data['pickup']);
                $agency='';

                if(isset($data['agency']['checkbox'])){
                foreach (array_keys($data['agency']['checkbox']) as $item){
                    if(!empty($data['agency']['info'][$item])){
                    $agency.= $item.': '.$data['agency']['info'][$item].', ';
                 }  }  }

                $transport->customer_note=$agency;
                if(!empty($data['quote']['transitDays']) && !empty($transport->shipping_date)){
                    $transport->receiver_at = ConvertsUtil::dateAddDay($transport->shipping_date, $data['quote']['transitDays'], 'Y-m-d');
                    $transport->shipping_from_at = ConvertsUtil::dateAddDay($transport->receiver_at, 7);
                    $transport->shipping_to_at = ConvertsUtil::dateAddDay($transport->receiver_at, 14);
                }
                $transport->save();

                $transport_total_weight = 0;
                $transport_total_declare_price = 0;
                $transport_total_surcharge_fee = 0;
                $transport_total_insurrance_fee = 0;
                $transport_total_shipping_fee = 0;
                $transport_total_final = 0;

                $count_container = 1;
                foreach ($data['containers'] as $con) {
                    $order = new Order();
                    $order->customer_id = null;
                    $order->currency_id = CurrencyEnum::CAD;
                    $order->dimension_unit_id = DimensionUnitEnum::INCH;
                    $order->weight_unit_id = WeightUnitEnum::LBS;
                    $order->transport_id = $transport->id;

                    $order->sender_first_name = $sender['first_name'];
                    $order->sender_middle_name = $sender['middle_name'];
                    $order->sender_last_name = $sender['last_name'];
                    $order->sender_phone = $sender['telephone'];
                    $order->sender_address = $sender['address_1'];
                    $order->sender_email = $sender['email'];
                    $order->sender_country_id = $sender['country_id'];
                    $order->sender_province_id = $sender['province_id'];
                    $order->sender_city_id = $sender['city_id'];
                    $order->sender_post_code = $sender['postal_code'];
                    $order->sender_cellphone = $sender['cellphone'];

                    $order->receive_last_name = $receiver['last_name'];
                    $order->receive_first_name = $receiver['first_name'];
                    $order->receiver_middle_name = $receiver['middle_name'];
                    $order->receiver_phone = $receiver['telephone'];
                    $order->receiver_address = $receiver['address_1'];
                    $order->receiver_address_2 = $receiver['address_2'];
                    $order->receiver_country_id = $receiver['country_id'];
                    $order->receiver_province_id = $receiver['province_id'];
                    $order->receiver_city_id = $receiver['city_id'];
                    $order->receiver_ward_id = $receiver['ward_id'];
                    $order->receiver_cellphone = $receiver['cellphone'];
                    $order->receiver_post_code = $receiver['postal_code'];

                    $order->save();
                    $total_weight = 0;
                    $total_goods = 0;
                    $total_discount = 0;
                    $total_declare_price = 0;
                    $total_surcharge_fee = 0;
                    $total_insurrance_fee = 0;
                    $total_final = 0;
                    $description = "";
                    $products_code = "";
                    $user_note = "";
                    foreach ($con['products'] as $product) {
                        $source = Product::where('id', '=', $product['product_id'])->first();
                        if(empty($source)){
                            continue;
                        }
                        $quantity = 0;
                        $orderItem = new OrderItem();
                        $orderItem->name = $product['name'];
                        $orderItem->description = $product['description'];
                        $orderItem->order_id = $order->id;
                        $orderItem->product_id = $product['product_id'];
                        $orderItem->quantity = $product['quantity'];
                        if (!empty($product['note'])) {
                            $orderItem->user_note = $product['note'];
                            $user_note .= $orderItem->user_note . "; ";
                        }
                        $orderItem->unit_goods_fee = $product['price'];
                        $total_weight += $orderItem->sub_total_weight = $product['weight'];
                        if (isset($product['by_weight']) && $product['by_weight'] == 1) {
                            $quantity = $product['weight'];
                        } else {
                            $quantity = $product['quantity'];
                        }
                        $total_goods += $orderItem->sub_total_goods = $product['price'] * $quantity;
                        $total_declare_price += $orderItem->sub_total_declare_price = $product['declared_value'];
                        $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                        $orderItem->per_discount = $discount;
                        $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);
                        $quota = $product['declared_value'] / $quantity;
                        if ($quota > $configSurcharge->quota) {
                            $total_surcharge_fee += $orderItem->sub_total_surcharge_fee = $product['declared_value'] * ($configSurcharge->percent / 100);
                        } else {
                            $orderItem->sub_total_surcharge_fee = 0;
                        }
                        if (isset($product['is_insurance']) && $product['is_insurance'] == 1) {
                            $total_insurrance_fee += $orderItem->sub_total_insurrance_fee = $product['declared_value'] * ($configInsurance->percent / 100);
                            $orderItem->is_insurance = 1;
                        } else {
                            $orderItem->sub_total_insurrance_fee = 0;
                        }
                        $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                        $total_final += $orderItem->sub_total_goods - $orderItem->sub_total_discount;

                        $description .= $product['quantity'] . ' ' . $product['name'] . '; ';
                        $products_code .= $product['code'] . '; ';
                        $orderItem->messure_unit_id = $source->messure_unit_id;
                        $orderItem->save();
                    }

                    $order->description = $description;
                    $order->user_note = $user_note;
                    $order->products = $products_code;
                    $order->length = $con['length'];
                    $order->width = $con['width'];
                    $order->height = $con['height'];
                    $order->receive_status = $con['status'];
                    $order->total_weight = $total_weight;
                    $order->shipping_fee = $con['shipping_fee'];
                    $order->total_shipping_fee = $order->total_weight * $order->shipping_fee;
                    $order->total_declare_price = $total_declare_price;
                    $order->total_surcharge_fee = $total_surcharge_fee;
                    $order->total_insurrance_fee = $total_insurrance_fee;
                    $order->total_discount = $total_discount;
                    $order->total_goods_fee = $total_goods;

                    $totalFee = $order->total_shipping_fee + $order->total_goods_fee;
                    $minFee = OrderService::getMinFee(true, $order->receiver_province_id);
                    if($minFee > $totalFee){
                        $order->total_final = $minFee + $order->total_surcharge_fee + $order->total_insurrance_fee - $order->total_discount;
                    }else{
                        $order->total_final = $totalFee + $order->total_surcharge_fee + $order->total_insurrance_fee - $order->total_discount;
                    }

                    // Kiểm tra mã coupon
                    // if(!empty($con['coupon_code'])){
                    //     $amount = CampaignService::getAmountCoupon($con['coupon_code'], $order->customer_id, $order->total_weight);
                    //     if($amount > 0){
                    //         $order->coupon_code = $con['coupon_code'];
                    //         $order->coupon_amount = $amount;
                    //         $order->coupon_time = date("Y-m-d H:i:s");

                    //         $order->total_final -= $order->coupon_amount;
                    //         if($order->total_final < 0){
                    //             $order->total_final = 0;
                    //         }
                    //         $campaignLog = new CampaignLog();
                    //         $campaignLog->customer_id = $order->customer_id;
                    //         $campaignLog->order_id = $order->id;
                    //         $campaignLog->coupon_code = $order->coupon_code;
                    //         $campaignLog->coupon_amount = $amount;
                    //         $campaignLog->weight = $order->total_weight;
                    //         $campaignLog->save();
                    //         $campaign = Campaign::where('code', '=', $con['coupon_code'])->first();
                    //         if(!empty($campaign)){
                    //             $numberUse = Order::where('coupon_code', '=', $con['coupon_code'])->count();
                    //             if($numberUse + 1 >= $campaign->num_of_use){
                    //                 $campaign->is_used = 1;
                    //                 $campaign->update();
                    //             }
                    //         }
                    //     }
                    // }
                    $countryReceiver = Country::find($receiver['country_id']);
                    $countryCode = isset($countryReceiver->code) && !empty($countryReceiver->code) ? $countryReceiver->code : 'VN';
                    $order->code = $code_custom . sprintf('%05d', $transport->id) . "S". $countryCode . $count_container . count($data['containers']);

                    $order->box_number = $count_container;
                    $order->update();
                    $count_container++;

                    $transport_total_weight += $order->total_weight;
                    $transport_total_declare_price += $order->total_declare_price;
                    $transport_total_surcharge_fee += $order->total_surcharge_fee;
                    $transport_total_insurrance_fee += $order->total_insurrance_fee;
                    $transport_total_shipping_fee += $order->total_shipping_fee;
                    $transport_total_final += $order->total_final;
                }
                $transport->total_weight = $transport_total_weight;
                $transport->total_shipping_fee = $transport_total_shipping_fee;
                $transport->total_declare_price = $transport_total_declare_price;
                $transport->total_surcharge_fee = $transport_total_surcharge_fee;
                $transport->total_insurrance_fee = $transport_total_insurrance_fee;
                $transport->total_goods_fee = 0;
                $transport->international_fee = $transport_total_final;
                $transport->total_final = $transport->international_fee + $transport->foreign_fee;
                $transport->code = $code_custom . sprintf('%05d', $transport->id) . "S" .  $countryCode;
                $transport->update();
                //Đẩy vào notifi

                    $this->realtimeUtil->registerChannel('notification');
                        $notification = new Notification();
                        $notification->to_user_id = null;
                        $notification->name = $transport->code;
                        $notification->is_read = 0;
                        $notification->save();

                        $notification->path = route('transport.quote.edit', $transport->id) . '?notification_id='.$notification->id;
                        $notification->update();
                        // Đẩy vào 1 channel có sẵn
                        $this->realtimeUtil->publish(['name' => $notification->name, 'to_user_id' => $notification->to_user_id, 'path' => $notification->path], 'notification');



            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $transport = Transport::where('id', '=', $id)->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        $data = $this->convertTransport($transport);
        return response([
            'transport' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $transport = Transport::where('id', '=', $id)->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        $this->data['transport'] = $transport;
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['caPriority'] = config('app.ca_priority');
        $this->data['vnPriority'] = config('app.vn_priority');
        $this->data['caVnPriorityFee'] = config('app.ca_priority_vn_priority_fee');
        $this->data['caVnPriorityDefaultFee'] = config('app.ca_priority_vn_default_fee');
        $this->data['caVnDefaultPriorityFee'] = config('app.ca_default_vn_priority_fee');
        $this->data['caVnDefaultFee'] = config('app.ca_default_vn_default_fee');
        $this->data['discountLevel'] = config('app.discount_level');
        return view('transport.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $transport = Transport::where('id', '=', $request->route('id'))->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();
            $data = $request->all();
            $sender = $data['sender'];
            $receiver = $data['receiver'];
            $customer = auth()->user();
            if (isset($data['containers']) && !empty($data['containers'])) {
                $configSurcharge = Config::find(config('app.config_surcharge'));
                $configInsurance = Config::find(config('app.config_insurance'));

                $transport->sender_first_name = $sender['first_name'];
                $transport->sender_middle_name = $sender['middle_name'];
                $transport->sender_last_name = $sender['last_name'];
                $transport->sender_phone = $sender['telephone'];
                $transport->sender_address = $sender['address_1'];
                $transport->sender_email = $customer->email;
                $transport->sender_post_code = $sender['postal_code'];
                $transport->sender_city_id = $sender['city_id'];
                $transport->sender_province_id = $sender['province_id'];
                $transport->sender_country_id = $sender['country_id'];
                if(!empty($sender['shipping_date'])){
                    $transport->shipping_date = ConvertsUtil::strToDateFormat($sender['shipping_date']);
                }

                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }else{
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->update();
                        }
                    } else {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    }
                }
                $transport->receive_first_name = $receiver['first_name'];
                $transport->receiver_middle_name = $receiver['middle_name'];
                $transport->receive_last_name = $receiver['last_name'];
                $transport->receiver_phone = $receiver['telephone'];
                $transport->receiver_cellphone = $receiver['cellphone'];
                $transport->receiver_address = $receiver['address_1'];
                $transport->receiver_address_2 = $receiver['address_2'];
                $transport->receiver_post_code = $receiver['postal_code'];
                $transport->receiver_city_id = $receiver['city_id'];
                $transport->receiver_province_id = $receiver['province_id'];
                $transport->receiver_country_id = $receiver['country_id'];
                $transport->carrer_quotes = json_encode($data['quotes']);
                $transport->carrer_quote = !empty($data['quote']) ? json_encode($data['quote']) : '';
                $transport->foreign_fee = isset($data['quote']['totalCharge']) ? $data['quote']['totalCharge'] : 0;
                $transport->pickup = json_encode($data['pickup']);
                $transport->sender_at = date('Y-m-d H:i:s');
                $transport->warehouse_id = $data['warehouse_id'];

                if(!empty($data['quote']['transitDays']) && !empty($transport->shipping_date)){
                    $transport->receiver_at = ConvertsUtil::dateAddDay($transport->shipping_date, $data['quote']['transitDays'], 'Y-m-d');
                    $transport->shipping_from_at = ConvertsUtil::dateAddDay($transport->receiver_at, 7);
                    $transport->shipping_to_at = ConvertsUtil::dateAddDay($transport->receiver_at, 14);
                }
                $transport->update();

                $transport_total_weight = 0;
                $transport_total_declare_price = 0;
                $transport_total_surcharge_fee = 0;
                $transport_total_insurrance_fee = 0;
                $transport_total_shipping_fee = 0;
                $transport_total_final = 0;

                $count_container = 1;
                foreach ($data['containers'] as $con) {
                    if(isset($con['id']) && !empty($con['id'])){
                        $order = Order::find($con['id']);
                        if(empty($order)){
                            return response('Internal Server Error', 500);
                        }
                    }else{
                        $order = new Order();
                    }
                    $order->customer_id = $customer->id;
                    $order->currency_id = CurrencyEnum::CAD;
                    $order->dimension_unit_id = DimensionUnitEnum::INCH;
                    $order->weight_unit_id = WeightUnitEnum::LBS;
                    $order->transport_id = $transport->id;

                    $order->sender_first_name = $sender['first_name'];
                    $order->sender_middle_name = $sender['middle_name'];
                    $order->sender_last_name = $sender['last_name'];
                    $order->sender_phone = $sender['telephone'];
                    $order->sender_address = $sender['address_1'];
                    $order->sender_address_2 = $sender['address_2'];
                    $order->sender_email = $customer->email;
                    $order->sender_country_id = $sender['country_id'];
                    $order->sender_province_id = $sender['province_id'];
                    $order->sender_city_id = $sender['city_id'];
                    $order->sender_post_code = $sender['postal_code'];

                    $order->receive_last_name = $receiver['last_name'];
                    $order->receive_first_name = $receiver['first_name'];
                    $order->receiver_middle_name = $receiver['middle_name'];
                    $order->receiver_phone = $receiver['telephone'];
                    $order->receiver_address = $receiver['address_1'];
                    $order->receiver_address_2 = $receiver['address_2'];
                    $order->receiver_country_id = $receiver['country_id'];
                    $order->receiver_province_id = $receiver['province_id'];
                    $order->receiver_city_id = $receiver['city_id'];
                    $order->receiver_cellphone = $receiver['cellphone'];
                    $order->receiver_post_code = $receiver['postal_code'];

                    $order->save();

                    $total_weight = 0;
                    $total_goods = 0;
                    $total_discount = 0;
                    $total_declare_price = 0;
                    $total_surcharge_fee = 0;
                    $total_insurrance_fee = 0;
                    $total_final = 0;
                    $description = "";
                    $products_code = "";
                    $user_note = "";
                    foreach ($con['products'] as $product) {
                        $quantity = 0;
                        $source = Product::where('id', '=', $product['product_id'])->first();
                        if(empty($source)){
                            continue;
                        }
                        if (isset($product['id']) && !empty($product['id'])) {
                            $orderItem = OrderItem::find($product['id']);
                        } else {
                            $orderItem = new OrderItem();
                        }
                        $orderItem->name = $product['name'];
                        $orderItem->description = $source->description;
                        $orderItem->order_id = $order->id;
                        $orderItem->product_id = $product['product_id'];
                        $orderItem->quantity = $product['quantity'];
                        if (!empty($product['note'])) {
                            $orderItem->user_note = $product['note'];
                            $user_note .= $orderItem->user_note . "; ";
                        }
                        $orderItem->unit_goods_fee = $source->sale_price;
                        $total_weight += $orderItem->sub_total_weight = $product['weight'];
                        if (isset($product['by_weight']) && $product['by_weight'] == 1) {
                            $quantity = $product['weight'];
                        } else {
                            $quantity = $product['quantity'];
                        }
                        $total_goods += $orderItem->sub_total_goods = $source->sale_price * $quantity;
                        $total_declare_price += $orderItem->sub_total_declare_price = $product['declared_value'];
                        $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                        $orderItem->per_discount = $discount;
                        $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);
                        $quota = $product['declared_value'] / $quantity;
                        if ($quota > $configSurcharge->quota) {
                            $total_surcharge_fee += $orderItem->sub_total_surcharge_fee = $product['declared_value'] * ($configSurcharge->percent / 100);
                        } else {
                            $orderItem->sub_total_surcharge_fee = 0;
                        }
                        if (isset($product['is_insurance']) && $product['is_insurance'] == 1) {
                            $total_insurrance_fee += $orderItem->sub_total_insurrance_fee = $product['declared_value'] * ($configInsurance->percent / 100);
                            $orderItem->is_insurance = 1;
                        } else {
                            $orderItem->sub_total_insurrance_fee = 0;
                        }
                        $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                        $total_final += $orderItem->sub_total_goods - $orderItem->sub_total_discount;

                        $description .= $product['quantity'] . ' ' . $product['name'] . '; ';
                        $products_code .= $product['code'] . '; ';
                        $orderItem->messure_unit_id = $source->messure_unit_id;
                        $orderItem->save();
                    }

                    $order->description = $description;
                    $order->user_note = $user_note;
                    $order->products = $products_code;
                    $order->length = $con['length'];
                    $order->width = $con['width'];
                    $order->height = $con['height'];
                    $order->total_weight = $con['total_weight'];
                    $order->shipping_fee = $con['shipping_fee'];
                    $order->total_shipping_fee = $order->total_weight * $order->shipping_fee;
                    $order->total_declare_price = $total_declare_price;
                    $order->total_surcharge_fee = $total_surcharge_fee;
                    $order->total_insurrance_fee = $total_insurrance_fee;
                    $order->total_discount = $total_discount;
                    $order->total_goods_fee = $total_goods;

                    $totalFee = $order->total_shipping_fee + $order->total_goods_fee;
                    $minFee = OrderService::getMinFee(true, $order->receiver_province_id);
                    if($minFee > $totalFee){
                        $order->total_final = $minFee + $order->total_surcharge_fee + $order->total_insurrance_fee - $order->total_discount;
                    }else{
                        $order->total_final = $totalFee + $order->total_surcharge_fee + $order->total_insurrance_fee - $order->total_discount;
                    }
                    $order->box_number = $count_container;
                    $order->update();

                    $count_container++;
                    $transport_total_weight += $order->total_weight;
                    $transport_total_declare_price += $order->total_declare_price;
                    $transport_total_surcharge_fee += $order->total_surcharge_fee;
                    $transport_total_insurrance_fee += $order->total_insurrance_fee;
                    $transport_total_shipping_fee += $order->total_shipping_fee;
                    $transport_total_final += $order->total_final;
                }
                $transport->total_weight = $transport_total_weight;
                $transport->total_shipping_fee = $transport_total_shipping_fee;
                $transport->total_declare_price = $transport_total_declare_price;
                $transport->total_surcharge_fee = $transport_total_surcharge_fee;
                $transport->total_insurrance_fee = $transport_total_insurrance_fee;
                $transport->total_goods_fee = 0;
                $transport->international_fee = $transport_total_final;
                $transport->total_final = $transport->international_fee + $transport->foreign_fee;
                $transport->update();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function getQuotes(ExpressRequest $request) {
        $params = $this->buildParams($request->all());
        $result = EshiperService::getQuoteEshiper($params);
        return response($result);
    }

    public function getPdf($id) {
        $transport = Transport::where('id', '=', $id)->first();
        if (empty($transport) || empty($transport->eshipper_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $transport->eshipper_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    public function viewHistoryEshipper($id) {
        $transport = Transport::where('id', '=', $id)->first();
        if(empty($transport)){
            return response(["success" => false, "message" => __('message.err_sys'), []]);
        }
        if(!empty($transport->eshiper_order_id)){
            $eshiperResponse = EshiperService::getOrder($transport->eshiper_order_id);
            if($eshiperResponse['success']){
                $transport->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                $transport->update();
            }else{
                return response(["success" => false, "message" => $eshiperResponse['message'], []]);
            }
            return response(["success" => true, "message" => __('message.get_data_success'), "data" => $eshiperResponse['data']['history']]);
        }
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => []]);
    }

    private function buildParams($request) {
        $warehouse = Warehouse::where("id", $request['warehouse_id'])->first();
        $countries = Country::where("is_active", 1)->pluck('code','id');
        $provinces = Province::where("is_active", 1)->pluck('code','id');
        $cities = City::where("is_active", 1)->pluck('name','id');
        $params['typeAttrs'] = ['serviceId' => 0, 'stackable' => true];
        if(!empty($params['sender']['shipping_date'])){
            $params['typeAttrs']['scheduledShipDate'] = ConvertsUtil::strToDateFormat($params['sender']['shipping_date']);
        }
        $params['addressFrom']['company'] = $request['sender']['last_name'] . ' ' . $request['sender']['middle_name'] . ' ' . $request['sender']['first_name'];
        $params['addressFrom']['address_1'] = $request['sender']['address_1'];
        $params['addressFrom']['address_2'] = $request['sender']['address_2'];
        $params['addressFrom']['postal_code'] = $request['sender']['postal_code'];
        $params['addressFrom']['telephone'] = $request['sender']['telephone'];
        $params['addressFrom']['state'] = isset($provinces[ $request['sender']['province_id']]) ? $provinces[ $request['sender']['province_id']] : '';
        $params['addressFrom']['country'] = isset($countries[ $request['sender']['country_id']]) ? $countries[ $request['sender']['country_id']] : '';
        $params['addressFrom']['city'] = isset($cities[ $request['sender']['city_id']]) ? $cities[ $request['sender']['city_id']] : '';

        $params['addressTo']['company'] = $warehouse->contact_person;
        $params['addressTo']['address_1'] = $warehouse->address;
        $params['addressTo']['postal_code'] = $warehouse->post_code;
        $params['addressTo']['telephone'] = $warehouse->telephone;
        $params['addressTo']['state'] = isset($provinces[$warehouse->province_id]) ? $provinces[$warehouse->province_id] : '';
        $params['addressTo']['country'] = isset($countries[$warehouse->country_id]) ? $countries[$warehouse->country_id] : '';
        $params['addressTo']['city'] = isset($cities[$warehouse->city_id]) ? $cities[$warehouse->city_id] : '';

        if(isset($request['pickup']['is_schedule']) && $request['pickup']['is_schedule'] == EshiperEnum::IS_PICKUP){
            $params['pickup'] = $request['pickup'];
            $params['pickup']['pickup_date'] = ConvertsUtil::strToDateFormat($params['pickup']['date_time']);
            $params['pickup']['pickup_time'] = $params['pickup']['start_hour_time'] .':'. $params['pickup']['start_minute_time'];
            $params['pickup']['closing_time'] = $params['pickup']['closing_hour_time'] .':'. $params['pickup']['closing_minute_time'];
        }

        $params['packages']['type'] = 3;
        $params['packages']['dimType'] = 3;
        $params['packages']['boxes'] = [];
        $quantity = 0;
        foreach($request['containers'] as $container){
            $boxes = [];
            $boxes['length'] = $container['length'];
            $boxes['width'] = $container['width'];
            $boxes['height'] = $container['height'];
            $boxes['weight'] = $container['total_weight'];
            $boxes['insuranceAmount'] = $container['total_insurance'];

            $params['packages']['boxes'][] = $boxes;
            $quantity += 1;
        }
        $params['packages']['quantity'] = $quantity;
        return $params;
    }

    private function convertTransport (Transport $transport) {
        $orders = $transport->orders()->where('order_status', '!=' ,OrderStatusEnum::STATUS_CANCEL)->get();
        $container = [];
        foreach ($orders as $order) {
            $order_items = $order->order_items()->where('is_delete', 0)->get();
            $products = [];
            foreach($order_items as $order_item){
                $product['id'] = $order_item->id;
                $product['product_id'] = $order_item->product_id;
                $product['by_weight'] = $order_item->product->by_weight;
                $product['code'] = $order_item->product->code;
                $product['name'] = $order_item->name;
                $product['quantity'] = $order_item->quantity;
                $product['weight'] = $order_item->sub_total_weight;
                $product['unit_goods_fee'] = $order_item->unit_goods_fee;
                $product['unit'] = $order_item->messure->code;
                $product['declared_value'] = $order_item->sub_total_declare_price;
                $product['surcharge'] = $order_item->sub_total_surcharge_fee;
                $product['is_insurance'] = $order_item->is_insurance == 1 ? true : false;
                $product['insurance'] = $order_item->sub_total_insurrance_fee;
                $product['per_discount'] = $order_item->per_discount;
                $product['total'] = $order_item->sub_total_final;
                $product['note'] = $order_item->description;

                $products[] = $product;
            }
            $container[] = array(
                'id' => $order->id,
                'length' => $order->length,
                'width' => $order->width,
                'height' => $order->height,
                'shipping_fee'  => $order->shipping_fee,
                'total_weight'  => $order->total_weight,
                'total_shipping_fee'  => $order->total_shipping_fee,
                'total_declared_value' => $order->total_declare_price,
                'total_surcharge' => $order->total_surcharge_fee,
                'total_insurance' => $order->total_insurrance_fee,
                'total' => $order->total_final,
                'coupon_code' => $order->coupon_code,
                'coupon_amount' => !empty($order->coupon_amount) ? $order->coupon_amount : 0,
                'products' => $products
            );
        }
        $data = [];
        $data['sender'] = array(
            'first_name' => $transport->sender_first_name,
            'middle_name' => $transport->sender_middle_name,
            'last_name' => $transport->sender_last_name,
            'telephone' => $transport->sender_phone,
            'address_1' => $transport->sender_address,
            'address_2' => $transport->sender_address_2,
            'country_id' => $transport->sender_country_id,
            'province_id' => $transport->sender_province_id,
            'city_id' => $transport->sender_city_id,
            'postal_code' => $transport->sender_post_code,
            'shipping_date' => !empty($transport->shipping_date) ? date(config('app.date_format'), strtotime($transport->shipping_date)) : '',
        );
        $data['receiver'] = array(
            'first_name' => $transport->receive_first_name,
            'middle_name' => $transport->receiver_middle_name,
            'last_name' => $transport->receive_last_name,
            'telephone' => $transport->receiver_phone,
            'cellphone' => $transport->receiver_cellphone,
            'address_1' => $transport->receiver_address,
            'address_2' => $transport->receiver_address_2,
            'country_id' => $transport->receiver_country_id,
            'province_id' => $transport->receiver_province_id,
            'city_id' => $transport->receiver_city_id,
            'postal_code' => $transport->receiver_post_code,
        );
        $data['pickup'] = json_decode($transport->pickup, true);
        $data['date_sender'] = $transport->sender_at;
        $data['date_receiver'] = $transport->receiver_at;
        $data['date_from_shipping'] = $transport->shipping_from_at;
        $data['date_to_shipping'] = $transport->shipping_to_at;
        $data['total_shipping_local'] = $transport->foreign_fee;
        $data['total_shipping_international'] = $transport->international_fee;
        $data['total_shipping'] = $transport->total_final;
        $data['eshiper_status_name'] = $transport->eshipper_status_name;
        $data['payment_status'] = $transport->payment_status_name;
        $data['transport_status'] = $transport->transport_status;
        $data['quote'] = json_decode($transport->carrer_quote);
        $data['quotes'] = json_decode($transport->carrer_quotes);
        $data['warehouse_id'] = $transport->warehouse_id;
        $data['containers'] = $container;

        return $data;
    }
}
