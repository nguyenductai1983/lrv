<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Components\LanguageUtil;
use App\File;
use App\ProductGroup;
use App\Enum\ProductGroupEnum;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $lang =app()->getLocale();
        $this->data['groups'] = ProductGroup::where(['service' => ProductGroupEnum::SERVICE_YHL, 'is_deleted' => 0])->orderBy('id', 'asc')->get();
        $this->data['banners'] = File::where(['is_banner' => 1, 'valid' => 0,'lang' => $lang])->orderBy('display_order', 'asc')->get();

        return view('index', $this->data);
    }
}
