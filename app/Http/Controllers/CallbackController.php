<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use App\ShippingPackageLocal;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\VnPostEnum;
use App\Http\Requests\VnPostRequest;
use App\Http\Controllers\Controller;
use Exception;
class CallbackController extends Controller
{
    public function vnpost(VnPostRequest $request) {
        $param = $request->all();
        $log_api='';
        $user = auth()->user();
        if(!empty($user))
        {
            $user=$user->id;
        }
        if(!isset($param['sMaDonHang']) || empty($param['sMaDonHang'])){
            return response('sMaDonHang Not Found', 500);
        }
        if(!isset($param['sMaTrangThai']) || empty($param['sMaTrangThai'])){
            return response('sMaTrangThai Not Found', 500);
        }
        $shippingPackageLocal = ShippingPackageLocal::where(['shipping_code' => $param['sMaDonHang']])->first();
        try {
            DB::beginTransaction();
            $sMaTrangThai = $this->mappingShippingLocal($param['sMaTrangThai']);
            if(!empty($sMaTrangThai)){
               if(empty($shippingPackageLocal)){
                  $log_api='PO not Select shipping';
                }
                else {
                $shippingPackageLocal->tracking_code = $param['sSoHieu'];
                $shippingPackageLocal->status = $sMaTrangThai;
                $shippingPackageLocal->updated_at = date('Y-m-d H:i:s');
                     if(is_array($param)){
                        $shippingPackageLocal->last_response_api = json_encode($param);
                                }
                $shippingPackageLocal->update();
                }
                if (empty($shippingPackageLocal->order_id))
                {
                    $orderData = Order::where(['code' => $param['sMaDonHang']])->first();
                }
                else
                {
                    $orderData = Order::find($shippingPackageLocal->order_id);
                }
                if (empty($orderData)) {
                    DB::rollBack();
                    return response('sMaDonHang Not Found', 501);
                }
                $orderData->order_status = $this->mappingStatusOrder($param['sMaTrangThai']);
                $orderData->shipping_status = $this->mappingShippingOrder($param['sMaTrangThai']);
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->tracking_code = $param['sSoHieu'];
                $orderTracking->order_code = $param['sMaDonHang'];
                $orderTracking->status = $this->mappingTrackingOrder($param['sMaTrangThai']);
                $orderTracking->note = isset($param['sGhiChu']) ? $param['sGhiChu'] : '';
                $orderTracking->log_api = $log_api;
                if(is_array($param)){
                    if(!empty($param['_token'])){
                        array_shift($param);
                    }
                    $orderTracking->response_api = json_encode($param);
                }
                $orderTracking->create_user_id=$user;
                $orderTracking->save();
                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->status = $this->mappingLogOrder($param['sMaTrangThai']);
                $orderLog->create_user_id=$user;
                $orderLog->save();

                DB::commit();
            }
            else
            {
              return response('sMaTrangThai Fail', 501);

            }
        }
        // het hàm kiểm tra shipment
        catch (Exception $e) {
            DB::rollBack();
            return response('FAIL', 501);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    private function mappingShippingLocal($partnerStatus){
        $status = (int) $partnerStatus;
        if($status == VnPostEnum::SHIPPING_STATUS_PICKUP){
            return ShippingPackageLocalEnum::DELIVERY_CARRIER;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_PICKUP && $status < VnPostEnum::SHIPPING_STATUS_CANCEL){
            return ShippingPackageLocalEnum::CARRIER_PICKUP_FAIL;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_CANCEL && $status < VnPostEnum::SHIPPING_STATUS_APPROVED){
            return ShippingPackageLocalEnum::CANCEL;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_APPROVED){
            return ShippingPackageLocalEnum::DELIVERY_CARRIER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY){
            return ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_DELIVERY && $status < VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return ShippingPackageLocalEnum::CARRIER_DELIVERY_FAIL;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
        }
        return 0;
    }

    private function mappingStatusOrder($partnerStatus){
        $status = (int) $partnerStatus;
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY){
            return OrderStatusEnum::STATUS_COMPLETE;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderStatusEnum::STATUS_COMPLETE;
        }
        return OrderStatusEnum::STATUS_PROCESSING;
    }

    private function mappingShippingOrder($partnerStatus){
        $status = (int) $partnerStatus;
        if($status == VnPostEnum::SHIPPING_STATUS_PICKUP){
            return OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_PICKUP && $status < VnPostEnum::SHIPPING_STATUS_CANCEL){
            return OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_CANCEL && $status < VnPostEnum::SHIPPING_STATUS_APPROVED){
            return OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_APPROVED){
            return OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY){
            return OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_DELIVERY && $status < VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
        }
        return 0;
    }

    private function mappingTrackingOrder($partnerStatus){
        $status = (int) $partnerStatus;
        if($status == VnPostEnum::SHIPPING_STATUS_PICKUP){
            return OrderTrackingStatusEnum::STATUS_CARRIER;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_PICKUP && $status < VnPostEnum::SHIPPING_STATUS_CANCEL){
            return OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_CANCEL && $status < VnPostEnum::SHIPPING_STATUS_APPROVED){
            return OrderTrackingStatusEnum::STATUS_REVICER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_APPROVED){
            return OrderTrackingStatusEnum::STATUS_CARRIER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY){
            return OrderTrackingStatusEnum::STATUS_DONE;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_DELIVERY && $status < VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderTrackingStatusEnum::STATUS_CARRIER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderTrackingStatusEnum::STATUS_DONE;
        }
        return 0;
    }

    private function mappingLogOrder($partnerStatus){
        $status = (int) $partnerStatus;
        if($status == VnPostEnum::SHIPPING_STATUS_PICKUP){
            return OrderLogStatusEnum::STATUS_CARRIER;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_PICKUP && $status < VnPostEnum::SHIPPING_STATUS_CANCEL){
            return OrderLogStatusEnum::STATUS_READY_TO_SHIP;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_CANCEL && $status < VnPostEnum::SHIPPING_STATUS_APPROVED){
            return OrderLogStatusEnum::STATUS_CANCEL;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_APPROVED){
            return OrderLogStatusEnum::STATUS_CARRIER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY){
            return OrderLogStatusEnum::STATUS_DONE;
        }
        if($status > VnPostEnum::SHIPPING_STATUS_DELIVERY && $status < VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderLogStatusEnum::STATUS_OTHER;
        }
        if($status == VnPostEnum::SHIPPING_STATUS_DELIVERY_AGAIN){
            return OrderLogStatusEnum::STATUS_RETURN;
        }
        return 0;
    }
}
