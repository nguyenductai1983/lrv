<?php

namespace App\Http\Controllers;

use App\Product_suppliers;
use Illuminate\Http\Request;

class ProductSuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product_suppliers  $product_suppliers
     * @return \Illuminate\Http\Response
     */
    public function show(Product_suppliers $product_suppliers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product_suppliers  $product_suppliers
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_suppliers $product_suppliers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product_suppliers  $product_suppliers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_suppliers $product_suppliers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product_suppliers  $product_suppliers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_suppliers $product_suppliers)
    {
        //
    }
}
