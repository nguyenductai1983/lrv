<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActionLogs;

class LogsController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * CountriesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '1.8'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $query = ActionLogs::where([]);

        if ($order_id = $request->query('order_id')) {
            $query->where('order_id', '=', $order_id);
        }
        
        $actionLogs = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));

        $this->data['actionLogs'] = $actionLogs;

        return view('admin.logs.index', $this->data);
    }
    
    
}
