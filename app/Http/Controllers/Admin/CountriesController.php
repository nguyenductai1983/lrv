<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;

class CountriesController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * CountriesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.7'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $countries = Country::orderBy('code', 'asc')->get();

        if ($request->ajax() || $request->wantsJson()) {
            return response([
                'countries' => $countries
            ]);
        }

        $this->data['countries'] = $countries;

        return view('admin.countries.index', [
            'data' => $this->data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.create', [
            'data' => $this->data
        ]);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code' => 'required|string|max:20|unique:countries,code' . ($id ? (',' . $id . ',id') : ''),
            'name' => 'required|string|max:100'
        ], [], [
            'code' => __('country.code'),
            'name' => __('country.name')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $attrs = [
            'code' => $request->get('code'),
            'name' => $request->get('name'),
            'is_active' => 1
        ];

        try {
            DB::beginTransaction();

            Country::create($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.countries.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$country = Country::find($id)) {
            abort(404);
        }

        $this->data['country'] = $country;

        return view('admin.countries.edit', [
            'data' => $this->data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$country = Country::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $country->id);

        $attrs = [
            'code' => $request->get('code'),
            'name' => $request->get('name')
        ];

        try {
            DB::beginTransaction();

            $country->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.countries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$country = Country::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $country->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.countries.index');
    }
}
