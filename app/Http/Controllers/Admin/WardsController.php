<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Ward;
use App\City;
use App\Country;
use App\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LogService;
use Exception;

class WardsController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * wardsController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.10'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $wards = Ward::join('countries', 'wards.country_id', '=', 'countries.id')
                      ->join('provinces', 'wards.province_id', '=', 'provinces.id')
                      ->join('cities', 'wards.city_id', '=', 'cities.id');
         if ($is_active = $request->query('is_active')) {
            $wards = $wards->where('wards.is_active', '=', $is_active);
        }
        else
        {
            $wards = $wards->where('wards.is_active', '=', 1);
        }

        if ($countryId = $request->query('country_id')) {
            $wards = $wards->where('wards.country_id', '=', $countryId);
        }

        if ($provinceId = $request->query('province_id')) {
            $wards = $wards->where('wards.province_id', '=', $provinceId);
        }
        if ($cityId = $request->query('city_id')) {
            $wards = $wards->where('wards.city_id', '=', $cityId);
        }
        if ($request->ajax() || $request->wantsJson()) {
            return [
                'wards' => $wards->orderBy('wards.name')->get(['wards.*'])
            ];
        }

        $this->data['wards'] = $wards->orderBy('countries.code')
                                       ->orderBy('provinces.name')
                                       ->orderBy('cities.name')
                                       ->orderBy('wards.name')
                                       ->select([
                                           'wards.*',
                                           'countries.name AS country_name',
                                           'provinces.name AS province_name',
                                           'cities.name AS city_name'
                                       ])
                                       ->paginate(config('app.items_per_page'));

        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.wards.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        return view('admin.wards.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     */
    private function validateRequest(Request $request)
    {
        $this->validate($request, [
            'country_id'   => 'required|exists:countries,id',
            'province_id'  => 'required|exists:provinces,id',
            'city_id'  => 'required|exists:cities,id',
            'name'         => 'required|string|max:100',
        ], [], [
            'country_id'   => __('city.country_id'),
            'province_id'  => __('city.province_id'),
            'city_id'  => __('city.city_id'),
            'name'         => __('city.name'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        $attrs = [
            'country_id' => $request->get('country_id'),
            'province_id' => $request->get('province_id'),
            'city_id' => $request->get('city_id'),
            'vnpost_alias' => $request->get('vnpost_alias'),
            'name' => $request->get('name'),
            'is_active' => 1
        ];

        try {
            DB::beginTransaction();

            Ward::create($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.wards.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$Ward = Ward::find($id)) {
            abort(404);
        }
        $this->data['ward'] = $Ward;

        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        return view('admin.wards.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$Ward = Ward::find($id)) {
            abort(404);
        }

        $this->validateRequest($request);

        $attrs = [
            'country_id' => $request->get('country_id'),
            'province_id' => $request->get('province_id'),
            'city_id' => $request->get('city_id'),
            'name' => $request->get('name'),
            'vnpost_alias' => $request->get('vnpost_alias'),
        ];
        try {
            DB::beginTransaction();

            $Ward->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.wards.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$Ward = Ward::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $Ward->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.wards.index');
    }

}
