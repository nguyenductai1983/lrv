<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VnPostRequest;
use App\Http\Controllers\CallbackController;


class Vnptconfirm extends Controller
{
    private $data;
    protected $CallbackController;
    public function __construct(CallbackController $CallbackController)
    {
        $this->CallbackController = $CallbackController;
        $this->data = [
                     'menu' => 'extent.2'
                ];
    }
    public function index()
    {
        $this->data['complete']='';
        return view('admin.extent.vnptconfirm',$this->data);
    }
     public function store(VnPostRequest $request)
     {
        $response = $this->CallbackController->vnpost($request);
        $this->data['complete']= $response->content();
        if($response->status()==200)
        {
            $this->data['complete']=__('label.mts_status_4');
        }
        return view('admin.extent.vnptconfirm',$this->data);
    }
}
