<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PointConvert;
use App\Enum\PointEnum;
use App\Http\Controllers\Controller;
use Exception;
class PointConvertController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '2.13'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['pointConvert'] = PointConvert::where([])->orderBy('id', 'desc')->get();

        return view('admin.pointConvert.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pointConvert.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request) {
        $this->validate($request, [
            'point' => 'required|numeric',
            'amount' => 'required|numeric'
                ], [], [
            'point' => __('point.value'),
            'amount' => __('point.amount')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            PointConvert::where(['is_active' => PointEnum::IS_ACTIVE])->update(['is_active' => PointEnum::NOT_ACTIVE]);
            PointConvert::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.point-converts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$pointConvert = PointConvert::find($id)) {
            abort(404);
        }
        $this->data['pointConvert'] = $pointConvert;

        return view('admin.pointConvert.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$pointConvert = PointConvert::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $pointConvert->id);

        try {
            DB::beginTransaction();

            $pointConvert->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.point-converts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$pointConvert = PointConvert::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $pointConvert->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.point-converts.index');
    }

}
