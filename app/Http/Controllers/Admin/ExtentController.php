<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\OrderExtraRefund;
use Exception;
use App\Services\LogService;
use App\Exports\ExportQuery;
use App\Order;
use App\Enum\ExtraRefundStatusEnum;
// use Requests;

class ExtentController extends Controller {

    private $data;

    public function __construct() {
        $this->data = [
            'menu' => 'extent.1'
        ];
    }
    public function index(Request $request){
        $user = Auth::user();
        $query = OrderExtraRefund::where([]);
        $query_order= Order::where([]);
        if(!strtoupper($user->role->admin)){
            $query->where('user_id', '=', $user->id);
        }
        if (!empty($request->query('code'))) {
            $query_order->where('code', $request->query('code'));
            $po_order=$query_order->first();
            if(!empty( $po_order)){
            $query->where('order_id',$po_order->id);}
        }
        $this->data['extents'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('admin.extent.index', $this->data);

}
public function exportExcel(Request $request)
    {
        $query = OrderExtraRefund::where([]);
        $query->select('users.code as agency','tbl_order.code as order_code','tbl_extra_refund.code',
        'tbl_extra_refund.amount','tbl_extra_refund.status','tbl_extra_refund.reason');
        $query->leftJoin('tbl_order', 'order_id', '=', 'tbl_order.id' );
        $query->leftJoin('users', 'tbl_extra_refund.user_id', '=', 'users.id' );

        $user = Auth::user();
         if(strtoupper($user->role->admin) == 1){

        }else{
            $query->where('tbl_extra_refund.user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_extra_refund.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_extra_refund.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        $query->where('tbl_extra_refund.status', '!=', ExtraRefundStatusEnum::STATUS_CANCEL);
        return (new ExportQuery($query))->download('tbl_extra_refund'.date('Ymd').'.xlsx');
    }
public function store(Request $request)
{
    $data = $request->all();
        $validator = Validator::make($data, [
            'order_id' => 'required|integer',
            'amount' => 'required|numeric',
            'reason' => 'required|string'
        ],[],[
            'order_id' => __('label.order_id'),
            'amount' => __('label.amount'),
            'reason' => __('label.reason')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $user = Auth::user();
        try {
            DB::beginTransaction();
        $orderExtraRefund = new OrderExtraRefund();
        $orderExtraRefund->order_id = $data['order_id'];
        $user_own = Order::find($data['order_id']);
        $orderExtraRefund->user_id = $user_own->user_id;
        $orderExtraRefund->amount = $data['amount'];
        $orderExtraRefund->type = ExtraRefundStatusEnum::TYPE_EXTRA;
        $orderExtraRefund->status = ExtraRefundStatusEnum::STATUS_NEW;
        $orderExtraRefund->reason = $data['reason'];
        $orderExtraRefund->create_user_id = $user->id;
        $orderExtraRefund->image_1_file_id = $data['image_1_file_id'];
        $orderExtraRefund->image_2_file_id = $data['image_2_file_id'];
        $orderExtraRefund->image_3_file_id = $data['image_3_file_id'];
        $orderExtraRefund->save();

        $orderExtraRefund->code = substr($user_own->code, 0, 3) . sprintf('%04d', $orderExtraRefund->id);
        $orderExtraRefund->update();
        DB::commit();
    } catch (Exception $e) {
        DB::rollBack();
        LogService::error($e);
        abort(500);
    }
    return back();
}
public function extrafee($request)
{
    $query = OrderExtraRefund::where([]);
    $user = Auth::user();
    $query_order= Order::where([]);
    if(!strtoupper($user->role->admin) == 1){
        $query->where('user_id', '=', $user->id);
        $query_order->where('user_id', '=', $user->id);
    }
    $query_order->where('id', '=', $request);
    $po_order=$query_order->first();
    if(empty($po_order))
    {
        return redirect('admin');
    }
    $query->where('order_id', '=', $request)
    ->with([
        'image1',
        'image2',
        'image3',
    ]);
    $this->data['extras'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
    $this->data['language'] = LanguageUtil::getKeyJavascript();
    $this->data['order_id'] = $po_order->code;
    return view('admin.extent.extra', $this->data);
}
    public function approve (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ],[],[
            'id' => __('label.id'),
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $orderExtraRefund = OrderExtraRefund::where('id', $request->get('id'))->first();
        if (empty($orderExtraRefund)) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $user = Auth::user();

        $orderExtraRefund->status = ExtraRefundStatusEnum::STATUS_APPROVE;
        $orderExtraRefund->manager_user_id = $user->id;
        $orderExtraRefund->update();

        if($orderExtraRefund->type == ExtraRefundStatusEnum::TYPE_EXTRA){
            Mail::send('email.approved-extra', ['orderExtraRefund' => $orderExtraRefund], function ($message) use ($orderExtraRefund) {
                $message->to($orderExtraRefund->order->sender_email, $orderExtraRefund->order->sender_email)->subject(__('email.approved-extra'));
            });
        }else if($orderExtraRefund->type == ExtraRefundStatusEnum::TYPE_REFUND){
            Mail::send('email.approved-refund', ['orderExtraRefund' => $orderExtraRefund], function ($message) use ($orderExtraRefund) {
                $message->to($orderExtraRefund->order->sender_email)->subject(__('email.approved-refund'));
            });
        }

        return response(["success" => true, "message" => __('message.extent-approve-susscess'), "errors" => [],"data" => []]);
    }

    public function cancel ( Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ],[],[
            'id' => __('label.id'),
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $orderExtraRefund = OrderExtraRefund::where('id', $request->get('id'))->first();
        if (empty($orderExtraRefund)) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $user = Auth::user();

        $orderExtraRefund->status = ExtraRefundStatusEnum::STATUS_CANCEL;
        $orderExtraRefund->manager_user_id = $user->id;
        $orderExtraRefund->update();

        return response(["success" => true, "message" => __('message.extent-cancel-susscess'), "errors" => [],"data" => []]);
    }

    public function payment (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ],[],[
            'id' => __('label.id'),
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $orderExtraRefund = OrderExtraRefund::where('id', $request->get('id'))->first();
        if (empty($orderExtraRefund)) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [],"data" => []]);
        }
        $user = Auth::user();

        $orderExtraRefund->status = ExtraRefundStatusEnum::STATUS_COMPLETE;
        $orderExtraRefund->pay_user_id = $user->id;
        $orderExtraRefund->update();

        if($orderExtraRefund->type == ExtraRefundStatusEnum::TYPE_EXTRA){
            Mail::send('email.payment-extra', ['orderExtraRefund' => $orderExtraRefund], function ($message) use ($orderExtraRefund) {
                $message->to($orderExtraRefund->order->sender_email, $orderExtraRefund->order->sender_email)->subject(__('email.payment-extra'));
            });
        }else if($orderExtraRefund->type == ExtraRefundStatusEnum::TYPE_REFUND){
            Mail::send('email.payment-refund', ['orderExtraRefund' => $orderExtraRefund], function ($message) use ($orderExtraRefund) {
                $message->to($orderExtraRefund->order->sender_email)->subject(__('email.payment-refund'));
            });
        }

        return response(["success" => true, "message" => __('message.extent-payment-susscess'), "errors" => [],"data" => []]);
    }
}
