<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use App\Exports\ExportQuery;
use Exception;
use Illuminate\Http\Request;
// use Illuminate\Validation\Rule;
use App\Components\ConvertsUtil;
use App\Services\LogService;
use App\Http\Controllers\Controller;
// use App\Http\Requests\AddressRequest;
use App\Campaign;
use App\CustomerGroup;

class CampaignController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * CountriesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '1.5'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $query = Campaign::where([]);
        $query->whereNull('customer_id');
        if ($code = $request->query('code')) {
            $query->where('code', '=', $code);
        }

        $campaigns = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));

        $this->data['campaigns'] = $campaigns;

        $this->data['groups'] = CustomerGroup::orderBy('code')->get();

        return view('admin.campaign.index', $this->data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->data['groups'] = CustomerGroup::orderBy('code')->get();

        return view('admin.campaign.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param Campaign|null $campaign
     */
    private function _validate(Request $request) {
        $this->validate($request, [
            'customer_group_id' => 'nullable|exists:customer_groups,id',
            'name' => 'required|string|max:255',
            'amount' => 'required|numeric|min:0',
            'num_of_use' => 'required|string|max:50',
            'weight' => 'required|string|max:255',
            'apply_per_customer' => 'required|string|max:255',
            'from_date' => 'required|date_format:' . config('app.date_format'),
            'to_date' => 'required|date_format:' . config('app.date_format')
                ], [], [
            'customer_group_id' => __('campaign.customer_group_id'),
            'name' => __('campaign.name'),
            'amount' => __('campaign.amount'),
            'num_of_use' => __('campaign.num_of_use'),
            'weight' => __('campaign.weight'),
            'apply_per_customer' => __('campaign.apply_per_customer'),
            'from_date' => __('campaign.from_date'),
            'to_date' => __('campaign.to_date')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->_validate($request);
        $params = $request->all();
        try {
            DB::beginTransaction();
            $user = Auth::user();
            $campaign = new Campaign();
            $campaign->customer_group_id = $params['customer_group_id'];
            $campaign->name = $params['name'];
            $campaign->amount = $params['amount'];
            $campaign->num_of_use = $params['num_of_use'];
            $campaign->weight = $params['weight'];
            $campaign->apply_per_customer = $params['apply_per_customer'];
            $campaign->from_date = ConvertsUtil::dateFormat($params['from_date'], config('app.date_format'), 'Y-m-d 00:00:00');
            $campaign->to_date = ConvertsUtil::dateFormat($params['to_date'], config('app.date_format'), 'Y-m-d 23:59:59');
            $campaign->create_user_id = $user->id;
            $campaign->Transport = isset($params['Transport']) ?: 0;
            $campaign->Express = isset($params['Express']) ?: 0;
            $campaign->YHL = isset($params['YHL']) ?: 0;
            $campaign->Quote = isset($params['Quote']) ?: 0;
            $campaign->MTS = isset($params['MTS']) ?: 0;
            $campaign->save();

            $campaign->update(['code' =>  'CP' . rand(10000, 99999) . $campaign->id]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.campaign.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$campaign = Campaign::find($id)) {
            abort(404);
        }
        $this->data['campaign'] = $campaign;
        $this->data['campaign']['from_date'] = ConvertsUtil::strToDateFormat($campaign->from_date, 'Y-m-d H:i:s', 'd/m/Y');
        $this->data['campaign']['to_date'] = ConvertsUtil::strToDateFormat($campaign->to_date, 'Y-m-d H:i:s', 'd/m/Y');
        $this->data['groups'] = CustomerGroup::orderBy('code')->get();

        return view('admin.campaign.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!$campaign = Campaign::find($id)) {
            abort(404);
        }

        $this->_validate($request, $campaign);
        $params = $request->all();

        try {
            DB::beginTransaction();

            $campaign->customer_group_id = $params['customer_group_id'];
            $campaign->name = $params['name'];
            $campaign->amount = $params['amount'];
            $campaign->num_of_use = $params['num_of_use'];
            $campaign->weight = $params['weight'];
            $campaign->apply_per_customer = $params['apply_per_customer'];
            $campaign->from_date = ConvertsUtil::dateFormat($params['from_date'], config('app.date_format'), 'Y-m-d 00:00:00');
            $campaign->to_date = ConvertsUtil::dateFormat($params['to_date'], config('app.date_format'), 'Y-m-d 23:59:59');
            $campaign->status = $params['status'];
            $campaign->Transport = isset($params['Transport']) ?: 0;
            $campaign->Express = isset($params['Express']) ?: 0;
            $campaign->YHL = isset($params['YHL']) ?: 0;
            $campaign->Quote = isset($params['Quote']) ?: 0;
            $campaign->MTS = isset($params['MTS']) ?: 0;
            $campaign->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.campaign.index');
    }
}
