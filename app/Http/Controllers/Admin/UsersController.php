<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use App\User;
use App\Agency;
use App\Role;
use App\Warehouse;
use App\Country;
use App\Http\Controllers\Controller;
use App\Services\LogService;

class UsersController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '1.7'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->data['agencies'] = Agency::where('is_deleted', '=', 0)->get();
        $this->data['roles']    = Role::all();

        $users = User::with(['agency', 'role']);

        $keyword = $request->query('keyword');
        if ($keyword != '') {
            $users = $users->where(function ($query) use ($keyword) {
                $query->where('code', 'like', "%{$keyword}%")
                      ->orWhere('first_name', 'like', "%{$keyword}%");
            });
        }

        if ($agencyId = $request->query('agency_id')) {
            $users = $users->where('agency_id', '=', $agencyId);
        }

        if ($roleId = $request->query('role_id')) {
            $users = $users->where('role_id', '=', $roleId);
        }
        $users->orderBy('code', 'asc');
        $this->data['users'] = $users->get();

        return view('admin.users.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['agencies'] = Agency::where('is_deleted', '=', 0)->get();
        $this->data['roles']    = Role::all();
        $this->data['warehouses']= Warehouse::where(['is_deleted' => 0])->get();
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.users.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null|integer $id
     */
    private function _validate(Request $request, $id = null)
    {
        $this->validate($request, [
            'agency_id'        => 'required|exists:agencies,id',
            'role_id'          => 'required|exists:roles,id',
            'first_name'       => 'required|string|max:50',
            'last_name'       => 'required|string|max:50',
            'code'             => 'required|string|max:100|unique:users,code' . ($id ? (',' . $id . ',id') : ''),
            'username'         => ($id ? 'nullable' : 'required') . '|string|max:50|unique:users,username',
            'password'         => ($id ? 'nullable' : 'required') . '|string|min:6|max:50',
            'email'            => ($id ? 'nullable' : 'required') . '|string|max:100|email|unique:users,email',
            'address'          => 'nullable|string|max:255',
            'gender'           => 'nullable|in:1,2',
            'mobile_phone'     => 'nullable|string|max:20',
            'home_phone'       => 'nullable|string|max:20',
            'office_phone'     => 'nullable|string|max:20',
            'fax'              => 'nullable|string|max:20',
            'bank_account'     => 'nullable|string|max:50',
            'bank_code'        => 'nullable|string|max:50',
            'tax_code'         => 'nullable|string|max:50',
            'id_card'          => 'nullable|string|max:50',
            'date_issued'      => 'nullable|date_format:' . config('app.date_format'),
            'place_issued'     => 'nullable|string|max:255',
            'birthday'         => 'nullable|date_format:' . config('app.date_format'),
            'family_allowance' => 'nullable|numeric|min:0',
            'mts_percent'      => 'nullable|integer|min:0|max:100',
            'warehouse_id'      => 'nullable|exists:tbl_warehouse,id'
        ], [], [
            'agency_id'        => __('user.agency_id'),
            'role_id'          => __('user.role_id'),
            'first_name'       => __('user.first_name'),
            'last_name'       => __('user.last_name'),
            'code'             => __('user.code'),
            'username'         => __('user.username'),
            'password'         => __('user.password'),
            'email'            => __('user.email'),
            'address'          => __('user.address'),
            'gender'           => __('user.gender'),
            'mobile_phone'     => __('user.mobile_phone'),
            'home_phone'       => __('user.home_phone'),
            'office_phone'     => __('user.office_phone'),
            'fax'              => __('user.fax'),
            'bank_account'     => __('user.bank_account'),
            'bank_code'        => __('user.bank_code'),
            'tax_code'         => __('user.tax_code'),
            'id_card'          => __('user.id_card'),
            'date_issued'      => __('user.date_issued'),
            'place_issued'     => __('user.place_issued'),
            'birthday'         => __('user.birthday'),
            'family_allowance' => __('user.family_allowance'),
            'mts_percent'      => __('user.mts_percent'),
            'warehouse_id'      => __('user.warehouse_id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);

        try {
            DB::beginTransaction();

            $attrs             = $request->all();
            $attrs['password'] = bcrypt($attrs['password']);

            User::create($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.users.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with(['agency', 'role'])
                    ->where('id', '=', $id)
                    ->first();

        if (!$user) {
            abort(404);
        }

        $this->data['user']     = $user;
        $this->data['agencies'] = Agency::all();
        $this->data['roles']    = Role::all();
        $this->data['warehouses']= Warehouse::where(['is_deleted' => 0])->get();
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.users.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::with(['agency', 'role', 'warehouse'])
                    ->where('id', '=', $id)
                    ->first();

        if (!$user) {
            abort(404);
        }

        $this->_validate($request, $user->id);

        try {
            DB::beginTransaction();

            $attrs = $request->all();
            if (array_key_exists('password', $attrs)) {
                if ($attrs['password'] != '') {
                    $attrs['password'] = bcrypt($attrs['password']);
                } else {
                    unset($attrs['password']);
                }
            }

            if (array_key_exists('username', $attrs)) {
                unset($attrs['username']);
            }

            if (array_key_exists('email', $attrs)) {
                unset($attrs['email']);
            }

            $user->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::with(['agency', 'role'])
                    ->where('id', '=', $id)
                    ->first();

        if (!$user) {
            abort(404);
        }

        if ($user->id == auth()->user()->id) {
            return redirect()->back()
                             ->with(['error' => __('label.can_not_delete_record')]);
        }

        try {
            DB::beginTransaction();

            $user->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.users.index'));
    }
}
