<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExportQuery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Transaction;

class ReceiptController extends Controller {

    private $data;

    public function __construct() {
        $this->data = [
            'menu' => 'receipt.1'
        ];
    }

    public function index(Request $request) {
        $columns = [
            'tbl_transaction.code',
            'tbl_transaction.goods_code',
            'tbl_transaction.order_id',
            'tbl_transaction.mts_id',
            'tbl_order.type as order_type',
            'customers.first_name as customer_first_name',
            'customers.middle_name as customer_middle_name',
            'customers.last_name as customer_last_name',
            'customers.telephone as customer_telephone',
            'tbl_transaction.created_at',
            'tbl_transaction.credit_amount',
            'tbl_transaction.total_amount',
            'tbl_transaction.type as type2',
            'tbl_transaction.total_agency_discount',
            'tbl_transaction.agency_discount',
            'users.code as user_code',
        ];

        $query = DB::table('tbl_transaction')->leftJoin('users', 'tbl_transaction.user_id', '=', 'users.id');
        $query->leftJoin('customers', 'tbl_transaction.customer_id', '=', 'customers.id');
        $query->leftJoin('tbl_order', 'tbl_transaction.order_id', '=', 'tbl_order.id');
        $user = Auth::user();
        if(strtoupper($user->role->name) != "ADMIN"){
            $query->where('tbl_transaction.user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_transaction.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_transaction.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_code'))) {
            $query->where('users.code', '=', $request->query('user_code'));
        }
        if (!empty($request->query('customer_phone'))) {
            $query->where('customers.telephone', '=', $request->query('customer_phone'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 1) {
            $query->where(function($q) {
                $q->where('tbl_order.type', 1)->orWhere(function($q1) {
                    $q1->whereNull('tbl_transaction.mts_id')->whereNull('tbl_transaction.order_id');
                });
            });
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 2) {
            $query->where('tbl_order.type', '=', $request->query('service_type'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 3) {
            $query->where('tbl_order.type', '=', $request->query('service_type'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 4) {
            $query->where('tbl_order.type', '=', $request->query('service_type'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 5) {
            $query->where('tbl_transaction.mts_id', '>', 0);
        }
        $query->where('tbl_order.order_status', '!=', 0);
        $this->data['receipts'] = $query->orderBy('tbl_transaction.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['total_amount'] = $query->sum('tbl_transaction.credit_amount');
        $this->data['total_agency_discount'] = $query->sum('tbl_transaction.agency_discount');
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt.index', $this->data);
    }


    public function exportExcel(Request $request)
    {
        $query = Transaction::leftJoin('users', 'tbl_transaction.user_id', '=', 'users.id');
        $query->leftJoin('customers', 'tbl_transaction.customer_id', '=', 'customers.id');
        $query->leftJoin('tbl_order', 'tbl_transaction.order_id', '=', 'tbl_order.id');
        $user = Auth::user();
        if(strtoupper($user->role->name) != "ADMIN"){
            $query->where('tbl_transaction.user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_transaction.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_transaction.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_code'))) {
            $query->where('users.code', '=', $request->query('user_code'));
        }
        if (!empty($request->query('customer_phone'))) {
            $query->where('customers.telephone', '=', $request->query('customer_phone'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 1) {

            $query->where(function($q) {
                $q->where('tbl_order.type', 1)->orWhere(function($q1) {
                    $q1->whereNull('tbl_transaction.mts_id')->whereNull('tbl_transaction.order_id');
                });
            });
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 2) {
            $query->where('tbl_order.type', '=', $request->query('service_type'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 3) {
            $query->where('tbl_order.type', '=', $request->query('service_type'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 4) {
            $query->where('tbl_order.type', '=', $request->query('service_type'));
        }
        if (!empty($request->query('service_type')) && $request->query('service_type') == 5) {
            $query->where('tbl_transaction.mts_id', '>', 0);
        }
        $query->where('tbl_order.order_status', '!=', 0);
        $query->orderBy('tbl_transaction.id', 'desc')->select('tbl_transaction.code',
            'tbl_transaction.goods_code',
            'tbl_transaction.order_id',
            'tbl_transaction.mts_id',
            'tbl_order.type as order_type',
            'customers.id as customer_id',
            'customers.first_name as customer_first_name',
            'customers.middle_name as customer_middle_name',
            'customers.last_name as customer_last_name',
            'customers.telephone as customer_telephone',
            'tbl_transaction.created_at',
            'tbl_transaction.type as type_tran',
            DB::raw('IF(tbl_transaction.type = 2, tbl_transaction.credit_amount * -1, tbl_transaction.credit_amount) as credit_amount'),
            DB::raw('IF(tbl_transaction.type = 2, tbl_transaction.total_amount * -1, tbl_transaction.total_amount) as total_amount'),
            DB::raw('IF(tbl_transaction.type = 2, tbl_transaction.total_agency_discount * -1, tbl_transaction.total_agency_discount) as total_agency_discount'),
            DB::raw('IF(tbl_transaction.type = 2, tbl_transaction.agency_discount * -1, tbl_transaction.agency_discount) as agency_discount'),
            'users.code as user_code');
            return (new ExportQuery($query))->download('receipts.xlsx');
    }
}
