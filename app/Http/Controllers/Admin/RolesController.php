<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;

class RolesController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * RolesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '1.6'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['roles'] = Role::all();

        return view('admin.roles.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     */
    private function _validate(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:100',
            'description' => 'nullable|string|max:255'
        ], [], [
            'name'        => __('role.name'),
            'description' => __('role.description')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);
        if (!isset($request->admin)){
    $request->request->add(['admin'=>0 ]);
    }
        try {
            DB::beginTransaction();

            Role::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.roles.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$role = Role::find($id)) {
            abort(404);
        }
        $this->data['role'] = $role;

        return view('admin.roles.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$role = Role::find($id)) {
            abort(404);
        }

        $this->_validate($request);
        if (!isset($request->admin))
            {
             $request->request->add(['admin'=>0 ]);
            }
        try {
            DB::beginTransaction();

            $role->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.roles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$role = Role::find($id)) {
            abort(404);
        }

        if ($role->users()->count()) {
            return redirect()->back()
                             ->with('error', __('label.can_not_delete_record'));
        }

        try {
            DB::beginTransaction();

            $role->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.roles.index'));
    }

    /**
     * Show form for edit permissions of role
     *
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPermissions($id)
    {
        if (!$role = Role::with('permissions')->find($id)) {
            abort(404);
        }

        $this->data['role']        = $role;
        $this->data['permissions'] = Permission::all();
        $rolePermissionsId = $role->permissions->pluck('id')->all();

        return view('admin.roles.permissions', $this->data);
    }

    /**
     * Update permissions of role
     *
     * @param Request $request
     * @param integer $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updatePermissions(Request $request, $id)
    {
        if (!$role = Role::find($id)) {
            abort(404);
        }

        $this->validate($request, [
            'permissions' => 'required|array|distinct',
        ]);

        $permissionsId = $request->get('permissions');

        $count = Permission::whereIn('id', $permissionsId)->count();
        if ($count != count($permissionsId)) {
            return redirect()->back()
                             ->withInput()
                             ->withErrors(['permissions' => __('validation.exists', ['attribute' => __('permission.name')])]);
        }

        try {
            DB::beginTransaction();

            $role->permissions()->sync($permissionsId);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.roles.permissions', $role->id))->with([
            'status' => __('label.data_updated')
        ]);
    }
}
