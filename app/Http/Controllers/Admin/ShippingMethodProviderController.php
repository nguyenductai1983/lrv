<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ShippingMethodProvider;
use App\ShippingMethod;
use App\ShippingProvider;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Exception;

class ShippingMethodProviderController extends Controller
{
    //
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.16'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['ShippingMethodProviders'] = ShippingMethodProvider::where('is_active', '=', 1)->orderBy('display_order')->get();

        return view('admin.shippingMethodProvider.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['shipping_methods'] = ShippingMethod::all();
        $this->data['shipping_providers'] = ShippingProvider::all();
        return view('admin.shippingMethodProvider.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'name'        => 'required|string',
            'display_order' => 'nullable|numeric',
            'shipping_provider_id' => 'nullable|numeric',
            'shipping_method_id' => 'nullable|numeric',
            'is_active' => 'nullable|numeric',
            'image_file_id'     => [
                'nullable',
                Rule::exists('files', 'id')->where(function ($query) {
                    $query->where('created_user_id', '=', auth()->user()->id);
                })
            ],
        ], [], [
            'name'        => __('shipping.name'),
            'display_order'        => __('shipping.display_order'),
            'shipping_provider_id'        => __('shipping.Carrier'),
            'shipping_method_id'        => __('shipping.method'),
            'is_active'        => __('shipping.is_active'),
            'image_file_id'     => __('shipping.image_file_id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();
            ShippingMethodProvider::create($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-method-providers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$ShippingMethodProvider = ShippingMethodProvider::find($id)) {
            abort(404);
        }
        $this->data['ShippingMethodProvider'] = $ShippingMethodProvider;
        $this->data['shipping_methods'] = ShippingMethod::all();
        $this->data['shipping_providers'] = ShippingProvider::all();
        return view('admin.shippingMethodProvider.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$ShippingMethodProvider = ShippingMethodProvider::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $ShippingMethodProvider->id);

        try {
            DB::beginTransaction();

            $ShippingMethodProvider->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-method-providers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$ShippingMethodProvider = ShippingMethodProvider::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $ShippingMethodProvider->is_deleted = 1;
            $ShippingMethodProvider->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-mrthod-providers.index');
    }
}
