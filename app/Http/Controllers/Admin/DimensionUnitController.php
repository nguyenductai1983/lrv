<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\DimensionUnit;
use App\Http\Controllers\Controller;


class DimensionUnitController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.2'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['dimensionUnit'] = DimensionUnit::where('is_deleted', '=', 0)->orderBy('display_order')->get();

        return view('admin.dimensionUnit.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dimensionUnit.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'        => 'required|string|max:20|unique:tbl_dimension_unit,code' . ($id ? (',' . $id . ',id') : ''),
            'name'        => 'required|string|max:100',
            'display_order' => 'nullable|numeric',
            'is_default' => 'nullable|numeric',
            'radio' => 'nullable|numeric',
        ], [], [
            'code'        => __('dimension.code'),
            'name'        => __('dimension.name'),
            'display_order'        => __('dimension.name'),
            'is_default'        => __('dimension.name'),
            'radio'        => __('dimension.name'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            DimensionUnit::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.dimensions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$dimensionUnit = DimensionUnit::find($id)) {
            abort(404);
        }
        $this->data['dimensionUnit'] = $dimensionUnit;

        return view('admin.dimensionUnit.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$dimensionUnit = DimensionUnit::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $dimensionUnit->id);

        try {
            DB::beginTransaction();

            $dimensionUnit->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.dimensions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$dimensionUnit = DimensionUnit::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $dimensionUnit->is_deleted = 1;
            $dimensionUnit->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.dimensions.index');
    }
}
