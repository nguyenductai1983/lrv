<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;

class ConfigsController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * ConfigsController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '1.1'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $configs = Config::all();

        $this->data['configs'] = $configs;

        return view('admin.configs.index', $this->data);
    }
 public function phpinfo() {


        return view('admin.configs.phpinfo');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'configs.*.percent' => 'required|numeric|min:0|max:100',
            'configs.*.quota' => 'nullable|numeric|min:0',
            'configs.*.agency_percent' => 'required|numeric|min:0|max:100',
            'configs.*.agency_quota' => 'nullable|numeric|min:0',
        ]);

        $configs = Config::all();
        if ($configs->count() != count($request->get('configs'))) {
            return redirect()->back()
                            ->withInput()
                            ->withErrors(['configs' => __('validation.exists', ['attribute' => __('config.manage')])]);
        }

        try {
            DB::beginTransaction();

            foreach ($configs as $config) {
                $config->update($request->get('configs')[$config->id]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.configs.index')
                        ->with(['status' => __('label.data_updated')]);
    }

}
