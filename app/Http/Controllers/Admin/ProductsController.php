<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductGroup;
use App\Product;
use App\Agency;
use App\Currency;
use App\WeightUnit;
use App\MessureUnit;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;
use App\Enum\DangerTypeEnum;

class ProductsController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '8.2'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = [
            'products.id',
            'product_groups.name as group_name',
            'product_groups.code as group_code',
            'products.code',
            'products.name',
            'products.by_weight',
            'products.cost',
            'products.price',
            'products.sale_price',
            'products.description',
            'products.status',
            'products.messure_unit_id',
            'products.commission_amount',
            'products.commission_percent',
            'products.quota_discount',
            'products.commission_quota_amount',
            'products.commission_quota_percent'
        ];
        $query = Product::join('product_groups', 'products.product_group_id', '=', 'product_groups.id');
        $query->where('products.is_deleted', '=', 0);
        if (!empty($request->query('product_group'))) {
            $query->where('products.product_group_id', '=', $request->query('product_group'));
        }
        $products = $query->orderBy('products.code', 'asc')->get($columns);
        $this->data['products'] = $products;
        $this->data['groups']   = ProductGroup::orderBy('code', 'asc')->get();

        return view('admin.products.index', [
            'data' => $this->data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_product()
    {
        $product = Product::find(1);
        $product->increaseStock(10);
    }
    public function dell_product()
    {
        $product = Product::find(1);
        $product->decreaseStock(10);
    }
    public function check_product()
    {
        $product = Product::find(1);
      $abc=  $product->stock();
      return $abc;
    }
    public function create()
    {
        $this->data['groups'] = ProductGroup::orderBy('code', 'asc')->get();
        $this->data['currencies']  = Currency::orderBy('display_order', 'asc')->get();
        $this->data['weight_units']  = WeightUnit::orderBy('display_order', 'asc')->get();
        $this->data['messure_units']  = MessureUnit::orderBy('display_order', 'asc')->get();
        $this->data['dangers'] = $this->getListDanger();
        return view('admin.products.create', [
            'data' => $this->data
        ]);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'          => 'required|string|max:20',
            'barcode'       => 'nullable|string|max:20',
            'name'          => 'required|string|max:100',
            'product_group' => 'required|exists:product_groups,id',
            'messure_unit'  => 'required|exists:tbl_messure_unit,id',
            'currency'      => 'required|exists:currencies,id',
            'weight_unit'   => 'required|exists:tbl_weight_unit,id',
            'weight'        => 'required|numeric|min:0',
            'cost'          => 'nullable|numeric|min:0',
            'price'         => 'nullable|numeric|min:0',
            'sale_price'    => 'required|numeric|min:0',
            'by_weight'     => 'nullable|in:1',
            'description'   => 'nullable|string',
            'content'       => 'nullable|string',
            'thumbnail'     => 'nullable|integer',
            'thumbnail_path' => 'nullable|string',
            'status'        => 'nullable|in:1',
            'commission_amount'        => 'required|numeric|min:0',
            'commission_percent'        => 'required|numeric|min:0',
            'commission_quota_amount'        => 'required|numeric|min:0',
            'commission_quota_percent'        => 'required|numeric|min:0',
            'quota_discount'        => 'required|numeric|min:0'
        ], [], [
            'code'          => __('product.code'),
            'barcode'       => __('product.code'),
            'name'          => __('product.name'),
            'product_group' => __('product.product_group'),
            'messure_unit'  => __('product.unit'),
            'currency'      => __('product.currency'),
            'weight_unit'   => __('product.weight_unit'),
            'weight'        => __('product.weight'),
            'cost'          => __('product.cost'),
            'price'         => __('product.price'),
            'sale_price'    => __('product.sale_price'),
            'by_weight'     => __('product.by_weight'),
            'description'   => __('product.description'),
            'content'     => __('product.content'),
            'thumbnail'     => __('product.thumbnail'),
            'thumbnail_path' => __('product.thumbnail_path'),
            'status'        => __('label.status'),
            'commission_amount' => __('product.commission_amount'),
            'commission_percent' => __('product.commission_percent'),
            'commission_quota_amount' => __('product.commission_quota_amount'),
            'commission_quota_percent' => __('product.commission_quota_percent'),
            'quota_discount' => __('product.quota_discount'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $attrs = [
            'code'             => $request->get('code'),
            'barcode'          => $request->get('barcode'),
            'name'             => $request->get('name'),
            'product_group_id' => $request->get('product_group'),
            'currency_id'      => $request->get('currency'),
            'weight_unit_id'   => $request->get('weight_unit'),
            'messure_unit_id'  => $request->get('messure_unit'),
            'weight'           => $request->get('weight'),
            'cost'             => $request->get('cost') ?: 0,
            'price'            => $request->get('price') ?: 0,
            'sale_price'       => $request->get('sale_price') ?: 0,
            'by_weight'        => $request->get('by_weight') ? 1 : 0,
            'description'      => $request->get('description') ?: null,
            'content'           => $request->get('content') ?: null,
            'thumbnail_id'      => $request->get('thumbnail') ?: null,
            'thumbnail'         => $request->get('thumbnail_path') ?: null,
            'status'           => $request->get('status') ? 1 : 0,
            'danger'           => $request->get('danger') ?: null,
            'display_order'           => $request->get('display_order') ?: null,
            'separate'           => $request->get('separate') ?: null,
            'commission_amount'           => $request->get('commission_amount') ?: 0,
            'commission_percent'           => $request->get('commission_percent') ?: 0,
            'commission_quota_amount'           => $request->get('commission_quota_amount') ?: 0,
            'commission_quota_percent'           => $request->get('commission_quota_percent') ?: 0,
            'quota_discount'           => $request->get('quota_discount') ?: 0,
            'limit_pcs'           => $request->get('limit_pcs') ?: 0,
        ];

        try {
            DB::beginTransaction();

            $product = Product::create($attrs);

            $agencies = Agency::all('id');
            if ($agencies->count()) {
                $syncData = [];
                foreach ($agencies as $agency) {
                    $syncData[$agency->id] = [
                        'price' => $product->sale_price,
                        'weight' => $product->weight,
                        'currency_id' => $product->currency_id,
                        'weight_unit_id' => $product->weight_unit_id,
                        'messure_unit_id' => $product->messure_unit_id,
                        'messure_unit_code' => isset($product->messureunits->code) ? $product->messureunits->code : null,
                        'currency_code' => isset($product->currencies->code) ? $product->currencies->code : null,
                    ];
                }
                $product->agencies()->sync($syncData);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.products.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    // lây danh sách cảnh báo cho hàng hóa
    private function getListDanger()
    {
        $data[0] = "Null";
        $data[DangerTypeEnum::battery] = __('product.battery');
        $data[DangerTypeEnum::perfuner] = __('product.perfume');
        $data[DangerTypeEnum::tonic] =  __('product.tonic');
        return $data;
    }
    public function edit($id)
    {
        if (!$product = Product::find($id)) {
            abort(404);
        }

        $this->data['product'] = $product;
        $this->data['groups']  = ProductGroup::orderBy('code', 'asc')->get();
        $this->data['currencies']  = Currency::orderBy('display_order', 'asc')->get();
        $this->data['weight_units']  = WeightUnit::orderBy('display_order', 'asc')->get();
        $this->data['messure_units']  = MessureUnit::orderBy('display_order', 'asc')->get();
        $this->data['dangers'] = $this->getListDanger();
        return view('admin.products.edit', [
            'data' => $this->data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$product = Product::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $product->id);

        $attrs = [
            'code'             => $request->get('code'),
            'barcode'          => $request->get('barcode'),
            'name'             => $request->get('name'),
            'product_group_id' => $request->get('product_group'),
            'currency_id'      => $request->get('currency'),
            'weight_unit_id'   => $request->get('weight_unit'),
            'messure_unit_id'  => $request->get('messure_unit'),
            'weight'           => $request->get('weight'),
            'cost'             => $request->get('cost') ?: 0,
            'price'            => $request->get('price') ?: 0,
            'sale_price'       => $request->get('sale_price') ?: 0,
            'by_weight'        => $request->get('by_weight') ? 1 : 0,
            'description'      => $request->get('description') ?: null,
            'content'          => $request->get('content') ?: null,
            'thumbnail_id'     => $request->get('thumbnail') ?: null,
            'thumbnail'        => $request->get('thumbnail_path') ?: null,
            'status'           => $request->get('status') ? 1 : 0,
            'danger'           => $request->get('danger') ?: null,
            'display_order'           => $request->get('display_order') ?: null,
            'separate'           => $request->get('separate') ?: null,
            'commission_amount'           => $request->get('commission_amount') ?: 0,
            'commission_percent'           => $request->get('commission_percent') ?: 0,
            'commission_quota_amount'           => $request->get('commission_quota_amount') ?: 0,
            'commission_quota_percent'           => $request->get('commission_quota_percent') ?: 0,
            'quota_discount'           => $request->get('quota_discount') ?: 0,
            'limit_pcs'           => $request->get('limit_pcs') ?: 0,
        ];

        try {
            DB::beginTransaction();

            $product->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$product = Product::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $product->is_deleted = 1;
            $product->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.products.index');
    }
}
