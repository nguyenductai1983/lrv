<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Warehouse;
use App\Country;
use App\Http\Controllers\Controller;
use App\Services\LogService;

class ProfileController extends Controller
{
    private function _validate(Request $request)
    {
        $this->validate($request, [
            'first_name'       => 'required|string|max:50',
            'email'            => 'nullable|string|max:100|email|unique:users,email',
            'address'          => 'nullable|string|max:255',
            'gender'           => 'nullable|in:1,2',
            'mobile_phone'     => 'nullable|string|max:20',
            'home_phone'       => 'nullable|string|max:20',
            'office_phone'     => 'nullable|string|max:20',
            'fax'              => 'nullable|string|max:20',
            'bank_account'     => 'nullable|string|max:50',
            'bank_code'        => 'nullable|string|max:50',
            'tax_code'         => 'nullable|string|max:50',
            'id_card'          => 'nullable|string|max:50',
            'date_issued'      => 'nullable|date_format:' . config('app.date_format'),
            'place_issued'     => 'nullable|string|max:255',
            'birthday'         => 'nullable|date_format:' . config('app.date_format'),
            'family_allowance' => 'nullable|numeric|min:0',
            'mts_percent'      => 'nullable|integer|min:0|max:100',
            'warehouse_id'      => 'nullable|exists:tbl_warehouse,id'
        ], [], [
            'first_name'       => __('user.first_name'),
            'email'            => __('user.email'),
            'address'          => __('user.address'),
            'gender'           => __('user.gender'),
            'mobile_phone'     => __('user.mobile_phone'),
            'home_phone'       => __('user.home_phone'),
            'office_phone'     => __('user.office_phone'),
            'fax'              => __('user.fax'),
            'bank_account'     => __('user.bank_account'),
            'bank_code'        => __('user.bank_code'),
            'tax_code'         => __('user.tax_code'),
            'id_card'          => __('user.id_card'),
            'date_issued'      => __('user.date_issued'),
            'place_issued'     => __('user.place_issued'),
            'birthday'         => __('user.birthday'),
            'family_allowance' => __('user.family_allowance'),
            'mts_percent'      => __('user.mts_percent'),
            'warehouse_id'      => __('user.warehouse_id')
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function info()
    {
        $user = Auth::user();
        if (!$user) {
            abort(404);
        }

        $this->data['user']     = $user;
        $this->data['warehouses']= Warehouse::where(['is_deleted' => 0])->get();
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.profile.info', $this->data);
    }

    public function changePassword()
    {
        $user = Auth::user();
        if (!$user) {
            abort(404);
        }

        $this->data['user'] = $user;

        return view('admin.profile.change_password', $this->data);
    }
    public function PasswordExpired()
    {
        $user = Auth::user();
        if (!$user) {
            abort(404);
        }
        $this->data['user'] = $user;
        $this->data['expired'] = 1;
        return view('admin.profile.change_password', $this->data);
    }
    /**
     * Validate request
     *
     * @param Request $request
     * @param null|integer $id
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function infoUpdate(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            abort(404);
        }

        $this->_validate($request);

        try {
            DB::beginTransaction();

            $attrs = $request->all();

            $user->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.profile.info'));
    }

    public function changePasswordUpdate(Request $request) {
        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ], [], [
            'old_password' => __('customer.old_password'),
            'password' => __('customer.password')
        ]);
        $user = Auth::user();
        $current_password = $user->password;
        $params = $request->all();
        if(!(Hash::check($params['old_password'], $current_password))){
            return redirect(route('admin.profile.change-password'))->with("error", __('user.password_not_current'));
        }
        if(strcmp($request->get('old_password'), $request->get('password')) == 0){
                //Current password and new password are same
            return redirect()->back()->with("error", __('user.new_not_current'));
        }
            try {
                DB::beginTransaction();
                $user->password = Hash::make($params['password']);
                $user->password_changed_at = Carbon::now()->toDateTimeString();
                $user->update();
                DB::commit();
                return redirect(route('admin.profile.change-password'))->with(['status' => 200]);
            } catch (Exception $e) {
                DB::rollBack();
                abort(500);
            }
    }
}
