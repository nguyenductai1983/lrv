<?php

namespace App\Http\Controllers\Admin\stockMutations;

trait ReferencedByStockMutations
{
    /**
     * Relation with StockMutation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphMany
     */
    public function stockMutations()
    {
        return $this->morphMany(Stockable::class, 'reference');
    }
    public function warehouse()
    {
        return $this->morphMany(Stockable::class, 'warehouse');
    }
}
