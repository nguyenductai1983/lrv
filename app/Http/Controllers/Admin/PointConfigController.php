<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PointConfig;
use App\Enum\PointEnum;
use App\Http\Controllers\Controller;
use Exception;
class PointConfigController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '2.14'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['pointConfig'] = PointConfig::where([])->orderBy('id', 'desc')->get();

        return view('admin.pointConfig.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pointConfig.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request) {
        $this->validate($request, [
            'point' => 'required|numeric',
            'type' => 'required|numeric'
                ], [], [
            'point' => __('point.value'),
            'type' => __('point.config_type')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();
            $params = $request->all();
            PointConfig::where(['type' => $params['type'], 'is_active' => PointEnum::IS_ACTIVE])->update(['is_active' => PointEnum::NOT_ACTIVE]);
            PointConfig::create($params);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.point-configs.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$pointConfig = PointConfig::find($id)) {
            abort(404);
        }
        $this->data['pointConfig'] = $pointConfig;

        return view('admin.pointConfig.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$pointConfig = PointConfig::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $pointConfig->id);

        try {
            DB::beginTransaction();

            $pointConfig->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.point-configs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$pointConfig = PointConfig::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $pointConfig->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.point-configs.index');
    }

}
