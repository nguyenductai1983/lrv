<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ShippingProvider;
use App\Http\Controllers\Controller;
use Exception;
class ShippingProviderController extends Controller
{
    //
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.11'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['shippingProviders'] = ShippingProvider::where('is_deleted', '=', 0)->orderBy('display_order')->get();

        return view('admin.shippingProvider.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shippingProvider.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'name'        => 'required|string',
            'description' => 'nullable|string',
            'display_order' => 'nullable|numeric'
        ], [], [
            'name'        => __('shipping.name'),
            'description'        => __('shipping.description'),
            'display_order'        => __('shipping.display_order'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            ShippingProvider::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-providers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$shippingProvider = ShippingProvider::find($id)) {
            abort(404);
        }
        $this->data['shippingProvider'] = $shippingProvider;

        return view('admin.shippingProvider.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$shippingProvider = ShippingProvider::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $shippingProvider->id);

        try {
            DB::beginTransaction();

            $shippingProvider->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-providers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$shippingProvider = ShippingProvider::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $shippingProvider->is_deleted = 1;
            $shippingProvider->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-providers.index');
    }
}
