<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agency;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;

class AgenciesController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '1.2'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['agencies'] = Agency::where('is_deleted', '=', 0)->select()->paginate(config('app.items_per_page'));

        return view('admin.agencies.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agencies.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null|integer $id
     */
    private function _validate(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'    => 'required|string|max:100|unique:agencies,code' . ($id ? (',' . $id . ',id') : ''),
            'name'    => 'required|string|max:100',
            'address' => 'nullable|string|max:255',
            'phone'   => 'nullable|string|max:20',
            'note'    => 'nullable|string|max:255'
        ], [], [
            'code'    => __('agency.code'),
            'name'    => __('agency.name'),
            'address' => __('agency.address'),
            'phone'   => __('agency.phone'),
            'note'    => __('agency.note'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);

        try {
            DB::beginTransaction();

            $agency = Agency::create($request->all());

            $products = Product::all(['id', 'sale_price', 'currency_id', 'weight_unit_id', 'messure_unit_id', 'weight']);
            if ($products->count()) {
                $syncData = [];
                foreach ($products as $product) {
                    $syncData[$product->id] = [
                        'price' => $product->sale_price,
                        'weight' => $product->weight,
                        'currency_id' => $product->currency_id,
                        'weight_unit_id' => $product->weight_unit_id,
                        'messure_unit_id' => $product->messure_unit_id,
                        'messure_unit_code' => isset($product->messureunits->code) ? $product->messureunits->code : null,
                        'currency_code' => isset($product->currencies->code) ? $product->currencies->code : null,
                    ];
                }
                $agency->products()->sync($syncData);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.agencies.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$agency = Agency::find($id)) {
            abort(404);
        }
        $this->data['agency'] = $agency;

        return view('admin.agencies.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$agency = Agency::find($id)) {
            abort(404);
        }

        $this->_validate($request, $agency->id);

        try {
            DB::beginTransaction();

            $agency->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.agencies.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$agency = Agency::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $agency->is_deleted = 1;
            $agency->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.agencies.index'));
    }

    /**
     * Get products for edit price
     *
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProducts($id)
    {
        if (!$agency = Agency::with('products')->find($id)) {
            abort(404);
        }
        $this->data['agency'] = $agency;

        return view('admin.agencies.products', $this->data);
    }

    /**
     * Update products price
     *
     * @param Request $request
     * @param integer $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateProductsPrice(Request $request, $id)
    {
        if (!$agency = Agency::find($id)) {
            abort(404);
        }

        $this->validate($request, [
            'products'         => 'required|array|distinct',
            'products.*.price' => 'required|numeric|min:0',
            'products.*.pickup_fee' => 'required|numeric|min:0'
        ], [], [
            'products'         => __('product.sale_price'),
            'products.*.price' => __('product.sale_price'),
            'products.*.pickup_fee' => __('product.pickup_fee')
        ]);

        $products = $request->get('products');
        $count = Product::whereIn('id', array_keys($products))->count();

        if ($count != count($products)) {
            return redirect()->back()
                             ->withInput()
                             ->withErrors(['products' => __('validation.exists', ['attribute' => __('product.manage')])]);
        }

        try {
            DB::beginTransaction();

            $agency->products()->sync($products);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->back()
                         ->with(['status' => __('label.data_updated')]);
    }
}
