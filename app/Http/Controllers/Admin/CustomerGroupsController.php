<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomerGroup;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;

class CustomerGroupsController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * CustomerGroupsController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '3.1'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['groups'] = CustomerGroup::orderBy('code')->get();

        return view('admin.customer-groups.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer-groups.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'        => 'required|string|max:20|unique:customer_groups,code' . ($id ? (',' . $id . ',id') : ''),
            'name'        => 'required|string|max:100',
            'description' => 'nullable|string'
        ], [], [
            'code'        => __('customer-group.code'),
            'name'        => __('customer-group.name'),
            'description' => __('customer-group.description')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            CustomerGroup::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customer-groups.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$group = CustomerGroup::find($id)) {
            abort(404);
        }
        $this->data['group'] = $group;

        return view('admin.customer-groups.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$group = CustomerGroup::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $group->id);

        try {
            DB::beginTransaction();

            $group->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customer-groups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!$group = CustomerGroup::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $group->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customer-groups.index');
    }
}
