<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    /**
     * Set config
     *
     * @param Request $request
     * @return array
     */
    public function config(Request $request)
    {
        foreach ($request->all() as $key => $val) {
            session()->put('config.' . $key, $val);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }
}
