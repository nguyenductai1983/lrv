<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Enum\OrderTypeEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Order;
use App\OrderItem;
use App\Address;
use App\Transaction;
use App\PaymentMethod;
// use App\Notification;
use Exception;

class QuoteController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => 'shopforme.1'
        ];
    }

    private function _validate(Request $request)
    {
        $this->validate($request, [
            'sender' => 'required|array',
            'sender.first_name' => 'required|required_without:customer.id|string|max:50',
            'sender.middle_name' => 'nullable|string|max:50',
            'sender.last_name' => 'required|required_without:customer.id|string|max:50',
            'sender.address_1' => 'required|required_without:customer.id|string|max:35',
            'sender.email' => 'required|required_without:customer.id|string|max:255',
            'sender.telephone' => 'required|required_without:customer.id|string|max:20',
            'sender.postal_code' => 'required|required_without:customer.id|string|max:20',
            'sender.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'sender.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'sender.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:35',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.cellphone' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',

            'package_type' => 'required|string',

            'containers' => 'required|array',
            'containers.*.url' => 'required|string',
            'containers.*.name' => 'required|string',
            'containers.*.quantity' => 'required|numeric|min:0',
            'containers.*.price' => 'required|numeric|min:0',
            'containers.*.shop_fee' => 'nullable|numeric|min:0',
            'containers.*.shipping_fee' => 'nullable|numeric|min:0',
            'containers.*.delivery_fee' => 'nullable|numeric|min:0',
            'containers.*.coupon' => 'nullable|string',
            'containers.*.note' => 'nullable|string|max:255',

            'last_paid_amount' => 'nullable|numeric|min:0',
            'last_date_pay' => 'nullable|date_format:' . config('app.date_format'),
            'pay_method' => 'nullable|string',
        ], [], [
            'sender.first_name' => __('sender.first_name'),
            'sender.middle_name' => __('sender.middle_name'),
            'sender.last_name' => __('sender.last_name'),
            'sender.address_1' => __('sender.address_1'),
            'sender.telephone' => __('sender.telephone'),
            'sender.postal_code' => __('sender.postal_code'),
            'sender.country_id' => __('sender.country_id'),
            'sender.province_id' => __('sender.province_id'),
            'sender.city_id' => __('sender.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),

            'package_type' => __('customer.qoute-type'),

            'containers' => __('label.containers'),
            'containers.*.url' => __('product.url'),
            'containers.*.name' => __('product.name'),
            'containers.*.quantity' => __('label.quantity_short'),
            'containers.*.price' => __('label.price'),
            'containers.*.shop_fee' => __('label.shop_fee'),
            'containers.*.shipping_fee' => __('label.shipping_fee'),
            'containers.*.delivery_fee' => __('label.delivery_fee'),
            'containers.*.coupon' => __('label.coupon'),
            'containers.*.note' => __('label.note'),

            'last_paid_amount' => __('label.total_pay'),
            'last_date_pay' => __('label.date_pay'),
            'pay_method' => __('label.pay_method'),
        ]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::QUOTE);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.quote.index', $this->data);
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $order = Order::where('id', '=', $id)->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        $data = $this->convertQuote($order);
        return response([
            'order' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        // $notification_id = $request->get('notification_id', 0);
        // if($notification_id > 0){
        //     Notification::where('id', $notification_id)->update(['is_read' => 1]);
        // }
        $order = Order::where('id', '=', $id)->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        $this->data['order'] = $order;

        return view('admin.quote.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $order = Order::where('id', '=', $request->route('id'))->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        $this->_validate($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $sender = $data['sender'];
            $receiver = $data['receiver'];
            if (isset($data['containers']) && !empty($data['containers'])) {
                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiverAddress = Address::where('id', $data['receiver']['id'])->first();
                        if (empty($receiverAddress)) {
                            $receiverAddress = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiverAddress->$property = $data['receiver'][$property];
                            }
                            $receiverAddress->save();
                        }else{
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiverAddress->$property = $data['receiver'][$property];
                            }
                            $receiverAddress->update();
                        }
                    } else {
                        $receiverAddress = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiverAddress->$property = $data['receiver'][$property];
                        }
                        $receiverAddress->customer_id = $order->customer_id;
                        $receiverAddress->save();
                    }
                }

                $order->sender_first_name = $sender['first_name'];
                $order->sender_middle_name = $sender['middle_name'];
                $order->sender_last_name = $sender['last_name'];
                $order->sender_phone = $sender['telephone'];
                $order->sender_address = $sender['address_1'];
                $order->sender_country_id = $sender['country_id'];
                $order->sender_province_id = $sender['province_id'];
                $order->sender_city_id = $sender['city_id'];
                $order->sender_post_code = $sender['postal_code'];

                $order->shipping_address_id = isset($receiverAddress->id) ? $receiverAddress->id : null;
                $order->receive_last_name = $receiver['last_name'];
                $order->receive_first_name = $receiver['first_name'];
                $order->receiver_middle_name = $receiver['middle_name'];
                $order->receiver_phone = $receiver['telephone'];
                $order->receiver_address = $receiver['address_1'];
                $order->receiver_address_2 = $receiver['address_2'];
                $order->receiver_country_id = $receiver['country_id'];
                $order->receiver_province_id = $receiver['province_id'];
                $order->receiver_city_id = $receiver['city_id'];
                $order->receiver_cellphone = $receiver['cellphone'];
                $order->receiver_post_code = $receiver['postal_code'];

                $order->total_final = 0;
                $order->total_goods = 0;
                $order->total_shop_fee = 0;
                $order->total_shipping_fee = 0;
                $order->total_delivery_fee = 0;
                $order->payment_method = $data['pay_method'];
                $order->user_note = $data['user_note'];
                $order->package_type = isset($data['package_type']) ? $data['package_type'] : 1;

                OrderItem::where('order_id', '=', $order->id)->update(['is_delete' => 1]);

                foreach ($data['containers'] as $product) {
                    if (isset($product['id']) && !empty($product['id'])) {
                        $orderItem = OrderItem::find($product['id']);
                    } else {
                        $orderItem = new OrderItem();
                    }
                    $orderItem->order_id = $order->id;
                    $orderItem->name = $product['url'];
                    $orderItem->name = $product['name'];
                    $orderItem->quantity = $product['quantity'];
                    $orderItem->unit_goods = $product['price'];
                    $orderItem->sub_total_goods = $orderItem->unit_goods * $orderItem->quantity;
                    $orderItem->shop_fee = $product['shop_fee'];
                    $orderItem->shipping_fee = $product['shipping_fee'];
                    $orderItem->delivery_fee = $product['delivery_fee'];
                    $orderItem->sub_total_final = $orderItem->sub_total_goods + $orderItem->shop_fee + $orderItem->shipping_fee + $orderItem->delivery_fee;
                    $orderItem->coupon = $product['coupon'];
                    $orderItem->user_note = $product['note'];
                    $orderItem->is_delete = 0;
                    $orderItem->save();

                    $order->total_goods += $orderItem->sub_total_goods;
                    $order->total_shop_fee += $orderItem->shop_fee;
                    $order->total_shipping_fee += $orderItem->shipping_fee;
                    $order->total_delivery_fee += $orderItem->delivery_fee;
                }

                $order->total_final = $order->total_goods + $order->total_shop_fee + $order->total_shipping_fee + $order->total_delivery_fee;

                if($data['last_paid_amount'] > 0 && !empty($data['last_date_pay'])){
                    $count = Transaction::where('order_id', $order->id)->count();

                    $transaction = new Transaction();
                    $transaction->customer_id = $order->customer_id;
                    $transaction->order_id = $order->id;
                    $transaction->currency_id = $order->currency_id;
                    $transaction->code = "PT_" . $order->code . "_{$count}";
                    $transaction->transaction_at = ConvertsUtil::dateFormat($data['last_date_pay'], config('app.date_format'));
                    $transaction->type = TransactionTypeEnum::RECEIPT;
                    $transaction->credit_amount = $data['last_paid_amount'];
                    $transaction->goods_code = $order->code;
                    $transaction->save();

                    $order->total_paid_amount += $data['last_paid_amount'];
                    $order->total_remain_amount = $order->total_final - $order->total_paid_amount > 0 ? $order->total_final - $order->total_paid_amount : 0;
                    if($order->total_remain_amount == 0){
                        $order->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                    }else{
                        $order->payment_status = OrderPaymentStatusEnum::PART_PAID;
                    }
                    $order->last_payment_at = ConvertsUtil::dateFormat($data['last_date_pay'], config('app.date_format'));
                }
                $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
                $order->update();

                $payment_method = PaymentMethod::where(["code" => $order->payment_method])->first();
                Mail::send('email.approved-quote', ['order' => $order, 'payment_method' => $payment_method], function ($message) use ($order) {
                    $message->to($order->sender_email, $order->sender_email)->subject(__('email.approved-quote'));
                });

                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function cancel(Request $request){
        $order = Order::where('id', '=', $request->route('id'))->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();
            $order->order_status = OrderStatusEnum::STATUS_CANCEL;
            $order->update();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    private function convertQuote (Order $order) {
        $order_items = $order->order_items()->where('is_delete', '=', 0)->get();
        $containers = [];
        foreach($order_items as $order_item){
            $product['id'] = $order_item->id;
            $product['url'] = $order_item->product_url;
            $product['name'] = $order_item->name;
            $product['quantity'] = $order_item->quantity;
            $product['price'] = !empty($order_item->unit_goods) ? $order_item->unit_goods : 0;
            $product['shop_fee'] = !empty($order_item->shop_fee) ? $order_item->shop_fee : 0;
            $product['shipping_fee'] = !empty($order_item->shipping_fee) ? $order_item->shipping_fee : 0;
            $product['delivery_fee'] = !empty($order_item->delivery_fee) ? $order_item->delivery_fee : 0;
            $product['coupon'] = $order_item->coupon;
            $product['note'] = $order_item->user_note;

            $containers[] = $product;
        }
        $data = [];
        $data['sender'] = array(
            'id' => $order->customer_id,
            'first_name' => $order->sender_first_name,
            'middle_name' => $order->sender_middle_name,
            'last_name' => $order->sender_last_name,
            'telephone' => $order->sender_phone,
            'address_1' => $order->sender_address,
            'email' => $order->sender_email,
            'country_id' => $order->sender_country_id,
            'province_id' => $order->sender_province_id,
            'city_id' => $order->sender_city_id,
            'postal_code' => $order->sender_post_code,
            'address_2' => $order->sender_address_2,
        );
        $data['receiver'] = array(
            'first_name' => $order->receive_first_name,
            'middle_name' => $order->receiver_middle_name,
            'last_name' => $order->receive_last_name,
            'telephone' => $order->receiver_phone,
            'cellphone' => $order->receiver_cellphone,
            'address_1' => $order->receiver_address,
            'country_id' => $order->receiver_country_id,
            'province_id' => $order->receiver_province_id,
            'city_id' => $order->receiver_city_id,
            'postal_code' => $order->receiver_post_code,
            'address_2' => $order->receiver_address_2,
        );
        $data['package_type'] = strval($order->package_type);
        $data['total_final'] = $order->total_final;
        $data['total_goods'] = !empty($order->total_goods) ? $order->total_goods : 0;
        $data['total_shop_fee'] = !empty($order->total_shop_fee) ? $order->total_shop_fee : 0;
        $data['total_shipping_fee'] = !empty($order->total_shipping_fee) ? $order->total_shipping_fee : 0;
        $data['total_delivery_fee'] = !empty($order->total_delivery_fee) ? $order->total_delivery_fee : 0;
        $data['total_paid_amount'] = $order->total_paid_amount;
        $data['pay_method'] = !empty($order->payment_method) ? $order->payment_method : 'VIETCOMBANK';
        $data['last_payment_at'] = $order->last_payment_date;
        $data['payment_status'] = $order->payment_status_name;
        $data['user_note'] = $order->user_note;
        $data['last_paid_amount'] = 0;
        $data['last_date_pay'] = null;
        $data['containers'] = $containers;

        return $data;
    }
}
