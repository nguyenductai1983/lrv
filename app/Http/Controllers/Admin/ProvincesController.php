<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Province;
use App\Country;
use App\Http\Controllers\Controller;
use Exception;
use App\Services\LogService;

class ProvincesController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * ProvincesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '2.8'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array | \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax() || $request->wantsJson()) {
            $provinces = Province::where('country_id', '=', $request->get('country_id'))
                                 ->orderBy('name')
                                 ->get();

            return [
                'provinces' => $provinces
            ];
        }
        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        $provinces = Province::join('countries', 'provinces.country_id', '=', 'countries.id');

        if ($countryId = $request->query('country_id')) {
            $provinces = $provinces->where('provinces.country_id', '=', $countryId);
        }

        $this->data['provinces'] = $provinces->orderBy('countries.code')->orderBy('provinces.name')->select([
            'provinces.*',
            'countries.name AS country_name'
        ])->paginate(config('app.items_per_page'));

        return view('admin.provinces.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        return view('admin.provinces.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     */
    private function validateRequest(Request $request) {

        $this->validate($request, [
            'country_id' => 'required|exists:countries,id',
            'name' => 'required|string|max:100',
            'postal_code' => 'nullable|string|max:20',
                ], [], [
            'country_id' => __('province.country_id'),
            'name' => __('province.name'),
            'postal_code' => __('label.post_code')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validateRequest($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $province = new Province();
            $province->country_id = $data["country_id"];
            $province->name = $data["name"];
            $province->code = $data["name"];
            $province->is_active = 1;
            $province->postal_code = $data["postal_code"];
            $province->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.provinces.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$province = Province::find($id)) {
            abort(404);
        }
        $this->data['province'] = $province;

        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        return view('admin.provinces.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!$province = Province::find($id)) {
            abort(404);
        }

        $this->validateRequest($request);

        try {
            DB::beginTransaction();
            $data = $request->all();
            $province->country_id = $data["country_id"];
            $province->postal_code = $data["postal_code"];
            $province->name = $data["name"];
            $province->code = $data["name"];
            $province->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.provinces.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (!$province = Province::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $province->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.provinces.index');
    }

}
