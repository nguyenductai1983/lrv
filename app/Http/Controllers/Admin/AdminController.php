<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Page;


class AdminController extends Controller
{
    private $data;

    public function __construct()
    {
        $this->data = [
            'menu' => 'dashboard'
        ];
    }
    /**
     * Index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index()
    {
        $user = Auth::user();
        //test
        $query = Page::where([]);
        $agency=$user->agency;
        $query->leftJoin('agencie_page', 'pages.id', '=', 'agencie_page.page_id');
        $query->where('agencie_page.agencie_id','=',$agency->id);
        $list_page = $query->orderBy('pages.id', 'desc')->select()->paginate(config('app.page_5'));
        //end test
    //   $list_page = $user->agency->pages->take(5);

        $this->data['pages'] = $list_page;
        return view('admin.index', $this->data);
    }
}
