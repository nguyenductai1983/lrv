<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Area;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Services\LogService;

class AreasController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.1'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['areas'] = Area::orderBy('code')->get();

        return view('admin.areas.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.areas.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'        => 'required|string|max:20|unique:areas,code' . ($id ? (',' . $id . ',id') : ''),
            'name'        => 'required|string|max:100',
            'description' => 'nullable|string'
        ], [], [
            'code'        => __('area.code'),
            'name'        => __('area.name'),
            'description' => __('area.description')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            Area::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.areas.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$area = Area::find($id)) {
            abort(404);
        }
        $this->data['area'] = $area;

        return view('admin.areas.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$area = Area::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $area->id);

        try {
            DB::beginTransaction();

            $area->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.areas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$area = Area::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $area->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.areas.index');
    }
}
