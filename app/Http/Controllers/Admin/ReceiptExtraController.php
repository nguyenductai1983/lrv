<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExportQuery;
use Illuminate\Support\Facades\Validator;
use App\Enum\VoucherStatusEnum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\OrderExtraRefund;
use App\Enum\ExtraRefundStatusEnum;
use App\Voucher;
use App\User;
use Exception;
class ReceiptExtraController extends Controller {

    private $data;

    public function __construct() {
        $this->data = [
            'menu' => 'receipt.4'
        ];
    }

    public function index(Request $request) {
        $columns = [
            'tbl_voucher.id',
            'tbl_voucher.code',
            'tbl_voucher.status',
            'tbl_voucher.amount',
            'tbl_voucher.discount_amount',
            'tbl_voucher.paid_amount',
            'tbl_voucher.remain_amount',
            'tbl_voucher.created_at',
            'tbl_voucher.last_paid_at',
            'users.code as user_code',
        ];

        $query = DB::table('tbl_voucher')->leftJoin('users', 'tbl_voucher.user_id', '=', 'users.id');
        $user = Auth::user();
        if(strtoupper($user->role->name) != "ADMIN"){
            $query->where('tbl_voucher.user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_voucher.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_voucher.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_id'))) {
            $query->where('users.id', '=', $request->query('user_id'));
        }
        $query->where('tbl_voucher.type', '=', VoucherStatusEnum::TYPE_EXTRA);
        $query->where('tbl_voucher.is_deleted', '=', 0);
        $this->data['receipts'] = $query->orderBy('tbl_voucher.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['total_amount'] = $query->sum('tbl_voucher.amount');
        $this->data['total_agency_discount'] = $query->sum('tbl_voucher.discount_amount');
        $this->data['total_paid_amount'] = $query->sum('tbl_voucher.paid_amount');
        $this->data['total_remain_amount'] = $query->sum('tbl_voucher.remain_amount');
        $this->data['users'] = User::where([])->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-extra.index', $this->data);
    }

    public function exportExcel(Request $request)
    {
        $query = Voucher::leftJoin('users', 'tbl_voucher.user_id', '=', 'users.id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_voucher.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_voucher.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_id'))) {
            $query->where('users.id', '=', $request->query('user_id'));
        }
        $query->where('tbl_voucher.type', '=', 1);
        $query->where('tbl_voucher.is_deleted', '=', 0);
        $query->orderBy('tbl_voucher.id', 'desc')->select('tbl_voucher.code',
            'tbl_voucher.amount',
            'tbl_voucher.discount_amount',
            'tbl_voucher.paid_amount',
            'tbl_voucher.remain_amount',
            'tbl_voucher.created_at',
            'users.code as user_code');
            return (new ExportQuery($query))->download('extra_receipts.xlsx');
    }

    public function printItem($id)
    {
        if (!$voucher = Voucher::find($id)) {
            abort(404);
        }
        $query = OrderExtraRefund::where([]);
        $query->where('voucher_id', '=', $id);
        $this->data['extras'] = $query->orderBy('id', 'desc')->get();
        $this->data['voucher'] = $voucher;
        return view('admin.receipt-extra.print', $this->data);
    }

    public function create(Request $request) {
        $query = OrderExtraRefund::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->name) != "ADMIN"){
            $query->where('user_id', '=', $user->id);
            $this->data['users'] = User::where('id',$user->id)->get();
        }
        else
        {
            $this->data['users'] = User::where([])->orderBy('code', 'asc')->get();
            if(!empty($request->query('user_id'))){
                $query->where('user_id', '=', $request->query('user_id'));
            }
        }
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        $query->where('type', '=', ExtraRefundStatusEnum::TYPE_EXTRA);
        $query->where('status', '!=', ExtraRefundStatusEnum::STATUS_CANCEL);
        $query->whereNull('voucher_id');
        $this->data['extra'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.page_50'));

        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-extra.create', $this->data);
    }

    public function exportExcelItem($id)
    {
        $voucherData = Voucher::find($id);
        if (empty($voucherData)) {
            abort(404);
        }
        $query = OrderExtraRefund::where([]);
        $query->where('voucher_id', '=', $id);

      $query->orderBy('tbl_extra_refund.id', 'desc')->select('tbl_extra_refund.code',
            'tbl_extra_refund.amount'
            );
            return (new ExportQuery($query))->download('extra_receipts'.date('Ymd').'.xlsx');
    }

    public function edit($id)
    {
        if (!$voucher = Voucher::find($id)) {
            abort(404);
        }
        $query = OrderExtraRefund::where([]);
        $query->where('voucher_id', '=', $id);
        $this->data['extras'] = $query->orderBy('id', 'desc')->get();
        $this->data['voucher'] = $voucher;

        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-extra.view', $this->data);
    }

    public function addItems(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }

        $userData = User::find($request->get('user_id'));
        if (empty($userData)) {
            return response(["success" => false, "message" => __('message.err_user_no_exist'), "data" => []]);
        }

        $items = $request->get('items');
        if (empty($items)) {
            return response(["success" => false, "message" => __('message.err_receipt_empty'), "data" => []]);
        }
        $ExtraIds = [];
        foreach ($items as $item) {
            $ExtraIds[] = $item['value'];
        }
        $ExtraData = OrderExtraRefund::find($ExtraIds);
        if (empty($ExtraData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }

        try {
            DB::beginTransaction();

            $voucher = new Voucher();
            $voucher->create_user_id = $user->id;
            $voucher->user_id = $userData->id;
            $voucher->type = VoucherStatusEnum::TYPE_EXTRA;
            $voucher->save();

            $totalAmount = 0;
            $totalDiscountAmount = 0;
            foreach ($ExtraData as $extra) {
                $extra->voucher_id = $voucher->id;
                $extra->update();
                $totalAmount += $extra->amount;
            }
            $voucher->code = ConvertsUtil::getVoucherCode($userData->code, $voucher->id,"EXTRA");
            $voucher->amount = $totalAmount;
            $voucher->discount_amount = $totalDiscountAmount;
            $voucher->remain_amount = $voucher->amount - $voucher->discount_amount;
            $voucher->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.add_voucher_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function removeItem(Request $request) {
        $validator = Validator::make($request->all(), [
            'extra_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $extraData = OrderExtraRefund::find($request->get('extra_id'));
        if (empty($extraData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }

        $voucherData = Voucher::find($extraData->voucher_id);
        if (empty($voucherData)) {
            return response(["success" => false, "message" => __('message.err_voucher_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }

        try {
            DB::beginTransaction();

            $extraData->voucher_id = NULL;
            $extraData->update();

            $voucherData->amount -= $extraData->total_final;
            $voucherData->discount_amount -= $extraData->discount_agency_amount;
            $remain_amount = $extraData->total_final - $extraData->discount_agency_amount;
            if($remain_amount > 0){
                $voucherData->remain_amount -= $remain_amount;
            }
            $voucherData->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.remove_item_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function destroy($id) {
        $voucherData = Voucher::find($id);
        if (empty($voucherData)) {
            abort(404);
        }
        try {
            DB::beginTransaction();

            DB::table('tbl_extra_refund')->where('voucher_id', $id)->update(['voucher_id' => NULL,'status' =>ExtraRefundStatusEnum::STATUS_APPROVE]);
            $voucherData->is_deleted = 1;
            $voucherData->update();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.receipt-extra.index');
    }

    public function paidVoucher(Request $request) {
        $validator = Validator::make($request->all(), [
            'voucher_id' => 'required',
            'amount' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $voucherData = Voucher::find($request->get('voucher_id'));
        if (empty($voucherData)) {
            return response(["success" => false, "message" => __('message.err_voucher_no_exist'), "data" => []]);
        }
        $ExtraData = OrderExtraRefund::where('voucher_id','=',$voucherData->id)->get();
        if (empty($ExtraData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $voucherData->paid_amount += $request->get('amount');
            $remain_amount = $voucherData->amount - ($voucherData->discount_amount + $voucherData->paid_amount);
            if($remain_amount <= 0 ){
                $voucherData->remain_amount = 0;
                $voucherData->status = 2;
            }else{
                $voucherData->remain_amount = $remain_amount;
                $voucherData->status = 1;
            }
            $voucherData->update();
            foreach($ExtraData as $extra)
            {
                $extra->status=ExtraRefundStatusEnum::STATUS_COMPLETE;
                $extra->update();
            }
            DB::commit();

            return response(["success" => true, "message" => __('message.paid_voucher_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
}
