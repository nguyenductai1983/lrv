<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Currency;
use App\CurrencyRate;
use App\Services\LogService;

class CurrenciesController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * CurrenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.6'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currencies = Currency::all();

        if ($request->ajax() || $request->wantsJson()) {
            return response([
                'currencies' => $currencies
            ]);
        }

        $this->data['currencies'] = $currencies;

        return view('admin.currencies.index', $this->data);
    }

    public function rate($id) {
        if (!$currency = Currency::find($id)) {
            abort(404);
        }

        $query = CurrencyRate::where(['currency_from_id' => $id]);

        $this->data['rates'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['currency'] = $currency;

        return view('admin.currencies.rate', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.currencies.create', $this->data);
    }

    private function _validate(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'         => 'required|string|max:20|unique:currencies,code,' . ($id ? ($id . ',id') : ''),
            'name'         => 'required|string|max:50',
        ], [], [
            'code'         => __('currency.code'),
            'name'         => __('currency.name')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);

        try {
            DB::beginTransaction();
            $data = $request->all();
            $currency = new Currency();
            $currency->name = $data["name"];
            $currency->code = $data["code"];
            $currency->is_active = 1;
            $currency->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.currencies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$currency = Currency::find($id)) {
            abort(404);
        }

        $this->data['currency'] = $currency;

        return view('admin.currencies.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$currency = Currency::find($id)) {
            abort(404);
        }

        $this->_validate($request, $currency->id);

        try {
            DB::beginTransaction();

            $currency->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.currencies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$currency = Currency::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $currency->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.currencies.index');
    }
}
