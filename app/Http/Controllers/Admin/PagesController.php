<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Components\LanguageUtil;
use App\Page;
use App\PageTranslate;
use App\Services\LogService;

class PagesController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * Page type
     */
    private $type;

    /**
     * MenuHeaderController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->type = $request->query('type');

        $menu = '10.1';
        if ($this->type == config('page.type_footer')) {
            $menu = '10.2';
        }

        $this->data['type'] = $this->type;
        $this->data['menu'] = $menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pages = Page::with('translate')
                ->where('type', '=', $this->type)
                ->orderBy('order', 'asc')
                ->get();

        $this->data['pages'] = $pages;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('admin.pages.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$page = Page::with('translates')->find($id)) {
            abort(404);
        }
        $this->data['page'] = $page;

        $pages = Page::with('translate')
                ->where('id', '!=', $page->id)
                ->where('type', '=', $this->type)
                ->orderBy('order', 'asc')
                ->get();
        $this->data['pages'] = $pages;

        return view('admin.pages.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!$page = Page::find($id)) {
            abort(404);
        }

        $rules = [
            'parent_id' => 'required',
            'link_to' => 'nullable|string|max:100'
        ];

        if ($request->get('parent_id') != 0) {
            $rules['parent_id'] .= '|exists:pages,id';
        }

        $customAttributes = [
            'parent_id' => __('page.parent'),
            'link_to' => __('page.link_to'),
        ];
        foreach (config('app.locales') as $locale => $localeName) {
            $rules['name_' . $locale] = 'required|string|max:100';
            $customAttributes['name_' . $locale] = __('page.name');
            $rules['detail_' . $locale] = 'nullable|string';
            $customAttributes['detail_' . $locale] = __('page.detail');
        }

        $this->validate($request, $rules, [], $customAttributes);

        try {
            DB::beginTransaction();

            $locale = app()->getLocale();
            $name = $request->get('name_' . $locale);
            $detail = $request->get('detail_' . $locale);
            $attrs = [
                'slug' => $detail ? (str_slug($name) . '-' . $page->id) : null,
                'link_to' => $request->get('link_to'),
                'parent_id' => $request->get('parent_id')
            ];
            $page->update($attrs);

            foreach (config('app.locales') as $locale => $localeName) {
                PageTranslate::where('page_id', '=', $page->id)
                        ->where('lang', '=', $locale)
                        ->update([
                            'name' => $request->get('name_' . $locale),
                            'detail' => $request->get('detail_' . $locale) ?: null
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.pages.index', ['type' => $this->type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            DB::beginTransaction();
            PageTranslate::where('page_id', '=', $id)->delete();

            $page = Page::find($id);
            if (empty($page)) {
                return response(["success" => false, "message" => __('message.del_data_fail'), "data" => []]);
            }
            $page->delete();
            DB::commit();
            return response(["success" => true, "message" => __('message.del_data_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => __('message.del_data_fail'), "data" => []]);
        }
    }

}
