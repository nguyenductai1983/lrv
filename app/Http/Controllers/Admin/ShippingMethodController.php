<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ShippingMethod;
use App\Http\Controllers\Controller;
// use Illuminate\Validation\Rule;
use Exception;
class ShippingMethodController extends Controller
{
    //
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.15'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['shippingMethods'] = ShippingMethod::where('is_deleted', '=', 0)->orderBy('display_order')->get();

        return view('admin.shippingMethod.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shippingMethod.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'name'        => 'required|string',
            'description' => 'nullable|string',
            'display_order' => 'nullable|numeric',
            'is_active' => 'nullable|numeric',
            'image_file_id'     => ['nullable',
        //     Rule::exists('files', 'id')->where(function ($query) {
        //         $query->where('created_user_id', '=', auth()->user()->id);
        //     })
        ],
        ], [], [
            'name'        => __('shipping.name'),
            'description'        => __('shipping.description'),
            'display_order'        => __('shipping.display_order'),
            'is_active'        => __('shipping.is_active'),
            'is_active'        => __('shipping.logo'),
            'image_file_id'     => __('payment.image_file_id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            ShippingMethod::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-methods.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$shippingMethod = ShippingMethod::find($id)) {
            abort(404);
        }
        $this->data['shippingMethod'] = $shippingMethod;

        return view('admin.shippingMethod.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$shippingMethod = ShippingMethod::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $shippingMethod->id);

        try {
            DB::beginTransaction();

            $shippingMethod->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-methods.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$shippingMethod = ShippingMethod::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $shippingMethod->is_deleted = 1;
            $shippingMethod->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.shipping-methods.index');
    }
}
