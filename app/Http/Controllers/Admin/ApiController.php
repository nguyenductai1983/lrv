<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Services\LogService;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Ward;
use App\City;
use App\Country;
use App\DimensionUnit;
use App\Province;
use App\Warehouse;
use App\PaymentMethod;
use App\Customer;
use App\Address;
use App\OrderExtraRefund;
use App\Order;
use App\Transport;
use App\Mts;
use App\Currency;
use App\Agency;
use App\User;
use App\Enum\WarehouseEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\PaymentMethodEnum;
use App\Enum\CustomerGroupEnum;
use App\Services\CampaignService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRequest;

class ApiController extends Controller
{
    public function getInfoCustomer($id)
    {
        $data = [];
        $data['customer'] = Customer::where('id', $id)->first();
        $data['countries'] = Country::where('is_active', 1)->get();
        return response(["success" => true, "message" => 'Susscess', "data" => $data]);
    }

    public function getInfoReceiver($id)
    {
        $data = [];
        $data['address'] = Address::where('id', $id)->first();
        $data['countries'] = Country::where('is_active', 1)->get();
        return response(["success" => true, "message" => 'Susscess', "data" => $data]);
    }

    public function updateInfoCustomer(Request $request)
    {
        $data = $request->all();
        if (!isset($data['customer_id'])) {
            return response(["success" => false, "message" => __('message.err_sys'), "data" => []]);
        }
        $customer = Customer::where('id', $data['customer_id'])->first();
        if (empty($customer)) {
            return response(["success" => false, "message" => __('message.err_sys'), "data" => []]);
        }
        $customer->update($data);

        return response(["success" => true, "message" => 'Susscess', "data" => $customer]);
    }

    public function updateInfoReceiver(Request $request)
    {
        $data = $request->all();
        if (!isset($data['address_id'])) {
            return response(["success" => false, "message" => __('message.err_sys'), "data" => []]);
        }
        $address = Address::where('id', $data['address_id'])->first();
        if (empty($address)) {
            return response(["success" => false, "message" => __('message.err_sys'), "data" => []]);
        }
        $address->update($data);
        if (isset($data['id']) && !empty($data['id'])) {
            $order = Order::where('id', $data['id'])->first();
            $order->receive_first_name = $address->first_name;
            $order->receiver_middle_name = $address->middle_name;
            $order->receive_last_name = $address->last_name;
            $order->receiver_address = $address->address_1;
            $order->receiver_country_id = $address->country_id;
            $order->receiver_province_id = $address->province_id;
            $order->receiver_city_id = $address->city_id;
            $order->receiver_post_code = $address->postal_code;
            $order->receiver_phone = $address->telephone;
            $order->receiver_email = $address->email;
            $order->receiver_cellphone = $address->cellphone;
            $order->save();
        }

        return response(["success" => true, "message" => 'Susscess', "data" => $address]);
    }
    public function createContact(CreateRequest $request)
    {
        $data = $request->all();
        $customer_new  = $data['customer'];
        $address_new  = $data['address'];
        $Updatecustomer  = $data['Updatecustomer'];
        $Updatereciver  = $data['Updatereciver'];
        // nếu gửi người gửi là kháchàng mới
        if (empty($customer_new['id'])) {
            // kiem tao moi nguoi gui
            $customer_new['customer_group_id'] = 1;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            try {
                DB::beginTransaction();
                $customerExist = null;
                // kiểm tra email có tồn tại chưa
                if (!empty($customer_new['email'])) {
                    $customerExist = Customer::where('email', $customer_new['email'])->first();
                }
                if (!empty($customerExist)) {
                    // email da dang ki trong he thong
                    return response(__('message.cus_email_exist'), 500);
                }
                $customer_new['agency_id'] = $agency->id;
                $customer_new['customer_group_id'] = CustomerGroupEnum::CUS_SHP;
                $customer = Customer::create($customer_new);
                $customer->update(['code' => auth()->user()->code . '_' . $customer->id]);
                $address_new['customer_id'] = $customer->id;
                $receiver = Address::create($address_new);
                $address_new = $receiver;
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                LogService::error($e);
                abort(500);
            }
            // thu hien lenh tao nguoi moi, nguoi nhan moi
        }   // het tao mới người gửi
        else if (!$Updatecustomer) {
            try {
                DB::beginTransaction();
                $customer = Customer::where('id', $data['customer']['id'])->first();
                if (!empty($customer['email'])) {
                    // kiem tra neu email da ton tai thi khong cap nhat nua
                    unset($customer_new['email']);
                }
                $customer->update($customer_new);
                // xử lý người nhận
                if (!$Updatereciver) {
                    if (empty($address_new['id'])) {
                        $address_new['customer_id'] = $customer->id;
                        $receiver = Address::create($address_new);
                    } else {
                        $receiver = Address::where('id', $address_new['id'])->first();
                        if (empty($receiver['id'])) {
                            $receiver->update($address_new);
                        }
                    }
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                LogService::error($e);
                abort(500);
            }
        }
        // không cập nhật người gửi
        else {
            if (!$Updatereciver) {
                try {
                    DB::beginTransaction();
                    if (empty($address_new['id'])) {
                        $address_new['customer_id'] = $customer_new['id'];
                        $receiver = Address::create($address_new);
                    } else {
                        $receiver = Address::where('id', $address_new['id'])->first();
                        if (!empty($receiver['id'])) {
                            $receiver->update($address_new);
                        }
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    LogService::error($e);
                    abort(500);
                }
            }
        }
        return response(["success" => true, "message" => 'Susscess', 'customer' => isset($customer) ? $customer : $customer_new, 'address' => $address_new]);
    }

    public function createExtra(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'order_id' => 'required|integer',
            'amount' => 'required|numeric',
            'reason' => 'required|string'
        ], [], [
            'order_id' => __('label.order_id'),
            'amount' => __('label.amount'),
            'reason' => __('label.reason')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [], "data" => []]);
        }
        $user = Auth::user();
        $orderExtraRefund = new OrderExtraRefund();
        $orderExtraRefund->order_id = $data['order_id'];
        $user_own = Order::find($data['order_id']);
        $orderExtraRefund->user_id = $user_own->user_id;
        $orderExtraRefund->amount = $data['amount'];
        $orderExtraRefund->type = 1;
        $orderExtraRefund->status = 1;
        $orderExtraRefund->reason = $data['reason'];
        $orderExtraRefund->create_user_id = $user->id;

        $orderExtraRefund->save();

        $orderExtraRefund->code = substr($user_own->code, 0, 3) . sprintf('%04d', $orderExtraRefund->id);
        $orderExtraRefund->update();
        return response(["success" => true, "message" => __('message.extra-susscess'), "errors" => [], "data" => $orderExtraRefund]);
    }

    public function createRefund(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'order_id' => 'required|integer',
            'amount' => 'required|numeric',
            'reason' => 'required|string'
        ], [], [
            'order_id' => __('label.order_id'),
            'amount' => __('label.amount'),
            'reason' => __('label.reason')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [], "data" => []]);
        }
        $user = Auth::user();

        $orderExtraRefund = new OrderExtraRefund();
        $user_own = Order::find($data['order_id']);
        $orderExtraRefund->user_id = $user_own->user_id;
        $orderExtraRefund->order_id = $data['order_id'];
        $orderExtraRefund->amount = $data['amount'];
        $orderExtraRefund->type = 2;
        $orderExtraRefund->status = 1;
        $orderExtraRefund->reason = $data['reason'];
        $orderExtraRefund->create_user_id = $user->id;
        $orderExtraRefund->save();

        $orderExtraRefund->code = "REFUND" . sprintf('%04d', $orderExtraRefund->id);
        $orderExtraRefund->update();
        return response(["success" => true, "message" => __('message.refund-susscess'), "errors" => [], "data" => $orderExtraRefund]);
    }

    public function couponAmount(Request $request)
    {
        $data = $request->all();
        $amount = CampaignService::getAmountCoupon($data['container']['coupon_code'], $data['customer']['id'], $data['container']['total_weight']);
        return response([
            'amount' => $amount
        ]);
    }

    public function getWarehouses()
    {
        $warehouses = Warehouse::where("type", WarehouseEnum::TYPE_FOREIGN)->get();
        return response([
            'warehouses' => $warehouses
        ]);
    }

    public function getCountries()
    {
        $countries = Country::where("is_active", 1)->orderBy("name", 'asc')->get(['id', 'name', 'code']);
        return response([
            'countries' => $countries
        ]);
    }
    public function getCountriesCode(Request $request)
    {
        $countriesid = $request->query('id');
        $countries = Country::where("is_active", 1)->where("id", $countriesid)->get(['code']);
        return response([
            'countriescode' => $countries
        ]);
    }
    public function getProvinces(Request $request)
    {
        $countryId =  $request->query('country_id');
        $provinces = Province::where("is_active", 1)->where("country_id", $countryId)->orderBy("name", 'asc')->get();
        return response([
            'provinces' => $provinces
        ]);
    }
    public function getCities(Request $request)
    {
        $provinceId = $request->query('province_id');
        $countryId = $request->query('country_id');
        $cities = City::where("is_active", 1)->where("country_id", $countryId)
            ->where("province_id", $provinceId)->orderBy("name", 'asc')->get();
        return response([
            'cities' => $cities
        ]);
    }
    public function getWards(Request $request)
    {
        $cityId = $request->query('city_id');
        $Wards = Ward::where("is_active", 1)->where("city_id", $cityId)
            ->orderBy("name", 'asc')->get();
        return response([
            'wards' => $Wards
        ]);
    }
    public function getDimTypes()
    {
        $dimTypes = DimensionUnit::where([])->orderBy("display_order", 'asc')->get();
        return response([
            'dimTypes' => $dimTypes
        ]);
    }

    public function getPaymentMethods(Request $request)
    {
        $col = [
            'id',
            'name',
            'code'
        ];
        $type =  $request->query('type');
        $methods = PaymentMethod::where("is_active", PaymentMethodEnum::STATUS_ACTIVE)
            ->where("type", $type)->get($col);
        return response([
            'methods' => $methods
        ]);
    }

    public function getCustomers(Request $request)
    {
        $query = Customer::select();
        $query->where('is_delete', '=', 0);
        if (!empty($request->query('agency'))) {
            $query->where('agency_id', '=', $request->query('agency'));
        }
        if (!empty($request->query('group'))) {
            $query->where('customer_group_id', '=', $request->query('group'));
        }
        if (!empty($request->query('telephone'))) {
            $query->where('telephone', 'like', "%{$request->query('telephone')}%");
        }
        if (!empty($request->query('code'))) {
            $query->where('code', 'like', "%{$request->query('code')}%");
        }
        if (!empty($request->query('first_name'))) {
            $query->where('first_name', 'like', "%{$request->query('first_name')}%");
        }
        if (!empty($request->query('middle_name'))) {
            $query->where('middle_name', 'like', "%{$request->query('middle_name')}%");
        }
        if (!empty($request->query('last_name'))) {
            $query->where('last_name', 'like', "%{$request->query('last_name')}%");
        }
        $query->with(['image1', 'image2', 'image3']);
        return response(['customers' => $query->orderBy('id', 'desc')->paginate(config('app.items_per_page'))]);
    }

    public function getAddresses(Request $request)
    {
        $query = Address::select();
        $query->where('is_deleted', '=', 0);
        if (!empty($request->query('customer_id'))) {
            $query->where('customer_id', '=', $request->query('customer_id'));
        }
        if (!empty($request->query('phone_number'))) {
            $query->where('phone_number', 'like', "%{$request->query('phone_number')}%");
        }
        if (!empty($request->query('postal_code'))) {
            $query->where('postal_code', 'like', "%{$request->query('postal_code')}%");
        }
        if (!empty($request->query('first_name'))) {
            $query->where('first_name', 'like', "%{$request->query('first_name')}%");
        }
        if (!empty($request->query('middle_name'))) {
            $query->where('middle_name', 'like', "%{$request->query('middle_name')}%");
        }
        if (!empty($request->query('last_name'))) {
            $query->where('last_name', 'like', "%{$request->query('last_name')}%");
        }
        return response([
            'addresses' => $query->orderBy('id', 'desc')->paginate(config('app.items_per_page'))
        ]);
    }

    public function getAddressByCustomer(Request $request)
    {
        $query = Address::select();
        if (!empty($request->query('customer_id'))) {
            $query->where('customer_id', '=', $request->query('customer_id'));
        }
        if (!empty($request->query('type'))) {
            $query->where('type', '=', $request->query('type'));
        }
        return response([
            'address' => $query->orderBy('id', 'desc')->get()
        ]);
    }

    public function getProducts(Request $request)
    {
        $columns = [
            'products.id',
            'product_groups.name as group_name',
            'product_groups.code as group_code',
            'tbl_messure_unit.code as unit',
            'products.code',
            'products.name',
            'products.by_weight',
            'products.weight as weight',
            // DB::raw('(CASE WHEN products.weight > 0 THEN products.weight ELSE 1 END) AS weight'),
            'products.sale_price',
            'products.price',
            'agency_product.pickup_fee as pickup_fee',
            'products.description',
            'products.status',
            'products.danger',
            'products.separate',
            'products.limit_pcs',
        ];

        $query = DB::table('products')->leftJoin('product_groups', 'products.product_group_id', '=', 'product_groups.id');
        $query->leftJoin('tbl_messure_unit', 'products.messure_unit_id', '=', 'tbl_messure_unit.id');
        $query->leftJoin('agency_product', 'agency_product.product_id', '=', 'products.id');
        if (!empty($request->query('agency'))) {
            $query->where('agency_product.agency_id', '=', $request->query('agency'));
        }
        if (!empty($request->query('product_group'))) {
            $query->where('products.product_group_id', '=', $request->query('product_group'));
        }
        if (!empty($request->query('service'))) {
            $query->where('product_groups.service', '=', $request->query('service'));
        }
        if (!empty($request->query('status'))) {
            $query->where('products.status', '=', $request->query('status'));
        }
        $query->where('products.is_deleted', '=', 0);
        $products = $query
        // ->groupBy([
        //     'products.id',
        //     'group_name',
        //     'group_code',
        //     'unit',
        //     'products.code',
        //     'products.name',
        //     'products.by_weight',
        //     'weight',
        //     'products.sale_price',
        //     'products.price',
        //     'pickup_fee',
        //     'products.description',
        //     'products.status',
        //     'products.danger',
        //     'products.separate',
        // ])
        ->orderBy('products.display_order', 'asc')->get($columns);
        return response([
            'products' => $products
        ]);
    }


    public function transportAgencyOrder()
    {
        return response([
            'create' => Order::where(['order_status' => OrderStatusEnum::STATUS_NEW, 'type' => 1])->whereNotNull('agency_id')->count(),
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 1])->whereNotNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 1])->whereNotNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 1])->whereNotNull('agency_id')->count(),
        ]);
    }

    public function transportCustomerQuote()
    {

        return response([
            'create' => Transport::where(['transport_status' => 1])->whereNull('agency_id')->count(),
            'processing' => Transport::where(['transport_status' => 2])->whereNull('agency_id')->count(),
            'cancel' => Transport::where(['transport_status' => 3])->whereNull('agency_id')->count(),
            'completed' => Transport::where(['transport_status' => 4])->whereNull('agency_id')->count()
        ]);
    }

    public function transportCustomerOrder()
    {
        return response([
            'create' => 0,
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 1])->whereNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 1])->whereNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 1])->whereNull('agency_id')->count(),
        ]);
    }

    public function yhlAgencyOrder()
    {
        return response([
            'create' => Order::where(['order_status' => OrderStatusEnum::STATUS_NEW, 'type' => 3])->whereNotNull('agency_id')->count(),
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 3])->whereNotNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 3])->whereNotNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 3])->whereNotNull('agency_id')->count(),
        ]);
    }

    public function yhlCustomerOrder()
    {
        return response([
            'create' => Order::where(['order_status' => OrderStatusEnum::STATUS_NEW, 'type' => 3])->whereNull('agency_id')->count(),
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 3])->whereNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 3])->whereNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 3])->whereNull('agency_id')->count(),
        ]);
    }

    public function expressAgencyOrder()
    {
        return response([
            'create' => Order::where(['order_status' => OrderStatusEnum::STATUS_NEW, 'type' => 2])->whereNotNull('agency_id')->count(),
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 2])->whereNotNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 2])->whereNotNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 2])->whereNotNull('agency_id')->count(),
        ]);
    }

    public function expressCustomerOrder()
    {
        return response([
            'create' => Order::where(['order_status' => OrderStatusEnum::STATUS_NEW, 'type' => 2])->whereNull('agency_id')->count(),
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 2])->whereNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 2])->whereNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 2])->whereNull('agency_id')->count(),
        ]);
    }

    public function mtsAgencyOrder()
    {
        return response([
            'create' => Mts::where(['mts_status' => 1])->whereNotNull('agency_id')->count(),
            'processing' => Mts::where(['mts_status' => 2])->whereNotNull('agency_id')->count(),
            'cancel' => Mts::where(['mts_status' => 3])->whereNotNull('agency_id')->count(),
            'completed' => Mts::where(['mts_status' => 4])->whereNotNull('agency_id')->count()
        ]);
    }

    public function mtsCustomerOrder()
    {
        return response([
            'create' => Mts::where(['mts_status' => 1])->whereNull('agency_id')->count(),
            'processing' => Mts::where(['mts_status' => 2])->whereNull('agency_id')->count(),
            'cancel' => Mts::where(['mts_status' => 3])->whereNull('agency_id')->count(),
            'completed' => Mts::where(['mts_status' => 4])->whereNull('agency_id')->count()
        ]);
    }

    public function quoteShop()
    {
        return response([
            'create' => Order::where(['order_status' => OrderStatusEnum::STATUS_NEW, 'type' => 4])->whereNull('agency_id')->count(),
            'processing' => Order::where(['order_status' => OrderStatusEnum::STATUS_PROCESSING, 'type' => 4])->whereNull('agency_id')->count(),
            'cancel' => Order::where(['order_status' => OrderStatusEnum::STATUS_CANCEL, 'type' => 4])->whereNull('agency_id')->count(),
            'completed' => Order::where(['order_status' => OrderStatusEnum::STATUS_COMPLETE, 'type' => 4])->whereNull('agency_id')->count(),
        ]);
    }
    public function getCurrencies()
    {
        $currencies = Currency::where("is_active", 1)->orderBy("name", 'asc')->get(['id', 'name', 'code']);
        return response([
            'currencies' => $currencies
        ]);
    }
    public function getProvincesagency(Request $request)
    {
        $agencyId =  (int)$request->query('agency_id');
        $provinces = User::where('id', $agencyId)->first('province_id');
        return response([
            'provinces' => $provinces['province_id']
        ]);
    }
    public function getminFeeagency(Request $request)
    {
        $Order_Id =  (int)$request->query('order_id');
        $order = Order::where('id', $Order_Id)->first('user_id');
        $user = User::where('id', $order['user_id'])->first('agency_id');
        $agencyfee = Agency::where('id', $user['agency_id'])->first(['id', 'min_fee']);
        // ->agency['min_fee'];
        return response([
            'minFee' => $agencyfee['min_fee'],
            'agency' => $agencyfee['id']
        ]);
    }
}
