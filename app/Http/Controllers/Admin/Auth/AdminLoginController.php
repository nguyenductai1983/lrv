<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
        public function username()
    {
        return 'username';
    }
    protected $redirectTo = RouteServiceProvider::HOMEADMIN;
    /*
    thay thế form login admin
    */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/admin');
    }
    public function showLoginForm()
    {
        return view('auth.admin_login');
    }
    // public function checkAuth()
    // {
    //     $result=false;
    //     // if (Auth::check()) {
    //     //     // The user is logged in...
    //     // }
    //    if(Auth::guard('admin'))
    //    {
    //        $result= true;
    //    }
    //    return $result;
    // }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('guest:admin', ['except' => ['logout']]);
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
