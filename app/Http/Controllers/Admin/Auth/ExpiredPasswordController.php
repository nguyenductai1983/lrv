<?php

namespace App\Http\Controllers\Admin\Auth;
use Auth;
use Password;
use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
// use App\Providers\RouteServiceProvider;
// use Illuminate\Foundation\Auth\ResetsPasswords;

// use PasswordExpiredRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;


class ExpiredPasswordController extends Controller
{
    public function expired()
    {
        return view('admin.profile.change_password');
    }
    public function postExpired($request)
    {
        // Checking current password
        if (!Hash::check($request->current_password, $request->user()->password)) {
            return redirect()->back()->withErrors(['current_password' => 'Current password is not correct']);
        }

        $request->user()->update([
            'password' => bcrypt($request->password),
            // 'password_changed_at' => Carbon::now()->toDateTimeString()
        ]);
        return redirect('admin')->with(['status' => 'Password changed successfully']);
    }
    //
}
