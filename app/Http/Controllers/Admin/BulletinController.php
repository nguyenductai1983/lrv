<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Http\Request;
// use App\Http\Requests\BannerRequest;
// use App\Http\Requests\CreateBannerRequest;
use App\Http\Controllers\Controller;
// use App\Components\LanguageUtil;
use App\Page;
use App\PageTranslate;
use App\Services\LogService;
use App\Enum\PageEnum;
class BulletinController extends Controller
{
     /**
     * Assign data
     */
    private $data;

    public function __construct() {
        $this->data['menu'] = '10.6';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pages = Page::where('type', '=', PageEnum::bulletin)
                        ->orderBy('level', 'asc')
                        ->select()->paginate(config('app.items_per_page'));

        $this->data['pages'] = $pages;
        return view('admin.bulletin.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.bulletin.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'link_to' => 'nullable|string|max:100',
            'order' => 'nullable'
        ];

        $customAttributes = [
            'link_to' => __('page.link_to'),
            'order' => __('page.order')
        ];
        foreach (config('app.locales') as $locale => $localeName) {
            $rules['name_' . $locale] = 'required|string|max:100';
            $customAttributes['name_' . $locale] = __('page.name');
            $rules['detail_' . $locale] = 'nullable|string';
            $customAttributes['detail_' . $locale] = __('page.detail');
            $rules['thumnail_' . $locale] = 'nullable';
            $customAttributes['thumnail_' . $locale] = __('page.thumnail');
        }
        $this->validate($request, $rules, [], $customAttributes);
        try {
            DB::beginTransaction();
            $currentLocale = app()->getLocale();
            $name = $request->get('name_' . $currentLocale);
            $page = new Page();
            $page->link_to = $request->get('link_to');
            $page->order = $request->get('order');
            $page->type = PageEnum::bulletin;
            $page->save();
            $page->slug = str_slug($name) . '-' . $page->id;
            $page->update();

            foreach (config('app.locales') as $locale => $localeName) {
                $file = $request->file('thumnail_' . $locale);
                $thumnail = '';
                if (!empty($file)) {
                    $path = date('Y') . '/' . date('m');
                    $name = $file->hashName();
                    if (Storage::putFileAs('public/' . $path, $file, $name)) {
                        $thumnail = '/' . $path . '/' . $name;
                    }
                }

                $pageTranslate = new PageTranslate();
                $pageTranslate->page_id = $page->id;
                $pageTranslate->lang = $locale;
                $pageTranslate->name = $request->get('name_' . $locale);
                $pageTranslate->detail = $request->get('detail_' . $locale);
                $pageTranslate->thumnail = $thumnail;
                $pageTranslate->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.bulletin.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$page = Page::with('translates')->find($id)) {
            abort(404);
        }
        $this->data['page'] = $page;

        return view('admin.bulletin.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!$page = Page::find($id)) {
            abort(404);
        }

        $rules = [
            'link_to' => 'nullable|string|max:100',
            'order' => 'nullable'
        ];

        $customAttributes = [
            'link_to' => __('page.link_to'),
            'order' => __('page.order'),
        ];
        foreach (config('app.locales') as $locale => $localeName) {
            $rules['name_' . $locale] = 'required|string|max:100';
            $customAttributes['name_' . $locale] = __('page.name');
            $rules['detail_' . $locale] = 'nullable|string';
            $customAttributes['detail_' . $locale] = __('page.detail');
            $rules['thumnail_' . $locale] = 'nullable';
            $customAttributes['thumnail_' . $locale] = __('page.thumnail');
        }
        $this->validate($request, $rules, [], $customAttributes);

        try {
            DB::beginTransaction();

            $locale = app()->getLocale();
            $name = $request->get('name_' . $locale);
            $attrs = [
                'slug' => str_slug($name) . '-' . $page->id,
                'link_to' => $request->get('link_to'),
                'order' => $request->get('order')
            ];
            $page->update($attrs);

            foreach (config('app.locales') as $locale => $localeName) {
                $file = $request->file('thumnail_' . $locale);
                $thumnail = '';
                if (!empty($file)) {
                    $path = date('Y') . '/' . date('m');
                    $name = $file->hashName();
                    if (Storage::putFileAs('public/' . $path, $file, $name)) {
                        $thumnail = '/' . $path . '/' . $name;
                    }
                }
                PageTranslate::where('page_id', '=', $page->id)
                        ->where('lang', '=', $locale)
                        ->update([
                            'name' => $request->get('name_' . $locale),
                            'detail' => $request->get('detail_' . $locale) ?: null,
                            'thumnail' => !empty($thumnail) ? $thumnail : ''
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.bulletin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            DB::beginTransaction();
            PageTranslate::where('page_id', '=', $id)->delete();

            $page = Page::find($id);
            if (empty($page)) {
                abort(404);
            }
            $page->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.bulletin.index');
    }
    public function show($id)
    {
        if (!$page = Page::with('agencies')->find($id)) {
            abort(404);
        }

        $this->data['page'] = $page;
        $this->data['agencies'] = Agency::all();
        // $Pageid = $page->agencies->pluck('id')->all();

        return view('admin.bulletin.agencie_page', $this->data);
    }
    public function updatepage(Request $request, $id)
    {
        if (!$page = Page::find($id)) {
            abort(404);
        }

        // $this->validate($request, [
        //     'agencies' => 'required|array|distinct',
        // ]);

        $AgencyId = $request->get('agencies');

        // $count = Agency::whereIn('id', $AgencyId)->count();
        // if ($count != count($AgencyId)) {
        //     return redirect()->back()
        //                      ->withInput()
        //                      ->withErrors(['agencies' => __('validation.exists', ['attribute' => __('agencies.name')])]);
        // }

        try {
            DB::beginTransaction();

            $page->agencies()->sync($AgencyId);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect(route('admin.bulletin.show', $page->id))->with([
            'status' => __('label.data_updated')
        ]);
    }
}
