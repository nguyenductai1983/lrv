<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Auth;
use Storage;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\CreateBannerRequest;
use App\Http\Controllers\Controller;
use App\Components\LanguageUtil;
use App\Page;
use App\PageTranslate;
use App\Services\LogService;

class HelpsController extends Controller {

    /**
     * Assign data
     */
    private $data;

    public function __construct() {
        $this->data['menu'] = '10.5';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pages = Page::where('type', '=', 4)
                        ->orderBy('level', 'asc')
                        ->select()->paginate(config('app.items_per_page'));

        $this->data['pages'] = $pages;
        return view('admin.helps.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pages = Page::with('translate')
                ->where('level', '=', 1)
                ->where('type', '=', 4)
                ->orderBy('order', 'asc')
                ->get();
        $this->data['pages'] = $pages;

        return view('admin.helps.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'parent_id' => 'required',
            'link_to' => 'nullable|string|max:100',
            'order' => 'nullable'
        ];
        if ($request->get('parent_id') != 0) {
            $rules['parent_id'] .= '|exists:pages,id';
        }
        $customAttributes = [
            'parent_id' => __('page.parent'),
            'link_to' => __('page.link_to'),
            'order' => __('page.order')
        ];
        foreach (config('app.locales') as $locale => $localeName) {
            $rules['name_' . $locale] = 'required|string|max:100';
            $customAttributes['name_' . $locale] = __('page.name');
            $rules['detail_' . $locale] = 'nullable|string';
            $customAttributes['detail_' . $locale] = __('page.detail');
        }
        $this->validate($request, $rules, [], $customAttributes);
        try {
            DB::beginTransaction();

            $currentLocale = app()->getLocale();
            $name = $request->get('name_' . $currentLocale);
            $page = new Page();
            $page->parent_id = $request->get('parent_id');
            if($page->parent_id > 0){
                $page->level = 2;
            }
            $page->link_to = $request->get('link_to');
            $page->order = $request->get('order');
            $page->type = 4;
            $page->save();
            $page->slug = str_slug($name) . '-' . $page->id;
            $page->update();

            foreach (config('app.locales') as $locale => $localeName) {
                $pageTranslate = new PageTranslate();
                $pageTranslate->page_id = $page->id;
                $pageTranslate->lang = $locale;
                $pageTranslate->name = $request->get('name_' . $locale);
                $pageTranslate->detail = $request->get('detail_' . $locale);

                $pageTranslate->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.helps.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$page = Page::with('translates')->find($id)) {
            abort(404);
        }
        $this->data['page'] = $page;
        $pages = Page::with('translate')
                ->where('level', '=', 1)
                ->where('type', '=', 4)
                ->orderBy('order', 'asc')
                ->get();
        $this->data['pages'] = $pages;

        return view('admin.helps.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!$page = Page::find($id)) {
            abort(404);
        }

        $rules = [
            'parent_id' => 'required',
            'link_to' => 'nullable|string|max:100',
            'order' => 'nullable'
        ];

        if ($request->get('parent_id') != 0) {
            $rules['parent_id'] .= '|exists:pages,id';
        }
        $customAttributes = [
            'parent_id' => __('page.parent'),
            'link_to' => __('page.link_to'),
            'order' => __('page.order')
        ];
        foreach (config('app.locales') as $locale => $localeName) {
            $rules['name_' . $locale] = 'required|string|max:100';
            $customAttributes['name_' . $locale] = __('page.name');
            $rules['detail_' . $locale] = 'nullable|string';
            $customAttributes['detail_' . $locale] = __('page.detail');
        }
        $this->validate($request, $rules, [], $customAttributes);

        try {
            DB::beginTransaction();

            $locale = app()->getLocale();
            $name = $request->get('name_' . $locale);
            $level = 1;
            if($request->get('parent_id') > 0){
                $level = 2;
            }
            $attrs = [
                'slug' => str_slug($name) . '-' . $page->id,
                'link_to' => $request->get('link_to'),
                'order' => $request->get('order'),
                'parent_id' => $request->get('parent_id'),
                'level' => $level
            ];
            $page->update($attrs);

            foreach (config('app.locales') as $locale => $localeName) {
                PageTranslate::where('page_id', '=', $page->id)
                        ->where('lang', '=', $locale)
                        ->update([
                            'name' => $request->get('name_' . $locale),
                            'detail' => $request->get('detail_' . $locale) ?: null
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.helps.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            DB::beginTransaction();
            PageTranslate::where('page_id', '=', $id)->delete();

            $page = Page::find($id);
            if (empty($page)) {
                abort(404);
            }
            $page->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.helps.index');
    }

}
