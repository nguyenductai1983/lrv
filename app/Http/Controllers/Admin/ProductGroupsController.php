<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductGroup;
use App\Product;
use App\Services\LogService;

class ProductGroupsController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * ProductGroupsController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '8.1'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['groups'] = ProductGroup::where(['is_deleted' => 0])->orderBy('code', 'asc')->get();

        return view('admin.product-groups.index', [
            'data' => $this->data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product-groups.create', [
            'data' => $this->data
        ]);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'service'     => 'required|in:' . implode(',', array_keys(config('app.services'))),
            'code'        => 'required|string|max:20|unique:product_groups,code' . ($id ? (',' . $id . ',id') : ''),
            'name'        => 'required|string|max:100',
            'description' => 'nullable|string',
            'thumbnail'   => 'nullable|integer',
            'thumbnail_path' => 'nullable|string',
        ], [], [
            'service'     => __('label.service'),
            'code'        => __('productGroup.code'),
            'name'        => __('productGroup.name'),
            'description' => __('productGroup.description'),
            'thumbnail' => __('productGroup.thumbnail'),
            'thumbnail_path' => __('productGroup.thumbnail_path')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $attrs = [
            'service'     => $request->get('service'),
            'code'        => $request->get('code'),
            'name'        => $request->get('name'),
            'description' => $request->get('description') ?: null,
            'thumbnail_id' => $request->get('thumbnail') ?: null,
            'thumbnail' => $request->get('thumbnail_path') ?: null,
        ];

        try {
            DB::beginTransaction();

            ProductGroup::create($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.product-groups.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$group = ProductGroup::find($id)) {
            abort(404);
        }

        $this->data['group'] = $group;
        return view('admin.product-groups.edit', [
            'data' => $this->data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$group = ProductGroup::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $group->id);

        $attrs = [
            'service'     => $request->get('service'),
            'code'        => $request->get('code'),
            'name'        => $request->get('name'),
            'description' => $request->get('description') ?: null,
            'thumbnail_id' => $request->get('thumbnail') ?: null,
            'thumbnail' => $request->get('thumbnail_path') ?: null,
        ];

        try {
            DB::beginTransaction();

            $group->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.product-groups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$group = ProductGroup::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $group->is_deleted = 1;
            $group->update();

            Product::where(['product_group_id' => $group->id])->update(['is_deleted' => 1]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.product-groups.index');
    }
}
