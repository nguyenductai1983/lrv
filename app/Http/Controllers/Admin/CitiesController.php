<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\City;
use App\Country;
use App\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LogService;
use Exception;

class CitiesController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * CitiesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.9'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::join('countries', 'cities.country_id', '=', 'countries.id')
                      ->join('provinces', 'cities.province_id', '=', 'provinces.id');
         if ($is_active = $request->query('is_active')) {
            $cities = $cities->where('cities.is_active', '=', $is_active);
        }
        else
        {
            $cities = $cities->where('cities.is_active', '=', 1);
        }

        if ($countryId = $request->query('country_id')) {
            $cities = $cities->where('cities.country_id', '=', $countryId);
        }

        if ($provinceId = $request->query('province_id')) {
            $cities = $cities->where('cities.province_id', '=', $provinceId);
        }

        if ($request->ajax() || $request->wantsJson()) {
            return [
                'cities' => $cities->orderBy('cities.name')->get(['cities.*'])
            ];
        }

        $this->data['cities'] = $cities->orderBy('countries.code')
                                       ->orderBy('provinces.name')
                                       ->orderBy('cities.name')
                                       ->select([
                                           'cities.*',
                                           'countries.name AS country_name',
                                           'provinces.name AS province_name'
                                       ])
                                       ->paginate(config('app.items_per_page'));

        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.cities.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        return view('admin.cities.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     */
    private function validateRequest(Request $request)
    {
        $this->validate($request, [
            'country_id'   => 'required|exists:countries,id',
            'province_id'  => 'required|exists:provinces,id',
            'name'         => 'required|string|max:100',
            'shipping_fee' => 'required|numeric|min:0',
            'postal_code' => 'nullable|string|max:20',
            'pickup_fee' => 'nullable|numeric',
        ], [], [
            'country_id'   => __('city.country_id'),
            'province_id'  => __('city.province_id'),
            'name'         => __('city.name'),
            'shipping_fee' => __('city.shipping_fee'),
            'postal_code' => __('label.post_code'),
            'pickup_fee' => __('product.pickup_fee')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        $attrs = [
            'country_id' => $request->get('country_id'),
            'province_id' => $request->get('province_id'),
            'name' => $request->get('name'),
            'shipping_fee' => $request->get('shipping_fee'),
            'postal_code' => $request->get('postal_code'),
            'pickup_fee' => $request->get('pickup_fee'),
            'is_active' => 1
        ];

        try {
            DB::beginTransaction();

            City::create($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.cities.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$city = City::find($id)) {
            abort(404);
        }
        $this->data['city'] = $city;

        $this->data['countries'] = Country::orderBy('code', 'asc')->get();

        return view('admin.cities.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$city = City::find($id)) {
            abort(404);
        }

        $this->validateRequest($request);

        $attrs = [
            'country_id' => $request->get('country_id'),
            'province_id' => $request->get('province_id'),
            'name' => $request->get('name'),
            'shipping_fee' => $request->get('shipping_fee'),
            'postal_code' => $request->get('postal_code'),
            'pickup_fee' => $request->get('pickup_fee')
        ];
        try {
            DB::beginTransaction();

            $city->update($attrs);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.cities.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$city = City::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $city->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.cities.index');
    }

}
