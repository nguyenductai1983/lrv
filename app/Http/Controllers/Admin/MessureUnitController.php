<?php

namespace App\Http\Controllers\Admin;

use App\MessureUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MessureUnitController extends Controller
{
    //
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.3'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['measures'] = MessureUnit::where('is_deleted', '=', 0)->orderBy('display_order')->get();

        return view('admin.measures.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.measures.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'        => 'required|string|max:20|unique:tbl_messure_unit,code' . ($id ? (',' . $id . ',id') : ''),
            'name'        => 'required|string|max:100',
            'display_order' => 'nullable|numeric',
            'is_active' => 'nullable|numeric',
        ], [], [
            'code'        => __('messure.code'),
            'name'        => __('messure.name'),
            'display_order'        => __('messure.name'),
            'is_active'        => __('messure.name'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            MessureUnit::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.measures.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$measures = MessureUnit::find($id)) {
            abort(404);
        }
        $this->data['measures'] = $measures;

        return view('admin.measures.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$measures = MessureUnit::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $measures->id);

        try {
            DB::beginTransaction();

            $measures->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.measures.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$measures = MessureUnit::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $measures->is_deleted = 1;
            $measures->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.measures.index');
    }
}
