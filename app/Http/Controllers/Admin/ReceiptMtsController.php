<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExportQuery;
use Illuminate\Support\Facades\Validator;
use App\Enum\MtsStatusEnum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Mts;
use App\Voucher;
use App\User;
use Exception;
class ReceiptMtsController extends Controller {

    private $data;

    public function __construct() {
        $this->data = [
            'menu' => 'receipt.3'
        ];
    }

    public function index(Request $request) {
        $columns = [
            'tbl_voucher.id',
            'tbl_voucher.code',
            'tbl_voucher.status',
            'tbl_voucher.amount',
            'tbl_voucher.discount_amount',
            'tbl_voucher.paid_amount',
            'tbl_voucher.remain_amount',
            'tbl_voucher.created_at',
            'tbl_voucher.last_paid_at',
            'users.code as user_code',
        ];

        $query = DB::table('tbl_voucher')->leftJoin('users', 'tbl_voucher.user_id', '=', 'users.id');
        $user = Auth::user();
        if(strtoupper($user->role->name) != "ADMIN"){
            $query->where('tbl_voucher.user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_voucher.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_voucher.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_id'))) {
            $query->where('users.id', '=', $request->query('user_id'));
        }
        $query->where('tbl_voucher.type', '=', 1);
        $query->where('tbl_voucher.is_deleted', '=', 0);
        $this->data['receipts'] = $query->orderBy('tbl_voucher.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['total_amount'] = $query->sum('tbl_voucher.amount');
        $this->data['total_agency_discount'] = $query->sum('tbl_voucher.discount_amount');
        $this->data['total_paid_amount'] = $query->sum('tbl_voucher.paid_amount');
        $this->data['total_remain_amount'] = $query->sum('tbl_voucher.remain_amount');
        $this->data['users'] = User::where([])->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-mts.index', $this->data);
    }

    public function exportExcel(Request $request)
    {
        $query = Voucher::leftJoin('users', 'tbl_voucher.user_id', '=', 'users.id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_voucher.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_voucher.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_id'))) {
            $query->where('users.id', '=', $request->query('user_id'));
        }
        $query->where('tbl_voucher.type', '=', 1);
        $query->where('tbl_voucher.is_deleted', '=', 0);
        $query->orderBy('tbl_voucher.id', 'desc')->select('tbl_voucher.code',
            'tbl_voucher.amount',
            'tbl_voucher.discount_amount',
            'tbl_voucher.paid_amount',
            'tbl_voucher.remain_amount',
            'tbl_voucher.created_at',
            'users.code as user_code');
            return (new ExportQuery($query))->download('mts_receipts.xlsx');
    }

    public function printItem($id)
    {
        if (!$voucher = Voucher::find($id)) {
            abort(404);
        }
        $query = Mts::where([]);
        $query->where('voucher_id', '=', $id);
        $this->data['mtss'] = $query->orderBy('id', 'desc')->get();
        $this->data['voucher'] = $voucher;
        return view('admin.receipt-mts.print', $this->data);
    }

    public function create(Request $request) {
        $query = Mts::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->name) != "ADMIN"){
            $query->where('user_id', '=', $user->id);
            $this->data['users'] = User::where('id',$user->id)->get();
        }
        else
        {
            $this->data['users'] = User::where([])->orderBy('code', 'asc')->get();
            if(!empty($request->query('user_id'))){
                $query->where('user_id', '=', $request->query('user_id'));
            }
        }
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        $query->where('mts_status', '!=', MtsStatusEnum::CANCEL);
        $query->whereNotNull('agency_id');
        $query->whereNull('voucher_id');
        $this->data['mtss'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.page_100'));

        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-mts.create', $this->data);
    }

    public function exportExcelItem($id)
    {
        $voucherData = Voucher::find($id);
        if (empty($voucherData)) {
            abort(404);
        }
        $query = Mts::where([]);
        $query->where('voucher_id', '=', $id);

       $query->orderBy('tbl_mts.id', 'desc')->select('tbl_mts.code',
            'tbl_mts.total_final',
            'tbl_mts.discount_agency_amount');
            return (new ExportQuery($query))->download($voucherData->code);
    }

    public function edit($id)
    {
        if (!$voucher = Voucher::find($id)) {
            abort(404);
        }
        $query = Mts::where([]);
        $query->where('voucher_id', '=', $id);
        $this->data['mtss'] = $query->orderBy('id', 'desc')->get();
        $this->data['voucher'] = $voucher;

        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-mts.view', $this->data);
    }

    public function addItems(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }

        $userData = User::find($request->get('user_id'));
        if (empty($userData)) {
            return response(["success" => false, "message" => __('message.err_user_no_exist'), "data" => []]);
        }

        $items = $request->get('items');
        if (empty($items)) {
            return response(["success" => false, "message" => __('message.err_receipt_empty'), "data" => []]);
        }
        $mtsIds = [];
        foreach ($items as $item) {
            $mtsIds[] = $item['value'];
        }
        $mtsData = Mts::find($mtsIds);
        if (empty($mtsData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }

        try {
            DB::beginTransaction();

            $voucher = new Voucher();
            $voucher->create_user_id = $user->id;
            $voucher->user_id = $userData->id;
            $voucher->type = 1;
            $voucher->save();

            $totalAmount = 0;
            $totalDiscountAmount = 0;
            foreach ($mtsData as $mts) {
                $mts->voucher_id = $voucher->id;
                $mts->update();

                $totalAmount += $mts->total_final;
                $totalDiscountAmount += $mts->discount_agency_amount;
            }
            $voucher->code = ConvertsUtil::getVoucherCode($userData->code, $voucher->id);
            $voucher->amount = $totalAmount;
            $voucher->discount_amount = $totalDiscountAmount;
            $voucher->remain_amount = $voucher->amount - $voucher->discount_amount;
            $voucher->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.add_voucher_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function removeItem(Request $request) {
        $validator = Validator::make($request->all(), [
            'mts_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $mtsData = Mts::find($request->get('mts_id'));
        if (empty($mtsData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }

        $voucherData = Voucher::find($mtsData->voucher_id);
        if (empty($voucherData)) {
            return response(["success" => false, "message" => __('message.err_voucher_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }

        try {
            DB::beginTransaction();

            $mtsData->voucher_id = NULL;
            $mtsData->update();

            $voucherData->amount -= $mtsData->total_final;
            $voucherData->discount_amount -= $mtsData->discount_agency_amount;
            $remain_amount = $mtsData->total_final - $mtsData->discount_agency_amount;
            if($remain_amount > 0){
                $voucherData->remain_amount -= $remain_amount;
            }
            $voucherData->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.remove_item_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function destroy($id) {
        $voucherData = Voucher::find($id);
        if (empty($voucherData)) {
            abort(404);
        }
        try {
            DB::beginTransaction();

            DB::table('tbl_mts')->where('voucher_id', $id)->update(['voucher_id' => NULL]);

            $voucherData->is_deleted = 1;
            $voucherData->update();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.receipt-mts.index');
    }

    public function paidVoucher(Request $request) {
        $validator = Validator::make($request->all(), [
            'voucher_id' => 'required',
            'amount' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $voucherData = Voucher::find($request->get('voucher_id'));
        if (empty($voucherData)) {
            return response(["success" => false, "message" => __('message.err_voucher_no_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $voucherData->paid_amount += $request->get('amount');
            $remain_amount = $voucherData->amount - ($voucherData->discount_amount + $voucherData->paid_amount);
            if($remain_amount <= 0 ){
                $voucherData->remain_amount = 0;
                $voucherData->status = 2;
            }else{
                $voucherData->remain_amount = $remain_amount;
                $voucherData->status = 1;
            }
            $voucherData->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.paid_voucher_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
}
