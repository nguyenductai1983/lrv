<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Enum\WarehouseEnum;
use App\Warehouse;
use App\Currency;
use App\WeightUnit;
use App\DimensionUnit;
use App\Country;
use App\Province;
use App\City;
use App\Http\Controllers\Controller;
use Exception;
use App\Services\LogService;
use App\ProductGroup;
use App\Product;
use App\Enum\ProductGroupEnum;
class WarehouseController extends Controller

{
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.5'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['warehouses'] = Warehouse::with([
            'province',
            'city',
            'ward',
            'country',
            'dimension_unit',
            'weight_unit',
            'currency',
        ])->where('is_deleted', '=', 0)->orderBy('code')->get();

        return view('admin.warehouses.index', $this->data);
    }
    public function list_product($request)
    {
        $warehouse=Warehouse::find($request);
        $group_product=ProductGroup::where('service', '=',ProductGroupEnum::SERVICE_YHL)->get();
        $list_group=$group_product->modelKeys();
        $list_products=Product::all();
        $list_products = $list_products->intersect(Product::whereIn('product_group_id', $list_group)->get());
        $this->data['list_products']=$list_products;
        $this->data['warehouse']=$warehouse;
        return view('admin.warehouses.product', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['currencies']  = Currency::orderBy('display_order', 'asc')->get();
        $this->data['weight_units']  = WeightUnit::orderBy('display_order', 'asc')->get();
        $this->data['dimension_units']  = DimensionUnit::orderBy('display_order', 'asc')->get();
        $this->data['countries'] = Country::where('is_active', '=', 1)->orderBy('name', 'asc')->get();

        return view('admin.warehouses.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'        => 'required|string|max:20',
            'name'        => 'required|string|max:100',
            'country_id' => 'required|numeric',
            'province_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'description' => 'nullable|string',
            'address' => 'required|string',
            'telephone' => 'required|string',
            'email' => 'nullable|string',
            'contact_person' => 'required|string',
            'post_code' => 'required|string',
            'is_default' => 'nullable|string',
            'type' => 'nullable|numeric',
            'currency_id' => 'nullable|numeric',
            'weight_unit_id' => 'nullable|numeric',
            'dimension_unit_id' => 'nullable|numeric',
        ], [], [
            'code'        => __('warehouse.code'),
            'name'        => __('warehouse.name'),
            'country_id' => __('warehouse.country_id'),
            'province_id' => __('warehouse.province_id'),
            'city_id' => __('warehouse.city_id'),
            'description'=> __('warehouse.description'),
            'address' => __('warehouse.address'),
            'telephone' => __('warehouse.telephone'),
            'email' => __('warehouse.email'),
            'contact_person'=> __('warehouse.contact_person'),
            'post_code'=> __('warehouse.post_code'),
            'is_default'=> __('warehouse.is_default'),
            'type' => __('warehouse.type'),
            'currency_id' => __('warehouse.currency_id'),
            'weight_unit_id' => __('warehouse.weight_unit_id'),
            'dimension_unit_id' => __('warehouse.dimension_unit_id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            Warehouse::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.warehouses.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$warehouses = Warehouse::find($id)) {
            abort(404);
        }
        $this->data['warehouses'] = $warehouses;
        $this->data['currencies']  = Currency::orderBy('display_order', 'asc')->get();
        $this->data['weight_units']  = WeightUnit::orderBy('display_order', 'asc')->get();
        $this->data['dimension_units']  = DimensionUnit::orderBy('display_order', 'asc')->get();
        $this->data['countries'] = Country::where('is_active', '=', 1)->orderBy('name', 'asc')->get();

        return view('admin.warehouses.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$warehouses = Warehouse::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $warehouses->id);
        try {
            DB::beginTransaction();
            $params = $request->all();
            if($params['is_default'] == WarehouseEnum::IS_DEFAULT_TRUE){
                Warehouse::where([])->update(['is_default' => WarehouseEnum::IS_DEFAULT_FALSE]);
            }
            $warehouses->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.warehouses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$warehouses = Warehouse::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $warehouses->is_deleted = 1;
            $warehouses->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.warehouses.index');
    }
}
