<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\PaymentMethod;
use App\Http\Controllers\Controller;
use Exception;
class PaymentMethodController extends Controller
{
    //
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.12'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['paymentMethods'] = PaymentMethod::where('is_deleted', '=', 0)->orderBy('display_order')->get();

        return view('admin.paymentMethod.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.paymentMethod.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'              => 'required|string',
            'name'              => 'required|string',
            'description'       => 'nullable|string',
            'display_order'     => 'nullable|numeric',
            'card_holder'       => 'nullable|string',
            'account_number'    => 'nullable|string',
            'bank_branch'       => 'nullable|string',
            'type'              => 'nullable|numeric',
            'image_file_id'     => ['nullable',
                Rule::exists('files', 'id')->where(function ($query) {
                    $query->where('created_user_id', '=', auth()->user()->id);
                })
            ],
        ], [], [
            'code'              => __('payment.code'),
            'name'              => __('payment.name'),
            'description'       => __('payment.description'),
            'display_order'     => __('payment.display_order'),
            'card_holder'       => __('payment.card_holder'),
            'account_number'    => __('payment.account_number'),
            'bank_branch'       => __('payment.bank_branch'),
            'type'              => __('payment.type'),
            'image_file_id'     => __('payment.image_file_id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            PaymentMethod::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.payment-methods.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$paymentMethod = PaymentMethod::find($id)) {
            abort(404);
        }
        $this->data['paymentMethod'] = $paymentMethod;

        return view('admin.paymentMethod.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$paymentMethod = PaymentMethod::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $paymentMethod->id);

        try {
            DB::beginTransaction();

            $paymentMethod->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.payment-methods.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$paymentMethod = PaymentMethod::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $paymentMethod->is_deleted = 1;
            $paymentMethod->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }

        return redirect()->route('admin.payment-methods.index');
    }
}
