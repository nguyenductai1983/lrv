<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\WeightUnit;
use App\Http\Controllers\Controller;
use Exception;
use App\Services\LogService;
class WeightUnitController extends Controller
{
    //
    /**
     * Assign data
     */
    private $data;

    /**
     * AreasController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.4'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['weights'] = WeightUnit::where('is_deleted', '=', 0)->orderBy('display_order')->get();

        return view('admin.weights.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.weights.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param null $id
     */
    private function validateRequest(Request $request, $id = null)
    {
        $this->validate($request, [
            'code'        => 'required|string|max:20|unique:tbl_weight_unit,code' . ($id ? (',' . $id . ',id') : ''),
            'name'        => 'required|string|max:100',
            'display_order' => 'nullable|numeric',
            'is_active' => 'nullable|numeric',
            'radio' => 'nullable|numeric',
        ], [], [
            'code'        => __('weight.code'),
            'name'        => __('weight.name'),
            'display_order'        => __('weight.name'),
            'is_active'        => __('weight.name'),
            'radio' => __('weight.radio'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        try {
            DB::beginTransaction();

            WeightUnit::create($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.weights.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$weights = WeightUnit::find($id)) {
            abort(404);
        }
        $this->data['weights'] = $weights;

        return view('admin.weights.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$weights = WeightUnit::find($id)) {
            abort(404);
        }

        $this->validateRequest($request, $weights->id);

        try {
            DB::beginTransaction();

            $weights->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.weights.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$weights = WeightUnit::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();
            $weights->is_deleted = 1;
            $weights->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.weights.index');
    }
}
