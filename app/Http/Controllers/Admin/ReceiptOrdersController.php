<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExportQuery;
use Exception;
use Illuminate\Support\Facades\Validator;
use App\Enum\OrderStatusEnum;
use App\Enum\EshiperEnum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Order;
use App\Voucher;

class ReceiptOrdersController extends Controller {

    private $data;

    public function __construct() {
        $this->data = [
            'menu' => 'receipt.2'
        ];
    }

    public function index(Request $request) {
        $columns = [
            'tbl_voucher.id',
            'tbl_voucher.code',
            'tbl_voucher.status',
            'tbl_voucher.amount',
            'tbl_voucher.discount_amount',
            'tbl_voucher.paid_amount',
            'tbl_voucher.remain_amount',
            'tbl_voucher.created_at',
            'tbl_voucher.last_paid_at',
            'users.code as user_code',
        ];

        $query = DB::table('tbl_voucher')->leftJoin('users', 'tbl_voucher.user_id', '=', 'users.id');
        $user = Auth::user();
        if(!$user->role->admin){
            $query->where('tbl_voucher.user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_voucher.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_voucher.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_id'))) {
            $query->where('users.id', '=', $request->query('user_id'));
        }
        $query->where('tbl_voucher.type', '=', 0);
        $query->where('tbl_voucher.is_deleted', '=', 0);
        $this->data['receipts'] = $query->orderBy('tbl_voucher.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['total_amount'] = $query->sum('tbl_voucher.amount');
        $this->data['total_agency_discount'] = $query->sum('tbl_voucher.discount_amount');
        $this->data['total_paid_amount'] = $query->sum('tbl_voucher.paid_amount');
        $this->data['total_remain_amount'] = $query->sum('tbl_voucher.remain_amount');
        $this->data['users'] = \App\User::where([])->orderBy('code', 'asc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-order.index', $this->data);
    }

    public function exportExcel(Request $request)
    {
        $query = Voucher::leftJoin('users', 'tbl_voucher.user_id', '=', 'users.id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_voucher.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_voucher.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('user_id'))) {
            $query->where('users.id', '=', $request->query('user_id'));
        }
        $query->where('tbl_voucher.type', '=', 0);
        $query->where('tbl_voucher.is_deleted', '=', 0);
        $query->orderBy('tbl_voucher.id', 'desc')->select('tbl_voucher.code',
            'tbl_voucher.amount',
            'tbl_voucher.discount_amount',
            'tbl_voucher.paid_amount',
            'tbl_voucher.remain_amount',
            'tbl_voucher.created_at',
            'users.code as user_code');
            return (new ExportQuery($query))->download('transport_receipts.xlsx');
    }

    public function printItem($id)
    {
        if (!$voucher = Voucher::find($id)) {
            abort(404);
        }
        $query = Order::where([]);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        $query->where('voucher_id', '=', $id);
        $orders = $query->orderBy('id', 'desc')->get();
        $this->data['orders'] = $orders;
        $this->data['voucher'] = $voucher;
        //update 23-04-2021
        $this->data['orders_total_final'] = $orders->sum->total_paid_amount;
        $this->data['orders_agency_discount']= $orders->sum->agency_discount;
        $voucher_paid=$this->data['orders_total_final']-$this->data['orders_agency_discount'];
        $this->data['voucher_paid_amount']=0;
        $this->data['voucher_remain_amount']=0;
        if($voucher->status)
        {
            $this->data['voucher_paid_amount']=$voucher_paid;
        }
        else
        {
            $this->data['voucher_remain_amount']=$voucher_paid;
        }
        return view('admin.receipt-order.print', $this->data);
    }

    public function create(Request $request) {
        $query = Order::where([]);
        if(!empty($request->query('user_id'))){
            $query->where('user_id', '=', $request->query('user_id'));

        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        // $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        $query->where('order_status', '>', OrderStatusEnum::STATUS_NEW);
        $query->whereNotNull('agency_id');
        $query->whereNull('voucher_id');
        $this->data['orders'] = $query->orderBy('id', 'desc')->get();
    }
    else
    {
        $query->where('id', '=', OrderStatusEnum::STATUS_CANCEL);
        $this->data['orders'] = $query->orderBy('id', 'desc')->get();
    }
        $this->data['users'] = \App\User::where([])->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-order.create', $this->data);
    }

    public function exportExcelItem($id)
    {
        $voucherData = Voucher::find($id);
        if (empty($voucherData)) {
            abort(404);
        }
        $query = Order::where([]);
        $query->where('voucher_id', '=', $id);

      $query->orderBy('tbl_order.id', 'desc')->select('tbl_order.code',
            'tbl_order.total_final',
            'tbl_order.agency_discount');
            return (new ExportQuery($query))->download('transport_receipts_item.xlsx');

    }

    public function edit($id)
    {
        if (!$voucher = Voucher::find($id)) {
            abort(404);
        }
        $query = Order::where([]);
        $query->where('voucher_id', '=', $id);
        $this->data['orders'] = $query->orderBy('id', 'desc')->get();
        $this->data['voucher'] = $voucher;

        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.receipt-order.view', $this->data);
    }

    public function addItems(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_agency_not_choose'), []]);
        }

        $userData = \App\User::find($request->get('user_id'));
        if (empty($userData)) {
            return response(["success" => false, "message" => __('message.err_user_no_exist'), "data" => []]);
        }

        $items = $request->get('items');
        if (empty($items)) {
            return response(["success" => false, "message" => __('message.err_receipt_empty'), "data" => []]);
        }
        $orderIds = [];
        foreach ($items as $item) {
            $orderIds[] = $item['value'];
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }

        try {
            DB::beginTransaction();

            $voucher = new Voucher();
            $voucher->create_user_id = $user->id;
            $voucher->user_id = $userData->id;
            $voucher->save();

            $totalAmount = 0;
            $totalDiscountAmount = 0;
            foreach ($orderData as $order) {
                $order->voucher_id = $voucher->id;
                $order->update();
                // điều chỉnh ngày tính thu tiền
                $totalAmount += $order->total_paid_amount;
                $totalDiscountAmount += $order->agency_discount;
            }
            $voucher->code = ConvertsUtil::getVoucherCode($userData->code, $voucher->id);
            $voucher->amount = $totalAmount;
            $voucher->discount_amount = $totalDiscountAmount;
            $voucher->remain_amount = $voucher->amount - $voucher->discount_amount;
            $voucher->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.add_voucher_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function removeItem(Request $request) {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $orderData = Order::find($request->get('order_id'));
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }

        $voucherData = Voucher::find($orderData->voucher_id);
        if (empty($voucherData)) {
            return response(["success" => false, "message" => __('message.err_voucher_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        if (empty($user->role->admin)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $orderData->voucher_id = NULL;
            $orderData->update();

            $voucherData->amount -= $orderData->total_final;
            $voucherData->discount_amount -= $orderData->agency_discount;
            $remain_amount = $orderData->total_final - $orderData->agency_discount;
            if($remain_amount > 0){
                $voucherData->remain_amount -= $remain_amount;
            }
            $voucherData->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.remove_item_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function destroy($id) {
        $voucherData = Voucher::find($id);
        if (empty($voucherData)) {
            abort(404);
        }
        try {
            DB::beginTransaction();

            DB::table('tbl_order')->where('voucher_id', $id)->update(['voucher_id' => NULL]);

            $voucherData->is_deleted = 1;
            $voucherData->update();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.receipt-orders.index');
    }

    public function paidVoucher(Request $request) {
        $validator = Validator::make($request->all(), [
            'voucher_id' => 'required',
            'amount' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $voucherData = Voucher::find($request->get('voucher_id'));
        if (empty($voucherData)) {
            return response(["success" => false, "message" => __('message.err_voucher_no_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $voucherData->paid_amount += $request->get('amount');
            $remain_amount = $voucherData->amount - ($voucherData->discount_amount + $voucherData->paid_amount);
            if($remain_amount <= 0 ){
                $voucherData->remain_amount = 0;
                $voucherData->status = 2;
            }else{
                $voucherData->remain_amount = $remain_amount;
                $voucherData->status = 1;
            }
            $voucherData->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.paid_voucher_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
}
