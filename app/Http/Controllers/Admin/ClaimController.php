<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Enum\ClaimEnum;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\CustomerClaim;
use App\CustomerClaimConversation;
use App\Notification;
use App\CustomerContact;

class ClaimController extends Controller
{

    private $data;

    public function __construct()
    {
        $this->data = [
            'menu' => 'claim.1'
        ];
    }

    public function index(Request $request)
    {
        $query = CustomerClaim::where([]);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        $this->data['claims'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.claim.index', $this->data);
    }

    public function view(Request $request)
    {
        $notification_id = $request->get('notification_id', 0);
        $id = $request->get('id');
        if ($notification_id > 0) {
            Notification::where('id', $notification_id)->update(['is_read' => 1]);
        }
        $claim = CustomerClaim::where('id', $id)->first();
        if (empty($claim)) {
            return redirect()->route('claim.admin.index');
        }
        $this->data['claimConversations'] = CustomerClaimConversation::where('customer_claim_id', $id)->orderBy('id', 'asc')->get();
        $this->data['claim'] = $claim;
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('admin.claim.view', $this->data);
    }

    public function createConversation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'comment' => 'required|string',
        ], [], [
            'id' => __('claim.id'),
            'comment' => __('claim.comment')
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [], "data" => []]);
        }
        $claim = CustomerClaim::where('id', $request->get('id'))->first();
        if (empty($claim)) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [], "data" => []]);
        }
        $customerClaimConversation = new CustomerClaimConversation();
        $customerClaimConversation->customer_claim_id = $request->get('id');
        $customerClaimConversation->customer_id = $claim->customer_id;
        $customerClaimConversation->user_id = auth()->id();
        $customerClaimConversation->content = $request->get('comment');

        $customerClaimConversation->save();

        if ($claim->status == ClaimEnum::NORMAL) {
            $claim->status = ClaimEnum::PROCESSING;
            $claim->update();
        }

        return response(["success" => true, "message" => __('message.claim-susscess'), "errors" => [], "data" => $customerClaimConversation]);
    }

    public function complete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ], [], [
            'id' => __('claim.id'),
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [], "data" => []]);
        }
        $claim = CustomerClaim::where('id', $request->get('id'))->first();
        if (empty($claim)) {
            return response(["success" => false, "message" => __('message.err_sys'), "errors" => [], "data" => []]);
        }
        $claim->status = ClaimEnum::COMPLETE;
        $claim->update();

        return response(["success" => true, "message" => __('message.claim-susscess'), "errors" => [], "data" => []]);
    }
    public function contact(Request $request)
    {
        $this->data = [
            'menu' => 'claim.2'
        ];
        $query = CustomerContact::select();

        $telephone = $request->query('phone_number');
        if (!empty($telephone)) {
            $query->where('phone_number', 'like', "%{$telephone}%");
        }
        $fullName = $request->query('full_name');
        if ($fullName != '') {
            $query->where('full_name', 'like', "%{$fullName}%");
        }

        $email = $request->query('email');
        if ($email != '') {
            $query->where('email', '=', "%{$email}%");
        }
        $contacts = $query->orderBy('id', 'desc')
            ->select()->paginate(config('app.items_per_page'));

        $this->data['contacts'] = $contacts;

        return view('admin.claim.contact', $this->data);
    }
    public function delete($id)
    {
        $this->data = [
            'menu' => 'claim.2'
        ];
        $query = CustomerContact::where('id', '=', $id);
        $query->delete();
        return redirect('admin/claim/contact');
    }
}
