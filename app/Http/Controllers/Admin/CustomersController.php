<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Exports\ExportQuery;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressRequest;
use App\Area;
use App\CustomerGroup;
use App\Country;
use App\Customer;
use App\CustomerPointLog;
use App\CustomerMemberConfig;
use App\Address;
use App\Campaign;
use Exception;
use App\Services\LogService;

class CustomersController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * CountriesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '3.2'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $query = Customer::with(['area', 'group', 'country', 'province', 'city']);

        $query->where('is_delete', '=', 0);
        if ($firstName = $request->query('first_name')) {
            $query->where('first_name', 'like', "%{$firstName}%");
        }

        if ($lastName = $request->query('last_name')) {
            $query->where('last_name', 'like', "%{$lastName}%");
        }

        if ($code = $request->query('code')) {
            $query->where('code', 'like', "%{$code}");
        }

        if ($telephone = $request->query('telephone')) {
            $query->where('telephone', '=', $telephone);
        }

        if ($areaId = $request->query('area_id')) {
            $query->where('area_id', '=', $areaId);
        }

        if ($groupId = $request->query('customer_group_id')) {
            $query->where('customer_group_id', '=', $groupId);
        }

        if ($countryId = $request->query('country_id')) {
            $query->where('country_id', '=', $countryId);
        }

        $customers = $query->orderBy('id', 'desc')
                        ->select()->paginate(config('app.items_per_page'));

        $this->data['customers'] = $customers;

        $this->data['areas'] = Area::orderBy('code')->get();
        $this->data['groups'] = CustomerGroup::orderBy('code')->get();
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.customers.index', $this->data);
    }


    public function exportExcel(Request $request)
    {
        $query = Customer::where([]);
        $query->select('email', 'first_name', 'middle_name', 'last_name');
        $query->where('is_delete', '=', 0);
        $query->whereNotNull('email');
        if ($firstName = $request->query('first_name')) {
            $query->where('first_name', 'like', "%{$firstName}%");
        }

        if ($lastName = $request->query('last_name')) {
            $query->where('last_name', 'like', "%{$lastName}%");
        }

        if ($code = $request->query('code')) {
            $query->where('code', '=', $code);
        }

        if ($telephone = $request->query('telephone')) {
            $query->where('telephone', '=', $telephone);
        }

        if ($areaId = $request->query('area_id')) {
            $query->where('area_id', '=', $areaId);
        }

        if ($groupId = $request->query('customer_group_id')) {
            $query->where('customer_group_id', '=', $groupId);
        }

        if ($countryId = $request->query('country_id')) {
            $query->where('country_id', '=', $countryId);
        }

      $query->orderBy('id', 'desc');
        return (new ExportQuery($query))->download('customers.xlsx');
    }

    public function vip($id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }
        $this->data['customer'] = $customer;
        $this->data['transport'] = CustomerMemberConfig::where(['customer_id' => $id, 'type' => 1])->first();
        $this->data['express'] = CustomerMemberConfig::where(['customer_id' => $id, 'type' => 2])->first();
        $this->data['yhl'] = CustomerMemberConfig::where(['customer_id' => $id, 'type' => 3])->first();
        $this->data['mts'] = CustomerMemberConfig::where(['customer_id' => $id, 'type' => 4])->first();

        return view('admin.customers.vip', $this->data);
    }

    public function point($id) {
        $query = CustomerPointLog::where(['customer_id' => $id]);

        $this->data['customers'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));

        return view('admin.customers.point', $this->data);
    }

    public function address($id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }

        $query = Address::where(['customer_id' => $id, 'is_deleted' => 0]);

        $this->data['addresses'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['customer'] = $customer;

        return view('admin.customers.address', $this->data);
    }

    public function addressDestroy($id) {
        $address = Address::find($id);
        if (empty($address)) {
            abort(404);
        }
        try {
            DB::beginTransaction();

            $address->is_deleted = 1;
            $address->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customers.address', $address->customer_id);
    }

    public function addressEdit($id) {
        if (!$address = Address::find($id)) {
            abort(404);
        }
        $this->data['address'] = $address;
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.customers.address_edit', $this->data);
    }

    public function addressUpdate(AddressRequest $request, $id) {
        if (!$address = Address::find($id)) {
            abort(404);
        }
        $params = $request->all();

        try {
            DB::beginTransaction();
            $address->first_name = $params['first_name'];
            $address->middle_name = $params['middle_name'];
            $address->last_name = $params['last_name'];
            $address->address_1 = $params['address_1'];
            $address->address_2 = $params['address_2'];
            $address->telephone = $params['telephone'];
            $address->cellphone = $params['cellphone'];
            $address->postal_code = $params['postal_code'];
            $address->email = $params['email'];
            $address->country_id = $params['country_id'];
            $address->province_id = $params['province_id'];
            $address->city_id = $params['city_id'];

            $address->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }
        return redirect()->route('admin.customers.address', $address->customer_id);
    }

    /**
     * Search customers
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $query = Customer::select();
        $query->where('is_delete', '=', 0);
        $group = $request->query('group');
        if ($group != '') {
            $query->where('customer_group_id', '=', $group);
        }

        $telephone = $request->query('telephone');
        if (!empty($telephone)) {
            $query->where('telephone', 'like', "%{$telephone}%");
        }

        $code = $request->query('code');
        if ($code != '') {
            $query->where('code', 'like', "%{$code}%");
        }

        $firstName = $request->query('first_name');
        if ($firstName != '') {
            $query->where('first_name', 'like', "%{$firstName}%");
        }

        $middleName = $request->query('middle_name');
        if ($middleName != '') {
            $query->where('middle_name', 'like', "%{$middleName}%");
        }

        $lastName = $request->query('last_name');
        if ($lastName != '') {
            $query->where('last_name', 'like', "%{$lastName}%");
        }
        $query->with([
            'image1',
            'image2',
            'image3'
        ]);
        return response([
            'customers' => $query->orderBy('id', 'desc')->paginate(config('app.items_per_page'))
        ]);
    }

    /**
     * Search customers
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Search customers
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function points(Request $request) {
        $this->data = [
            'menu' => '3.4'
        ];
        $query = CustomerPointLog::select();

        $customer_id = $request->query('customer_id');
        if (!empty($customer_id)) {
            $query->where('customer_id', '=', $customer_id);
        }
        $points = $query->orderBy('id', 'desc')
                        ->select()->paginate(config('app.items_per_page'));

        $this->data['points'] = $points;

        return view('admin.customers.points', $this->data);
    }

    /**
     * Search customers
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function coupons(Request $request) {
        $this->data = [
            'menu' => '3.5'
        ];
        $query = Campaign::select();

        $query->whereNotNull('customer_id');
        $customer_id = $request->query('customer_id');
        if (!empty($customer_id)) {
            $query->where('customer_id', '=', $customer_id);
        }
        $coupons = $query->orderBy('id', 'desc')
                        ->select()->paginate(config('app.items_per_page'));

        $this->data['coupons'] = $coupons;

        return view('admin.customers.coupons', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->data['areas'] = Area::orderBy('code')->get();
        $this->data['groups'] = CustomerGroup::orderBy('code')->get();
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.customers.create', $this->data);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @param Customer|null $customer
     */
    private function _validate(Request $request) {
        $this->validate($request, [
            'area_id' => 'nullable|exists:areas,id',
            'customer_group_id' => 'nullable|exists:customer_groups,id',
            'image_1_file_id' => [
                'nullable',
                Rule::exists('files', 'id')->where(function ($query) {
                            $query->where('created_user_id', '=', auth()->user()->id);
                        })
            ],
            'image_2_file_id' => [
                'nullable',
                Rule::exists('files', 'id')->where(function ($query) {
                            $query->where('created_user_id', '=', auth()->user()->id);
                        })
            ],
            'image_3_file_id' => [
                'nullable',
                Rule::exists('files', 'id')->where(function ($query) {
                            $query->where('created_user_id', '=', auth()->user()->id);
                        })
            ],
            'first_name' => 'required|string|max:50',
            'middle_name' => 'nullable|string|max:50',
            'last_name' => 'required|string|max:50',
            'address_1' => 'required|string|max:255',
            'address_2' => 'nullable|string|max:255',
            'telephone' => 'required|string|max:20',
            'cellphone' => 'nullable|string|max:20',
            'postal_code' => 'required|string|max:20',
            'id_card' => 'nullable|string|max:20',
            'card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'birthday' => 'nullable|date_format:' . config('app.date_format'),
            'career' => 'nullable|string|max:100',
            'country_id' => 'required|exists:countries,id',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:cities,id'
                ], [], [
            'area_id' => __('customer.area_id'),
            'customer_group_id' => __('customer.customer_group_id'),
            'image_1_file_id' => __('customer.image_1_file_id'),
            'image_2_file_id' => __('customer.image_2_file_id'),
            'image_3_file_id' => __('customer.image_3_file_id'),
            'first_name' => __('customer.first_name'),
            'middle_name' => __('customer.middle_name'),
            'last_name' => __('customer.last_name'),
            'address_1' => __('customer.address_1'),
            'address_2' => __('customer.address_2'),
            'telephone' => __('customer.telephone'),
            'cellphone' => __('customer.cellphone'),
            'postal_code' => __('customer.postal_code'),
            'id_card' => __('customer.id_card'),
            'card_expire' => __('customer.card_expire'),
            'birthday' => __('customer.birthday'),
            'career' => __('customer.career'),
            'country_id' => __('customer.country_id'),
            'province_id' => __('customer.province_id'),
            'city_id' => __('customer.city_id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->_validate($request);

        try {
            DB::beginTransaction();

            $customer = Customer::create($request->all());
            $customer->update(['code' => auth()->user()->code . '_' . $customer->id]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }
        $this->data['customer'] = $customer;

        $this->data['areas'] = Area::orderBy('code')->get();
        $this->data['groups'] = CustomerGroup::orderBy('code')->get();
        $this->data['countries'] = Country::orderBy('code')->get();

        return view('admin.customers.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }

        $this->_validate($request, $customer);

        try {
            DB::beginTransaction();
            $customer->update($request->all());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $customer->is_delete = 1;
            $customer->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customers.index');
    }
    public function lock($id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $customer->is_lock = 1;
            $customer->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }
        return back();
        // return redirect()->route('admin.customers.index');
    }
    public function unlock($id) {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }

        try {
            DB::beginTransaction();

            $customer->is_lock = 0;
            $customer->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }

        return redirect()->route('admin.customers.index');
    }
    /**
     * Update vip
     *
     * @param Request $request
     * @param integer $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateVip(Request $request, $id)
    {
        if (!$customer = Customer::find($id)) {
            abort(404);
        }
        $params = $request->all();
        if($params['member_type'] != 2){
            return redirect()->route('admin.customers.index');
        }
        try {
            DB::beginTransaction();

            foreach ($params['discount'] as $type => $discount){
                $cogMember = CustomerMemberConfig::where(['customer_id' => $id, 'type' => $type])->first();
                if(!empty($cogMember)){
                    $cogMember->discount = $discount;
                    $cogMember->update();
                } else {
                    $newCogMember = new CustomerMemberConfig();
                    $newCogMember->customer_id = $id;
                    $newCogMember->type = $type;
                    $newCogMember->discount = $discount;
                    $newCogMember->save();
                }
            }
            $customer->member_type = 2;
            $customer->update();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            LogService::error($e);
            abort(500);
        }
        return redirect()->route('admin.customers.index');
    }
}
