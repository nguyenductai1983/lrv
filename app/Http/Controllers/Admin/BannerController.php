<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Exception;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\CreateBannerRequest;
use App\Http\Controllers\Controller;
use App\File;

class BannerController extends Controller {

    /**
     * Assign data
     */
    private $data;

    public function __construct() {
        $this->data['menu'] = '10.3';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $files = File::where('is_banner', '=', 1)
                        ->orderBy('display_order', 'asc')
                        ->select()->paginate(config('app.items_per_page'));

        $this->data['banners'] = $files;
        return view('admin.banners.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.banners.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBannerRequest $request) {
        $params = $request->all();
        try {
            DB::beginTransaction();
            $rules = [
                'file' => 'required'
            ];

            $rules['file'] .= '|mimes:' . config('file.img_mimes') . '|max:' . config('file.img_max_size');

            $this->validate($request, $rules, [], [
                'file' => __('label.file')
            ]);

            $file = $request->file('file');
            if (empty($file)) {
                abort(404);
            }
            $path = date('Y') . '/' . date('m');
            $name = $file->hashName();
            if (!Storage::putFileAs('public/' . $path, $file, $name)) {
                abort(500);
            }
            $banner = new File();
            $banner->path = '/' . $path . '/' . $name;
            $banner->type = $file->getMimeType();
            $banner->size = $file->getSize();
            $banner->name = $params['name'];
            $banner->valid = $params['valid'];
            $banner->lang = $params['lang'];
            $banner->display_order = $params['display_order'];
            $banner->is_banner = 1;
            $banner->created_user_id = isset(Auth::guard('admin')->user()->id) ? Auth::guard('admin')->user()->id : null;

            $banner->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.banners.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!$file = File::find($id)) {
            abort(404);
        }
        $this->data['banner'] = $file;

        return view('admin.banners.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, $id) {
        if (!$banner = File::find($id)) {
            abort(404);
        }
        $params = $request->all();
        try {
            DB::beginTransaction();
            $rules = [
                'file' => 'required'
            ];

            $rules['file'] .= '|mimes:' . config('file.img_mimes') . '|max:' . config('file.img_max_size');

            $file = $request->file('file');
            if (!empty($file)) {
                $path = date('Y') . '/' . date('m');
                $name = $file->hashName();
                if (!Storage::putFileAs('public/' . $path, $file, $name)) {
                    abort(500);
                }
                $banner->path = '/' . $path . '/' . $name;
                $banner->type = $file->getMimeType();
                $banner->size = $file->getSize();
            }
            $banner->name = $params['name'];
            $banner->valid = $params['valid'];
            $banner->lang = $params['lang'];
            $banner->display_order = $params['display_order'];

            $banner->update();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
        return redirect()->route('admin.banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            DB::beginTransaction();
            File::where('id', '=', $id)->delete();
            DB::commit();
            return redirect()->route('admin.banners.index');
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
    }

}
