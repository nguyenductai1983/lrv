<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth, Redirect, Session, URL;
use App\Customer;
use Socialite;

class SocialAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        if(!Session::has('pre_url')){
            Session::put('pre_url', URL::previous());
        }else{
            if(URL::previous() != URL::to('login')) Session::put('pre_url', URL::previous());
        }
        return Socialite::driver($provider)->stateless()->redirect();
    }
    
    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::guard()->login($authUser, true);
        return Redirect::to(Session::get('pre_url'));
    }
    /**
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        if ($provider == 'google') {
            $authUser = Customer::where('email', $user->email)->first();
            if (!empty($authUser)) {
                $authUser->provider = $provider;
                $authUser->provider_id = $user->id;
                $authUser->avatar_url = $user->avatar;
                $authUser->update();
            } else {
                $authUser = new Customer();
                $authUser->provider = $provider;
                $authUser->code = 'KH_';
                $authUser->provider_id = $user->id;
                $authUser->avatar_url = $user->avatar;
                $authUser->email = $user->email;
                $authUser->first_name = isset($user->user['givenName']) ? $user->user['givenName'] : '';
                $authUser->last_name = isset($user->user['familyName']) ? $user->user['familyName'] : '';
                $authUser->save();
                $authUser->code = 'KH_'. $authUser->id;
                $authUser->update();
            }
        }else {
            $authUser = Customer::where('email', $user->email)->first();
            if ($authUser) {
                $authUser->provider = $provider;
                $authUser->provider_id = $user->id;
                $authUser->avatar_url = $user->avatar;
                $authUser->update();
            } else {
                $authUser = new Customer();
                $authUser->provider = $provider;
                $authUser->code = 'KH_';
                $authUser->provider_id = $user->id;
                $authUser->avatar_url = $user->avatar;
                $authUser->email = $user->email;
                $authUser->first_name = $user->name;
                $authUser->save();
                $authUser->code = 'KH_'. $authUser->id;
                $authUser->update();
            }
        }
        return $authUser;
    }
}
