<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PagesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        if (!$page = Page::with('translate')->where('slug', '=', $slug)->first()) {
            abort(404);
        }

        return view('pages.show', [
            'page' => $page
        ]);
    }
}
