<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($locale = $request->query('locale')) {
            session()->put('locale', $locale);
        }

        if (session()->has('locale')) {
            $locale = session()->get('locale');
            app()->setLocale($locale);
        }

        return $next($request);
    }
}
