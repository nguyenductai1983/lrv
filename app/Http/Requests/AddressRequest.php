<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required|string|max:50',
            'middle_name' => 'nullable|string|max:50',
            'last_name' => 'required|string|max:50',
            'address_1' => 'required|string|max:35',
            'address_2' => 'nullable|string|max:35',
            'telephone' => 'required|string|max:100',
            'cellphone' => 'nullable|string|max:100',
            'postal_code' => 'required|string|max:100',
            'email' => 'nullable|string|max:50',
            'country_id' => 'required|exists:countries,id',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:cities,id'
        ];
    }
    
    public function attributes() {
        return[
            'first_name' => __('customer.first_name'),
            'middle_name' => __('customer.middle_name'),
            'last_name' => __('customer.last_name'),
            'address_1' => __('customer.address_1'),
            'address_2' => __('customer.address_2'),
            'telephone' => __('customer.telephone'),
            'cellphone' => __('customer.cellphone'),
            'postal_code' => __('customer.postal_code'),
            'email' => __('customer.email'),
            'country_id' => __('customer.country_id'),
            'province_id' => __('customer.province_id'),
            'city_id' => __('customer.city_id'),
        ];
    }

}
