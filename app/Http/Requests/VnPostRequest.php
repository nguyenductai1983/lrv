<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VnPostRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'sMaDonHang' => 'required|string',
            'sSoHieu' => 'nullable|string',
            'sMaTrangThai' => 'required|string',
            'sNgayTrangThai' => 'required|string',
            'sGhiChu' => 'nullable|string'
        ];
    }

    public function attributes() {
        return[
            'sMaDonHang' => __('vnpost.order'),
            'sSoHieu' => __('vnpost.agency'),
            'sMaTrangThai' => __('vnpost.status'),
            'sNgayTrangThai' => __('vnpost.date_update'),
            'sGhiChu' => __('vnpost.note'),
        ];
    }

}
