<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpressOnlineRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize() {
    //     return true;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'sender' => 'required|array',
            'sender.id' => 'nullable|exists:customers,id',
            'sender.first_name' => 'required|string|max:50',
            'sender.middle_name' => 'nullable|string|max:50',
            'sender.last_name' => 'required|string|max:50',
            'sender.address_1' => 'required|string|max:35',
            'sender.address_2' => 'nullable|string|max:35',
            'sender.telephone' => 'required|string|max:20',
            'sender.cellphone' => 'nullable|string|max:20',
            'sender.postal_code' => 'required|string|max:100',
            'sender.country_id' => 'required|exists:countries,id',
            'sender.province_id' => 'required|exists:provinces,id',
            'sender.city_id' => 'required|exists:cities,id',
            'sender.shipping_date' => 'nullable|string',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:35',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.cellphone' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',

            'pickup' => 'required|array',
            'pickup.is_schedule' => 'required|string|in:1,2',
            'pickup.contact_name' => 'required_if:pickup.is_schedule,2',
            'pickup.phone_number' => 'required_if:pickup.is_schedule,2',
            'pickup.location' => 'required_if:pickup.is_schedule,2',
            'pickup.date_time' => 'required_if:pickup.is_schedule,2',
            'pickup.start_hour_time' => 'required_if:pickup.is_schedule,2',
            'pickup.start_minute_time' => 'required_if:pickup.is_schedule,2',
            'pickup.closing_hour_time' => 'required_if:pickup.is_schedule,2',
            'pickup.closing_minute_time' => 'required_if:pickup.is_schedule,2',

            'warehouse_id' => 'required|integer|exists:tbl_warehouse,id',

            'containers' => 'required|array',
            'containers.*.length' => 'required|numeric|min:0',
            'containers.*.width' => 'required|numeric|min:0',
            'containers.*.height' => 'required|numeric|min:0',
            'containers.*.total_weight' => 'required|numeric|min:0',
        ];
    }

    public function attributes() {
        return[
            'sender.id' => __('label.sender'),
            'sender.first_name' => __('sender.first_name'),
            'sender.middle_name' => __('sender.middle_name'),
            'sender.last_name' => __('sender.last_name'),
            'sender.address_1' => __('sender.address_1'),
            'sender.address_2' => __('sender.address_2'),
            'sender.telephone' => __('sender.telephone'),
            'sender.cellphone' => __('sender.cellphone'),
            'sender.postal_code' => __('sender.postal_code'),
            'sender.country_id' => __('sender.country_id'),
            'sender.province_id' => __('sender.province_id'),
            'sender.city_id' => __('sender.city_id'),
            'sender.shipping_date' => __('sender.shipping_date'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),

            'pickup' => __('express.pickup_info'),
            'pickup.contact_name' =>  __('express.contact_name'),
            'pickup.phone_number' => __('express.phone_number'),
            'pickup.location' => __('express.pickup_location'),
            'pickup.date_time' => __('express.pickup_date'),
            'pickup.start_hour_time' => __('express.pickup_hour'),
            'pickup.start_minute_time' => __('express.pickup_minute'),
            'pickup.closing_hour_time' => __('express.closing_hour'),
            'pickup.closing_minute_time' => __('express.closing_minute'),

            'warehouse_id' => __('label.warehouse_id'),

            'containers' => __('label.containers'),
            'containers.*.length' => __('label.length'),
            'containers.*.width' => __('label.width'),
            'containers.*.height' => __('label.height'),
            'containers.*.total_weight' => __('label.total_weight'),
        ];
    }

}
