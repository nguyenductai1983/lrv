<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'display_order' => 'nullable',
            'valid' => 'nullable',
            'file' => 'nullable',
            'path' => 'required'
        ];
    }
    
    public function attributes() {
        return[
            'name' => __('banner.name'),
            'display_order' => __('banner.display_order'),
            'valid' => __('banner.valid'),
            'file' => __('banner.file'),
            'path' => __('banner.path')
        ];
    }

}
