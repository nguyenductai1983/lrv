<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'transport' => 'required|array',
            'transport.id' => 'required|numeric',
            'transport.last_paid_amount' => 'required|numeric|min:0',
            'transport.pay_method' => 'required|string',
            'transport.last_date_pay' => 'required|string'
        ];
    }
    
    public function attributes() {
        return[
            'transport.id' => __('label.id'),
            'transport.last_paid_amount' => __('label.first_name'),
            'transport.pay_method' => __('label.pay'),
            'transport.last_date_pay' => __('label.date_pay'),
        ];
    }

}
