<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'customer' => 'required|array',
            'customer.id' => 'nullable|exists:customers,id',
            'customer.image_1_file_id' => 'nullable|exists:files,id',
            'customer.image_2_file_id' => 'nullable|exists:files,id',
            'customer.image_3_file_id' => 'nullable|exists:files,id',
            'customer.first_name' => 'required|required_without:customer.id|string|max:50',
            'customer.middle_name' => 'nullable|string|max:50',
            'customer.last_name' => 'nullable|required_without:customer.id|string|max:50',
            'customer.address_1' => 'required|required_without:customer.id|string|max:250',
            'customer.email' => 'nullable|email',
            'customer.telephone' => 'required|required_without:customer.id|string|max:20',
            'customer.cellphone' => 'nullable|string|max:30',
            'customer.postal_code' => 'required|required_without:customer.id|string|max:20',
            'customer.id_card' => 'nullable|string|max:20',
            'customer.card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'customer.date_issued' => 'nullable|date_format:' . config('app.date_format'),
            'customer.birthday' => 'nullable|date_format:' . config('app.date_format'),
            'customer.career' => 'nullable|string|max:100',
            'customer.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'customer.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'customer.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'address' => 'required|array',
            'address.first_name' => 'required|string|max:50',
            'address.middle_name' => 'nullable|string|max:50',
            'address.last_name' => 'required|string|max:50',
            'address.address_1' => 'required|string|max:35',
            'address.postal_code' => 'nullable|string|max:20',
            'address.telephone' => 'required|string|max:20',
            'address.cellphone' => 'nullable|string|max:20',
            'address.country_id' => 'required|exists:countries,id',
            'address.province_id' => 'required|exists:provinces,id',
            'address.city_id' => 'required|exists:cities,id',
        ];
    }

    public function attributes()
    {
        return[
            'customer.id' => __('label.sender'),
            'customer.image_1_file_id' => __('customer.image_1_file_id'),
            'customer.image_2_file_id' => __('customer.image_2_file_id'),
            'customer.image_3_file_id' => __('customer.image_3_file_id'),
            'customer.first_name' => __('customer.first_name'),
            'customer.middle_name' => __('customer.middle_name'),
            'customer.last_name' => __('customer.last_name'),
            'customer.address_1' => __('customer.address_1'),
            'customer.email' => __('customer.email'),
            'customer.telephone' => __('customer.telephone'),
            'customer.cellphone' => __('customer.cellphone'),
            'customer.postal_code' => __('customer.postal_code'),
            'customer.id_card' => __('customer.id_card'),
            'customer.card_expire' => __('customer.card_expire'),
            'customer.birthday' => __('customer.birthday'),
            'customer.date_issued' => __('customer.date_issued'),
            'customer.career' => __('customer.career'),
            'customer.country_id' => __('customer.country_id'),
            'customer.province_id' => __('customer.province_id'),
            'customer.city_id' => __('customer.city_id'),

            'address.first_name' => __('receiver.first_name'),
            'address.middle_name' => __('receiver.middle_name'),
            'address.last_name' => __('receiver.last_name'),
            'address.address_1' => __('receiver.address'),
            'address.postal_code' => __('receiver.postal_code'),
            'address.telephone' => __('receiver.telephone'),
            'address.cellphone' => __('receiver.cellphone'),
            'address.country_id' => __('receiver.country_id'),
            'address.province_id' => __('receiver.province_id'),
            'address.city_id' => __('receiver.city_id'),
        ];
    }
}
