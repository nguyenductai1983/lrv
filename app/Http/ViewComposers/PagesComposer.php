<?php

namespace App\Http\ViewComposers;

use App\Page;
use Illuminate\View\View;

class PagesComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $pages = Page::where('type', config('page.type_header'))
            ->orWhere('type', config('page.type_footer'))
            ->orderBy('order', 'asc')
            ->with([
                'translate' => function ($query) {
                    $query->select([
                        'id',
                        'page_id',
                        'lang',
                        'name',
                    ]);
                }
            ])
            ->get();

        $view->with('pages', $pages);
    }
}
