<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_user_id',
        'to_user_id',
        'name',
        'description',
        'path',
        'is_read'
    ];

    public function from_user() {
        return $this->belongsTo(User::class, 'from_user_id', 'id');
    }

    public function to_user() {
        return $this->belongsTo(User::class, 'to_user_id', 'id');
    }
}
