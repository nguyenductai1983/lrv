<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Address extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'address_1',
        'address_2',
        'email',
        'telephone',
        'cellphone',
        'postal_code',
        'country_id',
        'province_id',
        'city_id',
        'ward_id',
        'customer_id',
        'relationship'
    ];

    /**
     * Belong to Province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province() {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /**
     * Belong to City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ward() {
        return $this->belongsTo(City::class, 'ward_id', 'id');
    }
    public function city() {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function getFullNameAttribute() {
        return "{$this->attributes['last_name']} {$this->attributes['middle_name']} {$this->attributes['first_name']}";
    }

    public function getShippingAddressAttribute() {
        $address = "{$this->attributes['last_name']} {$this->attributes['middle_name']} {$this->attributes['first_name']}; {$this->attributes['telephone']}; {$this->attributes['address_1']}";
        $city = City::find($this->attributes['city_id']);
        $province = Province::find($this->attributes['province_id']);
        if (!empty($city)) {
            $address = $address . ', ' . $city->name;
        }
        if (!empty($province)) {
            $address = $address . ', ' . $province->name;
        }
        return $address;
    }

}
