<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Components\ConvertsUtil;
use App\Enum\ShippingPackageLocalEnum;
use App\User;

class ShippingPackageLocal extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_shipping_package_local';

    public function user() {
        return $this->belongsTo(User::class, 'create_user_id', 'id');
    }

    public function warehouse() {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'id');
    }
    public function order() {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
    public function getStatusNameAttribute() {
        switch ($this->attributes['status']){
            case ShippingPackageLocalEnum::APPROVED:
                return "Đã duyệt";
            case ShippingPackageLocalEnum::CANCEL:
                return "Đã hủy";
            case ShippingPackageLocalEnum::DELIVERY_CARRIER:
                return "Đã giao hãng vận chuyển";
            case ShippingPackageLocalEnum::CARRIER_PICKUP_FAIL:
                return "Hvc pickup thất bại";
            case ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS:
                return "Giao hàng thành công";
            case ShippingPackageLocalEnum::CARRIER_DELIVERY_FAIL:
                return "Hvc giao hàng thất bại";
            default:
                return "Chờ duyệt";
        }
    }

    public function getCreatedDateAttribute() {
        return ConvertsUtil::strToDate($this->attributes['created_at']);
    }

    public function getSentDateAttribute() {
        return ConvertsUtil::strToDate($this->attributes['sent_at']);
    }

    public function getCustomerReceivedDateAttribute() {
        return ConvertsUtil::strToDate($this->attributes['customer_received_at']);
    }
}
