<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Components\ConvertsUtil;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\EshiperEnum;
use App\Enum\QuoteTypeEnum;

class Order extends Model {

    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'id'
    ];

    protected $casts = [
        'addtional_service' => 'array',
        'eshiper_service' => 'array',
        'express_order' => 'array',
        'eshiper_service_list' => 'array'
    ];
    /**
     * Has one customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function shipment() {
        return $this->belongsTo(Shipment::class, 'shipment_id', 'id');
    }

    public function warehouse() {
        return $this->belongsTo(Warehouse::class, 'from_warehouse_id', 'id');
    }
    public function warehouse_package() {
        return $this->belongsTo(WarehousePackage::class, 'warehouse_packge_id', 'id');
    }
    public function voucher() {
        return $this->belongsTo(Voucher::class, 'voucher_id', 'id');
    }
    public function receiver() {
        return $this->belongsTo(\App\Address::class, 'shipping_address_id', 'id');
    }

    public function agency() {
        return $this->belongsTo(Agency::class, 'agency_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function order_items() {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }
    public function list_items() {
        return $this->hasMany('App\OrderItem', 'order_id', 'id');
    }
    public function shipping_package_locals() {
        return $this->hasMany(ShippingPackageLocal::class, 'order_id', 'id');
    }

    public function getShippingMethodNameAttribute() {
        $shipping_local = $this->shipping_package_locals()->where('status','!=', ShippingPackageLocalEnum::CANCEL)->first();
        if(!empty($shipping_local)){
            return $shipping_local->shipping_method_provider_name;
        }
        return '';
    }

    public function getIsDeliveryAttribute() {
        $shipping_local = $this->shipping_package_locals()->where('status','!=', ShippingPackageLocalEnum::CANCEL)->first();
        if(!empty($shipping_local)){
            return true;
        }
        return false;
    }

    public function getFromWarehouseNameAttribute() {
        $shipping_local = $this->shipping_package_locals()->where('status','!=', ShippingPackageLocalEnum::CANCEL)->first();
        if(!empty($shipping_local->warehouse->name)){
            return $shipping_local->warehouse->name;
        }
        return '';
    }

    public function getShipmentWarehouseNameAttribute() {
        $shipment = $this->shipment()->where('id','=', $this->attributes['shipment_id'])->first();
        if(!empty($shipment->warehouse->name)){
            return $shipment->warehouse->name;
        }
        return '';
    }

    public function sender_city() {
        return $this->belongsTo(City::class, 'sender_city_id', 'id');
    }

    public function sender_province() {
        return $this->belongsTo(Province::class, 'sender_province_id', 'id');
    }

    public function sender_country() {
        return $this->belongsTo(Country::class, 'sender_country_id', 'id');
    }

    public function receiver_city() {
        return $this->belongsTo(City::class, 'receiver_city_id', 'id');
    }
    public function receiver_ward() {
        return $this->belongsTo(Ward::class, 'receiver_ward_id', 'id');
    }

    public function receiver_province() {
        return $this->belongsTo(Province::class, 'receiver_province_id', 'id');
    }

    public function receiver_country() {
        return $this->belongsTo(Country::class, 'receiver_country_id', 'id');
    }

    public function getSenderFullNameAttribute() {
        return "{$this->attributes['sender_last_name']} {$this->attributes['sender_middle_name']} {$this->attributes['sender_first_name']}";
    }

    public function getReceiveFullNameAttribute() {
        return "{$this->attributes['receive_last_name']} {$this->attributes['receiver_middle_name']} {$this->attributes['receive_first_name']}";
    }

    public function getCreatedDateAttribute() {
        return $this->created_at->format(config('app.date_format'));
    }

    public function getLastPaymentDateAttribute() {
        if(!empty($this->attributes['last_payment_at'])){
            return ConvertsUtil::strToDate($this->attributes['last_payment_at']);
        }
        return null;
    }

    public function getShippingStatusNameAttribute() {
        switch ($this->attributes['shipping_status']) {
            case OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN:
                return __('order.shipping_status_stockin');
            case OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT:
                return __('order.shipping_status_shipment');
            case OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_OUT:
                return __('order.shipping_status_stockout');
            case OrderShippingStatusEnum::SHIPPING_STATUS_REVICER:
                return __('order.shipping_status_local');
            case OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP:
                return __('order.shipping_status_ready');
            case OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER:
                return __('order.shipping_status_carrier');
            case OrderShippingStatusEnum::SHIPPING_STATUS_DONE:
                return __('order.shipping_status_done');
            case OrderShippingStatusEnum::SHIPPING_STATUS_DELAY:
                return substr(__('order.shipping_status_delay'),0,40);
            case OrderShippingStatusEnum::SHIPPING_IN_THE_CUSTOMS:
                return __('order.goods_in_the_customs');
            default:
                return __('order.shipping_status_new');
        }
    }

    public function getQuoteTypeNameAttribute() {
        switch ($this->attributes['package_type']) {
            case QuoteTypeEnum::SHOP_FOR_ME:
                return __('customer.qoute-shop');
            case QuoteTypeEnum::SHIP_FOR_ME:
                return __('customer.qoute-ship');
            default:
                return __('customer.qoute-shop');
        }
    }

    public function getExpressShippingStatusNameAttribute() {
        switch ($this->attributes['eshiper_shipping_status']) {
            case OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN:
                return __('order.shipping_status_stockin');
            case OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT:
                return __('order.shipping_status_shipment');
            case OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_OUT:
                return __('order.shipping_status_stockout');
            case OrderShippingStatusEnum::SHIPPING_STATUS_REVICER:
                return __('order.shipping_status_local');
            case OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP:
                return __('order.shipping_status_ready');
            case OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER:
                return __('order.shipping_status_carrier');
            case OrderShippingStatusEnum::SHIPPING_STATUS_DONE:
                return __('order.shipping_status_done');
            default:
                return __('order.shipping_status_new');
        }
    }

    public function getShippingStatusLabelAttribute() {
        if ($this->attributes['shipping_status'] == OrderShippingStatusEnum::SHIPPING_STATUS_NEW) {
            return "badge";
        } else if ($this->attributes['shipping_status'] == OrderShippingStatusEnum::SHIPPING_STATUS_DONE) {
            return "badge badge-success";
        } else {
            return "badge badge-warning";
        }
    }

    public function getReceiveStatusNameAttribute() {
        switch ($this->attributes['receive_status']) {
            case 1:
                return __('label.reciver_status_1');
            case 2:
                return __('label.reciver_status_2');
            default:
                return __('label.reciver_status_2');
        }
    }

    public function getReceiveStatusLabelAttribute() {
        if ($this->attributes['receive_status'] == 1) {
            return "badge badge-success";
        } else {
            return "badge badge-warning";
        }
    }

    public function getStatusClassAttribute() {
        switch ($this->attributes['order_status']) {
            case OrderStatusEnum::STATUS_PROCESSING:
                return 'text-warning';
            case OrderStatusEnum::STATUS_CANCEL:
                return 'text-danger';
            case OrderStatusEnum::STATUS_COMPLETE:
                return 'text-success';
            default:
                return 'text-default';
        }
    }

    public function getStatusNameAttribute() {
        switch ($this->attributes['order_status']) {
            case OrderStatusEnum::STATUS_PROCESSING:
                return __('order.status_processing');
            case OrderStatusEnum::STATUS_CANCEL:
                return __('order.status_cancel');
            case OrderStatusEnum::STATUS_COMPLETE:
                return __('order.status_complete');
            default:
                return __('order.status_new');
        }
    }

    public function getStatusLabelAttribute() {
        if ($this->attributes['order_status'] == OrderStatusEnum::STATUS_NEW) {
            return "badge";
        } else if ($this->attributes['order_status'] == OrderStatusEnum::STATUS_COMPLETE) {
            return "badge badge-success";
        } else if ($this->attributes['order_status'] == OrderStatusEnum::STATUS_CANCEL) {
            return "badge badge-danger";
        } else {
            return "badge badge-warning";
        }
    }

    public function getEshipperStatusNameAttribute() {
        switch ($this->attributes['eshiper_shipping_status']) {
            case EshiperEnum::READY_FOR_SHIPPING:
                return __('order.eshiper_status_ready_shipping');
            case EshiperEnum::IN_TRANSIT:
                return __('order.eshiper_status_in_transit');
            case EshiperEnum::DELIVERED:
                return __('order.eshiper_status_delivered');
            case EshiperEnum::CANCELLED:
                return __('order.eshiper_status_cancel');
            case EshiperEnum::EXCEPTION:
                return __('order.eshiper_status_exception');
            case EshiperEnum::CLOSED:
                return __('order.eshiper_status_closed');
            case EshiperEnum::READY_FOR_CHECKOUT_DHLEC:
                return __('order.eshiper_status_ready_checkout_dhlec');
            case EshiperEnum::READY_TO_PROCESS_DHLEC:
                return __('order.eshiper_status_ready_process_dhlec');
            default:
                return __('order.eshiper_status_undefine');
        }
    }

    public function getEshipperStatusLabelAttribute() {
        switch ($this->attributes['eshiper_shipping_status']) {
            case EshiperEnum::READY_FOR_SHIPPING:
                return "badge badge-warning";
            case EshiperEnum::IN_TRANSIT:
                return "badge badge-warning";
            case EshiperEnum::DELIVERED:
                return "badge badge-success";
            case EshiperEnum::CANCELLED:
                return "badge badge-danger";
            case EshiperEnum::EXCEPTION:
                return "badge badge-danger";
            case EshiperEnum::CLOSED:
                return "badge badge-success";
            case EshiperEnum::READY_FOR_CHECKOUT_DHLEC:
                return "badge badge-warning";
            case EshiperEnum::READY_TO_PROCESS_DHLEC:
                return "badge badge-warning";
            default:
                return "badge";
        }
    }

    public function getPaymentStatusLabelAttribute() {
        if ($this->attributes['payment_status'] == OrderPaymentStatusEnum::NOT_PAID) {
            return "badge";
        } else if ($this->attributes['payment_status'] == OrderPaymentStatusEnum::PART_PAID) {
            return "badge badge-warning";
        } else if ($this->attributes['payment_status'] == OrderPaymentStatusEnum::FULL_PAID) {
            return "badge badge-success";
        } else {
            return "badge badge-warning";
        }
    }

    public function getPaymentStatusNameAttribute() {
        switch ($this->attributes['payment_status']) {
            case OrderPaymentStatusEnum::PART_PAID:
                return __('order.payment_status_part_paid');
            case OrderPaymentStatusEnum::FULL_PAID:
                return __('order.payment_status_full_paid');
            case OrderPaymentStatusEnum::NOT_PAID:
                return __('order.payment_status_not_paid');
            default:
                return __('order.payment_status_not_paid');
        }
    }

    public function getStatusNewAttribute() {
        return OrderStatusEnum::STATUS_NEW;
    }

    public function getStatusProcessingAttribute() {
        return OrderStatusEnum::STATUS_PROCESSING;
    }

    public function getStatusCancelAttribute() {
        return OrderStatusEnum::STATUS_CANCEL;
    }

    public function getStatusCompleteAttribute() {
        return OrderStatusEnum::STATUS_COMPLETE;
    }

    public function getShippingStatusNewAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_NEW;
    }

    public function getShippingStatusStockInAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN;
    }

    public function getShippingStatusShipmentAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT;
    }

    public function getShippingStatusStockOutAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_OUT;
    }

    public function getShippingStatusRevicerAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
    }

    public function getShippingStatusReadyToShipAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
    }

    public function getShippingStatusCarrierAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
    }

    public function getShippingStatusDoneAttribute() {
        return OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
    }

    public function getShippingAddressAttribute() {
        $address = "{$this->attributes['receive_last_name']} {$this->attributes['receiver_middle_name']} {$this->attributes['receive_first_name']}; {$this->attributes['receiver_phone']}; {$this->attributes['receiver_address']}";
        $city = City::find($this->attributes['receiver_city_id']);
        $ward = Ward::find($this->attributes['receiver_ward_id']);
        $province = Province::find($this->attributes['receiver_province_id']);
        if (!empty($ward)) {
            $address = $address . ', ' . $ward->name;
        }
        if (!empty($city)) {
            $address = $address . ', ' . $city->name;
        }
        if (!empty($province)) {
            $address = $address . ', ' . $province->name;
        }
        return $address;
    }

    public function getVnpostReceiverAddressAttribute() {
        $address = $this->attributes['receiver_address'];
        $ward = Ward::find($this->attributes['receiver_ward_id']);
        $city = City::find($this->attributes['receiver_city_id']);
        $province = Province::find($this->attributes['receiver_province_id']);
        if (!empty($ward)) {
            $address = $address . ', ' . $ward->name;
        }
        if (!empty($city)) {
            $address = $address . ', ' . $city->name;
        }
        if (!empty($province)) {
            $address = $address . ', ' . $province->name;
        }
        return $address;
    }
    public function getUsersAttribute() {
        $user_id = $this->attributes['user_id'];
        return $user_id;
    }
}
