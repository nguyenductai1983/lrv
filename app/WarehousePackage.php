<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Components\ConvertsUtil;

class WarehousePackage extends Model {

    /**
     * Table to use
     */
    protected $table = 'tbl_warehouse_package';

    public function getStockinAtAttribute() {
        return ConvertsUtil::strToDate($this->attributes['stockin_at']);
    }

}
