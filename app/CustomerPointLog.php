<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\PointEnum;

class CustomerPointLog extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_customer_point_log';

    protected $fillable = [
        'customer_id',
        'point',
        'type'
    ];
    
    public function getTypeNameAttribute() {
        switch ($this->attributes['type']) {
            case PointEnum::TYPE_ADD_POINT:
                return __('point.transaction_add');
            default:
                return __('point.transaction_sub');
        }
    }
}
