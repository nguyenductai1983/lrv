<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingProvider extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_shipping_provider';
    
    protected $fillable = [
        'name',
        'description',
        'display_order'
    ];
}
