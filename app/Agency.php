<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Currency;
use App\DimensionUnit;
use App\WeightUnit;

class Agency extends Model
{
    /**
     * Table to use
     */
    protected $table = 'agencies';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'code',
        'name',
        'address',
        'phone',
        'note',
        'min_fee'
    ];

    /**
     * Relationship with products
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'agency_product', 'agency_id', 'product_id')
        ->withPivot(['pickup_fee', 'price', 'weight', 'currency_id', 'weight_unit_id', 'messure_unit_id', 'messure_unit_code', 'currency_code']);
    }

    /**
     * Relationship with user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'agency_id', 'id');
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    public function dimension_unit() {
        return $this->belongsTo(DimensionUnit::class, 'dimension_unit_id', 'id');
    }

    public function weight_unit() {
        return $this->belongsTo(WeightUnit::class, 'weight_unit_id', 'id');
    }
    public function pages() {
        return $this->belongsToMany(Page::class, 'agencie_page', 'agencie_id', 'page_id')->orderBy('page_id','desc');
    }
}
