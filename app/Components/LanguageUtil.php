<?php

namespace App\Components;

class LanguageUtil {

    public static function getKeyJavascript() {
        $data['err_sys'] = __('message.err_sys');
        $data['popup_notification'] = __('label.notification');
        $data['popup_confirm'] = __('label.confirm');
        $data['popup_btn_agree'] = __('label.agree');
        $data['popup_btn_close'] = __('label.close');
        $data['popup_bnt_cancel'] = __('label.cancel');
        $data['popup_bnt_change'] = __('label.change');

        $data['err_order_stockin'] = __('message.err_order_stockin');
        $data['err_add_stockin_exist'] = __('message.err_add_stockin_exist');
        $data['err_order_to_shipment'] = __('message.err_order_to_shipment');
        $data['err_add_to_shipment_exist'] = __('message.err_add_to_shipment_exist');
        $data['order_code'] = __('label.order_code');
        $data['no_records'] = __('label.no_records');
        $data['status'] = __('label.status');
        $data['create_time'] = __('label.create_time');
        $data['employee'] = __('label.employee');
        $data['view_history_shipping'] = __('transport.view_history_shipping');
        $data['view_history_log'] = __('transport.view_history_log');
        $data['confirm_cancel_order'] = __('transport.confirm_cancel_order');
        $data['confirm_restore_order'] = __('transport.confirm_restore_order');

        // cart
        $data['cart_empty'] = __('yhl.cart-empty');
        $data['agree_access'] = __('yhl.agree-access');
        $data['what_payment_method'] = __('yhl.what-payment-method');
        $data['pls_country'] = __('yhl.pls-country');
        $data['pls_province'] = __('yhl.pls-province');
        $data['pls_district'] = __('yhl.pls-district');
        $data['pls_ward'] = __('yhl.pls_ward');

        $data['claim_open'] = __('claim.open');
        $data['claim_title'] = __('claim.title');
        $data['claim_content'] = __('label.content');
        $data['address_delete_comfirm'] = __('message.address_delete_comfirm');

        // phiếu thu
        $data['receipt_del_item_title'] = __('receipt.del_item_title');
        $data['receipt_paid_title'] = __('receipt.paid_title');
        $data['receipt_amount'] = __('receipt.amount');
        $data['claim_complete'] = __('claim.complete');
        $data['reciver_status_1'] = __('label.reciver_status_1');
        $data['reciver_status_2'] = __('label.reciver_status_2');
        $data['create_extra'] = __('label.create_extra');
        $data['create_refund'] = __('label.create_refund');
        $data['amount'] = __('label.amount');
        $data['reason'] = __('label.reason');
        $data['extent_approve'] = __('label.extent_approve');
        $data['extent_payment'] = __('label.extent_payment');
        $data['extent_cancel'] = __('label.extent_cancel');
        $data['weight_volume'] = __('message.weight_volume');
        $data['msg_weight_volume'] = __('message.msg_weight_volume');
        $data['weight_volume_none'] = __('message.weight_volume_none');
        $data['notify_vip'] = __('message.notify_vip');
        $data['popup_btn_support'] = __('label.popup_btn_support');
        $data['eshipper_support'] = __('message.eshipper_support');
        $data['edit_info_customer'] = __('message.edit_info_customer');
        $data['notify_admin_vip'] = __('message.notify_admin_vip');
        $data['popup_confirm_point'] = __('message.popup_confirm_point');
        $data['get_coupon_code'] = __('message.get_coupon_code');
        $data['cell_phone'] = __('message.cell_phone');
        $data['add_box'] = __('message.add_box');
        $resault= json_encode($data);
        return $resault;
    }
    public static function getKeyTranports() {
        $data['weight_volume_none'] = __('message.weight_volume_none');
        $data['notify_vip'] = __('message.notify_vip');
        $data['popup_btn_support'] = __('label.popup_btn_support');
        $data['eshipper_support'] = __('message.eshipper_support');
        $data['edit_info_customer'] = __('message.edit_info_customer');
        $data['notify_admin_vip'] = __('message.notify_admin_vip');
        $data['popup_confirm_point'] = __('message.popup_confirm_point');
        $data['get_coupon_code'] = __('message.get_coupon_code');
        $data['cell_phone'] = __('message.cell_phone');
        $resault= json_encode($data);
        return $resault;
    }
    public static function getMTSKeyJavascript() {
        $data['err_sys'] = __('message.err_sys');
        $data['popup_notification'] = __('label.notification');
        $data['popup_confirm'] = __('label.confirm');
        $data['popup_btn_agree'] = __('label.agree');
        $data['popup_btn_close'] = __('label.close');
        $data['popup_bnt_cancel'] = __('label.cancel');
        $data['popup_bnt_change'] = __('label.change');

        $data['mts_amount3000'] = __('message.mts_amount3000');
        $data['mts_amount1000'] = __('message.mts_amount1000');
        $data['mts_reason'] = __('message.mts_reason');

        $data['id_card'] = __('customer.id_card');
        $data['card_expire'] = __('customer.card_expire');
        $data['birthday'] = __('customer.birthday');
        $data['career'] = __('customer.career');
        $data['check'] = __('customer.check');
        $data['image'] = __('customer.image_1_file_id');
        $data['relationship'] = __('receiver.relationship');
        $data['reason'] = __('mts.reason');
        $data['cell_phone'] = __('message.cell_phone');
        $resault= json_encode($data);
        return $resault;
    }

}
