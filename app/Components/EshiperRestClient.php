<?php
/**
 * Created by IntelliJ IDEA.
 * User: pc
 * Date: 11/04/2018
 * Time: 09:36 PM
 */

namespace App\Components;


class EshiperRestClient
{
    public static function callEshiper($xmlRequest) {
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, env('ESHIPER_API_TEST_URL','http://web.eshipper.com/rpc2'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return result (XML) rather than TRUE/FALSE
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

        $xmlResponse=curl_exec($ch);

        curl_close ($ch);

        return $xmlResponse;
    }
}
