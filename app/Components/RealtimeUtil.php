<?php

namespace App\Components;

use PubNub\PNConfiguration;
use PubNub\PubNub;

class RealtimeUtil
{
    protected $pnConfiguration;
    
    public function __construct(PNConfiguration $pnConfiguration) {
        $this->pnConfiguration = $pnConfiguration;
    }
    
    public function init()
    {
        $this->pnConfiguration->setSubscribeKey(config('app.pubnub_subcribe_key'));
        $this->pnConfiguration->setPublishKey(config('app.pubnub_publish_key'));
        $this->pnConfiguration->setSecretKey(config('app.pubnub_secret_key'));
        $this->pnConfiguration->setSecure(true);
        $this->pubnub = new PubNub($this->pnConfiguration);
        return $this;
    }

    public function publish(array $messages, $channel)
    {
        $this->init();
        return $this->pubnub->publish()->channel($channel)->message($messages)->sync();
    }

    public function registerChannel($channel)
    {
        $this->init();
        return $this->pubnub->grant()
            ->channels($channel)
            ->read(true)
            ->write(true)
            ->ttl(0)
            ->authKeys([])
            ->sync();
    }
}