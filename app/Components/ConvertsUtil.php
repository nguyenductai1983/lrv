<?php

namespace App\Components;

use App\WeightUnit;
use App\DimensionUnit;
use PhpParser\Node\Expr\Empty_;

class ConvertsUtil {
    public static function strToDateFormat($str, $fromFormat = 'd-m-Y', $toFormat = 'Y-m-d') {
        return date($toFormat, strtotime(date($fromFormat, strtotime(str_replace('/', '-', $str)))));
    }

    public static function strToDate($str, $format = 'd/m/Y') {
        $time = strtotime($str);

        return date($format, $time);
    }

    public static function getDimension($dimension, DimensionUnit $fromDimensionUnit, DimensionUnit $toDimensionUnit){
        return ($dimension * $toDimensionUnit->radio)/ $fromDimensionUnit->radio;
    }

    public static function getWeight($weight, WeightUnit $fromWeightUnit, WeightUnit $toWeightUnit){
        return (int) (($weight * $toWeightUnit->radio) / $fromWeightUnit->radio);
    }

    public static function getWPackageCode($whCode, $id) {
        return $whCode . "_" . $id;
    }

    public static function getSPackageCode($whCode, $id) {
        return "SM_". $whCode . "_" . $id;
    }

    public static function getVoucherCode( $userCode, $id, $Prefixes=NULL) {
        if(!empty($Prefixes))
        {
            $Prefixes=$Prefixes."_";
        }
        return "PT_".$Prefixes.$userCode . "_" . date('ymd') . "_" . $id;
    }

    public static function dateFormat($str, $format = 'd/m/Y H:i:s', $formatDb = 'Y-m-d H:i:s') {
        return \DateTime::createFromFormat($format, $str)->format($formatDb);
    }

    public static function dateAddDay($str, $day, $format = 'Y-m-d H:i:s', $toFormat = ''){
        $date = \DateTime::createFromFormat($format, $str);
        $date->modify("+{$day} day");
        if(empty($toFormat)){
            return $date->format(config('app.date_format_db'));
        }
        return $date->format($toFormat);
    }

    public static function numberFormat($number, $decimals = 0, $type = false) {
        if (empty($number) || $number <= 0) {
            return 0;
        }
        if ($type) {
            return number_format($number, $decimals, '.', ','); // ex: 100.000,00
        }
        return number_format($number, $decimals, ',', '.'); // ex: 100,000.00
    }

    public static function roundNumber($number, $decimals = 2){
        if (empty($number) || $number <= 0) {
            return 0;
        }
        return round($number, $decimals);
    }
     public static function convert_int_to_date($int_number)
    {
        $date = new \DateTime();
        $date->setTimestamp(intval($int_number)/1000);
        $result = $date->format('Y-m-d H:i:s');
        return $result;
    }
}
