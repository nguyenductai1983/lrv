<?php

namespace App\Components;

use Illuminate\Support\Facades\Log;
use App\Enum\VnPostEnum;
use App\ShippingPackageLocal;
use App\Order;
use App\Warehouse;

class VNPostClient{

    public static function createOrder($typeService, ShippingPackageLocal $shippingPackageLocal, Order $shippingAddress, Warehouse $warehouse) {
        $client = new \SoapClient(config('app.hcmpost.uri'));
        return $client->NewOrder([
            'sK' => config('app.hcmpost.sK'),
            'sI' => config('app.hcmpost.sI'),
            'sT' => self::buildParamOrder($typeService, $shippingPackageLocal, $shippingAddress, $warehouse),
            'sE' => ""
        ]);
    }

    public static function cancelOrder($code) {
        $client = new \SoapClient(config('app.hcmpost.uri'));

        return $client->CancelOrder([
            'sK' => config('app.hcmpost.sK'),
            'sI' => config('app.hcmpost.sI'),
            'sO' => "{$code}",
            'sE' => ""
        ]);
    }

    private static function buildParamOrder($typeService, ShippingPackageLocal $shippingPackageLocal, Order $shippingAddress, Warehouse $warehouse) {
        $data = [];
        $data["sMaDonHang"] = $shippingPackageLocal->shipping_code;
        $data["sNguoiLH"] = $warehouse->contact_person;
        $data["sDTLH"] = $warehouse->telephone;
        $data["sDiaChiNhan"] = $warehouse->address;
        $data["sQuanHuyenNhan"] = "{$warehouse->city->vnpost_alias}";
        $data["sTinhNhan"] = "{$warehouse->province->vnpost_alias}";
        $data["sTenNguoiGui"] = $shippingAddress->receive_full_name;
        $data["sDTGui"] = $shippingAddress->receiver_phone;
        // $data["sDiaChiGui"] = $shippingAddress->receiver_address;
        $data["sDiaChiGui"] = $shippingAddress->vnpost_receiver_address;
        $data["sEmail"] = !empty($shippingAddress->receiver_email) ? $shippingAddress->receiver_email : "";
        $data["sTinhGui"] = "{$shippingAddress->receiver_province->vnpost_alias}";
        $data["sQuanHuyenGui"] = "{$shippingAddress->receiver_city->vnpost_alias}";
        $data["sGhiChu"] = !empty($shippingPackageLocal->description) ? $shippingPackageLocal->description : "";
        $data["sSoLuong"] = "{$shippingPackageLocal->total_quantity_item}";
        $data["sTrongLuong"] = "{$shippingPackageLocal->weight}";
        $data["sCuoc"] = "0";
        $data["sGiaTriHang"] = "{$shippingPackageLocal->total_declare_goods}";
        $data["sTienCOD"] = "0";
        $data["sLoaiHang"] = VnPostEnum::TYPE_GOODS_WH; // 1: Dùng cho hàng kho; 2: Dùng cho hàng drop off
        $data["sLoaiDV"] = $typeService; // 1: chuyển nhanh; 2: thường
        $data["sSoHieu"] = "";
        $data["sCode"] = VnPostEnum::TYPE_PICKUP_WH; // 1 : Lấy tại kho, 2: hàng picking-up; 3: hàng drop-off
        $data["sIDShop"] = "";
        $data["sTenShop"] = "";
        // print_r($data);die();
        // Log::info(json_encode($data));
        return $data;
    }
}
