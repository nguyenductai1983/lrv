<?php

namespace App\Components;

use Illuminate\Support\Facades\Log;
use App\ShippingPackageLocal;
use App\Order;
use App\Warehouse;

//use Aws\DynamoDb\NumberValue;

class myVNPostComponents{
    public static function GetAccessToken() {
        $data =[];
        $data['TenDangNhap']=config('vnptpost.GetAccessToken.TenDangNhap');
        $data['MatKhau']=config('vnptpost.GetAccessToken.MatKhau');
        $data= json_encode($data);
        return $data;
    }
    public static function CreateOrder($typeService, ShippingPackageLocal $shippingPackageLocal, Order $shippingAddress, Warehouse $warehouse) {
        $data = [];
        $data["OrderCode"] = $shippingPackageLocal->shipping_code;
        $data["CustomerCode"] = config('vnptpost.CustomerCode');
        $data["VendorId"] = 1;
        $data["PickupType"] = 1;//$warehouse->type_pickup;
        // 1 Pickup - thu gom tận nơi, 2 Dropoff - gửi hàng tại bưu cục
        $data["IsPackageViewable"] = 0; // Có cho xem hàng hay không
        $data["PackageContent"] = !empty($shippingPackageLocal->description) ? $shippingPackageLocal->description : "";
        //Nội dung hàng hóa - (maxlength: 255)
        $data["ServiceName"] = $typeService;
// Mã dịch vụ chuyển phát *:
// - EMS: chuyển phát nhanh
// - BK: chuyển phát thường
// - ECOD: chuyển phát tiết kiệm (chỉ dành cho các đơn hàng liên tỉnh có áp dụng COD)
// - DONG_GIA: dịch vụ thỏa thuận riêng giữa khách hàng và VNPost (cần liên hệ bưu cục phục vụ của khách hàng nếu chưa rõ)
      //  $data["PickupPoscode"] = !empty($warehouse->post_code) ? (int)$warehouse->post_code : null;
        $data["PickupPoscode"] =  null;
        // mã bưu điện thêm vào ngày 21-05-2020
        $data["SenderFullname"] = $warehouse->contact_person; //Họ tên người gửi * (maxlength: 255)
        $data["SenderAddress"] = $warehouse->address;
//Địa chi người gửi * (không bao gồm phường xã, quận huyện, tỉnh thành) (maxlength: 150)
        $data["SenderTel"] = $warehouse->telephone; //Số điện thoại người gửi * (maxlength: 50)
        $data["SenderProvinceId"] = $warehouse->province->vnpost_alias;
        $data["SenderDistrictId"] = $warehouse->city->vnpost_alias;
        $data["SenderWardId"] =$warehouse->ward->vnpost_alias;
        if(!empty($shippingAddress->receiver_ward)){
            $data["ReceiverWardId"] = $shippingAddress->receiver_ward->vnpost_alias;
       }
       else
       {
           $data["ReceiverWardId"] =null;
       }
        $data["ReceiverFullname"] = $shippingAddress->receive_full_name;
        $data["ReceiverAddress"] = $shippingAddress->receiver_address;//vnpost_
        $data["ReceiverProvinceId"] = $shippingAddress->receiver_province->vnpost_alias;
        $data["ReceiverDistrictId"] = $shippingAddress->receiver_city->vnpost_alias;
        $data["ReceiverTel"] = $shippingAddress->receiver_phone;
        $data["ReceiverWardId"] = $shippingAddress->receiver_ward->vnpost_alias;
        $data["ShipperTel"] = "";
        $data["CodAmount"] = "0";
        $data["OrderAmount"] = "0";
        $data["CodAmountEvaluation"] = "0";
        $data["OrderAmountEvaluation"] = "0";
        $data["WeightEvaluation"] = $shippingPackageLocal->weight;
        $data["WidthEvaluation"] = $shippingPackageLocal->width;
        //Chiều rộng hàng hóa (tạm tính) - đơn vị là cm, tố đa 100 m Đây là chiều rộng do khách hàng nhập,
        //sau khi hàng thu gom về bưu cục, nhân viên bưu cục sẽ thực hiện cân lại và nhập vào trường Width
        $data["LengthEvaluation"] = $shippingPackageLocal->length;
        //Chiều dài hàng hóa (tạm tính) - đơn vị là cm, tối đa 100 m Đây là chiều dài do khách hàng nhập,
        //sau khi hàng thu gom về bưu cục, nhân viên bưu cục sẽ thực hiện cân lại và nhập vào trường Length
        $data["HeightEvaluation"] = $shippingPackageLocal->height;
        //Chiều cao hàng hóa (tạm tính) - đơn vị là cm, tối đa 100 m Đây là chiều cao do khách hàng nhập,
        //sau khi hàng thu gom về bưu cục, nhân viên bưu cục sẽ thực hiện cân lại và nhập vào trường Height
        $data["VASIds"] = [1, 2] ;
        $data["IsReceiverPayFreight"] = true;
        $data["CustomerNote"] = $shippingAddress->note ;
        $data["ItemCode"] = "" ;
        $data["DraftOrderId"] = "56f7278e-089d-420c-9047-642b1a5a1d3c" ;
        $data["IsDeleteDraft"] =true ;
        $data["SenderAddressType"] = 1 ;
        $data["ReceiverAddressType"] = 1 ;
        return $data;
        Log::error(json_encode($data));
    }
}
