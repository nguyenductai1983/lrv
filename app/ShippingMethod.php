<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Enum\ShippingMethodProviderEnum;

class ShippingMethod extends Model
{
    /**
     * Table to use
     */
    protected $table = 'tbl_shipping_method';

    protected $fillable = [
        'name',
        'description',
        'display_order',
        'is_active',
    ];

    public function getIsActiveNameAttribute() {
        switch ($this->attributes['is_active']) {
            case ShippingMethodProviderEnum::IS_ACTIVE_TRUE:
                return __('shipping.is_active_true');
            default:
                return __('shipping.is_active_fail');
        }
    }
}
