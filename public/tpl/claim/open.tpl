<form id="open-claim-form">
    <div class="form-group">
        <label class="control-label"><%= language.claim_title %></label>
        <input type="text" name="title" class="form-control form-white">
    </div>
    <div class="form-group">
        <label class="control-label"><%= language.claim_content %></label>
        <input type="text" name="content" class="form-control form-white">
    </div>
    <input type="hidden" name="order_id" value="<%= id%>">
    <input type="hidden" name="_token" value="<%= token%>">
</form>
