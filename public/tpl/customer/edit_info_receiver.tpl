<form id="update-info-receiver">
    <div class="form-group">
        <label class="control-label" for="first_name">Tên (*)</label>
        <input type="text" class="form-control" name="first_name" value="<%= data.address.first_name %>" autocomplete="off"/>
    </div>
    <div class="form-group">
        <label class="control-label" for="middle_name">Tên lót</label>
        <input type="text" class="form-control" name="middle_name" value="<%= data.address.middle_name %>" autocomplete="off"/>
    </div>
    <div class="form-group">
        <label class="control-label" for="last_name">Họ (*)</label>
        <input type="text" class="form-control" name="last_name" value="<%= data.address.last_name %>" autocomplete="off"/>
    </div>
    <div class="form-group">
        <label class="control-label" for="address_1">Địa chỉ (*)</label>
        <input type="text" class="form-control" name="address_1" value="<%= data.address.address_1 %>" autocomplete="off"/>
    </div>
    <div class="form-group">
        <label class="control-label" for="country_id">Quốc gia (*)</label>
        <select name="country_id" id="country_id" class="form-control">
            <option value="">Chọn quốc gia</option>
            <% data.countries.forEach(function(country) { %>
                <option value="<%= country.id %>" <% if(data.address.country_id == country.id){ %> selected <% } %>><%= country.code %> - <%= country.name %></option>
            <% }); %>
        </select>
    </div>
    <div class="form-group">
        <label class="control-label" for="province_id">Tỉnh thành (*)</label>
        <select name="province_id" id="province_id" class="form-control">
            <option value="">Chọn tỉnh thành</option>
        </select>
    </div>
    <div class="form-group">
        <label class="control-label" for="city_id">Quận huyện (*)</label>
        <select name="city_id" id="city_id" class="form-control">
            <option value="">Chọn quận huyện</option>
        </select>
    </div>
    <div class="form-group">
        <label class="control-label" for="postal_code">Mã bưu chính (*)</label>
        <input type="text" class="form-control" name="postal_code" value="<%= data.address.postal_code %>" autocomplete="off"/>
    </div>
    <div class="form-group">
        <label class="control-label" for="telephone">Phone (*)</label>
        <input type="text" class="form-control" name="telephone" value="<%= data.address.telephone %>" autocomplete="off"/>
    </div>
    <div class="form-group">
        <label class="control-label" for="cellphone">Cellphone</label>
        <input type="text" class="form-control" name="cellphone" value="<%= data.address.cellphone %>" autocomplete="off"/>
    </div>
    
    <input type="hidden" name="address_id" value="<%= data.address.id %>">
    <input type="hidden" name="id" value="<%= order_id %>">
    <input type="hidden" name="curr_province_id" value="<%= data.address.province_id %>">
    <input type="hidden" name="curr_city_id" value="<%= data.address.city_id %>">
    <input type="hidden" name="_token" value="<%= _token %>">
</form>

<script type="text/javascript">
    $(function () {
        var curr_province_id = parseInt($('input[name=curr_province_id]').val());
        var curr_city_id = parseInt($('input[name=curr_city_id]').val());
        var countryEle = $('select#country_id');
        var provinceEle = $('select#province_id');
        var cityEle = $('select#city_id');
        console.log(curr_province_id);
        var getProvinces = function () {
            var countryId = parseInt(countryEle.val()),
                data      = {
                    country_id: countryId
                };

            provinceEle.prop('disabled', true).find('option[value!=""]').remove();
            $.ajax({
                method     : 'GET',
                url        : '/api/provinces?' + toUrlEncodedString(data),
                contentType: 'appplication/json',
                dataType   : 'json'
            }).done(function (data) {
                $.each(data.provinces, function (key, province) {
                    if(province.id == curr_province_id){
                        var option = '<option value="' + province.id + '" selected>' + province.name + '</option>';
                    }else{
                        var option = '<option value="' + province.id + '">' + province.name + '</option>';
                    }
                    provinceEle.append(option);
                });
                provinceEle.prop('disabled', false).trigger('change');
            }).fail(function (jqXHR) {
                console.log(jqXHR);
            });
        };

        var getCities = function () {
            var countryId  = parseInt(countryEle.val()),
                provinceId = parseInt(provinceEle.val()),
                data       = {
                    country_id : countryId,
                    province_id: provinceId
                };

            cityEle.prop('disabled', true).find('option[value!=""]').remove();
            $.ajax({
                method     : 'GET',
                url        : '/api/cities?' + toUrlEncodedString(data),
                contentType: 'appplication/json',
                dataType   : 'json'
            }).done(function (data) {
                $.each(data.cities, function (key, city) {
                    if(city.id == curr_city_id){
                        var option = '<option value="' + city.id + '" selected>'+ city.name +'</option>';
                    }else{
                        var option = '<option value="' + city.id + '">'+ city.name +'</option>';
                    }
                    cityEle.append(option);
                });
                cityEle.prop('disabled', false);
            }).fail(function (jqXHR) {
                console.log(jqXHR);
            });
        };
        getProvinces();
        getCities();

        countryEle.change(function () {
            getProvinces();
        });

        provinceEle.change(function () {
            getCities();
        });
    });
</script>
