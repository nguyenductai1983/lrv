<form id="create-extra-form">
    <div class="form-group">
        <label class="control-label"><%= language.amount %></label>
        <input type="text" name="amount" value="" class="form-control form-white">
    </div>
    <div class="form-group">
        <label class="control-label"><%= language.reason %></label>
        <textarea name="reason" class="form-control form-white"></textarea>
    </div>
    <input type="hidden" name="order_id" value="<%= order_id%>">
    <input type="hidden" name="_token" value="<%= token%>">
</form>
