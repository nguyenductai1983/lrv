<form id="receipt-paid-form">
    <div class="form-group">
        <label class="control-label"><%= language.receipt_amount %></label>
        <input type="text" name="amount" value="<%= amount %>" readonly class="form-control form-white">
    </div>
    <input type="hidden" name="voucher_id" value="<%= id%>">
    <input type="hidden" name="_token" value="<%= token%>">
</form>
