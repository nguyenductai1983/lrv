<form>
    <div class="tab-box">
        <div class="manage-pd-box">
            <div class="table table-popup">
                <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th><%= language.status %></th>
                            <th><%= language.employee %></th>
                            <th><%= language.create_time %></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% if(data.log.length == 0){ %>
                            <tr>
                                <td colspan="7"><%= language.no_records %></td>
                            </tr>
                        <% }else{ %>
                            <% $.each(data.log, function (key, value) { %>
                            <tr>
                                <td><%= data.status[value.status]%></td>
                                <td><%= value.user%></td>
                                <td><%= value.created_at%></td>
                            </tr>
                            <% });%>
                        <% }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>