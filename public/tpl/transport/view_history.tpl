<form>
    <div class="tab-box">
        <div class="manage-pd-box">
            <div class="table table-popup">
                <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th width="20%"><%= language.status %></th>
                            <th width="20%"><%= language.employee %></th>
                            <th width="20%"><%= language.create_time %></th>
                            <th width="20%">Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% if(typeof data.tracking === undefined || data.tracking.length == 0){ %>
                            <tr>
                                <td colspan="7"><%= language.no_records %></td>
                            </tr>
                        <% }else{ %>
                            <% $.each(data.tracking, function (key, value) { %>
                            <tr>
                                <td><%= data.status[value.status]%></td>
                                <td><%= value.user%></td>
                                <td><%= value.created_at%></td>
                                <td><%= value.response_api%></td>
                            </tr>
                            <% });%>
                        <% }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
