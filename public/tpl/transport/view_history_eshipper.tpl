<form>
    <div class="tab-box">
        <div class="manage-pd-box">
            <div class="table table-popup">
                <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th><%= language.status %></th>
                            <th><%= language.create_time %></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% if(data.length == 0){ %>
                            <tr>
                                <td colspan="2"><%= language.no_records %></td>
                            </tr>
                        <% }else{ %>
                            <% $.each(data, function (key, value) { %>
                            <tr>
                                <td><%= value.name%></td>
                                <td><%= value.date%></td>
                            </tr>
                            <% });%>
                        <% }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>