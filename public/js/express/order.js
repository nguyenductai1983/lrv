var order = {};
order.export = function () {
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()
    };
    var url = "/admin/express/export?" + $.param(query);
   window.location.href = url;
};
order.exportcustomer = function () {
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()
    };
    var url = "/admin/customer/express/export?" + $.param(query);
   window.location.href = url;
};
order.cancel = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/express/cancel-order',
            method: "POST",
            loading: true,
            data: {
                id: id,
                _token : $('meta[name="token"]').attr('content')
            },

            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};

order.adminCancel = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/express/cancel-order',
            method: "POST",
            loading: true,
            data: {
                id: id,
                _token : $('meta[name="token"]').attr('content')
            },

            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};

order.viewHistoryEshiper = function (id) {
    fly.ajax({
        service: '/admin/express/view-history-eshipper',
        method: "POST",
        loading: true,
        data: {
            id: id
        },
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('express/view_history_eshipper.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};


order.cusViewHistoryEshiper = function (id) {
    fly.ajax({
        service: '/admin/customer/express/view-history-eshipper',
        method: "POST",
        loading: true,
        data: {
            id: id
        },
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('express/view_history_eshipper.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};

order.quoteViewHistoryEshiper = function (id) {
    fly.ajax({
        service: '/express/' + id + '/view-history',
        method: "GET",
        loading: true,
        data: {

        },
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('express/view_history_eshipper.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};
