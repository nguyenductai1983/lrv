(function () {
    'use strict';

    var app = angular.module('ExpressApp', []);
    var body_weight = 115;
    app.controller('ExpressCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.isEditDiscount = false;
        $scope.submittedSearch = false;
        $scope.submittedConfirm = false;
        $scope.is_custom = 2;
        $scope.isUseCoupon = false;
        $scope.showSearch = true;
        $scope.disable_from=true;
        $scope.initErrors = function () {
            $scope.errors = {
                addressFrom : [],
                addressTo : [],
                packages: [],
                quotes : [],
                pickups : [],
                custom : [],
                Coupon: [],
            };
        };
        $scope.serviceIndex = null;
        $scope.showCreate = false;
        $scope.initErrors();
        $scope.countries = [];
        $scope.provincesFrom = [];
        $scope.provincesTo = [];
        $scope.provincesCustom = [];
        $scope.citiesFrom = [];
        $scope.citiesTo = [];
        $scope.citiesCustom = [];
        $scope.pickup = [];
        $scope.dimTypes = [];
        $scope.quote = [];
        $scope.packageTypes = [
            {
                'id': 1,
                'name': 'Envelope'
            },{
                'id': 2,
                'name': 'Pak'
            },{
                'id': 3,
                'name': 'Package'
            },{
                'id': 5,
                'name': 'SmarteHome'
            }
        ];
        $scope.hourTimes = [
            {
                'id': '07',
                'name': '07'
            },{
                'id': '08',
                'name': '08'
            },{
                'id': '09',
                'name': '09'
            },{
                'id': '10',
                'name': '10'
            },{
                'id': '11',
                'name': '11'
            },{
                'id': '12',
                'name': '12'
            },{
                'id': '13',
                'name': '13'
            },{
                'id': '14',
                'name': '14'
            },{
                'id': '15',
                'name': '15'
            },{
                'id': '16',
                'name': '16'
            },{
                'id': '17',
                'name': '17'
            }
        ];

        $scope.minuteTimes = [
            {
                'id': '00',
                'name': '00'
            },{
                'id': '15',
                'name': '15'
            },{
                'id': '30',
                'name': '30'
            },{
                'id': '45',
                'name': '45'
            }
        ];

        $scope.pickupLocation = [
            {
                'id': 'Receiving',
                'name': 'Receiving'
            },{
                'id': 'Garage',
                'name': 'Garage'
            },{
                'id': 'Lobby',
                'name': 'Lobby'
            },{
                'id': 'Reception',
                'name': 'Reception'
            },{
                'id': 'Front Desk',
                'name': 'Front Desk'
            },{
                'id': 'Vault',
                'name': 'Vault'
            },{
                'id': 'Switchboard',
                'name': 'Switchboard'
            },{
                'id': 'Back Door',
                'name': 'Back Door'
            },{
                'id': 'Desk',
                'name': 'Desk'
            },{
                'id': 'Between Doors',
                'name': 'Between Doors'
            },{
                'id': 'Kiosk',
                'name': 'Kiosk'
            },{
                'id': 'Office',
                'name': 'Office'
            },{
                'id': 'Outside Door',
                'name': 'Outside Door'
            },{
                'id': 'Mailbox',
                'name': 'Mailbox'
            },{
                'id': 'Side Door',
                'name': 'Side Door'
            },{
                'id': 'Service Counter',
                'name': 'Service Counter'
            },{
                'id': 'Security',
                'name': 'Security'
            },{
                'id': 'Shipping',
                'name': 'Shipping'
            },{
                'id': 'Front Door',
                'name': 'Front Door'
            },{
                'id': 'Basement',
                'name': 'Basement'
            },{
                'id': 'Mail Room',
                'name': 'Mail Room'
            },{
                'id': 'Lab',
                'name': 'Lab'
            },{
                'id': 'Warehouse',
                'name': 'Warehouse'
            },{
                'id': 'Pharmacy',
                'name': 'Pharmacy'
            },{
                'id': 'Pro Shop',
                'name': 'Pro Shop'
            },{
                'id': 'Parts Department',
                'name': 'Parts Department'
            },{
                'id': 'Counter',
                'name': 'Counter'
            },{
                'id': 'Loading Dock',
                'name': 'Loading Dock'
            },{
                'id': 'Gate House',
                'name': 'Gate House'
            }
        ];
        $scope.product = [];
        $scope.quotes = [];
        $scope.addressFrom = {
            first_name : null,
            middle_name : null,
            last_name : null,
            address_1  : null,
            address_2  : null,
            country_id : 14,
            city_id : null,
            province_id: null,
            postal_code  : null,
            telephone  : null,
            email  : null,
            attention  : null,
            instruction  : null,
            shipping_date  : null,
            confirm_delivery  : false,
            residential  : false
        };
        $scope.addressTo = {
            first_name : null,
            middle_name : null,
            last_name : null,
            address_1  : null,
            address_2       : null,
            country_id : 47,
            province_id: null,
            city_id: null,
            postal_code  : null,
            telephone  : null,
            email  : null,
            attention  : null,
            instruction  : null,
            notify_recipient  : false,
            residential  : false
        };
        $scope.initPackages = function () {
            var product = {
                id:null,
                length: 1,
                width: 1,
                height: 1,
                volume: parseFloat (1 / body_weight),
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue:0.0,
                per_discount: 0,
                description: null
            };
            $scope.packages = {
                type: 3,
                quantity: 1,
                referenceCode: null,
                dimType: 3,
                boxes : []
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };
        $scope.initPickup = function(){
            $scope.pickup = {
                is_schedule: "1",
                contact_name: null,
                phone_number: null,
                location: null,
                date_time: null,
                start_hour_time: null,
                start_minute_time: null,
                closing_hour_time: null,
                closing_minute_time: null
            };
        };

        $scope.changeSchedulePickup = function () {
            setTimeout(function () {
                $("#pickup-date").datepicker({dateFormat: "dd/mm/yy"});
            });
        };

        $scope.initItem = function(){
            $scope.item = {
                description: null,
                code: '12345',
                country_id: null,
                country_code: null,
                quantity: null,
                unitPrice: null,
                subPrice: null
            };
        };

        $scope.initCustom = function (){
            $scope.custom = {
                company: null,
                name: null,
                brokerName: null,
                taxId: null,
                phone: null,
                receiptsTaxId: null,
                total: 0,
                items: []
            };

            $scope.custom.billTo = {
                company: null,
                name: null,
                address_1: null,
                country_id: null,
                province_id: null,
                city_id: null,
                postal_code: null
            };

            $scope.custom.dutiesTaxes = {
                dutiable: 'false',
                billTo: 'receiver',
                consigneeAccount: null,
                sedNumber: null
            };

            $scope.initItem();
        };

        $scope.initPackages();
        $scope.initPickup();
        $scope.initCustom();

        $scope.services = {
            saturdayDelivery: null,
            saturdayPickup: null,
            holdForPickup: null,
            docsOnly: null,
            dangerousGoods: null,
            signatureRequired: null,
            insuranceType: null,
            codPaymentType: null
        };
        $scope.initQuantity = function () {
            var range = [];
            for(var i=1;i< 50;i++) {
                range.push(i);
            }
            $scope.quantities = range;
        };
        $scope.setService = function (index) {
            $scope.isUseCoupon = false;
            $scope.quote = $scope.quotes[index];
            $scope.chooseCarrer = true;

            $scope.updateFee($scope.quote);
        };

        $scope.addItem = function (){
            $scope.item.subPrice = $scope.item.unitPrice * $scope.item.quantity;
            $scope.custom.total +=  $scope.item.subPrice;
            $scope.getCodeCountry();
            $scope.custom.items.push($scope.item);
            $scope.item = {};
        };

        $scope.removeItem = function(index){
            $scope.custom.total -= $scope.custom.items[index].subPrice;
            $scope.custom.items.splice(index, 1);
        };

        $scope.getCodeCountry = function (){
            angular.forEach($scope.countries, function(value, key){
                if(value.id == $scope.item.country_id){
                    $scope.item.country_code = value.code;
                    return;
                }
            });
        };

        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url   : '/api/countries'
                }),
                $http({
                    method: 'get',
                    url   : '/api/dim-types'
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.addressFrom.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' +
                    $scope.addressFrom.province_id
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.addressTo.country_id
                })
            ])
            .then(function (resp) {
                $scope.countries = resp[0].data.countries;
                $scope.dimTypes = resp[1].data.dimTypes;
                $scope.provincesFrom = resp[2].data.provinces;
                $scope.citiesFrom = resp[3].data.cities;
                $scope.provincesTo = resp[4].data.provinces;
                $scope.addressSearch.customer_id = $scope.addressFrom.id;
                $scope.searchAddress();
                $scope.pickup.location = "Front Door";
                $scope.pickup.start_hour_time = "07";
                $scope.pickup.start_minute_time = "00";
                $scope.pickup.closing_hour_time = "17";
                $scope.pickup.closing_minute_time = "30";
                $scope.checkInfoCustomer();
            });
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.addressSearch = {
            customer_id      : null
        };

        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url   : '/api/address?customer_id=' + $scope.addressSearch.customer_id
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.address;
                    if($scope.addressSearchResult.length > 0){
                        $scope.selectedId = $scope.addressSearchResult[0].id;
                        $scope.pickAddress($scope.addressSearchResult[0].id);
                    }
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                    $scope.submittedSearchAddress = false;
                });
        };
        $scope.selectAddress = function (address) {
            if (address == 0) {
                $scope.provincesTo = [];
                $scope.citiesTo = [];
                $scope.addressTo = {
                    first_name : null,
                    middle_name : null,
                    last_name : null,
                    address_1  : null,
                    address_2       : null,
                    country_id : 47,
                    province_id: null,
                    city_id: null,
                    postal_code  : null,
                    telephone  : null,
                    email  : null,
                    attention  : null,
                    instruction  : null,
                    notify_recipient  : false,
                    residential  : false
                };
            }
            if (address) {
                $scope.addressTo = address;
            }
            $scope.getProvincesTo();

        };

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function(address_id){
            var address = 0;
            if($scope.addressSearchResult.length > 0 && address_id >0){
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.getProvincesFrom = function () {
            $http({
                method: 'get',
                url   : '/api/provinces?country_id=' + $scope.addressFrom.country_id
            }).then(
                function (resp) {
                    $scope.provincesFrom = resp.data.provinces;
                    $scope.getCitiesFrom();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesFrom = function () {
            $http({
                method: 'get',
                url   : '/api/cities?country_id=' + $scope.addressFrom.country_id+ '&province_id=' + $scope.addressFrom.province_id
            }).then(
                function (resp) {
                    $scope.citiesFrom = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeFrom = function () {
            angular.forEach($scope.citiesFrom, function(value, key){
                if(value.id == $scope.addressFrom.city_id){
                    $scope.addressFrom.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeTo = function () {
            angular.forEach($scope.citiesTo, function(value, key){
                if(value.id == $scope.addressTo.city_id){
                    $scope.addressTo.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesTo = function () {
            if($scope.addressTo.country_id != $scope.sourceCounty){
                $scope.is_custom = 2;
            }else{
                $scope.is_custom = 1;
            }
            $http({
                method: 'get',
                url   : '/api/provinces?country_id=' + $scope.addressTo.country_id
            }).then(
                function (resp) {
                    $scope.provincesTo = resp.data.provinces;
                    $scope.getCitiesTo();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesTo = function () {
            $http({
                method: 'get',
                url   : '/api/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                $scope.addressTo.province_id
            }).then(
                function (resp) {
                    $scope.citiesTo = resp.data.cities;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getProvincesCustom = function () {
            $http({
                method: 'get',
                url   : '/api/provinces?country_id=' + $scope.custom.billTo.country_id
            }).then(
                function (resp) {
                    $scope.provincesCustom = resp.data.provinces;
                    $scope.getCitiesCustom();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustom = function () {
            $http({
                method: 'get',
                url   : '/api/cities?country_id=' + $scope.custom.billTo.country_id + '&province_id=' +
                $scope.custom.billTo.province_id
            }).then(
                function (resp) {
                    $scope.citiesCustom = resp.data.cities;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustom = function () {
            angular.forEach($scope.citiesCustom, function(value, key){
                if(value.id == $scope.custom.billTo.city_id){
                    $scope.custom.billTo.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.stringToDate = function (_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate;
        }

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function (cardDate) {
            if (!cardDate) {
                return false;
            }
            var cardTime = $scope.stringToDate(cardDate, 'dd/MM/yyyy', "/").getTime();
            var currentTime = new Date().getTime();
            if (cardTime < currentTime) {
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function () {
            $scope.isEditDiscount = true;
            if (!$scope.addressFrom.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.isAvailableIdCard($scope.addressFrom.card_expire)) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.image_1_file_id) {
                $scope.isEditDiscount = false;
            }
        };

        $scope.showPopupVip = function(){
            popup.confirm(language.notify_vip, function () {
                window.location = $scope.updateProfileLink;
            });
        };

        $scope.updateFee = function (quote) {
            var per_discount = parseFloat(quote.per_discount) > 0 ? parseFloat(quote.per_discount) : 0;
            $scope.quote.discount=quote.per_discount;
            $scope.quote.total_discount = (parseFloat(quote.baseCharge) * per_discount) / 100;
            $scope.quote.total = parseFloat(quote.totalCharge) - parseFloat($scope.quote.total_discount);
        };

        $scope.disable_field = false;
        $scope.show_field = false;
        $scope.changePackageType = function () {
            if ($scope.packages.type == 1 || $scope.packages.type == 2) {
                $scope.packages.quantity = 1;
                $scope.changeQuantity();
                $scope.addressFrom.residential = false;
                $scope.disable_field = true;
            } else if ($scope.packages.type == 4) {
                $scope.disable_field = false;
                $scope.show_field = true;
                $scope.product.freightClass = 50;
            } else {
                $scope.disable_field = false;
                $scope.show_field = false;
            }
        };
        $scope.changeDimType = function () {
            var dim = 'in';
            var weight = 'lbs';
            var productHead = angular.element('#productHead');
            if ($scope.packages.dimType == 1) {
                dim = 'cm';
                weight = 'kg';
            }
            productHead.find('#lengthHeader').text('Length ('+dim+')');
            productHead.find('#widthHeader').text('Width ('+dim+')');
            productHead.find('#heightHeader').text('Height ('+dim+')');
            productHead.find('#weightHeader').text('Weight ('+weight+')');
        };
        $scope.changeQuantity = function () {
            var currentProduct = $scope.packages.boxes.length;
            if ($scope.packages.quantity > currentProduct) {
                for (var i = 0 ; i < $scope.packages.quantity - currentProduct; i++) {
                    var product = {
                        id:null,
                        length: 1,
                        width: 1,
                        height: 1,
                        volume: parseFloat (1 / body_weight),
                        weight: 1,
                        insuranceAmount: 0.0,
                        freightClass: null,
                        nmfcCode: null,
                        codValue:0.0,
                        per_discount: 0,
                        description: null
                    };
                    $scope.packages.boxes.push(product);
                }
            } else {
                for (var i = 0 ; i < currentProduct - $scope.packages.quantity; i++) {
                    $scope.packages.boxes.pop();
                }
            }

        };

        $scope.updateVolume = function (container) {
            if (!isNaN(container.length) && container.length > 0
                    && !isNaN(container.width) && container.width > 0
                    && !isNaN(container.height) && container.height > 0) {
                container.volume = (parseFloat(container.length) * parseFloat(container.width) *
                        parseFloat(container.height)) / body_weight;
            } else {
                container.volume = 0;
            }
        };

        $scope.setagree = function (check) {
            if (check) {
                $scope.showCreate = true;
            }
            else {
                $scope.showCreate = false;
            }
        };

        $scope.applyCoupon = function () {
            var total_weight = 0;
            var boxes = angular.copy($scope.packages.boxes);

            angular.forEach(boxes, function (product) {
                if (product.volume > product.weight) {
                    total_weight += product.volume;
                }else{
                    total_weight += product.weight;
                }
            });
            var data = {
                customer: $scope.addressFrom,
                container: {
                    coupon_code: $scope.quote.coupon_code,
                    total_weight: total_weight
                }
            };
            $http({
                method: 'POST',
                url: '/api/coupon',
                data: data
            }).then(
                function (resp) {
                    var amount = parseFloat(resp.data.amount);
                    var message= resp.data.amount.message;
                    $scope.errors.Coupon.length=0;
                    if(amount > 0){
                        $scope.isUseCoupon = true;
                        $scope.quote.coupon_amount = amount;
                        $scope.quote.oldBaseCharge = $scope.quote.baseCharge;
                        $scope.quote.oldTotalCharge = $scope.quote.totalCharge;
                        if($scope.quote.baseCharge < amount){
                            $scope.quote.baseCharge = 0;
                            $scope.quote.totalCharge -= $scope.quote.baseCharge;
                        }else{
                            $scope.quote.baseCharge -= amount;
                            $scope.quote.totalCharge -= amount;
                        }
                        $scope.updateFee($scope.quote);
                    }else{
                        $scope.quote.coupon_code = '';
                        if(message!=='' )
                        {
                         $scope.errors.Coupon.push(message);
                     }
                    }
                }
            );
        };

        $scope.removeCoupon = function () {
            $scope.isUseCoupon = false;
            $scope.quote.coupon_code = '';
            $scope.quote.baseCharge = $scope.quote.oldBaseCharge;
            $scope.quote.totalCharge = $scope.quote.oldTotalCharge;
            $scope.quote.coupon_amount = 0;
            $scope.updateFee($scope.quote);
        };

        $scope.scanSearchExpress = function () {
            $scope.isUseCoupon = false;
            var flag = false;
            var packages = angular.copy($scope.packages.boxes);

            angular.forEach(packages, function (product) {
                if (product.volume > product.weight) {
                    flag = true;
                }
            });
            if(flag){
                popup.confirmChange(language.weight_volume, function () {
                    $scope.searchExpress();
                });
            }else{
                $scope.searchExpress();
            }
        };

        $scope.searchExpress = function () {
            $scope.submittedSearch = true;
            var packages = angular.copy($scope.packages);
            angular.forEach(packages.boxes, function (product) {
                if (product.volume > product.weight) {
                    product.weight = product.volume;
                }
            });
            var data = {
                addressFrom            : $scope.addressFrom,
                addressTo            : $scope.addressTo,
                pickup              : $scope.pickup,
                custom              : $scope.custom,
                packages          : packages,
                services            : $scope.services,
                is_custom : $scope.is_custom
            };
            $http({
                method: 'POST',
                url   : '/express/get-quote',
                data  : data
            }).then(
                function (resp) {
                     $scope.initErrors();
                    if(resp.data.success === true) {
                        $scope.submitted = false;
                        $scope.quotes = resp.data.data;
                    }else {
                        popup.confirmCustom(language.eshipper_support, function () {
                            $scope.createExpress();
                        });
                    }
                },
                function (resp) {
                    popup.confirmCustom(language.eshipper_support, function () {
                        $scope.createExpress();
                    });
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('quote') !== -1) {
                                $scope.errors.quotes.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes : [],
                                        orther : []
                                    };
                                }
                                if(field.indexOf('packages.boxes') !== -1 ) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                }else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        console.log($scope.errors);
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submittedSearch = false;
            });
        };
        $scope.clearExpress = function () {
          $scope.addressTo = [];
          $scope.addressFrom = [];
          $scope.initPackages();
          $scope.services = [];
          $scope.quotes = [];
        };
        $scope.createExpress = function () {
            var data = {
                addressFrom            : $scope.addressFrom,
                addressTo            : $scope.addressTo,
                pickup              : $scope.pickup,
                custom              : $scope.custom,
                packages          : $scope.packages,
                services            : $scope.services,
                quotes               : $scope.quotes,
                quote               : $scope.quote,
                is_custom : $scope.is_custom
            };

            $scope.submittedConfirm = true;
            $http({
                method: 'POST',
                url   : '/express/store',
                data  : data
            }).then(
                function (resp) {
                    location.href = '/express/list';
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('quote') !== -1) {
                                $scope.errors.quotes.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes : [],
                                        orther : []
                                    };
                                }
                                if(field.indexOf('packages.boxes') !== -1 ) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                }else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submittedConfirm = false;
            });
        };
    });
}());
