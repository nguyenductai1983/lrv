(function () {
    'use strict';

    var app = angular.module('ExpressApp', []);

    app.controller('ExpressEditController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.initErrors = function () {
            $scope.errors = {
                addressFrom: [],
                addressTo: [],
                packages: [],
                quotes: []
            };
        };
        $scope.serviceIndex = null;
        $scope.showCreate = false;
        $scope.initErrors();
        $scope.countries = [];
        $scope.provincesFrom = [];
        $scope.provincesTo = [];
        $scope.citiesFrom = [];
        $scope.citiesTo = [];
        $scope.dimTypes = [];
        $scope.pickup = [];
        $scope.quote = [];
        $scope.order_info = [];
        $scope.packageTypes = [
            {
                'id': 1,
                'name': 'Envelope'
            }, {
                'id': 2,
                'name': 'Pak'
            }, {
                'id': 3,
                'name': 'Package'
            }, {
                'id': 5,
                'name': 'SmarteHome'
            }
        ];
        $scope.hourTimes = [
            {
                'id': '07',
                'name': '07'
            }, {
                'id': '08',
                'name': '08'
            }, {
                'id': '09',
                'name': '09'
            }, {
                'id': '10',
                'name': '10'
            }, {
                'id': '11',
                'name': '11'
            }, {
                'id': '12',
                'name': '12'
            }, {
                'id': '13',
                'name': '13'
            }, {
                'id': '14',
                'name': '14'
            }, {
                'id': '15',
                'name': '15'
            }, {
                'id': '16',
                'name': '16'
            }, {
                'id': '17',
                'name': '17'
            }
        ];

        $scope.minuteTimes = [
            {
                'id': '00',
                'name': '00'
            }, {
                'id': '15',
                'name': '15'
            }, {
                'id': '30',
                'name': '30'
            }, {
                'id': '45',
                'name': '45'
            }
        ];

        $scope.pickupLocation = [
            {
                'id': 'Receiving',
                'name': 'Receiving'
            }, {
                'id': 'Garage',
                'name': 'Garage'
            }, {
                'id': 'Lobby',
                'name': 'Lobby'
            }, {
                'id': 'Reception',
                'name': 'Reception'
            }, {
                'id': 'Front Desk',
                'name': 'Front Desk'
            }, {
                'id': 'Vault',
                'name': 'Vault'
            }, {
                'id': 'Switchboard',
                'name': 'Switchboard'
            }, {
                'id': 'Back Door',
                'name': 'Back Door'
            }, {
                'id': 'Desk',
                'name': 'Desk'
            }, {
                'id': 'Between Doors',
                'name': 'Between Doors'
            }, {
                'id': 'Kiosk',
                'name': 'Kiosk'
            }, {
                'id': 'Office',
                'name': 'Office'
            }, {
                'id': 'Outside Door',
                'name': 'Outside Door'
            }, {
                'id': 'Mailbox',
                'name': 'Mailbox'
            }, {
                'id': 'Side Door',
                'name': 'Side Door'
            }, {
                'id': 'Service Counter',
                'name': 'Service Counter'
            }, {
                'id': 'Security',
                'name': 'Security'
            }, {
                'id': 'Shipping',
                'name': 'Shipping'
            }, {
                'id': 'Front Door',
                'name': 'Front Door'
            }, {
                'id': 'Basement',
                'name': 'Basement'
            }, {
                'id': 'Mail Room',
                'name': 'Mail Room'
            }, {
                'id': 'Lab',
                'name': 'Lab'
            }, {
                'id': 'Warehouse',
                'name': 'Warehouse'
            }, {
                'id': 'Pharmacy',
                'name': 'Pharmacy'
            }, {
                'id': 'Pro Shop',
                'name': 'Pro Shop'
            }, {
                'id': 'Parts Department',
                'name': 'Parts Department'
            }, {
                'id': 'Counter',
                'name': 'Counter'
            }, {
                'id': 'Loading Dock',
                'name': 'Loading Dock'
            }, {
                'id': 'Gate House',
                'name': 'Gate House'
            }
        ];

        $scope.changeSchedulePickup = function () {
            setTimeout(function () {
                $("#pickup-date").datepicker({dateFormat: "dd/mm/yy"});
            });
        };
        $scope.product = [];
        $scope.addressFromResult = [];
        $scope.addressToResult = [];
        $scope.selectedFromId = null;
        $scope.selectedToId = null;
        $scope.quotes = [];
        $scope.initAddressFrom = function () {
            $scope.addressFrom = {
                id: null,
                company: null,
                address_1: null,
                address_2: null,
                country_id: null,
                city_id: null,
                province_id: null,
                postal_code: null,
                telephone: null,
                email: null,
                attention: null,
                instruction: null,
                shipping_date: null,
                confirm_delivery: false,
                residential: false
            };
        };
        $scope.initAddressTo = function () {
            $scope.addressTo = {
                id: null,
                company: null,
                address_1: null,
                address_2: null,
                country_id: null,
                province_id: null,
                city_id: null,
                postal_code: null,
                telephone: null,
                email: null,
                attention: null,
                instruction: null,
                notify_recipient: false,
                residential: false
            };
        };
        $scope.initPackages = function () {
            var product = {
                id: null,
                length: 1,
                width: 1,
                height: 1,
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue: 0.0,
                description: null
            };
            $scope.packages = {
                type: 1,
                quantity: 1,
                referenceCode: null,
                dimType: 3,
                boxes: []
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };
        $scope.initPackages();
        $scope.services = {
            saturdayDelivery: null,
            saturdayPickup: null,
            holdForPickup: null,
            docsOnly: null,
            dangerousGoods: null,
            signatureRequired: null,
            insuranceType: null,
            codPaymentType: null
        };
        $scope.initQuantity = function () {
            var range = [];
            for (var i = 1; i < 50; i++) {
                range.push(i);
            }
            $scope.quantities = range;
        };
        $scope.setService = function (index) {
            $scope.quote = $scope.quotes[index];
            $scope.showCreate = true;
        };
        $scope.getExpress = function () {
            $http({
                method: 'get',
                url: '/express/' + $scope.id
            }).then(
                    function (resp) {
                        $scope.order_info = resp.data.express.order_info;
                        $scope.packages = resp.data.express.packages;
                        $scope.addressFrom = resp.data.express.addressFrom;
                        $scope.addressTo = resp.data.express.addressTo;
                        $scope.quote = resp.data.express.quote;
                        $scope.quote.total_discount = resp.data.express.order_info.discount_number;
                        $scope.quote.per_discount = resp.data.express.quote.discount;
                        $scope.quote.total = resp.data.express.order_info.total_pay;
                        $scope.selectQuote = resp.data.express.quote;
                        $scope.services = resp.data.express.services;
                        $scope.quotes = resp.data.express.quotes;
                        $scope.pickup = resp.data.express.pickup;
                        $scope.custom = resp.data.express.custom;
                        $scope.is_custom = resp.data.express.is_custom;
                        if($scope.is_custom == 2){
                            $scope.getProvincesCustom();
                            $scope.getCitiesCustom();
                        }
                        $scope.init();
                        $scope.changePackageType();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            ).finally(function () {
                $scope.searchAddress();
            });
        };
        $scope.initAddressTo();
        $scope.initAddressFrom();

        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.addressFrom.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.addressTo.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                            $scope.addressTo.province_id
                }), $http({
                    method: 'get',
                    url: '/api/dim-types'
                })
            ])
                    .then(function (resp) {
                        $scope.countries = resp[0].data.countries;
                        $scope.provincesFrom = resp[1].data.provinces;
                        $scope.citiesFrom = resp[2].data.cities;
                        $scope.provincesTo = resp[3].data.provinces;
                        $scope.citiesTo = resp[4].data.cities;
                        $scope.dimTypes = resp[5].data.dimTypes;
                    });
        };
        $scope.searchAddress = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/address?customer_id=' + $scope.customer_id + '&type=1'
                }),
                $http({
                    method: 'get',
                    url: '/api/address?customer_id=' + $scope.customer_id + '&type=2'
                })
            ])
                    .then(function (resp) {
                        $scope.addressFromResult = resp[0].data.address;
                        $scope.addressToResult = resp[1].data.address;
                        if ($scope.addressFromResult.length > 0) {
                            $scope.selectFromAddress($scope.addressFrom);
                            $scope.getCitiesFrom();
                        }
                        if ($scope.addressToResult.length > 0) {
                            $scope.selectToAddress($scope.addressTo);
                            $scope.getCitiesTo();
                        }
                    });
        };
        $scope.pickAddressFrom = function (selectedFromId) {
            var address = 0;
            if ($scope.addressFromResult.length > 0 && selectedFromId > 0) {
                address = search(selectedFromId, $scope.addressFromResult);
            }
            $scope.selectFromAddress(address);
        };
        $scope.pickAddressTo = function (selectedToId) {
            var address = 0;
            if ($scope.addressToResult.length > 0 && selectedToId > 0) {
                address = search(selectedToId, $scope.addressToResult);
            }
            $scope.selectToAddress(address);
        };
        $scope.selectFromAddress = function (address) {
            if (address) {
                $scope.addressFrom = address;
                $scope.selectedFromId = address.id;
                $scope.disable_from = true;
                $scope.getProvincesFrom();
            }
            if (address == 0) {
                $scope.disable_from = false;
                $scope.initAddressFrom();
            }


        };
        $scope.selectToAddress = function (address) {
            if (address) {
                $scope.addressTo = address;
                $scope.selectedToId = address.id;
                $scope.disable_to = true;
                $scope.getProvincesTo();
            }
            if (address == 0) {
                $scope.disable_to = false;
                $scope.initAddressTo();
            }
        };
        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }
        
        $scope.getProvincesFrom = function () {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.addressFrom.country_id
            }).then(
                    function (resp) {
                        $scope.provincesFrom = resp.data.provinces;
                        $scope.getCitiesFrom();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };

        $scope.getCitiesFrom = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
            }).then(
                    function (resp) {
                        $scope.citiesFrom = resp.data.cities;
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };
        $scope.getProvincesTo = function () {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.addressTo.country_id
            }).then(
                    function (resp) {
                        $scope.provincesTo = resp.data.provinces;
                        $scope.getCitiesTo();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };


        $scope.getCitiesTo = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                        $scope.addressTo.province_id
            }).then(
                    function (resp) {
                        $scope.citiesTo = resp.data.cities;
                    },
                    function (resp) {
                        console.log(resp);
                        console.log(resp.statusText);
                    }
            );
        };
        $scope.getProvincesCustom = function () {
            $http({
                method: 'get',
                url   : '/api/provinces?country_id=' + $scope.custom.billTo.country_id
            }).then(
                function (resp) {
                    $scope.provincesCustom = resp.data.provinces;
                    $scope.getCitiesCustom();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustom = function () {
            $http({
                method: 'get',
                url   : '/api/cities?country_id=' + $scope.custom.billTo.country_id + '&province_id=' +
                $scope.custom.billTo.province_id
            }).then(
                function (resp) {
                    $scope.citiesCustom = resp.data.cities;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };
        
        $scope.getPostCodeCustom = function () {
            angular.forEach($scope.citiesCustom, function(value, key){
                if(value.id == $scope.custom.billTo.city_id){
                    $scope.custom.billTo.postal_code = value.postal_code;
                    return;
                }
            });
        };
        $scope.disable_field = false;
        $scope.show_field = false;
        $scope.changePackageType = function () {
            if ($scope.packages.type == 1 || $scope.packages.type == 2) {
                $scope.packages.quantity = 1;
                $scope.changeQuantity();
                $scope.addressFrom.residential = false;
                $scope.disable_field = true;
            } else if ($scope.packages.type == 4) {
                $scope.disable_field = false;
                $scope.show_field = true;
            } else {
                $scope.disable_field = false;
                $scope.show_field = false;
            }
        };
        $scope.changeDimType = function () {
            var dim = 'in';
            var weight = 'lbs';
            var productHead = angular.element('#productHead');
            if ($scope.packages.dimType == 1) {
                dim = 'cm';
                weight = 'kg';
            }
            productHead.find('#lengthHeader').text('Length (' + dim + ')');
            productHead.find('#widthHeader').text('Width (' + dim + ')');
            productHead.find('#heightHeader').text('Height (' + dim + ')');
            productHead.find('#weightHeader').text('Weight (' + weight + ')');
        };
        $scope.changeQuantity = function () {
            var currentProduct = $scope.packages.boxes.length;
            if ($scope.packages.quantity > currentProduct) {
                for (var i = 0; i < $scope.packages.quantity - currentProduct; i++) {
                    var product = {
                        id: null,
                        length: 1,
                        width: 1,
                        height: 1,
                        weight: 1,
                        insuranceAmount: 0.0,
                        freightClass: null,
                        nmfcCode: null,
                        codValue: 0.0,
                        description: null
                    };
                    $scope.packages.boxes.push(product);
                }
            } else {
                for (var i = 0; i < currentProduct - $scope.packages.quantity; i++) {
                    $scope.packages.boxes.pop();
                }
            }

        };
        $scope.searchExpress = function () {
            $scope.submitted = true;
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                packages: $scope.packages,
                services: $scope.services
            };
            $http({
                method: 'POST',
                url: '/express/get-quote',
                data: data
            }).then(
                    function (resp) {
                        if (resp.data.success === true) {
                            $scope.submitted = false;
                            $scope.quotes = resp.data.data;
                        } else {
                            $scope.errors.quotes.push(resp.data.message);
                            angular.element('#modal-errors').modal('show');
                        }
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();
                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('addressFrom') !== -1) {
                                    $scope.errors.addressFrom.push(error[0]);
                                } else if (field.indexOf('addressTo') !== -1) {
                                    $scope.errors.addressTo.push(error[0]);
                                } else if (field.indexOf('quote') !== -1) {
                                    $scope.errors.quotes.push(error[0]);
                                } else if (field.indexOf('packages') !== -1) {
                                    if (typeof $scope.errors.packages.boxes === 'undefined') {
                                        $scope.errors.packages = {
                                            boxes: [],
                                            orther: []
                                        };
                                    }
                                    if (field.indexOf('packages.boxes') !== -1) {
                                        for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                            if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                                $scope.errors.packages.boxes[i] = [];
                                                var errorPushed = false;
                                            }
                                            if (field.indexOf('packages.boxes.' + i) === -1) {
                                                continue;
                                            }
                                            $scope.errors.packages.boxes[i].push(error[0]);
                                            errorPushed = true;
                                        }
                                    } else {
                                        $scope.errors.packages.orther.push(error[0]);
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                        }
                        if (resp.status === 500) {
                            $scope.errors.system = [];
                            $scope.errors.system.push(resp.data.message);
                            angular.element('#modal-errors').modal('show');
                        }
                    }
            ).finally(function () {
                console.log($scope.errors.packages.length);
                $scope.submitted = false;
            });
        };
        $scope.clearExpress = function () {
            $scope.addressTo = [];
            $scope.addressFrom = [];
            $scope.initPackages();
            $scope.services = [];
            $scope.quotes = [];
        };
        $scope.updateEpress = function () {
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                packages: $scope.packages,
                services: $scope.services,
                quote: $scope.quote,
                order_info: $scope.order_info
            };
            $scope.submitted = true;
            $http({
                method: 'POST',
                url: '/express/update',
                data: data
            }).then(
                    function (resp) {
                        location.href = '/express/list';
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();
                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('addressFrom') !== -1) {
                                    $scope.errors.addressFrom.push(error[0]);
                                } else if (field.indexOf('addressTo') !== -1) {
                                    $scope.errors.addressTo.push(error[0]);
                                } else if (field.indexOf('quote') !== -1) {
                                    $scope.errors.quotes.push(error[0]);
                                } else if (field.indexOf('packages') !== -1) {
                                    if (typeof $scope.errors.packages.boxes === 'undefined') {
                                        $scope.errors.packages = {
                                            boxes: [],
                                            orther: []
                                        }
                                    }
                                    if (field.indexOf('packages.boxes') !== -1) {
                                        for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                            if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                                $scope.errors.packages.boxes[i] = [];
                                                var errorPushed = false;
                                            }
                                            if (field.indexOf('packages.boxes.' + i) === -1) {
                                                continue;
                                            }
                                            $scope.errors.packages.boxes[i].push(error[0]);
                                            errorPushed = true;
                                        }
                                    } else {
                                        $scope.errors.packages.orther.push(error[0]);
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                        } else {
                            console.log(resp.statusText);
                        }
                        if (resp.status === 500) {
                            $scope.errors.system = [];
                            $scope.errors.system.push(resp.data.message);
                            angular.element('#modal-errors').modal('show');
                        }
                    }
            ).finally(function () {
                $scope.submitted = false;
            });
        };
    });
}());