var chat = window.iGreenLink;

pubnub.addListener({
    message: function(content) {
        var to_user_id = typeof content.message.to_user_id !== undefined && content.message.to_user_id > 0 ? content.message.to_user_id : 0;
        if(to_user_id === 0 || to_user_id != chat.user_id){
            return;
        }
        var path = typeof content.message.path !== undefined ? content.message.path : '';
        var name = typeof content.message.name !== undefined ? content.message.name : '';
        var html = "";
        html += '<li>';
        html += '<ul class="dropdown-menu-list list-unstyled ps-scrollbar ps-container">'
        html += '<li class="notification-success">'
        html += '<a href="'+ path +'">';
        html += '<span class="line">'
        html += name
        html += '</span>'
        html += '</a>'
        html += '</li>';

        var notifi_qty = parseInt($('#notifi_qty').text());
        if(notifi_qty > 0){
            $('#notifi_qty').text(notifi_qty + 1);
            $('#notification_content').prepend(html);
        }else{
            $('#notifi_qty').text(1);
            $('#notification_content').html(html);
        }
    }
});

pubnub.subscribe({
    channels: ['notification'],
    callback: function(m) {
         console.log(m);
    }
});
