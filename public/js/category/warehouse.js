var warehouse = {};

warehouse.init = function () {
    setTimeout(function () {
        warehouse.getInfo();
    }, 100);
    
    $('select[name=country_id]').change(function(){
        warehouse.loadProvince();
        warehouse.loadDistrict();
    });
    
    $('select[name=province_id]').change(function(){
        warehouse.loadDistrict();
    });
};

warehouse.getInfo = function () {
    warehouse.loadCountry();
    warehouse.loadProvince();
    warehouse.loadDistrict();
};

warehouse.loadCountry = function () {
    var html = '<option value="">Vui lòng chọn Quốc gia</option>';
    $.each(countries, function (key, value) {
        if (this.id == currentCountryId) {
            html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
        } else {
            html += '<option value="' + this.id + '">' + this.name + '</option>';
        }
    });
    $("select[name=country_id]").html(html);
};

warehouse.loadProvince = function () {
    var id = 0;
    id = $('select[name=country_id]').val();
    var html = '<option value="">Vui lòng chọn Tỉnh / Thành</option>';
    $.each(provinces, function (key, value) {
        if (this.country_id == id) {
            if (this.id == currentProviceId) {
                html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
            } else {
                html += '<option value="' + this.id + '">' + this.name + '</option>';
            }
        }
        
    });
    $("select[name=province_id]").html(html);
};

warehouse.loadDistrict = function () {
    var id = 0;
    id = $('select[name=province_id]').val();
    var html = '<option value="">Vui lòng chọn Quận / Huyện</option>';
    $.each(districts, function () {
        if (this.province_id == id) {
            if (this.id == currentDistrictId) {
                html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
            } else {
                html += '<option value="' + this.id + '">' + this.name + '</option>';
            }
        }
    });
    $("select[name=city_id]").html(html);
};