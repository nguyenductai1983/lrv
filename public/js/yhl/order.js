var order = {};
order.export = function () {    
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()       
    };
    var url = "/admin/yhls/export?" + $.param(query);
   window.location.href =url;
};
order.exportcustomer = function () {    
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()       
    };
    var url = "/admin/yhlcustomer/export?" + $.param(query);
   window.location.href =url;
};
order.cancel = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/yhl/tracking/cancel-order',
            method: "POST",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};