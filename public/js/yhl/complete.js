var complete = {};

complete.cancelRequest = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/complete/cancel-request',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};
complete.cancelOrder = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/yhl/complete/cancelorder',
            method: "POST",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};
complete.carrierPickup = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/complete/pickup',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};

complete.customerReceiver = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/complete/customer-receiver',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};
