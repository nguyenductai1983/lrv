var tracking = {};

tracking.viewHistory = function (id) {
    fly.ajax({
        service: '/admin/yhl/tracking/view-history',
        method: "POST",
        loading: true,
        data: {
            id: id
        },
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('yhl/view_history.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};
tracking.export = function () {
    var url = "/admin/yhl/tracking/export";
   window.location.href =url;
};
tracking.viewLog = function (id) {
    fly.ajax({
        service: '/admin/yhl/tracking/view-log',
        method: "POST",
        loading: true,
        data: {
            id: id
        },
        success: function (result) {
            if (result.success) {
                popup.open('view-log', language.view_history_log, template('yhl/view_log.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-log');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};
