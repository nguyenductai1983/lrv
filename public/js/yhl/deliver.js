var deliver = {};

deliver.cancelRequest = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/deliver/cancel-request',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};

deliver.approveRequest = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/deliver/approve-request',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};

deliver.carrierPickup = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/deliver/pickup',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};

deliver.customerReceiver = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/yhl/deliver/customer-receiver',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};