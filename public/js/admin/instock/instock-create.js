(function() {
    'use strict';
    var app = angular.module('InstockApp', []);
    app.controller('InstockController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.list_products = [];
        $scope.methods = [];
        $scope.initErrors = function() {
            $scope.errors = [];
        };
        $scope.initErrors();
        $scope.initProduct = function() {
            var list_product = [];
            for (var i = 1; i <= 4; ++i) {
                var product = {
                    product_id: null,
                    code: null,
                    name: null,
                    by_weight: null,
                    quantity: null,
                    weight: null,
                    price: null,
                    per_discount: null,
                    discount: null,
                    agency_unit_id: null,
                    agency_currency_id: null,
                    unit: null,
                    amount: null,
                    total: null,
                    note: null,
                    description: null,
                    product_list: $scope.products
                };
                list_product.push(product);
            }
            return list_product;
        };

        $scope.list_products = $scope.initProduct();

        $scope.init = function() {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/products?service=3&status=1&agency=' + $scope.agency
                })
            ]).then(function(resp) {
                $scope.products = resp[0].data.products;
                angular.forEach($scope.list_products, function(product) {
                    product.product_list = angular.copy($scope.products);
                });
            });
        };

        $scope.addproduct = function() {
            var product = {
                product_id: null,
                code: null,
                name: null,
                by_weight: null,
                quantity: null,
                weight: null,
                price: null,
                per_discount: null,
                discount: null,
                unit: null,
                amount: null,
                declared_value: null,
                surcharge: null,
                is_insurance: false,
                insurance: null,
                total: null,
                note: null,
                description: null,
                product_list: $scope.products
            };
            $scope.list_products.push(product);
        };

        $scope.searchProducts = function(product, idDropdown) {
            var code = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function(item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
            });
            var products = product.product_list.filter(function(item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function() {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function() {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function() {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function() {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function(product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.description = item.description;
            product.discount = 0;
            product.quantity = 1;

        };

        $scope.remove = function(p_index, index) {
            $scope.containers[p_index].products.splice(index, 1);
            var product = {
                product_id: null,
                code: null,
                name: null,
                by_weight: null,
                quantity: null,
                weight: null,
                price: null,
                per_discount: null,
                discount: null,
                agency_unit_id: null,
                agency_currency_id: null,
                unit: null,
                amount: null,
                total: null,
                note: null,
                description: null,
                product_list: $scope.products
            };
            $scope.containers[p_index].products.push(product);
            $scope.updateContainers();
        };
        $scope.createinstock = function() {
            angular.forEach($scope.list_products, function(product) {
                delete product.product_list;
            });
            $scope.list_products = $scope.list_products.filter(function(product) {
                return !!product.product_id && parseInt(product.quantity) !== 0;
            });

            var data = {
                products: $scope.list_products,
                warehouse_id: $scope.warehouse_id
            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url: '/admin/yhl/instock/store',
                data: data
            }).then(
                function(resp) {
                    $scope.submitted = false;
                    $scope.complete = true;
                    $scope.new_order = resp.data;
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('warehouse') !== -1) {
                                $scope.errors.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submitted = false;
            });
        };
    });
}());