(function () {
    'use strict';

    var app = angular.module('InstockApp', []);
    app.controller('InstockEditController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.complete=false;
        $scope.stocks;
        $scope.initErrors = function () {
            $scope.errors = {
                customer: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();
        $scope.initProduct = function () {
            var list_product=[];
                    for (var i = 1; i <= 4; ++i) {
                        var product = {
                            product_id    : null,
                            code          : null,
                            name          : null,
                            amount        : null,
                            product_list  : $scope.products
                        };
                        list_product.push(product);
                    }
                    return list_product;
                };
$scope.products = $scope.initProduct();
        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/products?service=3&status=1&agency=' + $scope.agency
                }),


            ]).then(function (resp) {
                $scope.products = resp[0].data.products;

                });

        };
             $scope.selectedId = 0;

                 $scope.addContainer = function () {
            $scope.containers.push($scope.initContainer());

            setTimeout(function () {
                setDatePicker();
            });

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function (index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function () {
                $scope.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function (product, idDropdown) {
            var code = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);

            angular.forEach($scope.products, function (item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
            });

            var products = $scope.products.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.sub_total_surcharge_fee = 0;
            product.sub_total_discount = 0;
            product.sub_total_insurrance_fee = 0;
            product.unit_goods_fee = parseFloat(item.sale_price) + parseFloat(item.pickup_fee);
            product.quantity = 1;
            product.sub_total_weight = item.weight;
            product.messure.code = item.unit;
            product.product = item;
            product.product_id = item.id;
            product.pickup_fee = item.pickup_fee;

            $scope.updateContainers();
        };

                function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        };


        $scope.update = function () {
            var data = {
                products            : $scope.list_products,
                warehouse_id :     $scope.warehouse_id
            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url   : '/admin/instock/update',
                data  : data
            }).then(
                function (resp) {
                    $scope.submitted=false;
                    $scope.complete=true;
                   $scope.new_order=resp.data;
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('warehouse') !== -1) {
                                $scope.errors.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });

        };
    });
}());
