/**
 * Get cookie
 *
 * @param name
 * @returns {*}
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

setInterval(function() {
    $.ajax({
        'url': '/admin/checkAuth',
        'success': function(response) {
            if (response == true) {

            }
        },
        error: function(jqXhr, textStatus, errorThrown) {
            if (errorThrown == 'Unauthorized') {
                location.reload();
            }
        }
    });
}, 1000 * 60 * 1); // will check every minute for Auth
/**
 * To url encoding
 *
 * @param data
 * @returns {string}
 */
function toUrlEncodedString(data) {
    var body = '';
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (body.length) {
                body += '&';
            }
            body += key + '=';
            body += encodeURIComponent(data[key]);
        }
    }
    return body;
}

var datePickerOptions = {
    dateFormat: 'dd/mm/yy',
    yearRange: '1970:' + ((new Date()).getFullYear() + 5),
    autoclose: true,
    orientation: 'auto',
    templates: {
        leftArrow: '<i class="fa fa-chevron-left"></i>',
        rightArrow: '<i class="fa fa-chevron-right"></i>'
    }
};

var setDatePicker = function() {
    $('.date-picker').each(function() {
        var format = $(this).data('format');
        if (format) {
            datePickerOptions.dateFormat = format;
        }
        var min = $(this).data('min');
        if (min) {
            datePickerOptions.minDate = min;
        }
        var max = $(this).data('max');
        if (max) {
            datePickerOptions.maxDate = max;
        }
        var changeYear = $(this).data('change-year');
        if (changeYear) {
            datePickerOptions.changeYear = true;
        }
        var changeMonth = $(this).data('change-month');
        if (changeMonth) {
            datePickerOptions.changeMonth = true;
        }
        var yearRange = $(this).data('year-range');
        if (yearRange) {
            datePickerOptions.yearRange = yearRange;
        }
        var defaultDate = $(this).data('default-date');
        if (defaultDate) {
            datePickerOptions.defaultDate = defaultDate;
        }

        if ($(this).hasClass('input-group')) {
            var input = $(this).find('input[type=text]');
            var widget = $(this).find('.input-group-btn');
            input.datepicker(datePickerOptions);
            widget.click(function() {
                input.focus();
            });
        } else {
            $(this).datepicker(datePickerOptions);
        }
    });
};

var setSelect2 = function() {
    $('select.select2').each(function(i, el) {
        var $this = $(el),
            opts = {
                placeholder: $(this).data('placeholder'),
                allowClear: attrDefault($this, 'allowClear', false),
                minimumResultsForSearch: $(this).data('results-search')
            };

        $this.select2(opts).on('select2-open', function() {
            $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
        });
        $this.addClass('visible');
    });
};

$(function() {
    setDatePicker();

    if ($.isFunction($.fn.select2)) {
        setSelect2();
    }

    /**
     * After form submit
     */
    $('form').submit(function() {
        var submit = $(this).find('button[type=submit], input[type=submit]');
        submit.each(function(key, btn) {
            if ($(btn).is(':focus') && $(btn).data('mode')) {
                $(this).append('<input type="hidden" name="mode" value="' + $(btn).data('mode') + '">');
            }
            if ($(btn).hasClass('disabled-submit')) {
                $(btn).prop('disabled', true);
            }
        });
        return true;
    });

    /**
     * Ajax setup
     */
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (settings.type === 'POST' || settings.type === 'PUT' || settings.type === 'DELETE') {
                xhr.setRequestHeader("X-XSRF-TOKEN", getCookie('XSRF-TOKEN'));
            }
        }
    });

    jQuery.validator.setDefaults({
        debug: false,
        errorElement: 'div',
        errorClass: 'has-error text-left',
        focusCleanup: true,
        showErrors: function(errorMap, errorList) {
            $.each(errorList, function(key, obj) {
                if (typeof obj.message === 'object' && obj.message.length > 1) {
                    errorList[key].message = [obj.message[0]];
                }
            });
            this.defaultShowErrors();
        },
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent('.radio-inline').length) {
                element.parent().parent().append(error);
            } else {
                error.insertAfter(element);
            }
        }
    });


    /**
     * Set config
     */
    var setConfig = function(config) {
        $.ajax({
            method: 'POST',
            url: '/admin/config',
            contentType: 'application/json',
            data: JSON.stringify(config)
        }).done(function(data, textStatus, jqXHR) {}).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        });
    };

    /**
     * Toggle sidebar click
     */
    $('a[data-toggle="sidebar"]').click(function() {
        var config = {
            menuCollapsed: $('.sidebar-menu').hasClass('collapsed')
        };
        setConfig(config);
    });

    /**
     * Image control
     */
    $('.img-control input[type=file]').fileupload({
        maxNumberOfFiles: 1,
        url: '/files/upload?type=image',
        method: 'post',
        dataType: 'json',
        formData: {},
        add: function(e, data) {
            $(this).prop('disabled', true)
                .parents('.form-group')
                .eq(0)
                .removeClass('validate-has-error')
                .find('.form-control+span').remove();
            $(this).parent().find('div.has-error').remove();
            $('<i class="fa fa-refresh fa-spin"></i>').insertAfter($(this));
            data.submit();
        },
        done: function(e, data) {
            var file = data.result,
                parent = $(this).parent(),
                imgPreview = parent.find('.img-preview'),
                scope = $(this).scope();

            imgPreview.find('img').attr('src', file.path);
            imgPreview.show();
            parent.find('input[type=hidden][class*=name]').val(file.id);
            parent.find('input[type=hidden][class*=path]').val(file.path);

            if (scope) {
                var attrName = parent.find('input[type=hidden][class*=name]').attr('name'),
                    names = attrName.split('.');

                if (names.length === 1) {
                    scope[names[0]] = file.id;
                } else if (names.length === 2) {
                    scope[names[0]][names[1]] = file.id;
                }

                if (!scope.$$phase) {
                    scope.$apply();
                }
            }
        },
        fail: function(e, data) {
            if (data.jqXHR.status === 422) {
                $(this).parents('.form-group').eq(0).addClass('validate-has-error');
                $('<span>' + data.jqXHR.responseJSON.errors.file[0] + '</span>').insertAfter($(this));
            } else {
                console.log(data);
            }
        },
        always: function(e, data) {
            $(this).prop('disabled', false).parent().find('.fa-spin').remove();
        }
    });

    $('.img-control .remove').click(function() {
        var parent = $(this).parent(),
            scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');
        parent.parent().find('input[type=hidden]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });

    $('.img-control1 input[type=file]').change(function() {
        if (this.files && this.files[0]) {
            $('.img-control1 .img-preview').css('display', 'block');
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img-preview-content').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.img-control1 .remove').click(function() {
        var parent = $(this).parent(),
            scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });

    $('.img-control-vi input[type=file]').change(function() {
        if (this.files && this.files[0]) {
            $('.img-control-vi .img-preview').css('display', 'block');
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img-preview-content-vi').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.img-control-vi .remove').click(function() {
        var parent = $(this).parent(),
            scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });

    $('.img-control-en input[type=file]').change(function() {
        if (this.files && this.files[0]) {
            $('.img-control-en .img-preview').css('display', 'block');
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img-preview-content-en').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.img-control-en .remove').click(function() {
        var parent = $(this).parent(),
            scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });
});