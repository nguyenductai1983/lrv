(function() {
    'use strict';

    var app = angular.module('MtsApp', []);

    app.controller('MtsEditController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.bntCancel = false;
        $scope.submittedCancel = false;
        $scope.bntConfirm = false;
        $scope.submittedConfirm = false;
        $scope.bntPayment = false;
        $scope.submittedPayment = false;
        $scope.mts = null;
        $scope.initErrors = function() {
            $scope.errors = {
                customer: [],
                receiver: []
            };
        };
        $scope.initErrors();
        $scope.currencies = [];
        $scope.countries = [];
        $scope.mts_code = '';
        $scope.selectedId = 0;
        $scope.addressSearchResult = [];
        $scope.addressSearch = {
            customer_id: null
        };

        $scope.initAddress = function() {
            $scope.provincesAddress = [];
            $scope.citiesAddress = [];
            $scope.address = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                phone_number: null,
                postal_code: null,
                city_id: null,
                province_id: null,
                country_id: null
            };
        };

        $scope.initContainer = function() {
            $scope.container = {
                id: null,
                destination: null,
                pay_date: $scope.date_now,
                status: 1,
                transaction_type: 1,
                payment_by: 1,
                amount: 0,
                discount_type: 1,
                discount_number: 0,
                transfer_fee: 0,
                total: 0,
                reason: '',
            };
        };

        $scope.initAddress();
        $scope.initContainer();

        $scope.$watchCollection('customer.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;

            if (!$scope.customer.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.card_expire) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.image_1_file_id) {
                $scope.isEditDiscount = false;
            }

        };

        $scope.getMts = function() {
            $http({
                method: 'get',
                url: '/admin/mts/quote/' + $scope.id
            }).then(
                function(resp) {
                    $scope.mts = resp.data.mts;

                    $scope.customer = $scope.mts.customer;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="customer.image_' + i +
                            '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if ($scope.customer['image' + i]) {
                            img = $scope.customer['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        } else {
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.receiver = $scope.mts.receiver;
                    $scope.selectedId = $scope.mts.receiver.id;
                    $scope.mts_code = $scope.mts.code;
                    $scope.addressSearch.customer_id = $scope.customer.id;
                    $scope.searchAddress();
                    $scope.init();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };


        $scope.init = function() {
            $q.all([
                $http({
                    method: 'get',
                    url: '/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/admin/provinces?country_id=' + $scope.customer.country_id
                }),
                $http({
                    method: 'get',
                    url: '/admin/cities?country_id=' + $scope.customer.country_id + '&province_id=' +
                        $scope.customer.province_id
                }),
                $http({
                    method: 'get',
                    url: '/admin/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                        $scope.receiver.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/currencies'
                })
            ]).then(function(resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesCustomer = resp[1].data.provinces;
                $scope.citiesCustomer = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.citiesReceiver = resp[4].data.cities;
                $scope.currencies = resp[5].data.currencies;
                if ($scope.mts.mts_status == 1) {
                    $scope.bntConfirm = true;
                    $scope.bntCancel = true;
                }
                if ($scope.mts.mts_status == 2) {
                    $scope.bntPayment = true;
                    $scope.bntCancel = true;
                }
                $scope.updateContainer($scope.mts);
            });
        };

        $scope.getProvincesCustomer = function(customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            $http({
                method: 'get',
                url: '/admin/provinces?country_id=' + country_id
            }).then(
                function(resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer(country_id)
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function(customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            var province_id = customer && customer.province_id ? customer.province_id : $scope.customer.province_id;
            $http({
                method: 'get',
                url: '/admin/cities?country_id=' + country_id + '&province_id=' + province_id
            }).then(
                function(resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };


        $scope.getProvincesReceiver = function() {
            $http({
                method: 'get',
                url: '/admin/provinces?country_id=' + $scope.address.country_id
            }).then(
                function(resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesReceiver = function() {
            $http({
                method: 'get',
                url: '/admin/cities?country_id=' + $scope.address.country_id + '&province_id=' +
                    $scope.address.province_id
            }).then(
                function(resp) {
                    $scope.citiesReceiver = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.searchAddress = function() {
            $http({
                method: 'get',
                url: '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function(resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    $scope.selectedId = $scope.receiver.id;
                    $scope.selectAddress($scope.receiver);

                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchAddress = false;
            });
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function(address_id) {
            var address = 0;
            if ($scope.addressSearchResult.length > 0 && address_id > 0) {
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        }

        $scope.selectAddress = function(address) {
            if (address) {
                $scope.address = address;
                $scope.selectId = address.id;
            }
            if (address == 0) {
                $scope.removeAddress();
            }
            $scope.getProvincesReceiver();

        };

        $scope.selectedItem = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            email: null,
            phone_number: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.removeAddress = function() {
            $scope.initAddress();
        };

        $scope.updateContainer = function(container) {
            $scope.container.id = container.id;
            $scope.container.destination = container.destination;
            if (!container.pay_date) {
                $scope.container.pay_date = $scope.date_now;
            } else {
                $scope.container.pay_date = container.pay_date;
            }
            $scope.container = container;
            $scope.container.status = container.mts_status;
            $scope.container.transaction_type = container.transaction_type;
            $scope.container.payment_by = container.payment_by;
            $scope.container.amount = container.total_goods;
            $scope.container.discount_type = container.discount_type;
            $scope.container.discount_number = container.discount_number;
            $scope.container.transfer_fee = container.transfer_fee;
            $scope.container.total = container.total_final;
            $scope.container.currency = container.currency_id;
        };

        $scope.updateFee = function(container) {
            // lấy giá trị phù hợp nhất
            let result = $scope.MTSSurcharge.filter(word => word.amount <= container.amount);
            result = result[result.length - 1];
            // hết đoạn xử lý chi phí
            var rate_number = result['rate'];
            let vnPriorityFlag = false;
            if ($scope.vnPriority.indexOf($scope.address.province_id) != -1) {
                vnPriorityFlag = true;
            }
            // nếu ngoài thành phố hồ chí minh
            if (!vnPriorityFlag) {
                rate_number = parseFloat(rate_number + 0.5);
            }
            rate_number = (parseFloat(rate_number) * parseFloat(container.amount)) / 100;
            var fee = result['fee'];
            var surcharge = result['surcharge'] * container.amount / 100;
            var total_fee = rate_number + fee + surcharge;
            var discount_number = parseFloat(container.discount_number) > 0 ? parseFloat(container.discount_number) : 0;
            if (discount_number > 0 && container.discount_type == 1) {
                discount_number = (parseFloat(container.amount) * parseFloat(container.discount_number)) / 100;
            } else if (discount_number > 0 && container.discount_type == 2) {
                discount_number = parseFloat(container.discount_number);
            }
            if (discount_number > total_fee) {
                discount_number = total_fee;
            }
            total_fee = total_fee - discount_number;
            var commission = parseFloat(total_fee) * $scope.commission;
            $scope.container.transfer_fee = total_fee.toFixed(2);
            $scope.container.commission = commission.toFixed(2);
            $scope.container.total = parseFloat(container.amount) + parseFloat(total_fee).toFixed(2);
            //  Surcharge=Surcharge*container.amount;
            // $scope.container.transfer_fee = (($scope.getPerFee() * parseFloat(container.amount)) + $scope.fix_fee) - parseFloat(discount_amount)+fee_hight+Surcharge;
            // $scope.container.total = parseFloat($scope.container.amount) + parseFloat($scope.container.transfer_fee);
        };

        $scope.getPerFee = function() {
            let vnPriorityFlag = false;
            let perFee = $scope.mts_max_per_fee;
            if ($scope.vnPriority.indexOf($scope.address.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (vnPriorityFlag) {
                perFee = $scope.mts_per_fee;
            }
            return perFee;
        };

        $scope.cancelOrder = function() {
            $scope.submittedCancel = true;
            $http({
                method: 'POST',
                url: '/admin/mts/quote/' + $scope.id + "/cancel",
                data: $scope.container
            }).then(
                function(resp) {
                    location.href = '/admin/mts/quote';
                },
                function(resp) {

                }
            ).finally(function() {
                $scope.submittedCancel = false;
            });
        };

        $scope.approve = function() {
            $scope.submittedConfirm = true;
            $http({
                method: 'POST',
                url: '/admin/mts/quote/' + $scope.id + "/approve",
                data: $scope.container
            }).then(
                function(resp) {
                    location.href = '/admin/mts/quote';
                },
                function(resp) {

                }
            ).finally(function() {
                $scope.submittedConfirm = false;
            });
        };

        $scope.payment = function() {
            $scope.submittedPayment = true;
            $http({
                method: 'POST',
                url: '/admin/mts/quote/' + $scope.id + "/payment",
                data: $scope.container
            }).then(
                function(resp) {
                    location.href = '/admin/mts/quote';
                },
                function(resp) {

                }
            ).finally(function() {
                $scope.submittedPayment = false;
            });
        };
    });
}());