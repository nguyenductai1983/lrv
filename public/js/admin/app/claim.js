var claim = {};

claim.addConversation = function () {
    fly.submit({
        id: 'claim-comment-form',
        service: '/admin/claim/create-conversation',
        success: function (result) {
            $('textarea[name=comment]').val('');
            if (result.success) {
                var content =   '<div class="customer-conversation">'
                                + '<label>' + result.data.content + '</label>'
                                + '<span class="claim-conversation-time">' + result.data.created_at + '</span>'
                                + '</div>';
                $("#claim-conversation-content").append(content);
            } else {
                popup.msg(result.message);
            }
        }
    });  
};

claim.complete = function (id) {
    popup.confirm(language.claim_complete, function () {
        fly.ajax({
            service: '/admin/claim/complete',
            method: "POST",
            loading: true,
            data: {
                id: id,
                _token : $('meta[name="token"]').attr('content')
            },

            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};