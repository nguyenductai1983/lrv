(function () {
    'use strict';

    var app = angular.module('TransportApp', []);
    var body_weight = 139;
    app.controller('TransportCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.submittedSearchCustomers = false;
        $scope.isEditDiscount = false;
        $scope.isDiscountCustom = false;
        $scope.showMoreInfoCustomer = false;
        $scope.isUseCoupon = false;
        $scope.customeraddress2 = false;
        $scope.reciveraddress2 = false;
        $scope.Updatereciver = false;
        $scope.Updatecustomer = false;
        $scope.SaveContact = true;
        $scope.ward = true;
        $scope.cod_type = false;
        $scope.initErrors = function () {
            $scope.errors = {
                customer: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.products = [];
        $scope.methods = [];
        $scope.isCrrCheckbox = false;

        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group: null,
            telephone: null,
            code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };

        $scope.addressesSearchResult = [];
        $scope.addressSearch = {
            customer_id: null,
            phone_number: null,
            postal_code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };
        $scope.CheckemailCus = function () {
            if ($scope.customer.email === null || $scope.customer.email === undefined) {
                $scope.customemailedit = false;
            }
            else {
                $scope.customemailedit = true;
            }
        };
        $scope.EditContact = function () {
            $scope.Updatecustomer = false;
            $scope.Updatereciver = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.CheckemailCus();
        };
        $scope.EditCustomer = function () {
            $scope.Updatecustomer = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.CheckemailCus();
        };
        $scope.Editreceiver = function () {
            $scope.Updatereciver = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
        };
        $scope.LockContact = function () {
            $scope.Updatereciver = true;
            $scope.Updatecustomer = true;
            $scope.SaveContact = false;
            $scope.contact_save = true;
            $scope.customemailedit = true;
        };
        $scope.FailContact = function () {
            $scope.Updatereciver = false;
            $scope.Updatecustomer = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.customemailedit = false;
        };
        $scope.initCustomer = function () {
            $scope.provincesCustomer = [];
            $scope.citiesCustomer = [];
            $scope.customer = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                email: null,
                telephone: null,
                cellphone: null,
                postal_code: null,
                city_id: null,
                province_id: null,
                country_id: $scope.senderCountryId,
                id_card: null,
                card_expire: null,
                birthday: null,
                career: null,
                image_1_file_id: null,
                image_2_file_id: null,
                image_3_file_id: null
            };
        };

        $scope.initAddress = function () {
            $scope.provincesAddress = [];
            $scope.citiesAddress = [];
            $scope.address = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                phone_number: null,
                cell_number: null,
                email: null,
                postal_code: null,
                city_id: null,
                // 11-04-2020
                ward_id: null,
                province_id: null,
                country_id: $scope.receiverCountryId
            };
        };
        $scope.initAddress();
        $scope.initCustomer();

        $scope.$watchCollection('customer.image_1_file_id', function (newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.initContainer = function () {
            var container = {
                products: [],
                length: null,
                width: null,
                height: null,
                volume: 0,
                shipping_fee: 0,
                total_weight: 0,
                total_shipping_fee: 0,
                total_declared_value: 0,
                total_surcharge: 0,
                total_insurance: 0,
                total_amount: 0,
                total_discount: 0,
                total_fee: 0,
                total_charge_fee: 0,
                min_fee: 0,
                total: 0,
                date_pay: null,
                pay: 0,
                debt: 0,
                currency_id: 1,
                pay_method: $scope.payMethod !== undefined ? $scope.payMethod : 'TD',
                status: 1,
                coupon_code: '',
                coupon_amount: 0,
                danger: 0, //them dong 02-07-2021
                cod_type: null //them dong 12-07-2021
            };
            for (var i = 1; i <= 4; ++i) {
                var product = {
                    product_id: null,
                    code: null,
                    name: null,
                    by_weight: null,
                    quantity: null,
                    weight: null,
                    price: null,
                    per_discount: null,
                    discount: null,
                    unit: null,
                    amount: null,
                    declared_value: null,
                    surcharge: null,
                    is_insurance: false,
                    insurance: null,
                    total: null,
                    note: null,
                    description: null,
                    danger: 0, //them dong 02-07-2021
                    limit_pcs: 0, // them ngay 07-07-2021
                    check_weight: false,// them ngay 07-07-2021
                    product_list: $scope.products
                };
                container.products.push(product);
            }
            return container;
        };

        $scope.containers = [$scope.initContainer()];

        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/products?service=1&status=1&agency=' + $scope.agency
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/payment-methods?type=1'
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.customer.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.address.country_id
                })
            ]).then(function (resp) {
                $scope.products = resp[0].data.products;
                angular.forEach($scope.containers, function (container) {
                    angular.forEach(container.products, function (product) {
                        product.product_list = angular.copy($scope.products);
                    });
                });

                $scope.countries = resp[1].data.countries;
                $scope.methods = resp[2].data.methods;
                $scope.provincesCustomer = resp[3].data.provinces;
                $scope.provincesReceiver = resp[4].data.provinces;
            });
            language = JSON.parse(language);
        };

        $scope.getProvincesCustomer = function (customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + country_id
            }).then(
                function (resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer(country_id);
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function (customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            var province_id = customer && customer.province_id ? customer.province_id : $scope.customer.province_id;
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + country_id + '&province_id=' +
                    province_id
            }).then(
                function (resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustomer = function () {
            angular.forEach($scope.citiesCustomer, function (value, key) {
                if (value.id == $scope.customer.city_id) {
                    $scope.customer.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function () {
            angular.forEach($scope.citiesReceiver, function (value, key) {
                if (value.id == $scope.address.city_id) {
                    $scope.address.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesReceiver = function () {
            // ngày 22-04-2020
            if ($scope.address.country_id === 91) {
                $scope.ward = false;
            }
            else {
                $scope.ward = true;
            }
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.address.country_id
            }).then(
                function (resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
            $scope.setCountryCode();
        };

        $scope.setCountryCode = function () {
            $http({
                method: 'get',
                url: '/api/admin/countriescode?id=' + $scope.address.country_id
            }).then(
                function (resp) {
                    $scope.country_code = resp.data.countriescode[0].code;
                }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.address.country_id + '&province_id=' +
                    $scope.address.province_id
            }).then(
                function (resp) {
                    $scope.citiesReceiver = resp.data.cities;
                    //24-04-2020
                    $scope.getWardsReceiver();
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };
        // ngày 22-04-2020
        $scope.getWardsReceiver = function () {
            if ($scope.address.country_id === 91) {
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.address.city_id
                }).then(
                    function (resp) {
                        $scope.wardsReceiver = resp.data.wards;
                    },
                    function (resp) {
                        console.log(resp);
                        console.log(resp.statusText);
                    }
                );
            }
            $scope.updateContainers(true);
        };
        $scope.searchCustomers = function () {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function (resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchCustomers = false;
            });
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url: '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    if ($scope.addressSearchResult.length > 0) {
                        $scope.selectedId = resp.data.addresses.data[0].id;
                        $scope.selectAddress(resp.data.addresses.data[0]);
                        $scope.getCitiesReceiver();
                    }
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchAddress = false;
                $scope.Updatereciver = true;
                $scope.SaveContact = false;
            });
        };

        $scope.selectCustomer = function (customer) {
            $scope.customer = customer;
            $scope.addressSearch.customer_id = customer.id;
            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="customer.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if (customer['image' + i]) {
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                } else {
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }
            $scope.getProvincesCustomer(customer);
            $scope.searchAddress();
            angular.element('#modal-customers-search-result').modal('hide');
            $scope.Updatecustomer = true;
            if (customer['email'] == null) {
                $scope.customemailedit = false;
            }
            else {
                $scope.customemailedit = true;
            }
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function (address_id) {
            var address = 0;
            if ($scope.addressSearchResult.length > 0 && address_id > 0) {
                address = search(address_id, $scope.addressSearchResult);
                $scope.Updatereciver = true;
            }
            else {
                $scope.Updatereciver = false;
            }
            $scope.selectAddress(address);
        };

        $scope.selectAddress = function (address) {
            if (address) {
                $scope.address = address;
                $scope.selectId = address.id;
            }
            if (address == 0) {
                $scope.removeAddress();
            }
            $scope.getProvincesReceiver();

        };

        $scope.selectedItem = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            email: null,
            phone_number: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.removeCustomer = function () {
            $scope.initCustomer();
            $scope.initAddress();
            $scope.EditContact();
            for (var i = 1; i <= 3; ++i) {
                var imgControl = angular.element('input[name="customer.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0),
                    imgPreview = imgControl.find('.img-preview');

                imgControl.find('.img-thumbnail').attr('src', '#');
                imgPreview.hide();
            }
        };

        $scope.removeAddress = function () {
            $scope.initAddress();
        };

        $scope.setActiveContainerTab = function () {
            setTimeout(function () {
                var container = angular.element('#container');
                container.find('.nav-tabs li.tab').removeClass('active');
                container.find('.nav-tabs li.tab:last').addClass('active');
                container.find('.tab-content .tab-pane').removeClass('active');
                container.find('.tab-content .tab-pane:last').addClass('active');
            });
        };

        $scope.addContainer = function () {
            $scope.containers.push($scope.initContainer());

            setTimeout(function () {
                setDatePicker();
            });

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function (index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function () {
                $scope.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function (product, idDropdown, indexcontainer) { //02-07-2021 them index container
            var code = product.code ? product.code.toLowerCase().trim() : '';
            var dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function (item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
            });
            // xu ly them loc san pham
            if (indexcontainer === undefined) {
                indexcontainer = 0;
            }
            let danger = $scope.containers[indexcontainer].danger;
            if (danger > 0) {
                if (danger === 1) {
                    product.product_list = product.product_list.filter(function (x) {
                        return x.danger !== 2
                    });
                    // product.product_list = product.product_list.filter(word=>word.danger != 2);
                    // do ie khong ho tro
                }
                else if (danger === 2) {
                    product.product_list = product.product_list.filter(function (x) {
                        return x.danger !== 1
                    });
                    // product.product_list = product.product_list.filter(word=>word.danger !== 1);
                }
            }
            //hết xử lý lọc sản phẩm
            var products = product.product_list.filter(function (item) {

                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.by_weight = item.by_weight;
            product.price = parseFloat(item.sale_price) + parseFloat(item.pickup_fee);
            product.description = item.description;
            product.quantity = 1;
            product.weight = item.weight;
            product.unit = item.unit;
            product.danger = item.danger;//them dong 02-07-2021
            product.limit_pcs = item.limit_pcs;//them dong 07-07-2021
            product.check_weight = false;// them ngay 07-07-2021
            $scope.updateContainers();
        };

        $scope.updateVolume = function (container) {
            if (!isNaN(container.length) && container.length > 0
                && !isNaN(container.width) && container.width > 0
                && !isNaN(container.height) && container.height > 0) {
                container.volume = ((parseFloat(container.length) * parseFloat(container.width) *
                    parseFloat(container.height)) / parseFloat(body_weight)).toFixed(2);
            } else {
                container.volume = 0;
            }
        };

        $scope.stringToDate = function (_date, _format, _delimiter) {
            if (_date != undefined) {
                var formatLowerCase = _format.toLowerCase();
                var formatItems = formatLowerCase.split(_delimiter);
                var dateItems = _date.split(_delimiter);
                var monthIndex = formatItems.indexOf("mm");
                var dayIndex = formatItems.indexOf("dd");
                var yearIndex = formatItems.indexOf("yyyy");
                var month = parseInt(dateItems[monthIndex]);
                month -= 1;
                var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
                return formatedDate;
            }
        }

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function (customer) {
            // $scope.errors.customer=null;
            $scope.errors.customer.id_card_required = false;
            $scope.errors.customer.birthday = false;
            $scope.errors.customer.card_expire = false;
            $scope.errors.customer.career = false;
            if (!customer.id_card) {
                $scope.errors.customer.id_card_required = true;
                return false;
            }
            if (customer.card_expire != undefined) {
                var cardTime = $scope.stringToDate(customer.card_expire, 'dd/MM/yyyy', "/").getTime();
                var currentTime = new Date().getTime();
                if (cardTime < currentTime) {
                    $scope.errors.customer.card_expire = true;
                    return false;
                }
            }
            else {
                $scope.errors.customer.card_expire = true;
            }
            if (!customer.birthday) {
                $scope.errors.customer.birthday = true;
                return false;
            }
            if (!customer.career) {
                $scope.errors.customer.career = true;
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function () {
            $scope.isEditDiscount = true;
            $scope.isDiscountCustom = false;
            if (!$scope.isAvailableIdCard($scope.customer)) {
                $scope.isEditDiscount = false;
            }
            else if ($scope.customer.image_1_file_id || $scope.customer.image_2_file_id || $scope.customer.image_3_file_id) {
                $scope.isDiscountCustom = true;
            }
            $scope.updateContainers();
        };

        $scope.remove = function (p_index, index) {
            $scope.containers[p_index].products.splice(index, 1);
            $scope.updateContainers();
        };
        $scope.addproduct = function (index) {
            var product = {
                product_id: null,
                code: null,
                name: null,
                by_weight: null,
                quantity: null,
                weight: null,
                price: null,
                per_discount: null,
                discount: null,
                unit: null,
                amount: null,
                declared_value: null,
                surcharge: null,
                is_insurance: false,
                insurance: null,
                total: null,
                note: null,
                description: null,
                danger: 0,
                limit_pcs: 0, // them ngay 07-07-2021
                check_weight: false,// them ngay 07-07-2021
                product_list: $scope.products
            };
            $scope.containers[index].products.push(product);
        };
        $scope.showPopupVip = function () {
            $scope.checkInfoCustomer();
            popup.msg(language.notify_admin_vip);
        };

        $scope.applyCoupon = function (container) {
            var data = {
                customer: $scope.customer,
                container: container
            };
            $http({
                method: 'POST',
                url: '/api/admin/coupon',
                data: data
            }).then(
                function (resp) {
                    var amount = parseFloat(resp.data.amount);
                    if (amount > 0) {
                        $scope.isUseCoupon = true;
                        container.coupon_amount = amount;
                        $scope.updateContainers();
                    } else {
                        container.coupon_code = '';
                    }
                }
            );
        };

        $scope.removeCoupon = function (container) {
            $scope.isUseCoupon = false;
            container.coupon_code = '';
            container.coupon_amount = 0;
            $scope.updateContainers();
        };

        $scope.updateContainers = function (isPostCode) {
            if (isPostCode) {
                $scope.getPostCodeReceiver();
            }
            angular.forEach($scope.containers, function (container) {
                container.shipping_fee = 0;
                container.total_shipping_fee = 0;
                container.total_weight = 0;
                container.total_declared_value = 0;
                container.total_surcharge = 0;
                container.total_insurance = 0;
                container.total_amount = 0;
                container.total_discount = 0;
                container.total_fee = 0;
                container.total_charge_fee = 0;
                container.min_fee = 0;
                container.total = 0;
                container.danger = 0; //them ngay 02-07-2021
                var city;
                if ($scope.address.city_id) {
                    angular.forEach($scope.citiesReceiver, function (item) {
                        if (item.id === $scope.address.city_id) {
                            city = item;
                            return true;
                        }
                    });
                }
                angular.forEach(container.products, function (product) {
                    // bat dau kiem tra san pham
                    if (product.product_id) {
                        var quantity = 0;
                        if (product.by_weight === 0) {
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                product.quantity = quantity;
                            }
                        } else {
                            if (!isNaN(product.weight) && product.weight > 0) {
                                quantity = parseFloat(product.weight);
                            }
                        }
                        // gioi han so luong can nang  them ngay 07-07-2021
                        product.check_weight = false;
                        if (product.weight > product.limit_pcs) {
                            product.weight = product.limit_pcs;
                            product.check_weight = true;
                        }
                        // het gioi han so luong can nang
                        if (!isNaN(product.weight) && product.weight > 0) {
                            container.total_weight = Number(container.total_weight) + Number(product.weight);
                        }
                        var amount = Math.round(parseFloat(product.price * quantity) * 1000) / 1000;
                        amount = Math.round(amount * 100) / 100;
                        if ($scope.isEditDiscount && !isNaN(product.per_discount) && product.per_discount > 0) {
                            var per_discount = parseFloat(product.per_discount);
                            var max_custom = parseFloat($scope.discountLevel.max_custom);
                            var max_base = parseFloat($scope.discountLevel.max_base);
                            if ($scope.isDiscountCustom) {
                                if (per_discount > max_custom) {
                                    product.per_discount = max_custom;
                                }
                            } else {
                                if (per_discount > max_base) {
                                    product.per_discount = max_base;
                                }
                            }
                            var discount = (product.per_discount / 100) * amount;
                            amount = amount - discount;
                            container.total_discount += discount;
                        }
                        product.amount = amount;
                        container.total_amount += product.amount;

                        product.surcharge = 0;
                        product.insurance = null;
                        if (!isNaN(product.declared_value) && product.declared_value > 0) {
                            if (quantity > 0) {
                                var quota = product.declared_value / quantity;
                                if (quota > $scope.configSurcharge.quota) {
                                    product.surcharge = parseFloat(product.declared_value) * $scope.configSurcharge.percent / 100;
                                }
                            }
                            container.total_surcharge += product.surcharge;
                            container.total_declared_value += parseFloat(product.declared_value);

                            if (product.is_insurance) {
                                product.insurance = parseFloat(product.declared_value) * $scope.configInsurance.percent / 100;
                                container.total_insurance += product.insurance;
                            }
                        }

                        product.total = product.amount + product.surcharge + product.insurance;
                        // Chỉ cộng thêm phần giá sản phẩm
                        container.total += product.amount;
                        //kiem tra co hang nguy hiem
                        if (product.danger === 1) {
                            container.danger = 1;
                        }
                        else if (product.danger === 2) {
                            container.danger = 2;
                        }
                        // het kiem tra hang nguy hiểm
                    }
                });
                // het kiem tra san phẩm
                container.shipping_fee = city && city.shipping_fee ? parseFloat(city.shipping_fee) : 0;
                container.total_shipping_fee = container.shipping_fee * container.total_weight;
                container.total_fee = container.total_shipping_fee + container.total_surcharge + container.total_insurance + container.total_amount;
                var old_min_fee = container.total_shipping_fee + container.total_amount;
                container.min_fee = $scope.getMinFee(old_min_fee);
                if (container.min_fee > old_min_fee) {
                    container.total_charge_fee = container.min_fee;
                } else {
                    container.total_charge_fee = old_min_fee;
                }
                container.total = container.total_charge_fee + container.total_surcharge + container.total_insurance;
                if (container.coupon_amount > 0) {
                    container.total -= container.coupon_amount;
                }
                if (container.total < 0) {
                    container.total = 0;
                }
                container.debt = 0;
                container.pay = container.total;
                if (!isNaN(container.pay) && container.pay > 0) {
                    container.debt = container.total - container.pay > 0 ? container.total - container.pay : 0;
                }
            });
        };
        $scope.setVolume = function (container) {
            if (parseFloat(container.volume) <= parseFloat(container.total_weight)) {
                return false;
            }
            var minProduct = {};
            var total_wight = 0;
            angular.forEach(container.products, function (product) {
                if (product.product_id) {
                    if (product.weight > 0) {
                        if (typeof minProduct.weight === 'undefined') {
                            minProduct = product;
                        }
                        if (typeof minProduct.weight !== 'undefined' && minProduct.weight > product.weight) {
                            minProduct = product;
                        }
                        product.weight = (container.volume * (parseFloat(product.weight) / container.total_weight)).toFixed(2);
                    } else {
                        product.weight = 0;
                    }
                    total_wight += parseFloat(product.weight);
                }
            });
            if (typeof minProduct.weight !== 'undefined' && parseFloat(minProduct.weight, 2) > 0) {
                minProduct.weight = (parseFloat(container.volume) - (parseFloat(total_wight) - parseFloat(minProduct.weight))).toFixed(2);
            }
            $scope.updateContainers();
        };
        $scope.setagree = function (check) {
            //   var flag = 0;
            if (check) {
                //     var containers = angular.copy($scope.containers);
                angular.forEach($scope.containers, function (container) {
                    if (container.volume > 0) {
                        if (parseFloat(container.volume) > parseFloat(container.total_weight)) {
                            $scope.setVolume(container);
                            //     flag=1;
                            popup.msg(language.msg_weight_volume, function () {
                                //     flag=2;
                            });
                            $scope.create = false;
                            $scope.agree = {
                                agree: false
                            };
                        }
                        else {
                            $scope.create = true;
                        }
                    }
                    else {
                        popup.msg(language.weight_volume_none);
                    }
                });
            };
        };
        $scope.updatepay = function () {
            var container = $scope.containers[0];
            // container.pay=$scope.containers.pay;
            // var test =  $scope.containers;
            var pay_customer = Math.abs(container.pay);
            if (!isNaN(pay_customer) && pay_customer >= 0) {
                container.debt = container.total - pay_customer > 0 ? container.total - pay_customer : 0;
            }
        };
        //Bsung mới tính toán giá trị min fee
        $scope.getMinFee = function () {
            let vnPriorityFlag = false;
            let minFee = 0;
            if ($scope.vnPriority.indexOf($scope.address.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (vnPriorityFlag) {
                minFee = $scope.minFee;
            } else {
                minFee = $scope.vn_hcm_priority + $scope.minFee;
            }
            return minFee;
        };

        $scope.scanCreateTransport = function () {
            var flag = 0;
            var containers = angular.copy($scope.containers);

            angular.forEach(containers, function (container) {
                if (container.volume > 0) {
                    if (container.volume > container.total_weight) {
                        flag = 1;
                    }
                }
                else {
                    flag = 2;
                }

            });
            if (flag == 1) {
                popup.confirmChange(language.weight_volume, function () {
                    $scope.createTransport();
                });
            }
            else if (flag == 2) {
                popup.msg(language.weight_volume_none);
            }
            else {
                $scope.createTransport();
            }
        };
        $scope.showAddress2 = function (tham_so) {
            if (tham_so === 1) {
                if ($scope.customeraddress2) {
                    $scope.customeraddress2 = false;
                }
                else {
                    $scope.customeraddress2 = true;
                };
            }
            else {

                if ($scope.reciveraddress2) {
                    $scope.reciveraddress2 = false;
                }
                else {
                    $scope.reciveraddress2 = true;
                };
            }
        };
        $scope.createTransport = function () {
            if ($scope.submittedSearchCustomers) {
                return true;
            }

            var containers = angular.copy($scope.containers);

            angular.forEach(containers, function (container) {
                $scope.setVolume(container);

                container.products = container.products.filter(function (product) {
                    return !!product.product_id;
                });
                angular.forEach(container.products, function (product) {
                    if (!$scope.isEditDiscount) {
                        product.discount = null;
                    }
                    delete product.product_list;
                });
            });
            var data = {
                customer: $scope.customer,
                receiver: $scope.address,
                containers: containers,
                is_crr: $scope.isCrrCheckbox,
                Updatereciver: $scope.Updatereciver,
                Updatecustomer: $scope.Updatecustomer
            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url: '/admin/transports/store',
                data: data
            }).then(
                function (resp) {
                    $scope.complete = true;
                    $scope.create = false;
                    $scope.new_order = resp.data;
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('containers') !== -1) {
                                for (var i = 0; i < containers.length; ++i) {
                                    if (typeof $scope.errors.container[i] === 'undefined') {
                                        $scope.errors.container[i] = {
                                            others: [],
                                            products: []
                                        };
                                    }
                                    if (field.indexOf('containers.' + i) === -1) {
                                        continue;
                                    }
                                    var errorPushed = false;
                                    for (var j = 0; j < containers[i].products.length; ++j) {
                                        if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                            $scope.errors.container[i].products[j] = [];
                                        }
                                        if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                            continue;
                                        }
                                        $scope.errors.container[i].products[j].push(error[0]);
                                        errorPushed = true;
                                    }
                                    if (!errorPushed) {
                                        $scope.errors.container[i].others.push(error[0]);
                                    }
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });
        };
        $scope.checkreceivercellphone = function () {
            if ($scope.address.country_id === 91) {
                if ($scope.createForm.receivertelephone.$viewValue) {
                    var test = $scope.createForm.receivertelephone.$viewValue.length;
                    if (test !== 10) {
                        popup.msg(language.cell_phone);
                    }
                }
            }
        };
        $scope.createContact = function () {
            var data = {
                customer: $scope.customer,
                address: $scope.address,
                Updatecustomer: $scope.Updatecustomer,
                Updatereciver: $scope.Updatereciver
            };
            $http({
                method: 'POST',
                url: '/api/admin/createcontact',
                data: data
            }).then(
                function (resp) {
                    $scope.customer = resp.data.customer;
                    $scope.address = resp.data.address;
                    $scope.addressSearchResult.unshift($scope.address);
                    $scope.LockContact();
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('address') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                        $scope.FailContact();
                    } else if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                        $scope.FailContact();
                    }

                }
            ).finally(function () {

            });
        };
    });
}());
