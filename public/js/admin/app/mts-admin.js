(function () {
    'use strict';
    var app = angular.module('MtsApp', []);
    app.controller('MtsListController', function ($scope) {
    $scope.Export = function () {
        var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()
    };
    var url = "/admin/mts/export?" + $.param(query);
   window.location.href = url;
       };
});
}());
