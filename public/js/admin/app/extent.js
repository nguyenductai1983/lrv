var extent = {};

extent.extra = function (id, token) {
    popup.open('create-extra', language.create_extra, template('extend/create_extra.tpl', {
        order_id: id,
        token: token
    }), [
        {
            title: language.popup_btn_agree,
            style: 'btn',
            fn: function () {
                fly.submit({
                    id: 'create-extra-form',
                    service: '/api/admin/create-extra',
                    success: function (result) {
                        popup.close('create-extra');
                        popup.msg(result.message);
                    }
                });
            }
        },
        {
            title: language.popup_btn_close,
            style: 'btn',
            fn: function () {
                popup.close('create-extra');
            }
        }
    ]);
};
extent.export = function () {
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()
    };
    var url = "/admin/extent/exportExcel?" + $.param(query);
   window.location.href =url;
};
extent.refund = function (id, token) {
    popup.open('create-refund', language.create_refund, template('extend/create_refund.tpl', {
        order_id: id,
        token: token
    }), [
        {
            title: language.popup_btn_agree,
            style: 'btn',
            fn: function () {
                fly.submit({
                    id: 'create-refund-form',
                    service: '/api/admin/create-refund',
                    success: function (result) {
                        popup.close('create-refund');
                        popup.msg(result.message);
                    }
                });
            }
        },
        {
            title: language.popup_btn_close,
            style: 'btn',
            fn: function () {
                popup.close('create-refund');
            }
        }
    ]);
};

extent.approve = function (id) {
    popup.confirm(language.extent_approve, function () {
        fly.ajax({
            service: '/admin/extent/approve',
            method: "POST",
            loading: true,
            data: {
                id: id,
                _token : $('meta[name="token"]').attr('content')
            },

            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};

extent.cancel = function (id) {
    popup.confirm(language.extent_cancel, function () {
        fly.ajax({
            service: '/admin/extent/cancel',
            method: "POST",
            loading: true,
            data: {
                id: id,
                _token : $('meta[name="token"]').attr('content')
            },

            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};
extent.payment = function (id) {
    popup.confirm(language.extent_payment, function () {
        fly.ajax({
            service: '/admin/extent/payment',
            method: "POST",
            loading: true,
            data: {
                id: id,
                _token : $('meta[name="token"]').attr('content')
            },

            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};
$(document).ready(function(){
  $("#mySearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#shipment-package-items tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
