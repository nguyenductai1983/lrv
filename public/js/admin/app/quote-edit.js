(function () {
    'use strict';

    var app = angular.module('QuoteApp', []);

    app.controller('QuoteEditController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submittedConfirm = false;
        $scope.submittedCancel = false;
        $scope.bntConfirm = true;
        $scope.bntCancel = true;
         $scope.initErrors = function () {
            $scope.errors = {
                sender: [],
                receiver: [],
                containers: []
            };
        };
        $scope.initErrors();
        $scope.countries = [];
        $scope.methods = [];
        $scope.quote = [];
        $scope.package_type = "1";

        $scope.sender = {
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            email: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.initContainers = function (order) {
            if (!order) {
                order = {
                    total_final: 0,
                    total_goods: 0,
                    total_shop_fee: 0,
                    total_shipping_fee: 0,
                    total_delivery_fee: 0,
                    total_paid_amount: 0,
                    last_paid_amount: 0,
                    last_date_pay: null,
                    last_payment_at: null,
                    payment_status: null,
                    pay_method: 'VIETCOMBANK',
                    user_note: '',
                    containers: []
                };
            }
            for (var i = 0; i < 7; ++i) {
                var product = order.containers[i];
                if (!product) {
                    product = {
                        id: null,
                        url: null,
                        name: null,
                        quantity: null,
                        price: null,
                        coupon: null,
                        shop_fee: null,
                        shipping_fee: null,
                        delivery_fee: null,
                        note: null
                    };
                    order.containers.push(product);
                }
            }
            return order;
        };

        $scope.getQuote = function () {
            $http({
                method: 'get',
                url: '/admin/quotes/' + $scope.id
            }).then(
                function (resp) {
                    $scope.init(resp.data.order);
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.init = function (order) {
            $scope.sender = order.sender;
            $scope.receiver = order.receiver;
            $scope.package_type = order.package_type;
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                    $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                    $scope.receiver.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/payment-methods?type=2'
                })
            ]).then(function (resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesSender = resp[1].data.provinces;
                $scope.citiesSender = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.citiesReceiver = resp[4].data.cities;
                $scope.methods = resp[5].data.methods;

                $scope.quote = $scope.initContainers(order);

                $scope.updateContainers();
            });
        };

        $scope.getProvincesSender = function () {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + country_id
            }).then(
                function (resp) {
                    $scope.provincesSender = resp.data.provinces;
                    $scope.getCitiesSender();
                }
            );
        };

        $scope.getCitiesSender = function () {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                function (resp) {
                    $scope.citiesSender = resp.data.cities;
                }
            );
        };

        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                    function (resp) {
                        $scope.provincesReceiver = resp.data.provinces;
                        $scope.getCitiesReceiver();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                    function (resp) {
                        $scope.citiesReceiver = resp.data.cities;
                    }
            );
        };

        $scope.updateContainers = function () {
            $scope.quote.total_goods = 0;
            $scope.quote.total_shop_fee = 0;
            $scope.quote.total_shipping_fee = 0;
            $scope.quote.total_delivery_fee = 0;
            $scope.quote.total_final = 0;
            angular.forEach($scope.quote.containers, function (container) {
                if(container.id){
                    $scope.quote.total_goods += parseFloat(container.price * container.quantity);
                    $scope.quote.total_shop_fee += parseFloat(container.shop_fee);
                    $scope.quote.total_shipping_fee += parseFloat(container.shipping_fee);
                    $scope.quote.total_delivery_fee += parseFloat(container.delivery_fee);
                }
            });
            $scope.quote.total_final = $scope.quote.total_goods + $scope.quote.total_shop_fee + $scope.quote.total_shipping_fee + $scope.quote.total_delivery_fee;
        };

        $scope.editQuote = function () {
            $scope.submittedConfirm = true;
            var containers = angular.copy($scope.quote.containers);
            containers = containers.filter(function (product) {
                return !!product.url;
            });

            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                total_final: $scope.quote.total_final,
                last_paid_amount: $scope.quote.last_paid_amount,
                last_date_pay: $scope.quote.last_date_pay,
                pay_method: $scope.quote.pay_method,
                package_type: $scope.package_type,
                user_note: $scope.quote.user_note,
                containers: containers
            };
            $http({
                method: 'PUT',
                url: '/admin/quotes/' + $scope.id + "/update",
                data: data
            }).then(
                    function (resp) {
                        location.href = '/admin/quotes';
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();
                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('customer') !== -1) {
                                    $scope.errors.customer.push(error[0]);
                                } else if (field.indexOf('receiver') !== -1) {
                                    $scope.errors.receiver.push(error[0]);
                                } else if (field.indexOf('containers') !== -1) {
                                    for (var i = 0; i < containers.length; ++i) {
                                        if (typeof $scope.errors.container[i] === 'undefined') {
                                            $scope.errors.container[i] = {
                                                others: [],
                                                products: []
                                            };
                                        }
                                        if (field.indexOf('containers.' + i) === -1) {
                                            continue;
                                        }
                                        var errorPushed = false;
                                        for (var j = 0; j < containers[i].products.length; ++j) {
                                            if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                                $scope.errors.container[i].products[j] = [];
                                            }
                                            if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                                continue;
                                            }
                                            $scope.errors.container[i].products[j].push(error[0]);
                                            errorPushed = true;
                                        }
                                        if (!errorPushed) {
                                            $scope.errors.container[i].others.push(error[0]);
                                        }
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                        } else {
                            console.log(resp.statusText);
                        }
                    }
            ).finally(function () {
                $scope.submittedConfirm = false;
            });
        };

        $scope.cancelQuote = function () {
            $scope.submittedCancel = true;
            var data = {
            };
            $http({
                method: 'POST',
                url: '/admin/quotes/' + $scope.id + "/cancel",
                data: data
            }).then(
                    function (resp) {
                        location.href = '/admin/quotes';
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();
                            angular.element('#modal-errors').modal('show');
                        } else {
                            console.log(resp.statusText);
                        }
                    }
            ).finally(function () {
                $scope.submittedCancel = false;
            });
        };
    });
}());
