(function () {
    'use strict';

    var app = angular.module('YhlApp', []);
    app.controller('YhlCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.submittedSearchCustomers = false;
        $scope.showMoreInfoCustomer = false;
        $scope.isUseCoupon = false;
        $scope.customeraddress2 = false;
        $scope.reciveraddress2 = false;
        $scope.Updatereciver = false;
        $scope.Updatecustomer = false;
        $scope.SaveContact = true;
        $scope.ward = true;
        $scope.initErrors = function () {
            $scope.errors = {
                customer : [],
                receiver : [],
                container: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.products = [];
        $scope.methods = [];

        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group      : null,
            telephone  : null,
            code       : null,
            first_name : null,
            middle_name: null,
            last_name  : null
        };

        $scope.addressesSearchResult = [];
        $scope.addressSearch = {
            customer_id      : null,
            phone_number  : null,
            postal_code       : null,
            first_name : null,
            middle_name: null,
            last_name  : null
        };

        $scope.initCustomer = function () {
            $scope.provincesCustomer = [];
            $scope.citiesCustomer = [];
            $scope.customer = {
                id    : null,
                first_name     : null,
                middle_name    : null,
                last_name      : null,
                address_1      : null,
                email      : null,
                telephone      : null,
                cellphone      : null,
                postal_code    : null,
                city_id        : null,
                province_id    : null,
                country_id     : 91,
                id_card        : null,
                card_expire    : null,
                birthday       : null,
                career         : null,
                image_1_file_id: null,
                image_2_file_id: null,
                image_3_file_id: null
            };
        };

        $scope.initAddress = function () {
            $scope.provincesAddress = [];
            $scope.citiesAddress = [];
            $scope.address = {
                id: null,
                first_name : null,
                middle_name: null,
                last_name  : null,
                address_1    : null,
                phone_number  : null,
                telephone: null,
                postal_code  : null,
                city_id    : null,
                ward_id    : null,
                province_id: null,
                country_id : 91
            };
        };
        $scope.initAddress();
        $scope.initCustomer();
 $scope.showAddress2 = function (tham_so)
{
    if (tham_so === 1)
    {
        if($scope.customeraddress2)
        {
            $scope.customeraddress2=false;
        }
        else
        {
              $scope.customeraddress2=true;
        };
    }
    else
    {

        if($scope.reciveraddress2)
        {
            $scope.reciveraddress2=false;
        }
        else
        {
              $scope.reciveraddress2=true;
        };
    }
};
        $scope.initContainer = function () {
            var container = {
                products            : [],
                length              : null,
                width               : null,
                height              : null,
                volume              : 0,
                yhl_fee             : 0,
                total_weight        : 0,
                total_shipping_fee  : 0,
                total_amount        : 0,
                total_discount      : 0,
                total_quantity      : 0,
                total               : 0,
                date_pay            : null,
                pay                 : 0,
                debt                : 0,
                currency_id         : 1,
                pay_method          : 'ACB',
                status              : 1,
                coupon_code: '',
                coupon_amount: 0
            };
            for (var i = 1; i <= 4; ++i) {
                var product = {
                    product_id    : null,
                    code          : null,
                    name          : null,
                    by_weight     : null,
                    quantity      : null,
                    weight        : null,
                    price         : null,
                    per_discount  : null,
                    discount      : null,
                    agency_unit_id      : null,
                    agency_currency_id      : null,
                    unit          : null,
                    amount        : null,
                    total         : null,
                    note          : null,
                    description   : null,
                    product_list  : $scope.products
                };
                container.products.push(product);
            }
            return container;
        };

        $scope.containers = [$scope.initContainer()];

        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url   : '/api/admin/products?service=3&status=1&agency=' + $scope.agency
                }),
                $http({
                    method: 'get',
                    url   : '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url   : '/api/admin/payment-methods?type=2'
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.customer.country_id
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.address.country_id
                })
            ]).then(function (resp) {
                $scope.products = resp[0].data.products;
                angular.forEach($scope.containers, function (container) {
                    angular.forEach(container.products, function (product) {
                        product.product_list = angular.copy($scope.products);
                    });
                });

                $scope.countries = resp[1].data.countries;
                $scope.methods = resp[2].data.methods;
                $scope.provincesCustomer = resp[3].data.provinces;
                $scope.provincesReceiver = resp[4].data.provinces;
            });
        };

        $scope.getProvincesCustomer = function (customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            $http({
                method: 'get',
                url   : '/api/admin/provinces?country_id=' + country_id
            }).then(
                function (resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer(country_id);
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function (customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            var province_id = customer && customer.province_id ? customer.province_id : $scope.customer.province_id;
            $http({
                method: 'get',
                url   : '/api/admin/cities?country_id=' + country_id+ '&province_id=' +
                province_id
            }).then(
                function (resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustomer = function () {
            angular.forEach($scope.citiesCustomer, function(value, key){
                if(value.id == $scope.customer.city_id){
                    $scope.customer.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function () {
            angular.forEach($scope.citiesReceiver, function(value, key){
                if(value.id == $scope.address.city_id){
                    $scope.address.postal_code = value.postal_code;
                    return;
                }
            });
        };


        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url   : '/api/admin/provinces?country_id=' + $scope.address.country_id
            }).then(
                function (resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesReceiver = function () {
             // ngày 22-04-2020
             if($scope.address.country_id===91)
             {
                 $scope.ward = false;
             }
             else
             {
                 $scope.ward = true;
             }
            $http({
                method: 'get',
                url   : '/api/admin/cities?country_id=' + $scope.address.country_id + '&province_id=' +
                        $scope.address.province_id
            }).then(
                function (resp) {
                    $scope.citiesReceiver = resp.data.cities;
                    $scope.getWardsReceiver();
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );

        };

        $scope.getWardsReceiver = function () {
            if($scope.address.country_id===91){
            $http({
                method: 'get',
                url: '/api/admin/wards?city_id=' + $scope.address.city_id
            }).then(
                function (resp) {
                    $scope.wardsReceiver = resp.data.wards;
                    $scope.updateContainers();
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        }
        else
        {
            $scope.updateContainers();
        }

        };

        $scope.searchCustomers = function () {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url   : '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function (resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchCustomers = false;
            });
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url   : '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    $scope.selectedId = resp.data.addresses.data[0].id;
                    $scope.selectAddress(resp.data.addresses.data[0]);
                 //   $scope.getCitiesReceiver();

                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchAddress = false;
                $scope.Updatereciver = true;
                $scope.SaveContact=false;
                });
        };

        $scope.selectCustomer = function (customer) {
            $scope.customer = customer;
            $scope.addressSearch.customer_id = customer.id;
            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="customer.image_' + i +
                        '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if(customer['image' + i]){
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                }else{
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }
            $scope.getProvincesCustomer(customer);
            $scope.searchAddress();
            angular.element('#modal-customers-search-result').modal('hide');
            $scope.Updatecustomer = true;
            if (customer['email']== null)
            {
                $scope.customemailedit=false;
            }
            else
            {
               $scope.customemailedit=true;
            }
        };

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function(address_id){
            var address = 0;
            console.log(address_id);
            if($scope.addressSearchResult.length > 0 && address_id >0){
                address = search(address_id, $scope.addressSearchResult);
                 $scope.Updatereciver=true;
            }
            else
            {
                $scope.Updatereciver=false;
            }
            $scope.selectAddress(address);
        };

        $scope.selectAddress = function (address) {
            if(address){
                $scope.address = address;
                $scope.selectId = address.id;
            }
            if(address == 0){
                $scope.removeAddress();
            }
            $scope.getProvincesReceiver();

        };
        $scope.selectedItem = {
            id: null,
            first_name : null,
            middle_name: null,
            last_name  : null,
            address_1    : null,
            email    : null,
            phone_number  : null,
            postal_code  : null,
            city_id    : null,
            province_id: null,
            country_id : null
        };


        $scope.removeCustomer = function () {
            $scope.initCustomer();
            $scope.initAddress();
            for (var i = 1; i <= 3; ++i) {
                var imgControl = angular.element('input[name="customer.image_' + i +
                                                 '_file_id"]').parents('.img-control').eq(0),
                    imgPreview = imgControl.find('.img-preview');

                imgControl.find('.img-thumbnail').attr('src', '#');
                imgPreview.hide();
            }
        };

        $scope.removeAddress = function () {
            $scope.initAddress();
        };

        $scope.setActiveContainerTab = function () {
            setTimeout(function () {
                var container = angular.element('#container');
                container.find('.nav-tabs li.tab').removeClass('active');
                container.find('.nav-tabs li.tab:last').addClass('active');
                container.find('.tab-content .tab-pane').removeClass('active');
                container.find('.tab-content .tab-pane:last').addClass('active');
            });
        };

        $scope.addContainer = function () {
            $scope.containers.push($scope.initContainer());

            setTimeout(function () {
                setDatePicker();
            });

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function (index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function () {
                $scope.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function (product, idDropdown) {
            var code     = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function (item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
            });

            var products = product.product_list.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.by_weight = item.by_weight;
            product.price = parseFloat(item.sale_price) + parseFloat(item.pickup_fee);
            product.description = item.description;
            product.discount = 0;
            product.quantity = 1;
            product.weight = item.weight;
            product.unit = item.unit;
            $scope.updateContainers();
        };

        $scope.remove = function(p_index, index){
            $scope.containers[p_index].products.splice(index, 1);
            var product = {
                product_id    : null,
                code          : null,
                name          : null,
                by_weight     : null,
                quantity      : null,
                weight        : null,
                price         : null,
                per_discount  : null,
                discount      : null,
                agency_unit_id      : null,
                agency_currency_id      : null,
                unit          : null,
                amount        : null,
                total         : null,
                note          : null,
                description   : null,
                product_list  : $scope.products
            };
            $scope.containers[p_index].products.push(product);
            $scope.updateContainers();
        };

        $scope.applyCoupon = function (container) {
            var data = {
                customer: $scope.customer,
                container: container
            };
            $http({
                method: 'POST',
                url: '/api/admin/coupon',
                data: data
            }).then(
                function (resp) {
                    var amount = parseFloat(resp.data.amount);
                    if(amount > 0){
                        $scope.isUseCoupon = true;
                        container.coupon_amount = amount;
                        $scope.updateContainers();
                    }else{
                        container.coupon_code = '';
                    }
                }
            );
        };

        $scope.removeCoupon = function (container) {
            $scope.isUseCoupon = false;
            container.coupon_code = '';
            container.coupon_amount = 0;
            $scope.updateContainers();
        };

         $scope.updateContainers = function (isPostCode) {
            if(isPostCode){
                $scope.getPostCodeReceiver();
            }
            angular.forEach($scope.containers, function (container) {
                container.yhl_fee = 0;
                container.total_shipping_fee = 0;
                container.total_weight = 0;
                container.total_amount = 0;
                container.total_discount = 0;
                container.total_quantity=0;
                container.total = 0;
                var city;
                if ($scope.address.city_id) {
                    angular.forEach($scope.citiesReceiver, function(value, key){
                        if(value.id == $scope.address.city_id){
                            city = value;
                            return;
                        }
                    });
                    // var list=$scope.citiesReceiver;
                    // city = list.find(list => list.id === $scope.address.city_id);
                }
                angular.forEach(container.products, function (product) {
                    if (product.product_id) {
                        var quantity = 0;
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                container.total_quantity += quantity;
                                container.total_weight += parseFloat(product.weight)*quantity;
                            }
                        var amount = product.price * quantity;
                        if (!isNaN(product.per_discount) && product.per_discount > 0) {
                            var discount = (parseFloat(product.per_discount) / 100) * amount;
                            amount = amount - discount;
                            container.total_discount += discount;
                        }
                        product.amount = amount;
                        container.total_amount += product.amount;
                        product.total = product.amount;
                        container.total += product.total;
                        container.debt = container.total;
                        if (!isNaN(container.pay) && container.pay > 0) {
                            container.debt = container.total - container.pay > 0 ? container.total - container.pay  : 0;
                        }
                    }
                });
                container.yhl_fee = city && city.yhl_fee ? parseFloat(city.yhl_fee) : 0;
                container.total_shipping_fee = container.yhl_fee * container.total_weight;
                container.total += container.total_shipping_fee;
                if(container.coupon_amount > 0){
                    container.total -= container.coupon_amount;
                }
                if(container.total < 0){
                    container.total = 0;
                }
                container.pay = container.total.toFixed(2);
                container.debt = container.total;
                if (!isNaN(container.pay) && container.pay > 0) {
                    container.debt = container.total - container.pay > 0 ? container.total - container.pay  : 0;
                }
            });
        };
        $scope.updatepay = function ()
         {
              var container = $scope.containers[0];
            // container.pay=$scope.containers.pay;
        // var test =  $scope.containers;
        var pay_customer = Math.abs(container.pay);
             if (!isNaN(pay_customer) && pay_customer >= 0) {
                    container.debt = container.total - pay_customer > 0 ? container.total - pay_customer : 0;
                    }
        };
        $scope.createYhl = function () {
            if ($scope.submittedSearchCustomers) {
                return true;
            }

            var containers = angular.copy($scope.containers);

            angular.forEach(containers, function (container) {
                container.products = container.products.filter(function (product) {
                    return !!product.product_id;
                });
                angular.forEach(container.products, function (product) {
                    delete product.product_list;
                });
            });

            var data = {
                customer            : $scope.customer,
                receiver            : $scope.address,
                containers          : containers,
                Updatecustomer      : $scope.Updatecustomer,
                Updatereciver       : $scope.Updatereciver
            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url   : '/admin/yhls/store',
                data  : data
            }).then(
                function (resp) {
                    // location.href = '/admin/yhls/create';
                    $scope.submitted=false;
                    $scope.complete=true;
                   $scope.new_order=resp.data;
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('containers') !== -1) {
                                for (var i = 0; i < containers.length; ++i) {
                                    if (typeof $scope.errors.container[i] === 'undefined') {
                                        $scope.errors.container[i] = {
                                            others  : [],
                                            products: []
                                        };
                                    }
                                    if (field.indexOf('containers.' + i) === -1) {
                                        continue;
                                    }
                                    var errorPushed = false;
                                    for (var j = 0; j < containers[i].products.length; ++j) {
                                        if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                            $scope.errors.container[i].products[j] = [];
                                        }
                                        if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                            continue;
                                        }
                                        $scope.errors.container[i].products[j].push(error[0]);
                                        errorPushed = true;
                                    }
                                    if (!errorPushed) {
                                        $scope.errors.container[i].others.push(error[0]);
                                    }
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });
        };

$scope.createContact = function (){
             var data = {
               customer: $scope.customer,
               address: $scope.address,
               Updatecustomer:$scope.Updatecustomer,
               Updatereciver:$scope.Updatereciver
            };
             $http({
                method: 'POST',
                url: '/api/admin/createcontact',
                data: data
            }).then(
                    function (resp) {
                        $scope.customer = resp.data.customer;
                        $scope.address = resp.data.address;
                        $scope.addressSearchResult.unshift($scope.address );
                        $scope.LockContact();
                    },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('address') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                        $scope.FailContact();
                    } else  if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                         $scope.FailContact();
                    }

                    }
                 ).finally(function () {

            });
         };
$scope.CheckemailCus =function()
{
   if($scope.customer.email === null)
    {
      $scope.customemailedit=false;
    }
    else
    {
        $scope.customemailedit=true;
    }
};
$scope.EditContact = function ()
{
    $scope.Updatecustomer=false;
    $scope.Updatereciver=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
    $scope.CheckemailCus();
};
$scope.EditCustomer = function ()
{
    $scope.Updatecustomer=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
    $scope.CheckemailCus();
};
$scope.LockContact = function ()
{
    $scope.Updatereciver=true;
    $scope.Updatecustomer=true;
    $scope.SaveContact=false;
    $scope.contact_save=true;
    $scope.customemailedit=true;
};
$scope.FailContact = function ()
{
    $scope.Updatereciver=false;
    $scope.Updatecustomer=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
    $scope.customemailedit=false;
};
$scope.setagree = function (check) {
    if (check) {
        $scope.create = true;
    }
            else
            {
                $scope.create = false;
            }
};
$scope.Editreceiver = function ()
{
    $scope.Updatereciver=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
};
   $scope.checkreceivercellphone = function (){
    if($scope.address.country_id===91)
    {
        if($scope.createForm.receivertelephone.$viewValue){
            var test =$scope.createForm.receivertelephone.$viewValue.length;
            if(test!==10)
            {
                popup.msg(language.cell_phone);
            }
        }

    }

  }

    });
}());
