(function() {
    'use strict';

    var app = angular.module('ExpressApp', []);

    app.controller('ExpressEditController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.isEditDiscount = false;
        $scope.submittedCancel = false;
        $scope.submittedGetShipping = false;
        $scope.submittedSearch = false;
        $scope.submittedConfirm = false;
        $scope.submittedPayment = false;
        $scope.disabled = true;
        $scope.initErrors = function() {
            $scope.errors = {
                addressFrom: [],
                addressTo: [],
                packages: [],
                pickups: [],
                order: [],
                quotes: []
            };
        };
        $scope.serviceIndex = null;
        $scope.showCreate = false;
        $scope.initErrors();
        $scope.countries = [];
        $scope.provincesFrom = [];
        $scope.provincesTo = [];
        $scope.citiesFrom = [];
        $scope.citiesTo = [];
        $scope.pickup = [];
        $scope.dimTypes = [];
        $scope.quote = [];
        $scope.methods = [];
        $scope.packageTypes = [{
            'id': 1,
            'name': 'Envelope'
        }, {
            'id': 2,
            'name': 'Pak'
        }, {
            'id': 3,
            'name': 'Package'
        }, {
            'id': 5,
            'name': 'SmarteHome'
        }];
        $scope.hourTimes = [{
            'id': '07',
            'name': '07'
        }, {
            'id': '08',
            'name': '08'
        }, {
            'id': '09',
            'name': '09'
        }, {
            'id': '10',
            'name': '10'
        }, {
            'id': '11',
            'name': '11'
        }, {
            'id': '12',
            'name': '12'
        }, {
            'id': '13',
            'name': '13'
        }, {
            'id': '14',
            'name': '14'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '16',
            'name': '16'
        }, {
            'id': '17',
            'name': '17'
        }];

        $scope.minuteTimes = [{
            'id': '00',
            'name': '00'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '30',
            'name': '30'
        }, {
            'id': '45',
            'name': '45'
        }];

        $scope.pickupLocation = [{
            'id': 'Receiving',
            'name': 'Receiving'
        }, {
            'id': 'Garage',
            'name': 'Garage'
        }, {
            'id': 'Lobby',
            'name': 'Lobby'
        }, {
            'id': 'Reception',
            'name': 'Reception'
        }, {
            'id': 'Front Desk',
            'name': 'Front Desk'
        }, {
            'id': 'Vault',
            'name': 'Vault'
        }, {
            'id': 'Switchboard',
            'name': 'Switchboard'
        }, {
            'id': 'Back Door',
            'name': 'Back Door'
        }, {
            'id': 'Desk',
            'name': 'Desk'
        }, {
            'id': 'Between Doors',
            'name': 'Between Doors'
        }, {
            'id': 'Kiosk',
            'name': 'Kiosk'
        }, {
            'id': 'Office',
            'name': 'Office'
        }, {
            'id': 'Outside Door',
            'name': 'Outside Door'
        }, {
            'id': 'Mailbox',
            'name': 'Mailbox'
        }, {
            'id': 'Side Door',
            'name': 'Side Door'
        }, {
            'id': 'Service Counter',
            'name': 'Service Counter'
        }, {
            'id': 'Security',
            'name': 'Security'
        }, {
            'id': 'Shipping',
            'name': 'Shipping'
        }, {
            'id': 'Front Door',
            'name': 'Front Door'
        }, {
            'id': 'Basement',
            'name': 'Basement'
        }, {
            'id': 'Mail Room',
            'name': 'Mail Room'
        }, {
            'id': 'Lab',
            'name': 'Lab'
        }, {
            'id': 'Warehouse',
            'name': 'Warehouse'
        }, {
            'id': 'Pharmacy',
            'name': 'Pharmacy'
        }, {
            'id': 'Pro Shop',
            'name': 'Pro Shop'
        }, {
            'id': 'Parts Department',
            'name': 'Parts Department'
        }, {
            'id': 'Counter',
            'name': 'Counter'
        }, {
            'id': 'Loading Dock',
            'name': 'Loading Dock'
        }, {
            'id': 'Gate House',
            'name': 'Gate House'
        }];

        $scope.changeSchedulePickup = function() {
            setTimeout(function() {
                setDatePicker();
            });
        };
        $scope.product = [];
        $scope.quotes = [];
        $scope.order = {
            customer_id: null,
            payment_method: 'TD',
            total_discount: 0,
            total_pay: 0,
            date_pay: null,
            pay: 0,
            debt: 0
        };
        $scope.initAddressFrom = function() {
            $scope.addressFrom = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                country_id: 14,
                city_id: null,
                province_id: null,
                postal_code: null,
                telephone: null,
                email: null,
                attention: null,
                instruction: null,
                shipping_date: null,
                confirm_delivery: false,
                residential: false
            };
        };
        $scope.initAddressTo = function() {
            $scope.addressTo = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                country_id: 47,
                province_id: null,
                city_id: null,
                postal_code: null,
                telephone: null,
                email: null,
                attention: null,
                instruction: null,
                notify_recipient: false,
                residential: false
            };
        };
        $scope.initAddressTo();
        $scope.initAddressFrom();
        $scope.initPackages = function() {
            var product = {
                id: null,
                length: 1,
                width: 1,
                height: 1,
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue: 0.0,
                description: null
            };
            $scope.packages = {
                type: 1,
                quantity: 1,
                referenceCode: null,
                dimType: 3,
                boxes: []
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };
        $scope.customer = null;
        $scope.initPackages();
        $scope.services = {
            saturdayDelivery: null,
            saturdayPickup: null,
            holdForPickup: null,
            docsOnly: null,
            dangerousGoods: null,
            signatureRequired: null,
            insuranceType: null,
            codPaymentType: null
        };
        $scope.initQuantity = function() {
            var range = [];
            for (var i = 1; i < 50; i++) {
                range.push(i);
            }
            $scope.quantities = range;
        };
        $scope.getExpress = function() {
            $http({
                method: 'get',
                url: '/admin/express/' + $scope.id
            }).then(
                function(resp) {
                    $scope.packages = resp.data.express.packages;
                    $scope.addressFrom = resp.data.express.addressFrom;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="addressFrom.image_' + i +
                            '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if ($scope.addressFrom['image' + i]) {
                            img = $scope.addressFrom['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        } else {
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.addressTo = resp.data.express.addressTo;
                    if (resp.data.express.quote !== null && resp.data.express.quote !== undefined) {
                        $scope.quote = resp.data.express.quote;
                        $scope.selectQuote = resp.data.express.quote;
                        $scope.quote.discount_number = resp.data.express.order.discount_number;
                        $scope.quote.total = resp.data.express.order.total_pay;
                    }
                    $scope.services = resp.data.express.services;
                    $scope.customer = resp.data.express.customer;
                    $scope.order = resp.data.express.order;
                    $scope.quotes = resp.data.express.quotes;
                    $scope.pickup = resp.data.express.pickup;
                    $scope.custom = resp.data.express.custom;
                    $scope.outside_canada = resp.data.express.outside_canada;
                    if ($scope.outside_canada == 2) {
                        $scope.getProvincesCustom();
                        $scope.getCitiesCustom();
                    }
                    $scope.init();
                    $scope.changePackageType();
                    $scope.initCustomerSearch();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.setService = function(index) {
            $scope.quote = $scope.quotes[index];
            $scope.quote.discount_number = 0;
            $scope.quote.discount_amount = (parseFloat($scope.quote.baseCharge) * parseFloat($scope.quote.discount_number)) / 100;
            $scope.quote.total = parseFloat($scope.quote.totalCharge) - parseFloat($scope.quote.discount_amount);
            $scope.chooseCarrer = true;
            setTimeout(function() {
                setDatePicker();
            });
        };
        $scope.customersSearchResult = [];
        $scope.initCustomerSearch = function() {
            $scope.customerSearch = {
                id: $scope.customer.id,
                telephone: $scope.customer.telephone,
                code: $scope.customer.code,
                first_name: $scope.customer.first_name,
                middle_name: $scope.customer.middle_name,
                last_name: $scope.customer.last_name
            };
        };
        $scope.changeOrder = function() {
            if ($scope.quote) {
                $scope.order.total_discount = 0.4 * $scope.quote.baseCharge;
                $scope.order.total_pay = $scope.quote.totalCharge - $scope.order.total_discount;
                $scope.order.debt = 0;
                if ($scope.order.total_pay >= $scope.order.pay) {
                    $scope.order.debt = $scope.order.total_pay - $scope.order.pay;
                }
            }
        };
        $scope.searchCustomers = function() {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function(resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchCustomers = false;
            });
        };
        $scope.selectCustomer = function(customer) {
            $scope.customer = customer;
            angular.element('#modal-customers-search-result').modal('hide');
            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="addressFrom.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if (customer['image' + i]) {
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                } else {
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }
        };
        $scope.init = function() {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.addressFrom.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.addressTo.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                        $scope.addressTo.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/dim-types'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/payment-methods?type=1'
                })
            ]).then(function(resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesFrom = resp[1].data.provinces;
                $scope.citiesFrom = resp[2].data.cities;
                $scope.provincesTo = resp[3].data.provinces;
                $scope.citiesTo = resp[4].data.cities;
                $scope.dimTypes = resp[5].data.dimTypes;
                $scope.methods = resp[6].data.methods;
                setTimeout(function() {
                    setDatePicker();
                });
            });
        };

        $scope.getProvincesFrom = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.addressFrom.country_id
            }).then(
                function(resp) {
                    $scope.provincesFrom = resp.data.provinces;
                    $scope.getCitiesFrom();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesFrom = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
            }).then(
                function(resp) {
                    $scope.citiesFrom = resp.data.cities;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.getProvincesTo = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.addressTo.country_id
            }).then(
                function(resp) {
                    $scope.provincesTo = resp.data.provinces;
                    $scope.getCitiesTo();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesTo = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                    $scope.addressTo.province_id
            }).then(
                function(resp) {
                    $scope.citiesTo = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getProvincesCustom = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.custom.billTo.country_id
            }).then(
                function(resp) {
                    $scope.provincesCustom = resp.data.provinces;
                    $scope.getCitiesCustom();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustom = function() {
            $http({
                method: 'get',
                url: '/admin/cities?country_id=' + $scope.custom.billTo.country_id + '&province_id=' +
                    $scope.custom.billTo.province_id
            }).then(
                function(resp) {
                    $scope.citiesCustom = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustom = function() {
            angular.forEach($scope.citiesCustom, function(value, key) {
                if (value.id === $scope.custom.billTo.city_id) {
                    $scope.custom.billTo.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.stringToDate = function(_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate;
        }

        $scope.$watchCollection('addressFrom.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function(cardDate) {
            if (!cardDate) {
                return false;
            }
            var cardTime = $scope.stringToDate(cardDate, 'dd/MM/yyyy', "/").getTime();
            var currentTime = new Date().getTime();
            if (cardTime < currentTime) {
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;
            if (!$scope.addressFrom.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.isAvailableIdCard($scope.addressFrom.card_expire)) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.image_1_file_id) {
                $scope.isEditDiscount = false;
            }
        };

        $scope.changePackageType = function() {
            if ($scope.packages.type == 1 || $scope.packages.type == 2) {
                $scope.packages.quantity = 1;
                $scope.changeQuantity();
                $scope.addressFrom.residential = false;
                $scope.disable_field = true;
            } else if ($scope.packages.type == 4) {
                $scope.disable_field = false;
                $scope.show_field = true;
            } else {
                $scope.disable_field = false;
                $scope.show_field = false;
            }
        };
        $scope.changeDimType = function() {
            var dim = 'in';
            var weight = 'lbs';
            var productHead = angular.element('#productHead');
            if ($scope.packages.dimType == 1) {
                dim = 'cm';
                weight = 'kg';
            }
            productHead.find('#lengthHeader').text('Length (' + dim + ')');
            productHead.find('#widthHeader').text('Width (' + dim + ')');
            productHead.find('#heightHeader').text('Height (' + dim + ')');
            productHead.find('#weightHeader').text('Weight (' + weight + ')');
        };
        $scope.changeQuantity = function() {
            var currentProduct = $scope.packages.boxes.length;
            if ($scope.packages.quantity > currentProduct) {
                for (var i = 0; i < $scope.packages.quantity - currentProduct; i++) {
                    var product = {
                        id: null,
                        length: 1,
                        width: 1,
                        height: 1,
                        weight: 1,
                        insuranceAmount: 0.0,
                        freightClass: null,
                        nmfcCode: null,
                        codValue: 0.0,
                        description: null
                    };
                    $scope.packages.boxes.push(product);
                }
            } else {
                for (var i = 0; i < currentProduct - $scope.packages.quantity; i++) {
                    $scope.packages.boxes.pop();
                }
            }

        };
        $scope.createExpress = function() {
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                pickup: $scope.pickup,
                custom: $scope.custom,
                packages: $scope.packages,
                services: $scope.services,
                quote: $scope.quote,
                quotes: $scope.quotes,
                order: $scope.order,
                outside_canada: $scope.outside_canada,
            };

            $scope.submittedConfirm = true;
            $http({
                method: 'POST',
                url: '/admin/express/storeupdate',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/express/list';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('quote') !== -1) {
                                $scope.errors.quotes.push(error[0]);
                            } else if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes: [],
                                        orther: []
                                    }
                                }
                                if (field.indexOf('packages.boxes') !== -1) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                } else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedConfirm = false;
            });
        };

        $scope.updateEpress = function() {
            var data = {
                order: $scope.order
            };

            $scope.submittedGetShipping = true;
            $http({
                method: 'POST',
                url: '/admin/express/update',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/express/list';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText)
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedGetShipping = false;
            });
        };

        $scope.paymentEpress = function() {
            var data = {
                order: $scope.order
            };

            $scope.submittedPayment = true;
            $http({
                method: 'POST',
                url: '/admin/express/payment',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/express/list';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedPayment = false;
            });
        };
        $scope.searchExpress = function() {
            $scope.submittedSearch = true;
            var packages = angular.copy($scope.packages);
            angular.forEach(packages.boxes, function(product) {
                if (product.volume > product.weight) {
                    product.weight = product.volume;
                }
            });
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                pickup: $scope.pickup,
                custom: $scope.custom,
                packages: packages,
                services: $scope.services,
                outside_canada: $scope.outside_canada
            };
            $http({
                method: 'POST',
                url: '/admin/express/get-quote',
                data: data
            }).then(
                function(resp) {
                    if (resp.data.success === true) {
                        $scope.submittedSearch = false;
                        $scope.quotes = resp.data.data;
                    } else {
                        $scope.errors.quotes.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes: [],
                                        orther: []
                                    };
                                }
                                if (field.indexOf('packages.boxes') !== -1) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                } else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedSearch = false;
            });
        };
        $scope.cancelOrder = function() {
            var data = {
                id: $scope.order.order_id
            };

            $scope.submittedCancel = true;
            $http({
                method: 'POST',
                url: '/admin/express/cancel-order',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/express/list';
                },
                function(resp) {
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        console.log($scope.errors.system);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedCancel = false;
            });
        };
    });
}());
