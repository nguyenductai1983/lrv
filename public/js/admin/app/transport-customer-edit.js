(function() {
    'use strict';

    var app = angular.module('TransportApp', []);
    var body_weight = 139;
    app.controller('TransportEditController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.isEditDiscount = false;
        $scope.submittedSearch = false;
        $scope.submittedCancel = false;
        $scope.submittedConfirm = false;
        $scope.submittedEdit = false;
        $scope.submittedGetShipping = false;
        $scope.submittedPayment = false;
        $scope.bntSearch = false;
        $scope.bntCancel = false;
        $scope.bntConfirm = false;
        $scope.bntEdit = false;
        $scope.ward = true;
        $scope.bntGetShipping = false;
        $scope.bntPayment = false;
        $scope.showMoreInfoCustomer = false;

        $scope.initErrors = function() {
            $scope.errors = {
                sender: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.products = [];
        $scope.warehouses = [];
        $scope.quotes = [];
        $scope.transport = [];

        $scope.hourTimes = [{
            'id': '07',
            'name': '07'
        }, {
            'id': '08',
            'name': '08'
        }, {
            'id': '09',
            'name': '09'
        }, {
            'id': '10',
            'name': '10'
        }, {
            'id': '11',
            'name': '11'
        }, {
            'id': '12',
            'name': '12'
        }, {
            'id': '13',
            'name': '13'
        }, {
            'id': '14',
            'name': '14'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '16',
            'name': '16'
        }, {
            'id': '17',
            'name': '17'
        }];

        $scope.minuteTimes = [{
            'id': '00',
            'name': '00'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '30',
            'name': '30'
        }, {
            'id': '45',
            'name': '45'
        }];

        $scope.pickupLocation = [{
            'id': 'Receiving',
            'name': 'Receiving'
        }, {
            'id': 'Garage',
            'name': 'Garage'
        }, {
            'id': 'Lobby',
            'name': 'Lobby'
        }, {
            'id': 'Reception',
            'name': 'Reception'
        }, {
            'id': 'Front Desk',
            'name': 'Front Desk'
        }, {
            'id': 'Vault',
            'name': 'Vault'
        }, {
            'id': 'Switchboard',
            'name': 'Switchboard'
        }, {
            'id': 'Back Door',
            'name': 'Back Door'
        }, {
            'id': 'Desk',
            'name': 'Desk'
        }, {
            'id': 'Between Doors',
            'name': 'Between Doors'
        }, {
            'id': 'Kiosk',
            'name': 'Kiosk'
        }, {
            'id': 'Office',
            'name': 'Office'
        }, {
            'id': 'Outside Door',
            'name': 'Outside Door'
        }, {
            'id': 'Mailbox',
            'name': 'Mailbox'
        }, {
            'id': 'Side Door',
            'name': 'Side Door'
        }, {
            'id': 'Service Counter',
            'name': 'Service Counter'
        }, {
            'id': 'Security',
            'name': 'Security'
        }, {
            'id': 'Shipping',
            'name': 'Shipping'
        }, {
            'id': 'Front Door',
            'name': 'Front Door'
        }, {
            'id': 'Basement',
            'name': 'Basement'
        }, {
            'id': 'Mail Room',
            'name': 'Mail Room'
        }, {
            'id': 'Lab',
            'name': 'Lab'
        }, {
            'id': 'Warehouse',
            'name': 'Warehouse'
        }, {
            'id': 'Pharmacy',
            'name': 'Pharmacy'
        }, {
            'id': 'Pro Shop',
            'name': 'Pro Shop'
        }, {
            'id': 'Parts Department',
            'name': 'Parts Department'
        }, {
            'id': 'Counter',
            'name': 'Counter'
        }, {
            'id': 'Loading Dock',
            'name': 'Loading Dock'
        }, {
            'id': 'Gate House',
            'name': 'Gate House'
        }];

        $scope.changeSchedulePickup = function() {
            setTimeout(function() {
                setDatePicker();
            });
        };

        $scope.sender = {
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            shipping_date: null,
            country_id: null,
            image_1_file_id: null,
            image_2_file_id: null,
            image_3_file_id: null
        };
        $scope.$watchCollection('customer.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });
        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            ward_id: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.initContainers = function(data) {
            var transport = {
                id: data.id,
                date_sender: data.date_sender,
                date_receiver: data.date_receiver,
                date_from_shipping: data.date_from_shipping,
                date_to_shipping: data.date_to_shipping,
                discount_international_fee: data.discount_international_fee || 0,
                total_shipping_local: data.total_shipping_local,
                total_shipping_international: data.total_shipping_international,
                total_shipping: data.total_shipping,
                warehouse_id: data.warehouse_id,
                quote: data.quote,
                quotes: data.quotes,
                transport_status: data.transport_status,
                eshiper_charge_order: data.eshiper_charge_order,
                eshiper_order_id: data.eshiper_order_id,
                eshiper_shipping_status: data.eshiper_shipping_status,
                eshiper_status_name: data.eshiper_status_name,
                payment_status: data.payment_status,
                last_payment_at: data.last_payment_at,
                charge_foreign_fee: data.charge_foreign_fee,
                charge_total_final: data.charge_total_final,
                total_paid_amount: data.total_paid_amount,
                total_remain_amount: data.total_remain_amount,
                pay_method: 'CK',
                last_paid_amount: 0,
                last_date_pay: null,
                receive_status: data.receive_status || 1,
                containers: []
            };
            for (var i = 0; i < data.containers.length; i++) {
                var container = {
                    products: [],
                    id: data.containers[i].id,
                    length: data.containers[i].length,
                    width: data.containers[i].width,
                    height: data.containers[i].height,
                    shipping_fee: data.containers[i].shipping_fee,
                    total_weight: data.containers[i].total_weight,
                    total_shipping_fee: data.containers[i].total_shipping_fee,
                    total_declared_value: data.containers[i].total_declare_price,
                    total_surcharge: data.containers[i].total_surcharge_fee,
                    total_insurance: data.containers[i].total_insurance_fee,
                    total: data.containers[i].total_final,
                    coupon_code: data.containers[i].coupon_code,
                    coupon_amount: parseFloat(data.containers[i].coupon_amount) || 0
                };
                for (var j = 0; j < 5; ++j) {
                    var product = data.containers[i].products[j];
                    if (!product) {
                        product = {
                            product_id: null,
                            code: null,
                            name: null,
                            quantity: null,
                            weight: 0,
                            declared_value: null,
                            surcharge: null,
                            is_insurance: false,
                            insurance: null,
                            per_discount: 0,
                            total: null,
                            note: null,
                            product_list: $scope.products
                        };
                    } else {
                        product.product_list = $scope.products;
                    }
                    container.products.push(product);
                }
                transport.containers.push(container);
            }
            return transport;
        };

        $scope.initContainer = function() {
            var container = {
                products: [],
                length: null,
                width: null,
                height: null,
                shipping_fee: 0,
                total_weight: 0,
                total_shipping_fee: 0,
                total_declared_value: 0,
                total_surcharge: 0,
                total_insurance: 0,
                total: 0,
                coupon_code: '',
                coupon_amount: 0
            };
            for (var i = 1; i <= 5; ++i) {
                var product = {
                    product_id: null,
                    code: null,
                    name: null,
                    quantity: null,
                    weight: null,
                    declared_value: null,
                    surcharge: null,
                    is_insurance: false,
                    insurance: null,
                    per_discount: 0,
                    total: null,
                    note: null,
                    product_list: $scope.products
                };
                container.products.push(product);
            }

            return container;
        };

        function getFormatDate(date) {
            var result = new Date(date);
            var dd = result.getDate();
            var mm = result.getMonth() + 1;
            var yyyy = result.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return dd + '/' + mm + '/' + yyyy;
        };

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);

            return result;
        };

        $scope.getTransport = function() {
            $http({
                method: 'get',
                url: '/admin/transports/quote/' + $scope.id
            }).then(
                function(resp) {
                    $scope.init(resp.data.transport);
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.init = function(transport) {
            $scope.sender = transport.sender;
            $scope.customer = transport.customer;
            if ($scope.customer !== null && $scope.customer !== undefined) {
                for (var i = 1; i <= 3; ++i) {
                    var img = '';
                    var imgControl = angular.element('input[name="customer.image_' + i +
                        '_file_id"]').parents('.img-control').eq(0);
                    var imgPreview = imgControl.find('.img-preview');
                    if ($scope.customer['image' + i]) {
                        img = $scope.customer['image' + i].path;
                    }
                    if (!img) {
                        imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                    } else {
                        imgControl.find('.img-thumbnail').attr('src', img);
                    }
                    imgPreview.show();
                }
            }
            $scope.receiver = transport.receiver;
            $scope.pickup = transport.pickup;
            $scope.quotes = transport.quotes;
            $scope.selectQuote = transport.quote;
            $scope.customer_note = transport.customer_note;
            $scope.chooseCarrer = true;
            $scope.bntEdit = false;
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/products?service=1&status=1'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                        $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                        $scope.receiver.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.receiver.city_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/warehouses'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/payment-methods?type=1'
                })
            ]).then(function(resp) {
                if (transport.transport_status != 3 && transport.eshiper_shipping_status != 4) {
                    $scope.bntCancel = true;
                }
                if (transport.quote && !transport.eshiper_order_id && transport.transport_status != 3 && transport.eshiper_shipping_status != 4) {
                    $scope.bntConfirm = true;
                } else if (!transport.quote && transport.transport_status == 1) {
                    $scope.bntConfirm = true;
                }
                if (transport.eshiper_order_id > 0 && transport.transport_status != 3 && transport.eshiper_shipping_status != 4) {
                    $scope.bntGetShipping = true;
                }
                if (transport.transport_status == 2) {
                    $scope.bntPayment = true;
                }
                if ($scope.quotes == null) {
                    $scope.bntSearch = true;
                    $scope.bntConfirm = false;
                }
                if ($scope.receiver.country_id === 91) {
                    $scope.ward = false;
                }
                $scope.products = resp[0].data.products;
                $scope.countries = resp[1].data.countries;
                $scope.provincesSender = resp[2].data.provinces;
                $scope.citiesSender = resp[3].data.cities;
                $scope.provincesReceiver = resp[4].data.provinces;
                $scope.citiesReceiver = resp[5].data.cities;
                $scope.wardsReceiver = resp[6].data.wards;
                $scope.warehouses = resp[7].data.warehouses;
                $scope.methods = resp[8].data.methods;

                $scope.transport = $scope.initContainers(transport);
                $scope.updateContainers(true);
                setTimeout(function() {
                    $("#shipping-date").datepicker({ dateFormat: "dd/mm/yy" });
                    setDatePicker();
                });
            });
        };

        $scope.getProvincesSender = function() {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + country_id
            }).then(
                function(resp) {
                    $scope.provincesSender = resp.data.provinces;
                    $scope.getCitiesSender();
                }
            );
        };
        $scope.bntEditset = function() {
            if ($scope.transport.transport_status < 3) {
                $scope.bntEdit = true;
            }
        }
        $scope.getCitiesSender = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                function(resp) {
                    $scope.citiesSender = resp.data.cities;
                    $scope.updateContainers(false);
                    $scope.bntEditset();
                }
            );
        };

        $scope.getProvincesReceiver = function() {
            // ngày 22-04-2020
            if ($scope.receiver.country_id === 91) {
                $scope.ward = false;
            } else {
                $scope.ward = true;
            }
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                function(resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesReceiver = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                function(resp) {
                    $scope.citiesReceiver = resp.data.cities;
                }
            );
            $scope.updateContainers();
            $scope.getWardsReceiver();

        };
        // ngày 22-04-2020
        $scope.getWardsReceiver = function() {
            if ($scope.receiver.country_id === 91) {
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.receiver.city_id
                }).then(
                    function(resp) {
                        $scope.wardsReceiver = resp.data.wards;
                    },
                    function(resp) {
                        console.log(resp);
                        console.log(resp.statusText);
                    }
                );
            }
            $scope.bntEditset();
        }
        $scope.setActiveContainerTab = function() {
            setTimeout(function() {
                var container = angular.element('#container');
                container.find('.nav-tabs li.tab').removeClass('active');
                container.find('.nav-tabs li.tab:last').addClass('active');
                container.find('.tab-content .tab-pane').removeClass('active');
                container.find('.tab-content .tab-pane:last').addClass('active');
            });
        };

        $scope.addContainer = function() {
            $scope.transport.containers.push($scope.initContainer());

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function(index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function() {
                $scope.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function(product, idDropdown) {
            var code = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function(item) {
                item.show = (item.code.toLowerCase().indexOf(code) !== -1);
            });

            var products = product.product_list.filter(function(item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function() {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function() {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function() {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function() {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function(product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.by_weight = item.by_weight;
            product.unit_goods_fee = parseFloat(item.price);
            product.description = item.description;
            product.quantity = 1;
            product.weight = 1;
            product.unit = item.unit;
            $scope.updateContainers();
        };
        $scope.remove = function(p_index, index) {
            $scope.transport.containers[p_index].products.splice(index, 1);
        };
        $scope.addproduct = function(index) {
            var product = {
                product_id: null,
                code: null,
                name: null,
                quantity: null,
                weight: null,
                declared_value: null,
                surcharge: null,
                is_insurance: false,
                insurance: null,
                per_discount: 0,
                total: null,
                note: null,
                product_list: $scope.products
            };
            $scope.transport.containers[index].products.push(product);
        };
        $scope.updateVolume = function(container) {
            if (!isNaN(container.length) && container.length > 0 &&
                !isNaN(container.width) && container.width > 0 &&
                !isNaN(container.height) && container.height > 0) {
                container.volume = (parseFloat(container.length) * parseFloat(container.width) *
                    parseFloat(container.height)) / body_weight;
            } else {
                container.volume = 0;
            }
        };

        $scope.setVolume = function(container) {
            if (container.volume <= container.total_weight) {
                return false;
            }
            angular.forEach(container.products, function(product) {
                if (product.product_id) {
                    if (product.weight > 0) {
                        product.weight = (container.volume * (parseFloat(product.weight) / container.total_weight)).toFixed(2);
                    } else {
                        product.weight = 0;
                    }
                }
            });
            $scope.updateContainers(false);
        };

        $scope.$watchCollection('customer.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.stringToDate = function(_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate;
        }

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function(cardDate) {
            if (!cardDate) {
                return false;
            }
            var cardTime = $scope.stringToDate(cardDate, 'dd/MM/yyyy', "/").getTime();
            var currentTime = new Date().getTime();
            if (cardTime < currentTime) {
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;
            if (!$scope.customer.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.isAvailableIdCard($scope.customer.card_expire)) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.image_1_file_id) {
                $scope.isEditDiscount = false;
            }

            $scope.updateContainers();
        };

        $scope.showPopupVip = function() {
            popup.msg(language.notify_admin_vip);
        };

        $scope.setCarrer = function(index) {
            $scope.transport.quote = $scope.quotes[index];
            $scope.chooseCarrer = true;
            $scope.bntSearch = false;
            $scope.bntEditset();
            $scope.bntConfirm = false;
            $scope.updateContainers();
        };

        $scope.setNoCarrer = function() {
            $scope.transport.quote = null;
            $scope.chooseCarrer = true;
            $scope.bntSearch = false;
            $scope.bntEditset();
            $scope.bntConfirm = false;
            $scope.updateContainers();
        };

        //Bsung mới tính toán giá trị min fee
        $scope.getMinFee = function() {
            let caPriorityFlag = true;
            let vnPriorityFlag = false;
            let minFee = 0;
            if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (caPriorityFlag) {
                if (vnPriorityFlag) {
                    minFee = $scope.caVnPriorityFee;
                } else {
                    minFee = $scope.caVnPriorityDefaultFee;
                }
            } else {
                if (vnPriorityFlag) {
                    minFee = $scope.caVnDefaultPriorityFee;
                } else {
                    minFee = $scope.caVnDefaultFee;
                }
            }
            return minFee;
        };

        $scope.searchExpress = function() {
            $scope.submittedSearch = true;
            var containers = angular.copy($scope.transport.containers);

            angular.forEach(containers, function(container) {
                container.products = container.products.filter(function(product) {
                    return !!product.product_id;
                });
                angular.forEach(container.products, function(product) {
                    delete product.product_list;
                });
            });
            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                pickup: $scope.pickup,
                warehouse_id: $scope.transport.warehouse_id,
                containers: containers
            };
            $http({
                method: 'POST',
                url: '/admin/transports/quote/get-carrer',
                data: data
            }).then(
                function(resp) {
                    if (resp.data.success === true) {
                        $scope.submittedSearch = false;
                        $scope.bntSearch = false;
                        $scope.bntEditset();
                        $scope.bntConfirm = false;
                        $scope.quotes = resp.data.data;
                    } else {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('sender') !== -1) {
                                $scope.errors.sender.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('containers') !== -1) {
                                for (var i = 0; i < containers.length; ++i) {
                                    if (typeof $scope.errors.container[i] === 'undefined') {
                                        $scope.errors.container[i] = {
                                            others: [],
                                            products: []
                                        };
                                    }
                                    if (field.indexOf('containers.' + i) === -1) {
                                        continue;
                                    }
                                    var errorPushed = false;
                                    for (var j = 0; j < containers[i].products.length; ++j) {
                                        if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                            $scope.errors.container[i].products[j] = [];
                                        }
                                        if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                            continue;
                                        }
                                        $scope.errors.container[i].products[j].push(error[0]);
                                        errorPushed = true;
                                    }
                                    if (!errorPushed) {
                                        $scope.errors.container[i].others.push(error[0]);
                                    }
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedSearch = false;
            });
        };

        $scope.updateContainers = function(isInit = true) {
            if (!isInit && $scope.transport.transport_status != 2) {
                $scope.quotes = null;
                $scope.transport.quote = null;
                $scope.selectQuote = null;
                $scope.bntSearch = true;
                $scope.bntEdit = false;
                $scope.bntConfirm = false;
            }
            $scope.transport.total_shipping_international = 0;
            $scope.transport.total_shipping_local = 0;
            $scope.transport.total_shipping = 0;
            $scope.transport.date_receiver = null;
            $scope.transport.date_from_shipping = null;
            $scope.transport.date_to_shipping = null;
            angular.forEach($scope.transport.containers, function(container) {
                container.shipping_fee = 0;
                container.total_shipping_fee = 0;
                container.total_weight = 0;
                container.total_declared_value = 0;
                container.total_surcharge = 0;
                container.total_insurance = 0;
                container.total_amount = 0;
                container.total_discount = 0;
                container.total = 0;

                var city;
                if ($scope.receiver.city_id) {
                    angular.forEach($scope.citiesReceiver, function(item) {
                        if (item.id === $scope.receiver.city_id) {
                            city = item;
                            return true;
                        }
                    });
                }
                angular.forEach(container.products, function(product) {
                    if (product.product_id) {
                        var quantity = 0;
                        if (product.by_weight === 0) {
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                product.quantity = quantity;
                            }
                        } else {
                            if (!isNaN(product.weight) && product.weight > 0) {
                                quantity = parseFloat(product.weight);
                            }
                        }

                        if (!isNaN(product.weight) && product.weight > 0) {
                            container.total_weight += parseFloat(product.weight);
                        }
                        product.amount = parseFloat(parseFloat(product.unit_goods_fee) * quantity);
                        if ($scope.isEditDiscount && !isNaN(product.per_discount) && parseFloat(product.per_discount) > 0) {
                            var per_discount = parseFloat(product.per_discount);
                            var max_custom = parseFloat($scope.discountLevel.max_custom);
                            if (per_discount > max_custom) {
                                product.per_discount = max_custom;
                            }
                            var discount = (product.per_discount / 100) * product.amount;
                            product.amount = product.amount - discount;
                            container.total_discount += discount;
                        }

                        container.total_amount += product.amount;

                        product.surcharge = 0;
                        product.insurance = 0;

                        if (!isNaN(product.declared_value) && product.declared_value > 0) {
                            if (quantity > 0) {
                                var quota = product.declared_value / quantity;
                                if (quota > $scope.configSurcharge.quota) {
                                    product.surcharge = parseFloat(product.declared_value) * $scope.configSurcharge.percent / 100;
                                }
                            }
                            container.total_surcharge += product.surcharge;
                            container.total_declared_value += parseFloat(product.declared_value);

                            if (product.is_insurance) {
                                product.insurance = parseFloat(product.declared_value) * $scope.configInsurance.percent / 100;
                                container.total_insurance += product.insurance;
                            }
                        }

                        product.total = product.amount + product.surcharge + product.insurance;

                        container.total += product.amount;
                    }
                });
                container.shipping_fee = city && city.shipping_fee ? parseFloat(city.shipping_fee) : 0;
                container.total_shipping_fee = container.shipping_fee * container.total_weight;
                container.total_fee = container.total_shipping_fee + container.total_surcharge + container.total_insurance + container.total_amount;
                var old_min_fee = container.total_shipping_fee + container.total_amount;
                container.min_fee = $scope.getMinFee(old_min_fee);
                if (container.min_fee > old_min_fee) {
                    container.total_charge_fee = container.min_fee;
                } else {
                    container.total_charge_fee = old_min_fee;
                }
                container.total = container.total_charge_fee + container.total_surcharge + container.total_insurance;
                if (container.coupon_amount > 0) {
                    container.total -= container.coupon_amount;
                }
                if (container.total < 0) {
                    container.total = 0;
                }
                $scope.updateVolume(container);
                $scope.transport.total_shipping_international += container.total;
            });
            if ($scope.transport.quote) {
                var discount_international_fee = parseFloat($scope.transport.discount_international_fee) || 0;
                $scope.transport.total_shipping_local = parseFloat($scope.transport.quote.totalCharge) - discount_international_fee;
                var transitDays = parseInt($scope.transport.quote.transitDays);
                var date_receiver = addDays(new Date(), transitDays);
                var date_from_shipping = addDays(new Date(), transitDays + 7);
                var date_to_shipping = addDays(new Date(), transitDays + 14);
                $scope.transport.date_receiver = getFormatDate(date_receiver);
                $scope.transport.date_from_shipping = getFormatDate(date_from_shipping);
                $scope.transport.date_to_shipping = getFormatDate(date_to_shipping);
            }
            $scope.transport.quotes = $scope.quotes;
            $scope.transport.total_shipping = $scope.transport.total_shipping_international + $scope.transport.total_shipping_local;
            $scope.transport.charge_foreign_fee = parseFloat($scope.transport.charge_foreign_fee);
            $scope.transport.charge_total_final = $scope.transport.total_shipping_international + $scope.transport.charge_foreign_fee;
            $scope.transport.total_paid_amount = parseFloat($scope.transport.total_paid_amount) ? parseFloat($scope.transport.total_paid_amount) : 0;
            $scope.transport.total_remain_amount = $scope.transport.charge_total_final - $scope.transport.total_paid_amount;
        };

        $scope.editTransport = function() {
            $scope.submittedEdit = true;
            var containers = angular.copy($scope.transport.containers);
            angular.forEach(containers, function(container) {
                container.products = container.products.filter(function(product) {
                    return !!product.product_id;
                });
                angular.forEach(container.products, function(product) {
                    delete product.product_list;
                });
            });

            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                pickup: $scope.pickup,
                warehouse_id: $scope.transport.warehouse_id,
                quote: $scope.transport.quote,
                quotes: $scope.transport.quotes,
                containers: containers,
                total_shipping_local: $scope.transport.total_shipping_local,
                discount_international_fee: $scope.transport.discount_international_fee
            };
            $http({
                method: 'PUT',
                url: '/admin/transports/quote/' + $scope.id + "/update",
                data: data
            }).then(
                function(resp) {
                    $scope.bntSearch = false;
                    $scope.bntEdit = false;
                    $scope.bntConfirm = true;
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('containers') !== -1) {
                                for (var i = 0; i < containers.length; ++i) {
                                    if (typeof $scope.errors.container[i] === 'undefined') {
                                        $scope.errors.container[i] = {
                                            others: [],
                                            products: []
                                        };
                                    }
                                    if (field.indexOf('containers.' + i) === -1) {
                                        continue;
                                    }
                                    var errorPushed = false;
                                    for (var j = 0; j < containers[i].products.length; ++j) {
                                        if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                            $scope.errors.container[i].products[j] = [];
                                        }
                                        if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                            continue;
                                        }
                                        $scope.errors.container[i].products[j].push(error[0]);
                                        errorPushed = true;
                                    }
                                    if (!errorPushed) {
                                        $scope.errors.container[i].others.push(error[0]);
                                    }
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                }
            ).finally(function() {
                $scope.submittedEdit = false;
            });
        };

        $scope.approveTransport = function() {
            $scope.submittedConfirm = true;
            $http({
                method: 'POST',
                url: '/admin/transports/quote/' + $scope.id + "/approve",
                data: $scope.transport
            }).then(
                function(resp) {
                    location.href = '/admin/transports/quote';
                },
                function(resp) {

                }
            ).finally(function() {
                $scope.submittedConfirm = false;
            });
        };

        $scope.getShipping = function() {
            $scope.submittedGetShipping = true;
            $http({
                method: 'POST',
                url: '/admin/transports/quote/get-shipping-eshipper',
                data: {
                    id: $scope.id
                }
            }).then(
                function(resp) {
                    $scope.init(resp.data.transport);
                },
                function(resp) {

                }
            ).finally(function() {
                $scope.submittedGetShipping = false;
            });
        };

        $scope.paymentTransport = function() {
            $scope.submittedPayment = true;
            $http({
                method: 'POST',
                url: '/admin/transports/quote/payment',
                data: {
                    transport: $scope.transport
                }
            }).then(
                function(resp) {
                    if (resp.status !== 422) {
                        popup.msg(resp.data['message']);
                        // location.href = '/admin/transports/quote';
                    }
                }
            ).finally(function() {
                $scope.submittedPayment = false;
            });
        };

        $scope.cancelOrder = function() {
            var data = {
                id: $scope.id
            };

            $scope.submittedCancel = true;
            $http({
                method: 'POST',
                url: '/admin/transports/quote/cancel-eshipper',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/transports/quote';
                },
                function(resp) {
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedCancel = false;
            });
        };
    });
}());