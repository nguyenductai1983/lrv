(function () {
    'use strict';

    var app = angular.module('ExpressApp', []);

    app.controller('ExpressCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.initErrors = function () {
            $scope.errors = {
                addressFrom : [],
                addressTo : [],
                packages: [],
                order: [],
                quotes : []
            };
        };
        $scope.serviceIndex = null;
        $scope.showCreate = false;
        $scope.initErrors();
        $scope.countries = [];
        $scope.provincesFrom = [];
        $scope.provincesTo = [];
        $scope.citiesFrom = [];
        $scope.citiesTo = [];
        $scope.dimTypes = [];
        $scope.quote = [];
        $scope.customer = [];
        $scope.methods = [];
        $scope.show_customer = false;
        $scope.packageTypes = [
            {
                'id': 1,
                'name': 'Envelope'
            },{
                'id': 2,
                'name': 'Pak'
            },{
                'id': 3,
                'name': 'Package'
            },{
                'id': 4,
                'name': 'Pallet'
            },{
                'id': 5,
                'name': 'SmarteHome'
            }
        ];
        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group      : null,
            telephone  : null,
            code       : null,
            first_name : null,
            middle_name: null,
            last_name  : null
        };
        $scope.product = [];
        $scope.quotes = [];
        $scope.addressFrom = {
            company : null,
            address_1  : null,
            address_2       : null,
            country_id : null,
            city_id : null,
            province_id: null,
            postal_code  : null,
            telephone  : null,
            email  : null,
            attention  : null,
            instruction  : null,
            confirm_delivery  : false,
            residential  : false
        };
        $scope.addressTo = {
            company      : null,
            address_1  : null,
            address_2       : null,
            country_id : null,
            province_id: null,
            city_id: null,
            postal_code  : null,
            telephone  : null,
            email  : null,
            attention  : null,
            instruction  : null,
            notify_recipient  : false,
            residential  : false
        };
        $scope.initPackages = function () {
            var product = {
                id:null,
                length: 1,
                width: 1,
                height: 1,
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue:0.0,
                description: null
            };
            $scope.packages = {
                type: 1,
                quantity: 1,
                referenceCode: null,
                dimType: 3,
                boxes : []
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };
        $scope.initPackages();
        $scope.services = {
            saturdayDelivery: null,
            saturdayPickup: null,
            holdForPickup: null,
            docsOnly: null,
            dangerousGoods: null,
            signatureRequired: null,
            insuranceType: null,
            codPaymentType: null
        };
        $scope.order = {
            customer_id : null,
            payment_method : null
        };
        $scope.initQuantity = function () {
            var range = [];
            for(var i=1;i< 50;i++) {
                range.push(i);
            }
            $scope.quantities = range;
        };
        $scope.setService = function (index) {
            $scope.quote = $scope.quotes[index];
            $scope.showCreate = true;
        };
        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url   : '/admin/countries'
                }),
                $http({
                    method: 'get',
                    url   : '/api/dim-types'
                }),
                $http({
                    method: 'get',
                    url   : '/admin/transport/payment/methods'
                })
            ])
            .then(function (resp) {
                $scope.countries = resp[0].data.countries;
                $scope.dimTypes = resp[1].data.dimTypes;
                $scope.methods = resp[2].data.methods;
            });
        };
        $scope.init();

        $scope.getProvincesFrom = function () {
            $http({
                method: 'get',
                url   : '/admin/provinces?country_id=' + $scope.addressFrom.country_id
            }).then(
                function (resp) {
                    $scope.provincesFrom = resp.data.provinces;
                    $scope.getCitiesFrom()
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesFrom = function () {
            $http({
                method: 'get',
                url   : '/admin/cities?country_id=' + $scope.addressFrom.country_id+ '&province_id=' + $scope.addressFrom.province_id
            }).then(
                function (resp) {
                    $scope.citiesFrom = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.getProvincesTo = function () {
            $http({
                method: 'get',
                url   : '/admin/provinces?country_id=' + $scope.addressTo.country_id
            }).then(
                function (resp) {
                    $scope.provincesTo = resp.data.provinces;
                    $scope.getCitiesTo();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.searchCustomers = function () {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url   : '/admin/customers/search?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function (resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchCustomers = false;
            });
        };
        $scope.selectCustomer = function (customer) {
            $scope.customer = customer;
            $scope.show_customer = true;
            $scope.order.customer_id = customer.id;
            angular.element('#modal-customers-search-result').modal('hide');
        };
        $scope.getCitiesTo = function () {
            $http({
                method: 'get',
                url   : '/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                $scope.addressTo.province_id
            }).then(
                function (resp) {
                    $scope.citiesTo = resp.data.cities;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };
        $scope.disable_field = false;
        $scope.show_field = false;
        $scope.changePackageType = function () {
            if ($scope.packages.type == 1 || $scope.packages.type == 2) {
                $scope.packages.quantity = 1;
                $scope.changeQuantity();
                $scope.disable_field = true;
            } else if ($scope.packages.type == 4) {
                $scope.disable_field = false;
                $scope.show_field = true;
            } else {
                $scope.disable_field = false;
                $scope.show_field = false;
            }
        };
        $scope.changeDimType = function () {
            var dim = 'in';
            var weight = 'lbs';
            var productHead = angular.element('#productHead');
            if ($scope.packages.dimType == 1) {
                dim = 'cm';
                weight = 'kg';
            }
            productHead.find('#lengthHeader').text('Length ('+dim+')');
            productHead.find('#widthHeader').text('Width ('+dim+')');
            productHead.find('#heightHeader').text('Height ('+dim+')');
            productHead.find('#weightHeader').text('Weight ('+weight+')');
        };
        $scope.changeQuantity = function () {
            var currentProduct = $scope.packages.boxes.length;
            if ($scope.packages.quantity > currentProduct) {
                for (var i = 0 ; i < $scope.packages.quantity - currentProduct; i++) {
                    var product = {
                        id:null,
                        length: 1,
                        width: 1,
                        height: 1,
                        weight: 1,
                        insuranceAmount: 0.0,
                        freightClass: null,
                        nmfcCode: null,
                        codValue: 0.0,
                        description: null
                    };
                    $scope.packages.boxes.push(product);
                }
            } else {
                for (var i = 0 ; i < currentProduct - $scope.packages.quantity; i++) {
                    $scope.packages.boxes.pop();
                }
            }

        };
        $scope.searchExpress = function () {
            $scope.submitted = true;
            var data = {
                addressFrom            : $scope.addressFrom,
                addressTo            : $scope.addressTo,
                packages          : $scope.packages,
                services            : $scope.services
            };
            $http({
                method: 'POST',
                url   : '/admin/express/get-quote',
                data  : data
            }).then(
                function (resp) {
                    if(resp.data.success === true) {
                        $scope.submitted = false;
                        $scope.quotes = resp.data.data;
                    }else {
                        $scope.errors.quotes.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            }  else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes : [],
                                        orther : []
                                    }
                                }
                                if(field.indexOf('packages.boxes') !== -1 ) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                }else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });
        };
        $scope.clearExpress = function () {
          $scope.addressTo = [];
          $scope.addressFrom = [];
          $scope.initPackages();
          $scope.services = [];
          $scope.quotes = [];
        };
        $scope.createExpress = function () {
            var data = {
                addressFrom            : $scope.addressFrom,
                addressTo            : $scope.addressTo,
                packages          : $scope.packages,
                services            : $scope.services,
                quote               : $scope.quote,
                order               : $scope.order
            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url   : '/admin/express/store',
                data  : data
            }).then(
                function (resp) {
                    location.href = '/admin/express/list';
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('quote') !== -1) {
                                $scope.errors.quotes.push(error[0]);
                            } else if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            }else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes : [],
                                        orther : []
                                    }
                                }
                                if(field.indexOf('packages.boxes') !== -1 ) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                }else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText)
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });
        }
    });
}());