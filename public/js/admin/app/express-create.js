(function() {
    'use strict';

    var app = angular.module('ExpressApp', []);
    var body_weight = 115;
    app.controller('ExpressCreateController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.isEditDiscount = false;
        $scope.submittedSearch = false;
        $scope.submittedConfirm = false;
        $scope.save_order = false;
        $scope.save_order_wait = false;
        $scope.bntSearch = true;
        $scope.bntConfirm = false;
        $scope.chooseCarrer = false;
        $scope.outside_canada = true;
        $scope.customeraddress2 = false;
        $scope.reciveraddress2 = false;
        $scope.Updatereciver = false;
        $scope.Updatecustomer = false;
        $scope.SaveContact = true;
        $scope.contact_save = false;
        $scope.initErrors = function() {
            $scope.errors = {
                addressFrom: [],
                addressTo: [],
                packages: [],
                order: [],
                quotes: [],
                pickups: [],
                custom: []
            };
        };
        $scope.serviceIndex = null;
        $scope.showCreate = false;
        $scope.initErrors();
        $scope.countries = [];
        $scope.provincesFrom = [];
        $scope.provincesTo = [];
        $scope.provincesCustom = [];
        $scope.citiesFrom = [];
        $scope.citiesTo = [];
        $scope.citiesCustom = [];
        $scope.pickup = [];
        $scope.dimTypes = [];
        $scope.quote = null;
        $scope.customer = [];
        $scope.methods = [];
        $scope.show_customer = false;
        $scope.show_pickup = false;

        $scope.packageTypes = [{
            'id': 1,
            'name': 'Envelope'
        }, {
            'id': 2,
            'name': 'Pak'
        }, {
            'id': 3,
            'name': 'Package'
        }, {
            'id': 5,
            'name': 'SmarteHome'
        }];

        $scope.hourTimes = [{
            'id': '07',
            'name': '07'
        }, {
            'id': '08',
            'name': '08'
        }, {
            'id': '09',
            'name': '09'
        }, {
            'id': '10',
            'name': '10'
        }, {
            'id': '11',
            'name': '11'
        }, {
            'id': '12',
            'name': '12'
        }, {
            'id': '13',
            'name': '13'
        }, {
            'id': '14',
            'name': '14'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '16',
            'name': '16'
        }, {
            'id': '17',
            'name': '17'
        }];

        $scope.minuteTimes = [{
            'id': '00',
            'name': '00'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '30',
            'name': '30'
        }, {
            'id': '45',
            'name': '45'
        }];

        $scope.pickupLocation = [{
            'id': 'Receiving',
            'name': 'Receiving'
        }, {
            'id': 'Garage',
            'name': 'Garage'
        }, {
            'id': 'Lobby',
            'name': 'Lobby'
        }, {
            'id': 'Reception',
            'name': 'Reception'
        }, {
            'id': 'Front Desk',
            'name': 'Front Desk'
        }, {
            'id': 'Vault',
            'name': 'Vault'
        }, {
            'id': 'Switchboard',
            'name': 'Switchboard'
        }, {
            'id': 'Back Door',
            'name': 'Back Door'
        }, {
            'id': 'Desk',
            'name': 'Desk'
        }, {
            'id': 'Between Doors',
            'name': 'Between Doors'
        }, {
            'id': 'Kiosk',
            'name': 'Kiosk'
        }, {
            'id': 'Office',
            'name': 'Office'
        }, {
            'id': 'Outside Door',
            'name': 'Outside Door'
        }, {
            'id': 'Mailbox',
            'name': 'Mailbox'
        }, {
            'id': 'Side Door',
            'name': 'Side Door'
        }, {
            'id': 'Service Counter',
            'name': 'Service Counter'
        }, {
            'id': 'Security',
            'name': 'Security'
        }, {
            'id': 'Shipping',
            'name': 'Shipping'
        }, {
            'id': 'Front Door',
            'name': 'Front Door'
        }, {
            'id': 'Basement',
            'name': 'Basement'
        }, {
            'id': 'Mail Room',
            'name': 'Mail Room'
        }, {
            'id': 'Lab',
            'name': 'Lab'
        }, {
            'id': 'Warehouse',
            'name': 'Warehouse'
        }, {
            'id': 'Pharmacy',
            'name': 'Pharmacy'
        }, {
            'id': 'Pro Shop',
            'name': 'Pro Shop'
        }, {
            'id': 'Parts Department',
            'name': 'Parts Department'
        }, {
            'id': 'Counter',
            'name': 'Counter'
        }, {
            'id': 'Loading Dock',
            'name': 'Loading Dock'
        }, {
            'id': 'Gate House',
            'name': 'Gate House'
        }];

        $scope.addressSearch = {
            customer_id: null
        };
        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group: null,
            telephone: null,
            code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };
        $scope.product = [];
        $scope.quotes = [];
        $scope.initAddressFrom = function() {
            $scope.addressFrom = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                country_id: 14,
                city_id: null,
                province_id: null,
                postal_code: null,
                telephone: null,
                cellphone: null,
                email: null,
                attention: null,
                instruction: '',
                shipping_date: null,
                confirm_delivery: false,
                residential: false,
                pickup_type: "1"
            };
        };
        $scope.initAddressTo = function() {
            $scope.addressTo = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                country_id: 14,
                province_id: null,
                city_id: null,
                postal_code: null,
                telephone: null,
                cellphone: null,
                email: null,
                attention: null,
                instruction: '',
                notify_recipient: false,
                residential: false
            };
        };
        $scope.showAddress2 = function(tham_so) {
            if (tham_so === 1) {
                if ($scope.customeraddress2) {
                    $scope.customeraddress2 = false;
                } else {
                    $scope.customeraddress2 = true;
                };
            } else {

                if ($scope.reciveraddress2) {
                    $scope.reciveraddress2 = false;
                } else {
                    $scope.reciveraddress2 = true;
                };
            }
        };
        $scope.initPickup = function() {
            $scope.pickup = {
                is_schedule: "1",
                contact_name: null,
                phone_number: null,
                location: null,
                date_time: null,
                start_hour_time: null,
                start_minute_time: null,
                closing_hour_time: null,
                closing_minute_time: null
            };
        };
        $scope.initItem = function() {
            $scope.item = {
                description: null,
                code: '12345',
                country_id: null,
                country_code: null,
                quantity: null,
                unitPrice: null,
                subPrice: null
            };
        };

        $scope.initCustom = function() {
            $scope.custom = {
                company: null,
                name: null,
                brokerName: null,
                taxId: null,
                phone: null,
                receiptsTaxId: null,
                total: 0,
                items: []
            };

            $scope.custom.billTo = {
                company: null,
                name: null,
                address_1: null,
                country_id: null,
                province_id: null,
                city_id: null,
                postal_code: null
            };

            $scope.custom.dutiesTaxes = {
                dutiable: 'false',
                billTo: 'receiver',
                consigneeAccount: null,
                sedNumber: null
            };

            $scope.initItem();
        };

        $scope.initAddressFrom();
        $scope.initAddressTo();
        $scope.initPickup();
        $scope.initCustom();

        $scope.initPackages = function() {
            var product = {
                id: null,
                length: 1,
                width: 1,
                height: 1,
                volume: parseFloat(1 / body_weight),
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue: 0.0,
                per_discount: 0,
                description: null
            };
            $scope.packages = {
                type: 3,
                quantity: 1,
                referenceCode: null,
                dimType: 3,
                boxes: []
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };
        $scope.initPackages();
        $scope.services = {
            saturdayDelivery: null,
            saturdayPickup: null,
            holdForPickup: null,
            docsOnly: null,
            dangerousGoods: null,
            signatureRequired: null,
            insuranceType: null,
            codPaymentType: null
        };
        $scope.order = {
            customer_id: null,
            payment_method: null,
            total_discount: 0,
            total_pay: 0,
            date_pay: null,
            debt: 0
        };
        $scope.addItem = function() {
            $scope.item.subPrice = $scope.item.unitPrice * $scope.item.quantity;
            $scope.custom.total += $scope.item.subPrice;
            $scope.getCodeCountry();
            $scope.custom.items.push($scope.item);
            $scope.item = { code: '12345' };
        };

        $scope.removeItem = function(index) {
            $scope.custom.total -= $scope.custom.items[index].subPrice;
            $scope.custom.items.splice(index, 1);
        };

        $scope.getCodeCountry = function() {
            angular.forEach($scope.countries, function(value, key) {
                if (value.id == $scope.item.country_id) {
                    $scope.item.country_code = value.code;
                    return;
                }
            });
        };

        $scope.initQuantity = function() {
            var range = [];
            for (var i = 1; i < 50; i++) {
                range.push(i);
            }
            $scope.quantities = range;
        };

        $scope.updateVolume = function(container) {
            if (!isNaN(container.length) && container.length > 0 &&
                !isNaN(container.width) && container.width > 0 &&
                !isNaN(container.height) && container.height > 0) {
                container.volume = (parseFloat(container.length) * parseFloat(container.width) *
                    parseFloat(container.height)) / body_weight;
            } else {
                container.volume = 0;
            }
        };

        $scope.setService = function(index) {
            $scope.quote = $scope.quotes[index];
            $scope.quote.discount_number = 0;
            $scope.updateFee();
            $scope.chooseCarrer = true;
            $scope.setagree($scope.bntConfirm);
            setTimeout(function() {
                setDatePicker();
            });
        };
        $scope.updatepay = function() {
            var pay_customer = Math.abs($scope.order.total_pay);
            if (!isNaN(pay_customer) && pay_customer >= 0) {
                $scope.order.debt = $scope.quote.total - pay_customer > 0 ? $scope.quote.total - pay_customer : 0;
            }
        };
        $scope.updateFee = function() {
            $scope.quote.discount_amount = (parseFloat($scope.quote.baseCharge) * parseFloat($scope.quote.discount_number)) / 100;
            $scope.quote.total = Math.round((parseFloat($scope.quote.totalCharge) - parseFloat($scope.quote.discount_amount)) * 100) / 100;
            $scope.order.total_pay = $scope.quote.total;
            $scope.order.debt = $scope.quote.total - $scope.order.total_pay;
        };

        $scope.changeSchedulePickup = function() {
            setTimeout(function() {
                setDatePicker();
            });
        };

        $scope.init = function() {
            $q.all([
                    $http({
                        method: 'get',
                        url: '/api/admin/countries'
                    }),
                    $http({
                        method: 'get',
                        url: '/api/dim-types'
                    }),
                    $http({
                        method: 'get',
                        url: '/api/admin/payment-methods?type=1'
                    })
                ])
                .then(function(resp) {
                    $scope.countries = resp[0].data.countries;
                    $scope.dimTypes = resp[1].data.dimTypes;
                    $scope.methods = resp[2].data.methods;
                    $scope.pickup.location = "Front Door";
                    $scope.pickup.start_hour_time = "07";
                    $scope.pickup.start_minute_time = "00";
                    $scope.pickup.closing_hour_time = "17";
                    $scope.pickup.closing_minute_time = "30";
                }).finally(function() {
                    $scope.getProvincesTo();
                    $scope.getProvincesFrom();
                });
        };

        $scope.getProvincesFrom = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.addressFrom.country_id
            }).then(
                function(resp) {
                    $scope.provincesFrom = resp.data.provinces;
                    $scope.getCitiesFrom();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesFrom = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
            }).then(
                function(resp) {
                    $scope.citiesFrom = resp.data.cities;

                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeFrom = function() {
            angular.forEach($scope.citiesFrom, function(value, key) {
                if (value.id == $scope.addressFrom.city_id) {
                    $scope.addressFrom.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.searchCustomers = function() {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function(resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchCustomers = false;
            });
        };
        $scope.selectCustomer = function(customer) {
            $scope.addressFrom = customer;
            $scope.addressFrom.attention = null;
            $scope.addressFrom.instruction = null;
            $scope.addressFrom.shipping_date = null;
            $scope.addressFrom.confirm_delivery = false;
            $scope.addressFrom.residential = false;
            $scope.addressFrom.pickup_type = "1";
            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="addressFrom.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if (customer['image' + i]) {
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                } else {
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }
            $scope.Updatecustomer = true;
            $scope.customemailedit = true;
            $scope.order.customer_id = customer.id;
            $scope.addressSearch.customer_id = customer.id;
            $scope.getProvincesFrom();
            $scope.searchAddress();
            angular.element('#modal-customers-search-result').modal('hide');
        };

        $scope.removeCustomer = function() {
            $scope.initAddressFrom();
            $scope.initAddressTo();
            $scope.order.customer_id = null;
            $scope.EditContact();
            for (var i = 1; i <= 3; ++i) {
                var imgControl = angular.element('input[name="addressFrom.image_' + i +
                        '_file_id"]').parents('.img-control').eq(0),
                    imgPreview = imgControl.find('.img-preview');

                imgControl.find('.img-thumbnail').attr('src', '#');
                imgPreview.hide();
            }
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.searchAddress = function() {
            $http({
                method: 'get',
                url: '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function(resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    if ($scope.addressSearchResult.length > 0) {
                        $scope.selectedId = resp.data.addresses.data[0].id;
                        $scope.selectAddress(resp.data.addresses.data[0]);
                        $scope.getCitiesTo();
                    } else {
                        $scope.selectedId = null;
                        $scope.initAddressTo();
                    }
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchAddress = false;
                $scope.Updatereciver = true;
                $scope.SaveContact = false;
            });
        };

        $scope.pickAddress = function(address_id) {
            var address = 0;
            if ($scope.addressSearchResult.length > 0 && address_id > 0) {
                address = search(address_id, $scope.addressSearchResult);
                $scope.Updatereciver = true;
            } else {
                $scope.Updatereciver = false;
            }
            $scope.selectAddress(address);
        };

        $scope.selectAddress = function(address) {
            if (address) {
                $scope.addressTo = address;
                $scope.selectId = address.id;
            }
            if (address == 0) {
                $scope.initAddressTo();
            }
            $scope.getProvincesTo();
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.getProvincesTo = function() {
            if ($scope.addressTo.country_id != $scope.sourceCounty) {
                $scope.outside_canada = true;
            } else {
                $scope.outside_canada = false;
            }
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.addressTo.country_id
            }).then(
                function(resp) {
                    $scope.provincesTo = resp.data.provinces;
                    $scope.getCitiesTo();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesTo = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' + $scope.addressTo.province_id
                    //  url   : '/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                    // $scope.addressTo.province_id
            }).then(
                function(resp) {
                    $scope.citiesTo = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeTo = function() {
            angular.forEach($scope.citiesTo, function(value, key) {
                if (value.id == $scope.addressTo.city_id) {
                    $scope.addressTo.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesCustom = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.custom.billTo.country_id
            }).then(
                function(resp) {
                    $scope.provincesCustom = resp.data.provinces;
                    $scope.getCitiesCustom();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustom = function() {
            $http({
                method: 'get',
                url: '/admin/cities?country_id=' + $scope.custom.billTo.country_id + '&province_id=' +
                    $scope.custom.billTo.province_id
            }).then(
                function(resp) {
                    $scope.citiesCustom = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustom = function() {
            angular.forEach($scope.citiesCustom, function(value, key) {
                if (value.id == $scope.custom.billTo.city_id) {
                    $scope.custom.billTo.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.stringToDate = function(_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate;
        }

        $scope.$watchCollection('addressFrom.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function(cardDate) {
            if (!cardDate) {
                return false;
            }
            var cardTime = $scope.stringToDate(cardDate, 'dd/MM/yyyy', "/").getTime();
            var currentTime = new Date().getTime();
            if (cardTime < currentTime) {
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;
            if (!$scope.addressFrom.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.isAvailableIdCard($scope.addressFrom.card_expire)) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.image_1_file_id) {
                $scope.isEditDiscount = false;
            }
        };

        $scope.disable_field = false;
        $scope.show_field = false;
        $scope.changePackageType = function() {
            if ($scope.packages.type == 1 || $scope.packages.type == 2) {
                $scope.packages.quantity = 1;
                $scope.changeQuantity();
                $scope.disable_field = true;
            } else if ($scope.packages.type == 4) {
                $scope.disable_field = false;
                $scope.show_field = true;
            } else {
                $scope.disable_field = false;
                $scope.show_field = false;
            }
        };
        $scope.changeDimType = function() {
            var dim = 'in';
            var weight = 'lbs';
            var productHead = angular.element('#productHead');
            if ($scope.packages.dimType == 1) {
                dim = 'cm';
                weight = 'kg';
            }
            productHead.find('#lengthHeader').text('Length (' + dim + ')');
            productHead.find('#widthHeader').text('Width (' + dim + ')');
            productHead.find('#heightHeader').text('Height (' + dim + ')');
            productHead.find('#weightHeader').text('Weight (' + weight + ')');
        };
        $scope.changeQuantity = function() {
            var currentProduct = $scope.packages.boxes.length;
            if ($scope.packages.quantity > currentProduct) {
                for (var i = 0; i < $scope.packages.quantity - currentProduct; i++) {
                    var product = {
                        id: null,
                        length: 1,
                        width: 1,
                        height: 1,
                        volume: parseFloat(1 / body_weight),
                        weight: 1,
                        insuranceAmount: 0.0,
                        freightClass: null,
                        nmfcCode: null,
                        codValue: 0.0,
                        per_discount: 0,
                        description: null
                    };
                    $scope.packages.boxes.push(product);
                }
            } else {
                for (var i = 0; i < currentProduct - $scope.packages.quantity; i++) {
                    $scope.packages.boxes.pop();
                }
            }

        };
        $scope.setagree = function(check) {
            if (check) {
                $scope.bntConfirm = true;
            } else {
                $scope.bntConfirm = false;
            }
        };

        $scope.scanSearchExpress = function() {
            var flag = false;
            var packages = angular.copy($scope.packages.boxes);

            angular.forEach(packages, function(product) {
                if (product.volume > product.weight) {
                    flag = true;
                }
            });
            if (flag) {
                popup.confirmChange(language.weight_volume, function() {
                    $scope.searchExpress();
                });
            } else {
                $scope.searchExpress();
            }
        };

        $scope.searchExpress = function() {
            $scope.submittedSearch = true;
            var packages = angular.copy($scope.packages);
            angular.forEach(packages.boxes, function(product) {
                if (product.volume > product.weight) {
                    product.weight = product.volume;
                }
            });
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                pickup: $scope.pickup,
                custom: $scope.custom,
                packages: packages,
                services: $scope.services,
                outside_canada: $scope.outside_canada
            };
            $http({
                method: 'POST',
                url: '/admin/express/get-quote',
                data: data
            }).then(
                function(resp) {
                    if (resp.data.success === true) {
                        $scope.submittedSearch = false;
                        for (var i = 0; i < resp.data.data.length; ++i) {
                            if (resp.data.data[i]['carrierId'] === "45") {
                                resp.data.data.splice(i, 1);
                            }
                        }
                        $scope.quotes = resp.data.data;
                    } else {
                        $scope.errors.quotes.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        $scope.save_order = false;
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes: [],
                                        orther: []
                                    };
                                }
                                if (field.indexOf('packages.boxes') !== -1) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                } else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.save_order = true;
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedSearch = false;
            });
        };

        $scope.clearExpress = function() {
            $scope.initAddressFrom();
            $scope.initAddressTo();
            $scope.initPackages();
            $scope.services = [];
            $scope.quotes = [];
        };

        $scope.remove = function(index) {
            $scope.packages.boxes.splice(index, 1);
            $scope.product.splice(index, 1);
            var product = {
                id: null,
                length: 1,
                width: 1,
                height: 1,
                volume: parseFloat(1 / body_weight),
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue: 0.0,
                per_discount: 0,
                description: null
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };

        $scope.showPopupVip = function() {
            popup.msg(language.notify_admin_vip);
        };

        $scope.createExpress = function(save_order) {
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                pickup: $scope.pickup,
                custom: $scope.custom,
                packages: $scope.packages,
                services: $scope.services,
                quote: $scope.quote,
                quotes: $scope.quotes,
                order: $scope.order,
                outside_canada: $scope.outside_canada,
                save: save_order,
            };

            $scope.submittedConfirm = true;
            $http({
                method: 'POST',
                url: '/admin/express/store',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/express/list';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('quote') !== -1) {
                                $scope.errors.quotes.push(error[0]);
                            } else if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes: [],
                                        orther: []
                                    }
                                }
                                if (field.indexOf('packages.boxes') !== -1) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                } else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedConfirm = false;
            });
        };
        $scope.saveExpress = function() {
            $scope.save_order_wait = true;
            $scope.createExpress(true);
        };
        // them ngay 25-12-2019
        $scope.CheckemailCus = function() {
            if ($scope.customer.email === null || $scope.customer.email === undefined) {
                $scope.customemailedit = false;
            } else {
                $scope.customemailedit = true;
            }
        };
        $scope.EditContact = function() {
            $scope.Updatecustomer = false;
            $scope.Updatereciver = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.CheckemailCus();
        };
        $scope.EditCustomer = function() {
            $scope.Updatecustomer = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.CheckemailCus();
        };
        $scope.Editreceiver = function() {
            $scope.Updatereciver = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
        };
        $scope.LockContact = function() {
            $scope.Updatereciver = true;
            $scope.Updatecustomer = true;
            $scope.SaveContact = false;
            $scope.contact_save = true;
            $scope.customemailedit = true;
        };
        $scope.FailContact = function() {
            $scope.Updatereciver = false;
            $scope.Updatecustomer = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.customemailedit = false;
        };
        $scope.createContact = function() {
            var data = {
                customer: $scope.addressFrom,
                address: $scope.addressTo,
                Updatecustomer: $scope.Updatecustomer,
                Updatereciver: $scope.Updatereciver
            };
            $http({
                method: 'POST',
                url: '/api/admin/createcontact',
                data: data
            }).then(
                function(resp) {
                    $scope.addressFrom = resp.data.customer;
                    $scope.addressTo = resp.data.address;
                    $scope.addressSearchResult.unshift($scope.addressTo);
                    $scope.LockContact();
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('address') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                        $scope.FailContact();
                    } else if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                        $scope.FailContact();
                    }
                }
            ).finally(function() {

            });
        };
        // het doan them
        $scope.image_click = function(image_object) {
            //function reply_click(clicked_id)
            if (document.getElementById(image_object).className === "col-sm-6 col-xs-12") {
                document.getElementById(image_object).className = "col-sm-3 col-xs-4";
            } else {
                document.getElementById(image_object).className = "col-sm-6 col-xs-12";
            }
        };

    });
}());
