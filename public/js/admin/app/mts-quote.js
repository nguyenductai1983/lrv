(function () {
    'use strict';
    var app = angular.module('MtsApp', []);
    app.controller('MtsquoteListController', function ($scope) {
    $scope.from_date = null;
    $scope.to_date = null;
    $scope.Export = function () {
        var query = {
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        };
        var url = "/admin/mts/quote/export?" + $.param(query);
       window.location.href = url;
    };
});
}());
