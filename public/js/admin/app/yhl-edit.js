(function () {
    'use strict';

    var app = angular.module('YhlApp', []);
    var body_weight = 139;
    app.controller('YhlEditController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.submittedSearchCustomers = false;
        $scope.showMoreInfoCustomer = false;
        $scope.isUseCoupon = false;
        $scope.Updatereciver = true;
        $scope.Updatecustomer = true;
        $scope.customemailedit = true;
        $scope.update = false;
        $scope.ward = true;
        $scope.complete=false;
        $scope.initErrors = function () {
            $scope.errors = {
                customer: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.products = [];
        $scope.methods = [];
        $scope.yhl_code = '';

        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group: 1,
            telephone: null,
            code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };

        $scope.initCustomer = function () {
            $scope.provincesCustomer = [];
            $scope.citiesCustomer = [];
            $scope.customer = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                email: null,
                telephone: null,
                cellphone: null,
                postal_code: null,
                city_id: null,
                province_id: null,
                country_id: null,
                id_card: null,
                card_expire: null,
                birthday: null,
                career: null,
                image_1_file_id: null,
                image_2_file_id: null,
                image_3_file_id: null
            };
        };
        $scope.initCustomer();
        $scope.provincesReceiver = [];
        $scope.citiesReceiver = [];
        $scope.receiver = {
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            post_code: null,
            city_id: null,
            ward_id: null,
            province_id: null,
            country_id: null
        };
        $scope.initContainer = function (container) {
            if (!container) {
                container = {
                    order_items: [],
                    length: null,
                    width: null,
                    height: null,
                    volume: 0,
                    yhl_fee: 0,
                    total_weight: 0,
                    total_shipping_fee: 0,
                    total_goods_fee: 0,
                    total_discount: 0,
                    total_final: 0,
                    total_quantity: 0,
                    total: 0,
                    date_pay: null,
                    total_paid_amount: 0,
                    total_remain_amount: 0,
                    last_paid_amount: 0,
                    last_date_pay: null,
                    currency_id: 1,
                    pay_method: 'VIETCOMBANK',
                    receiver_status: 1
                };
            }

            for (var i = 0; i < 4; ++i) {
                var product = container.order_items[i];
                if (!product) {
                    product = {
                        code: null,
                        name: null,
                        by_weight: null,
                        weight:0,
                        quantity: null,
                        per_discount: null,
                        discount: null,
                        unit: null,
                        amount: null,
                        sub_total_weight: 0,
                        sub_total_goods_fee: 0,
                        sub_total_discount: 0,
                        sub_total_final: 0,
                        total: null,
                        customer_note: null,
                        messure:{
                            code: null
                        },
                        product: {}
                    };
                    container.order_items.push(product);
                }
            }
            return container;
        };
        $scope.containers = [];
        $scope.transport = null;
        $scope.getYhl = function () {
            $http({
                method: 'get',
                url: '/admin/yhls/' + $scope.id
            }).then(
                function (resp) {
                    $scope.transport = resp.data.transport;
                    $scope.transport.coupon_amount = parseFloat($scope.transport.coupon_amount);
                    if($scope.transport.coupon_amount > 0){
                        $scope.isUseCoupon = true;
                    }else{
                        $scope.transport.coupon_amount = 0;
                    }
                    $scope.customer = $scope.transport.customer;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="customer.image_' + i +
                                '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if($scope.customer['image' + i]){
                            img = $scope.customer['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        }else{
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.receiver = $scope.transport.receiver;
                    $scope.selectedId =  $scope.transport.receiver.id;
                    $scope.customerSearch.code = $scope.customer.code;
                    $scope.yhl_code = $scope.transport.code;
                    $scope.addressSearch.customer_id = $scope.customer.id;
                    $scope.searchAddress();
                    $scope.init();
                    if ($scope.transport.shipping_status <6)
                    {
                        $scope.update=true;
                    }
                    else
                    {
                        $scope.update=false;
                    }
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/products?service=3&status=1&agency=' + $scope.agency
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.customer.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.customer.country_id + '&province_id=' +
                    $scope.customer.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                    $scope.receiver.province_id
                }),
                 //them ngay 24-04-2020
                 $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.receiver.city_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/payment-methods?type=2'
                })
            ]).then(function (resp) {
                $scope.products = resp[0].data.products;
                $scope.countries = resp[1].data.countries;
                $scope.provincesCustomer = resp[2].data.provinces;
                $scope.citiesCustomer = resp[3].data.cities;
                $scope.provincesReceiver = resp[4].data.provinces;
                $scope.citiesReceiver = resp[5].data.cities;
                $scope.wardsReceiver = resp[6].data.wards;
                $scope.methods = resp[7].data.methods;
                //angular.forEach($scope.transport.containers, function (container) {
                //});
                $scope.containers.push($scope.initContainer($scope.transport));

               $scope.updateContainers();
                setTimeout(function () {
                    setDatePicker();
                });
            });
        };

        $scope.getProvincesCustomer = function () {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.customer.country_id
            }).then(
                function (resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function () {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.customer.country_id + '&province_id=' +
                $scope.customer.province_id
            }).then(
                function (resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                function (resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesReceiver = function () {
            if($scope.receiver.country_id===91)
            {
                $scope.ward = false;
            }
            else
            {
                $scope.ward = true;
            }
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                $scope.receiver.province_id
            }).then(
                function (resp) {
                    $scope.citiesReceiver = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
            $scope.getPostCodeReceiver();
        };
        $scope.getWardsReceiver = function () {

            if($scope.address.country_id===91){
            $http({
                method: 'get',
                url: '/api/admin/wards?city_id=' + $scope.address.city_id
            }).then(
                function (resp) {
                    $scope.wardsReceiver = resp.data.wards;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        }
        $scope.updateContainers();
        };
        $scope.getPostCodeCustomer = function () {
            angular.forEach($scope.citiesCustomer, function(value, key){
                if(value.id == $scope.customer.city_id){
                    $scope.customer.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function () {
            angular.forEach($scope.citiesReceiver, function(value, key){
                if(value.id == $scope.receiver.city_id){
                    $scope.receiver.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.searchCustomers = function () {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function (resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                    $scope.submittedSearchCustomers = false;
                });
        };

        $scope.addressSearchResult = [];
        $scope.addressSearch = {
            customer_id      : null,
            phone_number  : null,
            postal_code       : null,
            first_name : null,
            middle_name: null,
            last_name  : null
        };
        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url: '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                    $scope.submittedSearchAddress = false;
                });
        };
        $scope.selectedId = 0;
        $scope.selectAddress = function (address) {
            if (address == 0) {
                $scope.provincesReceiver = [];
                $scope.citiesReceiver = [];
                $scope.receiver = {
                    id:null,
                    first_name: null,
                    middle_name: null,
                    last_name: null,
                    address_1: null,
                    address_2: null,
                    telephone: null,
                    cellphone: null,
                    post_code: null,
                    city_id: null,
                    ward_id: null,
                    province_id: null,
                    country_id: null
                };
            }
            if (address) {
                $scope.receiver = address;
            }
            $scope.getProvincesReceiver();

        };

        $scope.selectCustomer = function (customer) {
            $scope.customer = customer;
            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="customer.image_' + i +
                        '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if(customer['image' + i].hasOwnProperty('path')){
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                }else{
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }

            $scope.getProvincesCustomer();

            angular.element('#modal-customers-search-result').modal('hide');
        };

        $scope.removeCustomer = function () {
            $scope.initCustomer();

            for (var i = 1; i <= 3; ++i) {
                var imgControl = angular.element('input[name="customer.image_' + i +
                        '_file_id"]').parents('.img-control').eq(0),
                    imgPreview = imgControl.find('.img-preview');

                imgControl.find('.img-thumbnail').attr('src', '#');
                imgPreview.hide();
            }
        };

        $scope.setActiveContainerTab = function () {
            setTimeout(function () {
                var container = angular.element('#container');
                container.find('.nav-tabs li.tab').removeClass('active');
                container.find('.nav-tabs li.tab:last').addClass('active');
                container.find('.tab-content .tab-pane').removeClass('active');
                container.find('.tab-content .tab-pane:last').addClass('active');
            });
        };

        $scope.addContainer = function () {
            $scope.containers.push($scope.initContainer());

            setTimeout(function () {
                setDatePicker();
            });

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function (index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function () {
                $scope.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function (product, idDropdown) {
            var code = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);

            angular.forEach($scope.products, function (item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
            });

            var products = $scope.products.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.sub_total_surcharge_fee = 0;
            product.sub_total_discount = 0;
            product.sub_total_insurrance_fee = 0;
            product.unit_goods_fee = parseFloat(item.sale_price) + parseFloat(item.pickup_fee);
            product.quantity = 1;
            product.sub_total_weight = item.weight;
            product.messure.code = item.unit;
            product.product = item;
            product.product_id = item.id;
            product.pickup_fee = item.pickup_fee;

            $scope.updateContainers();
        };

        $scope.updateVolume = function (container) {
            if (!isNaN(container.length) && container.length > 0
                && !isNaN(container.width) && container.width > 0
                && !isNaN(container.height) && container.height > 0) {
                container.volume = (parseFloat(container.length) * parseFloat(container.width) *
                    parseFloat(container.height)) / body_weight;

            } else {
                container.volume = 0;
            }
        };

        $scope.setVolume = function (container) {
            if (container.volume <= container.total_weight) {
                return false;
            }
            angular.forEach(container.order_items, function (order_item) {
                if (order_item.product_id) {
                    if (order_item.sub_total_weight > 0) {
                        order_item.sub_total_weight = (container.volume * (parseFloat(order_item.sub_total_weight) / container.total_weight)).toFixed(2);
                    } else {
                        order_item.sub_total_weight = 0;
                    }
                }
            });
            $scope.updateContainers();
        };

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        };

        $scope.pickAddress = function(address_id){
            var address = 0;
            if($scope.addressSearchResult.length > 0 && address_id >0){
                address = search(address_id,$scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.applyCoupon = function (container) {
            var data = {
                customer: $scope.customer,
                container: container
            };
            $http({
                method: 'POST',
                url: '/api/admin/coupon',
                data: data
            }).then(
                function (resp) {
                    var amount = parseFloat(resp.data.amount);
                    if(amount > 0){
                        $scope.isUseCoupon = true;
                        container.coupon_amount = amount;
                        $scope.updateContainers();
                    }else{
                        container.coupon_code = '';
                    }
                }
            );
        };

        $scope.removeCoupon = function (container) {
            $scope.isUseCoupon = false;
            container.coupon_code = '';
            container.coupon_amount = 0;
            $scope.updateContainers();
        };

        $scope.updateContainers = function () {
            angular.forEach($scope.containers, function (container) {
                console.log("updateContainers",container);
                container.total_weight = 0;
                container.total_amount = 0;
                container.total_discount = 0;
                container.total_quantity = 0;
                container.total = 0;

                var city;
                if ($scope.receiver.city_id) {
                    angular.forEach($scope.citiesReceiver, function (item) {
                        if (item.id === $scope.receiver.city_id) {
                            city = item;
                            return true;
                        }
                    });
                }
                angular.forEach(container.order_items, function (product) {
                    if (product.product_id) {
                        var quantity = 0;
                        if (product.product.by_weight === 0) {
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                product.quantity = quantity;
                            }
                        } else {
                            if (!isNaN(product.sub_total_weight) && product.sub_total_weight > 0) {
                                quantity = parseFloat(product.sub_total_weight);
                            }
                        }

                          if (!isNaN(product.quantity) && product.quantity > 0) {
                            container.total_weight += parseFloat(product.product.weight)*quantity;
                        }
                        product.sub_total_goods = parseFloat(product.unit_goods_fee) * quantity;
                        if (!isNaN(product.per_discount) && product.per_discount > 0) {
                            var discount = (parseFloat(product.per_discount) / 100) * product.sub_total_goods;
                            product.sub_total_goods = product.sub_total_goods - discount;
                            container.total_discount += discount;
                        }
                        container.total_amount += product.sub_total_goods;
                        container.total_quantity+=product.quantity;
                        product.total = product.sub_total_goods;
                        container.total += product.total;
                    }
                });
                container.yhl_fee = city.yhl_fee ? parseFloat(city.yhl_fee) : 0;;
                container.total_shipping_fee = container.yhl_fee * parseFloat(container.total_weight);
                container.total += container.total_shipping_fee;
                if(container.coupon_amount > 0){
                    container.total -= container.coupon_amount;
                }
                if(container.total < 0){
                    container.total = 0;
                }
                container.debt = container.total;
                if (!isNaN(container.total_paid_amount) && parseFloat(container.total_paid_amount) > 0) {
                    container.debt = container.total - parseFloat(container.total_paid_amount) > 0 ? container.total - parseFloat(container.total_paid_amount) : 0;
                }
                if (!isNaN(container.debt)) {
                    container.last_paid_amount = container.debt.toFixed(2);
                }
                $scope.updateVolume(container);
            });
        };
        $scope.updatereceiver = function () {
            if ($scope.submittedSearchCustomers) {
                return true;
            }
            var data = {
                receiver    : $scope.receiver,
                order  : $scope.id,
                Updatereciver: $scope.Updatereciver,
            };
            $scope.submitted = true;
            $http({
                method: 'post',
                url: '/admin/yhls/' + $scope.id + "/updatereceiver",
                data: data
            }).then(
                function (resp) {
                   // location.href = '/admin/yhls/' + $scope.id + '/edit';
                    if (resp.status === 200) {
                        $scope.complete=true;
                    };
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                           if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                    $scope.submitted = false;
            });
        };
        $scope.updateYhl = function () {
            if ($scope.submittedSearchCustomers) {
                return true;
            }
            var containers = angular.copy($scope.containers);

            angular.forEach(containers, function (container) {
                container.products = container.order_items.filter(function (product) {
                    return !!product.product_id;
                });
                angular.forEach(container.order_items, function (product) {
                    delete product.product_list;
                });
            });

            var data = {
                customer    : $scope.customer,
                receiver    : $scope.receiver,
                containers  : containers,
                Updatereciver: $scope.Updatereciver,
                Updatecustomer: $scope.Updatecustomer
            };

            $scope.submitted = true;
            $http({
                method: 'PUT',
                url: '/admin/yhls/' + $scope.id + "/update",
                data: data
            }).then(
                function (resp) {
                    location.href = '/admin/yhls/' + $scope.id + '/edit';
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('containers') !== -1) {
                                for (var i = 0; i < containers.length; ++i) {
                                    if (typeof $scope.errors.container[i] === 'undefined') {
                                        $scope.errors.container[i] = {
                                            others: [],
                                            products: []
                                        };
                                    }
                                    if (field.indexOf('containers.' + i) === -1) {
                                        continue;
                                    }
                                    var errorPushed = false;
                                    for (var j = 0; j < containers[i].products.length; ++j) {
                                        if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                            $scope.errors.container[i].products[j] = [];
                                        }
                                        if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                            continue;
                                        }
                                        $scope.errors.container[i].products[j].push(error[0]);
                                        errorPushed = true;
                                    }
                                    if (!errorPushed) {
                                        $scope.errors.container[i].others.push(error[0]);
                                    }
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                    $scope.submitted = false;
            });
        };
$scope.CheckemailCus =function()
{
   if($scope.customer.email === null)
    {
      $scope.customemailedit=false;
    }
    else
    {
        $scope.customemailedit=true;
    }
};
$scope.Editreceiver = function ()
{
    $scope.Updatecustomer=false;
    $scope.Updatereciver=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
    $scope.CheckemailCus();
};
$scope.EditCustomer = function ()
{
    $scope.Updatecustomer=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
    $scope.CheckemailCus();
};
$scope.LockContact = function ()
{
    $scope.Updatereciver=true;
    $scope.Updatecustomer=true;
    $scope.SaveContact=false;
    $scope.contact_save=true;
    $scope.customemailedit=true;
};
$scope.FailContact = function ()
{
    $scope.Updatereciver=false;
    $scope.Updatecustomer=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
    $scope.customemailedit=false;
};
$scope.Editreceiver = function ()
{
    $scope.Updatereciver=false;
    $scope.SaveContact=true;
    $scope.contact_save=false;
};
    });
}());
