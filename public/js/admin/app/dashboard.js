(function () {
    'use strict';

    var app = angular.module('DashboardApp', []);
    app.controller('DashboardController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.transport = {
            order: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            },
            quote: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            },
            orderQuote: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            }
        };

        $scope.yhl = {
            order: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            },
            quote: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            }
        };

        $scope.express = {
            order: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            },
            quote: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            }
        };

        $scope.mts = {
            order: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            },
            quote: {
                create: 0,
                processing: 0,
                cancel: 0,
                completed: 0
            }
        };

        $scope.quote = {
            create: 0,
            processing: 0,
            cancel: 0,
            completed: 0
        };

        $scope.getTransport = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/transport-agency-order'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/transport-customer-quote'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/transport-customer-order'
                })
            ]).then(function (resp) {
                $scope.transport.order = resp[0].data;
                $scope.transport.quote = resp[1].data;
                $scope.transport.orderQuote = resp[2].data;
            });
        };

        $scope.getYhl = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/yhl-agency-order'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/yhl-customer-order'
                })
            ]).then(function (resp) {
                $scope.yhl.order = resp[0].data;
                $scope.yhl.quote = resp[1].data;
            });
        };

        $scope.getExpress = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/express-agency-order'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/express-customer-order'
                })
            ]).then(function (resp) {
                $scope.express.order = resp[0].data;
                $scope.express.quote = resp[1].data;
            });
        };

        $scope.getMts = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/mts-agency-order'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/mts-customer-order'
                })
            ]).then(function (resp) {
                $scope.mts.order = resp[0].data;
                $scope.mts.quote = resp[1].data;
            });
        };

        $scope.getQuote = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/quote-shop'
                })
            ]).then(function (resp) {
                $scope.quote = resp[0].data;
            });
        };
        
        $scope.init = function () {
            $scope.getTransport();
            $scope.getYhl();
            $scope.getExpress();
            $scope.getMts();
            $scope.getQuote();
        };
    });
}());