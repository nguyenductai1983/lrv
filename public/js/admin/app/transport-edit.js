(function () {
    'use strict';

    var app = angular.module('TransportApp', []);
    var body_weight = 139;
    app.controller('TransportCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.update = false;
        $scope.submittedSearchCustomers = false;
        $scope.isEditDiscount = false;
        $scope.isDiscountCustom = false;
        $scope.showMoreInfoCustomer = false;
        $scope.isUseCoupon = false;
        $scope.Updatereciver = true;
        $scope.Updatecustomer = true;
        $scope.ward = true;
        $scope.cod_type = false;
        $scope.initErrors = function () {
            $scope.errors = {
                customer: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();
        $scope.countries = [];
        $scope.products = [];
        $scope.product_list = [];
        $scope.methods = [];
        $scope.isCrrCheckbox = false;

        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group: 1,
            telephone: null,
            code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };
        $scope.EditCustomer = function () {
            $scope.Updatecustomer = false;
        };
        $scope.Editreceiver = function () {
            $scope.Updatereciver = false;
        };
        $scope.initCustomer = function () {
            $scope.provincesCustomer = [];
            $scope.citiesCustomer = [];
            $scope.customer = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                email: null,
                telephone: null,
                cellphone: null,
                postal_code: null,
                city_id: null,
                province_id: null,
                country_id: null,
                id_card: null,
                card_expire: null,
                birthday: null,
                career: null,
                image_1_file_id: null,
                image_2_file_id: null,
                image_3_file_id: null
            };
        };
        $scope.initCustomer();

        $scope.$watchCollection('customer.image_1_file_id', function (newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.provincesReceiver = [];
        $scope.citiesReceiver = [];
        $scope.receiver = {
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            post_code: null,
            city_id: null,
            ward_id: null,
            province_id: null,
            country_id: null
        };

        $scope.initContainer = function (container) {
            if (!container) {
                container = {
                    order_items: [],
                    length: null,
                    width: null,
                    height: null,
                    volume: 0,
                    shipping_fee: 0,
                    total_weight: 0,
                    total_shipping_fee: 0,
                    total_declare_price: 0,
                    total_surcharge_fee: 0,
                    total_insurance_fee: 0,
                    total_goods_fee: 0,
                    total_discount: 0,
                    total_final: 0,
                    total_fee: 0,
                    total_charge_fee: 0,
                    min_fee: 0,
                    total: 0,
                    date_pay: null,
                    total_paid_amount: 0,
                    total_remain_amount: 0,
                    last_paid_amount: 0,
                    last_date_pay: null,
                    currency_id: 1,
                    pay_method: 1,
                    receiver_status: 1,
                    coupon_code: '',
                    coupon_amount: 0,
                    shipping_status: null,
                    danger: 0, //them dong 02-07-2021
                    cod_type: null //them dong 12-07-2021
                };
            }

            for (var i = 0; i < 4; ++i) {
                var product = container.order_items[i];

                if (!product) {
                    product = {
                        code: null,
                        name: null,
                        by_weight: null,
                        quantity: null,
                        per_discount: null,
                        discount: null,
                        unit: null,
                        amount: null,
                        sub_total_weight: 0,
                        sub_total_shipping_fee: 0,
                        sub_total_declare_price: 0,
                        sub_total_surcharge_fee: 0,
                        sub_total_insurance_fee: 0,
                        sub_total_goods_fee: 0,
                        sub_total_discount: 0,
                        sub_total_final: 0,
                        total: null,
                        is_insurance: false,
                        customer_note: null,
                        user_note: null,
                        danger: 0, //them dong 02-07-2021
                        limit_pcs:0, //them dong 07-07-2021
                        check_weight:false,//them dong 07-07-2021
                        messure: {
                            code: null
                        },
                        product: {}
                    };
                    container.order_items.push(product);
                } else {
                    if (product.is_insurance > 0)
                        container.order_items[i].is_insurance = true;
                }
            }
            return container;
        };

        $scope.containers = [];
        $scope.transport = null;

        $scope.getTransport = function () {
            $http({
                method: 'get',
                url: '/admin/transports/' + $scope.id
            }).then(
                function (resp) {
                    $scope.transport = resp.data.transport;
                    $scope.transport.coupon_amount = parseFloat($scope.transport.coupon_amount);
                    if ($scope.transport.coupon_amount > 0) {
                        $scope.isUseCoupon = true;
                    } else {
                        $scope.transport.coupon_amount = 0;
                    }
                    if ($scope.transport.order_status === 1) {
                        $scope.update = true;
                    }
                    $scope.customer = $scope.transport.customer;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="customer.image_' + i +
                            '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if ($scope.customer['image_' + i + '_file_id']) {
                            img = $scope.customer['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        } else {
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.receiver = $scope.transport.receiver;
                    $scope.selectedId = $scope.transport.receiver.id;
                    $scope.customerSearch.code = $scope.customer.code;
                    $scope.isCrrCheckbox = $scope.transport.is_crr ? true : false;
                    $scope.addressSearch.customer_id = $scope.customer.id;
                    $scope.minFee = $scope.transport.minFee;
                    $scope.agency = $scope.transport.agency_id;
                    if ($scope.transport.agency_id === undefined) {
                        $scope.agency = $scope.transport.agency;
                    }
                    $scope.searchAddress();
                    $scope.init();
                    $scope.checkInfoCustomer();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/products?service=1&status=1&agency=' + $scope.agency
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.customer.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.customer.country_id + '&province_id=' +
                        $scope.customer.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                        $scope.receiver.province_id
                }),
                //them ngay 24-04-2020
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.receiver.city_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/payment-methods?type=1'
                })
            ]).then(function (resp) {
                $scope.product_list = resp[0].data.products;
                $scope.countries = resp[1].data.countries;
                $scope.provincesCustomer = resp[2].data.provinces;
                $scope.citiesCustomer = resp[3].data.cities;
                $scope.provincesReceiver = resp[4].data.provinces;
                $scope.citiesReceiver = resp[5].data.cities;
                //them ngay 24-04-2020
                $scope.wardsReceiver = resp[6].data.wards;
                $scope.methods = resp[7].data.methods;
                //angular.forEach($scope.transport.containers, function (container) {
                //});
                $scope.containers.push($scope.initContainer($scope.transport));
                $scope.updateContainers();

                setTimeout(function () {
                    setDatePicker();
                });
            });
            language = JSON.parse(language);
        };
        $scope.getProvincesCustomer = function () {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.customer.country_id
            }).then(
                function (resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function () {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.customer.country_id + '&province_id=' +
                    $scope.customer.province_id
            }).then(
                function (resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustomer = function () {
            angular.forEach($scope.citiesCustomer, function (value, key) {
                if (value.id == $scope.customer.city_id) {
                    $scope.customer.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function () {
            angular.forEach($scope.citiesReceiver, function (value, key) {
                if (value.id == $scope.receiver.city_id) {
                    $scope.receiver.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesReceiver = function () {
            if ($scope.receiver.country_id === 91) {
                $scope.ward = false;
            } else {
                $scope.ward = true;
            }
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                function (resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                    $scope.receiver.province_id
            }).then(
                function (resp) {
                    $scope.citiesReceiver = resp.data.cities;
                    $scope.getWardsReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.getWardsReceiver = function () {
            if ($scope.receiver.country_id === 91) {
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.receiver.city_id
                }).then(
                    function (resp) {
                        $scope.wardsReceiver = resp.data.wards;
                    },
                    function (resp) {
                        console.log(resp);
                        console.log(resp.statusText);
                    }
                );
            }
        };

        $scope.searchCustomers = function () {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function (resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchCustomers = false;
            });
        };

        $scope.addressSearchResult = [];
        $scope.addressSearch = {
            customer_id: null,
            phone_number: null,
            postal_code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };
        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url: '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    if ($scope.receiver.country_id === 91) {
                        $scope.ward = false;
                    } else {
                        $scope.ward = true;
                    }
                    // $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                $scope.submittedSearchAddress = false;
            });
        };
        $scope.selectedId = 0;
        $scope.selectAddress = function (address) {
            if (address == 0) {
                $scope.provincesReceiver = [];
                $scope.citiesReceiver = [];
                $scope.receiver = {
                    id: null,
                    first_name: null,
                    middle_name: null,
                    last_name: null,
                    address_1: null,
                    address_2: null,
                    telephone: null,
                    cellphone: null,
                    post_code: null,
                    city_id: null,
                    province_id: null,
                    country_id: null
                };
            }
            if (address) {
                $scope.receiver = address;
            }
            $scope.getProvincesReceiver();
            $scope.updateContainers();
        };
        $scope.selectCustomer = function (customer) {
            $scope.customer = customer;

            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="customer.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if (customer['image' + i].hasOwnProperty('path')) {
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                } else {
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }

            $scope.getProvincesCustomer();

            angular.element('#modal-customers-search-result').modal('hide');
        };

        $scope.removeCustomer = function () {
            $scope.initCustomer();

            for (var i = 1; i <= 3; ++i) {
                var imgControl = angular.element('input[name="customer.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0),
                    imgPreview = imgControl.find('.img-preview');

                imgControl.find('.img-thumbnail').attr('src', '#');
                imgPreview.hide();
            }
        };

        $scope.setActiveContainerTab = function () {
            setTimeout(function () {
                var container = angular.element('#container');
                container.find('.nav-tabs li.tab').removeClass('active');
                container.find('.nav-tabs li.tab:last').addClass('active');
                container.find('.tab-content .tab-pane').removeClass('active');
                container.find('.tab-content .tab-pane:last').addClass('active');
            });
        };

        $scope.addContainer = function () {
            $scope.containers.push($scope.initContainer());

            setTimeout(function () {
                setDatePicker();
            });

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function (index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function () {
                $scope.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function (product, idDropdown, indexcontainer) {
            var code = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);
            // tao danh sach hang hoa bang chu hoa
            angular.forEach($scope.product_list, function (item) {
                item.show = (item.code.toLowerCase().indexOf(code) !== -1);
            });
            // xu ly them loc san pham
            // nạp danh sách sản phẩm đầy đủ
            // xu ly them loc san pham
            if (indexcontainer === undefined) {
                indexcontainer = 0;
            }
            // nạp danh sách sản phẩm đầy đủ
            var products = $scope.product_list;
            let danger = $scope.containers[indexcontainer].danger;
            // kiểm tra và lọc danh sach
            if (danger > 0) {
                if (danger === 1) {
                    // products = $scope.product_list.filter(word => word.danger !== 2);
                    products = $scope.product_list.filter(function (x) {
                        return x.danger !== 2
                    });

                }
                else if (danger === 2) {
                    // products = $scope.product_list.filter(word => word.danger !== 1);
                    products = $scope.product_list.filter(function (x) {
                        return x.danger !== 1
                    });
                }

            }
            //hết xử lý lọc sản phẩm
            $scope.products = products.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.sub_total_surcharge_fee = 0;
            product.sub_total_discount = 0;
            product.sub_total_insurrance_fee = 0;
            product.unit_goods_fee = parseFloat(item.sale_price) + parseFloat(item.pickup_fee);
            product.quantity = 1;
            product.sub_total_weight = item.weight;
            product.messure.code = item.unit;
            product.product = item;
            product.product_id = item.id;
            product.pickup_fee = item.pickup_fee;
            product.danger = item.danger;//them dong 02-07-2021
            $scope.updateContainers();
        };

        $scope.updateVolume = function (container) {
            if (!isNaN(container.length) && container.length > 0 &&
                !isNaN(container.width) && container.width > 0 &&
                !isNaN(container.height) && container.height > 0) {
                container.volume = (parseFloat(container.length) * parseFloat(container.width) *
                    parseFloat(container.height)) / body_weight;

            } else {
                container.volume = 0;
            }
        };

        $scope.setVolume = function (container) {
            if (container.volume <= container.total_weight) {
                return false;
            }
            angular.forEach(container.order_items, function (order_item) {
                if (order_item.product_id) {
                    if (order_item.sub_total_weight > 0) {
                        order_item.sub_total_weight = (container.volume * (parseFloat(order_item.sub_total_weight) / container.total_weight)).toFixed(2);
                    } else {
                        order_item.sub_total_weight = 0;
                    }
                }
            });
            $scope.updateContainers();
        };
        $scope.stringToDate = function (_date, _format, _delimiter) {
            if (_date != undefined) {
                var formatLowerCase = _format.toLowerCase();
                var formatItems = formatLowerCase.split(_delimiter);
                var dateItems = _date.split(_delimiter);
                var monthIndex = formatItems.indexOf("mm");
                var dayIndex = formatItems.indexOf("dd");
                var yearIndex = formatItems.indexOf("yyyy");
                var month = parseInt(dateItems[monthIndex]);
                month -= 1;
                var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
                return formatedDate;
            }
        }
        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function (customer) {
            // $scope.errors.customer=null;
            $scope.errors.customer.id_card_required = false;
            $scope.errors.customer.birthday = false;
            $scope.errors.customer.card_expire = false;
            $scope.errors.customer.career = false;
            if (!customer.id_card) {
                $scope.errors.customer.id_card_required = true;
                return false;
            }
            if (customer.card_expire != undefined) {
                var cardTime = $scope.stringToDate(customer.card_expire, 'dd/MM/yyyy', "/").getTime();
                var currentTime = new Date().getTime();
                if (cardTime < currentTime) {
                    $scope.errors.customer.card_expire = true;
                    return false;
                }
            }
            else {
                $scope.errors.customer.card_expire = true;
            }
            if (!customer.birthday) {
                $scope.errors.customer.birthday = true;
                return false;
            }
            if (!customer.career) {
                $scope.errors.customer.career = true;
                return false;
            }
            return true;
        };
        $scope.checkInfoCustomer = function () {
            $scope.isEditDiscount = true;
            $scope.isDiscountCustom = false;
            if (!$scope.isAvailableIdCard($scope.customer)) {
                $scope.isEditDiscount = false;
            }
            else if ($scope.customer.image_1_file_id || $scope.customer.image_2_file_id || $scope.customer.image_3_file_id) {
                $scope.isDiscountCustom = true;
            }
            $scope.updateContainers();
        };

        $scope.remove = function (p_index, index) {
            $scope.containers[p_index].products.splice(index, 1);
            $scope.updateContainers();
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function (address_id) {
            var address = 0;
            if ($scope.addressSearchResult.length > 0 && address_id > 0) {
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.showPopupVip = function () {
            popup.msg(language.notify_admin_vip);
        };

        $scope.applyCoupon = function (container) {
            var data = {
                customer: $scope.customer,
                container: container
            };
            $http({
                method: 'POST',
                url: '/api/admin/coupon',
                data: data
            }).then(
                function (resp) {
                    var amount = parseFloat(resp.data.amount);
                    if (amount > 0) {
                        $scope.isUseCoupon = true;
                        container.coupon_amount = amount;
                        $scope.updateContainers();
                    } else {
                        container.coupon_code = '';
                    }
                }
            );
        };

        $scope.removeCoupon = function (container) {
            $scope.isUseCoupon = false;
            container.coupon_code = '';
            container.coupon_amount = 0;
            $scope.updateContainers();
        };

        $scope.editInfoCustomer = function (token) {
            fly.ajax({
                service: '/api/admin/' + $scope.customer.id + '/customer',
                method: "GET",
                loading: true,
                success: function (result) {
                    if (result.success) {
                        popup.open('edit-info-customer', language.edit_info_customer, template('customer/edit_info_customer.tpl', {
                            data: result.data,
                            _token: token
                        }), [{
                            title: language.popup_confirm,
                            style: 'btn-primary',
                            fn: function () {
                                fly.submit({
                                    id: 'update-info-customer',
                                    service: '/api/admin/update-info-customer',
                                    success: function (result) {
                                        popup.close('edit-info-customer');
                                        if (result.success) {
                                            location.reload();
                                        } else {
                                            popup.msg(result.message);
                                        }
                                    }
                                });
                            }
                        },
                        {
                            title: language.popup_btn_close,
                            style: 'btn-default',
                            fn: function () {
                                popup.close('edit-info-customer');
                            }
                        }
                        ], 'modal-lg');
                    } else {
                        popup.msg(result.message);
                    }
                }
            });
        };
        $scope.editInfoReceiver = function (token) {
            fly.ajax({
                service: '/api/admin/' + $scope.receiver.id + '/receiver',
                method: "GET",
                loading: true,
                success: function (result) {
                    if (result.success) {
                        popup.open('edit-info-receiver', language.edit_info_customer, template('customer/edit_info_receiver.tpl', {
                            data: result.data,
                            order_id: $scope.id,
                            _token: token
                        }), [{
                            title: language.popup_confirm,
                            style: 'btn-primary',
                            fn: function () {
                                fly.submit({
                                    id: 'update-info-receiver',
                                    service: '/api/admin/update-info-receiver',
                                    success: function (result) {
                                        popup.close('edit-info-receiver');
                                        if (result.success) {
                                            location.reload();
                                        } else {
                                            popup.msg(result.message);
                                        }
                                    }
                                });
                            }
                        },
                        {
                            title: language.popup_btn_close,
                            style: 'btn-default',
                            fn: function () {
                                popup.close('edit-info-receiver');
                            }
                        }
                        ], 'modal-lg');
                    } else {
                        popup.msg(result.message);
                    }
                }
            });
        };
        $scope.updateContainers = function (isPostCode) {
            if (isPostCode) {
                $scope.getPostCodeReceiver();
            }
            angular.forEach($scope.containers, function (container) {
                container.total_shipping_fee = 0;
                container.total_weight = 0;
                container.total_declared_value = 0;
                container.total_surcharge = 0;
                container.total_insurance = 0;
                container.total_amount = 0;
                container.total_discount = 0;
                container.total_fee = 0;
                container.total_charge_fee = 0;
                container.min_fee = 0;
                container.total = 0;
                container.danger = 0; //them ngay 02-07-2021
                var city;
                if ($scope.receiver.city_id) {
                    angular.forEach($scope.citiesReceiver, function (item) {
                        if (item.id === $scope.receiver.city_id) {
                            city = item;
                            return true;
                        }
                    });
                }
                angular.forEach(container.order_items, function (product) {
                    if (product.product_id) {
                        var quantity = 0;
                        if (product.product.by_weight === 0) {
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                product.quantity = quantity;
                            }
                        } else {
                            if (!isNaN(product.sub_total_weight) && product.sub_total_weight > 0) {
                                quantity = parseFloat(product.sub_total_weight);
                            }
                        }
                        // gioi han so luong can nang
                        product.check_weight=false;
                        if (product.sub_total_weight > product.product.limit_pcs) {
                            product.sub_total_weight = product.product.limit_pcs;
                            product.check_weight=true;
                        }
                        // het gioi han so luong can nang

                        if (!isNaN(product.sub_total_weight) && product.sub_total_weight > 0) {
                            container.total_weight += parseFloat(product.sub_total_weight);
                        }
                        product.sub_total_goods = Math.round((parseFloat(product.unit_goods_fee) * quantity)*1000)/1000;
                        product.sub_total_goods = Math.round(product.sub_total_goods*100)/100;
                        if ($scope.isEditDiscount && !isNaN(product.per_discount) && product.per_discount > 0) {
                            var per_discount = parseFloat(product.per_discount);
                            var max_custom = parseFloat($scope.discountLevel.max_custom);
                            var max_base = parseFloat($scope.discountLevel.max_base);
                            if ($scope.isDiscountCustom) {
                                if (per_discount > max_custom) {
                                    product.per_discount = max_custom;
                                }
                            } else {
                                if (per_discount > max_base) {
                                    product.per_discount = max_base;
                                }
                            }
                            var discount = (product.per_discount / 100) * product.sub_total_goods;
                            product.sub_total_goods = product.sub_total_goods - discount;
                            container.total_discount += discount;
                        }
                        container.total_amount += product.sub_total_goods;

                        product.sub_total_surcharge_fee = 0;
                        product.sub_total_insurrance_fee = 0;

                        if (!isNaN(product.sub_total_declare_price) && product.sub_total_declare_price > 0) {
                            if (quantity > 0) {
                                var quota = product.sub_total_declare_price / quantity;
                                if (quota > $scope.configSurcharge.quota) {
                                    product.sub_total_surcharge_fee = parseFloat(product.sub_total_declare_price) * $scope.configSurcharge.percent / 100;
                                }
                            }
                            container.total_surcharge += parseFloat(product.sub_total_surcharge_fee);
                            container.total_declared_value += parseFloat(product.sub_total_declare_price);
                            if (product.is_insurance) {
                                product.sub_total_insurrance_fee = parseFloat(product.sub_total_declare_price) * $scope.configInsurance.percent / 100;
                                container.total_insurance += parseFloat(product.sub_total_insurrance_fee);
                            }
                        }
                        product.total = parseFloat(parseFloat(product.sub_total_goods) + parseFloat(product.sub_total_surcharge_fee) + parseFloat(product.sub_total_insurrance_fee));
                        // Chỉ cộng thêm phần giá sản phẩm
                        container.total += product.sub_total_goods;
                        //kiem tra co hang nguy hiem
                        if (product.danger === 1) {
                            container.danger = 1;
                        }
                        else if (product.danger === 2) {
                            container.danger = 2;
                        }
                        // het kiem tra hang nguy hiểm
                    }
                });
                container.shipping_fee = city && city.shipping_fee ? parseFloat(city.shipping_fee) : 0;
                container.shipping_fee = parseFloat(container.shipping_fee);
                container.total_shipping_fee = container.shipping_fee * parseFloat(container.total_weight);
                container.total_fee = container.total_shipping_fee + container.total_surcharge + container.total_insurance + container.total_amount;
                var old_min_fee = container.total_shipping_fee + container.total_amount;
                container.min_fee = $scope.getMinFee(old_min_fee);
                if (container.min_fee > old_min_fee) {
                    container.total_charge_fee = container.min_fee;
                } else {
                    container.total_charge_fee = old_min_fee;
                }
                container.total = container.total_charge_fee + container.total_surcharge + container.total_insurance;

                if (container.coupon_amount > 0) {
                    container.total -= container.coupon_amount;
                }
                if (container.total < 0) {
                    container.total = 0;
                }
                if(!container.cod_type)
                {
                    container.last_paid_amount = parseFloat(container.total) - parseFloat(container.total_paid_amount);
                    container.last_paid_amount = container.last_paid_amount.toFixed(2);
                    container.debt = container.total - container.last_paid_amount - parseFloat(container.total_paid_amount);
                }
                else
                {
                container.debt = container.total  - parseFloat(container.total_paid_amount);
                }
               // container.debt = 555 ;// container.debt.toFixed(2);
                // container.last_paid_amount = 0;
                // if (!isNaN(container.total_paid_amount) && parseFloat(container.total_paid_amount) > 0) {
                //     container.debt = container.total - parseFloat(container.total_paid_amount);
                // }
                // if (!isNaN(container.debt)) {
                //     container.last_paid_amount = container.debt.toFixed(2);
                // }
                $scope.updateVolume(container);
            });
        };
        $scope.updatePaid = function () {
            angular.forEach($scope.containers, function (container) {
                container.debt = -((parseFloat(container.total_paid_amount) - parseFloat(container.total)) + parseFloat(container.last_paid_amount));
            });
        };
        //Bsung mới tính toán giá trị min fee
        $scope.getMinFee = function () {
            let vnPriorityFlag = false;
            let minFee = 25;
            if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (vnPriorityFlag) {
                minFee = $scope.minFee;
            } else {
                minFee = $scope.vn_hcm_priority + $scope.minFee;
            }
            return minFee;
        };
        $scope.editCustomer = function () {
            $scope.customeredit = false;
        }
        $scope.saveCustomer = function () {
            var data = $scope.customer;

            $http({
                method: 'PUT',
                url: '/admin/transports/' + $scope.customer.id + "/updatecustomer",
                data: data
            }).then(
                function (resp) {
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });
        }
        $scope.remove = function (p_index, index) {
            $scope.containers[p_index].order_items.splice(index, 1);
            $scope.updateContainers();
        };
        $scope.addproduct = function (index) {
            var product = {
                product_id: null,
                code: null,
                name: null,
                by_weight: null,
                quantity: null,
                weight: 0,
                price: null,
                per_discount: null,
                discount: null,
                unit: null,
                amount: null,
                declared_value: null,
                surcharge: null,
                is_insurance: false,
                insurance: null,
                total: null,
                note: null,
                description: null,
                messure: { code: null },
                danger: 0,
                limit_pcs:0, //them dong 07-07-2021
                check_weight:false, //them dong 07-07-2021
                products: $scope.order_items
            };
            $scope.containers[index].order_items.push(product);
        };
        $scope.checkvolume = function () {
            $scope.updatevolume = true;
            angular.forEach($scope.containers, function (container) {
                if (container.volume > 0) {
                    if (parseFloat(container.volume, 2) > parseFloat(container.total_weight, 2)) {
                        $scope.updatevolume = false;
                        popup.confirmChange(language.msg_weight_volume, function () {
                            $scope.setVolume(container);
                            $scope.updatevolume = true;
                        });
                    }
                } else {
                    popup.msg(language.weight_volume_none);
                }
            });
        };
        $scope.updateTransport = function () {
            $scope.checkvolume();
            if ($scope.updatevolume) {

                if ($scope.submittedSearchCustomers) {
                    return true;
                }
                var containers = angular.copy($scope.containers);

                angular.forEach(containers, function (container) {
                    container.products = container.order_items.filter(function (product) {
                        return !!product.product_id;
                    });
                    angular.forEach(container.order_items, function (product) {
                        if (!$scope.isEditDiscount) {
                            product.discount = null;
                        }
                        delete product.product_list;
                    });
                });

                var data = {
                    customer: $scope.customer,
                    receiver: $scope.receiver,
                    containers: containers,
                    is_crr: $scope.isCrrCheckbox,
                    Updatereciver: $scope.Updatereciver,
                    Updatecustomer: $scope.Updatecustomer,
                };
                $scope.submitted = true;
                $http({
                    method: 'PUT',
                    url: '/admin/transports/' + $scope.id + "/update",
                    data: data
                }).then(
                    function (resp) {
                        location.href = '/admin/transports/' + $scope.id + '/edit';
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.errors = null;
                            $scope.initErrors();
                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('customer') !== -1) {
                                    $scope.errors.customer.push(error[0]);
                                } else if (field.indexOf('receiver') !== -1) {
                                    $scope.errors.receiver.push(error[0]);
                                } else if (field.indexOf('containers') !== -1) {
                                    for (var i = 0; i < containers.length; ++i) {
                                        if (typeof $scope.errors.container[i] === 'undefined') {
                                            $scope.errors.container[i] = {
                                                others: [],
                                                products: []
                                            };
                                        }
                                        if (field.indexOf('containers.' + i) === -1) {
                                            continue;
                                        }
                                        var errorPushed = false;
                                        for (var j = 0; j < containers[i].products.length; ++j) {
                                            if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                                $scope.errors.container[i].products[j] = [];
                                            }
                                            if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                                continue;
                                            }
                                            $scope.errors.container[i].products[j].push(error[0]);
                                            errorPushed = true;
                                        }
                                        if (!errorPushed) {
                                            $scope.errors.container[i].others.push(error[0]);
                                        }
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                        }
                        if (resp.status === 500) {
                            $scope.errors = null;
                            $scope.initErrors();
                            $scope.errors.system = [];
                            $scope.errors.system.push(resp.data);
                            angular.element('#modal-errors').modal('show');
                        }
                    }
                ).finally(function () {
                    $scope.submitted = false;
                });
            };
        };
        //het hàm update
    });

}());
