(function() {
    'use strict';

    var app = angular.module('MtsApp', []);

    app.controller('MtsEditController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.bntConfirm = true;
        $scope.submitted = false;
        $scope.submittedSearchCustomers = false;
        $scope.showThirdPerson = false;
        $scope.mts = null;
        $scope.Updatecustomer = false;
        $scope.Updatereciver = false;
        $scope.ward = true;
        $scope.flag_last = false;
        $scope.initErrors = function() {
            $scope.errors = {
                customer: [],
                receiver: [],
                thirdperson: [],
                employer: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.mts_code = '';

        $scope.customersSearchResult = [];
        $scope.customerSearch = {
            group: null,
            telephone: null,
            code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };

        $scope.addressesSearchResult = [];
        $scope.addressSearch = {
            customer_id: null,
            phone_number: null,
            postal_code: null,
            first_name: null,
            middle_name: null,
            last_name: null
        };

        $scope.initCustomer = function() {
            $scope.provincesCustomer = [];
            $scope.citiesCustomer = [];
            $scope.customer = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                email: null,
                telephone: null,
                cellphone: null,
                postal_code: null,
                city_id: null,
                province_id: null,
                country_id: null,
                id_card: null,
                card_expire: null,
                birthday: null,
                career: null,
                image_1_file_id: null,
                image_2_file_id: null,
                image_3_file_id: null
            };
        };

        $scope.initAddress = function() {
            $scope.provincesAddress = [];
            $scope.citiesAddress = [];
            $scope.address = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                phone_number: null,
                postal_code: null,
                ward_id: null,
                city_id: null,
                province_id: null,
                country_id: null,
                relationship: null,
            };
        };

        $scope.initContainer = function() {
            $scope.container = {
                id: null,
                thirdperson_name: null,
                thirdperson_tel: null,
                thirdperson_address: null,
                thirdperson_message: null,
                employer_name: null,
                employer_tel: null,
                employer_address: null,
                employer_source_of_fund: null,
                reason: null,
                pay_date: $scope.date_now,
                status: 1,
                transaction_type: 1,
                payment_by: 1,
                amount: 0,
                discount_type: 1,
                discount_number: 0,
                transfer_fee: 0,
                total: 0,
                relationship: null,
                bank_name: null,
                bank_account: null
            };
        };
        $scope.Editreceiver = function() {
            $scope.Updatereciver = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
        };
        $scope.EditCustomer = function() {
            $scope.Updatecustomer = false;
            $scope.SaveContact = true;
            $scope.contact_save = false;
            $scope.CheckemailCus();
        };
        $scope.CheckemailCus = function() {
            if ($scope.customer.email === null) {
                $scope.customemailedit = false;
            } else {
                $scope.customemailedit = true;
            }
        };
        $scope.initAddress();
        $scope.initCustomer();
        $scope.initContainer();

        $scope.$watchCollection('customer.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;

            if (!$scope.customer.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.card_expire) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.customer.image_1_file_id) {
                $scope.isEditDiscount = false;
            }

        };

        $scope.getMts = function() {
            $http({
                method: 'get',
                url: '/admin/mts/' + $scope.id
            }).then(
                function(resp) {
                    $scope.mts = resp.data.mts;
                    $scope.customer = $scope.mts.customer;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="customer.image_' + i +
                            '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if ($scope.customer['image' + i]) {
                            img = $scope.customer['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        } else {
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.receiver = $scope.mts.receiver;
                    $scope.selectedId = $scope.mts.receiver.id;
                    $scope.customerSearch.code = $scope.customer.code;
                    $scope.mts_code = $scope.mts.code;
                    $scope.addressSearch.customer_id = $scope.customer.id;
                    $scope.searchAddress();
                    $scope.Updatecustomer = true;
                    $scope.Updatereciver = true;
                    $scope.init();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };


        $scope.init = function() {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/admin/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.customer.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.customer.country_id + '&province_id=' +
                        $scope.customer.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                        $scope.receiver.province_id
                }),
                //them ngay 24-04-2020
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.receiver.city_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/currencies'
                })
            ]).then(function(resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesCustomer = resp[1].data.provinces;
                $scope.citiesCustomer = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.citiesReceiver = resp[4].data.cities;
                $scope.wardsReceiver = resp[5].data.wards;
                $scope.currencies = resp[6].data.currencies;
                if ($scope.mts.mts_status == 4 || $scope.mts.mts_status == 3) {
                    $scope.bntConfirm = false;
                }
                $scope.updateContainer($scope.mts);
            });
        };

        $scope.getProvincesCustomer = function(customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + country_id
            }).then(
                function(resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer(country_id)
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function(customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.customer.country_id;
            var province_id = customer && customer.province_id ? customer.province_id : $scope.customer.province_id;
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + country_id + '&province_id=' + province_id
            }).then(
                function(resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.getWardsReceiver = function() {

            if ($scope.address.country_id === 91) {
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.address.city_id
                }).then(
                    function(resp) {
                        $scope.wardsReceiver = resp.data.wards;
                    },
                    function(resp) {
                        console.log(resp);
                        console.log(resp.statusText);
                    }
                );
            }
        };
        $scope.getWardsReceiver = function() {
            if ($scope.address.country_id === 91) {
                $http({
                    method: 'get',
                    url: '/api/admin/wards?city_id=' + $scope.address.city_id
                }).then(
                    function(resp) {
                        $scope.wardsReceiver = resp.data.wards;
                    },
                    function(resp) {
                        console.log(resp);
                        console.log(resp.statusText);
                    }
                );
            }
        };
        $scope.getCitiesReceiver = function() {
            if ($scope.receiver.country_id === 91) {
                $scope.ward = false;
            } else {
                $scope.ward = true;
            }
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.address.country_id
            }).then(
                function(resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    // $scope.getProvincesReceiver();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getProvincesReceiver = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.address.country_id + '&province_id=' +
                    $scope.address.province_id
            }).then(
                function(resp) {
                    $scope.citiesReceiver = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
            if ($scope.flag_last) {
                var container = $scope.container;
                $scope.updateFee(container);
            }
            $scope.flag_last = true;
        };

        $scope.searchCustomers = function() {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function(resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchCustomers = false;
            });
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = 0;
        $scope.searchAddress = function() {

            $http({
                method: 'get',
                url: '/api/admin/addresses?' + $httpParamSerializer($scope.addressSearch)
            }).then(
                function(resp) {
                    $scope.addressSearchResult = resp.data.addresses.data;
                    $scope.selectedId = $scope.receiver.id;
                    $scope.selectAddress($scope.receiver);
                    $scope.getCitiesReceiver();

                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchAddress = false;
            });
        };

        $scope.selectCustomer = function(customer) {
            $scope.customer = customer;
            $scope.addressSearch.customer_id = customer.id;
            for (var i = 1; i <= 3; ++i) {
                var img = '';
                var imgControl = angular.element('input[name="customer.image_' + i +
                    '_file_id"]').parents('.img-control').eq(0);
                var imgPreview = imgControl.find('.img-preview');
                if (customer['image' + i].hasOwnProperty('path')) {
                    img = customer['image' + i].path;
                }
                if (!img) {
                    imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                } else {
                    imgControl.find('.img-thumbnail').attr('src', img);
                }
                imgPreview.show();
            }
            $scope.getProvincesCustomer(customer);
            $scope.searchAddress();
            angular.element('#modal-customers-search-result').modal('hide');
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function(address_id) {
            var address = 0;
            if ($scope.addressSearchResult.length > 0 && address_id > 0) {
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        }

        $scope.selectAddress = function(address) {
            if (address) {
                $scope.address = address;
                $scope.selectId = address.id;
            }
            if (address == 0) {
                $scope.removeAddress();
            }
            $scope.getProvincesReceiver();

        };

        $scope.selectedItem = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            email: null,
            phone_number: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };


        $scope.removeCustomer = function() {
            $scope.initCustomer();

            for (var i = 1; i <= 3; ++i) {
                var imgControl = angular.element('input[name="customer.image_' + i +
                        '_file_id"]').parents('.img-control').eq(0),
                    imgPreview = imgControl.find('.img-preview');

                imgControl.find('.img-thumbnail').attr('src', '#');
                imgPreview.hide();
            }
        };

        $scope.removeAddress = function() {
            $scope.initAddress();
        };

        $scope.updateContainer = function(container) {
            $scope.container.id = container.id;
            $scope.container.thirdperson_name = container.third_person_name;
            $scope.container.thirdperson_tel = container.third_person_tel;
            $scope.container.thirdperson_address = container.third_person_address;
            $scope.container.thirdperson_message = container.third_person_message;
            $scope.container.employer_name = container.employer_name;
            $scope.container.employer_tel = container.employer_tel;
            $scope.container.employer_address = container.employer_address;
            $scope.container.employer_source_of_fund = container.employer_source;
            $scope.container.reason = container.reason;
            $scope.container.relationship = container.relationship;
            if (!container.pay_date) {
                $scope.container.pay_date = $scope.date_now;
            } else {
                $scope.container.pay_date = container.pay_date;
            }
            $scope.container.status = container.mts_status;
            $scope.container.transaction_type = container.transaction_type;
            // ngày 21-07-2021
            $scope.container.bank_name = container.bank_name;
            $scope.container.bank_account = container.bank_account;
            // ngày 21-07-2021
            $scope.container.payment_by = container.payment_by;
            $scope.container.amount = container.total_goods;
            $scope.container.discount_type = container.discount_type;
            $scope.container.discount_number = container.discount_number;
            $scope.container.transfer_fee = container.transfer_fee;
            $scope.container.total = container.total_final;
            $scope.container.currency = container.currency_id;
            $scope.container.commission = container.discount_agency_amount;
        };

        //Bsung mới tính toán giá trị per fee
        // $scope.getPerFee = function () {
        //     let vnPriorityFlag = false;
        //     let perFee = $scope.mts_max_per_fee;
        //     if ($scope.vnPriority.indexOf($scope.address.province_id) != -1) {
        //         vnPriorityFlag = true;
        //     }
        //     if (vnPriorityFlag) {
        //         perFee = $scope.mts_per_fee;
        //     }
        //     return perFee;
        // };
        $scope.updateFee = function(container) {
            // lấy giá trị phù hợp nhất
            let result = $scope.MTSSurcharge.filter(word => word.amount <= container.amount);
            result = result[result.length - 1];
            // hết đoạn xử lý chi phí
            var rate_number = result['rate'];
            // let vnPriorityFlag = false;
            // if ($scope.vnPriority.indexOf($scope.address.province_id) != -1) {
            //     vnPriorityFlag = true;
            // }
            // // nếu ngoài thành phố hồ chí minh
            // if (!vnPriorityFlag) {
            //     rate_number = parseFloat(rate_number + 0.5);
            // }
            rate_number = (parseFloat(rate_number) * parseFloat(container.amount)) / 100;
            var fee = result['fee'];
            var surcharge = result['surcharge'] * container.amount / 100;
            var total_fee = rate_number + fee + surcharge;
            var discount_number = parseFloat(container.discount_number) > 0 ? parseFloat(container.discount_number) : 0;
            if (discount_number > 0 && container.discount_type == 1) {
                discount_number = (parseFloat(container.amount) * parseFloat(container.discount_number)) / 100;
            } else if (discount_number > 0 && container.discount_type == 2) {
                discount_number = parseFloat(container.discount_number);
            }
            if (discount_number > total_fee) {
                discount_number = total_fee;
            }
            total_fee = total_fee - discount_number;
            var commission = parseFloat(total_fee) * $scope.commission;
            $scope.container.transfer_fee = total_fee.toFixed(2);
            $scope.container.commission = commission.toFixed(2);
            $scope.container.total = (parseFloat(container.amount) + parseFloat(total_fee)).toFixed(2);
        };
        $scope.checkInfo = function(relationship) {
            var result = '';
            var result3000 = '';
            var resultreason = '';
            var check_card_expire;
            var sign = 0;
            if ($scope.customer.card_expire) {
                var ngay = $scope.customer.card_expire.split('/');
                var varDate = new Date(ngay[2], ngay[1], ngay[0]); //dd-mm-YYYY
                var today = new Date();
                if (varDate >= today) {
                    check_card_expire = true
                }
            }
            if (!$scope.customer.image_1_file_id && !$scope.customer.image_2_file_id && !$scope.customer.image_2_file_id) {
                result3000 += language.image + ', ';
                sign = 1;
            }
            if (!$scope.customer.id_card) {
                result += language.id_card + ', ';
                sign = 2;
            }
            if (!$scope.customer.card_expire) {
                result += language.card_expire + ', ';
                sign = 2;
            }
            if (!$scope.customer.birthday) {
                result += language.birthday + ', ';
                sign = 2;
            }
            if (!$scope.customer.career) {
                result += language.career + ', ';
                sign = 2;
            }
            if ($scope.customer.card_expire && !check_card_expire) {
                result += language.card_expire + ', ';
                sign = 2;
            }
            if (relationship === undefined || relationship === null) {
                result += language.relationship + ', ';
                sign = 2;
            }
            if ($scope.container.reason === undefined || $scope.container.reason === null) {
                resultreason += language.reason + ', ';
                sign = 3;
            }
            return [sign, result, result3000, resultreason];
        };
        $scope.checkInfo_amount = function(amount, relationship) {
            let result = true;
            var cusinfo_check = $scope.checkInfo(relationship)
            if (amount >= 3000 && cusinfo_check[0] >= 1) {
                popup.msg(language.mts_amount3000 + ': ' + cusinfo_check[1] + cusinfo_check[2] + cusinfo_check[3]);
                return false;
            } else if (amount >= 850 && cusinfo_check[0] >= 2) {
                popup.msg(language.mts_amount1000 + ': ' + cusinfo_check[1] + cusinfo_check[3]);
                return false;
            } else if (cusinfo_check[0] >= 3) {
                popup.msg(language.mts_reason + cusinfo_check[3]);
                return false;
            }
            return result;
        }

        $scope.updateMts = function() {
            if ($scope.submittedSearchCustomers) {
                return true;
            }
            var data = {
                customer: $scope.customer,
                receiver: $scope.address,
                container: $scope.container
            };

            $scope.submitted = true;
            $http({
                method: 'PUT',
                url: '/admin/mts/' + $scope.id + "/update",
                data: data
            }).then(
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('container') !== -1) {
                                $scope.errors.container.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 200) {
                        location.href = '/admin/mts';
                    }
                }
            ).finally(function() {
                $scope.submitted = false;
            });
        };
        $scope.select = (function(select_content, option_content) {
            if (select_content === '' || select_content === '0') {
                document.getElementById(option_content).style.visibility = "visible";
                document.getElementById(option_content).focus();
            } else {
                document.getElementById(option_content).style.visibility = "hidden";;
            }
        });
        //het
    });
}());
