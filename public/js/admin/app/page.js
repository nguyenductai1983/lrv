var page = {};

page.remove = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/pages/'+ id +'/destroy',
            method: "POST",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};