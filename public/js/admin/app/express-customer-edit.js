(function() {
    'use strict';

    var app = angular.module('ExpressApp', []);

    app.controller('ExpressEditController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.isEditDiscount = false;
        $scope.submittedSearch = false;
        $scope.submittedCancel = false;
        $scope.submittedApprove = false;
        $scope.submittedShipping = false;
        $scope.submittedPayment = false;
        $scope.bntCancel = false;
        $scope.bntSearch = false;
        $scope.bntConfirm = false;
        $scope.disabled = true;
        $scope.outside_canada = true;
        $scope.initErrors = function() {
            $scope.errors = {
                addressFrom: [],
                addressTo: [],
                packages: [],
                order: [],
                quotes: [],
                custom: []
            };
        };
        $scope.serviceIndex = null;
        $scope.showCreate = false;
        $scope.initErrors();
        $scope.countries = [];
        $scope.provincesFrom = [];
        $scope.provincesTo = [];
        $scope.provincesCustom = [];
        $scope.citiesFrom = [];
        $scope.citiesTo = [];
        $scope.citiesCustom = [];
        $scope.dimTypes = [];
        $scope.quote = [];
        $scope.methods = [];
        $scope.packageTypes = [{
            'id': 1,
            'name': 'Envelope'
        }, {
            'id': 2,
            'name': 'Pak'
        }, {
            'id': 3,
            'name': 'Package'
        }, {
            'id': 5,
            'name': 'SmarteHome'
        }];
        $scope.hourTimes = [{
            'id': '07',
            'name': '07'
        }, {
            'id': '08',
            'name': '08'
        }, {
            'id': '09',
            'name': '09'
        }, {
            'id': '10',
            'name': '10'
        }, {
            'id': '11',
            'name': '11'
        }, {
            'id': '12',
            'name': '12'
        }, {
            'id': '13',
            'name': '13'
        }, {
            'id': '14',
            'name': '14'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '16',
            'name': '16'
        }, {
            'id': '17',
            'name': '17'
        }];

        $scope.minuteTimes = [{
            'id': '00',
            'name': '00'
        }, {
            'id': '15',
            'name': '15'
        }, {
            'id': '30',
            'name': '30'
        }, {
            'id': '45',
            'name': '45'
        }];

        $scope.pickupLocation = [{
            'id': 'Receiving',
            'name': 'Receiving'
        }, {
            'id': 'Garage',
            'name': 'Garage'
        }, {
            'id': 'Lobby',
            'name': 'Lobby'
        }, {
            'id': 'Reception',
            'name': 'Reception'
        }, {
            'id': 'Front Desk',
            'name': 'Front Desk'
        }, {
            'id': 'Vault',
            'name': 'Vault'
        }, {
            'id': 'Switchboard',
            'name': 'Switchboard'
        }, {
            'id': 'Back Door',
            'name': 'Back Door'
        }, {
            'id': 'Desk',
            'name': 'Desk'
        }, {
            'id': 'Between Doors',
            'name': 'Between Doors'
        }, {
            'id': 'Kiosk',
            'name': 'Kiosk'
        }, {
            'id': 'Office',
            'name': 'Office'
        }, {
            'id': 'Outside Door',
            'name': 'Outside Door'
        }, {
            'id': 'Mailbox',
            'name': 'Mailbox'
        }, {
            'id': 'Side Door',
            'name': 'Side Door'
        }, {
            'id': 'Service Counter',
            'name': 'Service Counter'
        }, {
            'id': 'Security',
            'name': 'Security'
        }, {
            'id': 'Shipping',
            'name': 'Shipping'
        }, {
            'id': 'Front Door',
            'name': 'Front Door'
        }, {
            'id': 'Basement',
            'name': 'Basement'
        }, {
            'id': 'Mail Room',
            'name': 'Mail Room'
        }, {
            'id': 'Lab',
            'name': 'Lab'
        }, {
            'id': 'Warehouse',
            'name': 'Warehouse'
        }, {
            'id': 'Pharmacy',
            'name': 'Pharmacy'
        }, {
            'id': 'Pro Shop',
            'name': 'Pro Shop'
        }, {
            'id': 'Parts Department',
            'name': 'Parts Department'
        }, {
            'id': 'Counter',
            'name': 'Counter'
        }, {
            'id': 'Loading Dock',
            'name': 'Loading Dock'
        }, {
            'id': 'Gate House',
            'name': 'Gate House'
        }];

        $scope.changeSchedulePickup = function() {
            setTimeout(function() {
                setDatePicker();
            });
        };

        $scope.product = [];
        $scope.quotes = [];
        $scope.order = {
            customer_id: null,
            payment_method: null,
            total_discount: 0,
            total_pay: 0,
            date_pay: null,
            pay: 0,
            debt: 0
        };
        $scope.initAddressFrom = function() {
            $scope.addressFrom = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                country_id: 14,
                city_id: null,
                province_id: null,
                postal_code: null,
                telephone: null,
                email: null,
                attention: null,
                instruction: null,
                shipping_date: null,
                confirm_delivery: false,
                residential: false
            };
        };
        $scope.initAddressTo = function() {
            $scope.addressTo = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                country_id: 47,
                province_id: null,
                city_id: null,
                postal_code: null,
                telephone: null,
                email: null,
                attention: null,
                instruction: null,
                notify_recipient: false,
                residential: false
            };
        };

        $scope.initItem = function() {
            $scope.item = {
                description: null,
                code: null,
                country_id: null,
                country_code: null,
                quantity: null,
                unitPrice: null,
                subPrice: null
            };
        };

        $scope.initCustom = function() {
            $scope.custom = {
                company: null,
                name: null,
                brokerName: null,
                taxId: null,
                phone: null,
                receiptsTaxId: null,
                total: 0,
                items: []
            };

            $scope.custom.billTo = {
                company: null,
                name: null,
                address_1: null,
                country_id: null,
                province_id: null,
                city_id: null,
                postal_code: null
            };

            $scope.custom.dutiesTaxes = {
                dutiable: 'false',
                billTo: 'receiver',
                consigneeAccount: null,
                sedNumber: null
            };

            $scope.initItem();
        };

        $scope.initAddressTo();
        $scope.initAddressFrom();
        $scope.initCustom();

        $scope.initPackages = function() {
            var product = {
                id: null,
                length: 1,
                width: 1,
                height: 1,
                weight: 1,
                insuranceAmount: 0.0,
                freightClass: null,
                nmfcCode: null,
                codValue: 0.0,
                description: null
            };
            $scope.packages = {
                type: 1,
                quantity: 1,
                referenceCode: null,
                dimType: 3,
                boxes: []
            };
            $scope.packages.boxes.push(product);
            $scope.product.push(product);
        };
        $scope.customer = null;
        $scope.initPackages();
        $scope.services = {
            saturdayDelivery: null,
            saturdayPickup: null,
            holdForPickup: null,
            docsOnly: null,
            dangerousGoods: null,
            signatureRequired: null,
            insuranceType: null,
            codPaymentType: null
        };
        $scope.initQuantity = function() {
            var range = [];
            for (var i = 1; i < 50; i++) {
                range.push(i);
            }
            $scope.quantities = range;
        };
        $scope.setService = function(index) {
            $scope.quote = $scope.quotes[index];
            $scope.showCreate = true;
        };
        $scope.getExpress = function() {
            $http({
                method: 'get',
                url: '/admin/customer/express/' + $scope.id
            }).then(
                function(resp) {
                    $scope.packages = resp.data.express.packages;
                    $scope.addressFrom = resp.data.express.addressFrom;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="addressFrom.image_' + i +
                            '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if ($scope.addressFrom['image' + i]) {
                            img = $scope.addressFrom['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        } else {
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.addressTo = resp.data.express.addressTo;
                    $scope.quote = resp.data.express.quote;
                    $scope.quote.discount_number = resp.data.express.order.discount_number;
                    $scope.quote.total = resp.data.express.order.total_pay;
                    $scope.selectQuote = resp.data.express.quote;
                    $scope.services = resp.data.express.services;
                    $scope.customer = resp.data.express.customer;
                    $scope.order = resp.data.express.order;
                    $scope.quotes = resp.data.express.quotes;
                    if ($scope.quotes.length == 0) {
                        $scope.bntSearch = true;
                        $scope.bntConfirm = false;
                    }
                    if ($scope.order.order_status != 3 && $scope.order.eshiper_shipping_status != 4) {
                        $scope.bntCancel = true;
                    }
                    if (!$scope.order.express_order_id && $scope.order.order_status == 1) {
                        $scope.bntConfirm = true;
                    }
                    $scope.pickup = resp.data.express.pickup;
                    $scope.custom = resp.data.express.custom;
                    $scope.outside_canada = resp.data.express.outside_canada;
                    if ($scope.outside_canada) {
                        $scope.getProvincesCustom();
                        $scope.getCitiesCustom();
                    }
                    $scope.init();
                    $scope.changePackageType();
                    $scope.initCustomerSearch();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.customersSearchResult = [];
        $scope.initCustomerSearch = function() {
            $scope.customerSearch = {
                id: $scope.customer.id,
                telephone: $scope.customer.telephone,
                code: $scope.customer.code,
                first_name: $scope.customer.first_name,
                middle_name: $scope.customer.middle_name,
                last_name: $scope.customer.last_name
            };
        };
        $scope.changeOrder = function() {
            if ($scope.quote) {
                $scope.order.debt = 0;
                if ($scope.order.total_pay >= $scope.order.pay) {
                    $scope.order.debt = $scope.order.total_pay - $scope.order.pay;
                }
            }
        };
        $scope.searchCustomers = function() {
            angular.element('#modal-customers-search-result').modal('show');

            $scope.customersSearchResult = [];
            $scope.submittedSearchCustomers = true;

            $http({
                method: 'get',
                url: '/api/admin/customers?' + $httpParamSerializer($scope.customerSearch)
            }).then(
                function(resp) {
                    $scope.customersSearchResult = resp.data.customers.data;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchCustomers = false;
            });
        };
        $scope.selectCustomer = function(customer) {
            $scope.customer = customer;
            angular.element('#modal-customers-search-result').modal('hide');
        };

        $scope.init = function() {
            $q.all([
                    $http({
                        method: 'get',
                        url: '/api/admin/countries'
                    }),
                    $http({
                        method: 'get',
                        url: '/api/admin/provinces?country_id=' + $scope.addressFrom.country_id
                    }),
                    $http({
                        method: 'get',
                        url: '/api/admin/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
                    }),
                    $http({
                        method: 'get',
                        url: '/api/admin/provinces?country_id=' + $scope.addressTo.country_id
                    }),
                    $http({
                        method: 'get',
                        url: '/api/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                            $scope.addressTo.province_id
                    }), $http({
                        method: 'get',
                        url: '/api/dim-types'
                    }),
                    $http({
                        method: 'get',
                        url: '/api/admin/payment-methods?type=1'
                    })
                ])
                .then(function(resp) {
                    $scope.countries = resp[0].data.countries;
                    $scope.provincesFrom = resp[1].data.provinces;
                    $scope.citiesFrom = resp[2].data.cities;
                    $scope.provincesTo = resp[3].data.provinces;
                    $scope.citiesTo = resp[4].data.cities;
                    $scope.dimTypes = resp[5].data.dimTypes;
                    $scope.methods = resp[6].data.methods;
                    setTimeout(function() {
                        setDatePicker();
                    });
                });
        };

        $scope.getProvincesFrom = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.addressFrom.country_id
            }).then(
                function(resp) {
                    $scope.provincesFrom = resp.data.provinces;
                    $scope.getCitiesFrom();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesFrom = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.addressFrom.country_id + '&province_id=' + $scope.addressFrom.province_id
            }).then(
                function(resp) {
                    $scope.citiesFrom = resp.data.cities;
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };
        $scope.getProvincesTo = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.addressTo.country_id
            }).then(
                function(resp) {
                    $scope.provincesTo = resp.data.provinces;
                    $scope.getCitiesTo();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesTo = function() {
            $http({
                method: 'get',
                url: '/api/admin/cities?country_id=' + $scope.addressTo.country_id + '&province_id=' +
                    $scope.addressTo.province_id
            }).then(
                function(resp) {
                    $scope.citiesTo = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };
        $scope.getProvincesCustom = function() {
            $http({
                method: 'get',
                url: '/api/admin/provinces?country_id=' + $scope.custom.billTo.country_id
            }).then(
                function(resp) {
                    $scope.provincesCustom = resp.data.provinces;
                    $scope.getCitiesCustom();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustom = function() {
            $http({
                method: 'get',
                url: '/admin/cities?country_id=' + $scope.custom.billTo.country_id + '&province_id=' +
                    $scope.custom.billTo.province_id
            }).then(
                function(resp) {
                    $scope.citiesCustom = resp.data.cities;
                },
                function(resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getPostCodeCustom = function() {
            angular.forEach($scope.citiesCustom, function(value, key) {
                if (value.id == $scope.custom.billTo.city_id) {
                    $scope.custom.billTo.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.stringToDate = function(_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate;
        }

        $scope.$watchCollection('addressFrom.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */
        $scope.isAvailableIdCard = function(cardDate) {
            if (!cardDate) {
                return false;
            }
            var cardTime = $scope.stringToDate(cardDate, 'dd/MM/yyyy', "/").getTime();
            var currentTime = new Date().getTime();
            if (cardTime < currentTime) {
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;
            if (!$scope.addressFrom.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.isAvailableIdCard($scope.addressFrom.card_expire)) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.addressFrom.image_1_file_id) {
                $scope.isEditDiscount = false;
            }
        };

        $scope.updateFee = function(quote) {
            var per_discount = parseFloat(quote.per_discount) > 0 ? parseFloat(quote.per_discount) : 0;
            $scope.quote.discount = quote.per_discount;
            $scope.quote.total_discount = (parseFloat(quote.baseCharge) * per_discount) / 100;
            $scope.quote.total = parseFloat(quote.totalCharge) - parseFloat($scope.quote.total_discount);
        };

        $scope.changePackageType = function() {
            if ($scope.packages.type == 1 || $scope.packages.type == 2) {
                $scope.packages.quantity = 1;
                $scope.changeQuantity();
                $scope.disable_field = true;
            } else if ($scope.packages.type == 4) {
                $scope.disable_field = false;
                $scope.show_field = true;
            } else {
                $scope.disable_field = false;
                $scope.show_field = false;
            }
        };
        $scope.changeDimType = function() {
            var dim = 'in';
            var weight = 'lbs';
            var productHead = angular.element('#productHead');
            if ($scope.packages.dimType == 1) {
                dim = 'cm';
                weight = 'kg';
            }
            productHead.find('#lengthHeader').text('Length (' + dim + ')');
            productHead.find('#widthHeader').text('Width (' + dim + ')');
            productHead.find('#heightHeader').text('Height (' + dim + ')');
            productHead.find('#weightHeader').text('Weight (' + weight + ')');
        };
        $scope.changeQuantity = function() {
            var currentProduct = $scope.packages.boxes.length;
            if ($scope.packages.quantity > currentProduct) {
                for (var i = 0; i < $scope.packages.quantity - currentProduct; i++) {
                    var product = {
                        id: null,
                        length: 1,
                        width: 1,
                        height: 1,
                        weight: 1,
                        insuranceAmount: 0.0,
                        freightClass: null,
                        nmfcCode: null,
                        codValue: 0.0,
                        description: null
                    };
                    $scope.packages.boxes.push(product);
                }
            } else {
                for (var i = 0; i < currentProduct - $scope.packages.quantity; i++) {
                    $scope.packages.boxes.pop();
                }
            }

        };
        $scope.clearExpress = function() {
            $scope.addressTo = [];
            $scope.addressFrom = [];
            $scope.initPackages();
            $scope.services = [];
            $scope.quotes = [];
        };

        $scope.setService = function(index) {
            $scope.quote = $scope.quotes[index];
            $scope.quote.discount_number = 0;
            $scope.quote.discount_amount = (parseFloat($scope.quote.baseCharge) * parseFloat($scope.quote.discount_number)) / 100;
            $scope.quote.total = parseFloat($scope.quote.totalCharge) - parseFloat($scope.quote.discount_amount);
            $scope.bntSearch = false;
            $scope.bntConfirm = true;
        };

        $scope.showPopupVip = function() {
            popup.msg(language.notify_admin_vip);
        };

        $scope.scanSearchExpress = function() {
            var flag = false;
            var packages = angular.copy($scope.packages.boxes);

            angular.forEach(packages, function(product) {
                if (product.volume > product.weight) {
                    flag = true;
                }
            });
            if (flag) {
                popup.confirmChange(language.weight_volume, function() {
                    $scope.searchExpress();
                });
            } else {
                $scope.searchExpress();
            }
        };

        $scope.searchExpress = function() {
            $scope.submittedSearch = true;
            var packages = angular.copy($scope.packages);
            angular.forEach(packages.boxes, function(product) {
                if (product.volume > product.weight) {
                    product.weight = product.volume;
                }
            });
            var data = {
                addressFrom: $scope.addressFrom,
                addressTo: $scope.addressTo,
                pickup: $scope.pickup,
                custom: $scope.custom,
                packages: packages,
                services: $scope.services,
                outside_canada: $scope.outside_canada
            };
            $http({
                method: 'POST',
                url: '/admin/customer/express/get-quote',
                data: data
            }).then(
                function(resp) {
                    if (resp.data.success === true) {
                        $scope.submittedSearch = false;
                        $scope.quotes = resp.data.data;
                    } else {
                        $scope.errors.quotes.push(resp.data.message);
                        angular.element('#modal-errors').modal('show');
                    }
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('addressFrom') !== -1) {
                                $scope.errors.addressFrom.push(error[0]);
                            } else if (field.indexOf('addressTo') !== -1) {
                                $scope.errors.addressTo.push(error[0]);
                            } else if (field.indexOf('pickup') !== -1) {
                                $scope.errors.pickups.push(error[0]);
                            } else if (field.indexOf('custom') !== -1) {
                                $scope.errors.custom.push(error[0]);
                            } else if (field.indexOf('packages') !== -1) {
                                if (typeof $scope.errors.packages.boxes === 'undefined') {
                                    $scope.errors.packages = {
                                        boxes: [],
                                        orther: []
                                    };
                                }
                                if (field.indexOf('packages.boxes') !== -1) {
                                    for (var i = 0; i < $scope.packages.boxes.length; ++i) {
                                        if (typeof $scope.errors.packages.boxes[i] === 'undefined') {
                                            $scope.errors.packages.boxes[i] = [];
                                            var errorPushed = false;
                                        }
                                        if (field.indexOf('packages.boxes.' + i) === -1) {
                                            continue;
                                        }
                                        $scope.errors.packages.boxes[i].push(error[0]);
                                        errorPushed = true;
                                    }
                                } else {
                                    $scope.errors.packages.orther.push(error[0]);
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedSearch = false;
            });
        };

        $scope.approveOrder = function() {
            if ($scope.quote.carrierId) {
                var data = {
                    id: $scope.order.order_id,
                    addressFrom: $scope.addressFrom,
                    quotes: $scope.quotes,
                    quote: $scope.quote,
                };

                $scope.submittedApprove = true;
                $http({
                    method: 'POST',
                    url: '/admin/customer/express/approve',
                    data: data
                }).then(
                    function(resp) {
                        location.href = '/admin/customer/express/list';
                    },
                    function(resp) {
                        if (resp.status === 500) {
                            $scope.errors.system = [];
                            $scope.errors.system.push(resp.data);
                            angular.element('#modal-errors').modal('show');
                        }
                    }
                ).finally(function() {
                    $scope.submittedApprove = false;
                });
            }
            //het kiem tra da chon don vi shipper
        };


        $scope.getShipping = function() {
            var data = {
                order: $scope.order
            };

            $scope.submittedShipping = true;
            $http({
                method: 'POST',
                url: '/admin/customer/express/update',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/customer/express/list';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedShipping = false;
            });
        };

        $scope.paymentEpress = function() {
            var data = {
                order: $scope.order
            };

            $scope.submittedPayment = true;
            $http({
                method: 'POST',
                url: '/admin/customer/express/payment',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/customer/express/list';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('order') !== -1) {
                                $scope.errors.order.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedPayment = false;
            });
        };

        $scope.cancelOrder = function() {
            var data = {
                id: $scope.order.order_id
            };

            $scope.submittedCancel = true;
            $http({
                method: 'POST',
                url: '/admin/customer/express/cancel-order',
                data: data
            }).then(
                function(resp) {
                    location.href = '/admin/customer/express/list';
                },
                function(resp) {
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submittedCancel = false;
            });
        };
    });
}());
