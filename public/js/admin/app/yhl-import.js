(function () {
    'use strict';
    var app = angular.module('YhlApp', []);
    app.controller('YhlImportController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;
        $scope.initErrors = function () {
            $scope.errors = [];
        };
        $scope.initErrors();
        $scope.products = [];
        $scope.methods = [];
        $scope.list_warehouse = data;
        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url   : '/api/admin/products?service=3&status=1&agency=' + $scope.agency
                })

            ]).then(function (resp) {
                $scope.products = resp[0].data.products;
                    angular.forEach($scope.list_products, function (product) {
                        product.product_list = angular.copy($scope.products);
                    });
            });
        };
        $scope.initproduct = function () {
            var  products = [];

            for (var i = 1; i <= 4; ++i) {
                var product = {
                    product_id    : null,
                    code          : null,
                    name          : null,
                    by_weight     : null,
                    quantity      : null,
                    weight        : null,
                    price         : null,
                    per_discount  : null,
                    discount      : null,
                    agency_unit_id      : null,
                    agency_currency_id      : null,
                    unit          : null,
                    amount        : null,
                    total         : null,
                    note          : null,
                    description   : null,
                    product_list  : $scope.products
                };
                products.push(product);
            }

            return products;
        };

        $scope.list_products = $scope.initproduct();


       function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }



        $scope.searchProducts = function (product, idDropdown) {
            var code     = product.code ? product.code.toLowerCase().trim() : '',
                dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function (item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
            });

            var products = product.product_list.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.by_weight = item.by_weight;
            product.price = parseFloat(item.sale_price) + parseFloat(item.pickup_fee);
            product.description = item.description;
            product.discount = 0;
            product.quantity = 1;
            product.weight = item.weight;
            product.unit = item.unit;
            $scope.updateContainers();
        };


        $scope.createimport = function () {
                    var list_products = angular.copy($scope.list_products);
                    list_products = list_products.filter(function (product) {
                         return !!product.product_id;
                });
                angular.forEach(list_products, function (product) {
                    delete product.product_list;
                });
            var data = {
                description        : $scope.description,
                list_warehouse     : $scope.list_warehouse,
                list_products      : list_products,
                list_supplies      : $scope.list_supplies

            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url   : '/admin/yhl/importgood/store',
                data  : data
            }).then(
                function (resp) {
                    $scope.submitted=false;
                    $scope.complete=true;
                   $scope.new_order=resp.data;
                },
                function (resp) {
                    if( resp.status === 422 ) {
                        angular.forEach(resp.data.errors, function (error, field) {
                            $scope.errors.push(error[0]);
                        });
                        angular.element('#modal-errors').modal('show');
                      }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function () {
                $scope.submitted = false;
            });
        };

        $scope.remove = function (index) {
            $scope.list_products.splice(index, 1);
        };
        $scope.addproduct = function () {
            var product = {
                product_id: null,
                code: null,
                name: null,
                by_weight: null,
                quantity: null,
                weight: null,
                price: null,
                per_discount: null,
                discount: null,
                unit: null,
                amount: null,
                declared_value: null,
                surcharge: null,
                is_insurance: false,
                insurance: null,
                total: null,
                note: null,
                description: null,
                product_list: $scope.products
            };
           $scope.list_products.push(product);
        };
        $scope.updateContainers = function (isPostCode) {
            if(isPostCode){
                $scope.getPostCodeReceiver();
            }
            angular.forEach($scope.containers, function (container) {
                container.yhl_fee = 0;
                container.total_shipping_fee = 0;
                container.total_weight = 0;
                container.total_amount = 0;
                container.total_discount = 0;
                container.total_quantity=0;
                container.total = 0;

                var city;
                if ($scope.address.city_id) {
                    angular.forEach($scope.citiesReceiver, function (item) {
                        if (item.id === $scope.address.city_id) {
                            city = item;
                            return true;
                        }
                    });
                }

                angular.forEach(container.products, function (product) {
                    if (product.product_id) {
                        var quantity = 0;
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                container.total_quantity += quantity;
                                container.total_weight += parseFloat(product.weight)*quantity;
                            }
                        var amount = product.price * quantity;
                        if (!isNaN(product.per_discount) && product.per_discount > 0) {
                            var discount = (parseFloat(product.per_discount) / 100) * amount;
                            amount = amount - discount;
                            container.total_discount += discount;
                        }
                        product.amount = amount;
                        container.total_amount += product.amount;
                        product.total = product.amount;
                        container.total += product.total;
                        container.debt = container.total;
                        if (!isNaN(container.pay) && container.pay > 0) {
                            container.debt = container.total - container.pay > 0 ? container.total - container.pay  : 0;
                        }
                    }
                });
                container.yhl_fee = city && city.yhl_fee ? parseFloat(city.yhl_fee) : 0;
                container.total_shipping_fee = container.yhl_fee * container.total_weight;
                container.total += container.total_shipping_fee;
                if(container.coupon_amount > 0){
                    container.total -= container.coupon_amount;
                }
                if(container.total < 0){
                    container.total = 0;
                }
                container.pay = container.total.toFixed(2);
                container.debt = container.total;
                if (!isNaN(container.pay) && container.pay > 0) {
                    container.debt = container.total - container.pay > 0 ? container.total - container.pay  : 0;
                }
            });
        };

    });
}());
