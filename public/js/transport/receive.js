var receive = {};
var sound_classicalarm = new Audio('/sounds/classicalarm.wav');
var sound_complete = new Audio('/sounds/complete.wav');
receive.pushItems = function (shipment_id) {
    fly.ajax({
        service: '/admin/transport/receive/push-items',
        method: "POST",
        loading: true,
        data: {
            items: $('#shipment-package-form').serializeArray(),
            to_warehouse_id: $('select[name=to_warehouse_id]').val(),
            shipment_id: shipment_id
        },
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
    // fly.submit({
    //     id: 'shipment-package-form',
    //     service: '/admin/transport/receive/push-items',
    //     success: function (result) {
    //         if (result.success) {
    //             popup.msg(result.message, function () {
    //                 window.location = result.data.refUrl;
    //             });
    //         } else {
    //             popup.msg(result.message);
    //         }
    //     }
    // });
};
receive.checkItem = function (code) {
    var item_check = document.getElementById(code)
    if (item_check) {
        console.log(code + ' success');
        item_check.checked = true;
        document.getElementById('error_alert').innerHTML = '';
        sound_complete.play();
    }
    else {
        console.log(code + ' code is not exists');
        document.getElementById('error_alert').innerHTML = code + ' error';
        sound_classicalarm.play();
    }
};
