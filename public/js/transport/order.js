var order = {};
// tam thoi khong dung nua
order.print = function (id) {
     location.href = 'transports/'+id+'/print_bills';
};
// language = JSON.parse(language);
order.cancel = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/transport/tracking/cancel-order',
            method: "POST",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};
order.restore = function (id) {
    popup.confirm(language.confirm_restore_order, function () {
        fly.ajax({
            service: '/admin/transport/tracking/restoreorder',
            method: "post",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};
order.export = function () {
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()
    };
    var url = "/admin/transports/excel?" + $.param(query);
   window.location.href =url;
};
// danh sach don hang khach online
order.exportcustom = function () {
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val()
    };
    var url = "/admin/transportquote/order/export?" + $.param(query);
   window.location.href = url;
};
order.cancelEshiper = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/transports/quote/cancel-eshipper',
            method: "POST",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};


order.viewHistoryEshiper = function (id) {
    fly.ajax({
        service: '/admin/transports/quote/view-history-eshipper',
        method: "POST",
        loading: true,
        data: {
            id: id
        },
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('transport/view_history_eshipper.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};

order.cusViewHistoryEshiper = function (id) {
    fly.ajax({
        service: '/transports/'+ id +'/view-history-eshipper',
        method: "GET",
        loading: true,
        data: {

        },
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('transport/view_history_eshipper.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};
