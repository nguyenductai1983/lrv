var stockout = {};
var sound_classicalarm = new Audio('/sounds/classicalarm.wav');
var sound_complete = new Audio('/sounds/complete.wav');
stockout.pushItems = function () {
    fly.submit({
        id: 'shipment-package-form',
        service: '/admin/transport/stockout/push-items',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};
stockout.checkItem = function (code) {
    var item_check = document.getElementById(code)
    if (item_check) {
        console.log(code + ' success');
        item_check.checked = true;
        document.getElementById('error_alert').innerHTML = '';
        sound_complete.play();
    }
    else {
        console.log('code is not exists');
        document.getElementById('error_alert').innerHTML = code + ' error';
        sound_classicalarm.play();
    }
};
