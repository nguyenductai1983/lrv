        var carrier = {};
        carrier.pushItems = function(id) {
            fly.submit({
                id: 'shipment-package-form',
                service: '/admin/transport/carrier/' + id + '/push-items',
                success: function(result) {
                    if (result.success) {
                        popup.msg(result.message, function() {
                            window.location = result.data.refUrl;
                        });
                    } else {
                        popup.msg(result.message);
                    }
                }
            });
        };