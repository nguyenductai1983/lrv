var stockout = {};

stockout.pushItems = function () {
    fly.submit({
        id: 'pickup-package-form',
        service: '/admin/transport/pickup/push-items',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};
