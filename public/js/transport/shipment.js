var shipment = {};
shipment.addPackage = function(refUrl) {
    fly.ajax({
        service: '/admin/transport/shipment/add-package',
        method: "POST",
        loading: true,
        data: {
            items: $('#shipment-package-form').serializeArray(),
            from_warehouse_id: $('select[name=from_warehouse_id]').val(),
            shipment_name: $('[name=shipment_name]').val(),
            estimate_delivery: $('#estimate_delivery').val()
        },
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};

shipment.addItem = function() {
    var code = $('#order_code').val();
    fly.ajax({
        service: '/admin/transport/shipment/add-item',
        method: "POST",
        loading: true,
        data: {
            code: code
        },
        success: function(result) {
            if (result.success) {
                var num = parseInt($('input[name=numItem]').val());
                if (result.data.length == 0) {
                    popup.msg(language.err_order_to_shipment);
                    return;
                } else if (num == 1) {
                    $('#empty-item').remove();
                }
                var html = '';
                $.each(result.data, function(key, data) {
                    if (shipment.checkItemExist(data.id)) {
                        popup.msg(language.order_code + ' ' + data.code + ' ' + language.err_add_to_shipment_exist);
                        return;
                    }
                    html += '<tr id="shipment-package-create-' + num + '">' +
                        '<td><input type="checkbox" name="id" value="' + data.id + '" checked=""></td>' +
                        '<td>' + data.code + '</td>' +
                        '<td>' + data.products + '</td>' +
                        '<td>' + data.user_note + '</td>' +
                        '<td>' +
                        '<button type="button" onclick="warehouse.removeItem(' + num + ')" class="btn btn-xs btn-danger">' +
                        '<i class="fas fa-trash"></i>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';
                    num = num + 1;
                });
                $('#shipment-package-items').append(html);
                $('input[name=numItem]').val(num + 1);
                $('#order_code').val('');
            } else {
                popup.msg(result.message);
            }
        }
    });
};

shipment.checkItemExist = function(id) {
    var data = $('#shipment-package-form').serializeArray();
    var flag = false;
    $.each(data, function(key, item) {
        if (item.value == id) {
            flag = true;
        }
    });
    return flag;
};

shipment.removeItem = function(id) {
    $('#shipment-package-create-' + id).remove();
    var num = parseInt($('input[name=numItem]').val());

    if (num - 1 == 1) {
        var html = '<tr id="empty-item">' +
            '<td colspan="7">' + language.no_records + '</td>' +
            '</tr>';
        $('#shipment-package-items').html(html);
    }
    $('input[name=numItem]').val(num - 1);
};


shipment.addItemExist = function(id) {
    var code = $('#order_code').val();
    fly.ajax({
        service: '/admin/transport/shipment/add-item-exist',
        method: "POST",
        loading: true,
        data: {
            shipment_id: id,
            code: code
        },
        success: function(result) {
            if (result.success) {
                var html = '<tr id="shipment-package-item-' + result.data.id + '">' +
                    '<td>' + '</td>' +
                    '<td>' + result.data.code + '</td>' +
                    '<td>' + result.data.products + '</td>' +
                    '<td>' + result.data.user_note + '</td>' +
                    '<td><span class="' + result.data.shipping_status_label + '">' + result.data.shipping_status_name + '</span></td>' +
                    '<td>' +
                    '<button type="button" onclick="shipment.removeItemExist(' + result.data.id + ',' + id + ')" class="btn btn-xs btn-danger">' +
                    '<i class="fas fa-trash"></i>' +
                    '</button>' +
                    '</td>' +
                    '</tr>';

                $('#shipment-package-items').append(html);
                $('#order_code').val('');
            } else {
                popup.msg(result.message);
            }
        }
    });
};

shipment.removeItemExist = function(id, shipment_id) {
    fly.ajax({
        service: '/admin/transport/shipment/remove-item-exist',
        method: "POST",
        loading: true,
        data: {
            shipment_id: shipment_id,
            id: id
        },
        success: function(result) {
            if (result.success) {
                $('#shipment-package-item-' + id).remove();
                if (result.data.status == 2) {
                    location.reload();
                }
            } else {
                popup.msg(result.message);
            }
        }
    });

};

shipment.editPackage = function(refUrl) {
    fly.submit({
        id: 'shipment-package-info-form',
        method: "POST",
        service: '/admin/transport/shipment/edit-package',
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};