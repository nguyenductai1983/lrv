var warehouse = {};
var sound_classicalarm = new Audio('/sounds/classicalarm.wav');
var sound_complete = new Audio('/sounds/complete.wav');
warehouse.addPackage = function(refUrl) {
    fly.ajax({
        service: '/admin/transport/warehouse/add-package',
        method: "POST",
        loading: true,
        data: {
            items: $('#warehouse-package-form').serializeArray(),
            from_warehouse_id: $('select[name=from_warehouse_id]').val(),
            stockin_at: $('#stockin_at').val()
        },
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};
warehouse.addItem = function() {
    var code = $('#order_code').val();
    fly.ajax({
        service: '/admin/transport/warehouse/add-item',
        method: "POST",
        loading: true,
        data: {
            code: code
        },
        success: function(result) {
            if (result.success) {
                var num = parseInt($('input[name=numItem]').val());
                if (result.data.length == 0) {
                    if(barcode){
                        sound_classicalarm.play();
                    }
                    popup.msg(language.err_order_stockin);
                    return;
                } else if (num == 1) {
                    $('#empty-item').remove();
                }
                var html = '';
                $.each(result.data, function(key, data) {
                    if (warehouse.checkItemExist(data.id)) {
                        popup.msg(language.order_code + ' ' + data.code + ' ' + language.err_add_stockin_exist);

                        return;
                    }
                    var receive_status_name = '' + language.reciver_status_2 + '';
                    // var input_ck = '';
                    var input_ck = '<input type="checkbox" name="id" value="' + data.id + '" checked="">';
                    if (data.receive_status == 1) {
                        receive_status_name = '' + language.reciver_status_1 + '';
                        // input_ck = '<input type="checkbox" name="id" value="'+ data.id +'" checked="">';
                    }
                    html += '<tr id="warehouse-package-create-' + num + '">' +
                        '<td>' + input_ck + '</td>' +
                        '<td>' + data.code + '</td>' +
                        '<td>' + data.products + '</td>' +
                        '<td>' + data.user_note + '</td>' +
                        '<td>' + receive_status_name + '</td>' +
                        '<td>' +
                        '<button type="button" onclick="warehouse.removeItem(' + num + ')" class="btn btn-xs btn-danger">' +
                        '<i class="fas fa-trash"></i>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';
                    num = num + 1;
                });
                $('#warehouse-package-items').append(html);
                $('input[name=numItem]').val(num);
                $('#order_code').val('');
                if(barcode){
                sound_complete.play();
                }
            } else {
                popup.msg(result.message);
            }
        }
    });
};

warehouse.checkItemExist = function(id) {
    var data = $('#warehouse-package-form').serializeArray();
    var flag = false;
    $.each(data, function(key, item) {
        if (item.value == id) {
            flag = true;
        }
    });
    return flag;
};

warehouse.removeItem = function(id) {
    $('#warehouse-package-create-' + id).remove();
    var num = parseInt($('input[name=numItem]').val());

    if (num - 1 == 1) {
        var html = '<tr id="empty-item">' +
            '<td colspan="7">' + language.no_records + '</td>' +
            '</tr>';
        $('#warehouse-package-items').html(html);
    }
    $('input[name=numItem]').val(num - 1);
};


warehouse.addItemExist = function(id) {
    var code = $('#order_code').val();
    fly.ajax({
        service: '/admin/transport/warehouse/add-item-exist',
        method: "POST",
        loading: true,
        data: {
            warehouse_packge_id: id,
            code: code
        },
        success: function(result) {
            if (result.success) {
                var html = '<tr class="text-info" id="warehouse-package-item-' + result.data.id + '">' +
                    '<td>' + '</td>' +
                    '<td>' + result.data.code + '</td>' +
                    '<td>' + result.data.products + '</td>' +
                    '<td>' + result.data.user_note + '</td>' +
                    '<td><span class="' + result.data.receive_status_label + '">' + result.data.receive_status_name + '</span></td>' +
                    '<td><span class="' + result.data.shipping_status_label + '">' + result.data.shipping_status_name + '</span></td>' +
                    '<td>' +
                    '<button type="button" onclick="warehouse.removeItemExist(' + result.data.id + ',' + id + ')" class="btn btn-xs btn-danger">' +
                    '<i class="fas fa-trash"></i>' +
                    '</button>' +
                    '</td>' +
                    '</tr>';

                $('#warehouse-package-items').append(html);
                $('#order_code').val('');
            } else {
                popup.msg(result.message);
            }
        }
    });
};

warehouse.removeItemExist = function(id, warehouse_packge_id) {
    fly.ajax({
        service: '/admin/transport/warehouse/remove-item-exist',
        method: "POST",
        loading: true,
        data: {
            warehouse_packge_id: warehouse_packge_id,
            id: id
        },
        success: function(result) {
            if (result.success) {
                $('#warehouse-package-item-' + id).remove();
                if (result.data.status == 2) {
                    location.reload();
                }
            } else {
                popup.msg(result.message);
            }
        }
    });

};

warehouse.editPackage = function(refUrl) {
    fly.submit({
        id: 'warehouse-package-info-form',
        service: '/admin/transport/warehouse/edit-package',
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};
