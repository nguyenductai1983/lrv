var complete = {};

complete.cancelRequest = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/transport/complete/cancel-request',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};

complete.carrierPickup = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/transport/complete/pickup',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};

complete.customerReceiver = function () {
    fly.submit({
        id: 'shipping-package-local-form',
        service: '/admin/transport/complete/customer-receiver',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
};
complete.vncacomplete = function () {
    fly.ajax({
        service: '/admin/transport/complete/completevnca',
        method: "POST",
        loading: true,
        data: {
            items: $('#order-package-form').serializeArray()
        },
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data.refUrl;
                });
            } else {
                popup.msg(result.message);
            }
        }
    });
}
