var PubNubConfig = (function() {
    var config = window.iGreenLink;
    var pubnub = new PubNub({
        subscribeKey: config.sub,
        publishKey: config.pub
    });
    return {
        pubnub: pubnub,
    }
})();

var pubnub = PubNubConfig.pubnub;