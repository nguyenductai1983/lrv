var order = {};

order.cancel = function (id) {
    popup.confirm(language.confirm_cancel_order, function () {
        fly.ajax({
            service: '/admin/mts/cancel-order',
            method: "POST",
            loading: true,
            data: {
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location = result.data.refUrl;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};