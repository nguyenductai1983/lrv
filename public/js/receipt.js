var receipt = {};
receipt.addItems = function(refUrl) {
    fly.ajax({
        service: '/admin/receipt-orders/add-items',
        method: "POST",
        loading: true,
        data: {
            items: $('#receipt-package-form').serializeArray(),
            user_id: $('select[name=user_id]').val()
        },
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};

receipt.removeItem = function(orderId) {
    popup.confirm(language.receipt_del_item_title, function() {
        fly.ajax({
            service: '/admin/receipt-orders/remove-item',
            method: "POST",
            data: {
                order_id: orderId
            },
            loading: true,
            success: function(resp) {
                if (resp.success) {
                    location.reload();
                } else {
                    popup.msg(resp.message);
                }
            }
        });
    });
};

receipt.paid = function(id, amount, token) {
    popup.open('receipt-paid', language.receipt_paid_title, template('receipt/paid.tpl', {
        id: id,
        amount: amount,
        token: token
    }), [{
            title: language.popup_confirm,
            style: 'btn-primary',
            fn: function() {
                fly.submit({
                    id: 'receipt-paid-form',
                    service: '/admin/receipt-orders/paid-voucher',
                    success: function(result) {
                        popup.close('receipt-paid');
                        if (result.success) {
                            location.reload();
                        } else {
                            popup.msg(result.message);
                        }
                    }
                });
            }
        },
        {
            title: language.popup_btn_close,
            style: 'btn-default',
            fn: function() {
                popup.close('receipt-paid');
            }
        }
    ], 'modal-lg');
};


receipt.addMtsItems = function(refUrl) {
    fly.ajax({
        service: '/admin/receipt-mts/add-items',
        method: "POST",
        loading: true,
        data: {
            items: $('#receipt-package-form').serializeArray(),
            user_id: $('select[name=user_id]').val()
        },
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};

receipt.removeMtsItem = function(mtsId) {
    popup.confirm(language.receipt_del_item_title, function() {
        fly.ajax({
            service: '/admin/receipt-mts/remove-item',
            method: "POST",
            data: {
                mts_id: mtsId
            },
            loading: true,
            success: function(resp) {
                if (resp.success) {
                    location.reload();
                } else {
                    popup.msg(resp.message);
                }
            }
        });
    });
};

receipt.paidMts = function(id, amount, token) {
    popup.open('receipt-paid', language.receipt_paid_title, template('receipt/paid-mts.tpl', {
        id: id,
        amount: amount,
        token: token
    }), [{
            title: language.popup_confirm,
            style: 'btn-primary',
            fn: function() {
                fly.submit({
                    id: 'receipt-paid-form',
                    service: '/admin/receipt-mts/paid-voucher',
                    success: function(result) {
                        popup.close('receipt-paid');
                        if (result.success) {
                            location.reload();
                        } else {
                            popup.msg(result.message);
                        }
                    }
                });
            }
        },
        {
            title: language.popup_btn_close,
            style: 'btn-default',
            fn: function() {
                popup.close('receipt-paid');
            }
        }
    ], 'modal-lg');
};

receipt.addExtraItems = function(refUrl) {
    fly.ajax({
        service: '/admin/receipt-extra/add-items',
        method: "POST",
        loading: true,
        data: {
            items: $('#receipt-package-form').serializeArray(),
            user_id: $('select[name=user_id]').val()
        },
        success: function(result) {
            if (result.success) {
                window.location = refUrl;
            } else {
                popup.msg(result.message);
            }
        }
    });
};

receipt.removeExtraItem = function(mtsId) {
    popup.confirm(language.receipt_del_item_title, function() {
        fly.ajax({
            service: '/admin/receipt-extra/remove-item',
            method: "POST",
            data: {
                mts_id: mtsId
            },
            loading: true,
            success: function(resp) {
                if (resp.success) {
                    location.reload();
                } else {
                    popup.msg(resp.message);
                }
            }
        });
    });
};

receipt.paidExtra = function(id, amount, token) {
    popup.open('receipt-paid', language.receipt_paid_title, template('receipt/paid-extra.tpl', {
        id: id,
        amount: amount,
        token: token
    }), [{
            title: language.popup_confirm,
            style: 'btn-primary',
            fn: function() {
                fly.submit({
                    id: 'receipt-paid-form',
                    service: '/admin/receipt-extra/paid-voucher',
                    success: function(result) {
                        popup.close('receipt-paid');
                        if (result.success) {
                            location.reload();
                        } else {
                            popup.msg(result.message);
                        }
                    }
                });
            }
        },
        {
            title: language.popup_btn_close,
            style: 'btn-default',
            fn: function() {
                popup.close('receipt-paid');
            }
        }
    ], 'modal-lg');
};