(function () {
    'use strict';

    var app = angular.module('TransportApp', []);
    var body_weight = 139;
    app.controller('TransportCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submittedSearch = false;
        $scope.submittedConfirm = false;
        $scope.bntSearch = true;
        $scope.bntConfirm = false;
        $scope.isUseCoupon = false;
        $scope.isEditDiscount = false;
        $scope.flag_change = true;
        $scope.showMoreInfoCustomer = false;
        $scope.warehouse_id=0;
        $scope.ward = false;
        $scope.temp = {
            p_index:0,
            index:0
        };
        $scope.initErrors = function () {
            $scope.errors = {
                sender: [],
                receiver: [],
                container: [],
                pickups : [],
                warehouse : [],
                quotes: [],
                Coupon: [],
            };
            $scope.errors2 =[0];
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.warehouses = [];
        $scope.products = [];
        $scope.quotes = [];
        $scope.pickup = [];

        $scope.sender = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            shipping_date: null,
            card_expire: null,
            birthday: null,
            country_id: 14
        };

        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            // 29-04-2020
            ward_id: null,
            email: null,
            province_id: null,
            country_id: 91
        };
        // cập nhật thông tin khách hàng
        $scope.initContainers = function () {
            var transport = {
                date_sender: getFormatDate(new Date()),
                date_receiver: null,
                date_from_shipping: null,
                date_to_shipping: null,
                total_shipping_local: 0,
                total_shipping_international: 0,
                total_shipping: 0,
                shipping_fee: 0,
                quote: null,
                quotes: null,
                containers: []
            };

            var container = {
                products: [],
                length: null,
                width: null,
                height: null,
                volume: 0,
                total_weight: 0,
                total_shipping_fee: 0,
                total_declared_value: 0,
                total_surcharge: 0,
                total_insurance: 0,
                total_amount: 0,
                total_discount: 0,
                total_fee: 0,
                total_charge_fee: 0,
                min_fee: 0,
                total: 0,
                date_pay: null,
                pay: 0,
                debt: 0,
                currency_id: 1,
                pay_method: 'CK',
                status: 1,
                coupon_code: '',
                coupon_amount: 0
            };
                var product = {
                    product_id: null,
                    code: null,
                    name: null,
                    by_weight: null,
                    quantity: null,
                    weight: null,
                    price: null,
                    per_discount: null,
                    discount: null,
                    unit: null,
                    amount: null,
                    declared_value: null,
                    surcharge: null,
                    is_insurance: false,
                    insurance: null,
                    total: null,
                    note: null,
                    description: null,
                    product_list: $scope.products
                };
                container.products.push(product);

            transport.containers.push(container);

            return transport;
        };
        $scope.addproduct = function (index) {
            var product = {
                product_id: null,
                code: null,
                name: null,
                by_weight: null,
                quantity: null,
                weight: null,
                price: null,
                per_discount: null,
                discount: null,
                unit: null,
                amount: null,
                declared_value: null,
                surcharge: null,
                is_insurance: false,
                insurance: null,
                total: null,
                note: null,
                description: null,
                product_list: $scope.products
            };
           $scope.transport.containers[index].products.push(product);
        };
        $scope.initContainer = function () {
            var container = {
                products: [],
                length: null,
                width: null,
                height: null,
                volume: 0,
                total_weight: 0,
                total_shipping_fee: 0,
                total_declared_value: 0,
                total_surcharge: 0,
                total_insurance: 0,
                total_amount: 0,
                total_discount: 0,
                total: 0,
                date_pay: null,
                pay: 0,
                debt: 0,
                currency_id: 1,
                pay_method: 'CK',
                status: 1,
                coupon_code: '',
                coupon_amount: 0
            };
            // for (var i = 1; i <= 1; ++i) {
                var product = {
                    product_id: null,
                    code: null,
                    name: null,
                    by_weight: null,
                    quantity: null,
                    weight: null,
                    price: null,
                    per_discount: null,
                    discount: null,
                    unit: null,
                    amount: null,
                    declared_value: null,
                    surcharge: null,
                    is_insurance: false,
                    insurance: null,
                    total: null,
                    note: null,
                    description: null,
                    product_list: $scope.products
                };
                container.products.push(product);
            // }

            return container;
        };

        $scope.hourTimes = [
            {
                'id': '07',
                'name': '07'
            },{
                'id': '08',
                'name': '08'
            },{
                'id': '09',
                'name': '09'
            },{
                'id': '10',
                'name': '10'
            },{
                'id': '11',
                'name': '11'
            },{
                'id': '12',
                'name': '12'
            },{
                'id': '13',
                'name': '13'
            },{
                'id': '14',
                'name': '14'
            },{
                'id': '15',
                'name': '15'
            },{
                'id': '16',
                'name': '16'
            },{
                'id': '17',
                'name': '17'
            }
        ];

        $scope.minuteTimes = [
            {
                'id': '00',
                'name': '00'
            },{
                'id': '15',
                'name': '15'
            },{
                'id': '30',
                'name': '30'
            },{
                'id': '45',
                'name': '45'
            }
        ];

        $scope.pickupLocation = [
            {
                'id': 'Receiving',
                'name': 'Receiving'
            },{
                'id': 'Garage',
                'name': 'Garage'
            },{
                'id': 'Lobby',
                'name': 'Lobby'
            },{
                'id': 'Reception',
                'name': 'Reception'
            },{
                'id': 'Front Desk',
                'name': 'Front Desk'
            },{
                'id': 'Vault',
                'name': 'Vault'
            },{
                'id': 'Switchboard',
                'name': 'Switchboard'
            },{
                'id': 'Back Door',
                'name': 'Back Door'
            },{
                'id': 'Desk',
                'name': 'Desk'
            },{
                'id': 'Between Doors',
                'name': 'Between Doors'
            },{
                'id': 'Kiosk',
                'name': 'Kiosk'
            },{
                'id': 'Office',
                'name': 'Office'
            },{
                'id': 'Outside Door',
                'name': 'Outside Door'
            },{
                'id': 'Mailbox',
                'name': 'Mailbox'
            },{
                'id': 'Side Door',
                'name': 'Side Door'
            },{
                'id': 'Service Counter',
                'name': 'Service Counter'
            },{
                'id': 'Security',
                'name': 'Security'
            },{
                'id': 'Shipping',
                'name': 'Shipping'
            },{
                'id': 'Front Door',
                'name': 'Front Door'
            },{
                'id': 'Basement',
                'name': 'Basement'
            },{
                'id': 'Mail Room',
                'name': 'Mail Room'
            },{
                'id': 'Lab',
                'name': 'Lab'
            },{
                'id': 'Warehouse',
                'name': 'Warehouse'
            },{
                'id': 'Pharmacy',
                'name': 'Pharmacy'
            },{
                'id': 'Pro Shop',
                'name': 'Pro Shop'
            },{
                'id': 'Parts Department',
                'name': 'Parts Department'
            },{
                'id': 'Counter',
                'name': 'Counter'
            },{
                'id': 'Loading Dock',
                'name': 'Loading Dock'
            },{
                'id': 'Gate House',
                'name': 'Gate House'
            }
        ];
        $scope.initPickup = function(){
            $scope.pickup = {
                is_schedule: "1",
                contact_name: null,
                phone_number: null,
                location: null,
                date_time: null,
                start_hour_time: null,
                start_minute_time: null,
                closing_hour_time: null,
                closing_minute_time: null
            };
        };

        $scope.changeSchedulePickup = function () {
            setTimeout(function () {
                $("#pickup-date").datepicker({dateFormat: "dd/mm/yy"});
            });
        };

        $scope.transport = $scope.initContainers();
        $scope.initPickup();

        function getFormatDate(date) {
            var result = new Date(date);
            var dd = result.getDate();
            var mm = result.getMonth() + 1;
            var yyyy = result.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return dd + '/' + mm + '/' + yyyy;
        };

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);

            return result;
        };

        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/products?service=1&status=1'
                }),
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/warehouses'
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                    $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.receiver.country_id
                })
            ]).then(function (resp) {
                $scope.products = resp[0].data.products;
                angular.forEach($scope.transport.containers, function (container) {
                    angular.forEach(container.products, function (product) {
                        product.product_list = angular.copy($scope.products);
                    });
                });

                $scope.countries = resp[1].data.countries;
                $scope.warehouses = resp[2].data.warehouses;
                $scope.provincesSender = resp[3].data.provinces;
                $scope.citiesSender = resp[4].data.cities;
                $scope.provincesReceiver = resp[5].data.provinces;
                $scope.addressSearch.customer_id = $scope.sender.id;
                $scope.searchAddress();
                angular.forEach($scope.warehouses, function (warehouse) {
                    if(warehouse.is_default == 1){
                        $scope.transport.warehouse_id = warehouse.id;
                    }
                });
                $scope.pickup.location = "Front Door";
                $scope.pickup.start_hour_time = "07";
                $scope.pickup.start_minute_time = "00";
                $scope.pickup.closing_hour_time = "17";
                $scope.pickup.closing_minute_time = "30";

                $scope.checkInfoCustomer();
            });
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.addressSearch = {
            customer_id      : null
        };
        $scope.updatecustomer = function (){
            $scope.contact_save=false;
            var data =  $scope.sender;
            $http({
               method: 'POST',
               url: '/profile/update',
               data: data
           }).then(
               function (resp) {
                   if(resp.status === 200)
                   {
                    $scope.contact_save=true;
                    //3-1-2021
                    $scope.updateContainers(true, true);
                   }
                   if (resp.status === 422) {
                       $scope.initErrors();
                       angular.forEach(resp.data.errors, function (error, field) {
                           if (field.indexOf('customer') !== -1) {
                               $scope.errors.user.push(error[0]);
                           }
                       });
                       angular.element('#modal-errors').modal('show');
                       $scope.FailContact();
                   } else  if (resp.status === 500) {
                       $scope.errors.system = [];
                       $scope.errors.system.push(resp.data);
                       angular.element('#modal-errors').modal('show');
                   }

                   }
                ).finally(function () {

           });
        };
        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url   : '/api/address?customer_id=' + $scope.addressSearch.customer_id
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.address;
                    if($scope.addressSearchResult.length > 0){
                        $scope.selectedId = $scope.addressSearchResult[0].id;
                        $scope.pickAddress($scope.addressSearchResult[0].id);
                    }
                    //$scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                    $scope.submittedSearchAddress = false;
                });
        };
        $scope.selectAddress = function (address) {
            if (address == 0) {
                $scope.provincesReceiver = [];
                $scope.citiesReceiver = [];
                $scope.receiver = {
                    id:null,
                    first_name: null,
                    middle_name: null,
                    last_name: null,
                    address_1: null,
                    address_2: null,
                    telephone: null,
                    cellphone: null,
                    post_code: null,
                    city_id: null,
                    province_id: null,
                    country_id: null,
                    email: null
                };
            }
            if (address) {
                $scope.receiver = address;
            }
            $scope.getProvincesReceiver();
        };

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function(address_id){
            var address = 0;
            // 03-01-2021
            $scope.flag_change=true;
            if($scope.addressSearchResult.length > 0 && address_id >0){
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.getProvincesSender = function () {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + country_id
            }).then(
                    function (resp) {
                        $scope.provincesSender = resp.data.provinces;
                        $scope.getCitiesSender();
                    }
            );
        };

        $scope.getCitiesSender = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                    function (resp) {
                        $scope.citiesSender = resp.data.cities;
                    }
            );
        };

        $scope.getPostCodeSender = function () {
            angular.forEach($scope.citiesSender, function(value, key){
                if(value.id == $scope.sender.city_id){
                    $scope.sender.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function () {
            angular.forEach($scope.citiesReceiver, function(value, key){
                if(value.id == $scope.receiver.city_id){
                    $scope.receiver.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesReceiver = function () {
            // ngày 22-04-2020
            if($scope.receiver.country_id===91)
            {
                $scope.ward = false;
            }
            else
            {
                $scope.ward = true;
            }
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                    function (resp) {
                        $scope.provincesReceiver = resp.data.provinces;
                        $scope.getCitiesReceiver();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                    function (resp) {
                        $scope.citiesReceiver = resp.data.cities;
                    }
            );
            // ngày 22-04-2020
            $scope.getWardsReceiver() ;

        };
         // ngày 22-04-2020
         $scope.getWardsReceiver = function () {
            $scope.updateContainers(true, true);
            if($scope.receiver.country_id===91){
            $http({
                method: 'get',
                url: '/api/wards?city_id=' + $scope.receiver.city_id
            }).then(
                function (resp) {
                    $scope.wardsReceiver = resp.data.wards;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        }
    }

        $scope.setActiveContainerTab = function () {
             setTimeout(function () {
                var container = angular.element('#container');
                container.find('.nav-tabs li.tab').removeClass('active');
                container.find('.nav-tabs li.tab:last').addClass('active');
                container.find('.tab-content .tab-pane').removeClass('active');
                container.find('.tab-content .tab-pane:last').addClass('active');
             });
        };

        $scope.addContainer = function () {
            $scope.transport.containers.push($scope.initContainer());

            // setTimeout(function () {
            //     setDatePicker();
            // });

            $scope.setActiveContainerTab();
        };

        $scope.deleteContainer = function (index) {
            var modal = angular.element('#modal-delete-container-' + index);

            modal.on('hidden.bs.modal', function () {
                $scope.transport.containers.splice(index, 1);

                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                $scope.setActiveContainerTab();
            });

            modal.modal('hide');
        };

        $scope.searchProducts = function (product, idDropdown) {
             var code = product.code ? product.code.toLowerCase().trim() : '',
                    dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function (item) {
                item.show = (item.name.toLowerCase().indexOf(code) !== -1);
                item.price_total = parseFloat(item.price);
            });
              var products = product.product_list.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.by_weight = item.by_weight;
            product.price = parseFloat(item.price);
            product.description = item.description;
            product.quantity = 1;
            product.weight = 1;
            product.unit = item.unit;
            $scope.updateContainers();
        };

        $scope.updateVolume = function (product) {
            if (!isNaN(product.length) && product.length > 0
                    && !isNaN(product.width) && product.width > 0
                    && !isNaN(product.height) && product.height > 0) {
                        product.volume = (parseFloat(product.length) * parseFloat(product.width) *
                        parseFloat(product.height)) / body_weight;
            } else {
                product.volume = 0;
            }
        };

        $scope.setVolume = function (container) {
            if (container.volume <= container.total_weight) {
                return false;
            }
            var minProduct = {};
            angular.forEach(container.products, function (product) {
                if (product.product_id) {
                    if (product.weight > 0) {
                        if(typeof minProduct.weight ===  'undefined'){
                            minProduct = product;
                        }
                        if(typeof minProduct.weight !== 'undefined' && minProduct.weight > product.weight){
                            minProduct = product;
                        }
                        // product.weight = (container.volume * (parseFloat(product.weight) / container.total_weight)).toFixed(2);
                    } else {
                        product.weight = 0;
                    }
                }
            });

            if(typeof minProduct.weight !== 'undefined' && parseFloat(minProduct.weight, 2) > 0){
                minProduct.weight = (parseFloat(minProduct.weight) + (parseFloat(container.volume) - parseFloat(container.total_weight))).toFixed(2);
            }

            $scope.updateContainers();
        };


        // $scope.setVolume = function (container) {
        //     if (container.volume <= container.total_weight) {
        //         return false;
        //     }
        //     angular.forEach(container.products, function (product) {
        //         if (product.product_id) {
        //             if (product.weight > 0) {
        //                 product.weight = (container.volume * (parseFloat(product.weight) / container.total_weight)).toFixed(2);
        //             } else {
        //                 product.weight = 0;
        //             }
        //         }
        //     });
        //     $scope.updateContainers();
        // };

        $scope.stringToDate = function (_date, _format, _delimiter) {
            var formatLowerCase = _format.toLowerCase();
            var formatItems = formatLowerCase.split(_delimiter);
            var dateItems = _date.split(_delimiter);
            var monthIndex = formatItems.indexOf("mm");
            var dayIndex = formatItems.indexOf("dd");
            var yearIndex = formatItems.indexOf("yyyy");
            var month = parseInt(dateItems[monthIndex]);
            month -= 1;
            var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
            return formatedDate;
        }

        /**
         * Kiểm tra xem mã card còn thời gian không?
         */

        $scope.showPopupVip = function(){
            popup.confirm(language.notify_vip, function () {
                window.location = $scope.updateProfileLink;
            });
        };

        $scope.setCarrer = function (index) {
            $scope.transport.quote = $scope.quotes[index];
            $scope.chooseCarrer = true;
            $scope.flag_change=true;
            $scope.updateContainers($scope.bntSearch);
        };
        $scope.setNoCarrer = function () {
            $scope.transport.quote = null;
            $scope.chooseCarrer = true;
            $scope.flag_change=false;
            $scope.updateContainers();
        };
       $scope.setagree = function (check) {
            if (check) {
                $scope.bntConfirm = true;
            }
            else {
                $scope.bntConfirm = false;
            }
        };

        //Bsung mới tính toán giá trị min fee
        $scope.getMinFee = function () {
            let caPriorityFlag = true;
            let vnPriorityFlag = false;
            let minFee = 0;
            if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (caPriorityFlag) {
                if (vnPriorityFlag) {
                    minFee = $scope.caVnPriorityFee;
                } else {
                    minFee = $scope.caVnPriorityDefaultFee;
                }
            } else {
                if (vnPriorityFlag) {
                    minFee = $scope.caVnDefaultPriorityFee;
                } else {
                    minFee = $scope.caVnDefaultFee;
                }
            }
            return minFee;
        };
        $scope.searchExpress = function () {
            $scope.submittedSearch = true;
            var containers = angular.copy($scope.transport.containers);

            angular.forEach(containers, function (container) {
                container.products = container.products.filter(function (product) {
                    return !!product.product_id;
                });
                angular.forEach(container.products, function (product) {
                    delete product.product_list;
                });
            });
            $scope.warehouse_id=$scope.transport.warehouse_id;
            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                pickup: $scope.pickup,
                warehouse_id: $scope.transport.warehouse_id,
                containers: containers
            };
            $http({
                method: 'POST',
                url: '/transports/get-quote',
                data: data
            }).then(
                    function (resp) {
                        if (resp.data.success === true) {
                            $scope.submittedSearch = false;
                            $scope.quotes = resp.data.data;
                            // ngay 3-1-2021
                            $scope.bntSearch=false;
                            $scope.errors=null;
                        } else {
                            popup.confirmCustom(language.eshipper_support, function () {
                                $scope.createTransport();
                            });
                        }
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();

                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('sender') !== -1) {
                                    $scope.errors.sender.push(error[0]);
                                } else if (field.indexOf('receiver') !== -1) {
                                    $scope.errors.receiver.push(error[0]);
                                } else if (field.indexOf('pickup') !== -1) {
                                    $scope.errors.pickups.push(error[0]);
                                } else if (field.indexOf('warehouse_id') !== -1) {
                                    $scope.errors.warehouse.push(error[0]);
                                } else if (field.indexOf('containers') !== -1) {
                                    for (var i = 0; i < containers.length; ++i) {
                                        if (typeof $scope.errors.container[i] === 'undefined') {
                                            $scope.errors.container[i] = {
                                                others: [],
                                                products: []
                                            };
                                        }
                                        if (field.indexOf('containers.' + i) === -1) {
                                            continue;
                                        }
                                        var errorPushed = false;
                                        for (var j = 0; j < containers[i].products.length; ++j) {
                                            if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                                $scope.errors.container[i].products[j] = [];
                                            }
                                            if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                                continue;
                                            }
                                            $scope.errors.container[i].products[j].push(error[0]);
                                            errorPushed = true;
                                        }
                                        if (!errorPushed) {
                                            $scope.errors.container[i].others.push(error[0]);
                                        }
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                            $scope.bntSearch=true;
                        }
                    }
            ).finally(function () {
                $scope.submittedSearch = false;
                $scope.flag_change=false;
            });
        };

        $scope.remove = function (p_index, index) {
            $scope.transport.containers[p_index].products.splice(index, 1);
            // var product = {
            //     product_id: null,
            //     code: null,
            //     name: null,
            //     by_weight: null,
            //     quantity: null,
            //     weight: null,
            //     price: null,
            //     per_discount: null,
            //     discount: null,
            //     agency_unit_id: null,
            //     agency_currency_id: null,
            //     agency_surcharge_fee: null,
            //     agency_per_insurrance: null,
            //     unit: null,
            //     amount: null,
            //     declared_value: null,
            //     surcharge: null,
            //     is_insurance: false,
            //     insurance: null,
            //     total: null,
            //     note: null,
            //     description: null,
            //     product_list: $scope.products
            // };
            // $scope.transport.containers[p_index].products.pop(product);
            $scope.updateContainers();
        };

        $scope.applyCoupon = function (container) {
            var data = {
                customer: $scope.sender,
                container: container
            };
            $http({
                method: 'POST',
                url: '/api/coupon',
                data: data
            }).then(
                function (resp) {
                    var amount = parseFloat(resp.data.amount);
                    var message= resp.data.amount.message;
                    $scope.errors.Coupon.length=0;
                    if(amount > 0){
                        $scope.isUseCoupon = true;
                        container.coupon_amount = amount;
                        $scope.updateContainers();
                    }else{
                        container.coupon_code = '';
                        if(message!=='' ){
                         $scope.errors.Coupon.push(message);
                     }
                    }
                }
            );
        };

        $scope.removeCoupon = function (container) {
            $scope.isUseCoupon = false;
            container.coupon_code = '';
            container.coupon_amount = 0;
            $scope.updateContainers();
        };
        $scope.showMoreInfo = function (show=true)
        {
            if(show)
            {
                $scope.showMoreInfoCustomer=true;
            }
            else
            {
            if($scope.showMoreInfoCustomer)
            {
                $scope.showMoreInfoCustomer=false;
            }
            else
            {
                $scope.showMoreInfoCustomer=true;
            }
        }

        }
        $scope.isAvailableIdCard = function (cardDate) {
            if (!cardDate) {
                return false;
            }
            var cardTime = $scope.stringToDate(cardDate, 'dd/MM/yyyy', "/").getTime();
            var currentTime = new Date().getTime();
            if (cardTime < currentTime) {
                return false;
            }
            return true;
        };

        $scope.checkInfoCustomer = function () {
            $scope.isEditDiscount = true;
            if (!$scope.sender.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.isAvailableIdCard($scope.sender.card_expire)) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.image_1_file_id) {
                $scope.isEditDiscount = false;
            }
        };
        $scope.checktotalweight = function ()
        {
            let total_weight = 0;
            angular.forEach($scope.transport.containers, function (container) {
                let total_weight_product=0;
                angular.forEach(container.products, function (product) {
                    if (product.product_id) {
                        total_weight_product += parseFloat(product.weight);
                        }
                });
                total_weight+=  total_weight_product;
            });
            $scope.total_weight=total_weight;
        };
        $scope.updateContainers = function (isInit, isPostCode = false) {
            // don update ngay 29-12-2020
            $scope.checkInfoCustomer();
            $scope.checktotalweight();
            $scope.discount=0;
            let result = $scope.Online_Disounts.filter(word => word.Weight<=$scope.total_weight);
             result = result[result.length - 1];
             $scope.discount_info=parseFloat(result['discount']);
             if($scope.isEditDiscount)
             {
                $scope.discount=$scope.discount_info;
             }

              // don update ngay 29-12-2020
            if(!isInit){
                // $scope.quotes = null;
                $scope.transport.quote = null;
                $scope.selectQuote = null;
                $scope.bntConfirm = false;
            }
            if(isPostCode){
                $scope.getPostCodeReceiver();
            }
            $scope.transport.total_shipping_international = 0;
            $scope.transport.total_shipping_local = 0;
            $scope.transport.total_shipping = 0;
            $scope.transport.date_receiver = null;
            $scope.transport.date_from_shipping = null;
            $scope.transport.date_to_shipping = null;
            // them du tinh tong
            $scope.transport.total_amount = null;
            $scope.transport.total_shipping_fee = null;
            $scope.transport.total_weight = null;
            $scope.transport.total_surcharge = null;
            $scope.transport.total_insurance = null;
            $scope.transport.total_discount = null;
            $scope.transport.total_amount_discount = null;
            $scope.transport.total = null;
            $scope.transport.shipping_fee=0;
            // them du tinh tong
            // calc shiping fee on city
            var city;
                if ($scope.receiver.city_id) {
                    angular.forEach($scope.citiesReceiver, function (item) {
                        if (item.id === $scope.receiver.city_id) {
                            city = item;
                            return true;
                        }
                    });
                }
            $scope.transport.shipping_fee = city && city.shipping_fee ? parseFloat(city.shipping_fee) : 0;
            // end calc shiping fee on city
            // cal containers
            angular.forEach($scope.transport.containers, function (container) {
                container.total_shipping_fee = 0;
                container.total_weight = 0;
                container.total_declared_value = 0;
                container.total_surcharge = 0;
                container.total_insurance = 0;
                container.total_amount_discount = 0;
                container.total_discount = 0;
                container.total_fee_product=0;
                container.total = 0;
                container.shipping_fee = $scope.transport.shipping_fee;
                //calc product
                angular.forEach(container.products, function (product) {
                    if (product.product_id) {
                        var quantity = 0;
                        if (product.by_weight === 0) {
                            if (!isNaN(product.quantity) && product.quantity > 0) {
                                quantity = parseInt(product.quantity);
                                product.quantity = quantity;
                            }
                        } else {
                            if (!isNaN(product.weight) && product.weight > 0) {
                                quantity = parseFloat(product.weight);
                            }
                        }
                        var amount = parseFloat(product.price * quantity);
                        var discount = ($scope.discount / 100) * amount;
                        var amount_discount = amount - discount;
                        product.amount = amount_discount;
                        //cal amount for product with weight or quantity
                        product.surcharge = 0;
                        product.insurance = null;
                        if (!isNaN(product.declared_value) && product.declared_value > 0) {
                            if (quantity > 0) {
                                var quota = product.declared_value / quantity;
                                if (quota > $scope.configSurcharge.quota) {
                                    product.surcharge = parseFloat(product.declared_value) * $scope.configSurcharge.percent / 100;
                                }
                            }
                            if (product.is_insurance) {
                                product.insurance = parseFloat(product.declared_value) * $scope.configInsurance.percent / 100;
                            }
                        }
                        if (!isNaN(product.weight) && product.weight > 0) {
                            container.total_weight += parseFloat(product.weight);
                        }
                        product.total = product.amount + product.surcharge + product.insurance;
                        container.total_discount += discount;
                        container.total_amount_discount += product.amount;
                        container.total_insurance += product.insurance;
                        container.total_surcharge += product.surcharge;
                        container.total_declared_value += parseFloat(product.declared_value);
                        container.total_fee_product += amount;
                    }
                });
                 //end calc product
                container.total_shipping_fee = $scope.transport.shipping_fee * container.total_weight;
                var old_min_fee = container.total_shipping_fee + container.total_amount_discount;
                // calc min fee
                container.min_fee = $scope.getMinFee(old_min_fee);
                if (container.min_fee > container.total_amount_discount) {
                    container.total_charge_fee = container.min_fee;
                } else {
                    container.total_charge_fee = old_min_fee;
                }
                container.total = container.total_charge_fee + container.total_surcharge + container.total_insurance;
                if(container.coupon_amount > 0){
                    container.total -= container.coupon_amount;
                }
                if(container.total < 0){
                    container.total = 0;
                }
                $scope.transport.total_shipping_international += container.total;
                $scope.transport.total_weight += container.total_weight;
                $scope.transport.total_shipping_fee += container.total_shipping_fee;
                $scope.transport.total_surcharge += container.total_surcharge;
                $scope.transport.total_insurance += container.total_insurance;
                $scope.transport.total_discount += container.total_discount;
                $scope.transport.total_amount += container.total_fee_product;
                $scope.transport.total_amount_discount += container.total_amount_discount;
                $scope.transport.total += container.total;
            });
            // cal containers
            if ($scope.transport.quote) {
                $scope.transport.total_shipping_local = parseFloat($scope.transport.quote.totalCharge);
                var transitDays = parseInt($scope.transport.quote.transitDays);
                var date_receiver = addDays(new Date(), transitDays);
                var date_from_shipping = addDays(new Date(), transitDays + 7);
                var date_to_shipping = addDays(new Date(), transitDays + 14);
                $scope.transport.date_receiver = getFormatDate(date_receiver);
                $scope.transport.date_from_shipping = getFormatDate(date_from_shipping);
                $scope.transport.date_to_shipping = getFormatDate(date_to_shipping);
            }
            $scope.transport.quotes = $scope.quotes;
            $scope.transport.total_shipping = $scope.transport.total_shipping_international + $scope.transport.total_shipping_local;
            if($scope.flag_change){
                $scope.bntSearch=true;
            }
            else
            {
                $scope.bntSearch=false;
            }
        };
            $scope.changeContainers =function()
            {
                $scope.flag_change=true;
                $scope.updateContainers();

            }
        $scope.scanCreateTransport = function () {
            var flag = false;
            var containers = angular.copy($scope.transport.containers);

            angular.forEach(containers, function (container) {
                if (container.volume > container.total_weight) {
                    flag = true;
                }
            });
            if(flag){
                popup.confirmChange(language.weight_volume, function () {
                    $scope.createTransport();
                });
            }else{
                $scope.createTransport();
            }
        };

        $scope.createTransport = function () {
            $scope.submittedConfirm = true;
            var containers = angular.copy($scope.transport.containers);
            angular.forEach(containers, function (container) {
                $scope.setVolume(container);

                container.products = container.products.filter(function (product) {
                    return !!product.product_id;
                });
                angular.forEach(container.products, function (product) {
                    delete product.product_list;
                });
            });

            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                pickup: $scope.pickup,
                warehouse_id: $scope.transport.warehouse_id,
                quote: $scope.transport.quote,
                quotes: $scope.transport.quotes,
                containers: containers
            };
            $http({
                method: 'POST',
                url: '/transports/store',
                data: data
            }).then(
                    function (resp) {
                        location.href = '/transports?complete=1';
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();
                            $scope.errors2=resp.data.errors;
                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('sender') !== -1) {
                                    $scope.errors.sender.push(error[0]);
                                } else if (field.indexOf('receiver') !== -1) {
                                    $scope.errors.receiver.push(error[0]);
                                } else if (field.indexOf('pickup') !== -1) {
                                    $scope.errors.pickups.push(error[0]);
                                } else if (field.indexOf('containers') !== -1) {
                                    for (var i = 0; i < containers.length; ++i) {
                                        if (typeof $scope.errors.container[i] === 'undefined') {
                                            $scope.errors.container[i] = {
                                                others: [],
                                                products: []
                                            };
                                        }
                                        if (field.indexOf('containers.' + i) === -1) {
                                            continue;
                                        }
                                        var errorPushed = false;
                                        for (var j = 0; j < containers[i].products.length; ++j) {
                                            if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                                $scope.errors.container[i].products[j] = [];
                                            }
                                            if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                                continue;
                                            }
                                            $scope.errors.container[i].products[j].push(error[0]);
                                            errorPushed = true;
                                        }
                                        if (!errorPushed) {
                                            $scope.errors.container[i].others.push(error[0]);
                                        }
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                        } else {
                            console.log(resp.statusText);
                        }
                    }
            ).finally(function () {
                $scope.submittedConfirm = false;
            });
        };

        $scope.Confirmaddproduct = function() {
            $scope.addproduct($scope.temp.p_index);
          };
        //   $scope.showConfirmaddbox = function() {
        //     // popup.confirm(language.add_box, function () {
        //     //     $scope.addContainer();
        //     // });
        //     if (confirm(language.add_box)) {
        //         $scope.addContainer();
        //       };
        //   };
          $scope.showconfirm_Box_Product = function(p_index,index) {
              var id_model='Modal_Box_Product'+p_index;
              $scope.temp.p_index=p_index;
              $scope.temp.index=index;
              var model= document.getElementById(id_model);
            $(model).modal()
          };
          //end
    });
}());
