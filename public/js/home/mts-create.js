(function() {
    'use strict';

    var app = angular.module('MtsApp', []);

    app.controller('MtsCreateController', function($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submitted = false;

        $scope.initErrors = function() {
            $scope.errors = {
                sender: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.currencies = [];

        $scope.sender = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            shipping_date: null,
            country_id: 14
        };

        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: 47
        };

        $scope.addressesSearchResult = [];
        $scope.addressSearch = {
            customer_id: null
        };

        $scope.$watchCollection('customer.image_1_file_id', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.initContainer = function() {
            $scope.container = {
                transaction_type: 1,
                payment_by: 1,
                currency: 1,
                discount_type: 1,
                amount: 0,
                transfer_fee: 0,
                discount_number: 0,
                total: 0,
                //  reason: 'Phu giup gia dinh',
            };
        };

        $scope.initContainer();

        $scope.checkInfoCustomer = function() {
            $scope.isEditDiscount = true;

            if (!$scope.sender.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.card_expire) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.image_1_file_id) {
                $scope.isEditDiscount = false;
            }

        };

        $scope.init = function() {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                        $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/currencies'
                })
            ]).then(function(resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesSender = resp[1].data.provinces;
                $scope.citiesSender = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.currencies = resp[4].data.currencies;
                $scope.addressSearch.customer_id = $scope.sender.id;
                $scope.searchAddress();
            });
        };

        $scope.getProvincesSender = function() {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + country_id
            }).then(
                function(resp) {
                    $scope.provincesSender = resp.data.provinces;
                    $scope.getCitiesSender();
                }
            );
        };

        $scope.getCitiesSender = function() {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                function(resp) {
                    $scope.citiesSender = resp.data.cities;
                }
            );
        };

        $scope.getPostCodeSender = function() {
            angular.forEach($scope.citiesSender, function(value, key) {
                if (value.id == $scope.sender.city_id) {
                    $scope.sender.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function() {
            angular.forEach($scope.citiesReceiver, function(value, key) {
                if (value.id == $scope.receiver.city_id) {
                    $scope.receiver.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesReceiver = function() {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                function(resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getCitiesReceiver();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesReceiver = function() {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                function(resp) {
                    $scope.citiesReceiver = resp.data.cities;
                }
            );
        };
        $scope.getPostCodeSender = function() {
            angular.forEach($scope.citiesSender, function(value, key) {
                if (value.id == $scope.sender.city_id) {
                    $scope.sender.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function() {
            angular.forEach($scope.citiesReceiver, function(value, key) {
                if (value.id == $scope.receiver.city_id) {
                    $scope.receiver.postal_code = value.postal_code;
                    return;
                }
            });
        };
        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.searchAddress = function() {
            $http({
                method: 'get',
                url: '/api/address?customer_id=' + $scope.addressSearch.customer_id
            }).then(
                function(resp) {
                    $scope.addressSearchResult = resp.data.address;
                    if ($scope.addressSearchResult.length > 0) {
                        $scope.selectedId = $scope.addressSearchResult[0].id;
                        $scope.pickAddress($scope.addressSearchResult[0].id);
                    }
                    // $scope.getCitiesReceiver();
                },
                function(resp) {
                    console.log(resp.statusText);
                }
            ).finally(function() {
                $scope.submittedSearchAddress = false;
            });
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        };

        $scope.pickAddress = function(address_id) {
            var address = 0;
            if ($scope.addressSearchResult.length > 0 && address_id > 0) {
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.selectAddress = function(address) {
            if (address) {
                $scope.receiver = address;
                $scope.selectId = address.id;
            }
            if (address == 0) {
                $scope.removeAddress();
            }
            $scope.getProvincesReceiver();

        };

        $scope.removeAddress = function() {
            $scope.provincesReceiver = [];
            $scope.citiesReceiver = [];
            $scope.receiver = {
                id: null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                telephone: null,
                cellphone: null,
                post_code: null,
                city_id: null,
                province_id: null,
                country_id: null
            };
        };

        //Bsung mới tính toán giá trị per fee
        $scope.getPerFee = function() {
            let vnPriorityFlag = false;
            let perFee = $scope.mts_max_per_fee;
            if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (vnPriorityFlag) {
                perFee = $scope.mts_per_fee;
            }
            return perFee;
        };

        $scope.updateFee = function(container) {
            // lấy giá trị phù hợp nhất
            let result = $scope.MTSSurcharge.filter(word => word.amount <= container.amount);
            result = result[result.length - 1];
            // hết đoạn xử lý chi phí
            var rate_number = result['rate'];
            let vnPriorityFlag = false;
            if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;
            }
            // nếu ngoài thành phố hồ chí minh
            if (!vnPriorityFlag) {
                rate_number = parseFloat(rate_number + 0.5);
            }
            rate_number = (parseFloat(rate_number) * parseFloat(container.amount)) / 100;
            var fee = result['fee'];
            var surcharge = result['surcharge'] * container.amount / 100;
            var total_fee = rate_number + fee + surcharge;
            var discount_number = parseFloat(container.discount_number) > 0 ? parseFloat(container.discount_number) : 0;
            if (discount_number > 0 && container.discount_type == 1) {
                discount_number = (parseFloat(container.amount) * parseFloat(container.discount_number)) / 100;
            } else if (discount_number > 0 && container.discount_type == 2) {
                discount_number = parseFloat(container.discount_number);
            }
            if (discount_number > total_fee) {
                discount_number = total_fee;
            }
            total_fee = total_fee - discount_number;
            var commission = parseFloat(total_fee) * $scope.commission;
            $scope.container.transfer_fee = total_fee.toFixed(2);
            $scope.container.commission = commission.toFixed(2);
            $scope.container.total = (parseFloat(container.amount) + parseFloat(total_fee)).toFixed(2);
            //  Surcharge=Surcharge*container.amount;
            // $scope.container.transfer_fee = (($scope.getPerFee() * parseFloat(container.amount)) + $scope.fix_fee) - parseFloat(discount_amount)+fee_hight+Surcharge;
            // $scope.container.total = parseFloat($scope.container.amount) + parseFloat($scope.container.transfer_fee);
        };
        $scope.checkInfo = function(amount, relationship) {
            var result = '';
            var result3000 = '';
            var resultreason = '';
            var check_card_expire;
            var sign = 0;
            if ($scope.sender.card_expire) {
                var ngay = $scope.sender.card_expire.split('/');
                var varDate = new Date(ngay[2], ngay[1], ngay[0]); //dd-mm-YYYY
                var today = new Date();
                if (varDate >= today) {
                    check_card_expire = true
                }
            }
            if (!$scope.sender.image_1_file_id && !$scope.sender.image_2_file_id && !$scope.sender.image_2_file_id) {
                result3000 += language.image + ', ';
                sign = 1;
            }
            if (!$scope.sender.id_card) {
                result += language.id_card + ', ';
                sign = 2;
            }
            if (!$scope.sender.card_expire) {
                result += language.card_expire + ', ';;
                sign = 2;
            }
            if (!$scope.sender.birthday) {
                result += language.birthday + ', ';;
                sign = 2;
            }
            if (!$scope.sender.career) {
                result += language.career + ', ';;
                sign = 2;
            }
            if ($scope.sender.card_expire && !check_card_expire) {
                result += language.card_expire + ', ';;
                sign = 2;
            }
            if (relationship === undefined || relationship === null || relationship === '') {
                result += language.relationship + ', ';;
                sign = 2;
            }
            if ($scope.container.reason === undefined || $scope.container.reason === null) {
                resultreason += language.reason + ', ';;
                sign = 3;
            }
            return [sign, result, result3000, resultreason];
        };
        $scope.checkInfo_amount = function(amount, relationship) {
            let result = true;
            var cusinfo_check = $scope.checkInfo(amount, relationship)
            if (amount >= 3000 && cusinfo_check[0] >= 1) {
                popup.msg(language.mts_amount3000 + ': ' + cusinfo_check[1] + cusinfo_check[2] + cusinfo_check[3]);
                return false;
            } else if (amount >= 850 && cusinfo_check[0] >= 2) {
                popup.msg(language.mts_amount1000 + ': ' + cusinfo_check[1] + cusinfo_check[3]);
                return false;
            } else if (cusinfo_check[0] >= 3) {
                popup.msg(language.mts_reason + cusinfo_check[3]);
                return false;
            }
            return result;
        }
        $scope.createMts = function() {
            var check_amount = $scope.checkInfo_amount($scope.container.amount, $scope.receiver.relationship);
            if (!check_amount) {
                return false;
            }
            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                container: $scope.container
            };

            $scope.submitted = true;
            $http({
                method: 'POST',
                url: '/mts/store',
                data: data
            }).then(
                function(resp) {
                    location.href = '/mts?complete=1';
                },
                function(resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function(error, field) {
                            if (field.indexOf('sender') !== -1) {
                                $scope.errors.sender.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('container') !== -1) {
                                $scope.errors.container.push(error[0]);
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    }
                    if (resp.status === 500) {
                        $scope.errors.system = [];
                        $scope.errors.system.push(resp.data);
                        angular.element('#modal-errors').modal('show');
                    }
                }
            ).finally(function() {
                $scope.submitted = false;
            });
        }
    });
}());
