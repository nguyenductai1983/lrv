var user = {};
user.login = function () {
    fly.submit({
        id: 'login-form',
        service: '/customer/login',
        success: function (result) {
            if (result.success) {
                window.location = result.data;
            }
        }
    });
};

user.register = function () {
    fly.submit({
        id: 'register-form',
        service: '/customer/register',
        success: function (result) {
            if (result.success) {
                $('#dang-ky').modal('hide');
                popup.msg(result.message);
            }
        }
    });
};

user.forgot = function () {
    fly.submit({
        id: 'request-pass-form',
        service: '/customer/forgot-password',
        success: function (result) {
            if (result.success) {
                $('#lay-lai-mat-khau').modal('hide');
                popup.msg(result.message);
            }
        }
    });
};

user.resetPassword = function () {
    fly.submit({
        id: 'reset-form',
        service: '/customer/reset-password',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function () {
                    window.location = result.data;
                });
            }
        }
    });
};

user.contact = function () {
    fly.submit({
        id: 'form-contact',
        service: '/contact',
        success: function (result) {
            popup.msg(result.message, function () {
                location.reload();
            });
        }
    });
};
user.registerLink = function () {
    fly.submit({
        id: 'register-form',
        service: '/customer/register',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message, function(){
                    location.href = '/';
                });
            }
        }
    });
};

user.forgotLink = function () {
    fly.submit({
        id: 'request-pass-form',
        service: '/customer/forgot-password',
        success: function (result) {
            if (result.success) {
                popup.msg(result.message);
            }
        }
    });
};
