var customer = {};

customer.init = function () {
    setTimeout(function () {
        customer.getInfo();
    }, 100);

    $('select[name=country_id]').change(function(){
        customer.loadProvince();
        customer.loadDistrict();
        customer.loadWard();
    });

    $('select[name=province_id]').change(function(){
        customer.loadDistrict();
        customer.loadWard();
    });
};

customer.getInfo = function () {
    customer.loadCountry();
    customer.loadProvince();
    customer.loadDistrict();
    customer.loadWard();
};

customer.loadCountry = function () {
    var html = '<option value="">' + language.pls_country + '</option>';
    $.each(countries, function (key, value) {
        if (this.id == currentCountryId) {
            html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
        } else {
            html += '<option value="' + this.id + '">' + this.name + '</option>';
        }
    });
    $("select[name=country_id]").html(html);
};

customer.loadProvince = function () {
    var id = 0;
    id = $('select[name=country_id]').val();
    var html = '<option value="">' + language.pls_province + '</option>';
    $.each(provinces, function (key, value) {
        if (this.country_id == id) {
            if (this.id == currentProviceId) {
                html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
            } else {
                html += '<option value="' + this.id + '">' + this.name + '</option>';
            }
        }

    });
    $("select[name=province_id]").html(html);
};

customer.loadDistrict = function () {
    var id = 0;
    id = $('select[name=province_id]').val();
    var html = '<option value="">' + language.pls_district + '</option>';
    $.each(districts, function () {
        if (this.province_id == id) {
            if (this.id == currentDistrictId) {
                html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
            } else {
                html += '<option value="' + this.id + '">' + this.name + '</option>';
            }
        }
    });
    $("select[name=city_id]").html(html);
};
customer.loadWard = function () {
    var id = 0;
    id = $('select[name=city_id]').val();
    var html = '<option value="">' + language.pls_district + '</option>';
    $.each(wards, function () {
        if (this.city_id == id) {
            if (this.id == currentWardId) {
                html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
            } else {
                html += '<option value="' + this.id + '">' + this.name + '</option>';
            }
        }
    });
    $("select[name=ward_id]").html(html);
};
customer.profile = function () {
    fly.submit({
        id: 'profile-form',
        service: '/profile/update',
        success: function (result) {
            var html = '';
            if (result.success) {
                html = '<div class="alert-1 alert alert-success text-center">'+ result.message +'</div>';
            }else{
                var noti_error='';
                Object.keys(result.errors).forEach(function (item) {
                    noti_error+= '<br/>'+result.errors[item]; // key
                });
                html = '<div class="alert-1 alert alert-danger text-center">'+ result.message + noti_error + '</div>';
            }
            $('#profile-content-alert').html(html);
        }
    });
};

customer.changePassword = function () {
    fly.submit({
        id: 'change-password-form',
        service: '/profile/update-change-password',
        success: function (result) {
            var html = '';
            if (result.success) {
                html = '<div class="alert-1 alert alert-success text-center">'+ result.message +'</div>';
            }else{
                html = '<div class="alert-1 alert alert-danger text-center">'+ result.message +'</div>';
            }
            $('#change-password-content-alert').html(html);
        }
    });
};

customer.delete = function (id) {
    popup.confirm(language.address_delete_comfirm, function () {
        fly.ajax({
            service: '/profile/' + id + '/address-destroy',
            method: "GET",
            loading: true,
            success: function (result) {
                if (result.success) {
                    location.reload();
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};

customer.convertPoint = function (url) {
    popup.confirmPoint(language.get_coupon_code, function () {
        fly.ajax({
            service: '/profile/get-coupon-code',
            method: "GET",
            loading: true,
            success: function (result) {
                if (result.success) {
                    window.location = url;
                }else{
                    popup.msg(result.message);
                }
            }
        });
    });
};
