(function () {
    'use strict';
    var app = angular.module('TransportApp', []);
    var body_weight = 139;
    app.controller('TransportCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.orderCode = null;
        $scope.showMoreInfoCustomer = true;
        $scope.initErrors = function () {
            $scope.errors = {
                sender: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();
        $scope.countries = [];
        $scope.products = [];
        $scope.container = [];
        $scope.sender = {
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.initContainer = function (data) {
            var container = {
                products: [],
                length: data.container.length,
                width: data.container.width,
                height: data.container.height,
                shipping_fee: data.container.shipping_fee,
                total_weight: data.container.total_weight,
                total_shipping_fee: data.container.total_shipping_fee,
                total_declared_value: data.container.total_declared_value,
                total_surcharge: data.container.total_surcharge,
                total_insurance: data.container.total_insurance,
                total: data.container.total,
                coupon_code: data.container.coupon_code,
                coupon_amount: parseFloat(data.container.coupon_amount)
            };
            for (var i = 0; i < data.container.products.length; ++i) {
                var product = data.container.products[i];
                if (!product) {
                    product = {
                        product_id: null,
                        code: null,
                        name: null,
                        quantity: null,
                        weight: 0,
                        declared_value: null,
                        surcharge: null,
                        is_insurance: false,
                        insurance: null,
                        total: null,
                        note: null,
                        product_list: $scope.products
                    };
                }else{
                    product.product_list = $scope.products;
                }
                container.products.push(product);
            }

            return container;
        };

        $scope.getOrder = function () {
            $http({
                method: 'get',
                url: '/transport/orders/' + $scope.id
            }).then(
                function (resp) {
                    $scope.init(resp.data.order);
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.init = function (order) {
            $scope.sender = order.sender;
            $scope.receiver = order.receiver;
            $scope.orderCode = order.container.code;
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/products?service=1&status=1'
                }),
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                    $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                    $scope.receiver.province_id
                })
            ]).then(function (resp) {
                $scope.products = resp[0].data.products;
                $scope.countries = resp[1].data.countries;
                $scope.provincesSender = resp[2].data.provinces;
                $scope.citiesSender = resp[3].data.cities;
                $scope.provincesReceiver = resp[4].data.provinces;
                $scope.citiesReceiver = resp[5].data.cities;

                $scope.container = $scope.initContainer(order);

                $scope.updateContainers(true);
            });
        };

        $scope.getProvincesSender = function () {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + country_id
            }).then(
                    function (resp) {
                        $scope.provincesSender = resp.data.provinces;
                        $scope.getCitiesSender();
                    }
            );
        };

        $scope.getCitiesSender = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                function (resp) {
                    $scope.citiesSender = resp.data.cities;
                    $scope.updateContainers(false);
                }
            );
        };

        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                    function (resp) {
                        $scope.provincesReceiver = resp.data.provinces;
                        $scope.getCitiesReceiver();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                    function (resp) {
                        $scope.citiesReceiver = resp.data.cities;
                    }
            );
        };

        $scope.searchProducts = function (product, idDropdown) {
            var code = product.code ? product.code.toLowerCase().trim() : '',
                    dropdown = angular.element('#' + idDropdown);
            angular.forEach(product.product_list, function (item) {
                item.show = (item.code.toLowerCase().indexOf(code) !== -1);
            });

            var products = product.product_list.filter(function (item) {
                return !!item.show;
            });

            if (products.length > 0) {
                if (!dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr:visible').eq(0).trigger('mouseenter');
                }, 151);
            } else {
                if (dropdown.hasClass('open')) {
                    setTimeout(function () {
                        dropdown.find('.dropdown-toggle').dropdown('toggle');
                    }, 150);
                }
                setTimeout(function () {
                    dropdown.find('.dropdown-menu .table tbody tr').removeClass('hover');
                }, 151);
            }
        };

        $scope.selectProduct = function (product, item) {
            product.product_id = item.id;
            product.code = item.code;
            product.name = item.name;
            product.by_weight = item.by_weight;
            product.price = parseFloat(item.sale_price);
            product.agency_unit_id = item.agency_unit_id;
            product.agency_currency_id = item.agency_currency_id;
            product.agency_surcharge_fee = item.agency_surcharge_fee;
            product.agency_per_insurrance = item.agency_per_insurrance;
            product.description = item.description;
            product.quantity = 1;
            product.weight = 0;
            product.unit = item.messure_unit_code;
            $scope.updateContainers();
        };

        $scope.updateVolume = function (container) {
            if (!isNaN(container.length) && container.length > 0
                    && !isNaN(container.width) && container.width > 0
                    && !isNaN(container.height) && container.height > 0) {
                container.volume = (parseFloat(container.length) * parseFloat(container.width) *
                    parseFloat(container.height)) / body_weight;
            } else {
                container.volume = 0;
            }
        };

        $scope.updateContainers = function () {
            var container = $scope.container;
            container.shipping_fee = 0;
            container.total_shipping_fee = 0;
            container.total_weight = 0;
            container.total_declared_value = 0;
            container.total_surcharge = 0;
            container.total_insurance = 0;
            container.total_amount = 0;
            container.total_discount = 0;
            container.total = 0;

            var city;
            if ($scope.receiver.city_id) {
                angular.forEach($scope.citiesReceiver, function (item) {
                    if (item.id === $scope.receiver.city_id) {
                        city = item;
                        return true;
                    }
                });
            }
            angular.forEach(container.products, function (product) {
                if (product.product_id) {
                    var quantity = 0;
                    if (product.by_weight === 0) {
                        if (!isNaN(product.quantity) && product.quantity > 0) {
                            quantity = parseInt(product.quantity);
                            product.quantity = quantity;
                        }
                    } else {
                        if (!isNaN(product.weight) && product.weight > 0) {
                            quantity = parseFloat(product.weight);
                        }
                    }

                    if (!isNaN(product.weight) && product.weight > 0) {
                        container.total_weight += parseFloat(product.weight);
                    }

                    product.amount = parseFloat(product.price) * quantity;
                    product.surcharge = 0;
                    product.insurance = 0;
                    if (!isNaN(product.declared_value) && product.declared_value > 0) {
                        if (quantity > 0) {
                            var quota = product.declared_value / quantity;
                            if (quota > $scope.configSurcharge.quota) {
                                product.surcharge = parseFloat(product.declared_value) * $scope.configSurcharge.percent / 100;
                            }
                        }
                        container.total_surcharge += product.surcharge;
                        container.total_declared_value += parseFloat(product.declared_value);

                        if (product.is_insurance) {
                            product.insurance = parseFloat(product.declared_value) * $scope.configInsurance.percent / 100;
                            container.total_insurance += product.insurance;
                        }
                    }
                    product.total = parseFloat(product.amount) + parseFloat(product.surcharge) + parseFloat(product.insurance);
                    container.total += product.total;
                }
            });
            container.shipping_fee = city && city.shipping_fee ? parseFloat(city.shipping_fee) : 0;
            container.total_shipping_fee = container.shipping_fee * container.total_weight;
            container.total += container.total_shipping_fee;
            if(container.coupon_amount > 0){
                container.total -= container.coupon_amount;
            }
            if(container.total < 0){
                container.total = 0;
            }
        };
    });
}());
