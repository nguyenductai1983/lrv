(function () {
    'use strict';

    var app = angular.module('QuoteApp', []);

    app.controller('QuoteEditController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submittedConfirm = false;
        $scope.bntConfirm = false;

        $scope.initErrors = function () {
            $scope.errors = {
                sender: [],
                receiver: [],
                containers: []
            };
        };
        $scope.initErrors();
        $scope.countries = [];
        $scope.methods = [];
        $scope.quote = [];
        $scope.package_type = "1";

        $scope.sender = {
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: null
        };

        $scope.initContainers = function (order) {
            if (!order) {
                order = {
                    payment_status: null,
                    order_status: null,
                    total_final: 0,
                    total_goods: 0,
                    total_shop_fee: 0,
                    total_shipping_fee: 0,
                    total_delivery_fee: 0,
                    total_paid_amount: 0,
                    last_paid_amount: 0,
                    last_date_pay: null,
                    last_payment_at: null,
                    pay_method: 'VIETCOMBANK',
                    user_note: '',
                    containers: []
                };
            }
            for (var i = 0; i < 7; ++i) {
                var product = order.containers[i];
                if (!product) {
                    product = {
                        id: null,
                        url: null,
                        name: null,
                        quantity: null,
                        price: null,
                        shop_fee: null,
                        shipping_fee: null,
                        delivery_fee: null,
                        coupon: null,
                        note: null
                    };
                    order.containers.push(product);
                }
            }
            return order;
        };

        $scope.getQuote = function () {
            $http({
                method: 'get',
                url: '/quotes/' + $scope.id
            }).then(
                function (resp) {
                    $scope.init(resp.data.order);
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.init = function (order) {
            $scope.sender = order.sender;
            $scope.receiver = order.receiver;
            $scope.package_type = order.package_type;
            if(order.order_status == 1){
                $scope.bntConfirm = true;
            }
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                    $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                    $scope.receiver.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/payment-methods?type=2'
                })
            ]).then(function (resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesSender = resp[1].data.provinces;
                $scope.citiesSender = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.citiesReceiver = resp[4].data.cities;
                $scope.methods = resp[5].data.methods;

                $scope.quote = $scope.initContainers(order);
            });
        };

        $scope.getProvincesSender = function () {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + country_id
            }).then(
                    function (resp) {
                        $scope.provincesSender = resp.data.provinces;
                        $scope.getCitiesSender();
                    }
            );
        };

        $scope.getCitiesSender = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                function (resp) {
                    $scope.citiesSender = resp.data.cities;
                }
            );
        };

        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                    function (resp) {
                        $scope.provincesReceiver = resp.data.provinces;
                        $scope.getCitiesReceiver();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                    function (resp) {
                        $scope.citiesReceiver = resp.data.cities;
                    }
            );
        };

        $scope.editQuote = function () {
            $scope.submittedConfirm = true;
            var containers = angular.copy($scope.quote.containers);
            containers = containers.filter(function (product) {
                return !!product.url;
            });

            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                package_type: $scope.package_type,
                containers: containers
            };
            $http({
                method: 'PUT',
                url: '/quotes/' + $scope.id + "/update",
                data: data
            }).then(
                    function (resp) {
                        location.href = '/quotes';
                    },
                    function (resp) {
                        if (resp.status === 422) {
                            $scope.initErrors();
                            angular.forEach(resp.data.errors, function (error, field) {
                                if (field.indexOf('customer') !== -1) {
                                    $scope.errors.customer.push(error[0]);
                                } else if (field.indexOf('receiver') !== -1) {
                                    $scope.errors.receiver.push(error[0]);
                                } else if (field.indexOf('containers') !== -1) {
                                    for (var i = 0; i < containers.length; ++i) {
                                        if (typeof $scope.errors.container[i] === 'undefined') {
                                            $scope.errors.container[i] = {
                                                others: [],
                                                products: []
                                            };
                                        }
                                        if (field.indexOf('containers.' + i) === -1) {
                                            continue;
                                        }
                                        var errorPushed = false;
                                        for (var j = 0; j < containers[i].products.length; ++j) {
                                            if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                                $scope.errors.container[i].products[j] = [];
                                            }
                                            if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                                continue;
                                            }
                                            $scope.errors.container[i].products[j].push(error[0]);
                                            errorPushed = true;
                                        }
                                        if (!errorPushed) {
                                            $scope.errors.container[i].others.push(error[0]);
                                        }
                                    }
                                }
                            });
                            angular.element('#modal-errors').modal('show');
                        } else {
                            console.log(resp.statusText);
                        }
                    }
            ).finally(function () {
                $scope.submittedConfirm = false;
            });
        };
    });
}());
