(function () {
    'use strict';

    var app = angular.module('MtsApp', []);

    app.controller('MtsEditController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.mts = null;

        $scope.countries = [];
        $scope.mts_code = '';

        $scope.addressSearchResult = [];
        $scope.addressSearch = {
            customer_id: null
        };

        $scope.initContainer = function () {
            $scope.container = {
                id: null,
                reason: null,
                pay_date: $scope.date_now,
                status: 1,
                transaction_type: 1,
                payment_by: 1,
                amount: 0,
                discount_type: 1,
                discount_number: 0,
                transfer_fee: 0,
                total: 0
            };
        };

        $scope.initContainer();

        $scope.$watchCollection('customer.image_1_file_id', function (newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.checkInfoCustomer();
        });

        $scope.checkInfoCustomer = function () {
            $scope.isEditDiscount = true;

            if (!$scope.sender.id_card) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.card_expire) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.birthday) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.career) {
                $scope.isEditDiscount = false;
            } else if (!$scope.sender.image_1_file_id) {
                $scope.isEditDiscount = false;
            }

        };

        $scope.getMts = function () {
            $http({
                method: 'get',
                url: '/mts/' + $scope.id
            }).then(
                function (resp) {
                    $scope.mts = resp.data.mts;
                    $scope.sender = $scope.mts.customer;
                    for (var i = 1; i <= 3; ++i) {
                        var img = '';
                        var imgControl = angular.element('input[name="customer.image_' + i +
                                '_file_id"]').parents('.img-control').eq(0);
                        var imgPreview = imgControl.find('.img-preview');
                        if($scope.sender['image' + i]){
                            img = $scope.sender['image' + i].path;
                        }
                        if (!img) {
                            imgControl.find('.img-thumbnail').attr('src', '/images/admin/user-2.png');
                        }else{
                            imgControl.find('.img-thumbnail').attr('src', img);
                        }
                        imgPreview.show();
                    }
                    $scope.receiver = $scope.mts.receiver;
                    $scope.selectedId =  $scope.mts.receiver.id;
                    $scope.mts_code = $scope.mts.code;
                    $scope.addressSearch.customer_id = $scope.sender.id;
                    $scope.searchAddress();
                    $scope.init();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };


        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                    $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/provinces?country_id=' + $scope.receiver.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                    $scope.receiver.province_id
                }),
                $http({
                    method: 'get',
                    url: '/api/admin/currencies'
                })
            ]).then(function (resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesCustomer = resp[1].data.provinces;
                $scope.citiesCustomer = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.citiesReceiver = resp[4].data.cities;
                $scope.currencies = resp[5].data.currencies;
                $scope.updateContainer($scope.mts);
            });
        };

        $scope.getProvincesCustomer = function (customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + country_id
            }).then(
                function (resp) {
                    $scope.provincesCustomer = resp.data.provinces;
                    $scope.getCitiesCustomer(country_id)
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getCitiesCustomer = function (customer) {
            var country_id = customer && customer.country_id ? customer.country_id : $scope.sender.country_id;
            var province_id = customer && customer.province_id ? customer.province_id : $scope.sender.province_id;
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + country_id + '&province_id=' + province_id
            }).then(
                function (resp) {
                    $scope.citiesCustomer = resp.data.cities;
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };


        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                function (resp) {
                    $scope.provincesReceiver = resp.data.provinces;
                    $scope.getProvincesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            );
        };

        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' +
                $scope.receiver.province_id
            }).then(
                function (resp) {
                    $scope.citiesReceiver = resp.data.cities;
                },
                function (resp) {
                    console.log(resp);
                    console.log(resp.statusText);
                }
            );
        };

        $scope.selectedId = null;
        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url: '/api/address?customer_id=' + $scope.addressSearch.customer_id
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.address;
                    if($scope.addressSearchResult.length > 0){
                        $scope.pickAddress($scope.addressSearchResult[0].id);
                    }
                    // $scope.getCitiesReceiver();
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                    $scope.submittedSearchAddress = false;
            });
        };

        function search(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function (address_id) {
            var address = 0;
            if($scope.addressSearchResult.length > 0 && address_id >0){
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.selectAddress = function (address) {
            if (address) {
                $scope.receiver = address;
                $scope.selectId = address.id;
            }
            if (address == 0) {
                $scope.removeAddress();
            }
            $scope.getProvincesReceiver();

        };

        $scope.removeAddress = function () {
            $scope.provincesReceiver = [];
            $scope.citiesReceiver = [];
            $scope.receiver = {
                id:null,
                first_name: null,
                middle_name: null,
                last_name: null,
                address_1: null,
                address_2: null,
                telephone: null,
                cellphone: null,
                post_code: null,
                city_id: null,
                province_id: null,
                country_id: null
            };
        };

        $scope.updateContainer = function (container) {
            $scope.container.id = container.id;
            $scope.container.reason = container.reason;
            $scope.container.status = container.mts_status;
            $scope.container.transaction_type = container.transaction_type;
            $scope.container.payment_by = container.payment_by;
            $scope.container.amount = container.total_goods;
            $scope.container.discount_type = container.discount_type;
            $scope.container.discount_number = container.discount_number;
            $scope.container.transfer_fee = container.transfer_fee;
            $scope.container.total = container.total_final;
            $scope.container.currency = container.currency_id;
            $scope.container.pay_date = container.pay_date;
        };

        //Bsung mới tính toán giá trị per fee
        $scope.getPerFee = function () {
            let vnPriorityFlag = false;
            let perFee = $scope.mts_max_per_fee;
            if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;
            }
            if (vnPriorityFlag) {
                perFee = $scope.mts_per_fee;
            }
            return perFee;
        };

        $scope.updateFee = function (container) {
            // lấy giá trị phù hợp nhất
            let result = $scope.MTSSurcharge.filter(word => word.amount<=container.amount);
             result = result[result.length - 1];
             // hết đoạn xử lý chi phí
             var rate_number=result['rate'];
             let vnPriorityFlag = false;
             if ($scope.vnPriority.indexOf($scope.receiver.province_id) != -1) {
                vnPriorityFlag = true;            }
            // nếu ngoài thành phố hồ chí minh
            if (!vnPriorityFlag) {
                rate_number=parseFloat(rate_number+0.5);
            }
             rate_number =(parseFloat(rate_number)*parseFloat(container.amount))/100;
             var fee=result['fee'];
             var surcharge=result['surcharge']*container.amount/100;
             var total_fee=rate_number+fee+surcharge;
             var discount_number = parseFloat(container.discount_number)>0 ? parseFloat(container.discount_number) :0 ;
            if(discount_number > 0 && container.discount_type == 1){
                discount_number = (parseFloat(container.amount) * parseFloat(container.discount_number)) / 100;
            }else if(discount_number > 0 && container.discount_type == 2){
                discount_number = parseFloat(container.discount_number);
            }
            if(discount_number>total_fee )
            {
                discount_number=total_fee ;
            }
            total_fee=total_fee-discount_number;
            var commission=parseFloat(total_fee)*$scope.commission;
            $scope.container.transfer_fee=total_fee.toFixed(2);
            $scope.container.commission=commission.toFixed(2);
            $scope.container.total=(parseFloat(container.amount)+parseFloat(total_fee)).toFixed(2);
            // var discount_amount = 0;
            //  var fee_hight = 0;
            // var discount_number = parseFloat(container.discount_number);
            // if(discount_number > 0 && container.discount_type == 1){
            //     discount_amount = (parseFloat(container.amount) * parseFloat(container.discount_number)) / 100;
            // }else if(discount_number > 0 && container.discount_type == 2){
            //     discount_amount = parseFloat(container.discount_number);
            // }
            // if (parseFloat(container.amount)>3000)
            // {
            //     fee_hight=$scope.fix_fee3000;
            // }
            // else if (parseFloat(container.amount)>1500)
            // {
            //     fee_hight=$scope.fix_fee1500;
            // }
            // $scope.container.transfer_fee = (($scope.getPerFee() * parseFloat(container.amount)) + $scope.fix_fee) - parseFloat(discount_amount)+fee_hight;
            // $scope.container.total = parseFloat($scope.container.amount) + parseFloat($scope.container.transfer_fee);
        };
    });
}());
