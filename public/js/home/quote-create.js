(function () {
    'use strict';

    var app = angular.module('QuoteApp', []);

    app.controller('QuoteCreateController', function ($scope, $document, $httpParamSerializer, $http, $q) {
        $scope.submittedConfirm = false;
        $scope.bntConfirm = true;

        $scope.initErrors = function () {
            $scope.errors = {
                sender: [],
                receiver: [],
                container: []
            };
        };
        $scope.initErrors();

        $scope.countries = [];
        $scope.products = [];
        $scope.package_type = "1";

        $scope.sender = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: 14
        };

        $scope.receiver = {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            address_1: null,
            address_2: null,
            telephone: null,
            cellphone: null,
            postal_code: null,
            city_id: null,
            province_id: null,
            country_id: 47
        };

        $scope.initContainers = function () {
            var containers = [];
            for (var i = 1; i <= 7; ++i) {
                var product = {
                    url: null,
                    name: null,
                    quantity: null,
                    price: null,
                    coupon: null,
                    note: null
                };
                containers.push(product);
            }

            return containers;
        };

        $scope.containers = $scope.initContainers();
        $scope.init = function () {
            $q.all([
                $http({
                    method: 'get',
                    url: '/api/countries'
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.sender.country_id
                }),
                $http({
                    method: 'get',
                    url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' +
                    $scope.sender.province_id
                }),
                $http({
                    method: 'get',
                    url   : '/api/provinces?country_id=' + $scope.receiver.country_id
                })
            ]).then(function (resp) {
                $scope.countries = resp[0].data.countries;
                $scope.provincesSender = resp[1].data.provinces;
                $scope.citiesSender = resp[2].data.cities;
                $scope.provincesReceiver = resp[3].data.provinces;
                $scope.addressSearch.customer_id = $scope.sender.id;
                $scope.searchAddress();
            });
        };

        $scope.addressSearchResult = [];
        $scope.selectedId = null;
        $scope.addressSearch = {
            customer_id      : null
        };

        $scope.searchAddress = function () {
            $http({
                method: 'get',
                url   : '/api/address?customer_id=' + $scope.addressSearch.customer_id
            }).then(
                function (resp) {
                    $scope.addressSearchResult = resp.data.address;
                    if($scope.addressSearchResult.length > 0){
                        $scope.selectedId = $scope.addressSearchResult[0].id;
                        $scope.pickAddress($scope.addressSearchResult[0].id);
                    }
                },
                function (resp) {
                    console.log(resp.statusText);
                }
            ).finally(function () {
                    $scope.submittedSearchAddress = false;
                });
        };
        $scope.selectAddress = function (address) {
            if (address == 0) {
                $scope.provincesReceiver = [];
                $scope.citiesReceiver = [];
                $scope.receiver = {
                    id:null,
                    first_name: null,
                    middle_name: null,
                    last_name: null,
                    address_1: null,
                    address_2: null,
                    telephone: null,
                    cellphone: null,
                    post_code: null,
                    city_id: null,
                    province_id: null,
                    country_id: null
                };
            }
            if (address) {
                $scope.receiver = address;
            }
            $scope.getProvincesReceiver();

        };

        function search(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].id == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.pickAddress = function(address_id){
            var address = 0;
            if($scope.addressSearchResult.length > 0 && address_id >0){
                address = search(address_id, $scope.addressSearchResult);
            }
            $scope.selectAddress(address);
        };

        $scope.getProvincesSender = function () {
            var country_id = $scope.sender.country_id;
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + country_id
            }).then(
                    function (resp) {
                        $scope.provincesSender = resp.data.provinces;
                        $scope.getCitiesSender();
                    }
            );
        };

        $scope.getCitiesSender = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.sender.country_id + '&province_id=' + $scope.sender.province_id
            }).then(
                    function (resp) {
                        $scope.citiesSender = resp.data.cities;
                    }
            );
        };

        $scope.getPostCodeSender = function () {
            angular.forEach($scope.citiesSender, function(value, key){
                if(value.id == $scope.sender.city_id){
                    $scope.sender.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getPostCodeReceiver = function () {
            angular.forEach($scope.citiesReceiver, function(value, key){
                if(value.id == $scope.receiver.city_id){
                    $scope.receiver.postal_code = value.postal_code;
                    return;
                }
            });
        };

        $scope.getProvincesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/provinces?country_id=' + $scope.receiver.country_id
            }).then(
                    function (resp) {
                        $scope.provincesReceiver = resp.data.provinces;
                        $scope.getCitiesReceiver();
                    },
                    function (resp) {
                        console.log(resp.statusText);
                    }
            );
        };

        $scope.getCitiesReceiver = function () {
            $http({
                method: 'get',
                url: '/api/cities?country_id=' + $scope.receiver.country_id + '&province_id=' + $scope.receiver.province_id
            }).then(
                    function (resp) {
                        $scope.citiesReceiver = resp.data.cities;
                    }
            );
        };

        $scope.createQuote = function () {
            $scope.submittedConfirm = true;
            var containers = angular.copy($scope.containers);
            containers = containers.filter(function (product) {
                return !!product.url;
            });

            var data = {
                sender: $scope.sender,
                receiver: $scope.receiver,
                package_type: $scope.package_type,
                containers: containers
            };
            $http({
                method: 'POST',
                url: '/quotes/store',
                data: data
            }).then(
                function (resp) {
                    location.href = '/quotes/create';
                },
                function (resp) {
                    if (resp.status === 422) {
                        $scope.initErrors();
                        angular.forEach(resp.data.errors, function (error, field) {
                            if (field.indexOf('customer') !== -1) {
                                $scope.errors.customer.push(error[0]);
                            } else if (field.indexOf('receiver') !== -1) {
                                $scope.errors.receiver.push(error[0]);
                            } else if (field.indexOf('containers') !== -1) {
                                for (var i = 0; i < containers.length; ++i) {
                                    if (typeof $scope.errors.container[i] === 'undefined') {
                                        $scope.errors.container[i] = {
                                            others: [],
                                            products: []
                                        };
                                    }
                                    if (field.indexOf('containers.' + i) === -1) {
                                        continue;
                                    }
                                    var errorPushed = false;
                                    for (var j = 0; j < containers[i].products.length; ++j) {
                                        if (typeof $scope.errors.container[i].products[j] === 'undefined') {
                                            $scope.errors.container[i].products[j] = [];
                                        }
                                        if (field.indexOf('containers.' + i + '.products.' + j) === -1) {
                                            continue;
                                        }
                                        $scope.errors.container[i].products[j].push(error[0]);
                                        errorPushed = true;
                                    }
                                    if (!errorPushed) {
                                        $scope.errors.container[i].others.push(error[0]);
                                    }
                                }
                            }
                        });
                        angular.element('#modal-errors').modal('show');
                    } else {
                        console.log(resp.statusText);
                    }
                }
            ).finally(function () {
                $scope.submittedConfirm = false;
            });
        };
    });
}());
