var claim = {};

claim.addConversation = function () {
    fly.submit({
        id: 'claim-comment-form',
        service: '/claim/create-conversation',
        success: function (result) {
            $('textarea[name=comment]').val('');
            if (result.success) {
                var content =   '<div class="customer-conversation">'
                                + '<label>' + result.data.content + '</label>'
                                + '<span class="claim-conversation-time">' + result.data.created_at + '</span>'
                                + '</div>';
                $("#claim-conversation-content").append(content);
            } else {
                popup.msg(result.message);
            }
        }
    });
    
};

claim.open = function (id, token){
    popup.open('open-claim', language.claim_open, template('claim/open.tpl', {
        id: id, 
        token: token
    }), [
        {
            title: language.popup_confirm,
            style: 'btn-primary',
            fn: function () {
                fly.submit({
                    id: 'open-claim-form',
                    service: '/claim/open',
                    success: function (result) {
                        popup.close('open-claim');
                        if (result.success) {
                            location.reload();
                        } else {
                            popup.msg(result.message);
                        }
                    }
                });
            }
        },
        {
            title: language.popup_btn_close,
            style: 'btn-default',
            fn: function () {
                popup.close('open-claim');
            }
        }
    ], 'modal-lg');
};