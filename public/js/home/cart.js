var cart = {};

cart.applyCoupon = function () {
    fly.submit({
        id: 'coupon-form',
        service: '/api/used-coupon',
        success: function (result) {
            if (result.success) {
                $('#bnt-usecoupon').css('display', 'none');
                $('#bnt-removecoupon').css('display', 'block');
                $('#coupon-amount').text(result.data.coupon_amount  + ' CAD');
                $('#total-final').text(result.data.total_final  + ' CAD');
            }else{
                popup.msg(result.message);
            }
        }
    });
};

cart.removeCoupon = function () {
    fly.submit({
        id: 'coupon-form',
        service: '/api/remove-coupon',
        success: function (result) {
            if (result.success) {
                $('#bnt-usecoupon').css('display', 'block');
                $('#bnt-removecoupon').css('display', 'none');
                $('#coupon-amount').text('0.00 CAD');
                $('#total-final').text(result.data.total_final + ' CAD');
            }else{
                popup.msg(result.message);
            }
        }
    });
};

cart.addItem = function (id) {
    fly.submit({
        id: 'cart-additem-form-' + id,
        service: '/cart/add-item',
        success: function (result) {
            popup.msg(result.message);
            if (result.success) {
                $('#cart-count').text(result.data);
            }
        }
    });
};

cart.removeItem = function (rowId) {
    fly.ajax({
        service: '/cart/remove-item',
        method: "GET",
        loading: true,
        data: {
            rowId: rowId
        },
        success: function (result) {
            if (result.success) {
                $('#cart-count').text(result.data);
                $('#product-item-' + rowId).remove();
                if (result.data == 0) {
                    var html = '<tr><td colspan="6" class="guide-text">' + language.cart_empty + '</td></tr>';
                    $('#cart-step1-content').html(html);
                    $('#cart-step1-tfoot').remove();
                }
            } else {
                popup.msg(result.message);
            }
        }
    });
};

cart.updateItem = function (rowId, quantity) {
    fly.ajax({
        service: '/cart/update-item',
        method: "GET",
        loading: true,
        data: {
            rowId: rowId,
            quantity: quantity
        },
        success: function (result) {
            if (result.success) {
                $('#item-price-' + rowId).text(common.formatMoney(result.data.price, 2) + ' CAD');
                $('#item-subprice-' + rowId).text(common.formatMoney(result.data.sub_total, 2) + ' CAD');
                $('#total-price').text(common.formatMoney(result.data.total, 2) + ' CAD');
            } else {
                popup.msg(result.message);
            }
        }
    });
};

cart.step2 = function () {
    fly.submit({
        id: 'cart-form-step2',
        service: '/cart/create-order',
        success: function (result) {
            if (result.success) {
                window.location = result.data;
            }else{
                popup.msg(result.message);
            }
        }
    });
};

cart.step3 = function () {
    var agree = $('input[name=Agree]').is(":checked");
    if (!agree) {
        popup.msg(language.agree_access);
        return false;
    }
    var paymentMethod = $('input[name=payment_method]').val();
    if (!paymentMethod) {
        popup.msg(language.what_payment_method);
        return false;
    }
    fly.submit({
        id: 'payment-form',
        service: '/cart/approve-order',
        success: function (result) {
            if (result.success) {
                window.location = result.data;
            }else{
                popup.msg(resp.message);
            }
        }
    });
};

cart.init = function () {
    setTimeout(function () {
        cart.loadCountry();
    }, 100);
};

cart.loadCountry = function () {
    var html = '<option value="">' + language.pls_country + '</option>';
    $.each(countries, function (key, value) {
        if (this.id == customer.country_id) {
            html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
        } else {
            html += '<option value="' + this.id + '">' + this.name + '</option>';
        }
    });
    $("select[name=BuyerCountryId]").html(html);
    $("select[name=ReceiverCountryId]").html(html);
   cart.loadProvince(true);
  cart.loadProvince(false);
};

cart.loadProvince = function (buyer) {
    var id = 0;
    if (buyer) {
        id = $('select[name=BuyerCountryId]').val();
    } else {
        id = $('select[name=ReceiverCountryId]').val();
        if(id==='91'){
            document.getElementById("ward").style.display = "block";
        }
        else
        {
            document.getElementById("ward").style.display = "none";
        }
    }
    var html = '<option value="">' + language.pls_province + '</option>';
    fly.ajax({
        service: '/api/provinces?country_id='+ id,
        method: "GET",
        loading: false,
        success: function (result) {
            $.each(result.provinces, function (key, value) {
                if (this.id == customer.province_id) {
                    html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
                } else {
                    html += '<option value="' + this.id + '">' + this.name + '</option>';
                }
            });
            if (buyer) {
                $("select[name=BuyerProvinceId]").html(html);
                cart.loadDistrict(true);
            } else {
                $("select[name=ReceiverProvinceId]").html(html);
                cart.loadDistrict(false);
            }
        }
    });
};

cart.loadDistrict = function (buyer) {
    var countryId = 0;
    var provinceId = 0;
    if (buyer) {
        countryId = $('select[name=BuyerCountryId]').val();
        provinceId = $('select[name=BuyerProvinceId]').val();
    } else {
        countryId = $('select[name=ReceiverCountryId]').val();
        provinceId = $('select[name=ReceiverProvinceId]').val();
    }
    var html = '<option value="">' + language.pls_district +  '</option>';
    fly.ajax({
        service: '/api/cities?country_id=' + countryId + '&province_id=' + provinceId,
        method: "GET",
        loading: false,
        success: function (result) {
        //    var flag = false;
            $.each(result.cities, function (key, value) {
                if (this.id == customer.city_id) {
                    flag = true;
                    html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
                } else {
                    html += '<option value="' + this.id + '">' + this.name + '</option>';
                }
            });
            if (buyer) {
                $("select[name=BuyerDistrictId]").html(html);
            } else {
                $("select[name=ReceiverDistrictId]").html(html);
                if(countryId==='91'){
                    document.getElementById("ward").style.display = "block";
                    cart.loadWard(false);
                }
                else
                {
                    document.getElementById("ward").style.display = "none";
                }
                // if(flag){
                //     cart.calShippingFee();
                // }
            }
        }
    });
};
// them ham get ward
cart.loadWard = function (buyer) {
    var DistrictId = '';
    if (buyer) {
        DistrictId = $('select[name=BuyerDistrictId]').val();
    } else {
        DistrictId = $('select[name=ReceiverDistrictId]').val();
    }
    var html = '<option value="">' + language.pls_ward +  '</option>';
    fly.ajax({
        service: '/api/wards?city_id=' + DistrictId,
        method: "GET",
        loading: false,
        success: function (result) {
            var flag = false;
            $.each(result.wards, function (key, value) {
                if (this.id == customer.ward_id) {
                    flag = true;
                    html += '<option value="' + this.id + '" selected>' + this.name + '</option>';
                } else {
                    html += '<option value="' + this.id + '">' + this.name + '</option>';
                }
            });
            if (buyer) {
                $("select[name=BuyerWardId]").html(html);
            } else {
                $("select[name=ReceiverWardId]").html(html);
            }
        }
    });
    if (!buyer && DistrictId>0 ) {
    cart.calShippingFee();  }
};
cart.calShippingFee = function () {
    var districtId = parseInt($("select[name=ReceiverDistrictId]").val());

    fly.ajax({
        service: '/cart/shipping-local',
        method: "GET",
        loading: true,
        data: {
            districtId: districtId
        },
        success: function (result) {
            if (result.success) {
                $('#shipping-price').text(common.formatMoney(result.data.shipping_fee, 2) + ' CAD');
                $('#total-final').text(common.formatMoney(result.data.total, 2) + ' CAD');
            } else {
                popup.msg(result.message);
            }
        }
    });
};

cart.chooseMethod = function (methodName){
    $('input[name=payment_method]').val(methodName);
};
