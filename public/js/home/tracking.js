var tracking = {};

tracking.viewHistory = function (id) {
    fly.ajax({
        service: '/transport/orders/'+ id +'/view-history',
        method: "GET",
        loading: true,
        success: function (result) {
            if (result.success) {
                popup.open('view-history', language.view_history_shipping, template('transport/view_history.tpl', {
                    data: result.data
                }), [
                    {
                        title: language.popup_btn_close,
                        style: 'btn',
                        fn: function () {
                            popup.close('view-history');
                        }
                    }
                ], 'modal-lg', '900px');
            }else{
                popup.msg(result.message);
            }
        }
    });
};