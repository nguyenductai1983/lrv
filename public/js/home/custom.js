/**
 * Get cookie
 *
 * @param name
 * @returns {*}
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function toUrlEncodedString(data) {
    var body = '';
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (body.length) {
                body += '&';
            }
            body += key + '=';
            body += encodeURIComponent(data[key]);
        }
    }
    return body;
}

var setSelect2 = function () {
    $('select.select2').each(function (i, el) {
        var $this = $(el),
                opts = {
                    placeholder: $(this).data('placeholder'),
                    allowClear: attrDefault($this, 'allowClear', false),
                    minimumResultsForSearch: $(this).data('results-search')
                };

        $this.select2(opts).on('select2-open', function () {
            $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
        });
        $this.addClass('visible');
    });
};

$(function () {
    
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (settings.type === 'POST' || settings.type === 'PUT' || settings.type === 'DELETE') {
                xhr.setRequestHeader("X-XSRF-TOKEN", getCookie('XSRF-TOKEN'));
            }
        }
    });
    /**
     * Image control
     */
    $('.img-control input[type=file]').fileupload({
        maxNumberOfFiles: 1,
        url: '/files/upload?type=image',
        method: 'post',
        dataType: 'json',
        formData: {},
        add: function (e, data) {
            $(this).prop('disabled', true)
                    .parents('.form-group')
                    .eq(0)
                    .removeClass('validate-has-error')
                    .find('.form-control+span').remove();
            $(this).parent().find('div.has-error').remove();
            $('<i class="fa fa-refresh fa-spin"></i>').insertAfter($(this));
            data.submit();
        },
        done: function (e, data) {
            var file = data.result,
                    parent = $(this).parent(),
                    imgPreview = parent.find('.img-preview'),
                    scope = $(this).scope();

            imgPreview.find('img').attr('src', file.path);
            imgPreview.show();
            parent.find('input[type=hidden][class*=name]').val(file.id);
            parent.find('input[type=hidden][class*=path]').val(file.path);

            if (scope) {
                var attrName = parent.find('input[type=hidden][class*=name]').attr('name'),
                        names = attrName.split('.');

                if (names.length === 1) {
                    scope[names[0]] = file.id;
                } else if (names.length === 2) {
                    scope[names[0]][names[1]] = file.id;
                }

                if (!scope.$$phase) {
                    scope.$apply();
                }
            }
        },
        fail: function (e, data) {
            if (data.jqXHR.status === 422) {
                $(this).parents('.form-group').eq(0).addClass('validate-has-error');
                $('<span>' + data.jqXHR.responseJSON.errors.file[0] + '</span>').insertAfter($(this));
            } else {
                console.log(data);
            }
        },
        always: function (e, data) {
            $(this).prop('disabled', false).parent().find('.fa-spin').remove();
        }
    });

    $('.img-control .remove').click(function () {
        var parent = $(this).parent(),
                scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');
        parent.parent().find('input[type=hidden]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                    names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });

    $('.img-control1 input[type=file]').change(function () {
        if (this.files && this.files[0]) {
            $('.img-control1 .img-preview').css('display', 'block');
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-preview-content').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.img-control1 .remove').click(function () {
        var parent = $(this).parent(),
                scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                    names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });
    
    $('.img-control-vi input[type=file]').change(function () {
        if (this.files && this.files[0]) {
            $('.img-control-vi .img-preview').css('display', 'block');
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-preview-content-vi').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.img-control-vi .remove').click(function () {
        var parent = $(this).parent(),
                scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                    names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });
    
    $('.img-control-en input[type=file]').change(function () {
        if (this.files && this.files[0]) {
            $('.img-control-en .img-preview').css('display', 'block');
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-preview-content-en').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.img-control-en .remove').click(function () {
        var parent = $(this).parent(),
                scope = $(this).scope();
        parent.hide().find('img').attr('src', '#');
        parent.parent().find('input[type=file]').val('');

        if (scope) {
            var attrName = parent.parent().find('input[type=hidden][class*=name]').attr('name'),
                    names = attrName.split('.');

            if (names.length === 1) {
                scope[names[0]] = null;
            } else if (names.length === 2) {
                scope[names[0]][names[1]] = null;
            }

            if (!scope.$$phase) {
                scope.$apply();
            }
        }
    });
});