<?php

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
 use Illuminate\Support\Facades\Route;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();
// Route::get('/order-status', function() {
//     $exitCode = Artisan::call('order:status');
//     return '<h1>Order status</h1>';
// });

// Route::get('/order-user', function() {
//     $exitCode = Artisan::call('order:user');
//     return '<h1>Order user</h1>';
// });

// Route::get('/mts-user', function() {
//     $exitCode = Artisan::call('mts:user');
//     return '<h1>Mts user</h1>';
// });

// Route::get('/product-agency', function() {
//     $exitCode = Artisan::call('product:agency');
//     return '<h1>Product agency</h1>';
// });

// Route::get('/order-shipaddress', function() {
//     $exitCode = Artisan::call('order:shipaddress');
//     return '<h1>Order shipaddress</h1>';
// });

// Route::get('/order-address', function() {
//     $exitCode = Artisan::call('order:address');
//     return '<h1>Order address</h1>';
// });

// Route::get('/import-update', function() {
//     $exitCode = Artisan::call('excel:import-update');
//     return '<h1>Migrate</h1>';
// });

// Route::get('/migrate-db', function() {
//     $exitCode = Artisan::call('migrate');
//     return '<h1>Migrate</h1>';
// });
// Storage link facade value:
// Route::get('/storage-link', function() {
//     $exitCode = Artisan::call('storage:link');
//     return '<h1>Storage facade value link</h1>';
// });
// Clear Cache facade value:
// Route::get('/clear-cache', function() {
//     $exitCode = Artisan::call('cache:clear');
//     return '<h1>Cache facade value cleared</h1>';
// });

//Reoptimized class loader:
// Route::get('/optimize', function() {
//     $exitCode = Artisan::call('optimize');
//     return '<h1>Reoptimized class loader</h1>';
// });

//Route cache:
// Route::get('/route-cache', function() {
//     $exitCode = Artisan::call('route:cache');
//     return '<h1>Routes cached</h1>';
// });

// //Clear Route cache:
// Route::get('/route-clear', function() {
//     $exitCode = Artisan::call('route:clear');
//     return '<h1>Route cache cleared</h1>';
// });

// //Clear View cache:
// Route::get('/view-clear', function() {
//     $exitCode = Artisan::call('view:clear');
//     return '<h1>View cache cleared</h1>';
// });

//Clear Config cache:
// Route::get('/config-cache', function() {
//     $exitCode = Artisan::call('config:cache');
//     return '<h1>Clear Config cleared</h1>';
// });
Route::get('/vnptconfirm', 'Vnptonline@index')->name('vnptconfirm.index');
Route::post('/vnptconfirm', 'Vnptonline@store')->name('vnptconfirm');
// Route::post('/vnpttest/GetAccessToken', 'vnpt@GetAccessToken')->name('vnpttest.GetAccessToken');
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/estimate/transport', 'EstimateController@index')->name('estimate.transport');
//Router web online
Route::get('/transport/online', 'TransportOnlineController@create')->name('transport.online.create');
Route::post('/transport/online/store', 'TransportOnlineController@store');
Route::post('/transport/online/getquote', 'TransportOnlineController@getQuotes');
//Router web online
Route::get('/password/reset/{token}', 'Admin\Auth\AdminResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/update', 'Admin\Auth\AdminResetPasswordController@reset')->name('adminpassword.update');

Route::get('/order/tracking', 'OrderController@index')->name('order.tracking');
Route::get('/contact', 'ContactController@index')->name('contact.index');
Route::post('/contact', 'ContactController@store')->name('contact.store');

Route::get('/auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.login');
Route::get('/auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');

Route::group(['prefix' => 'pages'], function () {
    Route::get('/{slug}', 'PagesController@show')->name('pages.show');
});

Route::group(['prefix' => 'api/admin', 'middleware' => ['auth:admin']], function () {
    Route::get('/{id}/customer', 'Admin\ApiController@getInfoCustomer');
    Route::get('/{id}/receiver', 'Admin\ApiController@getInfoReceiver');
    Route::post('/update-info-customer', 'Admin\ApiController@updateInfoCustomer');
    Route::post('/update-info-receiver', 'Admin\ApiController@updateInfoReceiver');
    Route::get('/provincesagency', 'Admin\ApiController@getProvincesagency');
    Route::get('/minfeeagency', 'Admin\ApiController@getminFeeagency');
    Route::get('/currencies', 'Admin\ApiController@getCurrencies');
    Route::get('/warehouses', 'Admin\ApiController@getWarehouses');
    Route::get('/countries', 'Admin\ApiController@getCountries');
    Route::get('/countriescode', 'Admin\ApiController@getCountriesCode');
    Route::get('/provinces', 'Admin\ApiController@getProvinces');
    Route::get('/cities', 'Admin\ApiController@getCities');
    Route::get('/wards', 'Admin\ApiController@getWards');
    Route::get('/products', 'Admin\ApiController@getProducts');
    Route::get('/dim-types', 'Admin\ApiController@getDimTypes');
    Route::get('/payment-methods', 'Admin\ApiController@getPaymentMethods');
    Route::get('/customers', 'Admin\ApiController@getCustomers');
    Route::get('/addresses', 'Admin\ApiController@getAddresses');
    Route::get('/address', 'Admin\ApiController@getAddressByCustomer');
    Route::get('/address', 'Admin\ApiController@getAddressByCustomer');
    Route::post('/createcontact', 'Admin\ApiController@createContact');

    Route::post('/create-extra', 'Admin\ApiController@createExtra');
    Route::post('/create-refund', 'Admin\ApiController@createRefund');

    Route::get('/transport-agency-order', 'Admin\ApiController@transportAgencyOrder');
    Route::get('/transport-customer-quote', 'Admin\ApiController@transportCustomerQuote');
    Route::get('/transport-customer-order', 'Admin\ApiController@transportCustomerOrder');

    Route::get('/yhl-agency-order', 'Admin\ApiController@yhlAgencyOrder');
    Route::get('/yhl-customer-order', 'Admin\ApiController@yhlCustomerOrder');

    Route::get('/express-agency-order', 'Admin\ApiController@expressAgencyOrder');
    Route::get('/express-customer-order', 'Admin\ApiController@expressCustomerOrder');

    Route::get('/mts-agency-order', 'Admin\ApiController@mtsAgencyOrder');
    Route::get('/mts-customer-order', 'Admin\ApiController@mtsCustomerOrder');

    Route::get('/quote-shop', 'Admin\ApiController@quoteShop');
});
Route::group(['prefix' => 'api', 'middleware' => ['web']], function () {
    Route::get('/warehouses', 'ApiController@getWarehouses');
    Route::get('/countries', 'ApiController@getCountries');
    Route::get('/provinces', 'ApiController@getProvinces')->name('api.provinces');
    Route::get('/cities', 'ApiController@getCities')->name('api.cities');
    Route::get('/wards', 'ApiController@getWards')->name('api.wards');;
    Route::get('/products', 'ApiController@getProducts');
    Route::get('/dim-types', 'ApiController@getDimTypes');
    Route::get('/payment-methods', 'ApiController@getPaymentMethods');
    Route::get('/address', 'ApiController@getAddress');
    Route::post('/coupon', 'ApiController@couponAmount');
    Route::post('/used-coupon', 'ApiController@usedCoupon');
    Route::post('/remove-coupon', 'ApiController@removedCoupon');
    Route::get('/currencies', 'ApiController@getCurrencies');
});

Route::group(['prefix' => 'files'], function () {
    Route::post('/upload', 'FilesController@upload')->name('files.upload');
});

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('customer.verify');
Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('customer.reset');

Route::group(['prefix' => 'agency'], function() {
    Route::get('/', 'AgencyController@index')->name('agency.index');
});

Route::group(['prefix' => 'quotes', 'middleware' => 'auth'], function() {
    Route::get('/', 'QuoteController@index')->name('quote.frontend.index');
    Route::get('/create', 'QuoteController@create')->name('quote.frontend.create');
    Route::post('/store', 'QuoteController@store');
    Route::get('/{id}', 'QuoteController@show')->name('quote.frontend.show');
    Route::put('/{id}/update', 'QuoteController@update')->name('quote.frontend.update');
    Route::get('/{id}/edit', 'QuoteController@edit')->name('quote.frontend.edit');
});

Route::group(['prefix' => 'transports', 'middleware' => 'auth'], function() {
    Route::get('/', 'TransportController@index')->name('transport.frontend.index');
    Route::get('/create', 'TransportController@create')->name('transport.frontend.create');
    Route::post('/store', 'TransportController@store');
    Route::post('/get-quote', 'TransportController@getQuotes');
    Route::get('/{id}', 'TransportController@show')->name('transport.frontend.show');
    Route::put('/{id}/update', 'TransportController@update')->name('transport.frontend.update');
    Route::get('/{id}/edit', 'TransportController@edit')->name('transport.frontend.edit');
    Route::get('/{id}/print_bill', 'TransportController@printBill')->name('transport.frontend.print');
    Route::get('/{id}/get-pdf', 'TransportController@getPdf')->name('transport.frontend.pdf');
    Route::get('/{id}/view-history-eshipper', 'TransportController@viewHistoryEshipper');
});

Route::group(['prefix' => 'transport/orders', 'middleware' => 'auth'], function() {
    Route::get('/', 'TransportOrderController@index')->name('transport.order.list');
    Route::get('/{id}', 'TransportOrderController@show')->name('transport.order.show');
    Route::put('/{id}/update', 'TransportOrderController@update')->name('transport.order.update');
    Route::get('/{id}/edit', 'TransportOrderController@edit')->name('transport.order.edit');
    Route::get('/{id}/print_bill', 'TransportOrderController@printBill')->name('transport.order.print');
     Route::get('/{id}/print_bills', 'TransportOrderController@printBills')->name('transport.order.prints');
    Route::get('/{id}/view-history', 'TransportOrderController@viewHistory');
     Route::get('/{id}/get-pdf', 'TransportOrderController@getPdf')->name('transport.order.pdf');
});

Route::group(['prefix' => 'yhl', 'middleware' => 'auth'], function () {
    Route::get('/', 'YhlController@index')->name('yhl.frontend.index');
});

Route::group(['prefix' => 'mts', 'middleware' => 'auth'], function () {
    Route::get('/', 'MtsController@index')->name('mts.frontend.index');
    Route::get('/create', 'MtsController@create')->name('mts.frontend.create');
    Route::post('/store', 'MtsController@store');
    Route::get('/{id}', 'MtsController@show')->name('mts.frontend.show');
    Route::put('/{id}/update', 'MtsController@update')->name('mts.frontend.update');
    Route::get('/{id}/edit', 'MtsController@edit')->name('mts.frontend.edit');
});

Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function () {
    Route::get('/', 'CustomerController@index')->name('profile.index');
    Route::get('/change-password', 'CustomerController@changePassword')->name('profile.changepass');
    Route::get('/address', 'CustomerController@address')->name('profile.address');
    Route::get('/point', 'CustomerController@point')->name('profile.point');
    Route::get('/coupon', 'CustomerController@coupon')->name('profile.coupon');
    Route::get('/get-coupon-code', 'CustomerController@getCouponCode');
    Route::get('/{id}/address-edit', 'CustomerController@addressEdit')->name('profile.address_edit');
    Route::put('/{id}/address-update', 'CustomerController@addressUpdate')->name('profile.address_update');
    Route::get('/address-create', 'CustomerController@addressCreate')->name('profile.address_create');
    Route::put('/address-store', 'CustomerController@addressStore')->name('profile.address_store');
    Route::get('/{id}/address-destroy', 'CustomerController@addressDestroy')->name('profile.address_destroy');
    Route::post('/update', 'CustomerController@update')->name('profile.update');
    Route::post('/update-change-password', 'CustomerController@updateChangePassword')->name('profile.update_changepass');
});

Route::prefix('product')->group(function() {
    Route::get('/', 'ProductController@index')->name('product.index');
    Route::get('/category/{id}', 'ProductController@category')->name('product.category');
    Route::get('/detail/{name}/{id}', 'ProductController@detail')->name('product.detail');
});

Route::prefix('news')->group(function() {
    Route::get('/', 'NewsController@index')->name('news.index');
    Route::get('/{slug}', 'NewsController@detail')->name('news.detail');
});

Route::prefix('helps')->group(function() {
    Route::get('/', 'HelpsController@index')->name('helps.index');
    Route::get('/{slug}', 'HelpsController@detail')->name('helps.detail');
});

Route::prefix('cart')->group(function() {
    Route::get('/step1', 'CartController@step1')->name('cart.step1');
    Route::get('/step2', 'CartController@step2')->name('cart.step2');
    Route::get('/step3/{code}', 'CartController@step3')->name('cart.step3');
    Route::get('/step4/{code}', 'CartController@step4')->name('cart.step4');
    Route::get('/confirm/{code}', 'CartController@confirm')->name('cart.confirm');

    Route::post('/add-item', 'CartController@addItem');
    Route::get('/remove-item', 'CartController@removeItem');
    Route::get('/update-item', 'CartController@updateItem');
    Route::get('/shipping-local', 'CartController@shippingLocal');
    Route::post('/create-order', 'CartController@createOrder');
    Route::post('/approve-order', 'CartController@approveOrder');
});

Route::group(['prefix' => 'claim', 'middleware' => 'auth'], function () {
    Route::get('/', 'ClaimController@index')->name('claim.frontend.index');
    Route::get('/{id}', 'ClaimController@view')->name('claim.frontend.view');
    Route::post('/create-conversation', 'ClaimController@createConversation');
    Route::post('/open', 'ClaimController@open');
});

Route::prefix('customer')->group(function() {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('customer.login');
    Route::get('/forgot-password', 'Auth\LoginController@showForgotForm')->name('customer.forgot');
    Route::get('/register', 'Auth\RegisterController@showRegisterForm')->name('customer.register');

    Route::post('/login-submit', 'Auth\LoginController@loginSubmit')->name('customer.login.submit');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/register', 'Auth\RegisterController@register')->name('customer.register.submit');
    Route::post('/forgot-password', 'Auth\ForgotPasswordController@forgot')->name('customer.forgot.submit');
    Route::post('/reset-password', 'Auth\ResetPasswordController@reset');
    Route::get('/logout', 'Auth\LoginController@logout')->name('customer.logout');
});

Route::group(['prefix' => 'admin/quotes', 'middleware' => 'auth:admin'], function() {
    Route::get('/', 'Admin\QuoteController@index')->name('quote.admin.index');
    Route::get('/{id}', 'Admin\QuoteController@show')->name('quote.admin.show');
    Route::put('/{id}/update', 'Admin\QuoteController@update')->name('quote.admin.update');
    Route::post('/{id}/cancel', 'Admin\QuoteController@cancel')->name('quote.admin.cancel');
    Route::get('/{id}/edit', 'Admin\QuoteController@edit')->name('quote.admin.edit');
});
Route::prefix('admin')->group(function() {
 Route::get('/password/forgot-pass', 'Admin\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.forgot-pass');
 Route::post('/password/email', 'Admin\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
 Route::post('/password/reset', 'Admin\Auth\AdminResetPasswordController@showLinkRequestForm')->name('admin.password.reset');
 Route::get('/password/expired', 'Admin\Auth\ExpiredPasswordController@expired')->name('admin.password.expired');
 Route::post('password/post_expired', 'Admin\Auth\ExpiredPasswordController@postExpired')->name('admin.password.post_expired');
 Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
 Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
 Route::post('/logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout.submit');
 Route::get('/logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    // Profile thay đổi MK, thông tin
    Route::get('/profile/info', 'Admin\ProfileController@info')->name('admin.profile.info');
    Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('admin.profile.change-password');
    Route::get('/profile/passwordexpired', 'Admin\ProfileController@PasswordExpired')->name('admin.profile.passwordexpired');
    Route::put('/profile/info-update', 'Admin\ProfileController@infoUpdate')->name('admin.profile.info-update');
    Route::put('/profile/change-password-update', 'Admin\ProfileController@changePasswordUpdate')->name('admin.profile.change-password-update');
    // cập nhật check passwords
    Route::middleware(['password_expired'])->group(function () {
    Route::get('/', 'Admin\AdminController@index')->name('admin.index');
    });
    Route::post('/config', 'Admin\ConfigController@config')->name('admin.config');
    Route::post('/pages/{id}/destroy', 'Admin\PagesController@destroy')->name('admin.page.destroy');

    Route::resource('product-groups', 'Admin\ProductGroupsController', [
        'except' => ['show'],
        'parameters' => [
            'product-groups' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-product-groups');

    Route::get('/checkAuth', function () {
        return true;
    });
    Route::resource('products', 'Admin\ProductsController', [
        'except' => ['show'],
        'parameters' => [
            'products' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-products');

    Route::resource('countries', 'Admin\CountriesController', [
        'except' => ['show'],
        'parameters' => [
            'countries' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-countries');

    Route::resource('provinces', 'Admin\ProvincesController', [
        'except' => ['show'],
        'parameters' => [
            'provinces' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-provinces');

    Route::resource('cities', 'Admin\CitiesController', [
        'except' => ['show'],
        'parameters' => [
            'cities' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-cities');

    Route::resource('wards', 'Admin\WardsController', [
        'except' => ['show'],
        'parameters' => [
            'wards' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-wards');

    Route::resource('pages', 'Admin\PagesController', [
        'except' => ['show'],
        'parameters' => [
            'pages' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-pages');

    Route::resource('banners', 'Admin\BannerController', [
        'except' => ['show'],
        'parameters' => [
            'banners' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-banners');

    Route::resource('news', 'Admin\NewsController', [
        'except' => ['show'],
        'parameters' => [
            'news' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-news');

    Route::resource('helps', 'Admin\HelpsController', [
        'except' => ['show'],
        'parameters' => [
            'helps' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-news');

    Route::put('/bulletin/{id}/updatepage', 'Admin\BulletinController@updatepage')->name('admin.bulletin.updatepage');;
    Route::resource('bulletin', 'Admin\BulletinController', [
        'parameters' => [
            'bulletin' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-bulletin');

    Route::resource('areas', 'Admin\AreasController', [
        'except' => ['show'],
        'parameters' => [
            'areas' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-areas');

    Route::resource('point-converts', 'Admin\PointConvertController', [
        'except' => ['show'],
        'parameters' => [
            'point-converts' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-point-converts');

    Route::resource('point-configs', 'Admin\PointConfigController', [
        'except' => ['show'],
        'parameters' => [
            'point-configs' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-point-configs');

    Route::resource('dimensions', 'Admin\DimensionUnitController', [
        'except' => ['show'],
        'parameters' => [
            'dimensions' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-dimensions');

    Route::resource('measures', 'Admin\MessureUnitController', [
        'except' => ['show'],
        'parameters' => [
            'measures' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-measures');
    Route::get('warehouses/{id}/list_product', 'Admin\WarehouseController@list_product')->name('warehouses.list_product');
    Route::resource('warehouses', 'Admin\WarehouseController', [
        'except' => ['show'],
        'parameters' => [
            'warehouses' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-warehouses');
    Route::resource('weights', 'Admin\WeightUnitController', [
        'except' => ['show'],
        'parameters' => [
            'weights' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-weights');

    Route::resource('shipping-methods', 'Admin\ShippingMethodController', [
        'except' => ['show'],
        'parameters' => [
            'shipping-methods' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-shipping-methods');

    Route::resource('shipping-providers', 'Admin\ShippingProviderController', [
        'except' => ['show'],
        'parameters' => [
            'shipping-providers' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-shipping-providers');
    // them ngay 15-07-2021
    Route::resource('shipping-method-providers', 'Admin\ShippingMethodProviderController', [
        'except' => ['show'],
        'parameters' => [
            'shipping-method-providers' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-shipping-providers');
    // het them ngay 15-07-2021
    Route::resource('payment-methods', 'Admin\PaymentMethodController', [
        'except' => ['show'],
        'parameters' => [
            'payment-methods' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-payment-methods');

    Route::get('/vnptconfirm', 'Admin\Vnptconfirm@index')->name('admin.vnptconfirm.index');
    Route::post('/vnptconfirm', 'Admin\Vnptconfirm@store')->name('admin.vnptconfirm');
    Route::post('/extent/approve', 'Admin\ExtentController@approve');
    Route::post('/extent/payment', 'Admin\ExtentController@payment');
    Route::post('/extent/cancel', 'Admin\ExtentController@cancel');
    Route::get('/extent', 'Admin\ExtentController@index')->name('admin.extent.index');
    Route::get('/extent/exportExcel', 'Admin\ExtentController@exportExcel')->name('admin.extent.exportExcel');
    Route::get('/extrafee/{orderid}/list', 'Admin\ExtentController@extrafee')->name('admin.extent.extrafee');

    Route::resource('extent', 'Admin\ExtentController', [
        'except' => ['show'],
        'parameters' => [
            'extent' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-extent');

    Route::get('/claim/view/{id}', 'Admin\ClaimController@view')->name('admin.claim.view');
    Route::post('/claim/create-conversation', 'Admin\ClaimController@createConversation');
    Route::post('/claim/complete', 'Admin\ClaimController@complete');
    Route::get('/claim/contact', 'Admin\ClaimController@contact')->name('admin.claim.contact');
    Route::get('/claim/contact/{id}/delete', 'Admin\ClaimController@delete')->name('admin.claim.delete');
    Route::resource('claim', 'Admin\ClaimController', [
        'except' => ['show'],
        'parameters' => [
            'claim' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-claim');

    Route::get('/receipts/excel', 'Admin\ReceiptController@exportExcel')->name('admin.receipts.excel');
    Route::resource('receipts', 'Admin\ReceiptController', [
        'except' => ['show'],
        'parameters' => [
            'receipts' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-receipts');

    Route::post('/receipt-orders/add-items', 'Admin\ReceiptOrdersController@addItems')->name('admin.receipt-orders.add-items');
    Route::post('/receipt-orders/remove-item', 'Admin\ReceiptOrdersController@removeItem')->name('admin.receipt-orders.remove-item');
    Route::post('/receipt-orders/paid-voucher', 'Admin\ReceiptOrdersController@paidVoucher')->name('admin.receipt-orders.paid-voucher');
    Route::get('/receipt-orders/excel-item/{id}', 'Admin\ReceiptOrdersController@exportExcelItem')->name('admin.receipt-orders.excel-item');
    Route::get('/receipt-orders/excel', 'Admin\ReceiptOrdersController@exportExcel')->name('admin.receipt-orders.excel');
    Route::get('/receipt-orders/{id}/print', 'Admin\ReceiptOrdersController@printItem')->name('admin.receipt-orders.print-item');
    Route::resource('receipt-orders', 'Admin\ReceiptOrdersController', [
        'except' => ['show'],
        'parameters' => [
            'receipt-orders' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-receipts-order');

    Route::post('/receipt-mts/add-items', 'Admin\ReceiptMtsController@addItems')->name('admin.receipt-mts.add-items');
    Route::post('/receipt-mts/remove-item', 'Admin\ReceiptMtsController@removeItem')->name('admin.receipt-mts.remove-item');
    Route::post('/receipt-mts/paid-voucher', 'Admin\ReceiptMtsController@paidVoucher')->name('admin.receipt-mts.paid-voucher');
    Route::get('/receipt-mts/excel-item/{id}', 'Admin\ReceiptMtsController@exportExcelItem')->name('admin.receipt-mts.excel-item');
    Route::get('/receipt-mts/excel', 'Admin\ReceiptMtsController@exportExcel')->name('admin.receipt-mts.excel');
    Route::get('/receipt-mts/{id}/print', 'Admin\ReceiptMtsController@printItem')->name('admin.receipt-mts.print-item');
    Route::resource('receipt-mts', 'Admin\ReceiptMtsController', [
        'except' => ['show'],
        'parameters' => [
            'receipt-mts' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-receipts-mts');

    Route::post('/receipt-extra/add-items', 'Admin\ReceiptExtraController@addItems')->name('admin.receipt-extra.add-items');
    Route::post('/receipt-extra/remove-item', 'Admin\ReceiptExtraController@removeItem')->name('admin.receipt-extra.remove-item');
    Route::post('/receipt-extra/paid-voucher', 'Admin\ReceiptExtraController@paidVoucher')->name('admin.receipt-extra.paid-voucher');
    Route::get('/receipt-extra/excel-item/{id}', 'Admin\ReceiptExtraController@exportExcelItem')->name('admin.receipt-extra.excel-item');
    Route::get('/receipt-extra/excel', 'Admin\ReceiptExtraController@exportExcel')->name('admin.receipt-extra.excel');
    Route::get('/receipt-extra/{id}/print', 'Admin\ReceiptExtraController@printItem')->name('admin.receipt-extra.print-item');
    Route::resource('receipt-extra', 'Admin\ReceiptExtraController', [
        'except' => ['show'],
        'parameters' => [
            'receipt-extra' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-receipts-extra');

    Route::resource('customer-groups', 'Admin\CustomerGroupsController', [
        'except' => ['show'],
        'parameters' => [
            'customer-groups' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-customer-groups');

    Route::get('/customers/points', 'Admin\CustomersController@points')->name('admin.customers.points');
    Route::get('/customers/coupons', 'Admin\CustomersController@coupons')->name('admin.customers.coupons');
    Route::get('/customers/search', 'Admin\CustomersController@search');
    Route::get('/customers/{id}/vip', 'Admin\CustomersController@vip')->name('admin.customers.getvip');
    Route::put('/customers/{id}/vip', 'Admin\CustomersController@updateVip')->name('admin.customers.vip');
    Route::get('/customers/{id}/point', 'Admin\CustomersController@point')->name('admin.customers.point');
    Route::get('/customers/excel', 'Admin\CustomersController@exportExcel')->name('admin.customers.excel');
    Route::get('/customers/{id}/address', 'Admin\CustomersController@address')->name('admin.customers.address');
    Route::delete('/customers/{id}/address-destroy', 'Admin\CustomersController@addressDestroy')->name('admin.customers.address_destroy');
    Route::get('/customers/{id}/address-edit', 'Admin\CustomersController@addressEdit')->name('admin.customers.address_edit');
    Route::put('/customers/{id}/address-update', 'Admin\CustomersController@addressUpdate')->name('admin.customers.address_update');
    Route::put('/customers/{id}/lock', 'Admin\CustomersController@lock')->name('admin.customers.lock');
    Route::put('/customers/{id}/unlock', 'Admin\CustomersController@unlock')->name('admin.customers.unlock');
    Route::resource('customers', 'Admin\CustomersController', [
        'except' => ['show'],
        'parameters' => [
            'customers' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-customers');

    Route::resource('campaign', 'Admin\CampaignController', [
        'except' => ['show'],
        'parameters' => [
            'campaign' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-campaign');

    Route::resource('logs', 'Admin\LogsController', [
        'except' => ['show'],
        'parameters' => [
            'logs' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-logs');

    Route::get('/roles/{id}/permissions', 'Admin\RolesController@getPermissions')->name('admin.roles.permissions');
    Route::put('/roles/{id}/permissions', 'Admin\RolesController@updatePermissions');
    Route::resource('roles', 'Admin\RolesController', [
        'except' => ['show'],
        'parameters' => [
            'roles' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-roles');

    Route::get('/agencies/{id}/products', 'Admin\AgenciesController@getProducts')->name('admin.agencies.products');
    Route::put('/agencies/{id}/products', 'Admin\AgenciesController@updateProductsPrice');
    Route::resource('agencies', 'Admin\AgenciesController', [
        'except' => ['show'],
        'parameters' => [
            'agencies' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-agencies');

    Route::resource('users', 'Admin\UsersController', [
        'except' => ['show'],
        'parameters' => [
            'users' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-users');

    Route::get('/configs', 'Admin\ConfigsController@index')->name('admin.configs.index');
    Route::get('/phpinfo', 'Admin\ConfigsController@phpinfo')->name('admin.phpinfo');
    Route::put('/configs', 'Admin\ConfigsController@update')->name('admin.configs.update');

    Route::get('/currencies/{id}/rate', 'Admin\CurrenciesController@rate')->name('admin.currencies.rate');
    Route::resource('currencies', 'Admin\CurrenciesController', [
        'except' => ['show'],
        'parameters' => [
            'currencies' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-currencies');

    Route::resource('product-groups', 'Admin\ProductGroupsController', [
        'except' => ['show'],
        'parameters' => [
            'product-groups' => 'id'
        ],
        'as' => 'admin'
    ])->middleware('can:manage-product-groups');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
