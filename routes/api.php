<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/vnpost', 'CallbackController@vnpost')->name('callback.vnpost');
Route::post('/updateShipment', 'callbackGHTK@updateShipment')->name('callback.ghtk');
// Route::post('/http_response', 'callbackGHTK@receiveCurlData')->name('callback.receiveCurlData');
// Route::get('/updateShipment', 'callbackGHTK@updateShipment')->name('callback.ghtk');
