@extends('layouts.home.index')

@section('breadcrumbs')
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">{{ $page->translate->name }}</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="{{ url('/') }}">{{ __('home.home') }}</a></li>
                <li class="active">{{ $page->translate->name }}</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div id="main">{!! $page->translate->detail !!}</div>
    </div>
@endsection