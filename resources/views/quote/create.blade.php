@extends('layouts.home.user', ['menu' => $menu])

@section('content')
    <div class="panel panel-default transport-content" ng-app="QuoteApp" ng-controller="QuoteCreateController">
        <div class="transport-content-title panel-heading">
            {{ __('quote.create') }}
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-create" name="createForm" ng-submit="createQuote()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.sender.length || errors.receiver.length || errors.pickups.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.pickups.length">
                                            <h4>{{ __('express.pickup_info') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.pickups">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-gray">
                    <table class="transport-content-shipping table table-form">
                        <tbody>
                        <tr>
                            <td class="transport-content-sender">
                                <h3>{{ __('label.sender') }}</h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name">
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('customer.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name">
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()">
                                                <option value="">{{ __('label.select_country') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()">
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-change="getPostCodeSender()">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone">
                                        </td>
                                        <td class="col-label">
                                            {{ __('customer.qoute-type') }}
                                        </td>
                                        <td>
                                            <select ng-model="package_type" class="form-control">
                                                <option value="1">{{ __('customer.qoute-shop') }}</option>
                                                <option value="2">{{ __('customer.qoute-ship') }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="transport-content-receiver">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>
                                <div style="float: right;position: absolute;right: 55px;top: 85px;" ng-if="addressSearchResult">
                                    <select class="form-control" ng-model="selectedId"  ng-change="pickAddress(selectedId)">
                                        <option value="" selected="selected">{{ __('label.select_address') }}</option>
                                        <option ng-if="addressSearchResult" ng-repeat="item in addressSearchResult" ng-value="item.id">@{{item.address_1}}</option>
                                    </select>
                                </div>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('receiver.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.address') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address" ng-model="receiver.address_1" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" >
                                                <option value="">{{ __('label.select_address') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="updateContainers(true, true)">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.postal_code" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone" >
                                        </td>

                                        <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="transport-content-package-title">
                    <i class="fa fa-cubes"></i> {{ __('label.goods') }}
                </h3>

                <div class="transport-content-package">
                    <table class="table table-hover table-bordered table-container" style="max-width: 100%;width: 100%;">
                        <thead>
                        <tr>
                            <th width="30" class="text-center">#</th>
                            <th width="200">{{ __('product.url') }}</th>
                            <th width="100">{{ __('product.name') }}</th>
                            <th width="30" class="text-center">{{ __('label.quantity') }}</th>
                            <th width="30" class="text-center">{{ __('label.price') }}</th>
                            <th width="30" class="text-center">{{ __('label.coupon') }}</th>
                            <th width="150">{{ __('label.note') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="product in containers">
                            <td class="col-middle text-center" ng-bind="$index + 1"></td>
                            <td class="col-middle">
                                <input type="text" class="form-control input-center" ng-model="product.url">
                            </td>
                            <td class="col-middle">
                                <input type="text" class="form-control input-center" ng-model="product.name">
                            </td>
                            <td class="col-middle">
                                <input type="text" class="form-control text-center" ng-model="product.quantity">
                            </td>
                            <td class="col-middle">
                                <input type="text" class="form-control text-center" ng-model="product.price">
                            </td>
                            <td class="col-middle">
                                <input type="text" class="form-control text-center" ng-model="product.coupon">
                            </td>
                            <td class="col-middle">
                                <input type="text" class="form-control" ng-model="product.note">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="text-right">
                    <a href="{{ route('quote.frontend.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.cancel') }}
                    </a>
                    <button type="submit" class="btn btn-info" ng-if="bntConfirm" ng-click="createQuote()" ng-disabled="submittedConfirm">
                        <i class="fa fa-check"></i> {{ __('label.confirm') }} <i class="fa fa-refresh fa-spin" ng-if="submittedConfirm"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/home/quote-create.js?t=' . File::lastModified(public_path('js/home/quote-create.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-create').scope();
            scope.sender = {!! $customer->toJson() !!};

            scope.init();

            if (!scope.$$phase) {
                scope.$apply();
            }
        });
    </script>
@endsection
