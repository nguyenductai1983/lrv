@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('quote.orders_title')}}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('quote.frontend.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('quote.frontend.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('quote.frontend.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th width="150">{{ __('label.form_code') }}</th>
                            <th width="150">{{ __('customer.qoute-type') }}</th>
                            <th width="150" class="text-center">{{ __('label.date') }}</th>
                            <th width="120" class="text-center">{{ __('label.order_status') }}</th>
                            <th width="120" class="text-center">{{ __('label.payment_status') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                            <td>
                                {{ $order->code }}
                            </td>
                            <td>
                                {{ $order->package_type_name }}
                            </td>
                            <td class="text-center">{{ $order->created_at }}</td>
                            <td class="text-center">
                                <span class="{{ $order->status_label }}">{{ $order->status_name }}</span>
                            </td>
                            <td class="text-center">
                                <span class="{{ $order->payment_status_label }}">{{ $order->payment_status_name }}</span>
                            </td>
                            <td>
                                <a href="{{ route('quote.frontend.edit', $order->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <?php if($order->payment_status > 1){?>
                                <button type="button" class="btn btn-xs btn-warning" onclick="claim.open({{$order->id}}, '{{ csrf_token() }}');">
                                    <i class="fa fa-bullhorn"></i>
                                </button>
                                <?php }?>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $orders->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/home/claim.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
