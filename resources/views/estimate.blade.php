@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/estimate.css') }}">
@endsection
@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('label.estimate-fee') }}</a></li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="panel panel-default transport-content" ng-app="TransportApp" ng-controller="TransportCreateController">
        <div class="transport-content-title panel-heading">
            {{ __('transport.send-to-vn') }}
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-create" name="createForm" ng-submit="createTransport()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.sender.length || errors.receiver.length || errors.warehouse.length || errors.pickups.length || errors.quotes.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.warehouse.length">
                                            <h4>{{ __('label.warehouse_id') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.warehouse">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.pickups.length">
                                            <h4>{{ __('express.pickup_info') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.pickups">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.quotes.length">
                                            <h4>{{ __('label.eshipper_error') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.quotes">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-gray">
                    <table class="transport-content-shipping table table-form">
                        <tbody>
                        <tr>
                            <td class="transport-content-sender">
                                <h3>{{ __('label.sender') }}</h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name">
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('customer.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name">
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()">
                                                <option value="">{{ __('label.select_country') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()">
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-change="getPostCodeSender()">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone">
                                        </td>
                                        <td class="col-label">
                                            {{ __('express.ship_date') }}
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="shipping-date" name="sender.shipping_date" ng-model="sender.shipping_date">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="transport-content-receiver" style="position: relative;">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>
                                <?php if(!empty($customer)){ ?>
                                <div style="float: right;position: absolute;right: 20px;top: 10px;" ng-if="addressSearchResult">
                                    <select class="form-control" ng-model="selectedId"  ng-change="pickAddress(selectedId)">
                                        <option value="" selected="selected">{{ __('label.select_address') }}</option>
                                        <option ng-if="addressSearchResult" ng-repeat="item in addressSearchResult" ng-value="item.id">@{{item.address_1}}</option>
                                    </select>
                                </div>
                                <?php }?>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('receiver.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.address') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address" ng-model="receiver.address_1" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" >
                                                <option value="">{{ __('label.select_address') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="updateContainers(true, true)">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.postal_code" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone" >
                                        </td>

                                        <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="transport-content-package-title">
                    <i class="fa fa-cubes"></i> {{ __('label.goods') }}
                </h3>

                <div id="container" class="tabs-border transport-content-package">
                    <ul class="nav nav-tabs tabs-border">
                        <li ng-repeat="container in transport.containers" class="tab" ng-class="{'active' : $first}">
                            <a href="#container-@{{ $index + 1 }}" data-toggle="tab" target="_self">
                                <span>{{ __('label.container') }} @{{ $index + 1 }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript://" ng-click="addContainer()">
                                <span><i class="fa fa-plus"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="padding: 15px;">
                        <div ng-repeat="container in transport.containers" class="tab-pane" ng-class="{'active' : $first}" id="container-@{{ $index + 1 }}">
                            <div class="form-group">
                                <table class="table table-form table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="150" class="text-right col-middle">{{ __('label.total_weight') }}<i class="text-danger">*</i>:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.total_weight.toFixed(2)"></strong>
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.length') }}<i class="text-danger">*</i>:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.length" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.width') }}<i class="text-danger">*</i>:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.width" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.height') }}<i class="text-danger">*</i>:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.height" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="100" class="text-right col-middle">{{ __('label.volume') }}:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.volume.toFixed(2)"></strong>
                                        </td>
                                        <td class="col-middle">
                                            <button type="button" class="btn btn-info btn-xs" ng-click="setVolume(container)">
                                                <i class="fa fa-check"></i> {{ __('label.select') }}
                                            </button>
                                        </td>
                                        <td ng-if="containers.length > 1" class="col-middle">
                                            <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#modal-delete-container-@{{ $index }}">
                                                <i class="fas fa-trash"></i> {{ __('label.delete_container') }} @{{ $index + 1 }}
                                            </button>
                                            <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! __('label.confirm_delete_msg') !!}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger disabled-submit" ng-click="deleteContainer($index)">
                                                                <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                            </button>
                                                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-container" style="max-width: 100%;width: 100%;">
                                    <thead>
                                    <tr>
                                        <th width="30" class="text-center">#</th>
                                        <th width="100">{{ __('product.code') }}</th>
                                        <th width="100">{{ __('product.name') }}</th>
                                        <th width="30" class="text-center">{{ __('label.quantity') }}</th>
                                        <th width="30" class="text-center">{{ __('label.weight') }}</th>
                                        <th width="60" class="text-right">{{ __('label.price') }}</th>
                                        <th width="60" class="text-center">{{ __('label.unit_short') }}</th>
                                        <th width="30" class="text-right">{{ __('label.declared_value_short') }}</th>
                                        <th width="60" class="text-right">{{ __('label.surcharge') }}</th>
                                        <th width="60" class="text-center">{{ __('label.insurance_short') }}</th>
                                        <th width="60" class="text-right">{{ __('label.total') }}</th>
                                        <th width="150">{{ __('label.note') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in container.products">
                                        <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.code"
                                                   ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')">
                                            <div id="products-@{{ $parent.$index + '-' + $index }}-dropdown" class="dropdown products-dropdown">
                                                <button class="btn btn-primary dropdown-toggle hidden" type="button" data-toggle="dropdown"></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <table class="table table-bordered table-hover">
                                                            <tbody>
                                                            <tr ng-repeat="item in product.product_list" ng-style="!item.show && {display: 'none'}" ng-click="selectProduct(product, item)">
                                                                <td ng-bind="item.code"></td>
                                                                <td ng-bind="item.name"></td>
                                                                <td class="text-right" ng-bind="item.sale_price"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control input-readonly" ng-model="product.name" readonly="readonly">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.quantity" ng-change="updateContainers()">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.weight" ng-change="updateContainers()">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.price.toFixed(2)"></td>
                                        <td class="text-center col-middle" ng-bind="product.unit"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-right" ng-model="product.declared_value" ng-change="updateContainers()">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.surcharge.toFixed(2)"></td>
                                        <td class="text-center col-middle">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" class="iswitch iswitch-info" style="margin: 0;" ng-model="product.is_insurance" ng-change="updateContainers()">
                                                    </td>
                                                    <td ng-bind="product.insurance.toFixed(2)"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.total.toFixed(2)"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.note">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td class="text-right" width="15%"></td>
                                    <td class="text-left" width="19%"></td>
                                    <td class="col-middle text-right" width="15%">{{ __('label.coupon_code') }}:</td>
                                    <td class="text-left" width="18%">
                                        <div class="input-group">
                                            <input type="text" class="form-control" ng-model="container.coupon_code" ng-disabled="isUseCoupon">
                                            <div class="input-group-btn" ng-show="!isUseCoupon">
                                                <button type="button" class="btn btn-info" ng-click="applyCoupon(container)">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            <div class="input-group-btn" ng-show="isUseCoupon">
                                                <button type="button" class="btn btn-danger" ng-click="removeCoupon(container)">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                    <td class="text-left">
                                        <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" width="15%">{{ __('label.shipping_fee') }}:</td>
                                    <td class="text-left" width="19%"><strong ng-bind="container.shipping_fee.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_weight') }}:</td>
                                    <td class="text-left" width="18%"><strong ng-bind="container.total_weight.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_shipping_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_shipping_fee.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_declared_value') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_declared_value.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_surcharge') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_insurance') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_insurance.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td class="text-right">{{ __('label.total_amount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_amount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_pay') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total.toFixed(2)"></strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-map-marker"></i> {{ __('transport.point-receiver') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-form">
                            <tbody>
                                <tr>
                                    <td colspan="1">
                                        <strong>{{ __('label.choose-option') }}:</strong>
                                    </td>
                                </tr>
                                <tr ng-repeat="warehouse in warehouses">
                                    <td class="col-label" width="90%">
                                        <label>
                                            <input type="radio" name="warehouseId" ng-model="transport.warehouse_id" ng-value="warehouse.id" ng-click="updateContainers(false)" ng-checked="@{{warehouse.id == transport.warehouse_id}}">
                                            @{{ warehouse.name}}
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-label">
                                        <label style="display:  block;float:  left;">{{ __('express.schedule_pickup') }}</label>
                                        <select ng-model="pickup.is_schedule" ng-change="changeSchedulePickup();" class="form-control" style="display:  block;width: 150px;margin-left:  10px;float: left;">
                                            <option value="1" selected="selected">{{ __('label.no') }}</option>
                                            <option value="2">{{ __('label.yes') }}</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="pickup.is_schedule == 2" class="panel panel-color panel-gray panel-border" style="">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-shopping-cart"></i> {{ __('express.pickup_info') }}
                    </h3>
                    <div class="panel-body" style="padding:15px;">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="10%" class="text-right">{{ __('express.contact_name') }}<i class="text-danger">*</i></td>
                                <td width="20%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.contact_name">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.phone_number') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.phone_number">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.pickup_location') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <select class="form-control" ng-model="pickup.location">
                                        <option ng-repeat="location in pickupLocation" ng-value="location.id">@{{ location.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.pickup_date') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <input id="pickup-date" autocomplete="off" type="text" class="form-control" name="pickup.date_time" ng-model="pickup.date_time" >
                                </td>
                                <td class="text-right">{{ __('express.pickup_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.start_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>
                                    <select class="form-control time-option" ng-model="pickup.start_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                                <td class="text-right">{{ __('express.closing_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.closing_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>:
                                    <select class="form-control time-option" ng-model="pickup.closing_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="quotes.length" class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-truck"></i> {{ __('transport.carrer-local') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-form">
                            <thead>
                            <tr>
                                <th></th>
                                <th width="15%">{{ __('express.carrier') }}</th>
                                <th width="15%">{{ __('express.service') }}</th>
                                <th width="15%">{{ __('express.est_transitday') }}</th>
                                <th width="10%">{{ __('express.base_charge') }}</th>
                                <th width="15%">{{ __('express.orther_surcharge') }}</th>
                                <th width="15%">{{ __('express.fuel_surcharge') }}</th>
                                <th width="10%">{{ __('express.total') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="quote in quotes">
                                <td >
                                    <input type="radio" name="serviceId" ng-value="$index+1" ng-click="setCarrer($index)">
                                </td>
                                <td>
                                    @{{ quote.carrierName}}
                                </td>
                                <td >
                                    @{{ quote.serviceName}}
                                </td>
                                <td >
                                    @{{ quote.transitDays}}
                                </td>
                                <td>
                                    @{{ quote.baseCharge | number : 2 }} @{{  quote.currency }}
                                </td>
                                <td>
                                    <p ng-if=" quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}} @{{  quote.currency }}
                                    </p>
                                </td>
                                <td >
                                    @{{ quote.fuelSurcharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    @{{ quote.totalCharge | number : 2}} @{{  quote.currency }}
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <input type="radio" name="serviceId" ng-click="setNoCarrer()">
                                </td>
                                <td colspan="7">
                                    {{ __('transport.auto-shipper') }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="chooseCarrer" class="transport-quote">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-money"></i> {{ __('transport.quote-cal') }}
                    </h3>
                    <div class="quote-shipping-result">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.date-shipping') }}</strong></td>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.service-fee') }}</strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_sender"></strong></td>
                                <td>{{ __('transport.shipping-local-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_local.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.igreen-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_receiver"></strong></td>
                                <td>{{ __('transport.send-tovn-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_international.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-send-vn') }}:</td>
                                <td class="text-right">{{ __('transport.to') }} <strong ng-bind="transport.date_from_shipping"></strong> - <strong ng-bind="transport.date_to_shipping"></strong></td>
                                <td>{{ __('transport.total-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping.toFixed(2)"></strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-info" ng-if="bntSearch" ng-click="searchExpress()" ng-disabled="submittedSearch">
                        <i class="fa fa-search"></i> {{ __('label.search') }} <i class="fa fa-refresh fa-spin" ng-if="submittedSearch"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script src="{{ asset('js/home/transport-estimate.js?t=' . File::lastModified(public_path('js/home/transport-estimate.js'))) }}"></script>
    <script>
        $(function () {
            $("#shipping-date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});

            var scope = angular.element('#form-create').scope();

            scope.configSurcharge = {!! $configSurcharge->toJson() !!};
            scope.configInsurance = {!! $configInsurance->toJson() !!};
            scope.sender = {
                id: null,
                first_name: 'iGreen',
                middle_name: null,
                last_name: 'Link',
                address_1: null,
                address_2: null,
                telephone: null,
                cellphone: null,
                postal_code: null,
                city_id: null,
                province_id: null,
                shipping_date: null,
                country_id: 14
            };
            <?php if(!empty($customer)){ ?>
                scope.sender = {!! $customer->toJson() !!};
            <?php }?>

            scope.init();

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr    = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
