@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
<link rel="stylesheet"
    href="{{ asset('css/home/tracking.css?t=' . File::lastModified(public_path('css/admin/tracking.css'))) }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('label.tracking-order') }}</a></li>
        </ol>
    </div>
</div>
<div class="container order-tracking">
    <form method="get" action="{{ route('order.tracking') }}">
        <div class="form-group">
            <input type="text" name="order-code" class="form-control" value="{{ app('request')->input('order-code') }}"
                placeholder="{{ __('label.form_code') }}" required="" autocomplete="false">
        </div>
        <button>
            <i class="fa fa-search"></i> {{ __('label.check') }}
        </button>
    </form>
</div>
@php if(app('request')->input('order-code')){ @endphp
<div class="container order-tracking-info">
    <h3>{{ __('label.order-info') }}</h3>
    <div class="table table-popup">
        <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th width="150" class="text-center">{{ __('label.form_code') }}</th>
                    <th width="160" class="text-center">{{ __('label.total_weight') }} (Lbs)</th>
                    <!--<th width="160" class="text-center">{{ __('label.amount') }} (CAD)</th>-->
                    <th width="150" class="text-center">{{ __('label.date') }}</th>
                    <th width="120" class="text-center">{{ __('label.order_status') }}</th>
                    <th width="120" class="text-center">{{ __('label.payment_status') }}</th>
                </tr>
            </thead>
            <tbody>
                @php if(!empty($order) > 0){ @endphp
                <tr>
                    <td class="text-center">{{ $order->code }}</td>
                    <td class="text-center">
                        {{ number_format($order->total_weight, 2, '.', ',') }}
                    </td>
                    <!--<td class="text-center">
                        {{ number_format($order->total_final, 2, '.', ',') }}
                    </td>-->
                    <td class="text-center">{{ $order->created_date }}</td>
                    <td class="text-center">
                        <span class="{{ $order->status_label }}">{{ $order->status_name }}</span>
                    </td>
                    <td class="text-center">
                        <span class="{{ $order->payment_status_label }}">{{ $order->payment_status_name }}</span>
                    </td>
                </tr>
                @php }else{ @endphp
                <tr>
                    <td colspan="3">{{ __('label.no_records') }}</td>
                </tr>
                @php } @endphp
            </tbody>
        </table>
        @if(isset($oder))
        <article class="card">

            <div class="track">
                <div class="step active"> <span class="icon"> <i class="fas fa-warehouse"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_stockin') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=3) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fas fa-pallet"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_shipment') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=4) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-plane"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_stockout') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=6) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-th-large"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_local') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=7) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fab fa-stack-overflow"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_carrier') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=9) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-truck"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_ready') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=10) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-check"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_done') }}
                    </span>
                </div>
            </div>
        </article>
        @endif
    </div>
    <div class="row">
        <ul class="timeline">
            @foreach($trackings as $tracking)
            <li>
                {{ $tracking->created_at }} -
                {{ isset($tracking->user->code ) ? $tracking->user->display_name : 'System'}}
                : {{ $tracking->shipping_status_name }}
                <br> {{ $tracking->note }}
            </li>
            @endforeach
        </ul>

    </div>
</div>
@php } @endphp
@endsection
