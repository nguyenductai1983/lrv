@extends('layouts.home.user', ['menu' => $menu])

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('claim.detail') }}: {{ $claim->code }}
        </div>
        <div class="row claim-container">
            <div class="col-sm-6">
                <div class="claim-content">
                    <h2>{{ $claim->title }}</h2>
                    <span>{{ __('label.order_code') }}: <label class="order-code">{{ $claim->order->code }}</label></span>
                    <span>{{ __('label.create_time') }}: <label>{{ $claim->created_at }}</label></span>
                    <span>{{ __('label.status') }}: <label>{{ $claim->status_name }}</label></span>
                    <span>{{ __('label.content') }}: <label class="claim-body">{{ $claim->title }}</label></span>
                </div>
            </div>
            <div class="col-sm-6 no-padding-left">
                <div class="claim-conversation">
                    <div class="claim-conversation-content" id="claim-conversation-content">
                        @if($claimConversations->count() > 0)
                        @foreach($claimConversations as $claimConversation)
                        @if(!empty($claimConversation->user_id))
                        <div class="user-conversation">
                            <label>{{ $claimConversation->content }}</label>
                            <span class="claim-conversation-user">{{ $claimConversation->user->username }}</span>
                            <span class="claim-conversation-time">{{ $claimConversation->created_at }}</span>
                        </div>
                        @else
                        <div class="customer-conversation">
                            <label>{{ $claimConversation->content }}</label>
                            <span class="claim-conversation-time">{{ $claimConversation->created_at }}</span>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                    
                    @if($claim->status != 3 && $claim->status != 4)
                    <div class="claim-conversation-form">
                        <form id="claim-comment-form">
                            <textarea name="comment"></textarea>
                            <input type="hidden" name="id" value="{{$claim->id}}">
                            {{ csrf_field() }}
                        </form>
                        
                        <button type="button" onclick="claim.addConversation();">{{ __('label.send') }}</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
<script src="/js/home/claim.js"></script>
@endsection