@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        <div class='{{ $complete }}' role="alert">{{ __('label.complete')}}</div>
</br>
        {{ __('transport.orders_title')}}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('transport.order.list') }}" class="btn btn-info btn-single">
                        <i class="fa fa-th-list"></i> {{ __('home.list-quote') }}
                    </a>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('transport.frontend.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.frontend.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.frontend.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="30">#</th>
                            <th width="120">{{ __('label.action') }}</th>
                            <th width="150">{{ __('label.form_code') }}</th>
                            <th width="150" class="text-center">{{ __('label.date') }}</th>
                            <th width="150" class="text-center">{{ __('label.receiver') }}</th>
                            <th width="160" class="text-center">{{ __('label.total_weight') }}</th>
                            <th width="160" class="text-center">{{ __('label.amount') }}</th>
                            <th width="110" class="text-center">{{ __('label.total_discount') }}</th>
                            <th width="120" class="text-center">{{ __('label.order_status') }}</th>
                            <th width="120" class="text-center">{{ __('label.payment_status') }}</th>

                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($transports->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($transports as $transport)
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                             <td>
                                <a href="{{ route('transport.frontend.edit', $transport->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @php if(!empty($transport->eshiper_order_id)){ @endphp
                                <button type="button" class="btn btn-xs btn-info" onclick="order.cusViewHistoryEshiper({{ $transport->id }})">
                                    <i class="fa fa-truck"></i>
                                </button>
                                @php } @endphp
                                <?php if(!empty($transport->eshipper_label)){?>
                                <a href="{{ route('transport.frontend.pdf', $transport->id) }}" class="btn btn-xs btn-primary" download="label_{{ $transport->eshiper_order_id }}.pdf">
                                    <i class="fa fa-file-pdf"></i>
                                </a>
                                <?php }?>
                            </td>
                            <td>
                                {{ $transport->code }}
                            </td>
                            <td class="text-center">{{ $transport->created_at }}</td>
                            <td class="text-center">{{ $transport->receive_first_name }} {{ $transport->receiver_middle_name }}
                            {{ $transport->receive_last_name }}</td>
                             <td class="text-center">{{ number_format($transport->total_weight, 2) }}</td>
                            <td class="text-center">{{ number_format($transport->total_final, 2) }}</td>
                             <td class="text-center">{{ number_format($transport->total_discount, 2) }}
                                       @if($transport->total_final >0)
                                <br>
                           ({{ number_format($transport->total_discount/$transport->total_final, 3)*100 }}%)
                            @endif
                            </td>
                            <td class="text-center">
                                <span class="{{ $transport->status_label }}">{{ $transport->status_name }}</span>
                            </td>
                            <td class="text-center">
                                <span class="{{ $transport->payment_status_label }}">{{ $transport->payment_status_name }}</span>
                            </td>

                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $transports->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/tracking.js"></script>
<script src="/js/transport/order.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
