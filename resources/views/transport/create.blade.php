@extends('layouts.home.user', ['menu' => $menu])

@section('content')
    <div class="panel panel-default transport-content" ng-app="TransportApp" ng-controller="TransportCreateController">
        <div class="transport-content-title panel-heading">
            {{ __('transport.send-to-vn') }}
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-create" name="createForm" ng-submit="createTransport()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row"  ng-if="errors.sender.length || errors.receiver.length || errors.warehouse.length || errors.pickups.length || errors.quotes.length || errors.system.length">
                                    <div class="col-sm-12">
                                        <div  ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div  ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.warehouse.length">
                                            <h4>{{ __('label.warehouse_id') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.warehouse">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.pickups.length">
                                            <h4>{{ __('express.pickup_info') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.pickups">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.quotes.length">
                                            <h4>{{ __('label.eshipper_error') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.quotes">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div ng-if="errors.system.length">
                                <div class="row" >
                                    <div class="col-sm-12">
                                        <h4>{{ __('express.system_error') }}</h4>
                                        <ul>
                                            <li ng-repeat="error in errors.system">@{{ error }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-gray">
                <div class="row">
                            <div class="col-sm-6">
                                <h3>{{ __('label.sender') }}
                                    <i class="far fa-question-circle" title="{{ __('title.sender') }}" data-toggle="tooltip"></i></h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name" disabled>
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('customer.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name" disabled>
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name" disabled>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_2" ng-model="sender.address_2" disabled>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()" disabled>
                                                <option value="">{{ __('label.select_country') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()" disabled>
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-change="getPostCodeSender()" disabled>
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                        <td title="{{ __('title.postalcode') }}" data-toggle="tooltip">
                                       <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code" ng-blur="sender.postal_code = sender.postal_code.split(' ').join('')" disabled>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone" disabled>
                                        </td>
                                        <td class="col-label" colspan="2">

                                            <button type="button" ng-click="showMoreInfoCustomer = !showMoreInfoCustomer">{{ __('customer.change_profile') }}</button>
                                        </td>
                                    </tr>
                                    <tr ng-show="errors.sender.length" class="alert-danger">
                                        <td colspan="6">
                                        <label ng-repeat="error in errors.sender">
                                        @{{ error }} </label>
                                     </td>
                                    </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.id_card') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="sender.id_card"
                                                        ng-model="sender.id_card" ng-change="checkInfoCustomer()"
                                                        ng-disabled="Updatecustomer">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.card_expire') }}
                                                </td>
                                                <td>
                                                    <div class="input-group date-picker" data-change-year="true"
                                                        data-change-month="true"
                                                        data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                        <input type="text" class="form-control"
                                                            name="sender.card_expire" ng-model="sender.card_expire" id="card_expire"
                                                            ng-change="checkInfoCustomer()"
                                                            ng-disabled="Updatecustomer" autocomplete="off">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.birthday') }}
                                                </td>
                                                <td>
                                                    <div class="input-group date-picker" data-change-year="true"
                                                        data-change-month="true"
                                                        data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                                                        <input type="text" class="form-control" name="sender.birthday" id="birthday"
                                                            ng-model="sender.birthday" ng-change="checkInfoCustomer()"
                                                            ng-disabled="Updatecustomer" autocomplete="off">
                                                    </div>
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.career') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="sender.career"
                                                        ng-model="sender.career" ng-change="checkInfoCustomer()"
                                                        ng-disabled="Updatecustomer">
                                                </td>
                                            </tr>
                                    </tbody>
                                </table>

                                 {{-- hiện thị hành ảnh khách hàng --}}
                    <div class="container-fluid">
                        <div ng-show="showMoreInfoCustomer" class="row">
                            <div class="col-sm-3 col-xs-4">
                                <div class="col-label">
                                    {{ __('customer.image_1_file_id') }}
                                @php $field = 'image_1_file_id'; @endphp
                                </div>
                                @include('partials.form-controls.image', ['field' => 'sender.image_1_file_id', 'file' => $customer->{$field} ? $customer->image1 : null ])
                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <div class="col-label">
                                    {{ __('customer.image_2_file_id') }}
                                    @php $field = 'image_2_file_id'; @endphp
                                </div>
                                @include('partials.form-controls.image', ['field' => 'sender.image_2_file_id', 'file' => $customer->{$field} ? $customer->image2 : null,
                                'Update' => 'Updatecustomer'])
                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <div class="col-label">
                                    {{ __('customer.image_3_file_id') }}
                                    @php $field = 'image_3_file_id'; @endphp
                                </div>
                                @include('partials.form-controls.image', ['field' => 'sender.image_3_file_id', 'file' => $customer->{$field} ? $customer->image3 : null,
                                'Update' => 'Updatecustomer'])
                            </div>
                        </div>
                        <div colspan="2" ng-show="showMoreInfoCustomer">
                            <button type="button" class="btn-sm btn-success" ng-click="updatecustomer()">
                            {{ __('label.save_contact') }} <i class="fa fa-save"></i>
                        </button>
                        <div ng-if="contact_save" class='alert alert-success' role="alert">
                            {{ __('label.save_contact_success') }}
                        </div>
                            </div>
                            </div>
                         {{-- hiện thị hành ảnh khách hàng --}}
                            </div>
                            <div class="col-sm-6">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}
                                    <i class="far fa-question-circle" title="{{ __('title.receiver') }}" data-toggle="tooltip"></i></h3>
                                <div style="float: right;" ng-if="addressSearchResult">
                                    <select class="form-control" ng-model="selectedId"  ng-change="pickAddress(selectedId)">
                                        <option value="" selected="selected">{{ __('label.select_address') }}</option>
                                        <option ng-if="addressSearchResult" ng-repeat="item in addressSearchResult" ng-value="item.id">@{{item.last_name}} @{{item.middle_name}} @{{item.first_name}}</option>
                                    </select>
                                </div>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('receiver.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.address') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address" ng-model="receiver.address_1" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address2" ng-model="receiver.address_2" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" >
                                                <option value="">{{ __('label.select_address') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="getWardsReceiver()">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                         {{-- ngày 22-04-2020 --}}
                                         <td class="col-label" ng-hide="ward">
                                            {{ __('receiver.ward_id') }}<i class="text-danger">*</i>

                                        </td>
                                        <td ng-hide="ward">
                                            <select name="receiver.ward_id" class="form-control" ng-model="receiver.ward_id"
                                            ng-disabled="Updatereciver && receiver.ward_id">
                                                <option value="">{{ __('label.select_ward') }}</option>
                                                <option ng-repeat="ward in wardsReceiver" ng-value="ward.id">@{{ ward.name }}</option>
                                            </select>
                                        </td>
                                        <td ng-hide="!ward" class="col-label">{{ __('receiver.postal_code') }}</td>
                                        <td ng-hide="!ward">
                                            <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.postal_code" ng-blur="receiver.postal_code = receiver.postal_code.split(' ').join('')">
                                        </td>
                                        {{-- het doan them --}}
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone" >
                                        </td>

                                        <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" >
                                        </td>
                                    </tr>
                                    <tr>
                                         <td class="col-label">{{ __('express.email') }}</td>
                                         <td colspan="3" title="{{ __('title.email') }}" data-toggle="tooltip">
                                         <input type="text" class="form-control" name="receiver.email" ng-model="receiver.email">
                                         </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div ng-if="errors.receiver.length" class="alert-danger">
                                    <div class="row" ng-repeat="name_to in errors.receiver">
                                    <div class="col-md-12">
                                        <label>@{{ name_to }}</label>
                                    </div>
                                </div>
                               </div>
                            </div>
                        </div>

                </div>
                <div class="transport-content-package-title">
                <div class="row">
                <div class="col-md-6">  <h3 >
                    <i class="fa fa-cubes"></i> {{ __('label.goods') }}
                </h3> </div>
                </div>
                </div>
                <div id="container" class="tabs-border transport-content-package">
                    {{-- them ngay 29-12-2020 --}}
                    <div class="row">
                        <div class="col-md-10 offset-md-2">

                    <div ng-if="discount_info" class="inline-block alert-info" style="padding: 10px;">
                        <h3> <i class="fas fa-info"></i>
                            @php echo __('label.discount_online', ['wight' => '{{ total_weight }}','discount'=>'{{ discount_info}}']); @endphp
                            <span ng-if="!isEditDiscount"> {{ __('label.discount_online_info') }}
                                <a ng-click="showMoreInfo(true)">
                                    {{ __('customer.change_profile') }}</a>
                            </span>
                       </h3>
                    </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs tabs-border">
                        <li ng-repeat="container in transport.containers" class="tab" ng-class="{'active' : $first}" ng-class="{'active' : $first}">
                            <a href="#container-@{{ $index + 1 }}" data-toggle="tab" target="_self">
                               {{ __('label.container') }} @{{ $index + 1 }}
                            </a>
                        </li>
                        <li>
                            <a href="javascript://" ng-click="addContainer()">
                                <span><i class="fa fa-plus"></i></span>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content" style="padding: 15px;">
                        <div ng-repeat="container in transport.containers" class="tab-pane" ng-class="{'active' : $first}"
                        id="container-@{{ $index + 1 }}">
                            <div class="form-group">
                                {{-- hiện thi thông báo xóa --}}
                   <a href="javascript:void(0);" class="btn btn-xs btn-danger" ng-if="transport.containers.length > 1"
                    class="btn btn-danger pull-right"
                    data-toggle="modal"
                    data-target="#modal-delete-container-@{{ $index }}">
                    <i class="fas fa-window-close"></i> {{ __('label.delete') }} {{ __('label.container') }} @{{ $index +1}}
                </a>
                <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                            </div>
                            <div class="modal-body">{!! __('label.confirm_delete_msg') !!}</div>
                            <div class="modal-footer">
                                <button type="button"
                                        class="btn btn-danger disabled-submit"
                                        ng-click="deleteContainer($index)">
                                    <i class="fas fa-trash"></i>
                                    {{ __('label.delete') }}
                                </button>
                                <button type="button"
                                        class="btn btn-white"
                                        data-dismiss="modal">{{ __('label.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                 {{-- hiện thi thông báo xóa --}}
                                <table class="table table-form table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="250" class="text-right col-middle">
                                            <strong title="{{ __('title.measurecontainersize') }}" data-toggle="tooltip"> {{ __('label.Measure_container') }}
                                            </strong>
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.length') }}<i class="text-danger">*</i>:</td>
                                        <td width="80">
                                            <input title="{{ __('title.lengthin') }}" data-toggle="tooltip" type="text" class="form-control text-center" ng-model="container.length" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.width') }}<i class="text-danger">*</i>:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.width" ng-change="updateVolume(container)" title="{{ __('title.widthin') }}" data-toggle="tooltip">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.height') }}<i class="text-danger">*</i>:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.height" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="100" class="text-right col-middle">{{ __('label.volume') }}:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.volume.toFixed(2)" title="{{ __('title.volumeweight') }}" data-toggle="tooltip"></strong>
                                        </td>
                                        <!-- <td class="col-middle">
                                            <button type="button" class="btn btn-info btn-xs" ng-click="setVolume(container)">
                                                <i class="fa fa-check"></i> {{ __('label.select') }}
                                            </button>
                                        </td> -->
                                        <td ng-if="containers.length > 1" class="col-middle">
                                            <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#modal-delete-container-@{{ $index }}">
                                                <i class="fas fa-trash"></i> {{ __('label.delete_container') }} @{{ $index + 1 }}
                                            </button>
                                            <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! __('label.confirm_delete_msg') !!}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger disabled-submit" ng-click="deleteContainer($index)">
                                                                <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                            </button>
                                                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-container" style="max-width: 150%;width: 120%;">
                                    <thead>
                                    <tr>
                                        <th width="30" class="text-center">#</th>
                                        {{-- <th width="100">{{ __('product.code') }}</th> --}}
                                        <th width="100"><span data-toggle="tooltip" title="{{ __('title.productcode') }}">{{ __('product.name') }}<i class="text-danger">*</i></span></th>
                                        <th width="10" class="text-center"><span data-toggle="tooltip" title="{{ __('title.quantity') }}">{{ __('label.quantity') }}</span></th>
                                        <th width="10" class="text-center"><span data-toggle="tooltip" title="{{ __('title.physicalweight') }}">{{ __('label.physical_weight') }}</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.price') }}">{{ __('label.price') }}</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.discount') }}">{{ __('label.discount_short') }}(%)</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.total') }}">{{ __('label.amount') }}</span></th>
                                        {{-- <th width="60" class="text-center">{{ __('label.unit_short') }}</th> --}}
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.declaredvalue') }}">{{ __('label.declared_value_short') }}</span></th>
                                        <th width="70" class="text-right"><span data-toggle="tooltip" title="{{ __('title.surcharge') }}">{{ __('label.surcharge') }}</span></th>
                                        <th width="65" class="text-center"><span data-toggle="tooltip" title="{{ __('title.insurance') }}">{{ __('label.insurance_short') }}</span></th>
                                        <th width="150" class="text-center"><span data-toggle="tooltip" title="{{ __('title.note') }}">{{ __('label.note') }}<i class="text-danger">*</i></span></th>
                                        <th width="100">{{ __('label.action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in container.products">
                                        <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                        <td class="col-middle" style="display:none;">
                                            <input type="text" class="form-control" ng-model="product.code">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" ng-model="product.name" id="products-@{{ $parent.$index + '-' + $index }}"
                                                   ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')" autocomplete="off">
                                            <div id="products-@{{ $parent.$index + '-' + $index }}-dropdown" class="dropdown products-dropdown open" style="position: absolute">
                                                <button class="btn btn-primary dropdown-toggle hidden" type="button" data-toggle="dropdown"></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <table class="table table-bordered table-hover">
                                                            <tbody>
                                                            <tr ng-repeat="item in product.product_list" ng-style="!item.show && {display: 'none'}" ng-click="selectProduct(product, item)">
                                                                <td ng-if="item.separate" ng-bind="item.name" style="width:85% height: 60px; border-bottom: 2px dotted green;"></td>
                                                                <td ng-if="!item.separate" ng-bind="item.name" style="width:85%"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        {{-- <td class="col-middle">
                                            <input type="text" class="form-control input-readonly" ng-model="product.name" readonly="readonly">
                                        </td> --}}
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.quantity" ng-change="changeContainers()">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.weight" ng-change="changeContainers()">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.price.toFixed(2)"></td>
                                        <td class="text-right col-middle" ng-bind="discount.toFixed(2)">
                                        </td>
                                         <td class="text-right col-middle" ng-bind="product.amount.toFixed(2)"></td>
                                        {{-- <td class="text-center col-middle" ng-bind="product.unit"></td> --}}
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-right" ng-model="product.declared_value" ng-change="changeContainers()">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.surcharge.toFixed(2)"></td>
                                        <td class="text-center col-middle">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" class="iswitch iswitch-info" style="margin: 0;" ng-model="product.is_insurance" ng-change="changeContainers()">
                                                    </td>
                                                    <td ng-bind="product.insurance.toFixed(2)"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        {{-- <td class="text-right col-middle" ng-bind="product.total.toFixed(2)"></td> --}}
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.note" ng-blur="showconfirm_Box_Product($parent.$index,$index)">
                                        </td>
                                        <td class="col-middle">
                                            <div class="d-inline">
                                            <a href="javascript:void(0);" class="btn btn-danger" ng-click="remove($parent.$index, $index)">
                                                <i class="fas fa-window-close"></i>
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-info" ng-click="addproduct($parent.$index,$index)">
                                                <i class="fa fa-plus-square"></i>
                                            </a>
                                        </div>

                                          <!-- Button trigger modal -->  <!-- Modal -->
  <div class="modal fade" id="Modal_Box_Product@{{ $parent.$index }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"> {{ __('label.confirm_list_product') }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         {{ __('label.add_product_to_box') }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="addContainer()">{{ __('label.add_packing') }}</button>
          <button type="button"  class="btn btn-secondary" data-dismiss="modal" ng-click="addproduct($parent.$index)">{{ __('label.add_products') }}</button>
          <button type="button" aria-label="Close" class="btn btn-danger" data-dismiss="modal">{{ __('label.status_cancel') }}</button>
        </div>
      </div>
    </div>
  </div>
                                        </td>
                                    </tr>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-info" ng-click="addproduct($index)">
                                                    <i class="fa fa-plus-square"></i> {{ __('label.add_row') }}
                                                </button>
                                            </div>
                                                <div class="col-md-6 text-right">
                                                    <strong>{{ __('label.total') }}:</strong>
                                                </div>
                                            </div>
                                        </td>
                                        <td ></td>
                                        <td class="text-left"><strong ng-bind="container.total_weight.toFixed(2)"></strong>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-left"><strong ng-bind="container.total_amount_discount.toFixed(2)"></strong></td>
                                        <td class="text-left"><strong ng-bind="container.total_declared_value.toFixed(2)">
                                            </strong></td>
                                        <td class="text-left"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong>
                                        </td>
                                        <td class="text-left"><strong ng-bind="container.total_insurance.toFixed(2)"></strong>
                                        </td>

                                    </tr>
                                </tfoot>
                                    </tbody>
                                </table>
                            </div>
                            <div style="padding: 5px"></div>
                                <div class="row justify-content-md-center" style="padding: 5px;">
                                <div class="col-md-2 col-3">
                                    {{ __('label.coupon_code') }}:
                                </div>
                                <div class="col-md-3 col-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" ng-model="container.coupon_code" ng-disabled="isUseCoupon">
                                        <div class="input-group-btn" ng-show="!isUseCoupon">
                                            <button type="button" class="btn btn-info" ng-click="applyCoupon(container)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                        </div>
                                        <div class="input-group-btn" ng-show="isUseCoupon">
                                            <button type="button" class="btn btn-danger" ng-click="removeCoupon(container)">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-3">
                                <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                </div>
                                <div class="col-md-7 col-12">
                                    <div ng-if="errors.Coupon.length" color="red">
                                        <ul ng-repeat="error in errors.Coupon">@{{ error }}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {{-- total container --}}
                            <div class="row" style="padding: 5px">
                            <div class="col-md-3 col-xs-4 text-right">{{ __('label.total_weight') }}:</div>
                                <div class="col-md-1 col-xs-2"><strong ng-bind="container.total_weight.toFixed(2)"></strong></div>
                            <div class="col-md-3 col-xs-4 text-right">{{ __('label.shipping_fee') }}:</div>
                                <div class="col-md-1 col-xs-2"><strong ng-bind="transport.shipping_fee.toFixed(2)"></strong></div>
                            <div class="col-md-3 col-xs-4 col-md-offset-0 col-xs-offset-6 text-right"><strong>{{ __('label.total_shipping_fee') }}:</strong></div>
                                <div class="col-md-1 col-xs-2"><strong ng-bind="container.total_shipping_fee.toFixed(2)"></strong></div>
                        </div>
                        <div class="row" style="padding: 5px">
                            <div class="col-md-3 col-xs-4 text-right">{{ __('label.surcharge') }}:</div>
                            <div class="col-md-1 col-xs-2"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong></div>
                            <div class="col-md-3 col-xs-4 text-right">+ {{ __('label.insurance_short') }}:</div>
                            <div class="col-md-1 col-xs-2"><strong ng-bind="container.total_insurance.toFixed(2)"></strong></div>
                            <div class="col-md-3 col-xs-4 text-right"> <strong>{{ __('label.total_surcharge') }}: </strong></div>
                            <div class="col-md-1 col-xs-2"><strong ng-bind="(container.total_surcharge + container.total_insurance).toFixed(2)"></strong></div>
                        </div>
                        <div class="row" style="padding: 5px; border-bottom: 1px solid gray;">
                            <div class="col-md-3 col-xs-4 text-right">{{ __('label.total_amount') }}:</div>
                            <div class="col-md-1 col-xs-2"><strong ng-bind="(container.total_fee_product).toFixed(2)"></strong></div>
                            <div class="col-md-3 col-xs-4 text-right">- {{ __('label.total_discount') }}:</div>
                            <div class="col-md-1 col-xs-2"><strong ng-bind="container.total_discount.toFixed(2)"></strong></div>
                            <div class="col-md-3 col-xs-4 text-right">
                                <strong>  {{ __('label.total') }}:</strong>
                            </div>
                            <div class="col-md-1 col-xs-2">
                            <strong ng-bind="container.total_amount_discount.toFixed(2)"></strong>
                        </div>
                        </div>
                        <div class="row" style="padding: 5px">
                            <div class="col-md-6">
                                <h3>
                                    {{ __('express.ship_date') }} <input type="text" class="" id="shipping-date"
                                    name="sender.shipping_date" ng-model="sender.shipping_date" autocomplete="off">

                            </h3>
                            </div>
                            <div class="col-md-3 col-xs-3 text-right">
                                <span ng-show="container.total_charge_fee<=container.min_fee">
                                    <strong>{{ __('label.min_fee') }}:</strong>
                                </span>
                                <span ng-show="container.total_charge_fee>container.min_fee">
                                    <strong>{{ __('label.total_fee') }}:</strong>
                                </span>
                            </div>
                            <div class="col-md-2 col-xs-2 text-right">
                            <strong ng-bind="container.total.toFixed(2)"></strong>
                        </div>
                        </div>
                        {{-- end total container --}}
                        </div>
                    </div>
                </div>
    <div>


</div>
        <div class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-map-marker"></i> {{ __('transport.point-receiver') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-form">
                            <tbody>
                                <tr>
                                    <td colspan="1">
                                        <strong>{{ __('label.choose-option') }}:</strong>
                                    </td>
                                </tr>
                                <tr ng-repeat="warehouse in warehouses">
                                    <td class="col-label" width="90%">
                                        <label>
                                            <input type="radio" name="warehouseId" ng-model="transport.warehouse_id" ng-value="warehouse.id" ng-click="changeContainers(false)" ng-checked="@{{warehouse.id == transport.warehouse_id}}">
                                            @{{ warehouse.name}}
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-label">
                                        <label style="display:  block;float:  left;">{{ __('express.schedule_pickup') }}</label>
                                        <select ng-model="pickup.is_schedule" ng-change="changeSchedulePickup();" class="form-control" style="display:  block;width: 150px;margin-left:  10px;float: left;">
                                            <option value="1" selected="selected">{{ __('label.no') }}</option>
                                            <option value="2">{{ __('label.yes') }}</option>
                                        </select>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div ng-if="pickup.is_schedule == 2" class="panel panel-color panel-gray panel-border" style="">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-shopping-cart"></i> {{ __('express.pickup_info') }}
                    </h3>
                    <div class="panel-body" style="padding:15px;">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="10%" class="text-right">{{ __('express.contact_name') }}<i class="text-danger">*</i></td>
                                <td width="20%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.contact_name">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.phone_number') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.phone_number">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.pickup_location') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <select class="form-control" ng-model="pickup.location">
                                        <option ng-repeat="location in pickupLocation" ng-value="location.id">@{{ location.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.pickup_date') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <input id="pickup-date" autocomplete="off" type="text" class="form-control" name="pickup.date_time" ng-model="pickup.date_time" >
                                </td>
                                <td class="text-right">{{ __('express.pickup_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.start_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>
                                    <select class="form-control time-option" ng-model="pickup.start_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                                <td class="text-right">{{ __('express.closing_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.closing_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>:
                                    <select class="form-control time-option" ng-model="pickup.closing_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-truck"></i>  {{ __('transport.carrer-local') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                    <div ng-if="quotes.length && transport.warehouse_id===warehouse_id" class="transport-carrer">
                        <table class="table table-form">
                            <thead>
                            <tr>
                                <th></th>
                                <th width="15%">{{ __('express.carrier') }}</th>
                                <th width="15%">{{ __('express.service') }}</th>
                                <th width="10%">{{ __('express.est_transitday') }}</th>
                                <th width="10%">{{ __('express.base_charge') }}</th>
                                <th width="20%">{{ __('express.orther_surcharge') }}</th>
                                <th width="15%">{{ __('express.fuel_surcharge') }}</th>
                                <th width="10%">{{ __('express.total') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="quote in quotes.slice(0, 5)">
                                <td >
                                    <input type="radio" name="serviceId" ng-value="$index+1" ng-click="setCarrer($index)">
                                </td>
                                <td>
                                    @{{ quote.carrierName}}
                                </td>
                                <td >
                                    @{{ quote.serviceName}}
                                </td>
                                <td >
                                    @{{ quote.transitDays}}
                                </td>
                                <td>
                                    @{{ quote.baseCharge | number : 2 }} @{{  quote.currency }}
                                </td>
                                <td>
                                    <div ng-if=" quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}} @{{  quote.currency }}
                                    </div>
                                </td>
                                <td >
                                    @{{ quote.fuelSurcharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    @{{ quote.totalCharge | number : 2}} @{{  quote.currency }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    {{-- <div ng-if='transport.warehouse_id===warehouse_id'>aaaaaaaaaaaaaaaa</div> --}}
                    <div ng-if='transport.warehouse_id'>
                        <input type="radio" name="serviceId" ng-click="setNoCarrer()"> {{ __('transport.auto-shipper') }}
                    </div>
                    <button type="submit" class="btn btn-info" ng-if='transport.warehouse_id' ng-click="searchExpress()" ng-disabled="submittedSearch">
                        <i class="fa fa-search"></i> {{ __('label.search') }} {{ __('transport.carrer-local') }}<i class="fa fa-refresh fa-spin" ng-if="submittedSearch"></i>
                    </button>
                    </div>
                </div>

                <div ng-if="chooseCarrer" class="transport-quote">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-money"></i> {{ __('transport.quote-cal') }}
                    </h3>
    {{-- total transport --}}
     <div class="row" style="padding: 5px">
        <div class="col-md-3 col-xs-4 text-right">{{ __('label.total_weight') }}:</div>
            <div class="col-md-1 col-xs-2"><strong ng-bind="transport.total_weight.toFixed(2)"></strong></div>
        <div class="col-md-3 col-xs-4 text-right">{{ __('label.shipping_fee') }}:</div>
            <div class="col-md-1 col-xs-2"><strong ng-bind="transport.shipping_fee.toFixed(2)"></strong></div>
        <div class="col-md-3 col-xs-4 col-md-offset-0 col-xs-offset-6 text-right"><strong>{{ __('label.total_shipping_fee') }}:</strong></div>
            <div class="col-md-1 col-xs-2"><strong ng-bind="transport.total_shipping_fee.toFixed(2)"></strong></div>
    </div>
    <div class="row" style="padding: 5px">
        <div class="col-md-3 col-xs-4 text-right">{{ __('label.surcharge') }}:</div>
        <div class="col-md-1 col-xs-2"><strong ng-bind="transport.total_surcharge.toFixed(2)"></strong></div>
        <div class="col-md-3 col-xs-4 text-right">+ {{ __('label.insurance_short') }}:</div>
        <div class="col-md-1 col-xs-2"><strong ng-bind="transport.total_insurance.toFixed(2)"></strong></div>
        <div class="col-md-3 col-xs-4 text-right"> <strong>{{ __('label.total_surcharge') }}: </strong></div>
        <div class="col-md-1 col-xs-2"><strong ng-bind="(transport.total_surcharge + transport.total_insurance).toFixed(2)"></strong></div>
    </div>
    <div class="row" style="padding: 5px; border-bottom: 1px solid gray;">
        <div class="col-md-3 col-xs-4 text-right">{{ __('label.total_amount') }}:</div>
        <div class="col-md-1 col-xs-2"><strong ng-bind="(transport.total_amount).toFixed(2)"></strong></div>
        <div class="col-md-3 col-xs-4 text-right">- {{ __('label.total_discount') }}:</div>
        <div class="col-md-1 col-xs-2"><strong ng-bind="transport.total_discount.toFixed(2)"></strong></div>
        <div class="col-md-3 col-xs-4 text-right">
            <strong>  {{ __('label.total') }}:</strong>
        </div>
        <div class="col-md-1 col-xs-2">
        <strong ng-bind="transport.total_amount_discount.toFixed(2)"></strong>
    </div>
    </div>
    {{-- <div class="row" style="padding: 5px">
        <div class="col-sm-offset-6 col-md-4 col-xs-offset-6 col-xs-4 text-right">
                <strong>{{ __('transport.send-tovn-fee') }}:</strong>
            </div>
        <div class="col-md-2 col-xs-2 text-center">
        <strong ng-bind="transport.total.toFixed(2)"></strong>
    </div>
    </div> --}}
    {{-- end total transport --}}
                    <div class="quote-shipping-result">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.date-shipping') }}</strong></td>
                                <td>{{ __('transport.send-tovn-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_international.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_sender"></strong></td>
                                <td>{{ __('transport.shipping-local-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_local.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.igreen-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_receiver"></strong></td>
                                <td>{{ __('transport.total-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-send-vn') }}:</td>
                                <td class="text-right">{{ __('transport.to') }} <strong ng-bind="transport.date_from_shipping"></strong> - <strong ng-bind="transport.date_to_shipping"></strong></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- đoạn xử lý nút dồng ý -->
                       {!! isset($termagree->translate->detail) ? $termagree->translate->detail : '' !!}
                         <br>
                            <h3><input type="checkbox" name="term_agree" ng-click="setagree(agree)" ng-model="agree">
                        {{ __('transport.ok_Terms_Conditions') }} </h3>
                        <!-- đoạn xử lý nút dồng ý -->
                    </div>
                </div>

                <div class="text-right">
                    <a href="{{ route('transport.frontend.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.cancel') }}
                    </a>
                    <button type="button" class="btn btn-info" ng-if="bntConfirm" ng-click="scanCreateTransport()" ng-disabled="submittedConfirm">
                        <i class="fa fa-check"></i> {{ __('label.confirm') }} <i class="fa fa-refresh fa-spin" ng-if="submittedConfirm"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/home/transport-create.js?t=' . File::lastModified(public_path('js/home/transport-create.js'))) }}"></script>
    <script>
        $(function () {
            $("#shipping-date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
            $("#card_expire").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
            $("#birthday").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
            var scope = angular.element('#form-create').scope();
            scope.configSurcharge = {!! $configSurcharge->toJson() !!};
            scope.configInsurance = {!! $configInsurance->toJson() !!};
            scope.caPriority = {!! json_encode($caPriority) !!};
            scope.vnPriority = {!! json_encode($vnPriority) !!};
            scope.caVnPriorityFee = {!! $caVnPriorityFee !!};
            scope.caVnPriorityDefaultFee = {!! $caVnPriorityDefaultFee !!};
            scope.caVnDefaultPriorityFee = {!! $caVnDefaultPriorityFee !!};
            scope.caVnDefaultFee = {!! $caVnDefaultFee !!};
            scope.senderCountryId = {!! $senderCountryId !!};
            scope.receiverCountryId = {!! $receiverCountryId !!};
            scope.Online_Disounts = {!! $Online_Disounts !!};
            scope.payMethod = '{!! $payMethod !!}';
            scope.discountLevel = {!! json_encode($discountLevel) !!};
            scope.updateProfileLink = '{{ route('profile.index') }}';
            scope.sender = {!! $customer->toJson() !!};
            scope.init();

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr    = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
