@extends('layouts.home.user', ['menu' => $menu])

@section('content')
    <div class="panel panel-default" ng-app="TransportApp" ng-controller="TransportCreateController"
         style="padding: 15px;">
        <div class="panel-heading">
            {{ __('transport.view-detail') }} <strong ng-bind="orderCode"></strong>
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-edit" ng-submit="updateTransport()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.sender.length || errors.receiver.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-gray">
                    <table class="transport-content-shipping table table-form">
                        <tbody>
                        <tr>
                            <td class="transport-content-sender">
                                <h3>{{ __('label.sender') }}</h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name">
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('customer.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name">
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_2" ng-model="sender.address_2">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()">
                                                <option value="">{{ __('label.select_country') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()">
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-change="updateContainers(false)">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code" ng-change="updateContainers(false)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="transport-content-receiver">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('receiver.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.address') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address" ng-model="receiver.address_1" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address_2" ng-model="receiver.address_2" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" >
                                                <option value="">{{ __('label.select_address') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="updateContainers()" >
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.postal_code" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone" >
                                        </td>

                                        <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="transport-content-package-title">
                    <i class="fa fa-cubes"></i> {{ __('label.goods') }}
                </h3>
                <div id="container" class="transport-content-package">
                    <div class="tab-content">
                        <div class="form-group">
                            <table class="table table-form table-bordered">
                                <thead>
                                <tr>
                                    <th width="30" class="text-center">#</th>
                                    <th width="100">{{ __('product.code') }}</th>
                                    <th width="100">{{ __('product.name') }}</th>
                                    <th width="30" class="text-center">{{ __('label.quantity') }}</th>
                                    <th width="30" class="text-center">{{ __('label.weight') }}</th>
                                    <th width="60" class="text-right">{{ __('label.price') }}</th>
                                    <th width="30" class="text-right">{{ __('label.declared_value_short') }}</th>
                                    <th width="60" class="text-right">{{ __('label.surcharge') }}</th>
                                    <th width="60" class="text-center">{{ __('label.insurance_short') }}</th>
                                    <th width="60" class="text-right">{{ __('label.total') }}</th>
                                    <th width="150">{{ __('label.note') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="product in container.products">
                                    <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                    <td class="col-middle">
                                        <input type="text" class="form-control" ng-model="product.code">
                                      <!-- <input type="text" class="form-control" ng-model="product.code"
                                               ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                               ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')">
                                        <div id="products-@{{ $parent.$index + '-' + $index }}-dropdown" class="dropdown products-dropdown">
                                            <button class="btn btn-primary dropdown-toggle hidden" type="button" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <table class="table table-bordered table-hover">
                                                        <tbody>
                                                        <tr ng-repeat="item in product.product_list" ng-style="!item.show && {display: 'none'}" ng-click="selectProduct(product, item)">
                                                            <td ng-bind="item.code"></td>
                                                            <td ng-bind="item.name"></td>
                                                            <td class="text-right" ng-bind="item.sale_price"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </li>
                                            </ul>
                                        </div>  -->
                                    </td>
                                    <td class="col-middle">
                                        <input type="text" class="form-control input-readonly" ng-model="product.name" readonly="readonly">
                                    </td>
                                    <td class="col-middle">
                                        <input type="text" class="form-control text-center" ng-model="product.quantity" ng-change="updateContainers(false)">
                                    </td>
                                    <td class="col-middle">
                                        <input type="text" class="form-control text-center" ng-model="product.weight" ng-change="updateContainers(false)">
                                    </td>
                                    <td class="text-right col-middle" ng-bind="product.price"></td>
                                    <td class="col-middle">
                                        <input type="text" class="form-control text-right" ng-model="product.declared_value" ng-change="updateContainers(false)">
                                    </td>
                                    <td class="text-right col-middle" ng-bind="product.surcharge.toFixed(2)"></td>
                                    <td class="text-center col-middle">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="iswitch iswitch-info" style="margin: 0;" ng-model="product.is_insurance" ng-checked="product.is_insurance" ng-change="updateContainers(false)">
                                                </td>
                                                <td ng-bind="product.insurance.toFixed(2)"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text-right col-middle" ng-bind="product.total"></td>
                                    <td class="col-middle">
                                        <input type="text" class="form-control" ng-model="product.note">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                    <td class="text-right" width="15%"></td>
                                    <td class="text-left" width="19%"></td>
                                    <td class="col-middle text-right" width="15%">{{ __('label.coupon_code') }}:</td>
                                    <td class="text-left" width="18%">
                                        <div class="input-group">
                                            <input type="text" class="form-control" ng-model="container.coupon_code">
                                        </div>
                                    </td>
                                    <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                    <td class="text-left">
                                        <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                    </td>
                                </tr>
                            <tr>
                                <td class="text-right" width="15%">{{ __('label.shipping_fee') }}:</td>
                                <td class="text-left" width="19%"><strong ng-bind="container.shipping_fee.toFixed(2)"></strong></td>
                                <td class="text-right" width="15%">{{ __('label.total_weight') }}:</td>
                                <td class="text-left" width="18%"><strong ng-bind="container.total_weight.toFixed(2)"></strong></td>
                                <td class="text-right" width="15%">{{ __('label.total_shipping_fee') }}:</td>
                                <td class="text-left"><strong ng-bind="container.total_shipping_fee.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('label.total_declared_value') }}:</td>
                                <td class="text-left"><strong ng-bind="container.total_declared_value.toFixed(2)"></strong></td>
                                <td class="text-right">{{ __('label.total_surcharge') }}:</td>
                                <td class="text-left"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong></td>
                                <td class="text-right">{{ __('label.total_insurance') }}:</td>
                                <td class="text-left"><strong ng-bind="container.total_insurance.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4"></td>
                                <td class="text-right">{{ __('label.total_pay') }}:</td>
                                <td class="text-left"><strong ng-bind="container.total.toFixed(2)"></strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-right" style="padding-top: 30px;">
                    <a href="{{ route('transport.order.list') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/home/transport-order-edit.js?t=' . File::lastModified(public_path('js/home/transport-order-edit.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-edit').scope();

            scope.configSurcharge = {!! $configSurcharge->toJson() !!};
            scope.configInsurance = {!! $configInsurance->toJson() !!};
            scope.id = {!! request()->route('id') !!};

            scope.getOrder();

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
