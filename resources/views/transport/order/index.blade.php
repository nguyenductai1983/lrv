@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('transport.orders_title')}}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <a href="{{ route('transport.frontend.index') }}" class="btn btn-info btn-single">
                        <i class="fa fa-th-list"></i> {{ __('home.list-order') }}
                    </a>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('transport.frontend.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.order.list') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.order.list') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                 <table class="table table-hover table-bordered" style="min-width: 1700px;">
                    <thead>
                        <tr>
                            <th width="30">#</th>
                            <th width="120">{{ __('label.action') }}</th>
                            <th width="150">{{ __('label.form_code') }}</th>
                            <th width="80" class="text-center">{{ __('label.date') }}</th>
                            <th width="120">{{ __('label.receiver') }}</th>
                            <th width="120">{{ __('label.product_id') }}</th>
                            <th width="120">{{ __('label.note') }}</th>
                            <th width="60" class="text-center">{{ __('label.total_weight') }}(Lbs)</th>
                            <th width="80" class="text-center">{{ __('label.amount') }}(CAD)</th>
                            <th width="100" class="text-center">{{ __('label.coupon_code') }}</th>
                            <th width="110" class="text-right">{{ __('label.total_discount') }} (CAD)</th>
                            <th width="120" class="text-center">{{ __('label.order_status') }}</th>
                            <th width="120" class="text-center">{{ __('label.payment_status') }}</th>

                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                            <td>
                                <a href="{{ route('transport.order.edit', $order->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a href="{{ route('transport.order.prints', $order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                    <i class="fa fa-print "></i>
                                </a>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.viewHistory({{ $order->id }});">
                                    <i class="fa fa-truck"></i>
                                </button>
                                <?php if($order->payment_status > 1){?>
                                <button type="button" class="btn btn-xs btn-warning" onclick="claim.open({{$order->id}}, '{{ csrf_token() }}');">
                                    <i class="fa fa-bullhorn"></i>
                                </button>
                                <?php }?>
                                 <?php if(!empty($order->eshiper_shipping_status)){?>
                                <a href="{{ route('transport.order.pdf', $order->id) }}" class="btn btn-xs btn-primary" download="label_{{ $order->id }}.pdf">
                                    <i class="fa fa-file-pdf"></i>
                                </a>
                                <?php }?>
                            </td>
                            <td>
                                {{ $order->code }}
                            </td>
                              <td class="text-center">{{ $order->created_at }}</td>
                                 <td>{{ $order->receive_full_name }}</td>
                               <td>{{ $order->products }}</td>
                            <td>{{ $order->description }}</td>

                            <td class="text-center">{{ number_format($order->total_weight, 2) }}</td>
                            <td class="text-center">{{ number_format($order->total_final, 2) }}</td>
                            <td class="text-center">
                                {{ $order->coupon_code }}
                            </td>
                             <td class="text-right">{{ number_format($order->agency_discount, 2) }}
                            @if($order->total_final >0)
                                <br>
                           ({{ number_format($order->agency_discount/$order->total_final, 3)*100 }}%)
                            @endif
                            </td>
                            <td class="text-center"><span class="{{ $order->status_label }}">{{ $order->status_name }}</span></td>
                            <td class="text-center"><span class="{{ $order->payment_status_label }}">{{ $order->payment_status_name }}</span></td>

                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $orders->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/home/claim.js"></script>
<script src="/js/home/tracking.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
