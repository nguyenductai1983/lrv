@extends('layouts.home.index', ['menu' => $menu])
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/user.css') }}">
<style>
.tooltip{
    background-color: greenyellow;
    color: #fff;
  }
  </style>

@endsection
@section('content')
    <div class="panel panel-default transport-content" ng-app="TransportApp" ng-controller="TransportCreateController">
        <div class="panel-body ng-cloak">
                <div class="panel-body panel-gray">
                    <h3 class="transport-content-package-title" >
                    <div class="col-sm-3" title="{{ __('title.agencyinfomation') }}" data-toggle="tooltip">
                        <i class="fa fa-phone-square"></i> {{ __('label.agency_info') }}
                    </div>
                   <div class="col-sm-2" title="{{ __('title.code') }}" data-toggle="tooltip">
                       <input type="checkbox" ng-model="agency.checkbox.code" name="agency_code" value="code"
                       > {{ __('label.code') }} <i class="far fa-question-circle"></i> </div>
                   <div class="col-sm-2" title="{{ __('title.name') }}" data-toggle="tooltip">
                    <input type="checkbox" ng-model="agency.checkbox.name" name="agency_name" value="name">{{ __('label.name') }}
                    <i class="far fa-question-circle"></i>
                </div>
                <div class="col-sm-2" title="{{ __('title.address') }}" data-toggle="tooltip" >
                    <input type="checkbox" ng-model="agency.checkbox.address" name="agency_address" value="address">{{ __('label.address') }}
                    <i class="far fa-question-circle"></i>
                </div>
            </h3>
            <div class="row" ng-if="agency.checkbox.name || agency.checkbox.code || agency.checkbox.address ">
                    <div class="col-sm-3"> </div>
                    <div class="col-sm-2" ng-if="agency.checkbox.code">
                    <input type="text" class="form-control" ng-model="agency.info.code" id="agency_info" >
                    </div>
                    <div class="col-sm-2" ng-if="agency.checkbox.name">
                    <input type="text" class="form-control" ng-model="agency.info.name" id="agency_info" >
                    </div>
                    <div class="col-sm-4" ng-if="agency.checkbox.address">
                    <input type="text" class="form-control" ng-model="agency.info.address" id="agency_info">
                    </div>
                </div>
            </div>

            <form id="form-create" name="createForm" ng-submit="scanCreateTransport()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.sender.length || errors.receiver.length || errors.warehouse.length || errors.pickups.length || errors.quotes.length || errors.system.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.system.length">
                                            <div class="row" >
                                                <div class="col-sm-12">
                                                    <h4>{{ __('express.system_error') }}</h4>
                                                    <ul>
                                                        <li ng-repeat="error in errors.system">@{{ error }}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.warehouse.length">
                                            <h4>{{ __('label.warehouse_id') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.warehouse">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.pickups.length">
                                            <h4>{{ __('express.pickup_info') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.pickups">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.quotes.length">
                                            <h4>{{ __('label.eshipper_error') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.quotes">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-body panel-gray">
                    <h3 class="transport-content-package-title">
                       <i class="fa fa-user" ></i> {{ __('label.customer') }}
                        <i class="far fa-question-circle" title="{{ __('title.customer') }}" data-toggle="tooltip"></i>

                    </h3>
                    <table class="transport-content-shipping table table-form">
                        <tbody>
                        <tr>
                            <td class="transport-content-sender">
                                <h3>{{ __('label.sender') }}
                                    <i class="far fa-question-circle" title="{{ __('title.sender') }}" data-toggle="tooltip"></i></h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label" title="{{ __('title.firstname') }}" data-toggle="tooltip">
                                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('customer.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label" title="{{ __('title.lastname') }}" data-toggle="tooltip">
                                            {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_2" ng-model="sender.address_2" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()" >
                                                <option value="">{{ __('label.select_country') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-change="getPostCodeSender()" >
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">
                                            {{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                        <td title="{{ __('title.postalcode') }}" data-toggle="tooltip">
                                            <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code" ng-blur="sender.postal_code = sender.postal_code.split(' ').join('')" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone" >
                                        </td>
                                        <td class="col-label">
                                            {{ __('express.ship_date') }}
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="shipping-date" name="sender.shipping_date" ng-model="sender.shipping_date">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label" >
                                            {{ __('express.email') }} <i class="text-danger">*</i></td>
                                        <td colspan="3" title="{{ __('title.email') }}" data-toggle="tooltip">
                                        <input type="text" class="form-control" name="sender.email" ng-model="sender.email">
                                        </td>
                                   </tr>
                                    <tr  ng-if="errors.sender.length" class="alert-danger">
                                                <td colspan="6">
                                                <label ng-repeat="name_to in errors.sender">
                                                @{{ name_to }}
                                             </td>
                                            </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="transport-content-receiver">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}
                                    <i class="far fa-question-circle" title="{{ __('title.receiver') }}" data-toggle="tooltip"></i>
                                </h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label" title="{{ __('title.firstname') }}" data-toggle="tooltip">
                                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('receiver.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label" title="{{ __('title.lastname') }}" data-toggle="tooltip">
                                            {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.address') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address" ng-model="receiver.address_1" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address2" ng-model="receiver.address_2" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" >
                                                <option value="">{{ __('label.select_address') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="getWardsReceiver()">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                          {{-- ngày 22-04-2020 --}}
                                          <td class="col-label" ng-hide="ward">
                                            {{ __('receiver.ward_id') }}
                                        </td>
                                        <td ng-hide="ward">
                                            <select name="receiver.ward_id" class="form-control" ng-model="address.ward_id"
                                            ng-disabled="Updatereciver && address.ward_id">
                                                <option value="">{{ __('label.select_ward') }}</option>
                                                <option ng-repeat="ward in wardsReceiver" ng-value="ward.id">@{{ ward.name }}</option>
                                            </select>
                                        </td>
                                        {{-- het doan them --}}
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone" >
                                        </td>

                                        <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" >
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.postal_code') }}</td>
                                        <td title="{{ __('title.postalcode') }}" data-toggle="tooltip">
                                            <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.postal_code" ng-blur="receiver.postal_code = receiver.postal_code.split(' ').join('')">
                                        </td>
                                    </tr>
                                    <tr>
                                         <td class="col-label">{{ __('express.email') }}</td>
                                         <td colspan="3" title="{{ __('title.email') }}" data-toggle="tooltip">
                                         <input type="text" class="form-control" name="receiver.email" ng-model="receiver.email">
                                         </td>
                                    </tr>
                                    <tr  ng-if="errors.receiver.length" class="alert-danger">
                                                <td colspan="6">
                                                <label ng-repeat="name_to in errors.receiver">
                                                @{{ name_to }}
                                             </td>
                                            </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="transport-content-package-title">
                    <i class="fa fa-cubes"></i> {{ __('label.goods') }}
                    <i class="far fa-question-circle" title="{{ __('title.descriptionofgoods') }}" data-toggle="tooltip"></i>
                </h3>

                <div id="container" class="tabs-border transport-content-package">
                    <ul class="nav nav-tabs tabs-border">
                        <li ng-repeat="container in transport.containers" class="tab" ng-class="{'active' : $first}">
                            <a href="#container-@{{ $index + 1 }}" data-toggle="tab" target="_self">
                                <span title="{{ __('title.box') }}" data-toggle="tooltip">{{ __('label.container') }} @{{ $index + 1 }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript://" ng-click="addContainer()">
                                <span><i class="fa fa-plus"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="padding: 15px;">
                        <div ng-repeat="container in transport.containers" class="tab-pane" ng-class="{'active' : $first}" id="container-@{{ $index + 1 }}">
                            <div class="form-group">
                                <table class="table table-form table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="250" class="text-right col-middle">
                                            <strong title="{{ __('title.measurecontainersize') }}" data-toggle="tooltip"> {{ __('label.Measure_container') }}
                                            </strong>
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.length') }}<i class="text-danger">*</i>:</td>
                                        <td width="80" >
                                            <input title="{{ __('title.lengthin') }}" data-toggle="tooltip"
                                            type="text" class="form-control text-center" ng-model="container.length" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.width') }}<i class="text-danger">*</i>:</td>
                                        <td width="80" >
                                            <input title="{{ __('title.widthin') }}" data-toggle="tooltip"
                                            type="text" class="form-control text-center" ng-model="container.width" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.height') }}<i class="text-danger">*</i>:</td>
                                        <td width="80" >
                                            <input title="{{ __('title.heightin') }}" data-toggle="tooltip"
                                            type="text" class="form-control text-center" ng-model="container.height" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="100" class="text-right col-middle">{{ __('label.volume') }}:</td>
                                        <td width="70" class="col-middle" >
                                            <strong title="{{ __('title.volumeweight') }}" data-toggle="tooltip"
                                            ng-bind="container.volume.toFixed(2)"></strong>
                                        </td>
                                        <!-- <td class="col-middle">
                                            <button type="button" class="btn btn-info btn-xs" ng-click="setVolume(container)">
                                                <i class="fa fa-check"></i> {{ __('label.select') }}
                                            </button>
                                        </td> -->
                                        <td ng-if="containers.length > 1" class="col-middle">
                                            <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#modal-delete-container-@{{ $index }}">
                                                <i class="fas fa-trash"></i> {{ __('label.delete_container') }} @{{ $index + 1 }}
                                            </button>
                                            <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! __('label.confirm_delete_msg') !!}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger -submit" ng-click="deleteContainer($index)">
                                                                <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                            </button>
                                                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-container" style="max-width: 110%;width: 100%;">
                                    <thead>
                                    <tr>
                                        <th width="30" class="text-center">#</th>
                                        <th width="100"><span data-toggle="tooltip" title="{{ __('title.productcode') }}">{{ __('product.code') }}</span></th>
                                        <th width="100"><span data-toggle="tooltip" title="{{ __('title.productcode') }}">{{ __('product.name') }}</span></th>
                                        <th width="30" class="text-center"><span data-toggle="tooltip" title="{{ __('title.quantity') }}">{{ __('label.quantity') }}</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.physicalweight') }}">{{ __('label.physical_weight') }}</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.price') }}">{{ __('label.price') }}</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.discount') }}">{{ __('label.discount_short') }}(%)</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.unit') }}">{{ __('label.unit_short') }}</span></th>
                                        <th width="30" class="text-center"><span data-toggle="tooltip" title="{{ __('title.declaredvalue') }}">{{ __('label.declared_value_short') }}</span></th>
                                        <th width="65" class="text-right"><span data-toggle="tooltip" title="{{ __('title.surcharge') }}">{{ __('label.surcharge') }}</span></th>
                                        <th width="65" class="text-center"><span data-toggle="tooltip" title="{{ __('title.insurance') }}">{{ __('label.insurance_short') }}</span></th>
                                        <th width="60" class="text-center"><span data-toggle="tooltip" title="{{ __('title.total') }}">{{ __('label.total') }}</span></th>
                                        <th width="150" class="text-center"><span data-toggle="tooltip" title="{{ __('title.note') }}">{{ __('label.note') }}</span></th>
                                        <th width="70"><span>{{ __('label.action') }}</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in container.products">
                                        <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.code"
                                                   ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')">
                                            <div id="products-@{{ $parent.$index + '-' + $index }}-dropdown" class="dropdown products-dropdown">
                                                <button class="btn btn-primary dropdown-toggle hidden" type="button" data-toggle="dropdown"></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <table class="table table-bordered table-hover">
                                                            <tbody>
                                                            <tr ng-repeat="item in product.product_list" ng-style="!item.show && {display: 'none'}" ng-click="selectProduct(product, item)">
                                                                <td ng-if="item.separate" ng-bind="item.name" style="width:85% height: 60px; border-bottom: 2px dotted green;"></td>
                                                                <td ng-if="!item.separate" ng-bind="item.name" style="width:85%"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control input-readonly" ng-model="product.name" readonly="readonly">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.quantity" ng-change="updateContainers()">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.weight" ng-change="updateContainers()">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.price.toFixed(2)"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-right" ng-model="product.per_discount" ng-if="isEditDiscount" ng-change="updateContainers()">
                                            <input type="text" class="form-control text-right" ng-model="product.per_discount" ng-if="!isEditDiscount" ng-click="showPopupVip()" readonly>
                                        </td>
                                        <td class="text-center col-middle" ng-bind="product.unit"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-right" ng-model="product.declared_value" ng-change="updateContainers()">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.surcharge.toFixed(2)"></td>
                                        <td class="text-center col-middle">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" class="iswitch iswitch-info" style="margin: 0;" ng-model="product.is_insurance" ng-change="updateContainers()">
                                                    </td>
                                                    <td ng-bind="product.insurance.toFixed(2)"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.total.toFixed(2)"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.note">
                                        </td>
                                        <td class="col-middle">
                                            <a href="javascript:void(0);" class="btn btn-xs btn-danger" ng-click="remove($parent.$index, $index)">
                                                <i class="fas fa-window-close"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td colspan="2" class="text-right text-danger" width="34%">
                                        <div ng-if="errors.Coupon.length" color="red">
                                            <ul ng-repeat="error in errors.Coupon">@{{ error }}
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="col-middle text-right btn-info" width="15%">{{ __('label.coupon_code') }}:</td>
                                    <td class="text-left" width="18%">
                                        <div class="input-group">
                                            <input type="text" class="form-control" ng-model="container.coupon_code" ng-="isUseCoupon">
                                            <div class="input-group-btn" ng-show="!isUseCoupon">
                                                <button type="button" class="btn btn-info" ng-click="applyCoupon(container)">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            <div class="input-group-btn" ng-show="isUseCoupon">
                                                <button type="button" class="btn btn-danger" ng-click="removeCoupon(container)">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                    <td class="text-left">
                                        <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" width="15%">{{ __('label.shipping_fee') }}:</td>
                                    <td class="text-left" width="19%"><strong ng-bind="container.shipping_fee.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_weight') }}:</td>
                                    <td class="text-left" width="18%"><strong ng-bind="container.total_weight.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_shipping_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_shipping_fee.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_declared_value') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_declared_value.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_surcharge') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_insurance') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_insurance.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_amount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_amount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_fee.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.min_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.min_fee.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_charge_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_charge_fee.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_discount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_discount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_pay') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total.toFixed(2)"></strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-map-marker"></i> {{ __('transport.point-receiver') }}  <i class="far fa-question-circle"  data-toggle="tooltip" title="{{ __('title.warehouses') }}"></i>
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-form">
                            <tbody>
                                <tr>
                                    <td colspan="1">
                                        <strong>{{ __('label.choose_warehouse') }}:</strong>
                                    </td>
                                </tr>
                                <tr ng-repeat="warehouse in warehouses">
                                    <td class="col-label" width="90%">
                                        <label>
                                            <input type="radio" name="warehouseId" ng-model="transport.warehouse_id" ng-value="warehouse.id" ng-click="updateContainers(false)" ng-checked="@{{warehouse.id == transport.warehouse_id}}">
                                            @{{ warehouse.name}}
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-label">
                                        <label style="display:  block;float:  left;">{{ __('express.schedule_pickup') }}</label>
                                        <select ng-model="pickup.is_schedule" ng-change="changeSchedulePickup();" class="form-control" style="display:  block;width: 150px;margin-left:  10px;float: left;">
                                            <option value="1" selected="selected">{{ __('label.no') }}</option>
                                            <option value="2">{{ __('label.yes') }}</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div ng-show="pickup.is_schedule == 2" class="panel panel-gray">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-shopping-cart"></i> {{ __('express.pickup_info') }}
                        <i class="far fa-question-circle" title="{{ __('title.pickupinfomation') }}" data-toggle="tooltip"></i>
                    </h3>

                    <div class="panel-body" style="padding:15px;">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="10%" class="text-right">
                                    {{ __('express.contact_name') }}
                                    <i class="far fa-question-circle" title="{{ __('title.contactname') }}" data-toggle="tooltip"></i><i class="text-danger">*</i></td>
                                <td width="20%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.contact_name">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.phone_number') }}
                                    <i class="far fa-question-circle" title="{{ __('title.telephonepickup') }}" data-toggle="tooltip"></i>
                                    <i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.phone_number">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.pickup_location') }}
                                    <i class="far fa-question-circle" title="{{ __('title.locationpickup') }}" data-toggle="tooltip"></i>
                                    <i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <select class="form-control" ng-model="pickup.location">
                                        <i class="far fa-question-circle" title="{{ __('title.pickupinfomation') }}" data-toggle="tooltip"></i>
                                        <option ng-repeat="location in pickupLocation" ng-value="location.id">@{{ location.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.pickup_date') }}
                                    <i class="far fa-question-circle" title="{{ __('title.pickupdate') }}" data-toggle="tooltip"></i>
                                    <i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <input id="pickup-date" autocomplete="off" type="text" class="form-control" name="pickup.date_time" ng-model="pickup.date_time" >
                                </td>
                                <td class="text-right">{{ __('express.pickup_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.start_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>
                                    <select class="form-control time-option" ng-model="pickup.start_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                                <td class="text-right">{{ __('express.closing_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.closing_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>:
                                    <select class="form-control time-option" ng-model="pickup.closing_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <a href="#" id="quote"></a>
                <div ng-if="quotes.length" class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-truck"></i> {{ __('transport.carrer-local') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-form">
                            <thead>
                            <tr>
                                <th></th>
                                <th width="15%">{{ __('express.carrier') }}</th>
                                <th width="15%">{{ __('express.service') }}</th>
                                <th width="15%">{{ __('express.est_transitday') }}</th>
                                <th width="10%">{{ __('express.base_charge') }}</th>
                                <th width="15%">{{ __('express.orther_surcharge') }}</th>
                                <th width="15%">{{ __('express.fuel_surcharge') }}</th>
                                <th width="10%">{{ __('express.total') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="quote in quotes.slice(0, 5)">
                                <td >
                                    <input type="radio" name="serviceId" ng-value="$index+1" ng-click="setCarrer($index)">
                                </td>
                                <td>
                                    @{{ quote.carrierName}}
                                </td>
                                <td >
                                    @{{ quote.serviceName}}
                                </td>
                                <td >
                                    @{{ quote.transitDays}}
                                </td>
                                <td>
                                    @{{ quote.baseCharge | number : 2 }} @{{  quote.currency }}
                                </td>
                                <td>
                                    <div ng-if=" quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}} @{{  quote.currency }}
                                    </div>
                                </td>
                                <td >
                                    @{{ quote.fuelSurcharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    @{{ quote.totalCharge | number : 2}} @{{  quote.currency }}
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <input type="radio" name="serviceId" ng-click="setNoCarrer()">
                                </td>
                                <td colspan="7">
                                    {{ __('transport.auto-shipper') }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div ng-if="carrier_alert" class="alert alert-warning">
                            <h3>
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                {{ __('transport.carrier_alert') }}
                            </h3>
                        </div>
                    </div>
                </div>

                <div ng-if="chooseCarrer" class="transport-quote">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-money"></i> {{ __('transport.quote-cal') }}
                    </h3>
                    <div class="quote-shipping-result">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.date-shipping') }}</strong></td>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.service-fee') }}</strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_sender"></strong></td>
                                <td>{{ __('transport.shipping-local-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_local.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.igreen-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_receiver"></strong></td>
                                <td>{{ __('transport.send-tovn-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_international.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-send-vn') }}:</td>
                                <td class="text-right">{{ __('transport.to') }} <strong ng-bind="transport.date_from_shipping"></strong> - <strong ng-bind="transport.date_to_shipping"></strong></td>
                                <td>{{ __('transport.total-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping.toFixed(2)"></strong></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- đoạn xử lý nút dồng ý -->
                       {!! isset($termagree->translate->detail) ? $termagree->translate->detail : '' !!}
                         <br>
                            <h3><input type="checkbox" name="term_agree" id="term_agree" ng-click="setagree(agree)" ng-model="agree">
                        {{ __('transport.ok_Terms_Conditions') }} </h3>
                        <!-- đoạn xử lý nút dồng ý -->
                    </div>
                </div>

                <div class="text-right">
                    <a href="{{ route('transport.frontend.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.cancel') }}
                    </a>
                    {{-- ng-if="bntSearch"  --}}
                    <button type="button" class="btn btn-info" ng-click="searchExpress()">
                        <i class="fa fa-search"></i> {{ __('label.search') }} <i class="fa fa-refresh fa-spin" ng-if="submittedSearch"></i>
                    </button>
                    <button type="button" class="btn btn-info" ng-if="bntConfirm" ng-click="scanCreateTransport()" >
                        <i class="fa fa-check"></i> {{ __('label.confirm') }} <i class="fa fa-refresh fa-spin" ng-if="submittedConfirm"></i>
                    </button>
                </div>
                <div ng-if="complete" class="alert alert-success ng-scope" role="alert">
                    {{ __('label.complete')}}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/home/transportonline-create.js?t=' . File::lastModified(public_path('js/home/transportonline-create.js'))) }}"></script>
    <script>
        $(function () {
            $("#shipping-date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});

            var scope = angular.element('#form-create').scope();

            scope.configSurcharge = {!! $configSurcharge->toJson() !!};
            scope.configInsurance = {!! $configInsurance->toJson() !!};
            scope.caPriority = {!! json_encode($caPriority) !!};
            scope.vnPriority = {!! json_encode($vnPriority) !!};
            scope.caVnPriorityFee = {!! $caVnPriorityFee !!};
            scope.caVnPriorityDefaultFee = {!! $caVnPriorityDefaultFee !!};
            scope.caVnDefaultPriorityFee = {!! $caVnDefaultPriorityFee !!};
            scope.caVnDefaultFee = {!! $caVnDefaultFee !!};
            scope.senderCountryId = {!! $senderCountryId !!};
            scope.receiverCountryId = {!! $receiverCountryId !!};
            scope.payMethod = '{!! $payMethod !!}';
            scope.discountLevel = {!! json_encode($discountLevel) !!};
            scope.init();

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr    = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
