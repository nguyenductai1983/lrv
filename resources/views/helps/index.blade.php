@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/news.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('helps.manage') }}</a></li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach($pages as $key1 => $page1)
        @if ($page1->parent_id == 0)
        <div class="col-md-6 col-sm-6 border-home">
            <div class="help-item">
                <h3 class="help-item-title">
                    <i class="fa fa-folder" aria-hidden="true"></i>
                    <span>{{ $page1->translate->name }}</span>
                </h3>
                <ul class="help-item-content">
                    @foreach($pages as $key2 => $page2)
                    @if ($page2->parent_id == $page1->id)
                    <li>
                        <a href="{{ $page2->link_to ? url($page2->link_to) : route('helps.detail', $page2->slug) }}">
                            <i class="fa fa-file" aria-hidden="true"></i>
                            {{ $page2->translate->name }}
                        </a>
                    </li>
                    @php $pages->forget($key2); @endphp
                    @endif
                    @endforeach
                </ul>
            </div>
        </div>
        @php $pages->forget($key1); @endphp
        @endif
        @endforeach
    </div>
</div>
@endsection