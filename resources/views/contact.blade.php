@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('label.contact') }}</a></li>
        </ol>
    </div>
</div>
<div class="container contact-info">
    <div class="row">
        <div class="col-md-6">
            <div class="container contact-info end">
                <h2>{{ __('label.contact_us') }} </h2>
                <form class="form-contact" id="form-contact">
                    <input type="text" name="full_name" class="form-control"
                        placeholder="{{ __('label.full_name') }} (*)">
                    <input type="text" name="address" class="form-control" placeholder="{{ __('label.address') }} (*)">
                    <input type="text" name="phone_number" class="form-control"
                        placeholder="{{ __('label.phone_number') }} (*)">
                    <input type="text" name="email" class="form-control" placeholder="{{ __('label.email') }} (*)">
                    <textarea type="text" name="content" class="form-control"
                        placeholder="{{ __('label.content') }} (*)"></textarea>
                    {{ csrf_field() }}
                    <button type="button" onclick="user.contact()">{{ __('label.send') }}</button>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-map" id="map">
                <a href="https://goo.gl/maps/NqaBjrT1a5Pb3RsTA" target="new">
                <img src="/images/home/3080 Yonge St.jpg" height="100%">
                </a>
            </div>

            <div class="contact-content">

                <span><b>{{ __('home.address') }}: </b>{{ __('home.address_info') }}</span>
                <span><b>{{ __('home.hotline') }}: </b>{{ __('home.hotline_info') }}</span>
                <span><b>{{ __('home.tollfree') }}: </b>{{ __('home.tollfree_info') }}</span>
                <span><b>Email:</b> info@igreencorp.com | world@igreencorp.com</span>
            </div>
        </div>

    </div>


</div>


@endsection
