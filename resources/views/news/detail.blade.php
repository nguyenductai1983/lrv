@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/news.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ $page->translate->name }}</a></li>
        </ol>
    </div>
</div>
<div class="container">
    <div id="main">{!! $page->translate->detail !!}</div>
</div>
@endsection