@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/news.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('news.manage') }}</a></li>
        </ol>
    </div>
</div>
<div class="container product">
    <div class="row">
        @foreach($news as $new)
        <div class="col-md-3 col-sm-6 border-home product-item">
            <div class="fixed-image-wide-150 owl-item">
                <a href="{{ $new->link_to ? url($new->link_to) : route('news.detail', $new->slug) }}" title="{{ $new->translate->name }}">
                    <img class="lazy" alt="{{ $new->translate->name }}" src="{{ $new->translate->thumnail ? $new->translate->thumnail : asset('images/home/no_image.jpg')}}">
                </a>
            </div>
            <div class="product-name">
                <a title="{{ $new->translate->name }}" class="product-short" href="{{ $new->link_to ? url($new->link_to) : route('news.detail', $new->slug) }}">{{ $new->translate->name }}</a>
            </div>
            <div class="clearfix"></div>
        </div>
        @endforeach
    </div>
    <div class="paginate-single">
        {{ $news->appends(request()->query())->links() }}
    </div>
</div>
@endsection