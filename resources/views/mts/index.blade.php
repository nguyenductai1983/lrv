@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
         <div class='{{ $complete }}' role="alert">{{ __('label.complete')}}</div>     
</br>
        {{ __('home.list-mts') }}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('mts.frontend.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <form action="{{ route('mts.frontend.index') }}" method="get" role="form" class="form-inline">
                        <div class="form-group">
                            <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}"
                                   class="form-control" type="text" style="width: 160px;"
                                   placeholder="{{ __('label.from_date') }}">
                        </div>
                        <div class="form-group">
                            <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}"
                                   class="form-control" type="text" style="width: 160px;"
                                   placeholder="{{ __('label.to_date') }}">
                        </div>
                        <div class="form-group">
                            <input name="code" value="{{ request()->query('code')}}" class="form-control"
                                   type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-white btn-single">
                                <i class="fa fa-search"></i> {{ __('label.find') }}
                            </button>
                            <a href="{{ route('mts.frontend.index') }}" class="btn btn-white btn-single">
                                {{ __('label.clear') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <th width="50" class="text-center">#</th>
                    <th width="150" class="text-center">{{ __('label.form_code') }}</th>
                    <th width="160" class="text-center">{{ __('label.amount') }} (CAD)</th>
                    <th width="150" class="text-center">{{ __('label.fee') }} (CAD)</th>
                    <th width="150" class="text-center">{{ __('label.total') }} (CAD)</th>
                    <th width="150" class="text-center">{{ __('label.date') }}</th>
                    <th width="120" class="text-center">{{ __('label.mts_status') }}</th>
                    <th width="120">{{ __('label.action') }}</th>
                </thead>
                <tbody id="shipment-package-items">
                    @if($mtss->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($mtss as $mts)
                    <tr>
                        <td class="text-center">
                            {{ $no }}
                        </td>
                        <td class="text-center">{{ $mts->code }}</td>
                        <td class="text-center">
                            {{ number_format($mts->total_goods, 2, '.', ',') }}
                        </td>
                        <td class="text-center">{{ number_format($mts->transfer_fee, 2)}}</td>
                        <td class="text-center">{{ number_format($mts->total_final, 2)}}</td>
                        <td class="text-center">{{ $mts->created_date }}</td>
                        <td class="text-center">
                            <span class="{{ $mts->status_label }}">{{ $mts->status_name }}</span>
                        </td>
                        <td>
                            <a href="{{ route('mts.frontend.edit', $mts->id) }}" class="btn btn-xs btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="8">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $mtss->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection