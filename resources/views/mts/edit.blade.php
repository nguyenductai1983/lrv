@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default" ng-app="MtsApp" ng-controller="MtsEditController" style="padding: 15px;">
    <div class="transport-content-title panel-heading">
        {{ __('mts.edit') }}
    </div>
    <div class="panel-body ng-cloak">
        <form id="form-edit" ng-submit="updateMts()" novalidate>
            <div class="panel panel-gray">
                <table class="transport-content-shipping table table-form">
                    <tbody>
                        <tr>
                            <td class="transport-content-sender">
                                <h3>{{ __('label.sender') }}</h3>
                                <table class="table table-form">
                                    <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="20%">
                                                <input type="text" class="form-control" name="sender.first_name"
                                                    ng-model="sender.first_name" ng-disabled="sender.id">
                                            </td>
                                            <td width="15%" class="col-label">
                                                {{ __('customer.middle_name') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.middle_name"
                                                    ng-model="sender.middle_name" ng-disabled="sender.id">
                                            </td>
                                            <td width="10%" class="col-label">
                                                {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.last_name"
                                                    ng-model="sender.last_name" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="sender.address_1"
                                                    ng-model="sender.address_1" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.email') }}
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="sender.email"
                                                    ng-model="sender.email" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="sender.country_id" class="form-control"
                                                    ng-model="sender.country_id" ng-change="getProvincesCustomer()"
                                                    ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_country') }}</option>
                                                    <option ng-repeat="country in countries" ng-value="country.id">
                                                        @{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="sender.province_id" class="form-control"
                                                    ng-model="sender.province_id" ng-change="getCitiesCustomer()"
                                                    ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesCustomer"
                                                        ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="sender.city_id" class="form-control"
                                                    ng-model="sender.city_id" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesCustomer" ng-value="city.id">
                                                        @{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label">{{ __('customer.postal_code') }}<i
                                                    class="text-danger">*</i></td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.postal_code"
                                                    ng-model="sender.postal_code" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.telephone"
                                                    ng-model="sender.telephone" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="transport-content-receiver">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>
                                <div style="float: right;position: absolute;right: 55px;top: 85px;"
                                    ng-if="addressSearchResult">
                                    <select class="form-control" ng-model="selectedId"
                                        ng-change="pickAddress(selectedId)">
                                        <option value="">{{ __('label.select_address') }}</option>
                                        <option ng-repeat="item in addressSearchResult" ng-value="item.id">
                                            @{{item.address_1}}</option>
                                    </select>
                                </div>
                                <table class="table table-form">
                                    <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="20%">
                                                <input type="text" class="form-control" name="receiver.first_name"
                                                    ng-model="receiver.first_name" ng-disabled="receiver.id">
                                            </td>
                                            <td width="15%" class="col-label">
                                                {{ __('receiver.middle_name') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.middle_name"
                                                    ng-model="receiver.middle_name" ng-disabled="receiver.id">
                                            </td>
                                            <td width="10%" class="col-label">
                                                {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.last_name"
                                                    ng-model="receiver.last_name" ng-disabled="receiver.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.address') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="receiver.address"
                                                    ng-model="receiver.address_1" ng-disabled="receiver.id">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="receiver.country_id" class="form-control"
                                                    ng-model="receiver.country_id" ng-change="getCitiesReceiver()"
                                                    ng-disabled="receiver.id">
                                                    <option value="">{{ __('label.select_address') }}</option>
                                                    <option ng-repeat="country in countries" ng-value="country.id">
                                                        @{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.province_id" class="form-control"
                                                    ng-model="receiver.province_id" ng-change="getProvincesReceiver()"
                                                    ng-disabled="receiver.id">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesReceiver"
                                                        ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.city_id" class="form-control"
                                                    ng-model="receiver.city_id" ng-change="updateContainers()"
                                                    ng-disabled="receiver.id">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesReceiver" ng-value="city.id">
                                                        @{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.post_code"
                                                    ng-model="receiver.postal_code" ng-disabled="receiver.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.telephone"
                                                    ng-model="receiver.telephone" ng-disabled="receiver.id">
                                            </td>

                                            <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.cellphone"
                                                    ng-model="receiver.cellphone" ng-disabled="receiver.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">{{__('receiver.relationship')}}</td>
                                            <td colspan="2">
                                                <input ng-model="receiver.relationship" class="form-control" type="text"
                                                    id="relationship_other" ng-disabled="Updatereciver" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel panel-color panel-gray panel-border">
                <div class="transport-content-title panel-heading">
                    {{ __('mts.details') }}
                </div>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td class="col-middle text-right">
                                {{ __('mts.reason') }}:
                            </td>
                            <td colspan="5">
                                <input type="text" class="form-control" ng-model="container.reason">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-middle text-right">{{ __('label.date_pay') }}:</td>
                            <td class="col-middle text-left">
                                <input type="text" class="form-control" ng-model="container.pay_date"
                                    disabled="disabled" value="">
                            </td>
                            <td class="col-middle text-right">{{ __('label.currency_pay') }}:</td>
                            <td class="col-middle text-left">
                                <select class="form-control" ng-model="container.currency">
                                    <option ng-repeat="currency in currencies" ng-value="currency.id">
                                        @{{ currency.name }}</option>
                                </select>
                            </td>
                            <td class="col-middle text-right">{{ __('mts.transaction_type') }}:</td>
                            <td class="col-middle text-left">
                                <select class="form-control" ng-model="container.transaction_type">
                                    <option ng-repeat="transaction_type in transaction_types"
                                        ng-value="transaction_type.key">@{{ transaction_type.name }}</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                            <td class="col-middle text-left">
                                <select class="form-control" ng-model="container.payment_by">
                                    <option ng-repeat="payment_by in payment_bys" ng-value="payment_by.key">
                                        @{{ payment_by.name }}</option>
                                </select>
                            </td>
                            <td class="col-middle text-right">{{ __('mts.send_amount') }}:</td>
                            <td class="col-middle text-left">
                                <input type="text" class="form-control" ng-model="container.amount"
                                    ng-change="updateFee(container)">
                            </td>
                            <td class="col-middle text-right">{{ __('label.total_discount') }}:</td>
                            <td class="col-middle text-left">
                                <select class="form-control" ng-model="container.discount_type"
                                    ng-change="updateFee(container)"
                                    style="display: block;width: 65px;float: left;margin-right: 10px;">
                                    <option ng-repeat="discount_type in discount_types" ng-value="discount_type.key">
                                        @{{ discount_type.name }}</option>
                                </select>
                                <input type="text" class="form-control" ng-model="container.discount_number"
                                    style="width:200px;" ng-change="updateFee(container)">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-middle text-right">{{ __('label.status') }}</td>
                            <td class="col-middle text-left">
                                <select class="form-control" ng-model="container.status">
                                    <option ng-repeat="status in statuses" ng-value="status.key">@{{ status.name }}
                                    </option>
                                </select>
                            </td>
                            <td class="col-middle text-right">{{ __('mts.transfer_fee') }}:</td>
                            <td class="col-middle text-left">
                                <input type="text" class="form-control" ng-model="container.transfer_fee">
                            </td>
                            <td class="col-middle text-right">{{ __('label.total_pay') }}:</td>
                            <td class="col-middle text-left">
                                <input type="text" class="form-control" ng-model="container.total">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-right">
                <a href="{{ route('mts.frontend.index') }}" class="btn btn-white">
                    <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('js/home/mts-edit.js?t=' . File::lastModified(public_path('js/home/mts-edit.js'))) }}"></script>
<script>
    $(function () {
            var scope = angular.element('#form-edit').scope();
            scope.id = {!! request()->route('id') !!};
            scope.vnPriority = {!! json_encode(config('app.vn_priority')) !!};
            // scope.mts_per_fee = {!! config('mts.per_fee') !!};
            // scope.mts_max_per_fee = {!! config('mts.max_per_fee') !!};
            // scope.mts1500 = {!! config('mts.mts1500') !!};
            // scope.fix_fee1500 = {!! config('mts.fix_fee1500') !!};
            // scope.mts3000 = {!! config('mts.mts3000') !!};
            // scope.fix_fee3000 = {!! config('mts.fix_fee3000') !!};
            // scope.fix_fee = {!! config('mts.fix_fee') !!};
            scope.MTSSurcharge = {!! $MTSSurcharge !!};
            scope.getMts();

            scope.transaction_types = [];
            scope.statuses = [];
            scope.payment_bys = [];
            scope.discount_types = [];

            <?php foreach(config('mts.transaction_type') as $transaction_type): ?>
            scope.transaction_types.push({
                key : parseInt({!! $transaction_type !!}),
                name: '{!! __('label.transaction_type_' . $transaction_type) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.status_full') as $status): ?>
            scope.statuses.push({
                key : parseInt({!! $status !!}),
                name: '{!! __('label.mts_status_' . $status) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.payment_by') as $payment_by): ?>
            scope.payment_bys.push({
                key : parseInt({!! $payment_by !!}),
                name: '{!! __('label.payment_by' . $payment_by) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.discount_type') as $discount_type): ?>
            scope.discount_types.push({
                key : parseInt({!! $discount_type !!}),
                name: '{!! __('label.discount_type_' . $discount_type) !!}'
            });
            <?php endforeach; ?>

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });
        });
</script>
@endsection
