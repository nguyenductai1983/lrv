@extends('layouts.home.index')
@section('head-css')
  <link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
@endsection
@section('content')
  <div class="container">
adsffasdfasdfasddfsad
    <!-- cập nhật carousel slide -->
  @section('myCarousel')


    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        @php $y = 0;@endphp
        @foreach ($banners as $banner)
          @if ($y == 0)
            <div class="item active">
            @else
              <div class="item">
          @endif
          <a href="{{ $banner->url }}">
            <img src="{{ $banner->path }}" alt="Chania">
          </a>
          <div class="carousel-caption">
            <h3>{{ $banner->caption }}</h3>
            <p>{{ $banner->detail }}</p>
          </div>
      </div>
      @php $y++@endphp
      @endforeach
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>
@endsection
<!-- hết cập nhật carousel slide -->
<div class="row mgb-40">
  <div class="col-sm-12 col-sm-offset-0 text-center">
    <br><br>
    <h2> {{ __('home.introduce') }} </h2>
    <h2><b> {{ __('home.iGreenLink') }} </b>
      {{ __('home.introduce_1') }} </h2>
    <h2> {{ __('home.introduce_2') }} </h2>
  </div>
</div>
<div class="row mgb-40">
  <!-- <div class="col-sm-12 text-center">
        <img class="center-block img-responsive" src="{{ asset(__('home.image_home')) }}">
             <div class="col-sm-12 text-center icon-services">
                <img src="{{ asset('images/home/icon/cart.png') }}">
                <img class="next" src="{{ asset('images/home/icon/next.png') }}">
                <img src="{{ asset('images/home/icon/logo-service.png') }}">
                <img class="next" src="{{ asset('images/home/icon/next.png') }}">
                <img src="{{ asset('images/home/icon/box.png') }}">
            </div>
        </div> -->
  <!-- danh sach san pham -->
  <div class="container product">
    <div class="row">
      @foreach ($groups as $group)
        <div class="col-md-4 col-sm-6 border-home product-item">
          <div class="fixed-image-wide-150 owl-item">
            <a href="{{ $group->frontend_url }}" title="{{ $group->name }}">
              <img class="lazy" alt="{{ $group->name }}" src="{{ $group->thumbnail }}">
            </a>
          </div>
          <div class="product-name">
            <a title="{{ $group->name }}" class="product-short"
              href="{{ $group->frontend_url }}">{{ $group->name }}</a>
          </div>
          <div class="clearfix"></div>
        </div>
      @endforeach
    </div>
  </div>
  <!-- het don san pham -->


</div>
<div class="row mgb-40">
  <div class="col-sm-8 col-sm-offset-2 text-center">
    <p class="text-lg text-green">{{ __('home.lets_go_igreen') }}</p>
    <p>{{ __('home.lets_go_info_1') }}</p>
    <p>{{ __('home.lets_go_info_2') }}</p>
    <p>{{ __('home.lets_go_info_3') }}</p>
    <p><b> {{ __('home.iGreenLink') }} </b> {{ __('home.lets_go_info_4') }}</p>
    <p>{{ __('home.lets_go_info_5') }}</p>
    <p>
      <a href="#" class="button btn-green btn-circle text-md">{{ __('home.more_information') }}</a>
    </p>
  </div>
</div>
</div>

<!--     <div class="row mgb-40">
        <div class="col-sm-6" style="background: url('{{ asset('images/home/bg-1.jpg') }}') no-repeat;background-size: cover;">
            <div class="row">
                <div class="col-sm-6" style="padding: 0;">
                    <div style="padding: 50px;background-color: rgba(255, 255, 255, 0.84);min-height: 226px;">
                        <h2 class="text-green">{{ __('home.create_order_easy') }}</h2>
                        <p><a href="#" class="button btn-green btn-circle">{{ __('home.detail') }}</a></p>
                    </div>
                </div>
                <div class="col-sm-6" style="min-height: 226px;"></div>
            </div>
        </div>
        <div class="col-sm-6" style="background: url('{{ asset('images/home/bg-2.jpg') }}') no-repeat;background-size: cover;">
            <div class="row">
                <div class="col-sm-6" style="padding: 0;">
                    <div style="padding: 50px;background-color: rgba(255, 255, 255, 0.84);min-height: 226px;">
                        <h2 class="text-green">{{ __('home.create_order_easy') }}</h2>
                        <p><a href="#" class="button btn-green btn-circle">{{ __('home.detail') }}</a></p>
                    </div>
                </div>
                <div class="col-sm-6" style="min-height: 226px;"></div>
            </div>
        </div>
    </div> -->

<div class="container">
  <!--  <div class="row mgb-40">
            <div class="col-sm-12 text-center">
                <p class="text-lg text-green">{{ __('home.size_not_define_you') }}</p>
                <p>{{ __('home.create_order_reliable') }}</p>
            </div>
        </div> -->
  <div class="row mgb-40">
    <div class="col-sm-10 col-sm-offset-1 text-center">
      <h2 class="text-center text-green">
        {{ __('home.online_text') }} <br>
        {{ __('home.online_text1') }}

      </h2>
    </div>
  </div>
  <div class="row mgb-40">
    <div class="row mgb-40">
      <div class="col-sm-2 col-sm-offset-1 text-center">
        <a href="https://www.purolator.com"><img class="img-responsive center-block"
            src="{{ asset('images/home/purolator-logo.svg') }}"></a>
      </div>
      <div class="col-sm-2 text-center">
        <a href="http://www.dhl.com">
          <img class="img-responsive center-block" src="{{ asset('images/home/dhl-logo.svg') }}">
        </a>
      </div>
      <div class="col-sm-2 text-center">
        <a href="https://www.ups.com"> <img class="img-responsive center-block"
            src="{{ asset('images/home/UPS_logo.svg') }}"></a>

      </div>
      <div class="col-sm-2 text-center">
        <a href="https://www.fedex.com"><img class="img-responsive center-block"
            src="{{ asset('images/home/fedex-logo.svg') }}"></a>
      </div>
      <div class="col-sm-2 text-center">
        <a href="https://www.aramex.com">
          <img class="img-responsive center-block" src="{{ asset('images/home/aramex-logo.svg') }}"
            style="background-color:red;">

        </a>

      </div>
    </div>
  </div>

  <div class="features section global-map-area parallax" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row image-box style7">
        <div class="col-sm-8 col-sm-offset-2">
          <div style="padding: 10px;background: rgba(255, 255, 255, 0.72);">
            <div class="row">
              <div class="col-sm-10 col-sm-offset-1">
                <p class="text-green text-lg text-center">{{ __('home.we_want_advice') }}</p>
                <p>{{ __('home.advice_info_1') }}</p>
                <p>{{ __('home.advice_info_2') }}</p>
                <p>{{ __('home.advice_info_3') }}</p>
                <form id="form-contact">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <input name="full_name" type="text" class="form-control"
                          placeholder="{{ __('home.full_name') }}">
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <input name="phone_number" type="text" class="form-control"
                          placeholder="{{ __('home.phone') }}">
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <input name="email" type="text" class="form-control"
                          placeholder="{{ __('home.email_address') }}">
                      </div>
                    </div>
                  </div>
                  <div class="form-group text-center">
                    {{ csrf_field() }}
                    <button type="button" onclick="user.contact()"
                      class="button btn-green btn-circle">{{ __('home.call_me') }}</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
