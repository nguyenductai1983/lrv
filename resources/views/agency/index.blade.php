@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/agency.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('agency.manage') }}</a></li>
        </ol>
    </div>
</div>
<div class="container product">
    <div class="row">
        @foreach($agencies as $agency)
        <div class="col-md-12 col-sm-12">
            <div class="agency-content">
                <h3>
                    <span class="icon">
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </span>
                    {{ $agency->name }}({{ $agency->code }})
                </h3>
                <div class="agency-content-item">
                    <span>{{ $agency->address }}</span>
                    <span>Tel: {{ $agency->phone }}</span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="paginate-single">
        {{ $agencies->appends(request()->query())->links() }}
    </div>
</div>
@endsection