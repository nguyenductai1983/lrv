@if(session()->has('status'))
  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">×</span>
      <span class="sr-only">Close</span>
    </button>
    <span>{{ session()->pull('status') }}</span>
  </div>
@elseif(session()->has('error'))
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">×</span>
      <span class="sr-only">Close</span>
    </button>
    <span>{{ session()->pull('error') }}</span>
  </div>
@endif