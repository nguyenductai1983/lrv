{{-- @if(!old($field . '_path', $file ? $file->path : '')) style="display: none;"@endif> --}}
{{-- data-ng-click="image_click('{{ $field }}')" --}}
<div class="img-control">
 <div class="img-preview">
 <div @if(!old($field . '_path', $file ? $file->path : ''))@endif>
     <h4 class="remove"><i class="fa fa-times-circle"></i>
    </h4>
    {{-- {{ old($field . '_path', $file ? $file->path : '') }} --}}
<img name="{{ $field }}" id="{{ $field }}"
     src="{{ old($field . '_path', $file ? $file->path : '') }}" class="img-thumbnail"
     onclick="image_click('{{ $field }}')">
</div>
    <input type="file" accept="image/*,application/pdf" name="file" class="form-control" ng-disabled="<?php  if(isset($Update)){ echo $Update; } ?>"
    onchange="loadFile(event,'{{ $field }}')">

  @if($errors->has($field))
    <span>{{ $errors->first($field) }}</span>
  @endif
  <input type="hidden" class="name" name="{{ $field }}"
  value="{{ old($field, $file ? $file->id : '') }}">
  <input type="hidden" class="path" name="{{ $field }}_path"
       value="{{ old($field . '_path', $file ? $file->path : '') }}">

    </div>
</div>
<script>
    var loadFile = function(event,input) {
      var output = document.getElementById(input);
      output.src = URL.createObjectURL(event.target.files[0]);
      output.parentElement.style.removeProperty("display");

    };
    var image_click = function (image_object){
       var  output = document.getElementById(image_object);
         parentv3= output.parentElement.parentElement.parentElement.parentElement;
  if (parentv3.className === "col-sm-3 col-xs-4 col-6"){
    parentv3.className = "col-sm-6 col-xs-6 col-12";
  }
  else
  {
    parentv3.className = "col-sm-3 col-xs-4 col-6";
  }
  };
  </script>
