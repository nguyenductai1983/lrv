
{{-- bat dau doan dang ki --}}
<div class="modal reg-user-modal modal-1" id="registerModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-box">
            <div class="box-head">
                <div class="box-title">{{ __('home.btn-sign-up') }}</div>
                <button class="exit-btn" data-dismiss="modal"><i class="fas fa-window-close"></i></button>
            </div>
            <div class="box-main">
                <form id="register-form" class="reg-user-form form-1" method="POST" action="{{ route('customer.register') }}">
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-envelope-o"></i>{{ __('home.email') }} (*):</label>
                        <input type="text" name="email" class="form-control-1" placeholder="" required value="{{ old('email') }}" autofocus>
                        <span class="invalid-feedback" role="alert" id="emailError">
                            <strong></strong>
                        </span>
                    </div>
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-phone"></i> {{ __('home.phone-number') }} (*):</label>
                        <input type="text" name="telephone" class="form-control-1" placeholder="" required value="{{ old('last_name') }}">
                    </div>
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-user"></i> {{ __('customer.first_name') }} (*):</label>
                        <input type="text" name="first_name" class="form-control-1" placeholder="" value="{{ old('first_name') }}">
                        <span class="invalid-feedback" role="alert" id="first_nameError">
                            <strong></strong>
                        </span>

                    </div>
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-user"></i> {{ __('customer.last_name') }} (*):</label>
                        <input type="text" name="last_name" class="form-control-1" placeholder="" required value="{{ old('last_name') }}">
                        <span class="invalid-feedback" role="alert" id="first_nameError">
                            <strong></strong>
                        </span>
                    </div>
                    <!-- .form-group-1 -->
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-key"></i> {{ __('home.password') }} (*):</label>
                        <input type="password" name="password" class="form-control-1" placeholder="" required autocomplete="new-password">
                        <span class="invalid-feedback" role="alert" id="passwordError">
                            <strong></strong>
                        </span>
                    </div>
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-key"></i> {{ __('home.confirm-password') }} (*):</label>
                        <input type="password" name="password_confirmation" class="form-control-1" placeholder="" required autocomplete="new-password">
                    </div>
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-user"></i> {{ __('home.ref_code') }}:</label>
                        <input type="text" name="ref_code" class="form-control-1">
                    </div>
                    <div class="form-group-1 btn-wrap">
                    <button type="submit" class="btn btn-danger">
                            <i class="fa"></i>  {{ __('home.sign-up') }}
                    </div>
                    <div class="login-with-wrap">
                        {{ __('home.not-signup-signin-face-google') }}:
                        <a href="{{ route('social.login',['provider' => 'facebook']) }}"><img src="{{asset('images/home/icon-login-fb.png')}}"></a>
                        <a href="{{ route('social.login',['provider' => 'google']) }}" ><img src="{{asset('images/home/icon-login-gp.png')}}"></a>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
{{-- het doan dang ki --}}
@section('scripts')
@parent

<script>
$(function () {
    $('#registerForm').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serializeArray();
        $(".invalid-feedback").children("strong").text("");
        $("#registerForm input").removeClass("is-invalid");
        $.ajax({
            method: "POST",
            headers: {
                Accept: "application/json"
            },
            url: "{{ route('register') }}",
            data: formData,
            success: () => window.location.assign("{{ route('home') }}"),
            error: (response) => {
                if(response.status === 422) {
                    let errors = response.responseJSON.errors;
                    Object.keys(errors).forEach(function (key) {
                        $("#" + key + "Input").addClass("is-invalid");
                        $("#" + key + "Error").children("strong").text(errors[key][0]);
                    });
                } else {
                    window.location.reload();
                }
            }
        })
    });
})
</script>
@endsection
