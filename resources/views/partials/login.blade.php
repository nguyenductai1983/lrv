 {{-- bắt đầu đoạn đăng nhập --}}
 <div class="modal login-modal modal-1" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-box">
            <div class="box-head">
                <div class="box-title">{{ __('home.sign-in') }}</div>
                <button class="exit-btn" data-dismiss="modal"><i class="fas fa-window-close"></i></button>
            </div>
            <div class="box-main">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-envelope-o"></i>{{ __('home.email') }}:</label>
                        <input type="text" name="email" class="form-control-1" placeholder="" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group-1">
                        <label class="lbl-1"><i class="fa fa-key"></i> {{ __('home.password') }}:</label>
                        <input type="password" name="password" class="form-control-1" placeholder="" required="">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                    <div class="form-group-1 btn-wrap">
                        <div class="pull-left">
                            <button type="submit" class="btn btn-danger">
                                <i class="fa"></i> {{ __('home.sign-in') }}
                            </button>
                        </div>
                        <div class="pull-right">
                            <a class="line-height-40px" href="{{ route('customer.register') }}">
                                <i class="fa"></i> {{ __('home.btn-sign-up') }}
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="login-with-wrap">
                        {{ __('home.sign-in-face-google') }} :
                        <a href="{{ URL::to('auth/facebook') }}">
                            <img src="{{asset('images/home/icon-login-fb.png')}}">
                        </a>
                        <a href="{{ URL::to('auth/google') }}">
                            <img src="{{asset('images/home/icon-login-gp.png')}}">
                        </a>
                    </div>

                    <div class="text-1">
                        <b>{{ __('home.what-forgot') }} <a href="{{ route('customer.forgot') }}">{{ __('home.get-in') }}</a></b><br>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
{{-- hết đoạn đăng nhập --}}
@section('scripts')
@parent

@if($errors->has('email') || $errors->has('password'))
    <script>
    $(function() {
        $('#loginModal').modal({
            show: true
        });
    });
    </script>
@endif
document.onkeydown=function(){
    if(window.event.keyCode=='13'){
        submitForm();
    }
}
@endsection
