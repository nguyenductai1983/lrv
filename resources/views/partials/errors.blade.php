@if(isset($errors) && $errors->any())
  <div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    @if($errors->count() > 1)
      <ul style="padding-left: 15px;">
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    @else
      <span>{{ $errors->first() }}</span>
    @endif
  </div>
@endif