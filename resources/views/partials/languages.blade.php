        @foreach(config('app.locales') as $key => $lang)
        @if ($key != app()->getLocale())
        <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">
            <img src="\images\home\icon\flags\{{ $key }}.png" width="32">
            {{ $lang }}
        </a>
        @endif
        @endforeach
