@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('home.product_category') }}</a></li>
        </ol>
    </div>
</div>
<div class="container product">
    <div class="row">
        @foreach($groups as $group)        
        <div class="col-md-4 col-sm-6 border-home product-item">
            <div class="fixed-image-wide-150 owl-item">
                <a href="{{ $group->frontend_url }}" title="{{ $group->name }}">
                    <img class="lazy" alt="{{ $group->name }}" src="{{ $group->thumbnail }}">
                </a>
            </div>
            <div class="product-name">
                <a title="{{ $group->name }}" class="product-short" href="{{ $group->frontend_url }}">{{ $group->name }}</a>
            </div>
            <div class="clearfix"></div>
        </div>
        @endforeach
    </div>
</div>
@endsection