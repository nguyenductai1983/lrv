@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="{{ route('product.index') }}">{{ __('home.product_category') }}</a></li>
        </ol>
    </div>
</div>
<div class="container product">
    <div class="row">
        @if(!empty($products))
        @foreach($products as $product)
            <div class="col-md-3 col-sm-6 border-home product-item">
                <div class="product-item-content">
                    <div class="product-item-content-info">
                        <a href="{{ $product->frontend_url }}" class="product-image" title="{{ $product->name }}">
                            <img class="lazy" alt="{{ $product->name }}" src="{{ $product->thumbnail }}">
                            @if($product->price > 0 && $product->price > $product->sale_price)
                            <span class="product-sale">-{{ $product->per_discount }}%</span>
                            @endif
                        </a>
                        <a href="{{ $product->frontend_url }}" class="product-title">{{ $product->name }}</a>
                        <div class="product-price">
                            @if($product->price > 0 && $product->price > $product->sale_price)
                            <span class="origin-price">{{ $product->getOriginPriceFormatAttribute() }} CAD</span>
                            @endif
                            <span class="sale-price">{{ $product->getSalePriceFormatAttribute() }} CAD</span>
                        </div>
                    </div>
                    <button class="shopping-cart" onclick="cart.addItem({{ $product->id }})">
                        <i class="fa fa-shopping-cart"></i> {{ __('home.cart') }}
                    </button>
                    <form id="cart-additem-form-{{ $product->id }}">
                        <input type="hidden" name="id" value="{{ $product->id }}"/>
                        <input type="hidden" name="quantity" value="1"/>
                        {{ csrf_field() }}
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        @endforeach
        @else
        <span>{{ __('home.no-product') }}</span>
        @endif
    </div>
</div>
@endsection

@section('footer-js')
<script type="text/javascript" src="{{ asset('js/home/cart.js') }}"></script>
@endsection
