@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="{{ route('product.index') }}">{{ __('home.product_category') }}</a></li>
            <li><a href="javascript:void(0);">{{ $product->name }}</a></li>
        </ol>
    </div>
</div>
<div class="container product">
    <div class="row content">
        <div class="col-md-3 col-sm-4 left">
            <div class="gold title news">
                <h3><span>{{ __('home.product_category') }}</span></h3>
                <div class="dmsanpham">
                    <ul>
                        @foreach($groups as $group)
                        <li>
                            <a title="{{ $group->name }}" href="{{ $group->frontend_url }}">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                {{ $group->name }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="gold title news">
                <h3><span>{{ __('home.product_order') }}</span></h3>
                <ul class="news">
                    @foreach($other_products as $other_product)
                    <li class="orther-item">
                        <div class="item-img col-md-4 col-sm-12">
                            <a href="{{ $other_product->frontend_url }}" class="aimg"
                                title="{{ $other_product->name }}">
                                <img src="{{ $other_product->thumbnail }}" alt="{{ $other_product->name }}">
                            </a>
                        </div>
                        <div class="item-content col-md-8 col-sm-12">
                            <a title="{{ $other_product->name }}"
                                href="{{ $other_product->frontend_url }}">{{ $other_product->name }}</a>
                            <div class="left-price">
                                <span class="sale-price">{{ $other_product->getSalePriceFormatAttribute() }} CAD</span>
                                @if($other_product->price > 0 && $other_product->price > $other_product->sale_price)
                                <span class="origin-price">{{ $other_product->getOriginPriceFormatAttribute() }}
                                    CAD</span>
                                @endif
                            </div>
                            @if($other_product->price > 0 && $other_product->price > $other_product->sale_price)
                            <div class="per-discount">
                                <span>-{{ $other_product->per_discount }}%</span>
                            </div>
                            @endif

                        </div>
                        <div class="clearfix"></div>
                    </li>
                    @endforeach
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-9 col-sm-8 maincontent">
            <div class="product">
                <div class="product-detail">
                    <div class="row img-box">
                        <div class="col-md-5 col-sm-6 p_img">
                            <div id="gallerypopup" class="product-img">
                                <img src="{{ $product->thumbnail }}" alt="{{ $product->name }}" class="img-responsive">
                                @if($product->price > 0 && $product->price > $product->sale_price)
                                <span class="p_detail_sale">-{{ $product->per_discount }}%</span>
                                @endif

                            </div>
                        </div>
                        <div class="product-description col-md-7 col-sm-6 ">
                            <h2 class="h2des">{{ $product->name }}</h2>
                            <p> {{ $product->description }}</p>
                            <div class="detail-price">
                                <span class="sale-price">{{ $product->getSalePriceFormatAttribute() }} CAD</span>
                                @if($product->price > 0 && $product->price > $product->sale_price)
                                <span class="origin-price">{{ $product->getOriginPriceFormatAttribute() }} CAD</span>
                                @endif
                            </div>
                            <form id="cart-additem-form-{{ $product->id }}">
                                <div class="cart">
                                    <div class="pd-choose-item">
                                        <div class="lbl">{{ __('home.select_quantity') }}:</div>
                                        <div class="val">
                                            <input type="number" name="quantity" class="quantity-input text-center"
                                                value="1" min="1" max="10">
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" value="{{ $product->id }}" />
                                    {{ csrf_field() }}
                                    <button type="button" class="bnt-cart" onclick="cart.addItem({{ $product->id }})">
                                        <i class="fa fa-shopping-cart"></i> {{ __('home.cart') }}
                                    </button>
                                </div>
                            </form>
                            <div>
                                <h3>{{ __('home.share') }}:
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ $product->frontend_url }}"
                                        class="icon btn-lg"><i class="fab fa-facebook-square"
                                            style="color: #3b5998"></i></a>
                                    <a href="https://plus.google.com/share?url={{ $product->frontend_url }}"
                                        class="icon btn-lg"><i class="fab fa-google-plus-square"
                                            style="color: #db4a39"></i></a>
                                    <a href="http://www.twitter.com/share?url={{ $product->frontend_url }}"
                                        class="icon btn-lg"><i class="fab fa-twitter" style="color: #1DA1F2"></i></a>
                                </h3>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="product-info">
                        <h3 class="product-info-title">
                            <span>{{ __('home.more_info_product') }}</span>
                        </h3>
                        <div class="product-content">
                            {!! $product->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-js')
<script type="text/javascript" src="{{ asset('js/home/cart.js') }}"></script>
@endsection
