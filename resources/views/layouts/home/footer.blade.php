@if(auth()->user())
<div class="mobile-menu-toggle visible-xs">
    <div class="user-info-menu-left">
                <h3>{{ __('home.my-point') }}:
                    <strong>{{ number_format(auth()->user()->point, 0) }}</strong>
                </h3>
                @if(auth()->user()->point >= config('app.min_point_convert'))
                    <a href="javascript:void(0);" onclick="customer.convertPoint('{{ route('profile.coupon') }}')" class="convert-point">{{ __('home.convert-point') }}</a>
                @endif
    </div>
           <div class="user-info-notification">
                 <h3>
            {{ __('user.thong_bao') }}
            <br>
            {{ __('user.thong_bao2') }}
            </h3>
            </div>
  </div>
 @endif
<footer id="footer">
    <div class="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                     <h2 class="text-green">{{ __('home.hotline_support') }}</h2>
                    <p><a href="tel: {{ __('home.tollfree_info') }}"> {{ __('home.tollfree_info') }} </a></p>
                     <h2 class="text-green">{{ __('home.hotline') }}</h2>
                    <p><a href="tel: {{ __('home.hotline_info') }}" > {{ __('home.hotline_info') }} </a></p>
                    <h2 class="text-green">{{ __('home.head_office') }}</h2>
                    <p>{{ __('home.headquarters-val') }}</p>
                </div>
                <div class="col-sm-6 col-md-3">
                     <h2 class="text-green">{{ __('home.send_request') }}</h2>
                    <p> <a href="mailto:{{ __('home.email_add') }}"> {{ __('home.email_add') }}</a></p>
                    <h2 class="text-green">{{ __('home.sales') }}</h2>
                    <p><a href="mailto:{{ __('home.sales_mail') }}">{{ __('home.sales_mail') }}</a></p>
                    <h2 class="text-green">{{ __('home.foreign') }}</h2>
                    <p><a href="mailto:{{ __('home.foreign_mail') }}">{{ __('home.foreign_mail') }}</a></p>
                </div>
                @php
                    $pages = $pages->where('type', config('page.type_footer'));
                    $count = $pages->where('parent_id', 0)->count();
                    $i = 0;
                @endphp
                @foreach($pages as $key1 => $page1)
                    @if($page1->parent_id == 0 && $page1->type===2)
                        @php $i++; @endphp
                        <div class="col-sm-6 col-md-3">
                            <h2 class="text-green">{{ $page1->translate->name }}</h2>
                            <ul class="discover triangle hover">
                                @foreach($pages as $key2 => $page2)
                                    @if($page2->parent_id == $page1->id && $page2->type===2)
                                        <li>
                                            <a href="{{ $page2->link_to ? url($page2->link_to) : route('pages.show', $page2->slug) }}">
                                                {{ $page2->translate->name }}
                                            </a>
                                        </li>
                                        @php $pages->forget($key2); @endphp
                                    @endif
                                @endforeach
                            </ul>
                            @if($i == $count)
                                <h2 class="text-green">{{ __('home.link_with_us')  }}</h2>
                                <ul class="social-icons clearfix">
                                    <li class="facebook">
                                        <a title="facebook" href="https://www.facebook.com/iGreenLink" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a>
                                    </li>
                                    <li class="twitter">
                                        <a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a>
                                    </li>
                                    <li class="youtube">
                                        <a title="youtube" href="#" data-toggle="tooltip"><i class="soap-icon-youtube"></i></a>
                                    </li>
                                    <li class="linkedin">
                                        <a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a>
                                    </li>
                                </ul>
                            @endif
                        </div>
                        @php $pages->forget($key1); @endphp
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="bottom gray-area">
        <div class="container">
            <div class="logo pull-left">
                <a href="{{ route('home.index') }}">
                    <img src="{{ asset('images/home/logo.png') }}">
                </a>
            </div>
            <div class="pull-right">
                <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
            </div>
            <div class="copyright pull-right">
                <p>Copyright &copy; 2017 iGreenLink. Powered by Nextweb</p>
            </div>
        </div>
    </div>
</footer>

