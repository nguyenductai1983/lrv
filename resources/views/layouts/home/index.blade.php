<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>
  <title>{{ __('label.seo-title') }}</title>
  <meta charset="utf-8">
  <meta name="keywords" content="{{ __('label.seo-keywords') }}" />
  <meta name="description" content="{{ __('label.seo-description') }}">
  <meta name="author" content="SoapTheme">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/fontawesome.min.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/all.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Arimo:400,600,700,400italic">
  <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
  {{-- cho xoa --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('components/revolution_slider/css/settings.css') }}"
    media="screen" />
  {{-- <link rel="stylesheet" type="text/css" href="{{ asset('components/revolution_slider/css/style.css') }}"
        media="screen" /> --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('components/jquery.bxslider/jquery.bxslider.css') }}"
    media="screen" />
  <link rel="stylesheet" type="text/css" href="{{ asset('components/flexslider/flexslider.css') }}" media="screen" />
  {{-- cho xoa --}}
  <link id="main-style" rel="stylesheet"
    href="{{ asset('css/home/style.css?t=' . File::lastModified(public_path('css/home/style.css'))) }}">
  <link rel="stylesheet" href="{{ asset('css/home/updates.css') }}">
  <link rel="stylesheet"
    href="{{ asset('css/home/custom.css?t=' . File::lastModified(public_path('css/home/custom.css'))) }}">
  <link rel="stylesheet" href="{{ asset('css/home/responsive.css') }}">
  <link rel="stylesheet" href="{{ asset('css/loading.css') }}">
  {{-- CSS dung cho online --}}

  @yield('head-css')

  <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/home/ie.css') }}"/>
        <![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
        <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
        <![endif]-->
</head>

<body>
  <div id="page-wrapper">
    <header id="header" class="navbar-static-top">
      <div class="topnav hidden-xs">
        <div class="container">
          <h3>
            <ul class="quick-menu pull-left">
              <li>
                <a href="tel:{{ __('home.tollfree_info') }}">
                  <i class="fa fa-phone"></i> {{ __('home.tollfree_info') }}
                </a>

              </li>
              <li>
                <a href="tel:{{ __('home.hotline_info') }}">
                  <i class="fa fa-mobile"></i> {{ __('home.hotline_info') }}
                </a>
              </li>
              <li>
                <a href="/pages/lien-he-38" style="text-transform: none;">
                  <i class="fa fa-envelope-o"></i> {{ __('home.email_add') }}
                </a>
              </li>
            </ul>
            @php
              $pagefooter = $pages->where('type', config('page.type_footer'));
            @endphp
            <ul class="quick-menu pull-right">
              @if (!empty(auth()->user()))
                <li class="dropdown user-profile">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('images/admin/user-4.png') }}" alt="user-image"
                      class="img-circle img-inline userpic-32" width="28">
                    <span>{{ auth()->user()->email }} <i class="fa fa-sort-desc"></i></span>
                  </a>
                  <div class="dropdown-content">
                    <ul class="user-profile-menu list-unstyled">
                      <li>
                        <a href="{{ route('transport.frontend.index') }}">
                          <i class="fa fa-plane"></i> {{ __('home.header-manager-transport') }}
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('express.list') }}">
                          <i class="fa fa-globe"></i> {{ __('home.header-manager-express') }}
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('yhl.frontend.index') }}">
                          <i class="fa fa-truck"></i> {{ __('home.header-manager-yhl') }}
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('mts.frontend.index') }}">
                          <i class="fa fa-money-check-alt"></i>
                          {{ __('home.header-manager-mts') }}
                        </a>
                      </li>
                      <li>
                        <a href="{{ route('profile.index') }}">
                          <i class="fa fa-user"></i> {{ __('home.header-my-account') }}
                        </a>
                      </li>
                      <li class="last">
                        <a href="{{ route('customer.logout') }}">
                          <i class="fa fa-power-off"></i> {{ __('home.sign-out') }}
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              @else
                <li>

                  <a href="{{ route('customer.register') }}">
                    <span class="bnt-signup">{{ __('home.sign-up') }}</span>
                  </a>
                </li>
                <li>
                  <a class="line-height-40px" href="{{ route('customer.login') }}">
                    <i class="fa"></i> {{ __('home.sign-in') }}
                  </a>
                </li>
              @endif
              <li>
                <a href="{{ route('cart.step1') }}">
                  <i class="fa fa-shopping-cart"></i> {{ __('home.cart') }}(<span
                    id="cart-count">{{ $cart_qty }}</span>)
                </a>
              </li>
              <li class="dropdown select-language">
                @php
                  $locales = config('app.locales');
                  $locale = app()->getLocale();

                @endphp
                @foreach ($locales as $key => $lang)
                  @if ($key != $locale)
                    {{-- <li> --}}
                    <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">
                      <img src="\images\home\icon\flags\{{ $key }}.png" width="32">
                      {{-- <i class="fa fa-globe"></i> --}}
                      {{ $lang }}
                    </a>
                    {{-- </li> --}}
                  @endif
                @endforeach


                {{-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                    {{ $locales[$locale] }} <i class="fa fa-sort-desc"></i>
                                </a>
                                <ul class="dropdown-menu select-language-menu list-unstyled">
                                    @foreach ($locales as $key => $lang)
                                    @if ($key = $locale)
                                    <li>
                                        <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">
                                            <i class="fa fa-globe"></i> {{ $lang }}
                                        </a>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul> --}}
              </li>
              <li>
                <a href="{{ route('admin.index') }}">
                  <span class="">{{ __('home.partner') }} <i class="fas fa-handshake fa-lg text-success"></i></span>
                </a>
              </li>
            </ul>
          </h3>
        </div>
      </div>

      <div class="main-header">
        <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a>

        <div class="container">
          <nav id="main-menu" role="navigation">
            @include('layouts.home.menu-header')
            <a href="{{ route('home.index') }}">
              <img src="{{ asset('images/home/logo.png') }}">
            </a>
          </nav>
        </div>

        @include('layouts.home.menu-header-mobile')
      </div>
    </header>

    @yield('myCarousel')

    @yield('breadcrumbs')

    <section id="content">
      @yield('content')
    </section>
  </div>
  {{-- @include('partials.login') --}}
  @include('partials.register')
  @include('layouts.home.footer')

  <div class="modal fade request-pass-modal modal-1" id="lay-lai-mat-khau" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-box">
        <div class="box-head">
          <div class="box-title">{{ __('home.forgot-password') }}</div>
          <button class="exit-btn" data-dismiss="modal"><i class="fas fa-window-close"></i></button>
        </div>
        <div class="box-main">
          <form id="request-pass-form">
            <div class="request-pass-form form-1">
              <div class="form-group-1">
                <label class="lbl-1"><i class="fa fa-envelope-o"></i>{{ __('home.email') }}:</label>
                <input type="text" name="email" class="form-control-1" placeholder="">
              </div>
              <div class="form-group-1 btn-wrap">
                <button class="btn btn-danger" type="button" onclick="user.forgot();">
                  <i class="fa"></i>{{ __('home.complete') }}
                </button>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="loading" style="display:none">
    <div class="loading-inner"></div>
  </div>

  <!-- Javascript -->
  <script type="text/javascript" src="{{ asset('js/jquery-3.6.0.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/common.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/fly.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/popup.js') }}"></script>
  <script type="text/javascript"
    src="{{ asset('components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>
  {{-- cho --}}
  <script type="text/javascript" src="{{ asset('js/ejs.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/tmpl.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/modernizr.2.7.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery-migrate-3.3.2.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.placeholder.js') }}"></script>
  <script type="text/javascript" src="{{ asset('components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}">
  </script>
  <script type="text/javascript" src="{{ asset('components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('components/flexslider/jquery.flexslider.js') }}"></script>
  {{-- het co --}}
  <script type="text/javascript" src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.stellar.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/home/user.js') }}"></script>
  <script type="text/javascript"
    src="{{ asset('js/home/theme-scripts.js?t=' . File::lastModified(public_path('js/home/theme-scripts.js'))) }}">
  </script>
  @yield('footer-js')
  <script type="text/javascript">
    jQuery(document).ready(function() {
      $('.revolution-slider').revolution({
        dottedOverlay: "none",
        delay: 8000,
        startwidth: 1170,
        startheight: 646,
        onHoverStop: "on",
        hideThumbs: 10,
        fullWidth: "on",
        forceFullWidth: "on",
        navigationType: "none",
        shadow: 0,
        spinner: "spinner4",
        hideTimerBar: "on",
      });
    });
  </script>
  @if (isset($language))
    <script type="text/javascript">
      var language = {!! $language !!};
    </script>
  @endif
  @yield('footer')

  <!--Start of Tawk.to Script-->
  @yield('scripts')
  <script type="text/javascript">
    var Tawk_API = Tawk_API || {},
      Tawk_LoadStart = new Date();
    (function() {
      var s1 = document.createElement("script"),
        s0 = document.getElementsByTagName("script")[0];
      s1.async = true;
      s1.src = 'https://embed.tawk.to/563f2ca10717f5931049cebe/default';
      s1.charset = 'UTF-8';
      s1.setAttribute('crossorigin', '*');
      s0.parentNode.insertBefore(s1, s0);
    })();
  </script>
  <!--End of Tawk.to Script-->
</body>

</html>
