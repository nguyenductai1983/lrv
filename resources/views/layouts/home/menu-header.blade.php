<ul class="menu">
     <form class="navbar-form navbar-left" action="{{ route('order.tracking') }}" method="get">
  <div class="input-group">
    <input name="order-code" type="text" class="form-control" placeholder="{{ __('home.search_tracking') }} " size="20">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
@php if(empty(auth()->user())){
    $link='/transport/online';
}
else {
    $link='/transports/create';
}
    @endphp
<li class="menu-item-has-children megamenu-menu">
<a href="{{ $link }}" class="uppercase">{{ __('home.ship_now') }} <i class="fa fa-truck" aria-hidden="true"></i></a>
</li>
    @php $pages = $pages->where('type', config('page.type_header')); @endphp
    @foreach($pages as $key1 => $page1)
        @if ($page1->type == config('page.type_header') && $page1->parent_id == 0)
            @php $pages->forget($key1); @endphp
            <li class="menu-item-has-children megamenu-menu">
                <a href="javascript:void(0);">{{ $page1->translate->name }}</a>
                <div class="megamenu-wrapper container" data-items-per-column="8">
                    <div class="megamenu-holder">
                        <ul class="megamenu">
                            @foreach($pages as $key2 => $page2)
                                @if ($page2->parent_id == $page1->id)
                                    @php $pages->forget($key2); @endphp
                                    <li class="menu-item-has-children">
                                        <a href="javascript:void(0);">{{ $page2->translate->name }}</a>
                                        <ul class="clearfix">
                                            @foreach($pages as $key3 => $page3)
                                                @if ($page3->parent_id == $page2->id)
                                                    @php $pages->forget($key3); @endphp
                                                    <li>
                                                        <a href="{{ $page3->link_to ? url($page3->link_to) : route('pages.show', $page3->slug) }}">{{ $page3->translate->name }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </li>
        @endif
    @endforeach
</ul>
