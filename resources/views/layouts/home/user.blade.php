<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>
    <title>{{ __('label.seo-title') }}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{{ __('label.tiseo-keywords') }}"/>
    <meta name="description" content="{{ __('label.tiseo-description') }}">
    <meta name="author" content="SoapTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/fontawesome.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('components/flexslider/flexslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('components/revolution_slider/css/style.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('components/jquery.bxslider/jquery.bxslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('components/flexslider/flexslider.css') }}" media="screen"/>
    <link id="main-style" rel="stylesheet" href="{{ asset('css/home/style.css?t=' . File::lastModified(public_path('css/home/style.css'))) }}">
    <link rel="stylesheet" href="{{ asset('css/home/updates.css') }}">
    <link rel="stylesheet" href="{{ asset('css/home/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/loading.css') }}">
    <link rel="stylesheet" href="{{ asset('css/home/user.css') }}">
    <link rel="stylesheet" href="{{ asset('css/home/custom.css?t=' . File::lastModified(public_path('css/home/custom.css'))) }}">
    {{-- @yield('head-css') --}}

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home/ie.css') }}"/>
    <![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
<div id="page-wrapper">
    <header id="header" class="navbar-static-top">
        <div class="topnav hidden-xs">
            <div class="container">
                <ul class="quick-menu pull-left">
                    <li>
                        <a href="javascript:void(0);">
                            <i class="fa fa-phone"></i> {{ __('home.tollfree_info') }}
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" style="text-transform: none;">
                            <i class="fa fa-envelope-o"></i> {{ __('home.email_add') }}
                        </a>
                    </li>
                </ul>
                <ul class="quick-menu pull-right">
                    @php if(!empty(auth()->user())){@endphp
                    <li class="dropdown user-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('images/admin/user-4.png') }}" alt="user-image" class="img-circle img-inline userpic-32" width="28">
                            <span>{{ auth()->user()->email }} <i class="fa fa-sort-desc"></i></span>
                        </a>
                        <ul class="dropdown-menu user-profile-menu list-unstyled">
                            <li>
                                <a href="{{ route('transport.frontend.index') }}">
                                    <i class="fa fa-plane"></i>{{ __('home.header-manager-transport') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('express.list') }}">
                                    <i class="fa fa-globe"></i> {{ __('home.header-manager-express') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('yhl.frontend.index') }}">
                                    <i class="fa fa-truck"></i> {{ __('home.header-manager-yhl') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('mts.frontend.index') }}">
                                    <i class="fa fa-money-check-alt"></i> {{ __('home.header-manager-mts') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('profile.index') }}">
                                    <i class="fa fa-user"></i> {{ __('home.header-my-account') }}
                                </a>
                            </li>
                            <li class="last">
                                <a href="{{ route('customer.logout') }}">
                                    <i class="fa fa-power-off"></i> {{ __('home.sign-out') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    @php } @endphp
                    <li>
                        <a href="javascript:void(0);">
                            <i class="fa fa-shopping-cart"></i> {{ __('home.cart') }}(<span id="cart-count">{{ $cart_qty }}</span>)
                        </a>
                    </li>
                    <li class="dropdown select-language">
                        @php
                            $locales = config('app.locales');
                            $locale = app()->getLocale();
                        @endphp
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            {{ $locales[$locale] }} <i class="fa fa-sort-desc"></i>
                        </a>
                        <ul class="dropdown-menu select-language-menu list-unstyled">
                            @foreach($locales as $key => $lang)
                                @if ($key != $locale)
                                    <li>
                                        <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">
                                            <i class="fa fa-globe"></i> {{ $lang }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-header">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle"></a>

            <div class="container">
                <h1 class="logo navbar-brand">
                    <a href="{{ route('home.index') }}">
                        <img src="{{ asset('images/home/logo.png') }}">
                    </a>
                </h1>

                <nav id="main-menu" role="navigation">
                    @include('layouts.home.menu-header')
                </nav>
            </div>

            @include('layouts.home.menu-header-mobile')
        </div>
    </header>

    @yield('breadcrumbs')
    <div id="page-content">
        @include('layouts.home.user-menu-left')
        <section id="main-content">
            @yield('content')
        </section>
    </div>

    @include('layouts.home.footer')
</div>

<div class="loading" style="display:none">
    <div class="loading-inner"></div>
</div>

<!-- Javascript -->
<script type="text/javascript" src="{{ asset('js/jquery-3.6.0.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fly.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/ejs.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popup.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizr.2.7.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-3.3.2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('components/flexslider/jquery.flexslider.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/home/custom.js?t=' . File::lastModified(public_path('js/home/custom.js'))) }}"></script>
<script type="text/javascript" src="{{ asset('js/home/user.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/home/customer.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/home/theme-scripts.js?t=' . File::lastModified(public_path('js/home/theme-scripts.js'))) }}"></script>

@php if(isset($language)){@endphp
<script type="text/javascript">
    var language = {!! $language !!};
</script>
@php }@endphp
@yield('footer')
</body>
</html>
