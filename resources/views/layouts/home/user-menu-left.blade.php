<nav class="sidebar-menu toggle-others fixed" style="">
    <div class="sidebar-menu-inner ps-container">
        <header class="logo-env">
            <div class="mobile-menu-toggle visible-xs">
                <a href="#" data-toggle="mobile-menu">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
            <div class="settings-icon">
                <a href="#" data-toggle="settings-pane" data-animate="true">
                    <i class="linecons-cog"></i>
                </a>
            </div>
        </header>
        <div class="box-left">
             <div class="box-head">
            <div class="user-info-menu-left">
                <h3>{{ __('home.my-point') }}:
                    <strong>{{ number_format(auth()->user()->point, 0) }}</strong>
                </h3>
                @if(auth()->user()->point >= config('app.min_point_convert'))
                    <a href="javascript:void(0);" onclick="customer.convertPoint('{{ route('profile.coupon') }}')" class="convert-point">{{ __('home.convert-point') }}</a>
                @endif
            </div>
            </div>
</div>
<div class="box-left">
<div id="accordion">
  <div class="box-head">
        <a data-toggle="collapse" href="#collapseOne" class="title {{ isset($menu) && str_contains($menu, '1.') ? 'active' : '' }}">
        <i class="fa fa-user"></i> {{ __('home.my-account') }} {{ auth()->user()->id }}
      </a>
    <div id="collapseOne" class="{{ isset($menu) && str_contains($menu, '1.') ? 'collapse in' : 'collapse' }}" data-parent="#accordion">
         <div class="box-body-left">
                <ul id="main-menu" class="main-menu">
                    <li class="has-sub {{ isset($menu) && $menu == '1.1' ? 'active' : '' }}">
                        <a href="{{ route('profile.index') }}">
                            <i class="fa fa-user"></i>
                            <span class="title">{{ __('home.header-my-account') }}</span>
                        </a>
                    </li>
                    <li class="has-sub {{ isset($menu) && $menu == '1.2' ? 'active' : '' }}">
                        <a href="{{ route('profile.changepass') }}">
                            <i class="fa fa-lock"></i>
                            <span class="title">{{ __('home.change-password') }}</span>
                        </a>
                    </li>
                    <li class="has-sub {{ isset($menu) && $menu == '1.3' ? 'active' : '' }}">
                        <a href="{{ route('profile.address') }}">
                            <i class="fa fa-map-marker"></i>
                            <span class="title">{{ __('home.address') }}</span>
                        </a>
                    </li>
                    <li class="has-sub {{ isset($menu) && $menu == '1.4' ? 'active' : '' }}">
                        <a href="{{ route('profile.point') }}">
                            <i class="fas fa-money-check-alt"></i>
                            <span class="title">{{ __('home.point-manager') }}</span>
                        </a>
                    </li>
                    <li class="has-sub {{ isset($menu) && $menu == '1.5' ? 'active' : '' }}">
                        <a href="{{ route('profile.coupon') }}">
                           <i class="fas fa-money-check-alt"></i>
                            <span class="title">{{ __('home.coupon-manager') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
         </div>
    </div>
    <div class="box-left">
      <div class="box-head">
      <!--<a class="title" data-toggle="collapse" href="#collapsetwo">-->
        <a href="{{ route('transport.order.list') }}" class="{{ isset($menu) && $menu == '2.0' ? 'active' : '' }}">
        <i class="fa fa-shopping-cart"></i> {{ __('home.header-manager-transport') }} (iG-Transport)
      </a>
    </div>
   <!-- <div id="collapsetwo" class="collapse" data-parent="#accordion">
          <div class="box-body-left">
                <ul id="main-menu" class="main-menu">
                    <li class="has-sub {{ isset($menu) && $menu == '2.1' ? 'active' : '' }}">
                         <a href="{{ route('transport.order.list') }}">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="title">{{ __('home.list-quote') }}</span>
                        </a>
                    </li>
                    <li class="has-sub {{ isset($menu) && $menu == '2.2' ? 'active' : '' }}">
                        <a href="{{ route('transport.frontend.index') }}">
                            <i class="fa fa-cube"></i>
                            <span class="title">{{ __('home.list-order') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
    </div>  -->

  </div>
  </div>
</div>
        <div class="box-left">
            <div class="box-head">
                <a href="{{ route('express.list') }}" class="{{ isset($menu) && $menu == '3.0' ? 'active' : '' }}">
                   <i class="fa fa-globe"> </i> <span class="title">{{ __('home.header-manager-express') }} (iG-Express)</span>
                </a>
            </div>
        </div>
        <div class="box-left">
            <div class="box-head">
                <a href="{{ route('yhl.frontend.index') }}" class="{{ isset($menu) && $menu == '4.0' ? 'active' : '' }}">
                  <i class="fa fa-shopping-cart"> </i> <span class="title">{{ __('home.header-manager-yhl') }} (iG-YHL)</span>
                </a>
            </div>
        </div>

        <div class="box-left">
            <div class="box-head">
                <a href="{{ route('mts.frontend.index') }}" class="{{ isset($menu) && $menu == '5.0' ? 'active' : '' }}">
                    <i class="fas fa-money-check-alt"></i><span class="title">{{ __('home.header-manager-mts') }} (iG-MTS)</span>
                </a>
            </div>
        </div>

        <div class="box-left">
            <div class="box-head">
                <a href="{{ route('quote.frontend.index') }}" class="{{ isset($menu) && $menu == '6.0' ? 'active' : '' }}">
                  <i class="fa fa-shopping-cart"> </i> <span class="title">{{ __('home.header-manager-quote') }}</span>
                </a>
            </div>
        </div>

        <div class="box-left">
            <div class="box-head">
                <a href="{{ route('claim.frontend.index') }}" class="{{ isset($menu) && $menu == '7.0' ? 'active' : '' }}">
                     <i class="fa fa-bullhorn"></i> <span class="title">{{ __('home.claim-return') }}</span>
                </a>
            </div>
        </div>
      <div class="box-left">
            <div class="box-head">
        <div class="user-info-notification">
                 <h3>
            {{ __('user.thong_bao') }}
            <br>
            {{ __('user.thong_bao2') }}
            </h3>
            </div>
                </div>
       </div>
            </div>
</nav>
