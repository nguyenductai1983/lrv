<nav id="mobile-menu-01" class="mobile-menu collapse">
    <a href="/" class="active"><img src="/images/home/logo.png"></a>
    <ul id="mobile-primary-menu" class="menu">
        <li class="menu-item-has-children">
            <form class="navbar-form" action="{{ route('order.tracking') }}" method="get">
                <div class="input-group">
                    <input name="order-code" type="text" class="form-control"
                        placeholder="{{ __('home.search_tracking') }} " size="20">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            @php if(empty(auth()->user())){
            $link='/transport/online';
            }
            else {
            $link='/transports/create';
            }
            @endphp
        <li class="menu-item-has-children">
            <a href="{{ $link }}">{{ __('home.ship_now') }} <i class="fa fa-truck" aria-hidden="true"></i></a>
        </li>

        @php $pages = $pages->where('type', config('page.type_header'));
        $no=0;
        @endphp
        @foreach($pages as $key1 => $page1)
        @if ($page1->type == config('page.type_header') && $page1->parent_id == 0)
        @php $pages->forget($key1);
        $no++; @endphp
        <li class="menu-item-has-children">
            <a href="javascript:void(0);" data-target="#mobile-menu-submenu-item-{{$no}}"
                data-toggle="collapse">{{ $page1->translate->name }}</a>
            <ul>
                @foreach($pages as $key2 => $page2)
                @if ($page2->parent_id == $page1->id)
                @php $pages->forget($key2); @endphp
                <li class="menu-item-has-children">
                    <a href="javascript:void(0);">{{ $page2->translate->name }}</a>
                    <ul>
                        @foreach($pages as $key3 => $page3)
                        @if ($page3->parent_id == $page2->id)
                        @php $pages->forget($key3); @endphp
                        <li>
                            <a
                                href="{{ $page3->link_to ? url($page3->link_to) : route('pages.show', $page3->slug) }}">{{ $page3->translate->name }}</a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </li>
                @endif
                @endforeach
            </ul>
        </li>
        @endif
        @endforeach
        @php
        $no++;
        @endphp
        <li class="menu-item-has-children">
            <a href="javascript:void(0);" data-toggle="collapse"
                data-target="#mobile-menu-submenu-item-{{$no}}">{{ __('home.language') }}</a>
            <ul>
                @foreach(config('app.locales') as $key => $lang)
                <li>
                    <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">{{ $lang }}</a>
                </li>
                @endforeach
            </ul>
        </li>
    </ul>

    <ul class="mobile-topnav container">
        @php if(!empty(auth()->user())){@endphp
        <li class="dropdown user-profile">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('images/admin/user-4.png') }}" alt="user-image"
                    class="img-circle img-inline userpic-32" width="28">
                <span>{{ auth()->user()->email }} <i class="fa fa-sort-desc"></i></span>
            </a>
            <ul class="dropdown-menu user-profile-menu list-unstyled">
                <li>
                    <a href="{{ route('transport.frontend.index') }}">
                        <i class="fa fa-plane"></i> {{ __('home.header-manager-transport') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('express.list') }}">
                        <i class="fa fa-globe"></i> {{ __('home.header-manager-express') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('yhl.frontend.index') }}">
                        <i class="fa fa-truck"></i> {{ __('home.header-manager-yhl') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('mts.frontend.index') }}">
                        <i class="fa fa-money"></i> {{ __('home.header-manager-mts') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('profile.index') }}">
                        <i class="fa fa-user"></i> {{ __('home.header-my-account') }}
                    </a>
                </li>
                <li class="last">
                    <a href="{{ route('customer.logout') }}">
                        <i class="fa fa-power-off"></i> {{ __('home.sign-out') }}
                    </a>
                </li>
            </ul>
        </li>
        @php }else{ @endphp
        <li>
            <a href="#" data-toggle="modal" data-target="#dang-ky">
                <span class="bnt-signup">{{ __('home.sign-up') }}</span>
            </a>
        </li>
        <li>
            <a href="#" data-toggle="modal" data-target="#loginModal">
                {{ __('home.sign-in') }}
            </a>
        </li>
        @php } @endphp
        <li>
            <a href="{{ route('cart.step1') }}">
                <i class="fa fa-shopping-cart"></i> {{ __('home.cart') }}(<span id="cart-count">{{ $cart_qty }}</span>)
            </a>
        </li>
        <li class="dropdown select-language">
            @php
            $locales = config('app.locales');
            $locale = app()->getLocale();
            @endphp
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                {{ $locales[$locale] }} <i class="fa fa-sort-desc"></i>
            </a>
            <ul class="dropdown-menu select-language-menu list-unstyled">
                @foreach($locales as $key => $lang)
                @if ($key != $locale)
                <li>
                    <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">
                        <i class="fa fa-globe"></i> {{ $lang }}
                    </a>
                </li>
                @endif
                @endforeach
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);">
                <i class="fa fa-phone"></i> {{ __('home.hotline_info') }}
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <i class="fa fa-envelope-o"></i> {{ __('home.email_add') }}
            </a>
        </li>
    </ul>
</nav>
