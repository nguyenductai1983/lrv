<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="token" content="{{ csrf_token() }}">
  <meta name="description" content="Xenon Boostrap Admin Panel" />
  <meta name="author" content="" />
  <meta http-equiv="refresh" content="3200;url=/admin" />
  <title>{{ config('app.name') }}</title>
  {{-- <link href="https://fonts.googleapis.com/css?family=Roboto&amp;subset=vietnamese" rel="stylesheet"> --}}
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
  <link rel="stylesheet" href="{{ asset('css/fonts/linecons/css/linecons.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/brands.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/solid.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/all.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('components/select2/select2.css') }}">
  <link rel="stylesheet" href="{{ asset('components/select2/select2-bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('bower_components/jquery-ui/themes/base/jquery-ui.min.css') }}">
  <link rel="stylesheet" href="{{ asset('bower_components/angular/angular-csp.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin/xenon-core.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin/xenon-forms.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin/xenon-components.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin/xenon-skins.css') }}">
  <link rel="stylesheet" href="{{ asset('css/loading.css') }}">
  <link rel="stylesheet"
    href="{{ asset('css/admin/custom.css?t=' . File::lastModified(public_path('css/admin/custom.css'))) }}">
  @yield('head')
  <script src="{{ asset('js/jquery-3.6.0.js') }}"></script>
  <script type="text/javascript">
    window.iGreenLink = {
      user_id: '{!! auth()->user()->id !!}',
      pub: '{!! config('
            app.pubnub_publish_key ') !!}',
      sub: '{!! config('
            app.pubnub_subcribe_key ') !!}'
    };

  </script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="page-body">

  <div class="settings-pane">
    <a href="#" data-toggle="settings-pane" data-animate="true">&times;</a>
    <div class="settings-pane-inner">
      <div class="row">
        <div class="col-md-4">
          <div class="user-info">
            <div class="user-image">
              <a href="javascript:void(0);">
                <i class="fas fa-user-circle"></i>
                <img src="{{ asset('images/admin/user-2.png') }}" class="img-responsive img-circle" />
              </a>
            </div>
            <div class="user-details">
              <h3>
                <a href="#">{{ auth()->user()->full_name }}</a>
                <span class="user-status is-online"></span>
              </h3>
              <div class="user-links">
                <a href="{{ route('admin.profile.info') }}" class="btn btn-primary">
                  <i class="fa fa-edit"></i> {{ __('label.edit_profile') }}
                </a>
                <a href="{{ route('admin.profile.change-password') }}" class="btn btn-primary">
                  <i class="fa-lock"></i> {{ __('label.change_password') }}
                </a>
                <a href="javascript:void(0);" class="btn btn-primary"
                  onclick="event.preventDefault();document.getElementById('logout-form-2').submit();">
                  <i class="fa-sign-out"></i> {{ __('label.logout') }}
                </a>
                <form id="logout-form-2" method="post" action="{{ route('admin.logout.submit') }}">
                  {{ csrf_field() }}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="page-container">
    <div class="sidebar-menu toggle-others fixed{{ session()->get('config.menuCollapsed') ? ' collapsed' : '' }}">
      <div class="sidebar-menu-inner">
        <header class="logo-env">
          <div class="logo">
            <a href="{{ route('admin.index') }}" class="logo-expanded">
              <img src="{{ asset('images/admin/logo@2x.png') }}" width="80" alt="" />
            </a>
            <a href="{{ route('admin.index') }}" class="logo-collapsed">
              <img src="{{ asset('images/admin/logo-collapsed@2x.png') }}" width="40" alt="" />
            </a>
          </div>
          <div class="mobile-menu-toggle visible-xs">
            <a href="#" data-toggle="mobile-menu">
              <i class="fas fa-bars"></i>
            </a>
          </div>
          <div class="settings-icon">
            <a href="#" data-toggle="settings-pane" data-animate="true">
              <i class="linecons-cog"></i>
            </a>
          </div>
        </header>

        @include('layouts.admin.menu')
      </div>
    </div>

    <div class="main-content">
      <nav class="navbar user-info-navbar" role="navigation">
        <ul class="user-info-menu left-links list-inline list-unstyled">
          <li class="hidden-sm hidden-xs">
            <a href="#" data-toggle="sidebar">
              <i class="fas fa-bars"></i>
            </a>
          </li>
          <li class="dropdown hover-line" style="min-height: 76px;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <i class="fas fa-bell"></i>
              <span class="badge badge-danger" id="notifi_qty">{{ $notifi_qty > 0 ? $notifi_qty : 0 }}</span>
            </a>
            <ul class="dropdown-menu notifications" id="notification_content">
              @if ($notifi_qty > 0)
                <li>
                  <ul class="dropdown-menu-list list-unstyled ps-scrollbar ps-container">
                    @foreach ($notifi_top_contents as $notifi_top_content)
                      <li class="notification-success">
                        <a href="{{ $notifi_top_content->path }}">
                          <span class="line">
                            {{ $notifi_top_content->name }}
                          </span>
                        </a>
                      </li>
                    @endforeach
                  </ul>
                </li>
              @else
                <li>
                  <ul class="dropdown-menu-list list-unstyled ps-scrollbar ps-container">
                    <li class="notification-primary">
                      <a href="javascript:void(0);">
                        <span class="line">{{ __('label.no_notify') }}</span>
                      </a>
                    </li>
                  </ul>
                </li>
              @endif
            </ul>
          </li>
          <li class="dropdown hover-line language-switcher">
            @php
              $locales = config('app.locales');
              $locale = app()->getLocale();
            @endphp
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('images/admin/flags/flag-' . $locale . '.png') }}" />
              {{ $locales[$locale] }}
            </a>
            <ul class="dropdown-menu languages">
              @foreach ($locales as $key => $lang)
                @if ($key != $locale)
                  <li>
                    <a href="{{ request()->fullUrlWithQuery(['locale' => $key]) }}">
                      <img src="{{ asset('images/admin/flags/flag-' . $key . '.png') }}" />
                      {{ $lang }}
                    </a>
                  </li>
                @endif
              @endforeach
            </ul>
          </li>
        </ul>

        <ul class="user-info-menu right-links list-inline list-unstyled">
          <li class="dropdown user-profile">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fas fa-user-circle" style="font-size: 28px;"></i>
              <span>
                {{ auth()->user()->full_name }} <i class="fas fa-angle-down"></i></span>
            </a>
            <ul class="dropdown-menu user-profile-menu list-unstyled">
              <li>
                <a href="{{ route('admin.profile.info') }}">
                  <i class="fas fa-user"></i> {{ __('label.edit_profile') }}
                </a>
              </li>
              <li>
                <a href="{{ route('admin.profile.change-password') }}">
                  <i class="fas fa-lock"></i> {{ __('label.change_password') }}
                </a>
              </li>
              <li class="last">
                <a href="javascript:void(0);"
                  onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <i class="fas fa-sign-out-alt"></i> {{ __('label.logout') }}
                </a>
                <form id="logout-form" method="post" action="{{ route('admin.logout.submit') }}">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      @yield('body')
      <footer class="main-footer sticky footer-type-1">
        <div class="footer-inner">
          <div class="footer-text">
            Copyright &copy; 2017 <strong>iGreen</strong>
          </div>
          <div class="go-up">
            <a href="#" rel="go-top">
              <i class="fas fa-arrow-up" style="font-size: 20pt"></i>
            </a>
          </div>
        </div>
        @include('partials.languages')
      </footer>
    </div>
  </div>
  <div class="loading" style="display:none">
    <div class="loading-inner"></div>
  </div>
  <!-- Bottom Scripts -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/admin/TweenMax.min.js') }}"></script>
  <script src="{{ asset('js/admin/resizeable.js') }}"></script>
  <script src="{{ asset('js/admin/joinable.js') }}"></script>
  <script src="{{ asset('components/select2/select2.min.js') }}"></script>

  <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
  <script src="{{ asset('bower_components/blueimp-file-upload/js/jquery.iframe-transport.js') }}"></script>
  <script src="{{ asset('bower_components/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
  <script src="{{ asset('bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
  <script src="{{ asset('js/admin/xenon-api.js') }}"></script>
  <script src="{{ asset('js/admin/xenon-toggles.js') }}"></script>
  <!-- JavaScripts initializations and stuff -->
  <script
    src="{{ asset('js/admin/xenon-custom.js?t=' . File::lastModified(public_path('js/admin/xenon-custom.js'))) }}">
  </script>
  <script src="{{ asset('js/admin/custom.js?t=' . File::lastModified(public_path('js/admin/custom.js'))) }}">
  </script>
  <script src="{{ asset('js/fly.js') }}"></script>
  <script src="{{ asset('js/ejs.js') }}"></script>
  <script src="{{ asset('js/tmpl.js') }}"></script>
  <script src="{{ asset('js/popup.js') }}"></script>
  {{-- PubNub --}}
  {{-- <script src="//cdn.pubnub.com/sdk/javascript/pubnub.4.29.8.min.js"></script>
    <script src="{{ asset('js/pubnub.js') }}"></script> --}} --}}
  {{-- End PubNub --}}
  {{-- Notification --}}
  {{-- <script src="{{ asset('js/notification.js') }}"></script> --}}
  {{-- End Notification --}}
  @if (isset($language))
    <script type="text/javascript">
      var language = {!! $language !!};
    </script>
  @endif
  @yield('footer')
</body>

</html>
