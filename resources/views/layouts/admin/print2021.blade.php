<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <title>Print</title>
        <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('css/printbill2021.css') }}">
        <script src="/js/JsBarcode.all.min.js"></script>
        @yield('header')
    </head>
    <body>
        @yield('body')
    </body>
</html>
