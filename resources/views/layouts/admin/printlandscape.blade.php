<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <title>Print</title>
        <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/fontawesome.css') }}">
        <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/brands.css') }}">
        <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/solid.css') }}">
        <link rel="stylesheet" href="{{ asset('css/printbilllandscape.css') }}">
        {{-- <script src="/js/JsBarcode.all.min.js"></script> --}}
     </head>
    <body>
        @yield('body')
    </body>
</html>
