<ul id="main-menu" class="main-menu">
    <li class="{{ isset($menu) && str_contains($menu, 'dashboard') ? 'opened active' : '' }}">
        <a href="{{ route('admin.index') }}">
            <i class="fas fa-tachometer-alt"></i>
            <span class="title">{{ __('label.dashboard') }}</span>
        </a>
    </li>

    @can('manage-admin')
    <li class="{{ isset($menu) && str_contains($menu, '2.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fas fa-archive"></i>
            <span class="title">Admin</span>
        </a>
        <ul>
            @can('manage-osystems')
            <li class="{{ isset($menu) && str_contains($menu, '1.') ? 'opened active' : '' }}">
                <a href="javascript:void(0);">
                    <i class="fa fa-cogs"></i>
                    <span class="title">{{ __('label.system') }}</span>
                </a>

                <ul>
                    @can('manage-configs')
                    <li class="{{ isset($menu) && $menu == '1.1' ? 'active' : '' }}">
                        <a href="{{ route('admin.configs.index') }}">
                            <i class="fas fa-check-double"></i><span class="title">{{ __('config.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-agencies')
                    <li class="{{ isset($menu) && $menu == '1.2' ? 'active' : '' }}">
                        <a href="{{ route('admin.agencies.index') }}">
                            <i class="fas fa-diagnoses"></i><span class="title">{{ __('agency.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-roles')
                    <li class="{{ isset($menu) && $menu == '1.6' ? 'active' : '' }}">
                        <a href="{{ route('admin.roles.index') }}">
                            <i class="fas fa-users"></i></i><span class="title">{{ __('role.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-users')
                    <li class="{{ isset($menu) && $menu == '1.7' ? 'active' : '' }}">
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fas fa-user"></i><span class="title">{{ __('user.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-campaign')
                    <li class="{{ isset($menu) && $menu == '1.5' ? 'active' : '' }}">
                        <a href="{{ route('admin.campaign.index') }}">
                            <i class="fas fa-campground"></i><span class="title">{{ __('campaign.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-logs')
                    <li class="{{ isset($menu) && $menu == '1.8' ? 'active' : '' }}">
                        <a href="{{ route('admin.logs.index') }}">
                            <i class="fas fa-history"></i><span class="title">{{ __('log.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('manage-ocategorys')
            <li class="{{ isset($menu) && str_contains($menu, '2.') ? 'opened active' : '' }}">
                <a href="javascript:void(0);">
                    <i class="fas fa-archive"></i>
                    <span class="title">{{ __('label.category') }}</span>
                </a>
                <ul>
                    @can('manage-dimensions')
                    <li class="{{ isset($menu) && $menu == '2.2' ? 'active' : '' }}">
                        <a href="{{ route('admin.dimensions.index') }}">
                            <i class="fas fa-ruler-vertical"></i><span class="title">{{ __('label.dimension_unit') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-measures')
                    <li class="{{ isset($menu) && $menu == '2.3' ? 'active' : '' }}">
                        <a href="{{ route('admin.measures.index') }}">
                            <i class="fas fa-weight-hanging"></i><span class="title">{{ __('label.messure_unit') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-weights')
                    <li class="{{ isset($menu) && $menu == '2.4' ? 'active' : '' }}">
                        <a href="{{ route('admin.weights.index') }}">
                            <i class="fas fa-weight"></i><span class="title">{{ __('label.weight_unit') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-warehouses')
                    <li class="{{ isset($menu) && $menu == '2.5' ? 'active' : '' }}">
                        <a href="{{ route('admin.warehouses.index') }}">
                            <i class="fas fa-warehouse"></i><span class="title">{{ __('label.warehouse_manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-shipping-methods')
                    <li class="{{ isset($menu) && $menu == '2.15' ? 'active' : '' }}">
                        <a href="{{ route('admin.shipping-methods.index') }}">
                            <i class="fas fa-shipping-fast"></i><span class="title">{{ __('label.shipping_method') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-shipping-providers')
                    <li class="{{ isset($menu) && $menu == '2.11' ? 'active' : '' }}">
                        <a href="{{ route('admin.shipping-providers.index') }}">
                            <i class="fas fa-dolly-flatbed"></i><span class="title">{{ __('label.shipping_provider') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-shipping-provider-methods')
                    <li class="{{ isset($menu) && $menu == '2.16' ? 'active' : '' }}">
                        <a href="{{ route('admin.shipping-method-providers.index') }}">
                            <i class="fas fa-dolly-flatbed"></i><span class="title">{{ __('label.shipping_provider_method') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-payment-methods')
                    <li class="{{ isset($menu) && $menu == '2.12' ? 'active' : '' }}">
                        <a href="{{ route('admin.payment-methods.index') }}">
                            <i class="fas fa-money-check-alt"></i><span class="title">{{ __('label.payment_method') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-point-configs')
                    <li class="{{ isset($menu) && $menu == '2.14' ? 'active' : '' }}">
                        <a href="{{ route('admin.point-configs.index') }}">
                            <i class="fas fa-gifts"></i><span class="title">{{ __('point.manager-config') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-point-converts')
                    <li class="{{ isset($menu) && $menu == '2.13' ? 'active' : '' }}">
                        <a href="{{ route('admin.point-converts.index') }}">
                            <i class="fas fa-percent"></i><span class="title">{{ __('point.manager-convert') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-currencies')
                    <li class="{{ isset($menu) && $menu == '2.6' ? 'active' : '' }}">
                        <a href="{{ route('admin.currencies.index') }}">
                            <i class="fas fa-dollar-sign"></i><span class="title">{{ __('currency.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-countries')
                    <li class="{{ isset($menu) && $menu == '2.7' ? 'active' : '' }}">
                        <a href="{{ route('admin.countries.index') }}">
                            <i class="fas fa-globe-africa"></i><span class="title">{{ __('country.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-provinces')
                    <li class="{{ isset($menu) && $menu == '2.8' ? 'active' : '' }}">
                        <a href="{{ route('admin.provinces.index') }}">
                            <i class="fas fa-city"></i><span class="title">{{ __('province.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-cities')
                    <li class="{{ isset($menu) && $menu == '2.9' ? 'active' : '' }}">
                        <a href="{{ route('admin.cities.index') }}">
                            <i class="fas fa-city"></i><span class="title">{{ __('city.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-wards')
                    <li class="{{ isset($menu) && $menu == '2.10' ? 'active' : '' }}">
                        <a href="{{ route('admin.wards.index') }}">
                            <i class="fas fa-city"></i><span class="title">{{ __('city.wards') }}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('manage-oappearances')
            <li class="{{ isset($menu) && str_contains($menu, '10.') ? 'opened active' : '' }}">
                <a href="javascript:void(0);">
                    <i class="fa fa-th-large"></i>
                    <span class="title">{{ __('label.appearance') }}</span>
                </a>
                <ul>
                    @can('manage-pages')
                    <li class="{{ isset($menu) && $menu == '10.1' ? 'active' : '' }}">
                        <a href="{{ route('admin.pages.index', ['type' => config('page.type_header')]) }}">
                            <i class="fas fa-heading"></i>
                            <span class="title">Header</span>
                        </a>
                    </li>
                    <li class="{{ isset($menu) && $menu == '10.2' ? 'active' : '' }}">
                        <a href="{{ route('admin.pages.index', ['type' => config('page.type_footer')]) }}">
                            <i class="fas fa-shoe-prints"></i>
                            <span class="title">Footer</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-pages')
                    <li class="{{ isset($menu) && $menu == '10.3' ? 'active' : '' }}">
                        <a href="{{ route('admin.banners.index') }}">
                            <i class="fas fa-sliders-h"></i>
                            <span class="title">Slider</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-news')
                    <li class="{{ isset($menu) && $menu == '10.4' ? 'active' : '' }}">
                        <a href="{{ route('admin.news.index') }}">
                            <i class="fas fa-newspaper"></i>
                            <span class="title">{{ __('news.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-helps')
                    <li class="{{ isset($menu) && $menu == '10.5' ? 'active' : '' }}">
                        <a href="{{ route('admin.helps.index') }}">
                            <i class="fab fa-hire-a-helper"></i>
                            <span class="title">{{ __('helps.manage') }}</span>
                        </a>
                    </li>
                    @endcan
                    @can('manage-bulletin')
                    <li class="{{ isset($menu) && $menu == '10.6' ? 'active' : '' }}" id="bulletin">
                        <a href="{{ route('admin.bulletin.index') }}#bulletin">
                            <i class="fas fa-bullhorn"></i>
                            <span class="title">{{ __('news.bulletin') }}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan

        </ul>
    </li>
    @endcan




    @can('manage-ocustomers')
    <li class="{{ isset($menu) && str_contains($menu, '3.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fa fa-users"></i>
            <span class="title">{{ __('label.customer') }}</span>
        </a>
        <ul>
            @can('manage-customer-groups')
            <li class="{{ isset($menu) && $menu == '3.1' ? 'active' : '' }}">
                <a href="{{ route('admin.customer-groups.index') }}">
                    <i class="fas fa-users-cog"></i>
                    <span class="title">{{ __('label.customer_groups') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-customers')
            <li class="{{ isset($menu) && $menu == '3.2' ? 'active' : '' }}">
                <a href="{{ route('admin.customers.index') }}">
                    <i class="fas fa-user-friends"></i>
                    <span class="title">{{ __('label.customer') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-customers')
            <li class="{{ isset($menu) && $menu == '3.4' ? 'active' : '' }}">
                <a href="{{ route('admin.customers.points') }}">
                    <i class="fas fa-gift"></i>
                    <span class="title">{{ __('home.point-manager') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-customers')
            <li class="{{ isset($menu) && $menu == '3.5' ? 'active' : '' }}">
                <a href="{{ route('admin.customers.coupons') }}">
                    <i class="fas fa-gifts"></i>
                    <span class="title">{{ __('home.coupon-manager') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-ocatalogs')
    <li class="{{ isset($menu) && str_contains($menu, '8.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fas fa fa-bars"></i>
            <span class="title">{{ __('label.catalog') }}</span>
        </a>
        <ul>
            @can('manage-product-groups')
            <li class="{{ isset($menu) && $menu == '8.1' ? 'active' : '' }}">
                <a href="{{ route('admin.product-groups.index') }}">
                    <i class="fab fa-product-hunt"></i>
                    <span class="title">{{ __('label.product_groups') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-products')
            <li class="{{ isset($menu) && $menu == '8.2' ? 'active' : '' }}">
                <a href="{{ route('admin.products.index') }}">
                    <i class="fas fa-box"></i>
                    <span class="title">{{ __('label.product') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-otransport')
    <li class="{{ isset($menu) && str_contains($menu, '4.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fa fa-truck"></i>
            <span class="title">Transport</span>
        </a>
        <ul>
            @can('manage-transport')
            <li class="{{ isset($menu) && $menu == '4.1' ? 'active' : '' }}">
                <a href="{{ route('transport.index') }}">
                    <i class="fas fa-sort-amount-down-alt"></i>
                    <span class="title">{{ __('transport.menu_agency_order') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-quote')
            <li class="{{ isset($menu) && $menu == '4.11' ? 'active' : '' }}">
                <a href="{{ route('transport.quote.index') }}">
                    <i class="fas fa-globe"></i>
                    <span class="title">{{ __('transport.menu_customer_quote') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-quote-order')
            <li class="{{ isset($menu) && $menu == '4.12' ? 'active' : '' }}">
                <a href="{{ route('transport.quote.order.index') }}">
                    <i class="fas fa-globe"></i>
                    <span class="title">{{ __('transport.menu_customer_order') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-pickup')
            <li class="{{ isset($menu) && $menu == '4.13' ? 'active' : '' }}">
                <a href="{{ route('transport.pickup.index') }}">
                    <i class="fas fa-truck-pickup"></i>
                    <span class="title">{{ __('transport.menu_pick_up') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-warehouse')
            <li class="{{ isset($menu) && $menu == '4.2' ? 'active' : '' }}">
                <a href="{{ route('transport.warehouse.index') }}">
                    <i class="fas fa-warehouse"></i>
                    <span class="title">{{ __('transport.menu_stockin') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-shipment')
            <li class="{{ isset($menu) && $menu == '4.3' ? 'active' : '' }}">
                <a href="{{ route('transport.shipment.index') }}">
                    <i class="fas fa-pallet"></i>
                    <span class="title">{{ __('transport.menu_shipment') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-stockout')
            <li class="{{ isset($menu) && $menu == '4.4' ? 'active' : '' }}">
                <a href="{{ route('transport.stockout.index') }}">
                    <i class="fab fa-stack-overflow"></i>
                    <span class="title">{{ __('transport.menu_stockout') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-receive')
            <li class="{{ isset($menu) && $menu == '4.5' ? 'active' : '' }}">
                <a href="{{ route('transport.receive.index') }}">
                    <i class="fas fa-people-carry"></i>
                    <span class="title">{{ __('transport.menu_receiver') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-carrier')
            <li class="{{ isset($menu) && $menu == '4.6' ? 'active' : '' }}">
                <a href="{{ route('transport.carrier.index') }}">
                    <i class="fas fa-dolly-flatbed"></i>
                    <span class="title">{{ __('transport.menu_carrier') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-deliver')
            <li class="{{ isset($menu) && $menu == '4.7' ? 'active' : '' }}">
                <a href="{{ route('transport.deliver.approved') }}">
                    <i class="fas fa-people-carry"></i>
                    <span class="title">{{ __('transport.menu_pickup') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-complete')
            <li class="{{ isset($menu) && $menu == '4.8' ? 'active' : '' }}">
                <a href="{{ route('transport.complete.approved') }}">
                    <i class="fas fa-shipping-fast"></i>
                    <span class="title">{{ __('transport.menu_complete') }} ABS</span>
                </a>
            </li>
            @endcan
            @can('manage-transport-tracking')
            <li class="{{ isset($menu) && $menu == '4.9' ? 'active' : '' }}">
                <a href="{{ route('transport.tracking.index') }}">
                    <i class="fab fa-stack-exchange"></i>
                    <span class="title">{{ __('transport.menu_tracking') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-oexpress')
    <li class="{{ isset($menu) && str_contains($menu, '5.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
         <i class="fa fa-plane"></i>
            <span class="title">Express</span>
        </a>
        <ul>
            @can('manage-express-agency')
            <li class="{{ isset($menu) && $menu == '5.1' ? 'active' : '' }}">
                <a href="{{ route('admin.express.list') }}">
                    <span class="title">{{ __('express.menu_agency_order') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-express-customer')
            <li class="{{ isset($menu) && $menu == '5.2' ? 'active' : '' }}">
                <a href="{{ route('customer.express.list') }}">
                    <span class="title">{{ __('express.menu_customer_order') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-oyhl')
    <li class="{{ isset($menu) && str_contains($menu, '6.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fas fa-store"></i>
            <span class="title">YHL</span>
        </a>
        <ul>
            @can('manage-yhl')
            <li class="{{ isset($menu) && $menu == '6.1' ? 'active' : '' }}">
                <a href="{{ route('yhl.index') }}">
                    <span class="title">{{ __('yhl.menu_agency_order') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-yhl-customer')
            <li class="{{ isset($menu) && $menu == '6.7' ? 'active' : '' }}">
                <a href="{{ route('yhl.customer.index') }}">
                    <span class="title">{{ __('yhl.menu_customer_order') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-yhl-stockout')
            <li class="{{ isset($menu) && $menu == '6.2' ? 'active' : '' }}">
                <a href="{{ route('yhl.stockout.index') }}">
                    <span class="title">{{ __('yhl.menu_stockout') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-yhl-carrier')
            <li class="{{ isset($menu) && $menu == '6.3' ? 'active' : '' }}">
                <a href="{{ route('yhl.carrier.index') }}">
                    <span class="title">{{ __('yhl.menu_carrier') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-yhl-deliver')
            <li class="{{ isset($menu) && $menu == '6.4' ? 'active' : '' }}">
                <a href="{{ route('yhl.deliver.approved') }}">
                    <span class="title">{{ __('yhl.menu_pickup') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-yhl-complete')
            <li class="{{ isset($menu) && $menu == '6.5' ? 'active' : '' }}">
                <a href="{{ route('yhl.complete.approved') }}">
                    <span class="title">{{ __('yhl.menu_complete') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-yhl-tracking')
            <li class="{{ isset($menu) && $menu == '6.6' ? 'active' : '' }}">
                <a href="{{ route('yhl.tracking.index') }}">
                    <span class="title">{{ __('yhl.menu_tracking') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-warehouses')
            <li class="{{ isset($menu) && $menu == '2.6' ? 'active' : '' }}">
                <a href="{{ route('yhl.instock.index') }}">
                    <i class="fab fa-product-hunt"></i>
                    <span class="title">{{ __('label.stockin') }} </span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-omts')
    <li class="{{ isset($menu) && str_contains($menu, '7.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fas fa-hand-holding-usd"></i>
            <span class="title">MTS</span>
        </a>
        <ul>
            @can('manage-mts')
            <li class="{{ isset($menu) && $menu == '7.1' ? 'active' : '' }}">
                <a href="{{route('mts.index')}}">
                    <span class="title">{{ __('transport.menu_order') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-mts-quote')
            <li class="{{ isset($menu) && $menu == '7.2' ? 'active' : '' }}">
                <a href="{{ route('mts.quote.index') }}">
                    <span class="title">{{ __('transport.menu_customer_quote') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-shopforme')
    <li class="{{ isset($menu) && str_contains($menu, 'shopforme.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fa fa-shopping-cart"></i>
            <span class="title">{{ __('home.header-manager-quote') }}</span>
        </a>
        <ul>
            @can('manage-shopforme')
            <li class="{{ isset($menu) && $menu == 'shopforme.1' ? 'active' : '' }}">
                <a href="{{route('quote.admin.index')}}">
                    <span class="title">{{ __('quote.menu_shopforme') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('manage-extent')
    <li class="{{ isset($menu) && str_contains($menu, 'extent.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fa fa-tasks"></i>
            <span class="title">{{ __('label.extra_refund') }}</span>
        </a>
        <ul>
            @can('manage-extent')
            <li class="{{ isset($menu) && $menu == 'extent.1' ? 'active' : '' }}">
                <a href="{{ route('admin.extent.index') }}">
                    <span class="title">{{ __('label.extra_fee') }}</span>
                </a>
            </li>
            @endcan
        </ul>
        <ul>
            @can('manage-vnpt_confirm')
            <li class="{{ isset($menu) && $menu == 'extent.2' ? 'active' : '' }}">
                <a href="{{ route('admin.vnptconfirm.index') }}">
                    <span class="title">{{ __('label.confirm') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-claim')
    <li class="{{ isset($menu) && str_contains($menu, 'claim.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fa fa-bullhorn"></i>
            <span class="title">{{ __('label.claim') }}</span>
        </a>
        <ul>
            @can('manage-claim')
            <li class="{{ isset($menu) && $menu == 'claim.1' ? 'active' : '' }}">
                <a href="{{ route('admin.claim.index') }}">
                    <i class="fas fa-question"></i> <span class="title">{{ __('label.request') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-contact')
            <li class="{{ isset($menu) && $menu == 'claim.2' ? 'active' : '' }}">
                <a href="{{ route('admin.claim.contact') }}">
                    <i class="fas fa-file-signature"></i>
                    <span class="title">{{ __('label.contact') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('manage-oreport')
    <li class="{{ isset($menu) && str_contains($menu, 'receipt.') ? 'opened active' : '' }}">
        <a href="javascript:void(0);">
            <i class="fa fa-list-alt"></i>
            <span class="title">{{ __('label.report') }}</span>
        </a>
        <ul>
            @can('manage-receipts')
            <li class="{{ isset($menu) && $menu == 'receipt.1' ? 'active' : '' }}">
                <a href="{{ route('admin.receipts.index') }}">
                    <span class="title">{{ __('label.receipts') }}</span>
                </a>
            </li>
            @endcan

            @can('manage-receipts-order')
            <li class="{{ isset($menu) && $menu == 'receipt.2' ? 'active' : '' }}">
                <a href="{{ route('admin.receipt-orders.index') }}">
                    <span class="title">{{ __('label.receipts_order') }}</span>
                </a>
            </li>
            @endcan

            @can('manage-receipts-mts')
            <li class="{{ isset($menu) && $menu == 'receipt.3' ? 'active' : '' }}">
                <a href="{{ route('admin.receipt-mts.index') }}">
                    <span class="title">{{ __('label.receipts_mts') }}</span>
                </a>
            </li>
            @endcan
            @can('manage-receipts-extra')
            <li class="{{ isset($menu) && $menu == 'receipt.4' ? 'active' : '' }}">
                <a href="{{ route('admin.receipt-extra.index') }}">
                    <span class="title">{{ __('label.receipts_extra') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan


</ul>
