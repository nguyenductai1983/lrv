<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="Igreencorp Admin Panel"/>
  <meta name="author" content=""/>
  <title>{{ config('app.name') }}</title>
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Arimo:400,700,400italic">
  {{-- <link rel="stylesheet" href="{{ asset('css/fonts/linecons/css/linecons.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/fonts/fontawesome/css/all.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin/xenon-core.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('css/admin/xenon-forms.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/admin/xenon-components.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('css/admin/xenon-skins.css') }}"> --}}
  {{-- <link rel="stylesheet" href="{{ asset('css/admin/custom.css?t=' . File::lastModified(public_path('css/admin/custom.css'))) }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/loading.css') }}">

  <script src="{{ asset('js/jquery-3.6.0.js') }}"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
    <body>
    @yield('content')

<div class="loading" style="display:none">
    <div class="loading-inner"></div>
</div>
<!-- Bottom Scripts -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
@yield('footer')
</body>
</html>
