<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>       
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
    </head> 
    <body bgcolor="#cecccc" width="100%" style="margin: 0;">
        <div style="width:100%;background:#cecccc;">
            <table cellspacing="0" cellpadding="0" border="0" align="center" width="640" style="margin:auto;width:640px;min-width:640px;max-width:640px;" class="email-container">
                @include('layouts.email.header')
                @yield('content')
                @include('layouts.email.footer')
            </table>
        </div>
    </body>
</html>