<tr>
    <td id="m_6304820631880916025header" style="background-color:#3c9444;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
        <table class="m_6304820631880916025body" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
            <tbody>
                <tr>
                    <td style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                        <table class="m_6304820631880916025col-md-5 m_6304820631880916025col-xs-12" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;width:495px">
                            <tbody>
                                <tr>
                                    <td id="m_6304820631880916025logo" style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:5px;padding-bottom:0;padding-right:0;padding-left:24px">
                                        <a href="{{ route('home.index') }}" style="border-style:none;color:#0063d1;display:block;text-decoration:none" target="_blank" >
                                            <img src="{{ asset('images/home/logo.png') }}" style="border-width:0px;vertical-align:bottom;height: 23px;margin-top: 4px;" class="CToWUd">
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="m_6304820631880916025col-md-7 m_6304820631880916025col-xs-12" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;width:140px">
                            <tbody>
                                <tr>
                                    <td id="m_6304820631880916025slogans" style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                        <table class="m_6304820631880916025body" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td class="m_6304820631880916025slogan" id="m_6304820631880916025slogan_free_delivery" style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:15px;padding-bottom:15px;padding-right:0;padding-left:0">
                                                        <table class="m_6304820631880916025body" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left">
                                                                        <a href="tel:18886196869" style="border-style:none;color:#ffffff;font-size:11px;text-decoration:none;text-transform:uppercase" target="_blank">HOTLINE: 1 (888) 619-6869</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>