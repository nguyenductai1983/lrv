<tr>
    <td id="m_6304820631880916025footer_support" style="background-color:#ffffff;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
        <table class="m_6304820631880916025body" style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
            <tbody>
                <tr>
                    <td style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                        {{-- <table class="m_6304820631880916025col-md-6 m_6304820631880916025col-xs-12" cellspacing="0" cellpadding="0" align="left" style="border-collapse:collapse;width:320px">
                            <tbody>
                                <tr>
                                    <td class="m_6304820631880916025fotter-support m_6304820631880916025padding-5px-xs" style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0px;padding-bottom:20px;padding-right:10px;padding-left:20px;text-align:center;width:50%">
                                        <table class="m_6304820631880916025body" style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color:#f6f6f6;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:20px;padding-bottom:13px;padding-right:0;padding-left:0">
                                                        <table class="m_6304820631880916025body" style="border-collapse:collapse;font-family:Arial,sans-serif;min-height:120px;width:100%">
                                                            <tbody>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025title" style="font-family:Arial,sans-serif;font-size:16px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;text-transform:uppercase">{{ __('email.support-customer')}}
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025divider" style="height:22px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:2px;padding-bottom:2px;padding-right:0;padding-left:0">
                                                                        <span style="background-color:#999999;display:inline-block;font-family:Arial,sans-serif;height:2px;width:50px"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:20px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="#" style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none" target="_blank">{{ __('email.guide-shopping')}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:20px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="#" style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none" target="_blank">{{ __('email.guide-payment')}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:20px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="#" style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none" target="_blank">{{ __('email.policy-support')}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:20px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="#" style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none" target="_blank">{{ __('email.transaction-policy')}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:20px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="#" style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none" target="_blank">{{ __('email.privacy-policy')}}</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>--}}

                        <table class="m_6304820631880916025col-md-6 m_6304820631880916025col-xs-12" cellspacing="0" cellpadding="0" align="left" style="border-collapse:collapse">
                            <tbody>
                                <tr>
                                    <td class="m_6304820631880916025fotter-connect m_6304820631880916025padding-5px-xs m_6304820631880916025padding-bt-10px-xs" style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0px;padding-bottom:20px;padding-right:20px;padding-left:10px;text-align:center;width:50%">
                                        <table class="m_6304820631880916025body" style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color:#f6f6f6;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:20px;padding-bottom:23px;padding-right:0;padding-left:0">
                                                        <table class="m_6304820631880916025body" style="border-collapse:collapse;font-family:Arial,sans-serif;min-height:120px;width:100%">
                                                            <tbody>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025title" style="font-family:Arial,sans-serif;font-size:16px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;text-transform:uppercase">{{ __('email.connect-social')}}
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025divider" style="height:22px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:2px;padding-bottom:2px;padding-right:0;padding-left:0">
                                                                        <span style="background-color:#999999;display:inline-block;font-family:Arial,sans-serif;height:2px;width:50px"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:25px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="tel:16478876869" style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none">Hotline: 1 (647) 887-6869</a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:25px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        {{ __('email.payment_etransfer')}}
                                                                        <br>
                                                                        <a href="mailto:ready.made.for.you@gmail.com" target="_blank">Email: ready.made.for.you@gmail.com </a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_6304820631880916025link" style="line-height:25px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        {{ __('email.attn')}}
                                                                    </td>
                                                                </tr>
                                                                <tr style="text-align:center">
                                                                    <td class="m_8621296705903697986link-connect" style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:4px;padding-bottom:0;padding-right:0;padding-left:0">
                                                                        <a href="#" style="border-style:none;color:#0388cd;text-decoration:none" target="_blank">
                                                                            <img src="{{ asset('images/home/adr-icon-facebook.jpg') }}" alt="" style="border-width:0px;vertical-align:bottom" class="CToWUd">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td bgcolor="#f7f7f7" style="margin:0;padding:15px 20px 20px;" width="640">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td class="pdlr0" valign="top" align="center" style="margin:0;padding:15px 20px 0;">
                    <div style="font-family:Arial, sans-serif;font-size:12px;line-height:14px;color:#000000;font-weight:bold;">iGreenLink</div>
                    <div style="font-family:Arial, sans-serif;font-size:11px;line-height:14px;color:#666666;margin-top:5px;">3080 Yonge St Toronto ON</div>
                    <div style="font-family:Arial, sans-serif;font-size:11px;line-height:14px;color:#666666;margin-top:5px;">Hotline: <a href="tel:18886196869" style="color:#666666;">1 (888) 619-6869</a>. Email: <a href="mailto:contact@igreencorp.com" target="_blank" style="color:#0000ee;text-decoration:underline;">contact@igreencorp.com</a></div>
                </td>
            </tr>
        </table>
    </td>
</tr>
