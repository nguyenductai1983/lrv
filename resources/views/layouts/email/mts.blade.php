<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
</head>

<body bgcolor="#cecccc" width="100%" style="margin: 0;">
    <div style="width:100%;background:#cecccc;">
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="640"
            style="margin:auto;width:640px;min-width:640px;max-width:640px;" class="email-container">

            @yield('content')
            <tr>
                <td id="m_6304820631880916025footer_support"
                    style="background-color:#ffffff;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                    <table class="m_6304820631880916025body"
                        style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
                        <tbody>
                            <tr>
                                <td
                                    style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">

                                    <table class="m_6304820631880916025col-md-6 m_6304820631880916025col-xs-12"
                                        cellspacing="0" cellpadding="0" align="left" style="border-collapse:collapse">
                                        <tbody>
                                            <tr>
                                                <td class="m_6304820631880916025fotter-connect m_6304820631880916025padding-5px-xs m_6304820631880916025padding-bt-10px-xs"
                                                    style="font-family:Arial,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0px;padding-bottom:20px;padding-right:20px;padding-left:10px;text-align:center;width:50%">
                                                    <table class="m_6304820631880916025body"
                                                        style="border-collapse:collapse;font-family:Arial,sans-serif;width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td
                                                                    style="background-color:#f6f6f6;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:20px;padding-bottom:23px;padding-right:0;padding-left:0">
                                                                    <table class="m_6304820631880916025body"
                                                                        style="border-collapse:collapse;font-family:Arial,sans-serif;min-height:120px;width:100%">
                                                                        <tbody>
                                                                            <tr style="text-align:center">
                                                                                <td class="m_6304820631880916025title"
                                                                                    style="font-family:Arial,sans-serif;font-size:16px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;text-transform:uppercase">
                                                                                    {{ __('email.connect-social')}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="text-align:center">
                                                                                <td class="m_6304820631880916025divider"
                                                                                    style="height:22px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:2px;padding-bottom:2px;padding-right:0;padding-left:0">
                                                                                    <span
                                                                                        style="background-color:#999999;display:inline-block;font-family:Arial,sans-serif;height:2px;width:50px"></span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="text-align:center">
                                                                                <td class="m_6304820631880916025link"
                                                                                    style="line-height:25px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                                    <a href="tel:16478876869"
                                                                                        style="border-style:none;color:#666666;font-family:Arial,sans-serif;font-size:11px;text-align:center;text-decoration:none">Hotline:
                                                                                        1 (647) 887-6869</a>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="text-align:center">
                                                                                <td class="m_6304820631880916025link"
                                                                                    style="line-height:25px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                                    {{ __('email.payment_etransfer')}}
                                                                                    <br>
                                                                                    <a href="mailto:ready.made.for.you@gmail.com"
                                                                                        target="_blank">Email:
                                                                                        ready.made.for.you@gmail.com
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="text-align:center">
                                                                                <td class="m_6304820631880916025link"
                                                                                    style="line-height:25px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">
                                                                                    {{ __('email.attn')}}
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
