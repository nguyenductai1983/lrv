<!-- footer -->
<tr>
    <td>
        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    <p class="f-fallback sub align-center">
                        3080 Yonge St Toronto ON M4N3N1<br>
                        CA (888) 619-6869 - VN 1900 055 509<br>
                        Email: contact@igreencorp.com
                    </p>
                    <p class="f-fallback sub align-center">
                       <a href="{{ URL::to('/') }}">{{ URL::to('/') }}</a>
                    </p>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- footer -->
