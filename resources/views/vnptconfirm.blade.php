@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/catalog.css') }}">
@endsection

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('label.confirm') }}
    </div>
    @if ($errors->any())
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
<form action="" method="post">
    @csrf
    <div class="form-group row">
        <label for="sMaDonHang" class="col-sm-2 col-form-label"> {{ __('label.code') }}</label>
        <div class="col-sm-4"><input type="text" name="sMaDonHang" class="form-control" id="sMaDonHang"
            placeholder="mã đơn hàng" autocomplete="off">
    </div>
</div>
    <div class="form-group row">
        <label for="sSoHieu" class="col-sm-2 col-form-label"> {{ __('label.post_code') }}</label>
        <div class="col-sm-4">
<input type="text" name="sSoHieu" class="form-control" id="sSoHieu"
placeholder="mã của buu điện" autocomplete="off">
 </div>
</div>
<div class="form-group row">
    <label for="sMaTrangThai" class="col-sm-2 col-form-label"> {{ __('label.status') }}</label>
    <div class="col-sm-4">
<input type="text" name="sMaTrangThai" class="form-control" value="400"
placeholder='{{ __('label.vnpt_400_code') }}' autocomplete="off"/>
  </div>
</div>
<div class="form-group row">
    <label for="sNgayTrangThai" class="col-sm-2 col-form-label"> {{ __('label.date') }} {{ __('label.status') }}</label>
    <div class="col-sm-4">
    <input name="sNgayTrangThai" id="sNgayTrangThai" value="{{ request()->query('from_date')}}" class="form-control"
    type="text" placeholder="{{ __('label.from_date') }}" autocomplete="off">
</div>
</div>
<div class="form-group row">
    <label for="sGhichu" class="col-sm-2 col-form-label">{{ __('label.note') }}</label>
    <div class="col-sm-4">
    <input type="text" name="sGhiChu" class="form-control" id="sGhichu" autocomplete="off"/></div>
</div>
<div class="form-group row">
    <div class="col-sm-2"></div>
    <div class="col-sm-4">
<input type="submit" class="btn btn-info ng-scope">
</div></div>
</form>
@if ($complete)
<h1>{{ $complete ?? '' }}</h1>
@endif
</div>


@endsection
<script>
    $(function () {
       $("#sNgayTrangThai").datepicker({dateFormat:"yy-mm-dd"});
    });
</script>

