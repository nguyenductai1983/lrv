@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('news.manage') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-1">
                <a href="{{ route('admin.news.create') }}" class="btn btn-success">
                    <i class="fa fa-plus-circle"></i> {{ __('label.save') }}
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="25%">{{ __('news.slug') }}</th>
                        <th width="25%">{{ __('news.link_to') }}</th>
                        <th width="10%">{{ __('news.order') }}</th>
                        <th width="20%">{{ __('news.created_at') }}</th>
                        <th width="15%">{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($pages->count() > 0)
                    @foreach($pages as $page)
                    <tr>
                        <td>{{ $page->slug }}</td>
                        <td>
                            {{ $page->link_to }}
                        </td>
                        <td>{{ $page->order }}</td>
                        <td>{{ $page->created_at }}</td>
                        <td>
                            <a href="{{ route('admin.news.edit', $page->id) }}" class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $page->id }}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="modal-delete-{{ $page->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.news.destroy', $page->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_delete_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $pages->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection
