@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('payment.edit') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('admin.payment-methods.update', $paymentMethod->id) }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.code') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        
                        @php $field = 'name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.name') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        
                        @php $field = 'description'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.description') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control"
                                       value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        
                        @php $field = 'display_order'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.display_order') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        @php $field = 'card_holder'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.card_holder') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        
                        @php $field = 'account_number'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.account_number') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        
                        @php $field = 'bank_branch'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.bank_branch') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $paymentMethod->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        
                        @php $field = 'type'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.type') }}
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="1" @if(1 == old($field, $paymentMethod->{$field})) selected="selected"@endif>
                                        {{ __('payment.type_global') }}
                                    </option>
                                    <option value="2" @if(2 == old($field, $paymentMethod->{$field})) selected="selected"@endif>
                                        {{ __('payment.type_local') }}
                                    </option>
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'image_file_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.' . $field) }}
                            </label>
                            <div class="col-sm-9">
                                @include('partials.form-controls.image', ['field' => $field, 'file' => $paymentMethod->{$field} ? $paymentMethod->file : null])
                            </div>
                        </div>
                    </div>

                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <a href="{{ route('admin.payment-methods.index') }}" class="btn btn-white">
                                    {{ __('label.cancel') }}
                                </a>
                                <button type="submit" class="btn btn-info disabled-submit">
                                    <i class="fa fa-check"></i> {{ __('label.update') }}
                                </button>
                            </div>
                        </div>
                </div>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection