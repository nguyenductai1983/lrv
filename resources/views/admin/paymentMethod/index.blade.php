@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('payment.method') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-1">
          <a href="{{ route('admin.payment-methods.create') }}" class="btn btn-success">
            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
          </a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
          <tr>
            <th>{{ __('payment.code') }}</th>
            <th>{{ __('payment.name') }}</th>
            <th>{{ __('payment.card_holder') }}</th>
            <th>{{ __('payment.account_number') }}</th>
            <th>{{ __('payment.bank_branch') }}</th>
            <th>{{ __('payment.type') }}</th>
            <th>{{ __('payment.image_file_id') }}</th>
            <th>{{ __('label.action') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($paymentMethods->count() > 0)
            @foreach($paymentMethods as $paymentMethod)
              <tr>
                <td>{{ $paymentMethod->code }}</td>
                <td>{{ $paymentMethod->name }}</td>
                <td>{{ $paymentMethod->card_holder }}</td>
                <td>{{ $paymentMethod->account_number }}</td>
                <td>{{ $paymentMethod->bank_branch }}</td>
                <td>{{ $paymentMethod->type_name }}</td>
                <td>
                  <img width="100px" src="{{ isset($paymentMethod->file->path) && !empty($paymentMethod->file->path) ? $paymentMethod->file->path : '' }}">
                </td>
                <td>
                  <a href="{{ route('admin.payment-methods.edit', $paymentMethod->id) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-edit"></i>
                  </a>
                  <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $paymentMethod->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                  <div class="modal fade" id="modal-delete-{{ $paymentMethod->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('admin.payment-methods.destroy', $paymentMethod->id) }}" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                          </div>
                          <div class="modal-body">
                            {!! __('label.confirm_delete_msg') !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                            <button type="submit" class="btn btn-danger disabled-submit">
                              <i class="fas fa-trash"></i> {{ __('label.delete') }}
                            </button>
                          </div>
                          {{ csrf_field() }}
                          {{ method_field('delete') }}
                        </form>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="4">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
