@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default transport-content" ng-app="QuoteApp" ng-controller="QuoteEditController">
        <div class="transport-content-title panel-heading">
            {{ __('quote.edit') }} <strong>{{ $order->code }}</strong>
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-edit" name="editForm" ng-submit="editQuote()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.sender.length || errors.receiver.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-body" style="margin: -10px -20px;">
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td width="50%" style="border-right: 1px solid #ccc;">
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.sender') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="20%">
                                                <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name" ng-disabled="sender.id">
                                            </td>
                                            <td width="15%" class="col-label">
                                                {{ __('customer.middle_name') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name" ng-disabled="sender.id">
                                            </td>
                                            <td width="10%" class="col-label">
                                                {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.email') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="sender.email" ng-model="sender.email" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_country') }}</option>
                                                    <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone" ng-disabled="sender.id">
                                            </td>
                                            <td class="col-label">{{ __('customer.cellphone') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="sender.cellphone" ng-model="sender.cellphone" ng-disabled="sender.id">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.qoute-type') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select ng-model="package_type" class="form-control" ng-disabled="sender.id">
                                                    <option value="1">{{ __('customer.qoute-shop') }}</option>
                                                    <option value="2">{{ __('customer.qoute-ship') }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="20%">
                                                <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" ng-disabled="sender.id">
                                            </td>
                                            <td width="15%" class="col-label">
                                                {{ __('receiver.middle_name') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" ng-disabled="sender.id">
                                            </td>
                                            <td width="10%" class="col-label">
                                                {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.address') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="receiver.address_1" ng-model="receiver.address_1" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_country') }}</option>
                                                    <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="updateContainers()" ng-disabled="sender.id">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.post_code" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone"  ng-disabled="sender.id">
                                            </td>

                                            <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" ng-disabled="sender.id">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-color panel-gray panel-border" style="padding-bottom: 0;margin-bottom: -2px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-cubes"></i> {{ __('label.goods') }}</h3>
                    </div>
                </div>

                <div class="transport-content-package">
                    <div style="padding: 15px;border: 1px solid #ccc !important;">
                        <table class="table table-hover table-bordered table-container" style="max-width: 100%;width: 100%;">
                            <thead>
                            <tr>
                                <th width="30" class="text-center">#</th>
                                <th width="200">{{ __('product.url') }}</th>
                                <th width="100">{{ __('product.name') }}</th>
                                <th width="30" class="text-center">{{ __('label.quantity') }}</th>
                                <th width="30" class="text-center">{{ __('label.price') }}</th>
                                <th width="30" class="text-center">{{ __('label.coupon') }}</th>
                                <th width="30" class="text-center">{{ __('label.shop_fee') }}</th>
                                <th width="30" class="text-center">{{ __('label.shipping_fee') }}</th>
                                <th width="30" class="text-center">{{ __('label.delivery_fee') }}</th>
                                <th width="150">{{ __('label.note') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="product in quote.containers">
                                <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                <td class="col-middle">
                                    <input type="text" class="form-control input-center" ng-model="product.url">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control input-center" ng-model="product.name">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control text-center" ng-model="product.quantity">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control text-center" ng-model="product.price" ng-change="updateContainers()">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control text-center" ng-model="product.coupon">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control text-center" ng-model="product.shop_fee" ng-change="updateContainers()">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control text-center" ng-model="product.shipping_fee" ng-change="updateContainers()">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control text-center" ng-model="product.delivery_fee" ng-change="updateContainers()">
                                </td>
                                <td class="col-middle">
                                    <input type="text" class="form-control" ng-model="product.note">
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="text-right">{{ __('label.currency_pay') }}</td>
                                <td class="text-left">
                                    <strong class="ng-binding">CAD</strong>
                                </td>
                                <td class="text-right">{{ __('label.total_goods') }}</td>
                                <td class="text-left">
                                    <strong class="ng-binding" ng-bind="quote.total_goods.toFixed(2)"></strong>
                                </td>
                                <td class="text-right">{{ __('label.shop_fee') }}</td>
                                <td class="text-left">
                                    <strong class="ng-binding" ng-bind="quote.total_shop_fee.toFixed(2)"></strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('label.shipping_fee') }}</td>
                                <td class="text-left">
                                    <strong class="ng-binding" ng-bind="quote.total_shipping_fee.toFixed(2)"></strong>
                                </td>
                                <td class="text-right">{{ __('label.delivery_fee') }}</td>
                                <td class="text-left">
                                    <strong class="ng-binding" ng-bind="quote.total_delivery_fee.toFixed(2)"></strong>
                                </td>
                                <td class="text-right">{{ __('label.total_amount') }}</td>
                                <td class="text-left">
                                    <strong class="ng-binding" ng-bind="quote.total_final.toFixed(2)"></strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.date_pay') }}:</td>
                                <td class="col-middle text-left">
                                    <div class="input-group date-picker" data-change-year="true"
                                         data-change-month="true"
                                         data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                        <input type="text" class="form-control" ng-model="quote.last_date_pay">

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-white">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-middle text-right">{{ __('label.pay') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control text-right" ng-model="quote.last_paid_amount">
                                </td>
                                <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                                <td class="text-left">
                                    <select name="currency" id="payMethod" class="form-control" ng-model="quote.pay_method">
                                        <option ng-repeat="method in methods"
                                                ng-value="method.code">@{{ method.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.paid') }}:</td>
                                <td class="text-left" style="padding-top:18px">
                                    <strong class="ng-binding" ng-bind="quote.total_paid_amount"></strong>
                                </td>
                                <td class="col-middle text-right">{{ __('label.last_paid_time') }}:</td>
                                <td class="text-left" style="padding-top:18px">
                                    <strong class="ng-binding" ng-bind="quote.last_payment_at"></strong>
                                </td>
                                <td class="col-middle text-right">{{ __('label.payment_status') }}:</td>
                                <td class="text-left" style="padding-top:18px">
                                    <strong class="ng-binding" ng-bind="quote.payment_status"></strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"></td>
                                <td class="col-middle text-right">{{ __('label.tracking_code') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control" ng-model="quote.user_note">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-right" style="padding-top: 30px;">
                    <a href="{{ route('quote.admin.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                    <button type="submit" class="btn btn-danger" ng-if="bntCancel" ng-click="cancelQuote()" ng-disabled="submittedCancel">
                        <i class="fas fa-window-close"></i> {{ __('label.cancel') }} <i class="fa fa-refresh fa-spin" ng-if="submittedCancel"></i>
                    </button>
                    <button type="submit" class="btn btn-info" ng-if="bntConfirm" ng-click="editQuote()" ng-disabled="submittedConfirm">
                        <i class="fa fa-check"></i> {{ __('label.confirm') }} <i class="fa fa-refresh fa-spin" ng-if="submittedConfirm"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
    <script src="{{ asset('js/admin/app/quote-edit.js?t=' . File::lastModified(public_path('js/admin/app/quote-edit.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-edit').scope();

            scope.id = {!! request()->route('id') !!};

            scope.getQuote();

            if (!scope.$$phase) {
                scope.$apply();
            }
        });
    </script>
@endsection
