@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('receipt.create')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.receipt-extra.create') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    @if($users->count() > 1)
                        <div class="form-group">
                            <select name="user_id" class="form-control">
                                <option value="">{{ __('receipt.option_agency') }}</option>

                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}" @if($user->id == request()->query('user_id')) selected="selected"@endif>{{ $user->code }}</option>
                                    @endforeach

                            </select>
                        </div>
                        @else
                        <div class="form-group">
                        <select name="user_id" class="form-control">
                            <option value="{{ $users[0]->id }}">{{ $users[0]->code }}</option>
                        </select>
                    </div>
                        @endif
                    <div class="form-group">
                        <button class="btn btn-white btn-single" type="submit">
                            <i class="fa fa-search"></i> {{ __('label.find')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <form id="receipt-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="select-all" checked="">
                            </th>
                            <th>{{ __('label.no')}}</th>
                            <th>{{ __('label.code')}}</th>
                            <th>{{ __('label.order_code')}}</th>
                            <th>{{ __('label.create')}}</th>
                            <th>{{ __('receipt.amount') }} (CAD)</th>
                            <th>{{ __('label.status')}}</th>
                        </tr>
                    </thead>
                    <tbody id="receipt-package-items">
                        @if($extra->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($extra as $extraitem)
                        <tr id="receipt-package-create-{{ $extraitem->id }}">
                            <td>
                                <input type="checkbox" name="id" value="{{ $extraitem->id }}" checked=""></td>
                                <td>{{ $no  }}</td>
                                <td>{{ $extraitem->code }}</td>
                                <td>{{ $extraitem->order->code }}</td>
                                <td>{{ $extraitem->created_at }}</td>
                            <td class="text-right">{{ number_format($extraitem->amount, 2) }}</td>
                            <td>
                                <span class="{{ $extraitem->status_label }}">{{ $extraitem->status_name }}</span>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr id="empty-item">
                            <td colspan="7">{{ __('label.no_records')}}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $extra->appends(request()->query())->links() }}
            </div>
        </form>
        <div class="text-right">
            <a href="{{ route('admin.receipt-extra.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <button type="button" class="btn btn-info" onclick="receipt.addExtraItems('{{ route('admin.receipt-extra.index') }}');">
                <i class="fa fa-check"></i> {{ __('label.add')}}
            </button>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/receipt.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#receipt-package-items input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#receipt-package-items input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
