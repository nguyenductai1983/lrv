@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('receipt.view')}}: {{ $voucher->code }}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>{{ __('label.code')}}</th>
                        <th>{{ __('label.order_code')}}</th>
                        <th width="50%">{{ __('label.content') }}</th>
                        <th>{{ __('receipt.amount') }} (CAD)</th>
                        <th>{{ __('label.status')}}</th>
                        <th>{{ __('label.action')}}</th>
                    </tr>
                </thead>
                <tbody id="receipt-package-items">
                    @if($extras->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($extras as $extra)
                    <tr id="receipt-package-create-{{ $extra->id }}">
                        <td>{{ $no }}</td>
                        <td>{{ $extra->code }}</td>
                        <td>{{ $extra->order->code }}</td>
                        <td>{{ $extra->reason }}</td>
                        <td class="text-right">{{ number_format($extra->amount, 2) }}</td>
                        <td>
                            <span class="{{ $extra->status_label }}">{{ $extra->status_name }}</span>
                        </td>

                        <td>
                            <button type="button" onclick="receipt.removeExtraItem({{ $extra->id }})" class="btn btn-xs btn-danger"
                                {{ ( $extra->status==4) ? 'disabled' : '' }} >
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr id="empty-item">
                        <td colspan="7">{{ __('label.no_records')}}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="text-right">
            <a href="{{ route('admin.receipt-extra.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <a href="{{ route('admin.receipt-extra.excel-item', $voucher->id) }}" class="btn btn-white">
                <i class="fa fa-file"></i> {{ __('label.export_excel')}}
            </a>
            <a href="{{ route('admin.receipt-extra.print-item', $voucher->id) }}" target="_blank" class="btn btn-white">
                <i class="fa fa-print "></i>  {{ __('label.print_bill')}}
            </a>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
language = JSON.parse(language);
</script>
<script src="/js/receipt.js"></script>
@endsection
