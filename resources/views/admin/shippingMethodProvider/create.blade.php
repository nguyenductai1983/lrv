@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('shipping.create') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('admin.shipping-method-providers.store') }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                      @php $field = 'name'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.name') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-6">
                          <input type="text" name="{{ $field }}" class="form-control">
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      @php $field = 'display_order'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.display_order') }}
                        </label>

                        <div class="col-sm-6">
                          <input type="text" name="{{ $field }}" class="form-control">
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      @php $field = 'shipping_provider_id'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.provider') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-6">
                          <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                            <option value="">{{ __('shipping.provider') }}</option>
                            @foreach ($shipping_providers as $shipping_provider)
                              <option value="{{ $shipping_provider->id }}">
                                {{ $shipping_provider->name }}
                              </option>
                            @endforeach
                          </select>
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      @php $field = 'shipping_method_id'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.method') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-6">
                          <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                            <option value="">{{ __('shipping.method') }}</option>
                            @foreach ($shipping_methods as $shipping_method)
                              <option value="{{ $shipping_method->id }}">
                                {{ $shipping_method->name }}
                              </option>
                            @endforeach
                          </select>
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      @php $field = 'min_weight'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.min_weight') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-6">
                          <input type="text" name="{{ $field }}" class="form-control">
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      @php $field = 'min_fee'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.min_fee') }}
                        </label>

                        <div class="col-sm-6">
                          <input type="text" name="{{ $field }}" class="form-control">
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                  <div class="row">
                    <div class="col-sm-6">
                      @php $field = 'is_active'; @endphp
                      <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                          {{ __('shipping.is_active') }}
                        </label>
                        <div class="col-sm-6">
                          <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                            <option value="1">
                              {{ __('shipping.is_active_true') }}
                            </option>
                            <option value="0">
                              {{ __('shipping.is_active_fail') }}
                            </option>
                          </select>
                          @if ($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'image_file_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('payment.' . $field) }}
                            </label>
                            <div class="col-sm-9">
                                @include('partials.form-controls.image', ['field' => $field, 'file' => null])
                            </div>
                        </div>
                      </div>

                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                        <div class="col-sm-12 text-right">
                          <div class="form-group">
                              <div class="col-sm-12 text-right">
                                  <a href="{{ route('admin.shipping-method-providers.index') }}" class="btn btn-white">
                                      {{ __('label.cancel') }}
                                  </a>
                                  <button type="submit" class="btn btn-info disabled-submit">
                                      <i class="fa fa-check"></i> {{ __('label.save') }}
                                  </button>
                              </div>
                          </div>
                          </div>
                      </div>
              </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection
