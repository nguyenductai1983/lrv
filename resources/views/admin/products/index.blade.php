@extends('layouts.admin.index', ['menu' => $data['menu']])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('product.manage') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-2">
          <a href="{{ route('admin.products.create') }}" class="btn btn-success">
            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
          </a>
        </div>
        <div class="col-sm-10">
          <form action="{{ route('admin.products.index') }}" method="get" class="form-inline">
            <div class="form-group">
              <select name="product_group" class="form-control">
                <option value="">{{ __('product.product_group') }}</option>
                @foreach($data['groups'] as $group)
                  <option value="{{ $group->id }}" @if(request()->query('product_group') == $group->id) selected="selected"@endif>
                    {{ $group->code . '-' . $group->name }}
                  </option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-white btn-single">
                <i class="fa fa-filter"></i> {{ __('label.filter') }}
              </button>
              <a href="{{ route('admin.products.index') }}" class="btn btn-white btn-single">
                {{ __('label.clear') }}
              </a>
            </div>
          </form>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-hover table-bordered" style="min-width: 1350px;">
          <thead>
          <tr>
            <th width="100">{{ __('label.action') }}</th>
            <th width="100">{{ __('productGroup.code') }}</th>
            <th width="140">{{ __('product.code') }}</th>
            <th width="160">{{ __('product.name') }}</th>
            <th width="70">{{ __('product.stock') }}</th>
            <th width="70">{{ __('product.unit') }}</th>
            <th class="text-right" width="60">{{ __('product.cost') }}</th>
            <th class="text-right" width="100">{{ __('product.price') }}</th>
            <th class="text-right" width="80">{{ __('product.sale_price') }}</th>
            <th class="text-right" width="80">{{ __('product.commission_amount') }}</th>
            <th class="text-right" width="80">{{ __('product.commission_percent') }}</th>
            <th class="text-right" width="80">{{ __('product.commission_quota_amount') }}</th>
            <th class="text-right" width="80">{{ __('product.commission_quota_percent') }}</th>
            <th class="text-right" width="80">{{ __('product.quota_discount') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($data['products']->count() > 0)
            @foreach($data['products'] as $product)
              <tr class="{{ !$product->status ? 'data-hide' : '' }}">
                <td>
                    <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-xs btn-warning">
                      <i class="fa fa-edit"></i>
                    </a>
                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $product->id }}">
                      <i class="fas fa-trash"></i>
                    </button>
                    <div class="modal fade" id="modal-delete-{{ $product->id }}">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <form action="{{ route('admin.products.destroy', $product->id) }}" method="post">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                            </div>
                            <div class="modal-body">
                              {!! __('label.confirm_delete_msg') !!}
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                              <button type="submit" class="btn btn-danger disabled-submit">
                                <i class="fas fa-trash"></i> {{ __('label.delete') }}
                              </button>
                            </div>
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                <td>{{ $product->group_code }}</td>
                <td>{{ $product->code }}</td>
                <td>{{ $product->product_name }}</td>
                <td>{{ isset($product->messureunits->name) ? $product->messureunits->name : '' }}</td>
                <td class="text-right">{{ $product->stock ? number_format($product->stock, 0) : 0 }}</td>
                <td class="text-right">{{ $product->cost ? number_format($product->cost, 2) : '' }}</td>
                <td class="text-right">{{ $product->price ? number_format($product->price, 2) : '' }}</td>
                <td class="text-right">{{ $product->sale_price ? number_format($product->sale_price, 2) : '' }}</td>
                <td class="text-right">{{ $product->commission_amount ? number_format($product->commission_amount, 2) : 0 }}</td>
                <td class="text-right">{{ $product->commission_percent ? number_format($product->commission_percent, 2) : 0 }}</td>
                <td class="text-right">{{ $product->commission_quota_amount ? number_format($product->commission_quota_amount, 2) : 0 }}</td>
                <td class="text-right">{{ $product->commission_quota_percent ? number_format($product->commission_quota_percent, 2) : 0 }}</td>
                <td class="text-right">{{ $product->quota_discount ? number_format($product->quota_discount, 2) : 0 }}</td>


              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="9">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
