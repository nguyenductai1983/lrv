@extends('layouts.admin.index', ['menu' => $data['menu']])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('product.edit') }}
    </div>
    <div class="panel-body">
      @php $product = $data['product'] @endphp
      <form action="{{ route('admin.products.update', $product->id) }}" method="post" role="form"
        class="form-horizontal">
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'code'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.code') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->code) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'barcode'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.barcode') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->barcode) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            @php $field = 'name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.name') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->name) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'messure_unit'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.unit') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  @foreach ($data['messure_units'] as $messure_unit)
                    <option value="{{ $messure_unit->id }}" @if ($messure_unit->id == old($field, $product->messure_unit_id)) selected="selected" @endif>
                      {{ $messure_unit->code }}
                    </option>
                  @endforeach
                </select>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            @php $field = 'product_group'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.product_group') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  @foreach ($data['groups'] as $group)
                    <option value="{{ $group->id }}" @if ($group->id == old($field, $product->product_group_id)) selected="selected" @endif>
                      {{ $group->code . '-' . $group->name }}
                    </option>
                  @endforeach
                </select>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'currency'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.currency') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  @foreach ($data['currencies'] as $currency)
                    <option value="{{ $currency->id }}" @if ($currency->id == old($field, $product->currency_id)) selected="selected" @endif>
                      {{ $currency->code }}
                    </option>
                  @endforeach
                </select>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            @php $field = 'weight_unit'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.weight_unit') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  @foreach ($data['weight_units'] as $weight_unit)
                    <option value="{{ $weight_unit->id }}" @if ($weight_unit->id == old($field, $product->weight_unit_id)) selected="selected" @endif>
                      {{ $weight_unit->code }}
                    </option>
                  @endforeach
                </select>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            @php $field = 'weight'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.weight') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->weight) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            @php $field = 'sale_price'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.sale_price') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->sale_price) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'price'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.price') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->price) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'commission_amount'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.commission_amount') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->commission_amount) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'commission_percent'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.commission_percent') }}(%)<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->commission_percent) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            @php $field = 'commission_quota_amount'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.commission_quota_amount') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->commission_quota_amount) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'commission_quota_percent'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.commission_quota_percent') }}(%)<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->commission_quota_percent) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            @php $field = 'quota_discount'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.quota_discount') }}(%)<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control"
                  value="{{ old($field, $product->quota_discount) }}">
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'danger'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.danger') }}
              </label>
              <div class="col-sm-8">
                <div>
                  <select name="{{ $field }}" class="form-control">
                    @foreach ($data['dangers'] as $key => $value)
                      <option value={{ $key }} {{ $product->danger === $key ? 'selected' : '' }}>
                        {{ $value }}</option>
                    @endforeach
                  </select>
                </div>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'by_weight'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.by_weight') }}
              </label>
              <div class="col-sm-8">
                <div class="form-control">
                  <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1" @if (old($field, !$errors->any() ? $product->by_weight : null)) checked="checked" @endif>
                </div>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'status'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('label.status') }}
              </label>
              <div class="col-sm-8">
                <div class="form-control">
                  <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1" @if (old($field, !$errors->any() ? $product->status : null)) checked="checked" @endif>
                </div>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'display_order'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.display_order') }}
              </label>
              <div class="col-sm-8">
                <div>
                  <input type="input" class="form-control" name="{{ $field }}"
                    value={{ $product->display_order }}>
                </div>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'separate'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.separate') }}
              </label>
              <div class="col-sm-8">
                <div class="form-control">
                  <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1" @if (old($field, !$errors->any() ? $product->separate : null)) checked="checked" @endif>
                </div>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        {{-- bat dau 07-07-2021 --}}
        <div class="row">
            <div class="col-sm-6">
              @php $field = 'cost'; @endphp
              <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                <label for="{{ $field }}" class="control-label col-sm-4">
                  {{ __('product.cost') }}
                </label>
                <div class="col-sm-8">
                  <input type="text" name="{{ $field }}" class="form-control"
                    value="{{ old($field, $product->cost) }}">
                  @if ($errors->has($field))
                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
                @php $field = 'limit_pcs'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                  <label for="{{ $field }}" class="control-label col-sm-4">
                    {{ __('product.limit_pcs') }}
                  </label>
                  <div class="col-sm-8">
                    <input type="text" name="{{ $field }}" class="form-control"
                      value="{{ old($field, $product->limit_pcs) }}">
                    @if ($errors->has($field))
                      <span class="validate-has-error">{{ $errors->first($field) }}</span>
                    @endif
                  </div>
                </div>
              </div>
          </div>
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'thumbnail'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('product.thumbnail') }}
              </label>
              <div class="col-sm-8">
                @include('partials.form-controls.image', ['field' => 'thumbnail', 'file' => $product->thumbnails])
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
              @php $field = 'description'; @endphp
              <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                <label for="{{ $field }}" class="control-label col-sm-2">
                  {{ __('product.description') }}
                </label>
                <div class="col-sm-10">
                  <textarea name="{{ $field }}" cols="30" rows="3"
                    class="form-control">{{ old($field, $product->description) }}</textarea>
                  @if ($errors->has($field))
                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
        <div class="row">
          <div class="col-sm-12">
            @php $field = 'content'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-2">
                {{ __('product.content') }}
              </label>
              <div class="col-sm-10">
                <textarea name="content" class="form-control ckeditor" cols="30"
                  rows="20">{{ old($field, $product->content) }}</textarea>
                @if ($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
{{-- het 07-07-2021 --}}
        <div class="form-group">
          <div class="col-md-12 text-right">
            <a href="{{ route('admin.products.index') }}" class="btn btn-white">
              {{ __('label.cancel') }}
            </a>
            <button type="submit" class="btn btn-info disabled-submit">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button>
          </div>
        </div>
        {{ csrf_field() }}
        {{ method_field('put') }}
      </form>
    </div>
  </div>
@endsection
@section('footer')
  <script src="{{ asset('components\ckeditor\ckeditor.js') }}"></script>
@endsection
