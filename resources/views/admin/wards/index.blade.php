@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('ward.manage') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-2">
                <a href="{{ route('admin.wards.create') }}" class="btn btn-success">
                  <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                </a>
              </div>
        </div>
      <div class="row form-group">
        <div class="col-sm-12">
          <form action="{{ route('admin.wards.index') }}" method="get" class="form-inline" novalidate>
            @php $field = 'country_id'; @endphp
            <div class="form-group">
              <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                <option value="">{{ __('city.select_country') }}</option>
                @foreach($countries as $country)
                  <option value="{{ $country->id }}" @if($country->id == request()->query($field)) selected="selected"@endif>
                    {{ $country->code . '-' . $country->name }}
                  </option>
                @endforeach
              </select>
            </div>
            @php $field = 'province_id'; @endphp
            <div class="form-group">
              <select name="{{ $field }}" id="{{ $field }}" class="form-control" @if(request()->query('country_id')) disabled="disabled"@endif>
                <option value="">{{ __('city.select_province') }}</option>
              </select>
            </div>
            @php $field = 'city_id'; @endphp
            <div class="form-group">
              <select name="{{ $field }}" id="{{ $field }}" class="form-control" @if(request()->query('province_id')) disabled="disabled"@endif>
              <option value="">{{ __('city.select_city') }}</option>
              </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-white btn-single">
                <i class="fa fa-search"></i> {{ __('label.filter') }}
              </button>
              <a href="{{ route('admin.wards.index') }}" class="btn btn-white btn-single">
                {{ __('label.clear') }}
              </a>
            </div>
          </form>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
          <tr>
            <th>{{ __('city.country_id') }}</th>
            <th>{{ __('city.province_id') }}</th>
            <th>{{ __('city.name') }}</th>
            <th>{{ __('city.ward_name') }}</th>
            <th>{{ __('label.action') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($wards->count() > 0)
            @foreach($wards as $ward)
              <tr>
                <td>{{ $ward->country_name }}</td>
                <td>{{ $ward->province_name }}</td>
                <td>{{ $ward->city_name }}</td>
                <td>{{ $ward->name }}</td>
                <td>
                  <a href="{{ route('admin.wards.edit', $ward->id) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-edit"></i>
                  </a>
                  <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $ward->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                  <div class="modal fade" id="modal-delete-{{ $ward->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('admin.wards.destroy', $ward->id) }}" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                          </div>
                          <div class="modal-body">
                            {!! __('label.confirm_delete_msg') !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                            <button type="submit" class="btn btn-danger disabled-submit">
                              <i class="fas fa-trash"></i> {{ __('label.delete') }}
                            </button>
                          </div>
                          {{ csrf_field() }}
                          {{ method_field('delete') }}
                        </form>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="5">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
      <div class="paginate-single">
        {{ $wards->appends(request()->query())->links() }}
      </div>
    </div>
  </div>
@endsection

@section('footer')
  <script>
      $(function () {
          var countryEle = $('#country_id');
          var provinceEle = $('#province_id');
          var citieEle = $('#city_id');
          var setProvinces = function () {
              var countryId = countryEle.val();
              if (!countryId) {
                  provinceEle.find('option').each(function (key, option) {
                      if ($(option).val()) {
                          $(option).remove();
                      }
                  });
                  return false;
              }

              provinceEle.prop('disabled', true);

              var data = {
                  country_id: countryId
              };

              $.ajax({
                  method  : 'GET',
                  url     : '{{ route('admin.provinces.index') }}?' + toUrlEncodedString(data),
                  dataType: 'json'
              }).done(function (data) {
                  provinceEle.find('option').each(function (key, option) {
                      if ($(option).val()) {
                          $(option).remove();
                      }
                  });
                  data.provinces.forEach(function (province) {
                      var option = '<option value="' + province.id + '"';
                      if (province.id === parseInt('{{ request()->query('province_id') }}')) {
                          option += ' selected="selected"';
                          tam =province.id;
                      }
                      option += ' >' + province.name +'</option>';
                      provinceEle.append(option);
                  });
                  provinceEle.prop('disabled', false);
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });

              return true;
          };
          var setCities = function () {
              var countryId = countryEle.val();
              var provinceId = provinceEle.val();
              var citieEle = $('#city_id');

              if (!countryId) {
                  provinceEle.find('option').each(function (key, option) {
                      if ($(option).val()) {
                          $(option).remove();
                      }
                  });
                  return false;
              }

              citieEle.prop('disabled', true);

              var data = {
                  country_id: countryId,
                  province_id:provinceId
              };

              $.ajax({
                  method  : 'GET',
                  url     : '{{ route('admin.cities.index') }}?' + toUrlEncodedString(data),
                  dataType: 'json'
              }).done(function (data) {
                citieEle.find('option').each(function (key, option) {
                      if ($(option).val()) {
                          $(option).remove();
                      }
                  });
                  data.cities.forEach(function (citie) {
                      var option = '<option value="' + citie.id + '"';
                      if (citie.id === parseInt('{{ request()->query('city_id') }}')) {
                          option += ' selected="selected"';
                      }
                      option += ' >' + citie.name + '</option>';
                      citieEle.append(option);
                  });
                  citieEle.prop('disabled', false);
              }).fail(function (jqXHRs) {
                  console.log(jqXHRs);
              });

              return true;
          };
          if (countryEle.val()) {
              setProvinces();
              }
          countryEle.change(function () {
              setProvinces();
          });
          provinceEle.change(function () {
            setCities();
          });
      });
  </script>
@endsection
