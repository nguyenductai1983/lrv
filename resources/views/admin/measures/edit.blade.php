@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('messure.edit') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('admin.measures.update', $measures->id) }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('messure.code') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $measures->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('messure.name') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $measures->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        @php $field = 'display_order'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('messure.display_order') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $measures->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'is_active'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('messure.is_active') }}
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="1" @if(1 == old($field, $measures->{$field})) selected="selected"@endif>
                                        {{ __('messure.is_active_true') }}
                                    </option>
                                    <option value="2" @if(2 == old($field, $measures->{$field})) selected="selected"@endif>
                                        {{ __('messure.is_active_fail') }}
                                    </option>
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <a href="{{ route('admin.measures.index') }}" class="btn btn-white">
                                {{ __('label.cancel') }}
                            </a>
                            <button type="submit" class="btn btn-info disabled-submit">
                                <i class="fa fa-check"></i> {{ __('label.update') }}
                            </button>
                        </div>
                    </div>
                </div>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection