@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('shipping.create') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('admin.shipping-providers.store') }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('shipping.name') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'description'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('shipping.description') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control"
                                       value="{{ old($field) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        @php $field = 'display_order'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('shipping.display_order') }}
                            </label>

                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <a href="{{ route('admin.shipping-providers.index') }}" class="btn btn-white">
                                {{ __('label.cancel') }}
                            </a>
                            <button type="submit" class="btn btn-info disabled-submit">
                                <i class="fa fa-check"></i> {{ __('label.save') }}
                            </button>
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection
