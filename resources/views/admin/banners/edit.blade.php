@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('banner.edit') }}
    </div>
    <div class="panel-body">
        <form action="{{ route('admin.banners.update', $banner->id ) }}" method="post" enctype="multipart/form-data" role="form" class="form-horizontal">
            <div class="row">
                <div class="col-sm-6">
                    @php $field = 'name'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('banner.name') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $banner->{$field}) }}">
                            @if($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    @php $field = 'path'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-4">
                            {{ __('banner.thumnail') }}
                        </label>
                        <div class="col-sm-8">
                            <div class="img-control1">
                                <div class="img-preview" @if(!old($field, $banner->path)) style="display: none;"@endif>
                                    <h4 class="remove"><i class="fa fa-times-circle"></i></h4>
                                    <img id="img-preview-content" src="{{ old($field, $banner->path) }}" alt="" class="img-thumbnail">
                                </div>
                                <input type="file" accept="image/*" name="file" class="form-control">
                                <input type="hidden" name="{{ $field }}" value="{{ old($field, $banner->path) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    @php $field = 'display_order'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('banner.order') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $banner->{$field}) }}">
                            @if($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    @php $field = 'valid'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-4">
                            {{ __('banner.status') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-8">
                            <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="0" @if(0 == old($field, $banner->{$field})) selected="selected"@endif>{{ __('banner.active') }}</option>
                                <option value="1" @if(1 == old($field, $banner->{$field})) selected="selected"@endif>{{ __('banner.deactive') }}</option>
                            </select>
                            @if($errors->has($field))
                            <span>{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

<div class="row">
                <div class="col-sm-6">
                    @php $field = 'url'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('banner.url') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $banner->{$field}) }}">
                            @if($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            <div class="col-sm-6">
                    @php $field = 'lang'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-4">
                            {{ __('banner.lang') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-8">
                            <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="vi" @if("vi" == old($field, $banner->{$field})) selected="selected"@endif>Tiếng Việt</option>
                                <option value="en" @if("en" == old($field, $banner->{$field})) selected="selected"@endif>English</option>
                            </select>
                            @if($errors->has($field))
                            <span>{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
<div class="row">
                <div class="col-sm-6">
                    @php $field = 'caption'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('banner.caption') }}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $banner->{$field}) }}">
                            @if($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            <div class="col-sm-6">
                    @php $field = 'detail'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-4">
                            {{ __('banner.detail') }}
                        </label>
                        <div class="col-sm-8">
                            <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $banner->{$field}) }}">
                            @if($errors->has($field))
                            <span class="validate-has-error">{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>


            <div class="form-group">
                <div class="col-md-12 text-right">
                    <a href="{{ route('admin.banners.index') }}" class="btn btn-white">
                        {{ __('label.cancel') }}
                    </a>
                    <button type="submit" class="btn btn-info disabled-submit">
                        <i class="fa fa-check"></i> {{ __('label.update') }}
                    </button>
                </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('put') }}
        </form>
    </div>
</div>
@endsection