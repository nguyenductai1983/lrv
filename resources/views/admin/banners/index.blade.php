@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('banner.manage') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-1">
                <a href="{{ route('admin.banners.create') }}" class="btn btn-success">
                    <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">{{ __('banner.name') }}</th>
                        <th width="60%">{{ __('banner.thumnail') }}</th>
                        <th width="10%">{{ __('banner.status') }}</th>
                        <th width="10%">{{ __('banner.order') }}</th>
                        <th width="10%">{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($banners->count() > 0)
                    @foreach($banners as $banner)
                    <tr>
                        <td>{{ $banner->name }}</td>
                        <td>
                            <img width="100%" src="{{ $banner->path }}">
                        </td>
                        <td>{{ $banner->valid_name }}</td>
                        <td>{{ $banner->display_order }}</td>
                        <td>
                            <a href="{{ route('admin.banners.edit', $banner->id) }}" class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $banner->id }}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="modal-delete-{{ $banner->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.banners.destroy', $banner->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_delete_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $banners->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection
