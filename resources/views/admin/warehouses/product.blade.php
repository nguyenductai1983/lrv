@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('warehouse.manage') }}
      @if($warehouse->count() > 0)
       {{ $warehouse->name }} ({{ $warehouse->id }})
      @endif
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('yhl.instock.warehousecreate',$warehouse->id) }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                    </a>
                </div>
            </div>
        </div>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
          <tr>
            <th>{{ __('label.id') }}</th>
            <th>{{ __('label.code') }}</th>
            <th>{{ __('label.name') }}</th>
            <th>{{ __('label.amount') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($list_products->count() > 0)
            @foreach($list_products as $product)
              <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->code }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->stockinwarehouse($warehouse->id) }}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="4">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
      <div>
        <a href="{{ route('admin.warehouses.index') }}" class="btn btn-white">
            <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
        </a>
      </div>
    </div>
  </div>
@endsection
