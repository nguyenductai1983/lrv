@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('warehouse.edit') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('admin.warehouses.update', $warehouses->id) }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.code') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.name') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'description'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.description') }}
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'address'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.address') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'telephone'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.telephone') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'email'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.email') }}
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'contact_person'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.contact_person') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'post_code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.post_code') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $warehouses->{$field}) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        {{-- update 21-03-06 --}}
                        @php $field = 'stockin'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.stockin') }}
                        </label>
                        <div class="col-sm-9">
                    <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1"
                    @if($warehouses->{$field}) checked="checked"@endif>
                        </div>
                        </div>
                        {{-- update 21-03-06 --}}
                    </div>

                    <div class="col-sm-6">
                        @php $field = 'country_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="">{{ __('customer.select_country') }}</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" @if($country->id == old($field, $warehouses->{$field})) selected="selected"@endif>{{ $country->code . '-' . $country->name }}</option>
                                @endforeach
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'province_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                            <option value="">{{ __('customer.select_province') }}</option>
                            </select>
                            @if($errors->has($field))
                            <span>{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                        </div>
                        @php $field = 'city_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                            <option value="">{{ __('customer.select_city') }}</option>
                            </select>
                            @if($errors->has($field))
                            <span>{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                        </div>
                        @php $field = 'ward_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                            <option value="">{{ __('customer.select_ward') }}</option>
                            </select>
                            @if($errors->has($field))
                            <span>{{ $errors->first($field) }}</span>
                            @endif
                        </div>
                        </div>
                        @php $field = 'is_default'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('warehouse.is_default') }}
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="2" @if(2 == old($field, $warehouses->{$field})) selected="selected"@endif>
                                        {{ __('label.no') }}
                                    </option>
                                    <option value="1" @if(1 == old($field, $warehouses->{$field})) selected="selected"@endif>
                                        {{ __('label.yes') }}
                                    </option>
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'type'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.type') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="1" @if(1 == old($field, $warehouses->{$field})) selected="selected"@endif>
                                            {{ __('warehouse.type_global') }}
                                    </option>
                                    <option value="2" @if(2 == old($field, $warehouses->{$field})) selected="selected"@endif>
                                            {{ __('warehouse.type_local') }}
                                    </option>
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'currency_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.currency_unit') }}<i class="text-danger">*</i>
                            </label>

                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    @foreach($currencies as $currency)
                                    <option value="{{ $currency->id }}" @if($currency->id == old($field, $warehouses->{$field})) selected="selected"@endif>
                                            {{ $currency->code }}
                                    </option>
                                    @endforeach
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'weight_unit_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.weight_unit') }}<i class="text-danger">*</i>
                            </label>

                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    @foreach($weight_units as $weight_unit)
                                    <option value="{{ $weight_unit->id }}" @if($weight_unit->id == old($field, $warehouses->{$field})) selected="selected"@endif>
                                        {{ $weight_unit->code }}
                                    </option>
                                    @endforeach
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'dimension_unit_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.dimension_unit') }}<i class="text-danger">*</i>
                            </label>

                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    @foreach($dimension_units as $dimension_unit)
                                    <option value="{{ $dimension_unit->id }}" @if($dimension_unit->id == old($field, $warehouses->{$field})) selected="selected"@endif>
                                            {{ $dimension_unit->code }}
                                    </option>
                                    @endforeach
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <a href="{{ route('admin.warehouses.index') }}" class="btn btn-white">
                                {{ __('label.cancel') }}
                            </a>
                            <button type="submit" class="btn btn-info disabled-submit">
                                <i class="fa fa-check"></i> {{ __('label.update') }}
                            </button>
                        </div>
                    </div>
                </div>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection
@section('footer')
  <script>
      $(function () {
          var countryEle = $('select#country_id');
          var provinceEle = $('select#province_id');
          var cityEle = $('select#city_id');
          var wardEle = $('select#ward_id');

          var getProvinces = function () {
              var countryId = parseInt(countryEle.val()),
                  data      = {
                      country_id: countryId
                  };
              provinceEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.provinces.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.provinces, function (key, province) {
                      var option = '<option value="' + province.id + '"' + (province.id === parseInt({{ old('province_id', $warehouses->province_id) }}) ? ' selected="selected"' : '') + '>' + province.name + '</option>';
                      provinceEle.append(option);
                  });
                  provinceEle.prop('disabled', false).trigger('change');
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          var getCities = function () {
              var countryId  = parseInt(countryEle.val()),
                  provinceId = parseInt(provinceEle.val()),
                  data       = {
                      country_id : countryId,
                      province_id: provinceId
                  };
              cityEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.cities.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.cities, function (key, city) {
                      var option = '<option value="' + city.id + '"' + (city.id === parseInt({{ old('city_id', $warehouses->city_id) }}) ? ' selected="selected"' : '') + '>' + city.name + '</option>';
                      cityEle.append(option);
                  });
                  cityEle.prop('disabled', false).trigger('change');;
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };
          var getWards = function () {
              var cityId  = parseInt(cityEle.val()),
                  data   = {
                      city_id : cityId
                  };
              wardEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.wards.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.wards, function (key, ward) {
                      var option = '<option value="' + ward.id + '"' + (ward.id === parseInt({{ old('ward_id', $warehouses->ward_id) }}) ? ' selected="selected"' : '') + '>' + ward.name + '</option>';
                      wardEle.append(option);
                  });
                  wardEle.prop('disabled', false);
                  setSelect2();
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          if ('{{ old('country_id', $warehouses->country_id) }}') {
              getProvinces();
          }
          cityEle.change(function () {
              getWards();
          });
          countryEle.change(function () {
              getProvinces();
          });

          provinceEle.change(function () {
              getCities();
          });
      });
  </script>
@endsection
