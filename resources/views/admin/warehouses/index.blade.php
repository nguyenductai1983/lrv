@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('warehouse.manage') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-1">
          <a href="{{ route('admin.warehouses.create') }}" class="btn btn-success">
            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
          </a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-hover table-bordered" style="min-width: 2500px;">
          <thead>
          <tr>
            <th>{{ __('label.action') }}</th>
            <th>{{ __('label.code') }}</th>
            <th>{{ __('label.name') }}</th>
            <th>{{ __('label.country') }}</th>
            <th>{{ __('label.province') }}</th>
            <th>{{ __('label.city') }}</th>
            <th>{{ __('label.address') }}</th>
            <th>{{ __('label.telephone') }}</th>
            <th>{{ __('label.email') }}</th>
            <th>{{ __('label.contact_person') }}</th>
            <th>{{ __('label.type') }}</th>
            <th>{{ __('label.currency_unit') }}</th>
            <th>{{ __('label.weight_unit') }}</th>
            <th>{{ __('label.dimension_unit') }}</th>
            <th>{{ __('label.action') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($warehouses->count() > 0)
            @foreach($warehouses as $warehouse)
              <tr>
                <td>
                <a href="{{ route('warehouses.list_product', $warehouse->id) }}" class="btn btn-xs btn-info">
                    <i class="fa fa-eye"></i>
                </a>
            </td>
                <td>{{ $warehouse->code }}</td>
                <td>{{ $warehouse->name }}</td>
                <td>{{ $warehouse->country->name }}</td>
                <td>{{ $warehouse->province->name }}</td>
                <td>{{ $warehouse->city->name }}</td>
                <td>{{ $warehouse->address }}</td>
                <td>{{ $warehouse->telephone }}</td>
                <td>{{ $warehouse->email }}</td>
                <td>{{ $warehouse->contact_person }}</td>
                <td>{{ $warehouse->type_name }}</td>
                <td>{{ isset($warehouse->currency->code) ? $warehouse->currency->code : 'unknown' }}</td>
                <td>{{ isset($warehouse->weight_unit->name) ? $warehouse->weight_unit->name : 'unknown' }}</td>
                <td>{{ isset($warehouse->dimension_unit->name) ? $warehouse->dimension_unit->name : 'unknown' }}</td>
                <td>
                  <a href="{{ route('admin.warehouses.edit', $warehouse->id) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-edit"></i>
                  </a>
                  <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $warehouse->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                  <div class="modal fade" id="modal-delete-{{ $warehouse->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('admin.warehouses.destroy', $warehouse->id) }}" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                          </div>
                          <div class="modal-body">
                            {!! __('label.confirm_delete_msg') !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                            <button type="submit" class="btn btn-danger disabled-submit">
                              <i class="fas fa-trash"></i> {{ __('label.delete') }}
                            </button>
                          </div>
                          {{ csrf_field() }}
                          {{ method_field('delete') }}
                        </form>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="4">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
