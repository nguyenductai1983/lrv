@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('user.manage') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-2">
          <a href="{{ route('admin.users.create') }}" class="btn btn-success">
            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
          </a>
        </div>
        <div class="col-sm-10">
          <form action="{{ route('admin.users.index') }}" method="get" class="form-inline">
            @php $field = 'keyword'; @endphp
            <div class="form-group">
              <input type="text" name="{{ $field }}" class="form-control" placeholder="{{ __('label.keyword') }}"
                     value="{{ request()->query($field) }}">
            </div>

            @php $field = 'agency_id'; @endphp
            <div class="form-group">
              <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                <option value="">{{ __('user.select_agency') }}</option>
                @foreach($agencies as $agency)
                  <option value="{{ $agency->id }}"
                          @if($agency->id == request()->query($field)) selected="selected"@endif>
                    {{ $agency->name }}
                  </option>
                @endforeach
              </select>
            </div>

            @php $field = 'role_id'; @endphp
            <div class="form-group">
              <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                <option value="">{{ __('user.select_role') }}</option>
                @foreach($roles as $role)
                  <option value="{{ $role->id }}"
                          @if($role->id == request()->query($field)) selected="selected"@endif>
                    {{ $role->name }}
                  </option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-white btn-single">
                <i class="fa fa-search"></i> {{ __('label.find') }}
              </button>
              <a href="{{ route('admin.users.index') }}" class="btn btn-white btn-single">
                {{ __('label.clear') }}
              </a>
            </div>
          </form>
        </div>
      </div>
      @include('partials.flash')
      <div class="table-responsive">
        <table class="table table-hover table-bordered" >
          <thead>
          <tr>
            <th class="text-center" width="80">{{ __('user.code') }}</th>
            <th>{{ __('user.full_name') }}</th>
            <th>{{ __('user.display_name') }}</th>
            <th>{{ __('user.agency_id') }}</th>
            <th>{{ __('user.role_id') }}</th>
            <th>{{ __('user.email') }}</th>
            <th>{{ __('user.mobile_phone') }}</th>
            <th>{{ __('label.action') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($users->count() > 0)
            @foreach($users as $user)
              <tr>
                <td class="text-center">{{ $user->code }}</td>
                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                <td>{{ $user->display_name }}</td>
                <td>{{ $user->agency->name }}</td>
                <td>{{ $user->role->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->mobile_phone }}</td>
                <td>
                  <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-edit"></i>
                  </a>
                  <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $user->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                  <div class="modal fade" id="modal-delete-{{ $user->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('admin.users.destroy', $user->id) }}" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                          </div>
                          <div class="modal-body">
                            {!! __('label.confirm_delete_msg') !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                            <button type="submit" class="btn btn-danger disabled-submit">
                              <i class="fas fa-trash"></i> {{ __('label.delete') }}
                            </button>
                          </div>
                          {{ csrf_field() }}
                          {{ method_field('delete') }}
                        </form>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="7">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
