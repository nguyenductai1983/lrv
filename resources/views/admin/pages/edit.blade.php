@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        @if($type == config('page.type_header'))
        Header - {{ __('page.edit') }}
        @elseif($type == config('page.type_footer'))
        Footer - {{ __('page.edit') }}
        @endif
    </div>
    <div class="panel-body">
        <form action="{{ route('admin.pages.update', ['id' => $page->id, 'type' => $type]) }}" method="post" role="form" class="form-horizontal">
            @php
            $errorName=false;
            foreach(config('app.locales') as $locale => $localeName) {
            if ($errors->has('name_' . $locale)) {
            $errorName=true;
            break;
            }
            }
            @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.name') }}<i class="text-danger">*</i>
                </label>
                <div class="col-sm-10 tabs-border">
                    <ul class="nav nav-tabs">
                        @foreach(config('app.locales') as $locale => $localeName)
                        @php
                        $activeTab=false;
                        if ($errorName) {
                        if ($errors->has('name_' . $locale)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($locale == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <li class="{{ $activeTab ? 'active' : '' }}">
                            <a href="#tab-name-{{ $locale }}" data-toggle="tab">
                                <span class="visible-xs">{{ $locale }}</span>
                                <span class="hidden-xs">{{ $localeName }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($page->translates as $translate)
                        @php
                        $activeTab=false;
                        if ($errorName) {
                        if ($errors->has('name_' . $translate->lang)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($translate->lang == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <div class="tab-pane{{ $activeTab ? ' active' : '' }}" id="tab-name-{{ $translate->lang }}">
                            <div class="form-group{{ $errors->has('name_' . $translate->lang) ? ' validate-has-error' : '' }}">
                                <input type="text" name="name_{{ $translate->lang }}" class="form-control" value="{{ old('name_' . $translate->lang, $translate->name) }}">
                                @if($errors->has('name_' . $translate->lang))
                                <span>{{ $errors->first('name_' . $translate->lang) }}</span>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            @php $field='parent_id'; @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.parent') }}<i class="text-danger">*</i>
                </label>
                <div class="col-sm-10">
                    <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                        <option value="0">{{ __('page.root') }}</option>
                        @foreach($pages as $key1 => $page1)
                        @if($page1->parent_id == 0)
                        <option value="{{ $page1->id }}"
                                @if($page1->id == old($field, $page->parent_id)) selected="selected"@endif>{{ $page1->translate->name }}</option>
                        @if($type == config('page.type_header'))
                        @foreach($pages as $key2 => $page2)
                        @if ($page2->parent_id == $page1->id)
                        <option value="{{ $page2->id }}"
                                @if($page2->id == old($field, $page->parent_id)) selected="selected"@endif>|---{{ $page2->translate->name }}</option>
                        @php $pages->forget($key2); @endphp
                        @endif
                        @endforeach
                        @endif
                        @php $pages->forget($key1); @endphp
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>

            @php
            $errorDetail=false;
            foreach(config('app.locales') as $locale => $localeName) {
            if ($errors->has('detail_' . $locale)) {
            $errorDetail=true;
            break;
            }
            }
            @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.detail') }}
                </label>
                <div class="col-sm-10 tabs-border">
                    <ul class="nav nav-tabs">
                        @foreach(config('app.locales') as $locale => $localeName)
                        @php
                        $activeTab=false;
                        if ($errorDetail) {
                        if ($errors->has('detail_' . $locale)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($locale == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <li class="{{ $activeTab ? 'active' : '' }}">
                            <a href="#tab-detail-{{ $locale }}" data-toggle="tab">
                                <span class="visible-xs">{{ $locale }}</span>
                                <span class="hidden-xs">{{ $localeName }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($page->translates as $translate)
                        @php
                        $activeTab=false;
                        if ($errorDetail) {
                        if ($errors->has('detail_' . $translate->lang)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($translate->lang == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <div class="tab-pane{{ $activeTab ? ' active' : '' }}" id="tab-detail-{{ $translate->lang }}">
                            <textarea id="detail_{{ $translate->lang }}" name="detail_{{ $translate->lang }}" class="form-control" cols="30" rows="20">{!! $translate->detail !!}</textarea>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            @php $field='link_to'; @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.link_to') }}
                </label>
                <div class="col-sm-10">
                    <input type="text" name="{{ $field }}" class="form-control" value="{{ $page->link_to }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12 text-right">
                    <a href="{{ route('admin.pages.index', ['type' => $type]) }}" class="btn btn-white">
                        {{ __('label.cancel') }}
                    </a>
                    <button type="submit" class="btn btn-info disabled-submit">
                        <i class="fa fa-check"></i> {{ __('label.update') }}
                    </button>
                </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('put') }}
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('components\ckeditor\ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('detail_vi', {
        filebrowserBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });
    CKEDITOR.replace('detail_en', {
        filebrowserBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });
    //  <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('components/ckeditor/adapters/jquery.js') }}"></script>
</script>

@endsection
