@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            @if($type == config('page.type_header'))
                Header
            @elseif($type == config('page.type_footer'))
                Footer
            @endif
        </div>
        <div class="panel-body">
            {{--<div class="form-group">
                <a href="{{ route('admin.product-groups.create') }}" class="btn btn-success">
                    <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                </a>
            </div>--}}
            <ul class="menu-manage">
                @foreach($pages as $key1 => $page1)
                    @if ($page1->parent_id == 0)
                        <li>
                            <table>
                                <tr>
                                    <td>{{ $page1->translate->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.pages.edit', ['id' => $page1->id, 'type' => $type]) }}" class="text-white"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" class="text-white" onclick="page.remove({{ $page1->id }})"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            </table>
                            <ul>
                                @foreach($pages as $key2 => $page2)
                                    @if ($page2->parent_id == $page1->id)
                                        <li>
                                            <table>
                                                <tr>
                                                    <td>{{ $page2->translate->name }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.pages.edit', ['id' => $page2->id, 'type' => $type]) }}" class="text-white"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="text-white" onclick="page.remove({{ $page2->id }})"><i class="fas fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            @if($type == config('page.type_header'))
                                                <ul>
                                                    @foreach($pages as $key3 => $page3)
                                                        @if ($page3->parent_id == $page2->id)
                                                            <li>
                                                                <table>
                                                                    <tr>
                                                                        <td>{{ $page3->translate->name }}</td>
                                                                        <td>
                                                                            <a href="{{ route('admin.pages.edit', ['id' => $page3->id, 'type' => $type]) }}" class="text-white"><i class="fa fa-edit"></i></a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0);" class="text-white" onclick="page.remove({{ $page3->id }})"><i class="fas fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </li>
                                                            @php $pages->forget($key3); @endphp
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                        @php $pages->forget($key2); @endphp
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        @php $pages->forget($key1); @endphp
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
@endsection
@section('footer')
  <script src="{{ asset('js/admin/app/page.js?t=' . File::lastModified(public_path('js/admin/app/page.js'))) }}"></script>
@endsection
