@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('campaign.edit') }}
    </div>
    <div class="panel-body">
      <form id="form-customer" action="{{ route('admin.campaign.update', $campaign->id) }}" method="post" role="form" class="form-horizontal">
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'customer_group_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_customer_group') }}</option>
                  @foreach($groups as $group)
                    <option value="{{ $group->id }}" @if($group->id == old($field, $campaign->{$field})) selected="selected"@endif>{{ $group->name }}</option>
                  @endforeach
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'amount'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'num_of_use'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'weight'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

          </div>
          <div class="col-sm-6">
            @php $field = 'apply_per_customer'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
            @php $field = 'from_date'; @endphp
            <div class="form-group">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <div class="input-group date-picker{{ $errors->has($field) ? ' validate-has-error' : '' }}" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                  <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field} ? $campaign->{$field} : '') }}">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-white">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                </div>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'to_date'; @endphp
            <div class="form-group">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <div class="input-group date-picker{{ $errors->has($field) ? ' validate-has-error' : '' }}" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                  <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $campaign->{$field} ? $campaign->{$field} : '') }}">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-white">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                </div>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
            @php $field = 'status'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('campaign.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="1" @if(1 == old($field, $campaign->{$field})) selected="selected"@endif>{{ __('campaign.status_active') }}</option>
                  <option value="2" @if(2 == old($field, $campaign->{$field})) selected="selected"@endif>{{ __('campaign.status_deactive') }}</option>
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
            
          </div>
        </div>

    <div class="row">
            <div class="col-sm-4">
                @php $field = 'Transport'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-6">
                        {{ __('campaign.' . $field) }}
                    </label>
                    <div class="col-sm-6">
                        <div class="form-control"> 
                            <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1"                                 
                                   @if(old($field, !$errors->any() ? $campaign->$field : 0)) checked="checked"@endif>
                        </div>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>
        <div class="col-sm-4">
                @php $field = 'Express'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-6">
                        {{ __('campaign.' . $field) }}
                    </label>
                    <div class="col-sm-6">
                        <div class="form-control">
                            <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1"
                                  @if(old($field, !$errors->any() ? $campaign->$field : 0)) checked="checked"@endif>
                        </div>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>
        <div class="col-sm-4">
                @php $field = 'YHL'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-6">
                        {{ __('campaign.' . $field) }}
                    </label>
                    <div class="col-sm-6">
                        <div class="form-control">
                            <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1"
                                  @if(old($field, !$errors->any() ? $campaign->$field : 0)) checked="checked"@endif>
                        </div>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>         
    </div>
    <div class="row">
          <div class="col-sm-4">
                @php $field = 'MTS'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-6">
                        {{ __('campaign.' . $field) }}
                    </label>
                    <div class="col-sm-6">
                        <div class="form-control">
                            <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1"
                                  @if(old($field, !$errors->any() ? $campaign->$field : 0)) checked="checked"@endif>
                        </div>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>          
          
            <div class="col-sm-4">
                @php $field = 'Quote'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-6">
                        {{ __('campaign.' . $field) }}
                    </label>
                    <div class="col-sm-6">
                        <div class="form-control">
                            <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="1"
                                  @if(old($field, !$errors->any() ? $campaign->$field : 0)) checked="checked"@endif>
                        </div>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>  
    </div>          
        <div class="row form-group">
          <div class="col-sm-12 text-right">
            <a href="{{ route('admin.campaign.index') }}" class="btn btn-white">
              {{ __('label.cancel') }}
            </a>
            <button type="submit" class="btn btn-info disabled-submit">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button>
          </div>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
    </div>
  </div>
@endsection