@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('campaign.manage') }}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('admin.campaign.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.campaign.index') }}" method="get" role="form" class="form-inline">
                    @php $field = 'code'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('campaign.code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.campaign.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered" style="min-width: 1500px;">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="15%">{{ __('campaign.name') }}</th>
                        <th width="10%">{{ __('campaign.code') }}</th>
                        <th width="5%">{{ __('campaign.amount') }}</th>
                        <th width="10%">{{ __('campaign.from_date') }}</th>
                        <th width="10%">{{ __('campaign.to_date') }}</th>
                        <th width="5%">{{ __('campaign.customer_group_id') }}</th>
                        <th width="5%">{{ __('campaign.weight') }}</th>
                        <th width="7%">{{ __('campaign.apply_per_customer') }}</th>
                        <th width="7%">{{ __('campaign.num_of_use') }}</th>
                        <th width="7%">{{ __('campaign.status') }}</th>
                        <th width="7%">{{ __('campaign.created_at') }}</th>
                        <th width="10%">{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($campaigns->count() > 0)
                    <?php $no = 1;?>
                    @foreach($campaigns as $campaign)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $campaign->name }}</td>
                        <td>{{ $campaign->code }}</td>
                        <td>{{ $campaign->amount }}</td>
                        <td>{{ $campaign->from_date }}</td>
                        <td>{{ $campaign->to_date }}</td>
                        <td>{{ isset($campaign->group->name) ? $campaign->group->name : '' }}</td>
                        <td>{{ $campaign->weight }}</td>
                        <td>{{ $campaign->apply_per_customer }}</td>
                        <td>{{ $campaign->num_of_use }}</td>
                        <td><span class="{{ $campaign->status_label }}">{{ $campaign->status_name }}</span></td>
                        <td>{{ $campaign->created_at }}</td>
                        <td>                           
                            <a href="{{ route('admin.campaign.edit', $campaign->id) }}" class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $no ++;?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $campaigns->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection