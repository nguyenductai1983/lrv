@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('city.create') }}
    </div>
    <div class="panel-body">
        <form action="{{ route('admin.cities.store') }}" method="post" role="form" class="form-horizontal">
            <div class="row">
                <div class="col-sm-6">
                    @php $field = 'country_id'; @endphp
                    <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                        <label for="{{ $field }}" class="control-label col-sm-3">
                            {{ __('city.country_id') }}<i class="text-danger">*</i>
                        </label>
                        <div class="col-sm-9">
                            <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="">{{ __('city.select_country') }}</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" @if($country->id == old($field)) selected="selected"@endif>
                                        {{ $country->code . '-' . $country->name }}
                            </option>
                            @endforeach
                        </select>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                @php $field = 'province_id'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-3">
                        {{ __('province.name') }}<i class="text-danger">*</i>
                    </label>
                    <div class="col-sm-9">
                        <select id="{{ $field }}" name="{{ $field }}" class="form-control" @if(old($field)) disabled="disabled"@endif>
                                <option value="">{{ __('city.select_province') }}</option>
                        </select>
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                @php $field = 'name'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-3">
                        {{ __('city.name') }}<i class="text-danger">*</i>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                @php $field = 'shipping_fee'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-3">
                        {{ __('city.shipping_fee') }}<i class="text-danger">*</i>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                @php $field = 'postal_code'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-3">
                        {{ __('label.post_code') }}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                @php $field = 'pickup_fee'; @endphp
                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                    <label for="{{ $field }}" class="control-label col-sm-3">
                        {{ __('product.pickup_fee') }}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                        @if($errors->has($field))
                        <span class="validate-has-error">{{ $errors->first($field) }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12 text-right">
                <a href="{{ route('admin.cities.index') }}" class="btn btn-white">
                    {{ __('label.cancel') }}
                </a>
                <button type="submit" class="btn btn-info disabled-submit">
                    <i class="fa fa-check"></i> {{ __('label.save') }}
                </button>
            </div>
        </div>
        {{ csrf_field() }}
    </form>
</div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
    var countryEle = $('#country_id');
    var setProvinces = function () {
    var countryId = countryEle.val();
    var provinceEle = $('#province_id');
    if (!countryId) {
    provinceEle.find('option').each(function (key, option) {
    if ($(option).val()) {
    $(option).remove();
    }
    });
    return false;
    }

    provinceEle.prop('disabled', true);
    var data = {
    country_id: countryId
    };
    $.ajax({
    method  : 'GET',
            url     : '{{ route('admin.provinces.index') }}?' + toUrlEncodedString(data),
            dataType: 'json'
    }).done(function (data) {
    provinceEle.find('option').each(function (key, option) {
    if ($(option).val()) {
    $(option).remove();
    }
    });
    data.provinces.forEach(function (province) {
    var option = '<option value="' + province.id + '"';
    if (province.id === parseInt('{{ old('province_id') }}')) {
    option += ' selected="selected"';
    }
    option += ' >' + province.name + '</option>';
    provinceEle.append(option);
    });
    provinceEle.prop('disabled', false);
    }).fail(function (jqXHR) {
    console.log(jqXHR);
    });
    return true;
    };
    if (countryEle.val()) {
    setProvinces();
    }

    countryEle.change(function () {
    setProvinces();
    });
    });
</script>
@endsection
