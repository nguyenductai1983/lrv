@extends('layouts.admin.index', ['menu' => ''])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('user.edit') }}
    </div>
    <div class="panel-body">
      <form action="{{ route('admin.profile.info-update') }}" method="post" role="form" class="form-horizontal">
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'warehouse_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.warehouse_id') }}
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('user.select_warehouse') }}</option>
                  @foreach($warehouses as $warehouse)
                    <option value="{{ $warehouse->id }}" @if($warehouse->id == old($field, $user->{$field})) selected="selected"@endif>
                      {{ $warehouse->name }}
                    </option>
                  @endforeach
                </select>
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'first_name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.first_name') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'email'; @endphp
            <div class="form-group">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.email') }}
              </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" value="{{ $user->{$field} }}">
              </div>
            </div>

            @php $field = 'gender'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.gender') }}
              </label>
              <div class="col-sm-8">
                <div class="form-control">
                  <label class="cbr-inline">
                    <input type="radio" name="{{ $field }}" class="cbr cbr-info" value="{{ config('user.male') }}"
                           @if(old($field, $user->{$field}) == config('user.male')) checked="checked"@endif> {{ __('user.male') }}
                  </label>
                  <label class="cbr-inline">
                    <input type="radio" name="{{ $field }}" class="cbr cbr-info" value="{{ config('user.female') }}"
                           @if(old($field, $user->{$field}) == config('user.female')) checked="checked"@endif> {{ __('user.female') }}
                  </label>
                </div>
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'address'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.address') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'mobile_phone'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.mobile_phone') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'home_phone'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.home_phone') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
            @php $field = 'office_phone'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.office_phone') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'fax'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.fax') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'bank_account'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.bank_account') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'bank_code'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.bank_code') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'tax_code'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.tax_code') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            @php $field = 'id_card'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.id_card') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'date_issued'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.date_issued') }}
              </label>
              <div class="col-sm-8">
                <div class="input-group date-picker{{ $errors->has($field) ? ' validate-has-error' : '' }}" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                  <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field} ? $user->{$field . '_formatted'} : '') }}">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-white">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                </div>
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'place_issued'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.place_issued') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'birthday'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.birthday') }}
              </label>
              <div class="col-sm-8">
                <div class="input-group date-picker{{ $errors->has($field) ? ' validate-has-error' : '' }}" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                  <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field} ? $user->{$field . '_formatted'} : '') }}">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-white">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                </div>
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'family_allowance'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.family_allowance') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'mts_percent'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('user.mts_percent') }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'country_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_country') }}</option>
                  @foreach($countries as $country)
                    <option value="{{ $country->id }}" @if($country->id == old($field, $user->{$field})) selected="selected"@endif>{{ $country->code . '-' . $country->name }}</option>
                  @endforeach
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'province_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_province') }}</option>
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'city_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_city') }}</option>
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'postal_code'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $user->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row form-group">
          <div class="col-sm-12 text-right">
            <a href="{{ route('admin.index') }}" class="btn btn-white">
              {{ __('label.cancel') }}
            </a>
            <button type="submit" class="btn btn-info disabled-submit">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button>
          </div>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
    </div>
  </div>
@endsection

@section('footer')
  <script>
      $(function () {
          var countryEle = $('select#country_id');
          var provinceEle = $('select#province_id');
          var cityEle = $('select#city_id');

          var getProvinces = function () {
              var countryId = parseInt(countryEle.val()),
                  data      = {
                      country_id: countryId
                  };

              provinceEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.provinces.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.provinces, function (key, province) {
                      var option = '<option value="' + province.id + '"' + (province.id === parseInt({{ old('province_id', $user->province_id) }}) ? ' selected="selected"' : '') + '>' + province.name + '</option>';
                      provinceEle.append(option);
                  });
                  provinceEle.prop('disabled', false).trigger('change');
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          var getCities = function () {
              var countryId  = parseInt(countryEle.val()),
                  provinceId = parseInt(provinceEle.val()),
                  data       = {
                      country_id : countryId,
                      province_id: provinceId
                  };

              cityEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.cities.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.cities, function (key, city) {
                      var option = '<option value="' + city.id + '"' + (city.id === parseInt({{ old('city_id', $user->city_id) }}) ? ' selected="selected"' : '') + '>' + city.name + '</option>';
                      cityEle.append(option);
                  });
                  cityEle.prop('disabled', false);
                  setSelect2();
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          if ('{{ old('country_id', $user->country_id) }}') {
              getProvinces();
          }

          countryEle.change(function () {
              getProvinces();
          });

          provinceEle.change(function () {
              getCities();
          });
      });
  </script>
@endsection