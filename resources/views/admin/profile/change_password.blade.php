@extends('layouts.admin.index', ['menu' => ''])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
        @if(isset($expired))
        {{ __('user.password_expired') }}
        @else
        {{ __('home.change-password') }}
    @endif
    </div>

    <div class="panel-body">
        @if (session('status'))
        <div class="alert alert-success">
            {{ __('user.password_successfully') }}
        </div>
        <a href="/admin">{{ __('user.dashboard') }}</a>
    @else
    {{-- hiễn thị form nhập liệu --}}
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    {{-- hết hiển thị lỗi --}}
      <form action="{{ route('admin.profile.change-password-update') }}" method="post" role="form" class="form-horizontal">
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'old_password'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
              {{ __('home.current-password') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="password" name="{{ $field }}" class="form-control input-sm" value="">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'password'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('home.new-password') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="password" name="{{ $field }}" class="form-control input-sm" value="">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'password_confirmation'; @endphp
            <div class="form-group">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('home.retype-password') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="password" name="{{ $field }}" class="form-control input-sm" value="">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>

          <div class="col-sm-6">

          </div>
        </div>

        <div class="row form-group">
          <div class="col-sm-12 text-right">
            <a href="{{ route('admin.index') }}" class="btn btn-white">
              {{ __('label.cancel') }}
            </a>
            <button type="submit" class="btn btn-info disabled-submit">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button>
          </div>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
      @endif
    </div>
  </div>
@endsection
