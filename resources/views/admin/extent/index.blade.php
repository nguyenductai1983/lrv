@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('label.manager_extra_refund') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.extent.index') }}" method="get" role="form" class="form-inline">
                    {{-- <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div> --}}
                    <div class="form-group col-sm-6">
                        <input id="code" name="code" value="{{ request()->query('code')}}" style="width: 100%;" class="form-control" type="text"  placeholder="{{ __('label.find_order') }}">
                    </div>
                    <div class="form-group">
                        <a href="{{ route('admin.extent.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <button type="button" class="btn btn-white btn-single" onclick="extent.export();">
                            <i class="fas fa-file-excel"></i> {{ __('label.export_excel') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th width="160" class="text-center">{{ __('label.code') }}</th>
                            <th width="160" class="text-center">{{ __('label.order_code') }}</th>
                            <th width="160" class="text-center">{{ __('label.amount') }}</th>
                            <th width="160" class="text-center">{{ __('label.reason') }}</th>
                            <th width="150" class="text-center">{{ __('label.create_time') }}</th>
                            <th width="120" class="text-center">{{ __('label.status') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($extents->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($extents as $extent)
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                            <td class="text-center">{{ $extent->code }}</td>
                            <td class="text-center">{{ $extent->order->code }}</td>
                            <td class="text-center">{{ $extent->amount }}</td>
                            <td class="text-center">{{ $extent->reason }}</td>
                            <td class="text-center">{{ $extent->created_at }}</td>
                            <td class="text-center">
                                <span class="{{ $extent->status_label }}">{{ $extent->status_name }}</span>
                            </td>
                            <td>
                                @if($extent->status == 1)
                                <button type="button" class="btn btn-xs btn-info" onclick="extent.approve({{ $extent->id }})">
                                    <i class="fa fa-check"></i> {{ __('label.approve') }}
                                </button>
                                <button type="button" class="btn btn-xs btn-danger" onclick="extent.cancel({{ $extent->id }})">
                                    <i class="fas fa-trash"></i>
                                </button>
                                @endif
                                @if($extent->status == 2)
                                <button type="button" class="btn btn-xs btn-info" onclick="extent.payment({{ $extent->id }})">
                                    <i class="fas fa-money-check-alt"></i>
                                     {{-- <i class="fa fa-money" aria-hidden="true"></i> --}}
                                      {{ __('label.payment_status_short') }}
                                </button>
                                @endif
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $extents->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/admin/app/extent.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
