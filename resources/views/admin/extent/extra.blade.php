@extends('layouts.admin.index', ['menu' => $menu])
@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
       Extra Fee {{ $order_id }}
    </div>
    <div class="panel-body">
        <form id="create-extra-form" action="{{ route('admin.extent.store') }}" method="post" role="form">
            <input type="hidden" name="order_id" value={{ $order_id }} class="form-control form-white">
            <div class="form-group">
                <label class="control-label">{{ __('label.amount') }}</label>
                <input type="text" name="amount" value="" class="form-control form-white">
            </div>

            <div class="form-group">
                <label class="control-label">{{ __('label.reason') }} </label>
                <textarea name="reason" class="form-control form-white"></textarea>
            </div>
            <div class="container-fluid">
                <div class="col-sm-3 col-xs-4">
                    <div class="col-label">
                    {{ __('customer.image_1_file_id') }}
                    </div>
                    @include('partials.form-controls.image', ['field' => 'image_1_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
                </div>
                <div class="col-sm-3 col-xs-4">
                    <div class="col-label">
                    {{ __('customer.image_1_file_id') }}
                    </div>
                    @include('partials.form-controls.image', ['field' => 'image_2_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
                </div>
                <div class="col-sm-3 col-xs-4">
                    <div class="col-label">
                    {{ __('customer.image_1_file_id') }}
                    </div>
                    @include('partials.form-controls.image', ['field' => 'image_3_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
                </div>
            </div>
            <button type="submit" class="btn btn-info disabled-submit">
                <i class="fa fa-check"></i> {{ __('label.add') }}
              </button>
              {{ csrf_field() }}
        </form>

        <form id="shipment-package-form">
            <div class="table-responsive" id="table_transport">
                <table class="table table-hover table-bordered" style="min-width: 700px;">
                    <thead>
                        <tr>
                            <th width="5">#</th>
                            <th width="20">{{ __('label.form_code') }}</th>
                            <th width="20" class="text-center">{{ __('label.order_code') }}</th>
                            <th width="50">{{ __('label.reason') }} </th>
                            <th width="50">{{ __('label.amount') }} </th>
                            <th width="20" class="text-center">{{ __('label.date') }}</th>
                            <th width="20" class="text-center">{{ __('label.status') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($extras->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($extras as $extra)
                     </tr>
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                            <td>
                                {{ $extra->code }}
                             </td>
                             <td class="text-center">{{ $extra->order->code }}</td>
                            <td>
                               {{ $extra->reason }}
                            </td>
                            <td>
                                {{ $extra->amount }}
                             </td>
                             <td class="text-center">{{ $extra->created_at }}</td>
                             <td class="text-center">
                                <span class="{{ $extra->status_label }}">{{ $extra->status_name }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                          @if($extra->image_1_file_id!==null)
                          <div class="col-sm-3 col-xs-4" >
                            <img src="{{ $extra->image1->path }}" class="img-thumbnail" onclick="image_click('image1')" id="image1">
                          </div>
                           @endif
                           @if($extra->image_2_file_id!==null)
                           <div class="col-sm-3 col-xs-4" >
                           <img src="{{ $extra->image2->path }}" class="img-thumbnail" onclick="image_click('image2')" id="image2">
                           </div>
                            @endif
                            @if($extra->image_3_file_id!==null)
                            <div class="col-sm-3 col-xs-4" >
                            <img src="{{ $extra->image3->path }}" class="img-thumbnail" onclick="image_click('image3')" id="image3">
                            </div>
                             @endif
                        </td>

                        <?php $no++; ?>

                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $extras->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer')
<script>
$(document).ready(function(){
  $("#mySearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#shipment-package-items tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
var image_click = function (image_object){
       var  output = document.getElementById(image_object);
         parentv3= output.parentElement;
  if (parentv3.className === "col-sm-3 col-xs-4"){
    parentv3.className = "col-sm-6 col-xs-6";
  }
  else
  {
    parentv3.className = "col-sm-3 col-xs-4";
  }
  };
</script>
@endsection
