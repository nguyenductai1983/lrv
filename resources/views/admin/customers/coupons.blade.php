@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('campaign.manage') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.customers.coupons') }}" method="get" role="form" class="form-inline">
                    @php $field = 'customer_id'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('label.customer_id') }}">
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.customers.coupons') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="10%">{{ __('campaign.code') }}</th>
                        <th width="10%">{{ __('campaign.name') }}</th>
                        <th width="10%">{{ __('campaign.amount') }}</th>
                        <th width="10%">{{ __('campaign.num_of_use') }}</th>
                        <th width="10%">{{ __('campaign.created_at') }}</th>
                        <th width="10%">{{ __('campaign.use_status') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @if($coupons->count() > 0)
                    @foreach($coupons as $coupon)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $coupon->code }}</td>
                        <td>{{ $coupon->name }}</td>
                        <td>{{ $coupon->amount }}</td>
                        <td>{{ $coupon->num_of_use }}</td>
                        <td>{{ $coupon->created_at }}</td>
                        <td>{{ $coupon->is_used_name }}</td>
                    </tr>
                    @php $no++; @endphp
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $coupons->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection