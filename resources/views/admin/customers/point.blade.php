@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('point.customer_point_log') }}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">{{ __('point.value') }}</th>
                        <th width="10%">{{ __('point.type') }}</th>
                        <th width="15%">{{ __('point.create_time') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($customers->count() > 0)
                    @foreach($customers as $customer)
                    <tr>
                        <td>{{ $customer->point }}</td>
                        <td>{{ $customer->type_name }}</td>
                        <td>{{ $customer->created_at }}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $customers->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection