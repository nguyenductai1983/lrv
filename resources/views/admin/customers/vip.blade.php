@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('customer.member_vip') }} - <strong>{{ $customer->full_name }}</strong>
    </div>
    <div class="panel-body">
        @include('partials.errors')
        @include('partials.flash')
        <form action="{{ route('admin.customers.vip', $customer->id) }}" method="post" novalidate>
            <div class="form-group" style="width: 150px;">
                <label for="member_type">{{ __('customer.cog_label') }}</label>
                <select name="member_type" id="member_type" class="form-control">
                    <option value="1" <?php if($customer->member_type == 1){ ?>selected="selected"<?php }?>>{{ __('label.no') }}</option>
                    <option value="2" <?php if($customer->member_type == 2){ ?>selected="selected"<?php }?>>{{ __('label.yes') }}</option>
                </select>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('customer.type_service') }}</th>
                            <th>{{ __('customer.discount') }} (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">Transport</td>
                            <td>
                                @php $field = 'discount[1]'; @endphp
                                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                                    <input type="number" class="form-control" name="{{ $field }}"
                                           value="{{ old($field, isset($transport->discount) ? $transport->discount : 0) }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">Express</td>
                            <td>
                                @php $field = 'discount[2]'; @endphp
                                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                                    <input type="number" class="form-control" name="{{ $field }}"
                                           value="{{ old($field, isset($express->discount) ? $express->discount : 0) }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">YHL</td>
                            <td>
                                @php $field = 'discount[3]'; @endphp
                                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                                    <input type="number" class="form-control" name="{{ $field }}"
                                           value="{{ old($field, isset($yhl->discount) ? $yhl->discount : 0) }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">MTS</td>
                            <td>
                                @php $field = 'discount[4]'; @endphp
                                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                                    <input type="number" class="form-control" name="{{ $field }}"
                                           value="{{ old($field, isset($mts->discount) ? $mts->discount : 0) }}">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-right">
                <a href="{{ route('admin.customers.index') }}" class="btn btn-white">
                    <i class="fa fa-backward"></i> {{ __('label.go_back') }}
                </a>
                <button type="submit" class="btn btn-info disabled-submit">
                    <i class="fa fa-check"></i> {{ __('label.update') }}
                </button>
            </div>
            {{ method_field('put') }}
            {{ csrf_field() }}
        </form>
    </div>
</div>
@endsection