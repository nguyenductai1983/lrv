@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('point.manage') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.customers.points') }}" method="get" role="form" class="form-inline">
                    @php $field = 'customer_id'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('label.customer_id') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.customers.points') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="10%">{{ __('point.value') }}</th>
                        <th width="10%">{{ __('point.type') }}</th>
                        <th width="10%">{{ __('point.create_time') }}</th>
                        <th width="10%">{{ __('point.use_ref_code') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @if($points->count() > 0)
                    @foreach($points as $point)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $point->point }}</td>
                        <td>{{ $point->type_name }}</td>
                        <td>{{ $point->created_at }}</td>
                        <td>{{ $point->use_ref_code }}</td>
                    </tr>
                    @php $no++; @endphp
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $points->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection