@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('customer.edit') }}
    </div>
    <div class="panel-body">
      <form id="form-customer" action="{{ route('admin.customers.update', $customer->id) }}" method="post" role="form" class="form-horizontal">
        <div class="row">
          <div class="col-sm-6">
            @php $field = 'area_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_area') }}</option>
                  @foreach($areas as $area)
                    <option value="{{ $area->id }}" @if($area->id == old($field, $customer->{$field})) selected="selected"@endif>{{ $area->code . '-' . $area->name }}</option>
                  @endforeach
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'customer_group_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_customer_group') }}</option>
                  @foreach($groups as $group)
                    <option value="{{ $group->id }}" @if($group->id == old($field, $customer->{$field})) selected="selected"@endif>{{ $group->name }}</option>
                  @endforeach
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'image_1_file_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                @include('partials.form-controls.image', ['field' => $field, 'file' => $customer->{$field} ? $customer->image1 : null])
              </div>
            </div>

            @php $field = 'image_2_file_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                @include('partials.form-controls.image', ['field' => $field, 'file' => $customer->{$field} ? $customer->image2 : null])
              </div>
            </div>

            @php $field = 'image_3_file_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                @include('partials.form-controls.image', ['field' => $field, 'file' => $customer->{$field} ? $customer->image3 : null])
              </div>
            </div>

            @php $field = 'first_name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'middle_name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'last_name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'address_1'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'address_2'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'telephone'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            @php $field = 'cellphone'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'postal_code'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'id_card'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'card_expire'; @endphp
            <div class="form-group">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <div class="input-group date-picker{{ $errors->has($field) ? ' validate-has-error' : '' }}" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                  <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field} ? $customer->{$field} : '') }}">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-white">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                </div>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'birthday'; @endphp
            <div class="form-group">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <div class="input-group date-picker{{ $errors->has($field) ? ' validate-has-error' : '' }}" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                  <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field} ? $customer->{$field} : '') }}">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-white">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                </div>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'career'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $customer->{$field}) }}">
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'country_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_country') }}</option>
                  @foreach($countries as $country)
                    <option value="{{ $country->id }}" @if($country->id == old($field, $customer->{$field})) selected="selected"@endif>{{ $country->code . '-' . $country->name }}</option>
                  @endforeach
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'province_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_province') }}</option>
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'city_id'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                  <option value="">{{ __('customer.select_city') }}</option>
                </select>
                @if($errors->has($field))
                  <span>{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-12 text-right">
            <a href="{{ route('admin.customers.index') }}" class="btn btn-white">
              {{ __('label.cancel') }}
            </a>
            <button type="submit" class="btn btn-info disabled-submit">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button>
          </div>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
    </div>
  </div>
@endsection

@section('footer')
  <script>
      $(function () {
          var countryEle = $('select#country_id');
          var provinceEle = $('select#province_id');
          var cityEle = $('select#city_id');

          var getProvinces = function () {
              var countryId = parseInt(countryEle.val()),
                  data      = {
                      country_id: countryId
                  };

              provinceEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.provinces.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.provinces, function (key, province) {
                      var option = '<option value="' + province.id + '"' + (province.id === parseInt({{ old('province_id', $customer->province_id) }}) ? ' selected="selected"' : '') + '>' + province.name + '</option>';
                      provinceEle.append(option);
                  });
                  provinceEle.prop('disabled', false).trigger('change');
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          var getCities = function () {
              var countryId  = parseInt(countryEle.val()),
                  provinceId = parseInt(provinceEle.val()),
                  data       = {
                      country_id : countryId,
                      province_id: provinceId
                  };

              cityEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('admin.cities.index') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.cities, function (key, city) {
                      var option = '<option value="' + city.id + '"' + (city.id === parseInt({{ old('city_id', $customer->city_id) }}) ? ' selected="selected"' : '') + '>' + city.name + '</option>';
                      cityEle.append(option);
                  });
                  cityEle.prop('disabled', false);
                  setSelect2();
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          if ('{{ old('country_id', $customer->country_id) }}') {
              getProvinces();
          }

          countryEle.change(function () {
              getProvinces();
          });

          provinceEle.change(function () {
              getCities();
          });
      });
  </script>
@endsection