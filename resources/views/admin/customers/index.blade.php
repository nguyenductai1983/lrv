@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('customer.manage') }}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('admin.customers.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.customers.index') }}" method="get" role="form" class="form-inline">
                    @php $field = 'code'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('customer.code') }}">
                    </div>
                    @php $field = 'telephone'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('customer.telephone') }}">
                    </div>
                    @php $field = 'first_name'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('customer.first_name') }}">
                    </div>
                    @php $field = 'last_name'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('customer.last_name') }}">
                    </div>
                    @php $field = 'customer_group_id'; @endphp
                    <div class="form-group">
                        <select name="{{ $field }}" id="{{ $field }}" class="form-control" style="width: 150px;">
                            <option value="">{{ __('customer.select_customer_group') }}</option>
                            @foreach($groups as $group)
                            <option value="{{ $group->id }}" @if($group->id == request()->query($field)) selected="selected"@endif>{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    @php $field = 'country_id'; @endphp
                    <div class="form-group">
                        <select name="{{ $field }}" id="{{ $field }}" class="form-control" style="width: 150px;">
                            <option value="">{{ __('customer.select_country') }}</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}" @if($country->id == request()->query($field)) selected="selected"@endif>{{ $country->code . '-' . $country->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.customers.excel') }}" class="btn btn-white btn-single">
                            {{ __('label.export_excel') }}
                        </a>
                        <a href="{{ route('admin.customers.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="10%">{{ __('customer.name') }}</th>
                        <th width="8%">{{ __('customer.telephone') }}</th>
                        <th width="15%">{{ __('customer.province_id') }}</th>
                        <th width="10%">{{ __('customer.city_id') }}</th>
                        <th width="10%">{{ __('customer.country_id') }}</th>
                        <th width="10%">{{ __('customer.group') }}</th>
                        <th width="7%">{{ __('customer.point') }}</th>
                        <th width="15%">{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($customers->count() > 0)
                    @foreach($customers as $customer)
                    <tr>
                        <td>{{ $customer->code }}</td>
                        <td>{{ $customer->full_name }}</td>
                        <td>{{ $customer->telephone }}</td>
                        <td>{{ isset($customer->province->name) ? $customer->province->name : '' }}</td>
                        <td>{{ isset($customer->city->name) ? $customer->city->name : '' }}</td>
                        <td>{{ isset($customer->country->name) ? $customer->country->name : ''}}</td>
                        <td>{{ isset($customer->group->name) ? $customer->group->name : '' }}</td>
                        <td>{{ isset($customer->point) ? $customer->point : 0 }}</td>
                        <td>
                            <a href="{{ route('admin.customers.vip', $customer->id) }}" class="btn btn-xs btn-primary">
                                <i class="fa fa-cog"></i>
                              </a>
                            <a href="{{ route('admin.customers.address', $customer->id) }}" class="btn btn-xs btn-primary">
                                <i class="fa fa-truck"></i>
                            </a>
                            <a href="{{ route('admin.customers.point', $customer->id) }}" class="btn btn-xs btn-success">
                                <i class="fas fa-chart-bar"></i>
                            </a>
                            <a href="{{ route('admin.customers.edit', $customer->id) }}" class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $customer->id }}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="modal-delete-{{ $customer->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.customers.destroy', $customer->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_delete_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @if(!$customer->is_lock)
                            <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-lock-{{ $customer->id }}">
                                <i class="fas fa-lock"></i>
                            </button>
                            @endif
                            <div class="modal fade" id="modal-lock-{{ $customer->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.customers.lock', $customer->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_lock') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_lock_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-lock"></i> {{ __('label.lock') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('PUT') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @if($customer->is_lock)
                            <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-unlock-{{ $customer->id }}">
                                <i class="fas fa-lock-open"></i>
                            </button>
                            @endif
                            <div class="modal fade" id="modal-unlock-{{ $customer->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.customers.unlock', $customer->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_unlock') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_unlock_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-lock-open"></i> {{ __('label.unlock') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('PUT') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $customers->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection
