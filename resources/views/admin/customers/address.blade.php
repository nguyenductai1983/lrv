@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('customer.manage_address') }} <strong>{{ $customer->full_name }}</strong>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="10%">{{ __('customer.full_name') }}</th>
                        <th width="10%">{{ __('customer.address_1') }}</th>
                        <th width="10%">{{ __('customer.telephone') }}</th>
                        <th width="10%">{{ __('customer.postal_code') }}</th>
                        <th width="10%">{{ __('customer.city') }}</th>
                        <th width="10%">{{ __('customer.province') }}</th>
                        <th width="10%">{{ __('customer.country') }}</th>
                        <th width="10%">{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($addresses->count() > 0)
                    @foreach($addresses as $address)
                    <tr>
                        <td>{{ $address->full_name }}</td>
                        <td>{{ $address->address_1 }}</td>
                        <td>{{ $address->telephone }}</td>
                        <td>{{ $address->postal_code }}</td>
                        <td>{{ isset($address->city->name) ? $address->city->name : '' }}</td>
                        <td>{{ isset($address->province->name) ? $address->province->name : '' }}</td>
                        <td>{{ isset($address->country->name) ? $address->country->name : ''}}</td>
                        <td>
                            <a href="{{ route('admin.customers.address_edit', $address->id) }}" class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $address->id }}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="modal-delete-{{ $address->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.customers.address_destroy', $address->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_delete_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $addresses->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection
