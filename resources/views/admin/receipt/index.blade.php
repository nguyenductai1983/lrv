@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('receipt.index') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.receipts.index') }}" method="get" role="form" class="form-inline">
                    @if(auth()->user()->role->admin)
                    <div class="form-group">
                        <input name="user_code" id="user_code" value="{{ request()->query('user_code')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('receipt.agency') }}">
                    </div>
                    @endif
                    <div class="form-group">
                        <input name="customer_phone" value="{{ request()->query('customer_phone')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('receipt.customer_phone') }}">
                    </div>
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <select name="service_type" id="service_type" class="form-control" style="width: 150px;">
                            <option value="">{{ __('receipt.service_type') }}</option>
                            <option value="1" @if(1 == request()->query('service_type')) selected="selected"@endif>TRANSPORT</option>
                            <option value="2" @if(2 == request()->query('service_type')) selected="selected"@endif>EXPRESS</option>
                            <option value="3" @if(3 == request()->query('service_type')) selected="selected"@endif>YHL</option>
                            <option value="4" @if(4 == request()->query('service_type')) selected="selected"@endif>QUOTE</option>
                            <option value="5" @if(5 == request()->query('service_type')) selected="selected"@endif>MTS</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <!-- <a href="{{ route('admin.receipts.excel') }}" class="btn btn-white btn-single">
                            {{ __('label.export_excel') }}
                        </a> -->
                        <button type="button" class="btn btn-white btn-single" onclick="exportExel();">
                         {{ __('label.export_excel') }}<i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </button>
                        <a href="{{ route('admin.receipts.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th width="160" class="text-center">{{ __('receipt.agency') }}</th>
                            <th width="120" class="text-center">{{ __('receipt.code') }}</th>
                            <th width="150" class="text-center">{{ __('receipt.goods_code') }}</th>
                            <th width="150" class="text-center">{{ __('receipt.service_type') }}</th>
                            <th width="160" class="text-center">{{ __('receipt.customer') }}</th>
                            <th width="160" class="text-center">{{ __('receipt.customer_phone') }}</th>
                            <th width="160" class="text-center">{{ __('receipt.date') }}</th>
                            <th width="150" class="text-center">{{ __('receipt.amount') }} (CAD)</th>
                            <th width="160" class="text-center">{{ __('receipt.agency_commission') }} (CAD)</th>
                            <th width="160" class="text-center">{{ __('receipt.paid_agency') }} (CAD)</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($receipts->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($receipts as $receipt)
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                            <td class="text-center">{{ $receipt->user_code }}</td>
                            <td class="text-center">
                                {{ $receipt->code }}
                            </td>
                            <td class="text-center">{{ $receipt->goods_code }}</td>
                            <td class="text-center">
                                @php if($receipt->mts_id > 0){@endphp
                                    MTS
                                @php }else{@endphp
                                    @php if($receipt->order_type == 1){@endphp
                                    TRANSPORT
                                    @php }else if($receipt->order_type == 2){@endphp
                                    EXPRESS
                                    @php }else if($receipt->order_type == 3){@endphp
                                    YHL
                                    @php }else if($receipt->order_type == 4){@endphp
                                    QUOTE
                                    @php }else{@endphp
                                    TRANSPORT
                                    @php }@endphp
                                @php }@endphp
                            </td>
                            <td class="text-center">{{ $receipt->customer_last_name . ' ' . $receipt->customer_middle_name . ' ' . $receipt->customer_first_name }}</td>
                            <td class="text-center">{{ $receipt->customer_telephone }}</td>
                            <td class="text-center">{{ $receipt->created_at }}</td>
                            <td class="text-center">@php if($receipt->type2 == 2){ echo '-'; } echo number_format($receipt->credit_amount, 2); @endphp</td>
                            <td class="text-center">@php if($receipt->type2 == 2){ echo '-'; } echo number_format($receipt->agency_discount, 2); @endphp</td>
                            <td class="text-center">@php if($receipt->type2 == 2){ echo '-'; } echo number_format($receipt->credit_amount - $receipt->agency_discount, 2); @endphp</td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        <tr>
                            <td colspan="10"><strong>{{ __('receipt.amount') }}: {{ number_format($total_amount, 2) }} CAD</strong></td>
                        </tr>
                        <tr>
                            <td colspan="10"><strong>{{ __('receipt.agency_discount') }}: {{ number_format($total_agency_discount, 2) }} CAD</strong></td>
                        </tr>
                        @else
                        <tr>
                            <td colspan="10">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $receipts->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
    $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', -60);
    $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', +1);
    });
    function exportExel() {
    var query = {
        from_date: $('#from_date').val(),
        to_date: $('#to_date').val(),
        service_type: $('#service_type').val(),
        user_code:$('#user_code').val()
    };
    var url = "/admin/receipts/excel?" + $.param(query);
   window.location.href =url;
   }
</script>
@endsection
