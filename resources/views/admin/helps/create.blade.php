@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('news.create') }}
    </div>
    <div class="panel-body">
        <form action="{{ route('admin.helps.store') }}" method="post" role="form" class="form-horizontal">
            @php $field='parent_id'; @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.parent') }}<i class="text-danger">*</i>
                </label>
                <div class="col-sm-10">
                    <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                        <option value="0">{{ __('page.root') }}</option>
                        @foreach($pages as $key1 => $page1)
                        <option value="{{ $page1->id }}">{{ $page1->translate->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @php
            $errorName=false;
            foreach(config('app.locales') as $locale => $localeName) {
            if ($errors->has('name_' . $locale)) {
            $errorName=true;
            break;
            }
            }
            @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.name') }}<i class="text-danger">*</i>
                </label>
                <div class="col-sm-10 tabs-border">
                    <ul class="nav nav-tabs">
                        @foreach(config('app.locales') as $locale => $localeName)
                        @php
                        $activeTab=false;
                        if ($errorName) {
                        if ($errors->has('name_' . $locale)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($locale == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <li class="{{ $activeTab ? 'active' : '' }}">
                            <a href="#tab-name-{{ $locale }}" data-toggle="tab">
                                <span class="visible-xs">{{ $locale }}</span>
                                <span class="hidden-xs">{{ $localeName }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach(config('app.locales') as $locale => $localeName)
                        @php
                        $activeTab=false;
                        if ($errorName) {
                        if ($errors->has('name_' . $locale)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($locale == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <div class="tab-pane{{ $activeTab ? ' active' : '' }}" id="tab-name-{{ $locale }}">
                            <div class="form-group{{ $errors->has('name_' . $locale) ? ' validate-has-error' : '' }}">
                                <input type="text" name="name_{{ $locale }}" class="form-control" value="{{ old('name_' . $locale) }}">
                                @if($errors->has('name_' . $locale))
                                <span>{{ $errors->first('name_' . $locale) }}</span>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            @php
            $errorDetail=false;
            foreach(config('app.locales') as $locale => $localeName) {
            if ($errors->has('detail_' . $locale)) {
            $errorDetail=true;
            break;
            }
            }
            @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.detail') }}
                </label>
                <div class="col-sm-10 tabs-border">
                    <ul class="nav nav-tabs">
                        @foreach(config('app.locales') as $locale => $localeName)
                        @php
                        $activeTab=false;
                        if ($errorDetail) {
                        if ($errors->has('detail_' . $locale)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($locale == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <li class="{{ $activeTab ? 'active' : '' }}">
                            <a href="#tab-detail-{{ $locale }}" data-toggle="tab">
                                <span class="visible-xs">{{ $locale }}</span>
                                <span class="hidden-xs">{{ $localeName }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach(config('app.locales') as $locale => $localeName)
                        @php
                        $activeTab=false;
                        if ($errorDetail) {
                        if ($errors->has('detail_' . $locale)) {
                        $activeTab=true;
                        }
                        } else {
                        if ($locale == config('app.locale')) {
                        $activeTab=true;
                        }
                        }
                        @endphp
                        <div class="tab-pane{{ $activeTab ? ' active' : '' }}" id="tab-detail-{{ $locale }}">
                            <textarea id="detail_{{ $locale }}" name="detail_{{ $locale }}" class="form-control" cols="30" rows="20"></textarea>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            @php $field='link_to'; @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('page.link_to') }}
                </label>
                <div class="col-sm-10">
                    <input type="text" name="{{ $field }}" class="form-control" value="">
                </div>
            </div>

            @php $field='order'; @endphp
            <div class="form-group">
                <label class="control-label col-sm-2">
                    {{ __('news.order') }}
                </label>
                <div class="col-sm-10">
                    <input type="text" name="{{ $field }}" class="form-control" value="">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12 text-right">
                    <a href="{{ route('admin.helps.index') }}" class="btn btn-white">
                        {{ __('label.cancel') }}
                    </a>
                    <button type="submit" class="btn btn-success disabled-submit">
                        <i class="fa fa-check"></i> {{ __('label.save') }}
                    </button>
                </div>
            </div>
            {{ csrf_field() }}
            {{ method_field('post') }}
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('components\ckeditor\ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('detail_vi', {
        filebrowserBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });
    CKEDITOR.replace('detail_en', {
        filebrowserBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('components/ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });
</script>
@endsection
