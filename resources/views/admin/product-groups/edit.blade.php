@extends('layouts.admin.index', ['menu' => $data['menu']])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('productGroup.edit') }}
        </div>
        <div class="panel-body">
            @php $group = $data['group']; @endphp
            <form action="{{ route('admin.product-groups.update', $group->id ) }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'service'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.service') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    @foreach(config('app.services') as $key => $service)
                                        <option value="{{ $key }}" @if($key == old($field, $group->service)) selected="selected"@endif>{{ $service }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('productGroup.code') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $group->code) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('productGroup.name') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $group->name) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'description'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('productGroup.description') }}
                            </label>
                            <div class="col-sm-9">
                                <textarea name="{{ $field }}" cols="30" rows="3" class="form-control">{{ old($field, $group->description) }}</textarea>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'thumbnail'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('productGroup.thumbnail') }}
                            </label>
                            <div class="col-sm-9">
                            @include('partials.form-controls.image', ['field' => 'thumbnail', 'file' => $group->thumbnails])                                
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>   
                </div>
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <a href="{{ route('admin.product-groups.index') }}" class="btn btn-white">
                            {{ __('label.cancel') }}
                        </a>
                        <button type="submit" class="btn btn-info disabled-submit">
                            <i class="fa fa-check"></i> {{ __('label.update') }}
                        </button>
                    </div>
                </div>
                {{ csrf_field() }}
                {{ method_field('put') }}
            </form>
        </div>
    </div>
@endsection