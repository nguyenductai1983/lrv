@extends('layouts.admin.print')

@section('body')
<form name="form1" method="post" action="" id="form1">
    <div id="pnin">
        <center>
            <span id="lbdata">
                <div class="page">
                    <div class="subpage" style="padding:15px">
                        <div style="min-height:17.8cm;max-height:17.8cm">
                            <div class="head">
                            <div class="a" style="float:left;width:88%;text-align:left;height:100px;"> </div>
                            <div style="width:98%;float:left;text-align:center;font-size:14pt;">DANH SÁCH HÓA ĐƠN CÒN NỢ TIỀN </div>
                            <div style="clear:both"></div>
                            </div>
                            <div>
                                <i><b>Nhóm phiếu thu:</b> {{ $voucher->code }}</i> - <b>ĐẠI LÝ:</b> {{ $voucher->user->first_name }}
                            </div>
                            <div style="clear:both"></div>
                            <table border="1px" cellspacing="0px" cellpadding="5px" width="100%" style="margin-top:10px">
                                <tbody>
                                    <tr align="center" style="background:#F1F1F1">
                                        <th style="width:20px">STT</th>
                                        <th style="width:100px">Mã hóa đơn</th>
                                        <th>Khách hàng</th>
                                        <th>Ngày</th>
                                        <th>Tổng tiền công ty thu</th>
                                        <th>Đã thanh toán</th>
                                        <th>Còn nợ</th>
                                    </tr>
                                    @if($mtss->count() > 0)
                                    <?php $no = 1; ?>
                                    @foreach($mtss as $mts)
                                    <?php $total_final = $mts->total_final - $mts->discount_agency_amount; ?>
                                    <tr align="center">
                                        <td>{{ $no }}</td>
                                        <td>{{ $mts->code }}</td>
                                        <td>{{ $mts->sender_full_name }}</td>
                                        <td>{{ $mts->created_date }}</td>
                                        <td>{{ number_format($total_final, 2) }}</td>
                                        <?php if($voucher->status > 0){ ?>
                                        <td>{{ number_format($total_final, 2) }}</td>
                                        <?php }else{ ?>
                                        <td>0</td>
                                        <?php } ?>
                                        <?php if($voucher->status > 0){ ?>
                                        <td>0</td>
                                        <?php }else{ ?>
                                        <td>{{ number_format($total_final, 2) }}</td>
                                        <?php } ?>
                                    </tr>
                                    <?php $no++; ?>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="7">{{ __('label.no_records') }}</td>
                                    </tr>
                                    @endif
                                    <tr align="center">
                                        <td></td>
                                        <td colspan="3"><b>Tổng cộng</b></td>
                                        <td><b>{{ number_format($voucher->amount - $voucher->discount_amount, 2) }}</b></td>
                                        <td><b>{{ number_format($voucher->paid_amount, 2) }}</b></td>
                                        <td><b>{{ number_format($voucher->remain_amount, 2) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <span id="lbthongbao"><script language="javascript">window.print();</script></span>
                </div>
            </span>
        </center>
    </div>
</form>
@endsection

