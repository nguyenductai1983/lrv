@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        <strong>{{ $page->slug }}</strong> {{ __('page.agencies') }}
    </div>
    <div class="panel-body">
        @include('partials.flash')
        @include('partials.errors')
        <form action="{{ route('admin.bulletin.updatepage', $page->id) }}" method="post" novalidate>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th class="text-center" width="10%">{{ __('label.select') }}</th>
                            <th>{{ __('agencies.name') }}</th>
                            <th>{{ __('agencies.description') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($agencies->count() > 0)
                        @php $agencisId = $page->agencies->pluck('id')->all(); @endphp
                        @php $no = 0; @endphp
                        @foreach($agencies as $agencie)
                        @php $no++; @endphp
                        <tr>
                            <td>{{ $no }}</td>
                            <td class="text-center">
                                @php $field = 'agencies[]'; @endphp
                                <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}"
                                    value="{{ $agencie->id }}" @if(in_array($agencie->id, old($field, $agencisId)))
                                checked="checked"@endif>
                            </td>
                            <td>{{ $agencie->name }}</td>
                            <td>{{ $agencie->description }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-right">
                <a href="{{ route('admin.pages.index') }}" class="btn btn-white">
                    <i class="fa fa-backward"></i> {{ __('label.go_back') }}
                </a>
                <button type="submit" class="btn btn-info disabled-submit">
                    <i class="fa fa-check"></i> {{ __('label.update') }}
                </button>
            </div>
            {{ method_field('put') }}
            {{ csrf_field() }}
        </form>
    </div>
</div>
@endsection
