@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('config.manage') }}
    </div>
    <div class="panel-body">
      @include('partials.flash')
      <form action="{{ route('admin.configs.update') }}" method="post" novalidate>
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
            <tr>
              <th class="text-center">#</th>
              <th>{{ __('config.item') }}</th>
              <th class="text-center">{{ __('config.percent') }}</th>
              <th class="text-right">{{ __('config.quota') }}</th>
              <th class="text-center">{{ __('config.agency_percent') }}</th>
              <th class="text-right">{{ __('config.agency_quota') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($configs as $config)
              <tr>
                <td class="text-center">{{ $config->id }}</td>
                <td>{{ __('config.item_' . $config->id) }}</td>
                <td>
                  @php $field = 'configs.' . $config->id . '.percent'; @endphp
                  <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                    <input type="text" name="configs[{{ $config->id }}][percent]" class="form-control text-center"
                           value="{{ old($field, $config->percent) }}">
                  </div>
                </td>
                <td>
                  @php $field = 'configs.' . $config->id . '.quota'; @endphp
                  <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                    <input type="text" name="configs[{{ $config->id }}][quota]" class="form-control text-center"
                           value="{{ old($field, $config->quota) }}">
                  </div>
                </td>
                <td>
                  @php $field = 'configs.' . $config->id . '.agency_percent'; @endphp
                  <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                    <input type="text" name="configs[{{ $config->id }}][agency_percent]" class="form-control text-center"
                           value="{{ old($field, $config->agency_percent) }}">
                  </div>
                </td>
                <td>
                  @php $field = 'configs.' . $config->id . '.agency_quota'; @endphp
                  <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                    <input type="text" name="configs[{{ $config->id }}][agency_quota]" class="form-control text-center"
                           value="{{ old($field, $config->agency_quota) }}">
                  </div>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <div class="text-right">
          <button type="submit" class="btn btn-info disabled-submit">
            <i class="fa fa-check"></i> {{ __('label.update') }}
          </button>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
    </div>
  </div>
@endsection