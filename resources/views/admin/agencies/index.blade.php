@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('agency.manage') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-1">
          <a href="{{ route('admin.agencies.create') }}" class="btn btn-success">
            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
          </a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
          <tr>
            <th class="text-center">{{ __('agency.code') }}</th>
            <th>{{ __('agency.name') }}</th>
            <th>{{ __('agency.address') }}</th>
            <th>{{ __('agency.phone') }}</th>
            <th>{{ __('agency.note') }}</th>
            <th>{{ __('label.action') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($agencies->count() > 0)
            @foreach($agencies as $agency)
              <tr>
                <td class="text-center">{{ $agency->code }}</td>
                <td>{{ $agency->name }}</td>
                <td>{{ $agency->address }}</td>
                <td>{{ $agency->phone }}</td>
                <td>{!! nl2br(e($agency->note)) !!}</td>
                <td>
                  <a href="{{ route('admin.agencies.products', $agency->id) }}" class="btn btn-xs btn-primary">
                    <i class="fa fa-cog"></i>
                  </a>
                  <a href="{{ route('admin.agencies.edit', $agency->id) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-edit"></i>
                  </a>
                  <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $agency->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                  <div class="modal fade" id="modal-delete-{{ $agency->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('admin.agencies.destroy', $agency->id) }}" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                          </div>
                          <div class="modal-body">
                            {!! __('label.confirm_delete_msg') !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                            <button type="submit" class="btn btn-danger disabled-submit">
                              <i class="fas fa-trash"></i> {{ __('label.delete') }}
                            </button>
                          </div>
                          {{ csrf_field() }}
                          {{ method_field('delete') }}
                        </form>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="6">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
        <div class="paginate-single">
            {{ $agencies->appends(request()->query())->links() }}
        </div>
    </div>
  </div>
@endsection
