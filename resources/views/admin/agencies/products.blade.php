@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('product.manage') }} - <strong>{{ $agency->name }}</strong>
    </div>
    <div class="panel-body">
        @include('partials.errors')
        @include('partials.flash')
        <form action="{{ route('admin.agencies.products', $agency->id) }}" method="post" novalidate>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">{{ __('product.code') }}</th>
                            <th>{{ __('product.name') }}</th>
                            <th>{{ __('product.sale_price') }}</th>
                            <th>{{ __('product.pickup_fee') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($agency->products->count() > 0)
                        @foreach($agency->products as $product)
                        <tr>
                            <td class="text-center">{{ $product->code }}</td>
                            <td>{{ $product->name }}</td>
                            <td>
                                @php $field = 'products.' . $product->id . '.price'; @endphp
                                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                                    <input type="number" class="form-control" name="products[{{ $product->id }}][price]"
                                           value="{{ old($field, $product->pivot->price) }}">
                                </div>
                            </td>
                            <td>
                                @php $field = 'products.' . $product->id . '.pickup_fee'; @endphp
                                <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}" style="margin-bottom: 0;">
                                    <input type="number" class="form-control" name="products[{{ $product->id }}][pickup_fee]"
                                           value="{{ old($field, $product->pivot->pickup_fee) }}">
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-right">
                <a href="{{ route('admin.agencies.index') }}" class="btn btn-white">
                    <i class="fa fa-backward"></i> {{ __('label.go_back') }}
                </a>
                <button type="submit" class="btn btn-info disabled-submit">
                    <i class="fa fa-check"></i> {{ __('label.update') }}
                </button>
            </div>
            {{ method_field('put') }}
            {{ csrf_field() }}
        </form>
    </div>
</div>
@endsection