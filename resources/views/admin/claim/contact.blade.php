@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('label.contact') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.claim.contact') }}" method="get" role="form" class="form-inline">
                    @php $field = 'full_name'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;"
                            value="{{ request()->query($field) }}" placeholder="{{ __('label.full_name') }}">
                    </div>
                    @php $field = 'phone_number'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;"
                            value="{{ request()->query($field) }}" placeholder="{{ __('label.phone_number') }}">
                    </div>
                    @php $field = 'email'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;"
                            value="{{ request()->query($field) }}" placeholder="{{ __('label.email') }}">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.claim.contact') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="3%">#</th>
                        <th width="10%">{{ __('label.full_name') }}</th>
                        <th width="8%">{{ __('label.phone_number') }}</th>
                        <th width="15%">{{ __('label.email') }}</th>
                        <th width="10%">{{ __('label.address') }}</th>
                        <th width="30%">{{ __('label.content') }}</th>
                        <th width="10%">{{ __('label.date') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @if($contacts->count() > 0)
                    @foreach($contacts as $contact)
                    <tr>
                        <td>{{ $no }}
                            <a href="{{ route('admin.claim.delete', $contact->id) }}"
                                class="btn btn-xs btn-danger">
                                <i class="fas fa-trash"></i>
                            </a></td>
                        <td>
                            {{ $contact->full_name }}
                        </td>
                        <td>{{ $contact->phone_number }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->address }}</td>
                        <td>{{ $contact->content }}</td>
                        <td>{{ $contact->created_at }}</td>
                    </tr>
                    @php $no++; @endphp
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $contacts->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection
