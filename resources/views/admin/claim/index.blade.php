@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('claim.list-claim') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('claim.frontend.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th width="150">{{ __('claim.request-code') }}</th>
                            <th width="160" class="text-center">{{ __('label.order_code') }}</th>
                            <th width="160" class="text-center">{{ __('claim.title') }}</th>
                            <th width="150" class="text-center">{{ __('label.create_time') }}</th>
                            <th width="120" class="text-center">{{ __('label.status') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($claims->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($claims as $claim)
                        <tr>
                            <td>
                                {{ $no }}
                            </td>
                            <td>
                                {{ $claim->code }}
                            </td>
                            <td class="text-center">{{ $claim->order->code }}</td>
                            <td class="text-center">{{ $claim->title }}</td>
                            <td class="text-center">{{ $claim->created_at }}</td>
                            <td class="text-center">
                                <span class="{{ $claim->status_label }}">{{ $claim->status_name }}</span>
                            </td>
                            <td>
                                <a href="{{ route('admin.claim.view', $claim->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($claim->status != 3 && $claim->status != 4)
                                <button type="button" class="btn btn-xs btn-info" onclick="claim.complete({{ $claim->id }})">
                                    <i class="fa fa-check"></i>
                                </button>
                                @endif
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $claims->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/admin/app/claim.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection