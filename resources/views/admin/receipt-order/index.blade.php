@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('receipt.index') }}
    </div>
    <div class="panel-body">
        @php if((auth()->user()->role->admin)){@endphp
        <div class="row form-group">
            <div class="col-sm-1">
                <a href="{{ route('admin.receipt-orders.create') }}" class="btn btn-success">
                    <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                </a>
            </div>
        </div>
        @php }@endphp
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.receipt-orders.index') }}" method="get" role="form" class="form-inline">
                    @php if(auth()->user()->role->admin){@endphp
                    <div class="form-group">
                        <select name="user_id" class="form-control">
                            <option value="">{{ __('receipt.option_agency') }}</option>
                            @if($users->count() > 0)
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}" @if($user->id == request()->query('user_id')) selected="selected"@endif>{{ $user->code }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    @php }@endphp
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.receipt-orders.excel') }}" class="btn btn-white btn-single">
                            <i class="fa fa-file"></i> {{ __('label.export_excel')}}
                        </a>
                        <a href="{{ route('admin.receipt-orders.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th width="160" class="text-center">{{ __('receipt.agency') }}</th>
                        <th width="120" class="text-center">{{ __('receipt.code') }}</th>
                        <th width="150" class="text-center">{{ __('receipt.amount') }}</th>
                        <th width="150" class="text-center">{{ __('receipt.agency_commission') }}</th>
                        <th width="160" class="text-center">{{ __('receipt.paid_amount') }}</th>
                        <th width="160" class="text-center">{{ __('receipt.remain_amount') }}</th>
                        <th width="160" class="text-center">{{ __('receipt.date') }}</th>
                        <th width="160" class="text-center">{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody id="shipment-package-items">
                    @if($receipts->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($receipts as $receipt)
                    <tr>
                        <td>
                            {{ $no }}
                        </td>
                        <td class="text-center">{{ $receipt->user_code }}</td>
                        <td class="text-center">
                            {{ $receipt->code }}
                        </td>
                        <td class="text-center">{{ number_format($receipt->amount, 2) }}</td>
                        <td class="text-center">
                            {{ number_format($receipt->discount_amount, 2) }}
                        </td>
                        <td class="text-center">{{ number_format($receipt->paid_amount, 2) }}</td>
                        <td class="text-center">{{ $receipt->remain_amount > 0 ? '- ' . number_format($receipt->remain_amount, 2) : 0 }}</td>
                        <td class="text-center">{{ $receipt->created_at }}</td>
                        <td>
                            <a href="{{ route('admin.receipt-orders.print-item', $receipt->id) }}" target="_blank" class="btn btn-xs btn-info">
                                <i class="fa fa-print "></i>
                            </a>
                            @php if(auth()->user()->role->admin){@endphp
                            <a href="{{ route('admin.receipt-orders.edit', $receipt->id) }}" class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                            <?php if($receipt->remain_amount > 0){?>
                            <button onclick="receipt.paid({{ $receipt->id }}, {{ $receipt->remain_amount }}, '{{ csrf_token() }}');" type="button" class="btn btn-xs btn-success">
                                <i class="fas fa-money-bill"></i>
                            </button>
                            <?php }?>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $receipt->id }}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="modal-delete-{{ $receipt->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('admin.receipt-orders.destroy', $receipt->id) }}" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! __('label.confirm_delete_msg') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                <button type="submit" class="btn btn-danger disabled-submit">
                                                    <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @php } @endphp
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    <tr>
                        <td colspan="10"><strong>{{ __('receipt.amount') }}: {{ number_format($total_amount, 2) }} CAD</strong></td>
                    </tr>
                    <tr>
                        <td colspan="10"><strong>{{ __('receipt.agency_discount') }}: {{ number_format($total_agency_discount, 2) }} CAD</strong></td>
                    </tr>
                    <tr>
                        <td colspan="10"><strong>{{ __('receipt.paid_amount') }}: {{ number_format($total_paid_amount, 2) }} CAD</strong></td>
                    </tr>
                    <tr>
                        <td colspan="10"><strong>{{ __('receipt.remain_amount') }}: {{ $total_remain_amount > 0 ? '- ' . number_format($total_remain_amount, 2) : 0 }} CAD</strong></td>
                    </tr>
                    @else
                    <tr>
                        <td colspan="10">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $receipts->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/receipt.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
