@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('receipt.create')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.receipt-orders.create') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <select name="user_id" class="form-control">
                            <option value="">{{ __('receipt.option_agency') }}</option>
                            @if($users->count() > 0)
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}" @if($user->id == request()->query('user_id')) selected="selected"@endif>{{ $user->code }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-white btn-single" type="submit">
                            <i class="fa fa-search"></i> {{ __('label.find')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <form id="receipt-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" style="min-width: 860px;">
                    <thead>
                        <tr>
                            <th width="10%">{{ __('label.no')}}</th>
                            <th width="5%">
                            <input type="checkbox" id="select-all" checked="" >
                            </th>
                            <th width="15%">{{ __('label.order_code')}}</th>
                             <th width="20%">{{ __('receipt.customer') }}</th>
                             <th width="12%">{{ __('receipt.date') }}</th>
                            <th width="10%">{{ __('receipt.amount') }} (CAD)</th>
                             <th width="10%">{{ __('receipt.paid_amount') }} (CAD)</th>
                            <th width="10%">{{ __('receipt.agency_commission') }} (CAD)</th>
                             <th width="10%">{{ __('receipt.remain_amount') }} (CAD)</th>
                            <th width="10%">{{ __('label.status')}}</th>
                        </tr>
                    </thead>
                    <tbody id="receipt-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr id="receipt-package-create-{{ $order->id }}">
                            <td> {{ $no }}</td>
                            <td>
                                <input type="checkbox" name="id" value="{{ $order->id }}" checked=""></td>
                            <td>{{ $order->code }}</td>
                            <td>{{ $order->sender_full_name }}</td>
                            <td>{{ date_format($order->created_at,'d/m/Y') }}</td>
                            <td class="text-right">{{ number_format($order->total_final, 2) }}</td>
                             <?php

                           if ( $order->total_paid_amount > $order->total_final)
                           {
                               $alertdanger="alert-danger";
                           }
                           else
                           {
                                 $alertdanger="";
                           }
                                     ?>
                             <td class="text-right  {{  $alertdanger }}">{{ number_format($order->total_paid_amount, 2) }}</td>
                            <td class="text-right">{{ number_format($order->agency_discount, 2) }}</td>
                              <?php
                            $total_Remain = $order->total_final-($order->total_final-$order->total_paid_amount)-$order->agency_discount;
                             $alertdangerRemain="";
                            if ($total_Remain >0) {
                           if ( $total_Remain > $order->total_final)
                           {
                               $alertdangerRemain="alert-danger";
                           }

                            }
                            else
                            {
                               $alertdangerRemain="alert-warning";
                            }
                             ?>
                            <td class="text-right {{ $alertdangerRemain }}">{{ number_format($total_Remain, 2) }}</td>
                            <td>
                                <span class="{{ $order->status_label }}">{{ $order->status_name }}</span>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr id="empty-item">
                            <td colspan="7">{{ __('label.no_records')}}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </form>
        <div class="text-right">
            <a href="{{ route('admin.receipt-orders.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <button type="button" class="btn btn-info" onclick="receipt.addItems('{{ route('admin.receipt-orders.index') }}');">
                <i class="fa fa-check"></i> {{ __('label.add')}}
            </button>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/receipt.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#receipt-package-items input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#receipt-package-items input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
