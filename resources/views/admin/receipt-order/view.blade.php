@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('receipt.view')}}: {{ $voucher->code }}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>{{ __('label.order_code')}}</th>
                        <th>{{ __('receipt.amount') }} (CAD)</th>
                        <th>{{ __('receipt.agency_discount') }} (CAD)</th>
                        <th>{{ __('label.status')}}</th>
                        <th>{{ __('label.action')}}</th>
                    </tr>
                </thead>
                <tbody id="receipt-package-items">
                    @if($orders->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($orders as $order)
                    <tr id="receipt-package-create-{{ $order->id }}">
                        <td>{{ $no }}</td>
                        <td>{{ $order->code }}</td>
                        <td class="text-right">{{ number_format($order->total_paid_amount, 2) }}</td>
                        <td class="text-right">{{ number_format($order->agency_discount, 2) }}</td>
                        <td>
                            <span class="{{ $order->status_label }}">{{ $order->status_name }}</span>
                        </td>
                        <td>
                            <button type="button" onclick="receipt.removeItem({{ $order->id }})" class="btn btn-xs btn-danger">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr id="empty-item">
                        <td colspan="7">{{ __('label.no_records')}}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="text-right">
            <a href="{{ route('admin.receipt-orders.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <a href="{{ route('admin.receipt-orders.excel-item', $voucher->id) }}" class="btn btn-white">
                <i class="fa fa-file"></i> {{ __('label.export_excel')}}
            </a>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/receipt.js"></script>
@endsection
