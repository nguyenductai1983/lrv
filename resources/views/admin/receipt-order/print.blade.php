@extends('layouts.admin.print')

@section('body')
<form name="form1" method="post" action="" id="form1">
    <div id="pnin">
        <center>
            <span id="lbdata">
                <div class="page">
                    <div class="subpage" style="padding:15px">
                        <div style="min-height:17.8cm;max-height:17.8cm">
                            <div class="head">
                            <div class="a" style="float:left;width:88%;text-align:left;height:100px;"> </div>
                            <div style="width:98%;float:left;text-align:center;font-size:14pt; text-transform:uppercase;">{{ __('receipt.index')}} </div>
                            <div style="clear:both"></div>
                            </div>
                            <div>
                                <i><b>{{ __('receipt.paid_group')}} :</b> {{ $voucher->code }}</i> - <b>{{ __('receipt.agency')}} :</b> {{ $voucher->user->first_name }}
                            </div>
                            <div style="clear:both"></div>
                            <table border="1px" cellspacing="0px" cellpadding="5px" width="100%" style="margin-top:10px">
                                <tbody>
                                    <tr align="center" style="background:#F1F1F1">
                                        <th style="width:20px">{{ __('receipt.no')}}</th>
                                        <th style="width:100px">{{ __('receipt.goods_code')}}</th>
                                        <th>{{ __('receipt.customer')}}</th>
                                        <th>{{ __('receipt.date')}}</th>
                                        <th>{{ __('receipt.amount')}}</th>
                                        <th>{{ __('receipt.agency_commission')}}</th>
                                        <th>{{ __('receipt.paid_amount')}}</th>
                                        <th>{{ __('receipt.remain_amount')}}</th>
                                    </tr>
                                    @if($orders->count() > 0)
                                    <?php $no = 1; ?>
                                    @foreach($orders as $order)
                                    <?php $total_final = $order->total_paid_amount - $order->agency_discount; ?>
                                    <tr align="center">
                                        <td>{{ $no }}</td>
                                        <td>{{ $order->code }}</td>
                                        <td>{{ $order->sender_full_name }}</td>
                                        <td>{{ $order->created_date }}</td>
                                        <td>{{ number_format($order->total_paid_amount, 2) }}</td>
                                        <td>{{ number_format($order->agency_discount, 2) }}</td>
                                        <?php if($voucher->status > 0){ ?>
                                        <td>{{ number_format($total_final, 2) }}</td>
                                        <?php }else{ ?>
                                        <td>0</td>
                                        <?php } ?>

                                        <?php if($voucher->status > 0){ ?>
                                        <td>0</td>
                                        <?php }else{ ?>
                                        <td>{{ number_format($total_final, 2) }}</td>
                                        <?php } ?>
                                    </tr>
                                    <?php $no++; ?>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="7">{{ __('label.no_records') }}</td>
                                    </tr>
                                    @endif
                                    <tr align="center">
                                        <td></td>
                                        <td colspan="3"><b>{{ __('receipt.total')}}</b></td>
                                        {{-- <td><b>{{ number_format($voucher->amount, 2) }}</b></td> --}}
                                        <td><b>{{ number_format($orders_total_final, 2) }}</b></td>
                                        <td><b>{{ number_format($orders_agency_discount, 2) }}</b></td>
                                        {{-- <td><b>{{ number_format($voucher->discount_amount, 2) }}</b></td> --}}
                                        <td><b>{{ number_format($voucher_paid_amount, 2) }}</b></td>
                                        <td><b>{{ number_format($voucher_remain_amount, 2) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <span id="lbthongbao"><script language="javascript">window.print();</script></span>
                </div>
            </span>
        </center>
    </div>
</form>
@endsection

