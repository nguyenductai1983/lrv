@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('log.manage') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('admin.logs.index') }}" method="get" role="form" class="form-inline">
                    @php $field = 'order_id'; @endphp
                    <div class="form-group">
                        <input type="text" name="{{ $field }}" class="form-control" style="width: 150px;" value="{{ request()->query($field) }}" placeholder="{{ __('log.order_id') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('admin.campaign.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered" style="min-width: 1500px;">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="10%">{{ __('log.order_id') }}</th>
                        <th width="10%">{{ __('log.transport_id') }}</th>
                        <th width="10%">{{ __('log.mts_id') }}</th>
                        <th width="10%">{{ __('log.user_id') }}</th>
                        <th width="45%">{{ __('log.body') }}</th>
                        <th width="10%">{{ __('log.created_at') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($actionLogs->count() > 0)
                    <?php $no = 1;?>
                    @foreach($actionLogs as $actionLog)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $actionLog->order_id }}</td>
                        <td>{{ $actionLog->transport_id }}</td>
                        <td>{{ $actionLog->mts_id }}</td>
                        <td>{{ $actionLog->user_id }}</td>
                        <td>{{ $actionLog->body }}</td>
                        <td>{{ $actionLog->created_at }}</td>
                    </tr>
                    <?php $no ++;?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $actionLogs->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection