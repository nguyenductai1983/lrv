@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('role.manage') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-1">
          <a href="{{ route('admin.roles.create') }}" class="btn btn-success">
            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
          </a>
        </div>
      </div>
      @include('partials.flash')
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
          <tr>
            <th>{{ __('role.name') }}</th>
            <th>{{ __('role.description') }}</th>
            <th>{{ __('label.action') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($roles->count() > 0)
            @foreach($roles as $role)
              <tr>
                <td>{{ $role->name }}</td>
                <td>{!! nl2br(e($role->description)) !!}</td>
                <td>
                  <a href="{{ route('admin.roles.permissions', $role->id) }}" class="btn btn-xs btn-primary">
                    <i class="fa fa-cog"></i>
                  </a>
                  <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-edit"></i>
                  </a>
                  <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $role->id }}">
                    <i class="fas fa-trash"></i>
                  </button>
                  <div class="modal fade" id="modal-delete-{{ $role->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('admin.roles.destroy', $role->id) }}" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                          </div>
                          <div class="modal-body">
                            {!! __('label.confirm_delete_msg') !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                            <button type="submit" class="btn btn-danger disabled-submit">
                              <i class="fas fa-trash"></i> {{ __('label.delete') }}
                            </button>
                          </div>
                          {{ csrf_field() }}
                          {{ method_field('delete') }}
                        </form>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="4">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
