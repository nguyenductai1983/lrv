@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      <strong>{{ $role->name }}</strong> - {{ __('role.permissions') }}
    </div>
    <div class="panel-body">
      @include('partials.flash')
      @include('partials.errors')
      <form action="{{ route('admin.roles.permissions', $role->id) }}" method="post" novalidate>
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
            <tr>
              <th width="50">#</th>
              <th class="text-center" width="10%">{{ __('label.select') }}</th>
              <th>{{ __('permission.name') }}</th>
              <th>{{ __('permission.description') }}</th>
            </tr>
            </thead>
            <tbody>
            @if($permissions->count() > 0)
              @php $rolePermissionsId = $role->permissions->pluck('id')->all(); @endphp
              @php $no = 0; @endphp
              @foreach($permissions as $permission)
               @php $no++; @endphp
                <tr>
                  <td>{{ $no }}</td>
                  <td class="text-center">
                    @php $field = 'permissions[]'; @endphp
                    <input type="checkbox" class="iswitch iswitch-info" name="{{ $field }}" value="{{ $permission->id }}"
                           @if(in_array($permission->id, old($field, $rolePermissionsId))) checked="checked"@endif>
                  </td>
                  <td>{{ $permission->name }}</td>
                  <td>{{ $permission->description }}</td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="3">{{ __('label.no_records') }}</td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>
        <div class="text-right">
          <a href="{{ route('admin.roles.index') }}" class="btn btn-white">
            <i class="fa fa-backward"></i> {{ __('label.go_back') }}
          </a>
          <button type="submit" class="btn btn-info disabled-submit">
            <i class="fa fa-check"></i> {{ __('label.update') }}
          </button>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
    </div>
  </div>
@endsection