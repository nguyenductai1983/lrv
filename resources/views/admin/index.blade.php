@extends('layouts.admin.index', ['menu' => $menu ?? ''])

@section('body')
<div class="panel panel-default">
    @php $first = "in";
    $btn="btn-success";
    @endphp
    @foreach($pages as $page)
    <button type="button" id='btn' class="btn {{ $btn, $btn=""}}" data-toggle="collapse" aria-expanded data-target=#{{ $page->id }}>
        {!! $page->translate->name !!}</button>
    <div id={{ $page->id }} class='collapse {{ $first, $first=""}}'>
        {!! $page->translate->detail !!}
    </div>
    <br>
    @endforeach
    <div class="paginate-single">
        {{ $pages->appends(request()->query())->links() }}
    </div>
</div>
@php if(isset(auth()->user()->role->admin) && auth()->user()->role->admin){@endphp
<div id="dashboard-view" ng-app="DashboardApp" ng-controller="DashboardController">
    <div class="panel panel-default">
        <div class="panel-heading">
            Transport
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('order.type') }}</th>
                            <th>{{ __('order.status_new') }}</th>
                            <th>{{ __('order.status_processing') }}</th>
                            <th>{{ __('order.status_cancel') }}</th>
                            <th>{{ __('order.status_complete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ __('transport.menu_agency_order') }}</td>
                            <td ng-bind="transport.order.create.toFixed(0)"></td>
                            <td ng-bind="transport.order.processing.toFixed(0)"></td>
                            <td ng-bind="transport.order.cancel.toFixed(0)"></td>
                            <td ng-bind="transport.order.completed.toFixed(0)"></td>
                        </tr>
                        <tr>
                            <td>{{ __('transport.menu_customer_quote') }}</td>
                            <td ng-bind="transport.quote.create.toFixed(0)"></td>
                            <td ng-bind="transport.quote.processing.toFixed(0)"></td>
                            <td ng-bind="transport.quote.cancel.toFixed(0)"></td>
                            <td ng-bind="transport.quote.completed.toFixed(0)"></td>
                        </tr>
                        <tr>
                            <td>{{ __('transport.menu_customer_order') }}</td>
                            <td ng-bind="transport.orderQuote.create.toFixed(0)"></td>
                            <td ng-bind="transport.orderQuote.processing.toFixed(0)"></td>
                            <td ng-bind="transport.orderQuote.cancel.toFixed(0)"></td>
                            <td ng-bind="transport.orderQuote.completed.toFixed(0)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            YHL
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('order.type') }}</th>
                            <th>{{ __('order.status_new') }}</th>
                            <th>{{ __('order.status_processing') }}</th>
                            <th>{{ __('order.status_cancel') }}</th>
                            <th>{{ __('order.status_complete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ __('yhl.menu_agency_order') }}</td>
                            <td ng-bind="yhl.order.create.toFixed(0)"></td>
                            <td ng-bind="yhl.order.processing.toFixed(0)"></td>
                            <td ng-bind="yhl.order.cancel.toFixed(0)"></td>
                            <td ng-bind="yhl.order.completed.toFixed(0)"></td>
                        </tr>
                        <tr>
                            <td>{{ __('yhl.menu_customer_order') }}</td>
                            <td ng-bind="yhl.quote.create.toFixed(0)"></td>
                            <td ng-bind="yhl.quote.processing.toFixed(0)"></td>
                            <td ng-bind="yhl.quote.cancel.toFixed(0)"></td>
                            <td ng-bind="yhl.quote.completed.toFixed(0)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            Express
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('order.type') }}</th>
                            <th>{{ __('order.status_new') }}</th>
                            <th>{{ __('order.status_processing') }}</th>
                            <th>{{ __('order.status_cancel') }}</th>
                            <th>{{ __('order.status_complete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ __('express.menu_agency_order') }}</td>
                            <td ng-bind="express.order.create.toFixed(0)"></td>
                            <td ng-bind="express.order.processing.toFixed(0)"></td>
                            <td ng-bind="express.order.cancel.toFixed(0)"></td>
                            <td ng-bind="express.order.completed.toFixed(0)"></td>
                        </tr>
                        <tr>
                            <td>{{ __('express.menu_customer_order') }}</td>
                            <td ng-bind="express.quote.create.toFixed(0)"></td>
                            <td ng-bind="express.quote.processing.toFixed(0)"></td>
                            <td ng-bind="express.quote.cancel.toFixed(0)"></td>
                            <td ng-bind="express.quote.completed.toFixed(0)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            MTS
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('order.type') }}</th>
                            <th>{{ __('order.status_new') }}</th>
                            <th>{{ __('order.status_processing') }}</th>
                            <th>{{ __('order.status_cancel') }}</th>
                            <th>{{ __('order.status_complete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ __('transport.menu_agency_order') }}</td>
                            <td ng-bind="mts.order.create.toFixed(0)"></td>
                            <td ng-bind="mts.order.processing.toFixed(0)"></td>
                            <td ng-bind="mts.order.cancel.toFixed(0)"></td>
                            <td ng-bind="mts.order.completed.toFixed(0)"></td>
                        </tr>
                        <tr>
                            <td>{{ __('transport.menu_customer_quote') }}</td>
                            <td ng-bind="mts.quote.create.toFixed(0)"></td>
                            <td ng-bind="mts.quote.processing.toFixed(0)"></td>
                            <td ng-bind="mts.quote.cancel.toFixed(0)"></td>
                            <td ng-bind="mts.quote.completed.toFixed(0)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('home.header-manager-quote') }}
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('order.status_new') }}</th>
                            <th>{{ __('order.status_processing') }}</th>
                            <th>{{ __('order.status_cancel') }}</th>
                            <th>{{ __('order.status_complete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td ng-bind="quote.create.toFixed(0)"></td>
                            <td ng-bind="quote.processing.toFixed(0)"></td>
                            <td ng-bind="quote.cancel.toFixed(0)"></td>
                            <td ng-bind="quote.completed.toFixed(0)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@php }@endphp
@endsection

@section('footer')
@php if(isset(auth()->user()->role->name) && strtoupper(auth()->user()->role->name) == "ADMIN"){@endphp
<script
    src="{{ asset('js/admin/app/dashboard.js?t=' . File::lastModified(public_path('js/admin/app/dashboard.js'))) }}">
</script>
<script>
    $(function () {
            var scope = angular.element('#dashboard-view').scope();

            scope.init();

            if (!scope.$$phase) {
                scope.$apply();
            }
        });
</script>
@php }@endphp
@endsection
