@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('currency.edit') }}
    </div>
    <div class="panel-body">
      <form action="{{ route('admin.currencies.update', $currency->id) }}" method="post" role="form" class="form-horizontal">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-2">
            @php $field = 'code'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('currency.code') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $currency->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            @php $field = 'name'; @endphp
            <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
              <label for="{{ $field }}" class="control-label col-sm-4">
                {{ __('currency.name') }}<i class="text-danger">*</i>
              </label>
              <div class="col-sm-8">
                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $currency->{$field}) }}">
                @if($errors->has($field))
                  <span class="validate-has-error">{{ $errors->first($field) }}</span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-info disabled-submit">
                  <i class="fa fa-check"></i> {{ __('label.update') }}
                </button>
                <a href="{{ route('admin.currencies.index') }}" class="btn btn-white">
                  {{ __('label.cancel') }}
                </a>
              </div>
            </div>
          </div>
        </div>
        {{ method_field('put') }}
        {{ csrf_field() }}
      </form>
    </div>
  </div>
@endsection