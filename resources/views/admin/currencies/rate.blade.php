@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('currency.rate_manage') }} <strong>{{ $currency->name }}</strong>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
          <tr>
            <th class="text-center">{{ __('currency.code') }}</th>
            <th>{{ __('currency.rate') }}</th>
            <th>{{ __('currency.created_at') }}</th>
          </tr>
          </thead>
          <tbody>
          @if($rates->count() > 0)
            @foreach($rates as $rate)
              <tr>
                <td class="text-center">{{ $rate->toCurrencys->code }}</td>
                <td>{{ $rate->rate }}</td>
                <td>{{ $rate->created_at }}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="4">{{ __('label.no_records') }}</td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection