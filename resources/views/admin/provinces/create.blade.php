@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('province.create') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('admin.provinces.store') }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'country_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('province.country_id') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="">{{ __('province.select_country') }}</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" @if($country->id == old($field)) selected="selected"@endif>
                                            {{ $country->code . '-'. $country->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('province.name') }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'postal_code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-3">
                                {{ __('label.post_code') }}
                            </label>
                            <div class="col-sm-9">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field) }}">
                                @if($errors->has($field))
                                    <span class="validate-has-error">{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <a href="{{ route('admin.provinces.index') }}" class="btn btn-white">
                            {{ __('label.cancel') }}
                        </a>
                        <button type="submit" class="btn btn-info disabled-submit">
                            <i class="fa fa-check"></i> {{ __('label.save') }}
                        </button>
                    </div>
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection
