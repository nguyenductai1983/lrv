@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('province.manage') }}
        </div>
        <div class="panel-body">
            <div class="row form-group">
                <div class="col-sm-1">
                    <a href="{{ route('admin.provinces.create') }}" class="btn btn-success">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                    </a>
                </div>
                <div class="col-sm-3">
                    <form action="{{ route('admin.provinces.index') }}" method="get">
                        @php $field = 'country_id'; @endphp
                        <select id="{{ $field }}" name="{{ $field }}" class="form-control" onchange="this.form.submit()">
                            <option value="">{{ __('province.select_country') }}</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @if(request()->query($field) == $country->id) selected="selected"@endif>
                                    {{ $country->code . '-' . $country->name }}
                                </option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('province.country_id') }}</th>
                        <th>{{ __('province.name') }}</th>
                        <th>{{ __('label.post_code') }}</th>
                        <th>{{ __('label.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($provinces->count() > 0)
                        @foreach($provinces as $province)
                            <tr>
                                <td>{{ $province->country_name }}</td>
                                <td>{{ $province->name }}</td>
                                <td>{{ $province->postal_code }}</td>
                                <td>
                                    <a href="{{ route('admin.provinces.edit', $province->id) }}" class="btn btn-xs btn-warning">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $province->id }}">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                    <div class="modal fade" id="modal-delete-{{ $province->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form action="{{ route('admin.provinces.destroy', $province->id) }}" method="post">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        {!! __('label.confirm_delete_msg') !!}
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                        <button type="submit" class="btn btn-danger disabled-submit">
                                                            <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                        </button>
                                                    </div>
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">{{ __('label.no_records') }}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $provinces->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
@endsection
