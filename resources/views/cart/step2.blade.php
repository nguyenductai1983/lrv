@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/checkout.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javacript:void(0);">{{ __('home.cart') }}</a></li>
        </ol>
    </div>
</div>

<div class="container steps-order-page page">
    <form id="cart-form-step2" class="order-profile-block">
        <section class="order-cart-block">
            <div class="block-head">
                <div class="block-title">
                    {{ __('home.cart-step2-title') }}
                </div>
            </div>
            <div class="block-main">
                <div class="profile-tb">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tb-head">{{ __('home.cart-buyer') }}</div>
                            <div class="tb-body buyer-profile-wrap">
                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-user"></i></div>
                                        <input type="text" placeholder="{{ __('home.fullname') }}" class="input-2" name="BuyerName" value="{{ isset($customer->full_name) ? $customer->full_name: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-phone"></i></div>
                                        <input type="text" placeholder="{{ __('home.phone-number') }}" class="input-2" name="BuyerPhone" value="{{ isset($customer->telephone) ? $customer->telephone: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-envelope"></i></div>
                                        <input type="email" placeholder="{{ __('home.email') }}" class="input-2" name="BuyerEmail" value="{{ isset($customer->email) ? $customer->email: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-send"></i></div>
                                        <input type="text" placeholder="{{ __('home.address') }}" class="input-2" name="BuyerAddress" value="{{ isset($customer->address_1) ? $customer->address_1: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="BuyerCountryId" onchange="cart.loadProvince(true)" required="">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="BuyerProvinceId" onchange="cart.loadDistrict(true)" required="">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="BuyerDistrictId" required="">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <label class="control-radio-regiser control-radio-1">
                                    <div class="indicator">
                                        * {{ __('home.cart-step2-notifi') }}<br>
                                    </div>
                                    <div class="clearfix"></div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tb-head">
                                {{ __('home.cart-receiver')}}
                                <!-- <div class="pull-left">{{ __('home.cart-receiver')}}</div>
                                <div class="pull-right">
                                    <label class="control-radio-1">
                                        <input type="checkbox" class="buyer-profile-cb" name="ReceiverInfo">
                                        <div class="indicator" id="receiverInfo">{{ __('home.receiver-other-buyer')}}</div>
                                    </label>
                                </div><div class="clearfix"></div> -->
                            </div>
                            <div class="tb-body receiver-profile-wrap is-disable">
                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-user"></i></div>
                                        <input type="text" placeholder="{{ __('home.fullname') }}" class="input-2" name="ReceiverName" value="{{ isset($customer->full_name) ? $customer->full_name: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-phone"></i></div>
                                        <input type="text" placeholder="{{ __('home.phone-number') }}" class="input-2" name="ReceiverPhone" value="{{ isset($customer->telephone) ? $customer->telephone: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-envelope"></i></div>
                                        <input type="email" placeholder="{{ __('home.email') }}" class="input-2" name="ReceiverEmail" value="{{ isset($customer->email) ? $customer->email: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-send"></i></div>
                                        <input type="text" placeholder="{{ __('home.address') }}" class="input-2" name="ReceiverAddress" value="{{ isset($customer->address_1) ? $customer->address_1: '' }}" required="">
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="ReceiverCountryId" onchange="cart.loadProvince(false)" required="">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="ReceiverProvinceId" onchange="cart.loadDistrict(false)" required="">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="form-group-1">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="ReceiverDistrictId" required="" onchange="cart.loadWard(false);">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="form-group-1" id="ward">
                                    <div class="form-control-2">
                                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                                        <select name="ReceiverWardId" required="">
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-foot">
                <a class="btn btn-default" href="{{ route('cart.step1') }}"><i class="fa fa-angle-double-left"></i> {{ __('home.cart') }}</a>
                <button class="btn btn-danger" type="button" onclick="cart.step2();">
                    <span class="hidden-sm">{{ __('home.step3-title') }}: </span>{{ __('home.payment') }} <i class="fa fa-angle-double-right"></i>
                </button>
            </div>
        </section>
        {{ csrf_field() }}
    </form>
    <section class="order-cart-block">
        <div class="block-main">
            <div class="table-responsive">
                <table class="cart-tb">
                    <thead>
                        <tr>
                            <th style="width: 40px;">{{ __('home.cart-no') }}</th>
                            <th style="min-width: 200px;">{{ __('home.cart-product-info') }}</th>
                            <th style="width: 130px;">{{ __('home.cart-product-amount') }}</th>
                            <th style="width: 90px;">{{ __('home.cart-product-quantity') }}</th>
                            <th style="width: 230px;">{{ __('home.cart-product-totalamount') }}</th>
                        </tr>
                    </thead>
                    <tbody id="cart-step1-content">
                        @php $no = 1; $total = 0; @endphp
                        @foreach($cart as $item)
                        @php $subtotal = $item->price * $item->qty; $total += $subtotal; @endphp
                        <tr id="product-item-{{ $item->rowId }}" class="product-item">
                            <td class="order">{{ $no }}</td>
                            <td class="product-info">
                                <a target="_blank" href="{{ $item->options['url'] }}" class="img">
                                    <img alt="{{ $item->name }}" src="{{ $item->options['image'] }}">
                                </a>
                                <div class="info-wrap">
                                    <div class="title">
                                        <a target="_blank" href="{{ $item->options['url'] }}">{{ $item->name }}</a>
                                    </div>
                                    <p><b class="text-red"><ins>{{ __('home.product-shopping') }}</ins></b>: </p>
                                    <p>{{ __('home.product-code') }}: <span class="text-blue">{{ $item->options['code'] }}</span></p>

                                </div>
                            </td>

                            <td class="product-price">
                                <span id="item-price-{{ $item->rowId }}" class="item-final-amount price">{{ number_format($item->price, 2, '.', ',') }} CAD</span>
                            </td>
                            <td class="quantity">
                                <input required="" type="number" class="form-control inputQuantity" min="1" maxlength="2" name="quantity" value="{{ $item->qty }}" disabled>
                            </td>
                            <td class="product-price">
                                <span id="item-subprice-{{ $item->rowId }}" class="item-final-amount price">{{ number_format($subtotal, 2, '.', ',') }} CAD</span>
                            </td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                        <tr>
                            <td colspan="4" class="guide-text">{{ __('home.total-final') }}</td>
                            <td id="total-price" class="total-price order-total">{{ number_format($total, 2, '.', ',') }} CAD</td>
                        </tr>
                        <tr class="price-show">
                            <td colspan="4" class="guide-text">{{ __('home.fee-shipping') }}</td>
                            <td id="shipping-price" class="total-price">0 CAD</td>
                        </tr>
                        <tr class="price-show">
                            <td colspan="4" class="guide-text">{{ __('home.total-payment') }}</td>
                            <td id="total-final" class="total-price">{{ number_format($total, 2, '.', ',') }} CAD</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer-js')
<script type="text/javascript" src="{{ asset('js/home/cart.js') }}"></script>
<script type="text/javascript">
    var customer = '';
    <?php if(!empty($customer)){?>
        var customer = {!! $customer !!};
    <?php }?>
    var countries = {!! $countries !!};
    cart.init();
</script>
@endsection
