@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/checkout.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('home.payment') }}</a></li>
        </ol>
    </div>
</div>

<div class="container steps-order-page" id="cart-form-step3">
    <section class="order-cart-block">
        <div class="block-head">
            <div class="block-title">{{ __('home.cart-step3') }}</div>
        </div>
        <div class="block-main">
            <div class="table-responsive">
                <div class="order-cart-form">
                    <table class="cart-tb">
                        <thead>
                            <tr>
                                <th style="width: 40px;">{{ __('home.cart-no') }}</th>
                                <th style="min-width: 200px;">{{ __('home.cart-product-info') }}</th>
                                <th style="width: 130px;">{{ __('home.cart-product-amount') }}</th>
                                <th style="width: 90px;">{{ __('home.cart-product-quantity') }}</th>
                                <th style="width: 230px;">{{ __('home.cart-product-totalamount') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach($order->order_items as $item)
                            <tr class="product-item">
                                <td class="order">{{ $no }}</td>
                                <td class="product-info">
                                    <a target="_blank" href="{{ $item->product->frontend_url }}" class="img">
                                        <img alt="{{ $item->product->name }}" src="{{ $item->product->thumbnail }}">
                                    </a>
                                    <div class="info-wrap">
                                        <div class="title">
                                            <a target="_blank"
                                                href="{{ $item->product->frontend_url }}">{{ $item->product->name }}</a>
                                        </div>
                                        <p><b class="text-red"><ins>{{ __('home.product-shopping') }}</ins></b>: </p>
                                        <p>{{ __('home.product-code') }}: <span
                                                class="text-blue">{{ $item->product->code }}</span></p>
                                    </div>
                                </td>
                                <td class="product-price">
                                    <span
                                        class="item-final-amount price">{{ number_format($item->unit_goods_fee, 2, '.', ',') }}
                                        CAD</span>
                                </td>
                                <td class="quantity">
                                    <input required="" type="number" class="form-control inputQuantity" name="quantity"
                                        value="{{ $item->quantity }}" disabled="">
                                </td>
                                <td class="product-price">
                                    <span
                                        class="item-final-amount price">{{ number_format($item->sub_total_goods, 2, '.', ',') }}
                                        CAD</span>
                                </td>
                            </tr>
                            @php $no++; @endphp
                            @endforeach
                            <!-- <tr>
                                <td colspan="4" class="guide-text">{{ __('label.coupon_code') }}</td>
                                <td class="total-price">
                                    <form id="coupon-form" method="POST">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="coupon_code" value="{{ $order->coupon_code }}">
                                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                                            {{ csrf_field() }}
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info" id="bnt-usecoupon" style="<?php if(!empty($order->coupon_code)){ ?>display:none<?php }else{ ?>display:block<?php } ?>" onclick="cart.applyCoupon()">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger" id="bnt-removecoupon" style="<?php if(!empty($order->coupon_code)){ ?>display:block<?php }else{ ?>display:none<?php } ?>" onclick="cart.removeCoupon()">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
								</td>
                            </tr>-->
                            <tr>
                                <td colspan="4" class="guide-text">{{ __('label.coupon_amount') }}</td>
                                <td class="total-price" id="coupon-amount">
                                    {{ number_format($order->coupon_amount, 2, '.', ',') }} CAD</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="guide-text">{{ __('home.total-final') }}</td>
                                <td class="total-price">{{ number_format($order->total_goods_fee, 2, '.', ',') }} CAD
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="guide-text">{{ __('home.fee-shipping') }}</td>
                                <td class="total-price">{{ number_format($order->shipping_fee, 2, '.', ',') }} CAD</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="guide-text text-red"><b>{{ __('home.total-payment') }}</b></td>
                                <td class="total-price" id="total-final">
                                    {{ number_format($order->total_final, 2, '.', ',') }} CAD</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <form id="payment-form" class="type-payment-form">
        <section class="order-payment-type-block">
            <div class="block-head">
                <div class="block-title">{{ __('home.select-payment-option') }}</div>
            </div>
            <div class="block-main">
                <div class="row">
                    <div class="col-1 col-sm-4 col-md-3">
                        <label class="pt-radio-control">
                            <input type="radio" class="pt-rad paymentMethod" name="paymentMethod"
                                data-tab-target="#payment-type-4" checked="">
                            <div class="indicator">
                                <div class="icon">
                                    <img src="{{ asset('images/bank/icon-bank-money.png')}}" alt="">
                                </div>
                                <div class="title">
                                    <p>{{ __('home.payment-transfer') }}<br>{{ __('home.via-bank') }}</p>
                                </div>
                            </div>
                        </label>
                        <!-- <label class="pt-radio-control">
                            <input type="radio" class="pt-rad paymentMethod" name="paymentMethod" data-tab-target="#payment-type-5">
                            <div class="indicator">
                                <div class="icon"><img src="{{ asset('images/bank/payment-home.svg')}}" alt=""></div>
                                <div class="title">
                                    <p>{{ __('home.payment-code') }}
                                </div>
                            </div>
                        </label> -->
                    </div>
                    <div class="col-2 col-sm-8 col-md-9">
                        <div class="col-main">
                            <div id="payment-type-4" class="payment-type-wrap" style="display: block;">
                                <div style="padding-bottom: 15px;">
                                    <strong class="text-danger">{{ __('home.transaction-transfer') }}</strong>,
                                    {{ __('home.select-bank-payment') }}:
                                </div>
                                <div class="row">
                                    @foreach($payment_methods as $payment_method)
                                    <div class="col-xs-6 col-sm-6">
                                        <label class="bank-radio-control ttip"
                                            onclick="cart.chooseMethod('{{ $payment_method->code }}')">
                                            <input type="radio" name="bankID" @php if($order->payment_method =
                                            "{{ $payment_method->code }}"){ @endphp checked="" @php } @endphp>
                                            <div class="indicator">
                                                <img
                                                    src="{{ isset($payment_method->file->path) && !empty($payment_method->file->path) ? $payment_method->file->path : ''}}">
                                            </div>
                                            <div class="title">{{ $payment_method->name }}</div>
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                                {{-- asd --}}

                                {{-- dfgsd --}}


                            </div>
                            <!-- <div id="payment-type-5" class="payment-type-wrap">
                                <div style="padding-bottom: 15px;">
                                    <strong class="text-danger">{{ __('home.transaction-cod') }}</strong>, {{ __('home.select-cod-payment') }}:
                                </div>
                            </div> -->
                        </div>
                        <div class="col-foot">
                            <div class="pp-type-items-wrap">
                                <div class="pp-type-item">
                                    <label class="control-radio-1">
                                        <textarea class="form-control" type="text" cols="100"
                                            placeholder="{{ __('home.customer-note') }}" name="CustomerNote"></textarea>
                                    </label>
                                </div>
                            </div>

                            <div class="choose-rule-wrap">
                                <label class="control-radio-1">
                                    <input type="checkbox" name="Agree">
                                    <div class="indicator blinker">{{ __('home.agree') }}</div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-foot">
                <input type="hidden" name="payment_method" value="{{ $order->payment_method }}">
                <input type="hidden" name="order_code" value="{{ $order->code }}">
                <button class="btn btn-danger" type="button" onclick="cart.step3();">{{ __('home.complete') }}</button>
            </div>
        </section>
        {{ csrf_field() }}
    </form>
</div>
@endsection
@section('footer-js')
<script type="text/javascript" src="{{ asset('js/home/cart.js') }}"></script>
@endsection
