@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/checkout.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('home.invoice') }}</a></li>
        </ol>
    </div>
</div>

<div class="container steps-order-page" id="cart-form-step4">
    <section class="order-cart-block">
        <div class="block-head"><div class="block-title">{{ __('home.invoice-purchase') }}</div></div>
        <div class="block-main">
            <div class="ck-head">
                <div class="row">
                    <div class="col-md-8 left">
                        <p><b>iGreen:</b> {{ __('home.sologan') }}</p>
                        <p><b>{{ __('home.headquarters') }}:</b> {{ __('home.headquarters-val') }}</p>
                        <p><b>{{ __('home.hotline') }}:</b> 1900 1090</p>
                        <p><b>{{ __('home.email') }}:</b> info@igreen.com</p>
                    </div>
                    <div class="col-md-4 right">
                        <p>{{ __('home.create-at') }}: {{ $order->created_at }}</p>
                        <p>{{ __('home.order-code') }}: <strong class="text-danger">{{ $order->code }}</strong></p>
                        <p>{{ __('home.payment-method') }}: <strong>{{ $order->payment_method }}</strong></p>
                        <p>{{ __('home.status') }}: <strong class="{{ $order->status_class }}">{{ $order->status_name }}</strong></p>
                    </div>
                </div>
            </div>
            <div class="order-info">
                <div class="top">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>{{ __('home.invoice-info-receiver') }}</h4>
                            <p>{{ __('home.fullname') }}: <strong>{{ $order->receive_first_name }}</strong></p>
                            <p>{{ __('home.address') }}: {{ $order->receiver_address }}, {{ $order->receiver_city->name }}, {{ $order->receiver_province->name }}</p>
                            <p>{{ __('home.phone-number') }}: {{ $order->receiver_phone }}</p>
                            <p>{{ __('home.email') }}: {{ $order->receiver_email }}</p>
                        </div>
                        <div class="col-md-6">
                            <h4>{{ __('home.invoice-info-buyer') }}</h4>
                            <p>{{ __('home.fullname') }}: <strong>{{ $order->sender_first_name }}</strong></p>
                            <p>{{ __('home.address') }}: {{ $order->sender_address }}, {{ $order->sender_city->name }}, {{ $order->sender_province->name }}</p>
                            <p>{{ __('home.phone-number') }}: {{ $order->sender_phone }}</p>
                            <p>{{ __('home.email') }}: {{ $order->sender_email }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="order-cart-block view-info-product">
        <div class="block-head">
            <div class="block-title">{{ __('home.order-info') }}</div>
        </div>
        <div class="block-main">
            <table class="cart-tb">
                <thead>
                    <tr>
                        <th style="width: 40px;">{{ __('home.cart-no') }}</th>
                        <th style="min-width: 200px;">{{ __('home.cart-product-info') }}</th>
                        <th style="width: 130px;">{{ __('home.cart-product-amount') }}</th>
                        <th style="width: 90px;">{{ __('home.cart-product-quantity') }}</th>
                        <th style="width: 230px;">{{ __('home.cart-product-totalamount') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @foreach($order->order_items as $item)
                    <tr class="product-item">
                        <td class="order">{{ $no }}</td>
                        <td class="product-info">
                            <a target="_blank" href="{{ $item->product->frontend_url }}" class="img">
                                <img alt="{{ $item->product->name }}" src="{{ $item->product->thumbnail }}">
                            </a>
                            <div class="info-wrap">
                                <div class="title">
                                    <a target="_blank" href="{{ $item->product->frontend_url }}">{{ $item->product->name }}</a>
                                </div>
                                <p><b class="text-red"><ins>{{ __('home.product-shopping') }}</ins></b>: </p>
                                <p>{{ __('home.product-code') }}: <span class="text-blue">{{ $item->product->code }}</span></p>
                            </div>
                        </td>
                        <td class="product-price">
                            <span class="item-final-amount price">{{ number_format($item->unit_goods_fee, 2, '.', ',') }} CAD</span>
                        </td>
                        <td class="quantity">
                            <input required="" type="number" class="form-control inputQuantity" name="quantity" value="{{ $item->quantity }}" disabled="">
                        </td>
                        <td class="product-price">
                            <span class="item-final-amount price">{{ number_format($item->sub_total_goods, 2, '.', ',') }} CAD</span>
                        </td>
                    </tr>
                    @php $no++; @endphp
                    @endforeach
                    <?php if(!empty($order->coupon_code)){ ?>
                    <tr>
                        <td colspan="4" class="guide-text">{{ __('label.coupon_code') }}</td>
                        <td class="total-price">{{ $order->coupon_code }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="guide-text">{{ __('label.coupon_amount') }}</td>
                        <td class="total-price">{{ number_format($order->coupon_amount, 2, '.', ',') }} CAD</td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="4" class="guide-text">{{ __('home.total-final') }}</td>
                        <td class="total-price">{{ number_format($order->total_goods_fee, 2, '.', ',') }} CAD</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="guide-text">{{ __('home.fee-shipping') }}</td>
                        <td class="total-price">{{ number_format($order->shipping_fee, 2, '.', ',') }} CAD</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="guide-text text-red"><b>{{ __('home.total-payment') }}</b></td>
                        <td class="total-price ">{{ number_format($order->total_final, 2, '.', ',') }} CAD</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
</div>
@endsection