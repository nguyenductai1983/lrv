@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/checkout.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javacript:void(0);">{{ __('home.cart') }}</a></li>
        </ol>
    </div>
</div>
<div class="container steps-order-page">
    <section class="order-cart-block">
        <div class="block-head">
            <div class="block-title">{{ __('home.my-cart') }}</div>
        </div>
        <div class="block-main">
            <div class="table-responsive">
                <div class="order-cart-form" id="cart-form-step1">
                    <table class="cart-tb">
                        <thead>
                            <tr>
                                <th style="width: 40px;">{{ __('home.cart-no') }}</th>
                                <th style="min-width: 200px;">{{ __('home.cart-product-info') }}</th>
                                <th style="width: 130px;">{{ __('home.cart-product-amount') }}</th>
                                <th style="width: 90px;">{{ __('home.cart-product-quantity') }}</th>
                                <th style="width: 230px;">{{ __('home.cart-product-totalamount') }}</th>
                                <th style="width: 60px;"></th>
                            </tr>
                        </thead>
                        <tbody id="cart-step1-content">
                            @php if(count($cart) > 0){ @endphp
                            @php $no = 1; $total = 0; @endphp
                            @foreach($cart as $item)
                            @php $subtotal = $item->price * $item->qty; $total += $subtotal; @endphp
                            <tr id="product-item-{{ $item->rowId }}" class="product-item">
                                <td class="order">{{ $no }}</td>
                                <td class="product-info">
                                    <a target="_blank" href="{{ $item->options['url'] }}" class="img">
                                        <img alt="" src="{{ $item->options['image'] }}">
                                    </a>
                                    <div class="info-wrap">
                                        <div class="title">
                                            <a target="_blank" href="{{ $item->options['url'] }}">{{ $item->name }}</a>
                                        </div>
                                        <p><b class="text-red"><ins>{{ __('home.product-shopping') }}</ins></b>: </p>
                                        <p>{{ __('home.product-code') }}: <span class="text-blue">{{ $item->options['code'] }}</span></p>

                                    </div>
                                </td>
                                <td class="product-price">
                                    <span id="item-price-{{ $item->rowId }}" class="item-final-amount price">{{ number_format($item->price, 2, '.', ',') }} CAD</span>
                                </td>

                                <td class="quantity">
                                    <input type="number" class="form-control inputQuantity" min="1" maxlength="2" name="quantity" value="{{ $item->qty }}" onchange="cart.updateItem('{{ $item->rowId }}', $(this).val())">
                                </td>
                                <td class="product-price">
                                    <span id="item-subprice-{{ $item->rowId }}" class="item-final-amount price">{{ number_format($subtotal, 2, '.', ',') }} CAD</span>
                                </td>
                                <td class="tool">
                                    <button type="button" class="btn btn-danger" style="cursor: pointer;" onclick="cart.removeItem('{{ $item->rowId }}')">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @php $no++; @endphp
                            @endforeach
                            <tr>
                                <td colspan="4" class="guide-text">{{ __('home.total-final') }}</td>
                                <td id="total-price" class="total-price">{{ number_format($total, 2, '.', ',') }} CAD</td>
                                <td></td>
                            </tr>
                            @php }else{ @endphp
                            <tr>
                                <td colspan="6" class="guide-text">{{ __('home.cart-empty') }}</td>
                            </tr>
                            @php } @endphp
                        </tbody>
                        @php if(count($cart) > 0){ @endphp
                        <tfoot id="cart-step1-tfoot">
                            <tr>
                                <td colspan="6">
                                    <a href="{{ route('product.index') }}" class="btn btn-default">{{ __('home.next-shopping') }}</a>
                                    <a href="{{ route('cart.step2') }}" class="btn btn-danger btnInforConfirm">{{ __('home.cart-step2') }}</a>
                                </td>
                            </tr>
                        </tfoot>
                        @php } @endphp
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('footer-js')
<script type="text/javascript" src="{{ asset('js/home/cart.js') }}"></script>
@endsection
