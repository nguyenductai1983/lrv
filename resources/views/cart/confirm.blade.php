@extends('layouts.home.index')
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/checkout.css') }}">
@endsection

@section('content')
<div class="br-content br-title">
    <div class="container">
        <h1 class="br-label"></h1>
        <ol class="br-menu">
            <li><a href="/">{{ __('home.home') }}</a></li>
            <li><a href="javascript:void(0);">{{ __('home.payment-info') }}</a></li>
        </ol>
    </div>
</div>

<div class="container steps-order-page">
    <section class="order-cart-block">
        <div class="block-head">
            <div class="block-title">{{ __('home.info-transfer') }}</div>
        </div>
        <div class="block-main">
            <div class="row">
                <div class="col-md-12">
                    <p><b>{{ __('home.customer-payment-transfer') }}:</b></p>
                    @if(!empty($payment_method->card_holder))
                    <p><b>{{ __('home.account-holder') }}:</b> {{ $payment_method->card_holder }}</p>
                    @endif
                    @if(!empty($payment_method->account_number))
                    <p><b>{{ __('home.account-number') }}:</b> {{ $payment_method->account_number }}</p>
                    @endif
                    @if(!empty($payment_method->name))
                    <p><b>{{ __('home.bank') }}:</b> {{ $payment_method->name }} {{ $payment_method->bank_branch }} </p>
                    @endif
                    <p><b>{{ __('home.transfer-amount') }}:</b> {{ number_format($order->total_final, 2, '.', ',') }} CAD</p>
                    <p><b>{{ __('home.transfer-content') }}:</b>  {{ $order->code }} , {{ $order->sender_phone }}</p>
                    <div class="tranfer-foot">
                        <a class="btn btn-default" href="/">{{ __('home.back-home') }} </a>
                        <a href="{{ route('cart.step3', $order->code) }}" class="btn btn-danger">
                            {{ __('home.select-paymnet-method-other') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection