@extends('layouts.email.index')
@section('content')
<tr>
    <td style="background:#ffffff; border-bottom:2px solid #f4ae01; color:#0063d1; font-size:24px; text-transform:uppercase; font-family:Arial, Helvetica, sans-serif; padding:15px 0px 15px;font-weight: bold;"
        align="center">
        {{ __('email.payment-extra')}}
    </td>
</tr>
<tr class="tbHide">
    <td bgcolor="#ffffff" style="padding:13px 0 11px;" width="640">
        <table style="padding:0 0 0 0;border:1px solid #e3e3e3;border-top:none;border-bottom:none;background: #fff"
               border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td colspan="3">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding:20px 20px 0 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
                                        {{ __('email.welcome')}}, <b>{{ $orderExtraRefund->order->sender_email }}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:20px 20px 20px 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
                                        {{ __('email.hi-customer')}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 0 20px;">
                        <table style="border-collapse:collapse;border:1px solid #e3e3e3"
                               bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding:20px 20px 20px 20px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="50%" style="vertical-align: top">
                                                <h4 style="font-size: 14px; margin: 0 0 15px;text-transform: uppercase;">{{ __('email.extra-info')}}</h4>
                                                <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                                <p><b>{{ __('email.extra-code')}}:</b>{{ $orderExtraRefund->code}} </p>
                                                <p><b>{{ __('email.extra-date')}}:</b>{{ $orderExtraRefund->created_at}}</p>
                                            </td>
                                            <td width="50%" style="vertical-align: top">
                                                <h4 style="font-size: 14px; margin: 0 0 15px;text-transform: uppercase;">{{ __('email.extra-customer')}}</h4>
                                                <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                                <p><b>{{ __('email.extra-fullname')}}:</b> {{ $orderExtraRefund->order->sender_full_name}}</p>
                                                <p><b>{{ __('email.extra-address')}}:</b> {{ $orderExtraRefund->order->sender_address}}, {{ $orderExtraRefund->order->sender_city->name}}, {{ $orderExtraRefund->order->sender_province->name}}</p>
                                                <p><b>{{ __('email.extra-phone')}}:</b> {{ $orderExtraRefund->order->sender_phone}}</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px 20px 0 20px" colspan="3">
                        <table style="border-collapse:collapse;border:1px solid #e3e3e3"
                               bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 20px 1px 20px">
                                    <table style="border-collapse:collapse" border="0" cellpadding="0"
                                           cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:18px;font-weight:bold;color:#25396c;padding:0 0 10px 0"
                                                colspan="4" align="left" width="30%">
                                                <h4 style="font-size: 14px; margin: 0 0 15px;color: rgb(102, 102, 102);text-transform: uppercase;">{{ __('email.extra-detail')}}</h4>
                                                <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:13.5px;font-weight:bold;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="left" bgcolor="#fffbe2">{{ __('email.order_code')}}
                                            </td>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="right" bgcolor="#fffbe2">{{ $orderExtraRefund->order->code}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:13.5px;font-weight:bold;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="left" bgcolor="#fffbe2">{{ __('email.amount')}}
                                            </td>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="right" bgcolor="#fffbe2">{{ number_format($orderExtraRefund->amount, 2, '.', ',') }} CAD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:13.5px;font-weight:bold;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="left" bgcolor="#fffbe2">{{ __('email.paid_amount')}}
                                            </td>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="right" bgcolor="#fffbe2">{{ number_format($orderExtraRefund->amount, 2, '.', ',') }} CAD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:13.5px;font-weight:bold;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="left" bgcolor="#fffbe2">{{ __('email.reason')}}
                                            </td>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="right" bgcolor="#fffbe2">{{ $orderExtraRefund->reason }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
@endsection