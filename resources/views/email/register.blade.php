@extends('layouts.email.index')
@section('content')
<tr>
    <td style="background:#ffffff; border-bottom:2px solid #f4ae01; color:#0063d1; font-size:24px; text-transform:uppercase; font-family:Arial, Helvetica, sans-serif; padding:15px 0px 15px;font-weight: bold;"
        align="center">
        {{ __('email.register-success') }}
    </td>
</tr>
<tr class="tbHide">
    <td bgcolor="#ffffff" style="padding:13px 0 11px;" width="640">
        <table style="padding:0 0 0 0;border:1px solid #e3e3e3;border-top:none;border-bottom:none;background: #fff"
               border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td colspan="3">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding:20px 20px 0 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
                                        {{ __('email.welcome')}}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:20px 20px 20px 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
                                        {{ __('email.register-guide')}}
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 0 20px">
                        <table style="border: 1px solid #e3e3e3;" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td style="padding: 20px">
                                    <h4 style="font-size: 14px; margin: 0 0 15px;">{{ __('email.register-info') }}</h4>
                                    <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="padding: 15px;border: 1px solid #e3e3e3;" width="25%">
                                                <b>Email</b>
                                            </td>
                                            <td style="padding: 15px; border: 1px solid #e3e3e3;border-left: none; vertical-align: top" width="75%">
                                                {{ $customer->email }}
                                            </td>
                                        </tr>
                                    </table>
                                    <p style="padding: 20px 0 0 0">{{ __('email.register-authen-guide') }}:<br/>
                                        <a href="{{ route('customer.verify', $customer->verify_token) }}" style="display: block; width: 500px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis">{{ __('email.register-authen') }}</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
@endsection