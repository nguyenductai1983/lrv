@extends('layouts.email.index')
@section('content')
<tr>
    <td style="background:#ffffff; border-bottom:2px solid #f4ae01; color:#0063d1; font-size:24px; text-transform:uppercase; font-family:Arial, Helvetica, sans-serif; padding:15px 0px 15px;font-weight: bold;"
        align="center">
        {{ __('email.approved-transport-quote')}}
    </td>
</tr>
<tr class="tbHide">
    <td bgcolor="#ffffff" style="padding:13px 0 11px;" width="640">
        <table style="padding:0 0 0 0;border:1px solid #e3e3e3;border-top:none;border-bottom:none;background: #fff"
               border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td colspan="3">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding:20px 20px 0 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
                                        {{ __('email.welcome')}}, <b>{{ $transport->sender_full_name }}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:20px 20px 20px 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;">
                                        {{ __('email.hi-customer')}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 0 20px;">
                        <table style="border-collapse:collapse;border:1px solid #e3e3e3"
                               bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding:20px 20px 20px 20px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="50%" style="vertical-align: top">
                                                <h4 style="font-size: 14px; margin: 0 0 15px;text-transform: uppercase;">{{ __('email.transport-info')}}</h4>
                                                <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                                <p><b>{{ __('email.transport-code')}}:</b>{{ $transport->code}} </p>
                                                <p><b>{{ __('email.transport-date')}}:</b>{{ $transport->created_at}}</p>
                                            </td>
                                            <td width="50%" style="vertical-align: top">
                                                <h4 style="font-size: 14px; margin: 0 0 15px;text-transform: uppercase;">{{ __('email.transport-receiver')}}</h4>
                                                <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                                <p><b>{{ __('email.transport-fullname')}}:</b> {{ $transport->receive_full_name}}</p>
                                                <p><b>{{ __('email.transport-address')}}:</b> {{ $transport->receiver_address}}, {{ $transport->receiver_city->name}}, {{ $transport->receiver_province->name}}</p>
                                                <p><b>{{ __('email.transport-phone')}}:</b> {{ $transport->receiver_phone}}</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px 20px 0 20px" colspan="3">
                        <table style="border-collapse:collapse;border:1px solid #e3e3e3"
                               bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 20px 1px 20px">
                                    <table style="border-collapse:collapse" border="0" cellpadding="0"
                                           cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:18px;font-weight:bold;color:#25396c;padding:0 0 10px 0"
                                                colspan="4" align="left" width="30%">
                                                <h4 style="font-size: 14px; margin: 0 0 15px;color: rgb(102, 102, 102);text-transform: uppercase;">{{ __('email.transport-detail')}}</h4>
                                                <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:13.5px;font-weight:bold;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="left" bgcolor="#fffbe2">{{ __('email.transport-final-amount')}}
                                            </td>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="right" bgcolor="#fffbe2">{{ number_format($transport->total_final, 2, '.', ',') }} CAD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:13.5px;font-weight:bold;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="left" bgcolor="#fffbe2">{{ __('label.payment_status')}}
                                            </td>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:10px 10px 10px 10px;border-bottom:1px solid #e3e3e3"
                                                colspan="2" align="right" bgcolor="#fffbe2">{{ $transport->payment_status_name }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 0 20px;">
                        <table style="border-collapse:collapse;border:1px solid #e3e3e3"
                               bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding:20px 20px 20px 20px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <h4 style="font-size: 14px; margin: 0 0 15px;text-transform: uppercase;">{{ __('home.info-transfer') }}</h4>
                                        <p><i style="display: block; width: 50px; height: 1px; background: #2796b6;"></i></p>
                                        <p><b>{{ __('email.option-payment-transfer') }}:</b></p>
                                        {{-- @if($payment_methods)
                                        @foreach($payment_methods as $payment_method) --}}
                                        <tr>
                                            <td width="100%" style="vertical-align: top">
                                            {{-- <p><b>{{ __('home.account-holder') }}:</b> {{ $payment_method->card_holder }}</p>
                                            <p><b>{{ __('home.account-number') }}:</b> {{ $payment_method->account_number }}</p>
                                            <p><b>{{ __('home.bank') }}:</b> {{ $payment_method->bank_branch }}</p> --}}
                                            <p><b>{{ __('home.transfer-amount') }}:</b> {{ number_format($transport->total_final, 2, '.', ',') }} CAD</p>
                                            <p><b>{{ __('home.transfer-content') }}:</b>  {{ $transport->code }} , {{ $transport->sender_phone }}</p>
                                            </td>
                                        </tr>
                                        {{-- @endforeach
                                        @endif --}}
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
@endsection
