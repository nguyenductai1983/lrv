@extends('layouts.email.mts2021')
@section('content')

<!-- header -->

<!-- header -->
<!-- Email Body -->
<tr>
    <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <!-- Body content -->
            <tr>
                <td class="content-cell">
                    <div class="f-fallback">
                        <h1>Dear Mr/Ms {{ $mts->sender_full_name }}</h1>
                        <p>Thanks for using Transfer Money Service. This email is the receipt for your purchase. No payment is due.
                        </p>
                        <!-- Action -->
                        {{-- <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0"
                                    role="presentation">
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                role="presentation">
                                                <tr>
                                                    <td align="center">
                                                        <a href="  action_url " class="f-fallback button button--green"
                                                            target="_blank">Pay Invoice</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table> --}}
                        <!-- Action -->
                        <table class="purchase" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <h3> {{ $mts->code}} </h3>
                                </td>
                                <td>
                                    <h3 class="align-right">{{ $mts->pay_date }}</h3>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table class="purchase_content" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <th class="purchase_heading" align="left">
                                                <p class="f-fallback">Description</p>
                                            </th>
                                            <th class="purchase_heading" align="left">
                                                <p class="f-fallback">Amount</p>
                                            </th>
                                        </tr>

                                        <tr>
                                            <td width="60%" class="purchase_item">
                                                <span class="f-fallback"> Amount </span>
                                            </td>
                                            <td class="align-right" width="40%" class="purchase_item">
                                                <span class="f-fallback">
                                                    {{ number_format($mts->total_goods, 2, '.', ',') }} CAD </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="60%" class="purchase_footer" valign="middle">
                                                <p class="f-fallback purchase_total purchase_total--label">Total
                                                </p>
                                            </td>
                                            <td width="40%" class="purchase_footer" valign="middle">
                                                <p class="f-fallback purchase_total">
                                                    {{ number_format($mts->total_paid_amount, 2, '.', ',') }} CAD </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p>Cheers,
                            <br>The Transfer Money Service Team
                            <br>Hotline: CA 1 (888) 619-6869 / VN 1900 055 509</p>
                        <!-- Sub copy -->
                        {{-- <table class="body-sub" role="presentation">
                                    <tr>
                                        <td>
                                            <p class="f-fallback sub">If you’re having trouble with the button above,
                                                copy and paste the URL below into your web browser.</p>
                                            <p class="f-fallback sub">  action_url </p>
                                        </td>
                                    </tr>
                                </table> --}}
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- footer -->
<!-- footer -->

@endsection
