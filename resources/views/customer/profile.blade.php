@extends('layouts.home.user', ['menu' => $menu])
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/profile.css') }}">
@endsection

@section('content')
<div class="panel panel-default" style="padding: 15px;">
    <section class="user-block user-profile-block">
        <div class="block-head">
            <div class="block-title">{{ __('home.header-my-account') }}</div>
        </div>
        <div id="profile-content" class="block-main">
            <form id="profile-form" class="profile-form">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.code') }}:</div>
                            <div class="info-col">
                                <input type="text" name="code" class="form-control input-sm" value="{{ $customer->code }}" readonly="true">
                            </div>
                        </div>
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.first_name') }}:</div>
                            <div class="info-col">
                                <input type="text" name="first_name" class="form-control input-sm" value="{{ $customer->first_name }}">
                            </div>
                        </div>

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.middle_name') }}:</div>
                            <div class="info-col">
                                <input type="text" name="middle_name" class="form-control input-sm" value="{{ $customer->middle_name }}">
                            </div>
                        </div>

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.last_name') }}:</div>
                            <div class="info-col">
                                <input type="text" name="last_name" class="form-control input-sm" value="{{ $customer->last_name }}">
                            </div>
                        </div>

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('home.phone-number') }}:</div>
                            <div class="info-col">
                                <input type="text" name="telephone" class="form-control input-sm" value="{{ $customer->telephone }}">
                            </div>
                        </div><!-- .form-group -->
                        @php $field = 'country_id'; @endphp
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('home.country') }}:</div>
                            <div class="info-col">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="">{{ __('customer.select_country') }}</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" @if($country->id == old($field, $customer->{$field})) selected="selected"@endif>{{ $country->code . '-' . $country->name }}</option>
                                @endforeach
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div><!-- .form-group -->

                        @php $field = 'province_id'; @endphp
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('home.province') }}:</div>
                            <div class="info-col">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="">{{ __('customer.select_province') }}</option>
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div><!-- .form-group -->

                        @php $field = 'city_id'; @endphp
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('home.district') }}:</div>
                            <div class="info-col">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                <option value="">{{ __('customer.select_city') }}</option>
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div><!-- .form-group -->

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('home.address') }}:</div>
                            <div class="info-col">
                                <input name="address_1" type="text" class="form-control input-sm" value="{{ $customer->address_1 }}">
                            </div>
                        </div><!-- .form-group -->
                         <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.address_2') }}:</div>
                            <div class="info-col">
                                <input name="address_2" type="text" class="form-control input-sm" value="{{ $customer->address_2 }}">
                            </div>
                        </div><!-- .form-group -->
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('label.company') }}:</div>
                            <div class="info-col">
                                <input name="company" type="text" class="form-control input-sm" value="{{ $customer->company }}">
                            </div>
                        </div><!-- .form-group -->
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('label.post_code') }}:</div>
                            <div class="info-col">
                                <input name="postal_code" type="text" class="form-control input-sm" value="{{ $customer->postal_code }}">
                            </div>
                        </div><!-- .form-group -->

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.id_card') }}:</div>
                            <div class="info-col">
                                <input name="id_card" type="text" class="form-control input-sm" value="{{ $customer->id_card }}">
                            </div>
                        </div><!-- .form-group -->

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.card_expire') }}:</div>
                            <div class="info-col">
                                <input name="card_expire" id="card_expire" type="text" class="form-control input-sm" value="{{ $customer->card_expire }}">
                            </div>
                        </div><!-- .form-group -->

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('home.birthday') }}:</div>
                            <div class="info-col">
                                <input type="text" id="birth_date" name="birthday" class="form-control input-sm" value="{{ $customer->birthday }}">
                            </div>
                        </div><!-- .form-group -->

                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.career') }}:</div>
                            <div class="info-col">
                                <input name="career" type="text" class="form-control input-sm" value="{{ $customer->career }}">
                            </div>
                        </div><!-- .form-group -->

                        @php $field = 'image_1_file_id'; @endphp
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.image_1_file_id') }}:</div>
                            <div class="info-col">
                                @include('partials.form-controls.image', ['field' => 'image_1_file_id', 'file' => $customer->{$field} ? $customer->image1 : null ])
                            </div>
                        </div><!-- .form-group -->

                        @php $field = 'image_2_file_id'; @endphp
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.image_2_file_id') }}:</div>
                            <div class="info-col">
                                @include('partials.form-controls.image', ['field' => 'image_2_file_id', 'file' => $customer->{$field} ? $customer->image2 : null])
                            </div>
                        </div><!-- .form-group -->

                        @php $field = 'image_3_file_id'; @endphp
                        <div class="form-group is-col group-sm">
                            <div class="lbl-col">{{ __('customer.image_3_file_id') }}:</div>
                            <div class="info-col">
                                @include('partials.form-controls.image', ['field' => 'image_3_file_id', 'file' => $customer->{$field} ? $customer->image3 : null])
                            </div>
                        </div><!-- .form-group -->
                    </div>
                </div>
                <div id="profile-content-alert"></div>
                <div class="form-group is-col group-sm">
                    <div class="btn-wrap">
                        <button type="button" onclick="customer.profile()" class="submit-btn btn btn-sm btn-pill btn-grd-bg">{{ __('home.update') }}</button>
                    </div>
                </div>
                <div id="profile-content-alert"></div>
                {{ csrf_field() }}
            </form>
        </div>
    </section>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="{{ asset('js/home/customer.js') }}"></script>
<script type="text/javascript">
                        $(function () {
          var countryEle = $('select#country_id');
          var provinceEle = $('select#province_id');
          var cityEle = $('select#city_id');

          var getProvinces = function () {
              var countryId = parseInt(countryEle.val()),
                  data      = {
                      country_id: countryId
                  };

              provinceEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('api.provinces') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.provinces, function (key, province) {
                      var option = '<option value="' + province.id + '"' + (province.id === parseInt({{ old('province_id', $customer->province_id) }}) ? ' selected="selected"' : '') + '>' + province.name + '</option>';
                      provinceEle.append(option);
                  });
                  provinceEle.prop('disabled', false).trigger('change');
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          var getCities = function () {
              var countryId  = parseInt(countryEle.val()),
                  provinceId = parseInt(provinceEle.val()),
                  data       = {
                      country_id : countryId,
                      province_id: provinceId
                  };

              cityEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('api.cities') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.cities, function (key, city) {
                      var option = '<option value="' + city.id + '"' + (city.id === parseInt({{ old('city_id', $customer->city_id) }}) ? ' selected="selected"' : '') + '>' + city.name + '</option>';
                      cityEle.append(option);
                  });
                  cityEle.prop('disabled', false);
                  setSelect2();
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          if ('{{ old('country_id', $customer->country_id) }}') {
              getProvinces();
          }

          countryEle.change(function () {
              getProvinces();
          });

          provinceEle.change(function () {
              getCities();
          });
      });
    $(function () {
        $("#birth_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#card_expire").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
