@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('customer.manage_address') }}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('profile.address_create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add')}}
                    </a>
                </div>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="10%">{{ __('customer.full_name') }}</th>
                            <th width="10%">{{ __('customer.address_1') }}</th>
                            <th width="10%">{{ __('customer.telephone') }}</th>
                            <th width="10%">{{ __('customer.postal_code') }}</th>
                            <th width="10%">{{ __('customer.city') }}</th>
                            <th width="10%">{{ __('customer.province') }}</th>
                            <th width="10%">{{ __('customer.country') }}</th>
                            <th width="10%">{{ __('label.action') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($addresses->count() > 0)
                        @foreach($addresses as $address)
                        <tr>
                            <td>{{ $address->full_name }}</td>
                            <td>{{ $address->address_1 }}</td>
                            <td>{{ $address->telephone }}</td>
                            <td>{{ $address->postal_code }}</td>
                            <td>{{ isset($address->city->name) ? $address->city->name : '' }}</td>
                            <td>{{ isset($address->province->name) ? $address->province->name : '' }}</td>
                            <td>{{ isset($address->country->name) ? $address->country->name : ''}}</td>
                            <td>
                                <a href="{{ route('profile.address_edit', $address->id) }}" class="btn btn-xs btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-xs btn-danger" onclick="customer.delete({{ $address->id }})">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $addresses->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="{{ asset('js/home/customer.js') }}"></script>
@endsection
