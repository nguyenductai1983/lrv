@extends('layouts.home.user', ['menu' => $menu])
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/profile.css') }}">
@endsection

@section('content')
<div class="panel panel-default" style="padding: 15px;">
    <section class="user-block user-profile-block">
        <div class="block-head">
            <div class="block-title">{{ __('customer.address_edit') }}</div>
        </div>
        <div id="profile-content" class="block-main">
            <form id="form-customer" action="{{ route('profile.address_update', $address->id) }}" method="post" role="form" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        @php $field = 'first_name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'middle_name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'last_name'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'address_1'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'address_2'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'telephone'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @php $field = 'cellphone'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php $field = 'postal_code'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'email'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="{{ $field }}" class="form-control" value="{{ old($field, $address->{$field}) }}">
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'country_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="">{{ __('customer.select_country') }}</option>
                                    @foreach($countries as $country)
                                    <option value="{{ $country->id }}" @if($country->id == old($field, $address->{$field})) selected="selected"@endif>{{ $country->code . '-' . $country->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'province_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="">{{ __('customer.select_province') }}</option>
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>

                        @php $field = 'city_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="">{{ __('customer.select_city') }}</option>
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @if ($address->country_id===91)
                        @php $field = 'ward_id'; @endphp
                        <div class="form-group{{ $errors->has($field) ? ' validate-has-error' : '' }}">
                            <label for="{{ $field }}" class="control-label col-sm-4">
                                {{ __('customer.' . $field) }}<i class="text-danger">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select name="{{ $field }}" id="{{ $field }}" class="form-control">
                                    <option value="">{{ __('customer.select_ward') }}</option>
                                </select>
                                @if($errors->has($field))
                                <span>{{ $errors->first($field) }}</span>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12 text-right">
                        <a href="{{ route('profile.address') }}" class="btn btn-white">
                            {{ __('label.cancel') }}
                        </a>
                        <button type="submit" class="btn btn-info disabled-submit">
                            <i class="fa fa-check"></i> {{ __('label.update') }}
                        </button>
                    </div>
                </div>
                {{ method_field('put') }}
                {{ csrf_field() }}
            </form>
        </div>
    </section>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="{{ asset('js/home/customer.js') }}"></script>
<script type="text/javascript">
    $(function () {
          var countryEle = $('select#country_id');
          var provinceEle = $('select#province_id');
          var cityEle = $('select#city_id');
          var wardEle = $('select#ward_id');

          var getProvinces = function () {
              var countryId = parseInt(countryEle.val()),
                  data      = {
                      country_id: countryId
                  };

              provinceEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('api.provinces') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.provinces, function (key, province) {
                      var option = '<option value="' + province.id + '"' + (province.id === parseInt({{ old('province_id', $address->province_id) }}) ? ' selected="selected"' : '') + '>' + province.name + '</option>';
                      provinceEle.append(option);
                  });
                  provinceEle.prop('disabled', false).trigger('change');
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          var getCities = function () {
              var countryId  = parseInt(countryEle.val()),
                  provinceId = parseInt(provinceEle.val()),
                  data       = {
                      country_id : countryId,
                      province_id: provinceId
                  };

              cityEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('api.cities') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.cities, function (key, city) {
                      var option = '<option value="' + city.id + '"' + (city.id === parseInt({{ old('city_id', $address->city_id) }}) ? ' selected="selected"' : '') + '>' + city.name + '</option>';
                      cityEle.append(option);
                  });
                  cityEle.prop('disabled', false).trigger('change');
                //   setSelect2();
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };
          var getWards = function () {
              var CityId = parseInt(cityEle.val())
                  data = {
                      city_id : CityId
                  };

                wardEle.prop('disabled', true).find('option[value!=""]').remove();
              $.ajax({
                  method     : 'GET',
                  url        : '{{ route('api.wards') }}?' + toUrlEncodedString(data),
                  contentType: 'appplication/json',
                  dataType   : 'json'
              }).done(function (data) {
                  $.each(data.wards, function (key, ward) {
                      var option = '<option value="' + ward.id + '"' + (ward.id === parseInt({{ old('ward_id', $address->ward_id) }}) ? ' selected="selected"' : '') + '>' + ward.name + '</option>';
                      wardEle.append(option);
                  });
                  wardEle.prop('disabled', false);
              }).fail(function (jqXHR) {
                  console.log(jqXHR);
              });
          };

          if ('{{ old('country_id', $address->country_id) }}') {
              getProvinces();
          }

          countryEle.change(function () {
              getProvinces();
          });

          provinceEle.change(function () {
              getCities();
          });
          cityEle.change(function () {
              getWards();
          });
      });
</script>
@endsection
