@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('campaign.manage') }}
    </div>
    <div class="panel-body">
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="10%">{{ __('campaign.code') }}</th>
                            <th width="10%">{{ __('campaign.name') }}</th>
                            <th width="10%">{{ __('campaign.amount') }}</th>
                            <th width="10%">{{ __('campaign.num_of_use') }}</th>
                            <th width="10%">{{ __('campaign.created_at') }}</th>
                            <th width="10%">{{ __('campaign.use_status') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($coupons->count() > 0)
                        @foreach($coupons as $coupon)
                        <tr>
                            <td>{{ $coupon->code }}</td>
                            <td>{{ $coupon->name }}</td>
                            <td>{{ $coupon->amount }}</td>
                            <td>{{ $coupon->num_of_use }}</td>
                            <td>{{ $coupon->created_at }}</td>
                            <td><span class="{{ $coupon->is_used_label }}">{{ $coupon->is_used_name }}</span></td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $coupons->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection