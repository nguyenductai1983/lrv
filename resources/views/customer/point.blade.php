@extends('layouts.home.user', ['menu' => $menu])

@section('content')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('point.manage') }}
    </div>
    <div class="panel-body">
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="10%">{{ __('point.value') }}</th>
                            <th width="10%">{{ __('point.type') }}</th>
                            <th width="10%">{{ __('point.create_time') }}</th>
                            <th width="10%">{{ __('point.use_ref_code') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($points->count() > 0)
                        @foreach($points as $point)
                        <tr>
                            <td>{{ $point->point }}</td>
                            <td>{{ $point->type_name }}</td>
                            <td>{{ $point->created_at }}</td>
                            <td>{{ $point->use_ref_code }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $points->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection