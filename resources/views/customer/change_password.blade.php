@extends('layouts.home.user', ['menu' => $menu])
@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/profile.css') }}">
@endsection

@section('content')
<div class="panel panel-default" style="padding: 15px;">
    <section class="user-block user-profile-block">
        <div class="block-head">
            <div class="block-title">{{ __('home.change-password') }}</div>
        </div>
        <div id="change-password-content" class="block-main">
            <div id="change-password-content-alert"></div>
            <form id="change-password-form" class="profile-form">
                <div class="form-group is-col group-sm">
                    <div class="lbl-col">{{ __('home.current-password') }}:</div>
                    <div class="info-col">
                        <input type="password" name="old_password" class="form-control input-sm" value="">
                    </div>
                </div>

                <div class="form-group is-col group-sm">
                    <div class="lbl-col">{{ __('home.new-password') }}:</div>
                    <div class="info-col">
                        <input type="password" name="password" class="form-control input-sm" value="">
                    </div>
                </div><!-- .form-group -->
                
                <div class="form-group is-col group-sm">
                    <div class="lbl-col">{{ __('home.retype-password') }}:</div>
                    <div class="info-col">
                        <input type="password" name="password_confirmation" class="form-control input-sm" value="">
                    </div>
                </div><!-- .form-group -->
                
                <div class="btn-wrap">
                    <button type="button" onclick="customer.changePassword()" class="submit-btn btn btn-sm btn-pill btn-grd-bg">{{ __('home.approve') }}</button>
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </section>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="{{ asset('js/home/customer.js') }}"></script>
@endsection