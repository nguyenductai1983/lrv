@extends('layouts.home.index')
{{-- @extends('layouts.app') --}}
@section('head-css')
{{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
@endsection
@section('content')
{{-- bắt đầu đoạn đăng nhập --}}
            <div class="row align-items-center">
                <div class="col-md-6 col-xs-10 col-xs-offset-1">
                    <h2 class="font-weight-bold">{{ __('home.welcome') }}</h2>
                    <p>
                        {{ __('home.guidelogin') }}
                    </p>
                    <p>
                        {{ __('home.forgotpass') }}
                    </p>
                    <p>
                        {{ __('home.new_user') }}
                    </p>
                    <p>
                        {{ __('home.niceday') }}
                    </p>
                </div>
                <div class="col-md-4 col-md-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="box-title"><h2>{{ __('home.sign-in') }}</h2></div>
                        </div>
                       <div class="panel-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                                <label for="email" class="col-form-label text-left">{{ __('E-Mail Address') }}</label>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <label for="password" class="col-form-label text-left">{{ __('Password') }}</label>
                                <div class="form-group">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                     @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                                {{-- <div>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div> --}}
                                <button type="submit" class="btn btn-success btn-block btn-lg">Login</button>
                                <br>
                                <p class="text-muted text-left">
                                <a href="{{ route('customer.forgot') }}">{{ __('home.what-forgot') }} {{ __('home.get-in') }}</a>
                                </p>
                                <p class="text-muted text-left">
                                    Do not have an account?
                                    <a class="line-height-40px" href="{{ route('customer.register') }}">
                                        <i class="fa"></i> {{ __('home.btn-sign-up') }}
                                    </a>
                                </p>
                                <div class="login-with-wrap">
                                    {{ __('home.sign-in-face-google') }} :
                                    <a href="{{ URL::to('auth/facebook') }}" class="icon btn-lg">
                                        <h2><i class="fab fa-facebook-square" style="color: #3b5998"></i></h2>
                                      </a>
                                      <a href="{{ URL::to('auth/google') }}" class="icon btn-lg">
                                        <h2><i class="fab fa-google-plus-square" style="color: #db4a39"></i></h2>
                                      </a>
                                </div>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-xs-1"></div>
            </div>
{{-- hết đoạn đăng nhập --}}
@endsection
@section('scripts')
@parent

{{-- @if($errors->has('email') || $errors->has('password'))
    <script>
    $(function() {
        $('#loginModal').modal({
            show: true
        });
    });
    </script>
@endif --}}
document.onkeydown=function(){
    if(window.event.keyCode=='13'){
        submitForm();
    }
}
@endsection
