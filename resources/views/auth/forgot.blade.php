@extends('layouts.auth.index', ['bodyClass' => ' login-page login-light'])

@section('body')
    <!-- Errors container -->
    <div class="errors-container">
    </div>

    <!-- Add class "fade-in-effect" for login form effect -->
    <form id="request-pass-form" class="login-form fade-in-effect">
        <div class="login-header">
            <a href="{{ url('/') }}" class="logo">
                <img src="{{ asset('images/admin/logo-white-bg@2x.png') }}" alt="" width="80"/>
                <span>{{ __('home.forgot-password') }}</span>
            </a>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="email">{{ __('user.email') }}</label>
            <input type="text" class="form-control" name="email" id="email" autocomplete="off"/>
            @if($errors->has('email'))
                <span class="validate-has-error">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <div class="form-group-1 btn-wrap">
            <div class="pull-left">
                <button type="button" class="btn btn-primary btn-block disabled-submit" onclick="user.forgotLink();">
                    {{ __('home.complete') }}
                </button>
            </div>
            <div class="clearfix"></div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection

@section('footer')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            // Reveal Login form
            setTimeout(function () {
                $(".fade-in-effect").addClass('in');
            }, 1);
            // Set Form focus
            $("form#login .form-group:has(.form-control):first .form-control").focus();
        });
    </script>
    <script type="text/javascript" src="{{ asset('js/home/user.js') }}"></script>
@endsection
