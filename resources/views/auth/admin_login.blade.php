@extends('layouts.auth.index', ['bodyClass' => ' login-page login-light'])
{{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
@section('content')
  {{-- bắt đầu đoạn đăng nhập --}}

  <div class="container-fluid ">
    <div style="margin-top: 100px;">
      <div class="row">
        <div class="col-md-5 col-xs-10 col-xs-offset-1">
          <h2 class="font-weight-bold">{{ __('home.welcome') }}</h2>
          <p>
            {{ __('home.guidelogin') }}
          </p>
          <p>
            {{ __('home.forgotpass') }}
          </p>
          <p>
            {{ __('home.new_user') }}
          </p>
          <p>
            {{ __('home.niceday') }}
          </p>
        </div>
        <div class="col-md-4 col-md-offset-0 col-xs-10 col-xs-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="box-title">
                <h2>{{ __('home.sign-in') }} <img src="{{ asset('images/admin/logo-white-bg@2x.png') }}" alt=""
                    width="80" /></h2>
              </div>
            </div>
            <div class="panel-body">
              <form method="post" action="{{ route('admin.login.submit') }}" role="form" id="login"
                class="login-form fade-in-effect">
                @csrf
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="login-header">
                </div>
                <div class="form-group{{ $errors->has('username') ? ' validate-has-error' : '' }}">
                  <label class="control-label" for="username">{{ __('user.username') }}</label>
                  <input type="text" class="form-control" name="username" id="username" autocomplete="off" />
                  @if ($errors->has('username'))
                    <span class="validate-has-error">{{ $errors->first('username') }}</span>
                  @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' validate-has-error' : '' }}">
                  <label class="control-label" for="password">{{ __('user.password') }}</label>
                  <input type="password" class="form-control" name="password" id="password" autocomplete="off" />
                  @if ($errors->has('password'))
                    <span class="validate-has-error">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block disabled-submit">
                    <i class="fas fa-handshake fa-lg"></i>
                    {{ __('label.login') }}
                  </button>
                  @if (Route::has('admin.password.forgot-pass'))
                    <a class="btn btn-link" href="{{ route('admin.password.forgot-pass') }}">
                      {{ __('label.forgot_password') }}
                    </a>
                  @endif
                </div>
                @include('partials.languages')
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- hết đoạn đăng nhập --}}



@endsection

@section('footer')
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      // Reveal Login form
      setTimeout(function() {
        $(".fade-in-effect").addClass('in');
      }, 1);
      // Set Form focus
      $("form#login .form-group:has(.form-control):first .form-control").focus();
    });

  </script>
@endsection
