@extends('layouts.home.index')

@section('head-css')
<link rel="stylesheet" href="{{ asset('css/home/reset.css') }}">
@endsection

@section('content')
    <div class="br-content br-title">
        <div class="container">
            <h1 class="br-label"></h1>
            <ol class="br-menu">
                <li><a href="{{ route('home.index') }}">Trang chủ</a></li>
                <li><a href="javascript::void(0);">Lấy lại mật khẩu</a></li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 user-main">
                <section class="user-profile-block">
                    <div class="block-head">
                        <div class="block-title">Lấy lại mật khẩu</div>
                    </div><!-- .block-head -->
                    <div class="block-main">
                        <form id="reset-form" class="user-forgot-form">
                            <div class="group-item">
                                <div class="lbl"><i class="fa fa-key"></i>Mật khẩu mới:</div>
                                <div class="input">
                                    <input type="password" name="password" class="form-control-1 user-input" placeholder="" required="" aria-required="true">
                                </div>
                            </div><!-- .group-item -->

                            <div class="group-item">
                                <div class="lbl"><i class="fa fa-key"></i>Nhập lại mật khẩu mới:</div>
                                <div class="input">
                                    <input type="password" name="password_confirmation" class="form-control-1 password-input" placeholder="" required="" aria-required="true">
                                </div>
                            </div><!-- .group-item -->

                            <div class="group-item">
                                <div class="lbl"></div>
                                <div class="input">
                                    <input type="hidden" name="reset_password_token" value="{{ $customer->reset_password_token }}">
                                    <button class="btn btn-danger btnInforConfirm" type="button" onclick="user.resetPassword();">
                                        <i class="fa"></i>Hoàn tất                                        
                                    </button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection