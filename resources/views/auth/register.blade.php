
@extends('layouts.home.index')
{{-- @extends('layouts.app') --}}
@section('head-css')
{{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
@endsection
@section('content')
<div class="row align-items-center">
    <div class="col-md-6 col-xs-10 col-xs-offset-1">
        <Img width="100%" src="https://igreencorp.com/storage/2020/04/CIJAqHxR8PTAu4MS7XSYCpqa8PZJE0Gxv5z370EY.jpeg"></>
    </div>
    <div class="col-md-4 col-md-offset-0 col-xs-10 col-xs-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">
            <h3>{{ __('home.btn-sign-up') }}  <img src="{{ asset('images/admin/logo-white-bg@2x.png') }}" alt="" width="80"/></h3>
            </div>
           <div class="panel-body">
    <!-- Errors container -->
    <div class="errors-container">
    </div>
    <!-- Add class "fade-in-effect" for login form effect -->
    <form id="register-form" class="login-form fade-in-effect" method="POST" action="{{ route('register') }}">
        <div class="form-group{{ $errors->has('email') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="email">{{ __('home.email') }} (*)</label>
            <input type="text" class="form-control" name="email" id="email" autocomplete="off"/>
            @if($errors->has('email'))
                <span class="validate-has-error">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('telephone') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="telephone">{{ __('home.phone-number') }} (*)</label>
            <input type="text" class="form-control" name="telephone" id="telephone" autocomplete="off"/>
            @if($errors->has('telephone'))
                <span class="validate-has-error">{{ $errors->first('telephone') }}</span>
            @endif
        </div>
        <div class="row">
        <div class="col-md-6 col-md-offset-0 col-xs-10 col-xs-offset-1">
        <div class="form-group{{ $errors->has('first_name') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="first_name">{{ __('customer.first_name') }} (*)</label>
            <input type="text" class="form-control" name="first_name" id="first_name" autocomplete="off"/>
            @if($errors->has('first_name'))
                <span class="validate-has-error">{{ $errors->first('first_name') }}</span>
            @endif
        </div>
    </div>
        <div class="col-md-6 col-md-offset-0 col-xs-10 col-xs-offset-1">
        <div class="form-group{{ $errors->has('last_name') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="last_name">{{ __('customer.last_name') }} (*)</label>
            <input type="text" class="form-control" name="last_name" id="last_name" autocomplete="off"/>
            @if($errors->has('last_name'))
                <span class="validate-has-error">{{ $errors->first('last_name') }}</span>
            @endif
        </div>
    </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="password">{{ __('home.password') }} (*)</label>
            <input type="password" class="form-control" name="password" id="password" autocomplete="new-password"/>
            @if($errors->has('password'))
                <span class="validate-has-error">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="password_confirmation">{{ __('home.confirm-password') }} (*)</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" autocomplete="new-password"/>
            @if($errors->has('password_confirmation'))
                <span class="validate-has-error">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('ref_code') ? ' validate-has-error' : '' }}">
            <label class="control-label" for="ref_code">{{ __('home.ref_code') }}</label>
            <input type="text" class="form-control" name="ref_code" id="ref_code" autocomplete="off" placeholder="Optional if you have a referral number"/>
            @if($errors->has('ref_code'))
                <span class="validate-has-error">{{ $errors->first('ref_code') }}</span>
            @endif
        </div>
        <div class="form-group-1 btn-wrap">
                <button type="submit" class="btn btn-success btn-block btn-lg"> {{ __('home.sign-up') }}</button>
                <p class="text-muted text-left">
                  You have an account?
                  <a class="line-height-40px" href="{{ route('customer.login') }}">
                    <i class="fa"></i> {{ __('home.sign-in') }}
                </a>
                </p>
            <div class="clearfix"></div>
        </div>
        {{ csrf_field() }}
    </form>
</div>
</div>
</div>
@endsection

@section('footer')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            // Reveal Login form
            setTimeout(function () {
                $(".fade-in-effect").addClass('in');
            }, 1);
            // Set Form focus
            $("form#login .form-group:has(.form-control):first .form-control").focus();
        });
    </script>
    <script type="text/javascript" src="{{ asset('js/home/user.js') }}"></script>
@endsection
