<?php

return [
    'manage'         => 'Quản lý slider',
    'name'           => 'Tên',
    'thumnail'         => 'Hình ảnh',
    'status'           => 'Trạng thái',
    'order'           => 'Thứ tự',
    'active'        => 'Hoạt động',
    'deactive'          => 'Không hoạt động',
    'edit'      => 'Sửa một slider',
    'create'    => 'Tạo mới một slider',
    'url'       => 'Đường dẫn trang web',
    'lang'      => 'Ngôn ngữ',
    'caption'   => 'Tiêu đề',
    'detail'    => 'Chi tiết'
];