<?php

return [
    'manager-convert'      => 'Quy đổi điểm thưởng',
    'value'        => 'Số điểm',
    'amount'      => 'Giá trị',
    'create' => 'Tạo mới',
    'customer_point_log' => 'Lịch sử điểm thưởng của khách hàng',
    'create_time' => 'Thời gian',
    'type' => 'Loại',
    'transaction_add' => 'Tăng',
    'transaction_sub' => 'Giảm',
    
    'manager-config' => 'Cấu hình điểm thưởng',
    'type_config_customer' => 'Giới thiệu dịch vụ',
    'type_config_order' => 'Thanh toán đơn hàng',
    'is_active' => 'Đang hoạt động',
    'no_active' => 'Không hoạt động',
    'config_type' => 'Loại cấu hình',
    'edit' => 'Sửa',
    'manage' => 'Lịch sử điểm thưởng',
    'use_ref_code' => 'Mã liên quan',
];