<?php

return [
    'country_id'     => 'Quốc gia',
    'create'         => 'Thêm tỉnh thành',
    'edit'           => 'Sửa tỉnh thành',
    'manage'         => 'Tỉnh thành',
    'name'           => 'Tên tỉnh thành',
    'select_country' => 'Chọn quốc gia'
];