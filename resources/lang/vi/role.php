<?php

return [
    'create'      => 'Thêm nhóm người dùng',
    'edit'        => 'Chỉnh nhóm người dùng',
    'manage'      => 'Nhóm người dùng',
    'name'        => 'Tên nhóm',
    'description' => 'Mô tả',
    'permissions' => 'Phân quyền',
    'admin'       => 'Xem tất cả đơn hàng',  
];