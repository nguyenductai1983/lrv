<?php

return [
    'manage'                => 'Quản lý thao tác',
    'order_id'                => 'ID đơn hàng',
    'transport_id'                  => 'ID Transport',
    'mts_id'                  => 'ID Mts',
    'user_id'                  => 'ID người dùng',
    'body'                => 'Nội dung',
    'created_at'             => 'Thời gian tạo'
];