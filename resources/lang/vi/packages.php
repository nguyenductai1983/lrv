<?php

return [
    'type'   => 'Loại gói hàng',
    'quantity'   => 'Số lượng sản phẩm',
    'referenceCode'   => 'Reference Code',
    'dimType'   => 'Đơn vị đo',
    'boxes'   => 'Gói sản phẩm',
    'boxes_length'   => 'Độ dài',
    'boxes_width'   => 'Chiều rộng',
    'boxes_height'   => 'Chiều cao',
    'boxes_weight'   => 'Cân nặng',
    'boxes_cod_value'   => 'Giá trị Cod',
    'description'   => 'Ghi chú',
    'insurance_amount'   => 'Giá trị bảo hiểm',
    'freight_class'   => 'Freight Class',
    'nmfc_code'   => 'nmfc Code',
];