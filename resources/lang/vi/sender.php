<?php

return [
    'first_name'  => 'Tên',
    'middle_name' => 'Tên lót',
    'last_name'   => 'Họ',
    'address'     => 'Địa chỉ',
    'postal_code' => 'Mã bưu chính',
    'telephone'   => 'Điện thoại',
    'cellphone'   => 'Di động',
    'country_id'  => 'Quốc gia',
    'province_id' => 'Tỉnh thành',
    'city_id'     => 'Quận huyện',
    'check' => 'Kiểm tra ',
    'other' => 'Khác',
    'relationship'      => 'Mối quan hệ',
    'aunt' => 'Aunt',
    'brother' => 'Brother',
    'brother_in_law' => 'Brother-in-law',
    'cousin' => 'Cousin',
    'daughter' => 'Daughter',
    'emplyee' => 'Emplyee',
    'emplyer' => 'Emplyer',
    'father' => 'Father',
    'father_in_law' => 'Father-in-law',
    'friend' => 'Friend',
    'grandfather' => 'Grandfather',
    'granmother' => 'Granmother',
    'husband' => 'Husband',
    'mother' => 'Mother',
    'mother_in_law' => 'Mother-in-law',
    'nephew' => 'Nephew',
    'niece' => 'Niece',
    'self' => 'Self',
    'sister' => 'Sister',
    'sister_in_law' => 'Sister-in-law',
    'son' => 'Son',
    'uncle' => 'Uncle',
    'wife' => 'Wife',

];
