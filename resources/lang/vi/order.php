<?php

return [
    'shipping_status_new'            => 'Đơn hàng khởi tạo thành công',
    'shipping_status_pickup'         => 'Hàng đang Pickup',
    'shipping_status_stockin'        => 'Hàng đã nhập kho ở Canada',
    'shipping_status_shipment'       => 'Hàng đã sẵn sàng lên máy bay',
    'shipping_status_stockout'       => 'Hàng đang trên đường bay',
    'shipping_status_local'          => 'Hàng đã nhập kho ở Việt Nam',
    'shipping_status_ready'          => 'Hàng đã sẵn sàng chuyển đến người nhân',
    'shipping_status_carrier'        => 'Hàng trên đường vận chuyển trong nước',
    'shipping_status_done'           => 'Giao hàng thành công',
    'shipping_status_delay'          => 'Hàng nhập kho tại Sài gòn và sẽ được giao sau Tết nguyên đán do lỗi chậm trễ chuyển hàng của hãng bay',
    'goods_in_the_customs'           => 'hàng đang ở Hải Quan',

    'status_new'                    => 'Mới tạo',
    'status_processing'             => 'Đang xử lý',
    'status_cancel'                 => 'Đã bị hủy',
    'status_complete'               => 'Hoàn thành',
    'status_other'                  => 'Tình trạng khác',
    'status_return'                 => 'Hoàn trả',

    'payment_status_part_paid'       => 'Thanh toán một phần',
    'payment_status_full_paid'       => 'Thanh toán đủ',
    'payment_status_not_paid'        => 'Chưa thanh toán',

    'cancel' => 'Hủy đơn hàng',
    'get_shipping_status' => 'Lấy trạng thái vận chuyển',
    'payment' => 'Thanh toán',

    'eshiper_status_ready_shipping' => 'READY FOR SHIPPING',
    'eshiper_status_in_transit' => 'IN TRANSIT',
    'eshiper_status_delivered' => 'DELIVERED',
    'eshiper_status_cancel' => 'CANCELLED',
    'eshiper_status_exception' => 'EXCEPTION',
    'eshiper_status_closed' => 'CLOSED',
    'eshiper_status_ready_checkout_dhlec' => 'READY FOR CHECKOUT',
    'eshiper_status_ready_process_dhlec' => 'READY TO PROCESS',
    'eshiper_status_undefine' => 'NOT COURIERS ORDER',
    'type' => 'Loại'

];
