<?php

return [
    'create'       => 'Thêm tiền tệ',
    'edit'         => 'Edit tiền tệ',
    'manage'       => 'Loại tiền tệ',
    'code'         => 'Mã tiền tệ',
    'name'         => 'Tên tiền tệ',
    'exchange_vnd' => 'Quy đổi sang VNĐ',
    'rate_manage' => 'Quy đổi tiền tệ',
    'rate' => 'Tỉ giá',
    'created_at' => 'Ngày tạo'
];