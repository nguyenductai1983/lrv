<?php

return [
    'manage'            => 'Đơn vị đo lường',
    'messure_unit'      => 'Đơn vị đo lường',
    'code'              => 'Mã',
    'name'              => 'Tên',
    'display_order'     => 'Thứ tự hiển thị',
    'is_active'         => 'Trạng thái',
    'create'            => 'Tạo mới',
    'edit'              => 'Sửa',
    'is_active_true'    => 'Hoạt động',
    'is_active_fail'    => 'Không hoạt động'
];