<?php

return [
    'order'         => 'Mã đơn hàng',
    'agency'           => 'Số hiệu',
    'status'         => 'Trạng thái',
    'date_update'           => 'Ngày ghi chú',
    'note'           => 'Ghi chú'
];