<?php

return [
    'list-claim' => 'Danh sách yêu cầu khiếu nại',
    'request-code' => 'Mã yêu cầu',
    'title' => 'Tiêu đề',
    'detail' => 'Chi tiết khiếu nại',
    'open' => 'Mở khiếu nại',
    'complete' => 'Bạn có chắc chắn muốn hoàn tất khiếu nại này?'
];