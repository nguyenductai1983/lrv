<?php

return [
    'method'                => 'Phương thức thanh toán',
    'code'                  => 'Mã',
    'name'                  => 'Tên',
    'description'           => 'Mô tả',
    'is_active'             => 'Trạng thái',
    'display_order'         => 'Thứ tự hiển thị',
    'create'                => 'Tạo mới',
    'edit'                  => 'Sửa',
    'card_holder'           => 'Chủ TK',
    'account_number'        => 'Số TK',
    'bank_branch'           => 'Chi nhánh',
    'type'                  => 'Loại',
    'type_global'           => 'Tại Cannada',
    'type_local'            => 'Tại Vn',
    'image_file_id'         => 'Hình đại diện'
];