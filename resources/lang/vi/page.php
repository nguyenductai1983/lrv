<?php

return [
    'create'      => 'Tạo trang',
    'edit'        => 'Chỉnh sửa trang',
    'name'        => 'Tên trang',
    'parent'      => 'Trang cha',
    'root'        => 'TRANG GỐC',
    'detail'      => 'Chi tiết',
    'link_to'     => 'Liên kết đến',
    'thumnail' => 'Ảnh đại diện',
    'agencies'    => 'Cài đặt trang cho khu vực'
];
