<?php

return [
    'method'                => 'Phương thức vận chuyển',
    'provider'              => 'Hãng vận chuyển',
    'name'                  => 'Tên',
    'description'           => 'Mô tả',
    'logo'                  => 'Logo',
    'is_active'             => 'Trạng thái',
    'display_order'         => 'Thứ tự hiển thị',
    'create'                => 'Tạo mới',
    'edit'                  => 'Sửa',
    'is_active_true'        => 'Đang hoạt động',
    'is_active_fail'        => 'Không hoạt động',
    'min_weight'           => 'Min Weight',
    'min_fee'              => 'Min Fee',
];
