<?php

return [
    'manage'                => 'Đơn vị kích thước',
    'dimension_unit'        => 'Đơn vị kích thước',
    'code'                  => 'Mã',
    'name'                  => 'Tên',
    'display_order'         => 'Thứ tự hiển thị',
    'is_default'            => 'Là mặc định',
    'radio'                 => 'Tỷ lệ',
    'create'                => 'Tạo mới',
    'edit'                  => 'Sửa'
];