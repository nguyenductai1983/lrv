<?php
return [
 'agencyinfomation'	=>  	'Thông tin đại lý hướng dẫn giới thiệu quí khách',
'code'	=>  	'Mã cửa hàng hướng dẫn bạn do của hàng cung cấp',
'name'	=>  	'Tên cửa hàng hướng dẫn bạn',
'address'	=>  	'Địa chỉ của cửa hàng  hướng dẫn quý khách',
'customer'	=>  	'Thông tin khách hàng',
'sender'	=>  	'Thông tin khách hàng sẽ gửi hàng',
'receiver'  =>      'Thông tin khách hàng sẽ nhận hàng',
'firstname'	=>  	'Tên là thông tin quý khách cần cung cấp chính xác.',
'lastname'	=>  	'Họ là thông tin quý khách cần cung cấp chính xác.',
'postalcode'	=>  	'Mã bưu chính là một chuỗi ký tự viết bằng chữ, hoặc bằng số hay tổ hợp của số và chữ, được viết bổ sung vào địa chỉ nhận thư với mục đích tự động xác định điểm đến cuối cùng của thư tín, bưu phẩm. Mỗi quốc gia có một hệ thống ký hiệu mã bưu chính riêng.',
'shipdate'	=>  	'Ngày yêu cầu lấy hàng',
'email'	=>  	'Địa chỉ hộp thư điện tử',
'descriptionofgoods'	=>  	'Mô tả về kiện đóng gói hàng hóa',
'box'	=>  	'Thùng hàng gồm các món hàng nhỏ được đóng vào 1 thùng',
'measurecontainersize'	=>  	'Kích thước thùng hàng, quí khách vui lòng chiều dài, chiều rộng và chiều cao dùng đơn vị Inch để khai báo',
'lengthin'	=>  	'Chiều dài thùng hàng đo được đơn vị inch',
'widthin'	=>  	'Chiều rộng thùng hàng đo được đơn vị inch',
'heightin'	=>  	'Chiều cao thùng hàng đo được đơn vị inch',
'volumeweight'	=>  	'Trọng lượng kích thước, còn được gọi là trọng lượng thể tích, là một kỹ thuật định giá cho vận tải hàng hóa thương mại, sử dụng trọng lượng ước tính được tính từ chiều dài, chiều rộng và chiều cao của kiện hàng.',
'productcode'	=>  	'Quý khách chọn danh sách mục sản phẩm mà quý khách muốn gửi',
'productname'	=>  	'Sản phẩm quý khách đã chọn, quý khách có thể chọn lại bên cột mã hàng',
'quantity'	=>  	'Số lượng hàng hóa quý khách muốn gửi',
'physicalweight'	=>  	'Trọng lượng thực tế kiện hàng quý khách cân được',
'price'	=>  	'Phí vận chuyển hàng của quý khách',
'unit'	=>  	'Đơn vị hàng hóa(cái , hộp thùng, túi)',
'declaredvalue'	=>  	'Giá trị hàng hóa của quý khách khai báo, dựa vào thông tin này để tính bảo hiểm, phí phụ thu hàng hóa',
'surcharge'	=>  	'Phí phụ thu là thuế, phí đặc biệt cho sản phẩm phải chịu thêm chi phí khi về đến Việt Nam, phí này được dùng để chi trả các thủ tục thông quan nhập khẩu hàng hoá vào Việt Nam chứ không phải là chi phí dịch vụ của công ty',
'insurance'	=>  	'Bảo hiểm đền bù cho hàng hóa trong trường hợp sự số ngoài ý muốn. Nó là một hình thức quản lý rủi ro, chủ yếu được sử dụng để phòng ngừa rủi ro.',
'warehouses'	=>  	'Kho nơi gần nhất sẽ nhận hàng của quý khách, quý khách chọn kho gần nhất để được giá tốt.',
'pickupinformation'	=>  	'Thông tin cung cấp để nhân viên lấy hàng liên hệ với quý khách.',
'discount'          =>      'Chiết khấu',
'total'              =>     'Tổng tiền quí khách cần thanh toán',
'note'               =>     'Ghi chú, nội dung hàng hóa',
'pickupinfomation'    =>	'Thông tin cần liên hệ khi nhân viên đến lấy hàng',
'contactname'      =>  	'Tên liên hệ',
'telephonepickup'	    =>  'Số điện thoại cần cung cấp cho nhân viên lấy hàng',
'locationpickup'	    =>  'địa điểm nhận hàng của bạn là..., vui lòng chọn trong danh sách',
'pickupdate'	    =>  'Ngày quí khách hẹn nhân viên đến lấy hàng',
'pickuptime'	    =>  'Thời gian bắt đầu bạn có thể giao hàng cho nhân viên nhận hàng',
'closingtime'	    =>  'Hết thời gian bạn có thể gặp nhân viên lấy hàng',
'order_detail'	    =>    'xem bảng dữ liệu ',
'order_edit'	    =>    'Xem và chỉnh sửa PO',
'order_cancel'	    =>    'Hủy đơn hàng',
'order_restore'	    =>    'Phục hồi đơn hàng đã xóa',
'print_bill'	    =>    'In hóa đơn',
'order_tracking'	    =>    'Hành trình đơn hàng',
'extra_fee'	    =>    'Phí thu thêm',
'refund_fee'	    =>    'Tiền trả lại',
'print_label_receiver'	    =>    'In nhãn nhận hàng',
];
