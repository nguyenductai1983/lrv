<?php

return [
    'manage'    => 'Kho hàng',
    'is_default'          => 'Là mặc định',
    'edit'                => 'Sửa thông tin kho hàng',
    'type_local'          => 'Kho nội địa',
    'type_global'         => 'Kho quốc tế',
];