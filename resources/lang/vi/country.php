<?php

return [
    'code'   => 'Mã quốc gia',
    'create' => 'Thêm quốc gia',
    'edit'   => 'Sửa quốc gia',
    'manage' => 'Quốc gia',
    'name'   => 'Tên quốc gia'
];