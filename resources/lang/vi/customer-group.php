<?php

return [
    'create'      => 'Thêm nhóm khách hàng',
    'edit'        => 'Chỉnh sửa nhóm khách hàng',
    'manage'      => 'Nhóm khách hàng',
    'name'        => 'Tên nhóm',
    'code'        => 'Mã nhóm',
    'description' => 'Mô tả'
];