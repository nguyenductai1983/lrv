<?php

return [
    'country_id'      => 'Quốc gia',
    'create'          => 'Thêm quận huyện',
    'edit'            => 'Sửa quận huyện',
    'manage'          => 'Quận huyện',
    'name'            => 'Tên quận huyện',
    'province_id'     => 'Tỉnh thành',
    'shipping_fee'    => 'Phí vận chuyển',
    'select_country'  => 'Chọn quốc gia',
    'select_province' => 'Chọn tỉnh thành',
    'select_city' => 'Chọn Quận huyện',
    'wards' => 'Phường xã',
    'ward_edit' => 'Chỉnh Phường Xã',
    'ward_name' => 'Tên Phường xã',
    'vnpost_alias' => 'Mã VNPT',
    'create_ward'  => 'Thêm Phường Xã',
];
