<?php

return [
    'manage'         => 'Cấu hình',
    'percent'        => 'Mức (%)',
    'quota'          => 'Định mức ($)',
    'agency_percent' => 'Chiết khấu cho Đại lý (%)',
    'agency_quota'   => 'Định mức cho Đại lý ($)',
    'item'           => 'Mục',
    'item_1'         => 'Phụ thu',
    'item_2'         => 'Bảo hiểm',
];