<?php

return [
    'create'         => 'Thêm khu vực đại lý',
    'edit'           => 'Chỉnh sửa khu vực đại lý',
    'manage'         => 'Khu vực đại lý',
    'code'           => 'Mã khu vực đại lý',
    'name'           => 'Tên khu vực đại lý',
    'address'        => 'Địa chỉ',
    'phone'          => 'Số điện thoại',
    'note'           => 'Ghi chú',
    'products_price' => 'Giá sản phẩm'
];