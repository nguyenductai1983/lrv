<?php

return [
    'create'      => 'Tạo nhóm hàng',
    'code'        => 'Mã nhóm',
    'edit'        => 'Chỉnh sửa nhóm hàng',
    'manage'      => 'Nhóm hàng',
    'name'        => 'Tên nhóm',
    'thumbnail'   => 'Hình ảnh',
    'description' => 'Mô tả'
];