<?php

return [
    'manage'         => 'Tin tức',
    'slug'           => 'Slug',
    'link_to'         => 'Đường dẫn',
    'order'           => 'Thứ tự',
    'created_at'           => 'Thời gian tạo',
    'create'        => 'Tạo mới',
    'bulletin'        => 'Bulletin'
];
