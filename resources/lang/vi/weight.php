<?php

return [
    'manage'            => 'Đơn vị cân nặng',
    'weight_unit'       => 'Đơn vị cân nặng',
    'code'              => 'Mã',
    'name'              => 'Tên',
    'display_order'     => 'Thứ tự hiển thị',
    'is_default'        => 'Là mặc định',
    'radio'             => 'Tỷ lệ',
];