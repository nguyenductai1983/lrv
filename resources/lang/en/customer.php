<?php

return [
    'create'                => 'Create customer',
    'edit'                  => 'Edit customer',
    'manage'                => 'Customer',
    'code'                  => 'Customer code',
    'area_id'               => 'Area',
    'customer_group_id'     => 'Customer group',
    'image_1_file_id'       => 'Image 1',
    'image_2_file_id'       => 'Image 2',
    'image_3_file_id'       => 'Image 3',
    'first_name'            => 'First name',
    'middle_name'           => 'Middle name',
    'last_name'             => 'Last name',
    'address_1'             => 'Address 1',
    'address_2'             => 'Address 2',
    'telephone'             => 'Telephone',
    'cellphone'             => 'Cellphone',
    'postal_code'           => 'Postal code',
    'id_card'               => 'ID card',
    'id_card_required'      => 'ID card required',
    'card_expire'           => 'Card expire',
    'card_is_expire'        => 'Card is expire',
    'card_isssed'           => 'Card Isssed',
    'birthday'              => 'Birthday',
    'date_issued'           => 'Date Issued',
    'career'                => 'Career',
    'country_id'            => 'Country',
    'province_id'           => 'Province',
    'city_id'               => 'City',
    'ward_id'               => 'Ward',
    'select_area'           => 'Select area',
    'select_customer_group' => 'Select customer group',
    'select_country'        => 'Select country',
    'select_province'       => 'Select province',
    'select_city'           => 'Select city',
    'select_ward'           => 'Select Ward',
    'name'                  => 'Name',
    'group'                 => 'Group',
    'id'                    => 'Customer Id',
    'email'                 => 'Email',

    'signup-email-title' => 'Account registration successful',
    'signup-success' => 'We sent you an email confirmation',
    'not-found-token' => 'Not found token',
    'forgot-password-susscess' => 'You have successfully recovered your password',
    'forgot-email-title' => 'Forgot password',
    'forgot-email-susscess' => 'Please check the email follow the instructions to recover your password',
    'point' => 'Point',
    'check' => 'Check ',
    'full_name' => 'Fullname',
    'city' => 'City',
    'province' => 'Province',
    'country' => 'Country',
    'manage_address' => 'Manage address of',
    'address_edit' => 'Edit address',
    'address_create' => 'Add address',

    'cog_label' => 'Config Member VIP',
    'member_vip' => 'Manager config Member VIP',
    'type_service' => 'Type service',
    'discount' => 'Discount',
    'qoute-shop' => 'Shop for me',
    'qoute-ship' => 'Ship for me',
    'qoute-type' => 'Type',
    'change_profile'=> 'Update your profile',
    'old_password' => 'Current password',
    'password' => 'Password',

    'other' => 'Other',
    'career'    => 'Career',
    'accountant' => 'Accountant',
    'building_sector' => 'Building sector',
    'carpenter' => 'Carpenter',
    'cashier' => 'Cashier',
    'cnc_machine_operator' => 'CNC machine operator',
    'cnc_operator' => 'CNC operator',
    'electrical_technician' => 'Electrical technician',
    'executive_manager' => 'Executive manager',
    'factory_labourer' => 'Factory labourer',
    'financial_consultant' => 'Financial consultant',
    'forklift_operator' => 'Forklift operator',
    'grocery_store_manager' => 'Grocery store manager',
    'house_builder' => 'House builder',
    'laywer' => 'Laywer',
    'line_operator' => 'Line operator',
    'nail_technician' => 'Nail technician',
    'nurse' => 'Nurse',
    'nursing_assistant' => 'Nursing assistant',
    'pharmacy' => 'Pharmacy',
    'plumber' => 'Plumber',
    'retail_sales' => 'Retail sales',
    'retired' => 'RETIRED',
    'secretary' => 'Secretary',
    'self-employed' => 'SELF-EMPLOYED',
    'shop_labourer' => 'Shop labourer',
    'teacher' => 'Teacher',
    'truck_driver' => 'Truck driver',
];
