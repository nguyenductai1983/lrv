<?php

return [
    'create'      => 'Add area',
    'edit'        => 'Edit area',
    'manage'      => 'Area',
    'name'        => 'Area name',
    'code'        => 'Area code',
    'description' => 'Description'
];