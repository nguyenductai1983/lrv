<?php

return [
    'code'   => 'Country code',
    'create' => 'Add country',
    'edit'   => 'Edit country',
    'manage' => 'Country',
    'name'   => 'Country name'
];