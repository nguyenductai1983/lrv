<?php

return [
    'create'      => 'Create product group',
    'code'        => 'Group code',
    'edit'        => 'Edit product group',
    'description' => 'Description',
    'manage'      => 'Product group',
    'thumbnail'   => 'Picture',
    'name'        => 'Name',
];