<?php

return [
    'create'      => 'Add user group',
    'edit'        => 'Edit user group',
    'manage'      => 'User group',
    'name'        => 'Group name',
    'description' => 'Description',
    'permissions' => 'Permissions',
    'admin'       => 'Manager All PO',  
];