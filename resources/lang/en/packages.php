<?php

return [
    'type'   => 'Package Type',
    'quantity'   => 'Package Quantity',
    'referenceCode'   => 'Reference Code',
    'dimType'   => 'Dim Type',
    'boxes'   => 'Package Boxes',
    'boxes_length'   => 'Product Length',
    'boxes_width'   => 'Product Width',
    'boxes_height'   => 'Product Height',
    'boxes_weight'   => 'Product Weight',
    'boxes_cod_value'   => 'Product Cod Value',
    'description'   => 'Product Description',
    'insurance_amount'   => 'Product Insurance Amount',
    'freight_class'   => 'Freight Class',
    'nmfc_code'   => 'nmfc Code',

];