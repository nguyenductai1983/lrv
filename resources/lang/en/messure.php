<?php

return [
    'manage'            => 'Messure Unit',
    'messure_unit'      => 'Messure Unit',
    'code'              => 'Code',
    'name'              => 'Name',
    'display_order'     => 'Display Order',
    'is_active'         => 'Status',
    'create'            => 'Add new',
    'edit'              => 'Edit',
    'is_active_true'    => 'Active',
    'is_active_fail'    => 'Deactive'
];