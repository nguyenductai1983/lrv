<?php

return [
    'manage'    => 'Warehouse',
    'is_default'          => 'Is Default',
    'edit'                => 'Edit info warehouse',
    'type_local'          => 'Local',
    'type_global'         => 'International',
];