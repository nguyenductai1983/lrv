<?php

return [
    'country_id'      => 'Country',
    'create'          => 'Add city',
    'edit'            => 'Edit city',
    'manage'          => 'City',
    'name'            => 'City name',
    'province_id'     => 'Province',
    'shipping_fee'    => 'Shipping fee',
    'select_country'  => 'Select country',
    'select_province' => 'Select province',
    'select_city' => 'Select City',
    'wards' => 'Ward',
    'ward_edit' => 'Ward Edit',
    'ward_name' => 'Ward Name',
    'vnpost_alias' => 'Vnpost Alias',
    'create_ward'          => 'Add Ward',
];
