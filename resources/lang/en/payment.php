<?php

return [
    'method'                => 'Payment Method',
    'code'                  => 'Code',
    'name'                  => 'Name',
    'description'           => 'Description',
    'is_active'             => 'Status',
    'display_order'         => 'Display Order',
    'create'                => 'Add new',
    'edit'                  => 'Edit',
    'card_holder'           => 'Card Holder',
    'account_number'        => 'Account Number',
    'bank_branch'           => 'Branch',
    'type'                  => 'Tye',
    'type_global'           => 'In Cannada',
    'type_local'            => 'In Vn',
    'image_file_id'         => 'Icon'
];