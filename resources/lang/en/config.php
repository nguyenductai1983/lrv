<?php

return [
    'manage'         => 'Configs',
    'percent'        => 'Percent (%)',
    'quota'          => 'Quota ($)',
    'agency_percent' => 'Discount for Agency (%)',
    'agency_quota'   => 'Quota for Agency ($)',
    'item'           => 'Item',
    'item_1'         => 'Surcharge',
    'item_2'         => 'Insurance',
];