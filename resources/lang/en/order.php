<?php

return [
    'shipping_status_new'            => 'New',
    'shipping_status_pickup'         => 'Ready Pickup',
    'shipping_status_stockin'        => 'Warehousing in Canada',
    'shipping_status_shipment'       => 'Ready for departure',
    'shipping_status_stockout'       => 'In transit by air',
    'shipping_status_local'          => 'Warehousing in Vietnam',
    'shipping_status_ready'          => 'Ready for local transportation',
    'shipping_status_carrier'        => 'In transit to the destination',
    'shipping_status_done'           => 'Delivery',
    'shipping_status_delay'          => 'Warehing in Saigon and will be transited for delivery after Tet lunar year due to the late arrival of the freighter',
    'goods_in_the_customs'           => 'on hold at Vietnam Customs',

    'status_new'                    => 'New',
    'status_processing'             => 'Processing',
    'status_cancel'                 => 'Cancel',
    'status_complete'               => 'Complete',
    'status_other'                  => 'Other',
    'status_return'                 => 'Return',

    'payment_status_part_paid'       => 'Part Paid',
    'payment_status_full_paid'       => 'Full Paid',
    'payment_status_not_paid'        => 'UnPaid',

    'cancel' => 'Cancel Order',
    'get_shipping_status' => 'Get shipping',
    'payment' => 'Payment',

    'eshiper_status_ready_shipping' => 'READY FOR SHIPPING',
    'eshiper_status_in_transit' => 'IN TRANSIT',
    'eshiper_status_delivered' => 'DELIVERED',
    'eshiper_status_cancel' => 'CANCELLED',
    'eshiper_status_exception' => 'EXCEPTION',
    'eshiper_status_closed' => 'CLOSED',
    'eshiper_status_ready_checkout_dhlec' => 'READY FOR CHECKOUT',
    'eshiper_status_ready_process_dhlec' => 'READY TO PROCESS',
    'eshiper_status_undefine' => 'NOT COURIERS ORDER',
    'type' => 'Type'
];
