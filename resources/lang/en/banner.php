<?php

return [
    'manage'         => 'Manage slider',
    'name'           => 'Name',
    'thumnail'         => 'Image',
    'status'           => 'Status',
    'order'           => 'Order',
    'active'        => 'Active',
    'deactive'          => 'Deactive',
    'edit' 			=> 'Edit a slider',
    'create'		 => 'Add a slider',
    'lang' 			=> 'Language',
    'caption'	 => 'Caption',
    'detail' 		=> 'Detail',
    'lang_vi'      => 'Vietnam',
    'lang_en'      => 'English',
    'caption'   => 'Caption',
    'detail'    => 'Detail',
];