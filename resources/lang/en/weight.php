<?php

return [
    'manage'        => 'Weight',
    'code'           => 'Code',
    'name'           => 'Name',
    'display_order'        => 'Display Order',
    'is_default'          => 'Is Default',
    'radio'           => 'Radio',
];