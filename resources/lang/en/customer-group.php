<?php

return [
    'create'      => 'Create customer group',
    'edit'        => 'Edit customer group',
    'manage'      => 'Customer group',
    'name'        => 'Group name',
    'code'        => 'Group code',
    'description' => 'Description'
];