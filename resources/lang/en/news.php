<?php

return [
    'manage'         => 'News',
    'slug'           => 'Slug',
    'link_to'         => 'Link to',
    'order'           => 'Order',
    'created_at'           => 'Create time',
    'create'        => 'Create',
    'bulletin'        => 'Bulletin'
];
