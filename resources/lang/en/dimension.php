<?php

return [
    'manage'                => 'Dimension Unit',
    'dimension_unit'        => 'Dimension Unit',
    'code'                  => 'Code',
    'name'                  => 'Name',
    'display_order'         => 'Display Order',
    'is_default'            => 'Is Default',
    'radio'                 => 'Radio',
    'create'                => 'Add new',
    'edit'                  => 'Edit'
];