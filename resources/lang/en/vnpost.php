<?php

return [
    'order'         => 'Order code',
    'agency'           => 'Agency',
    'status'         => 'Status',
    'date_update'           => 'Date',
    'note'           => 'Note'
];