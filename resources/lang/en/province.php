<?php

return [
    'country_id'     => 'Country',
    'create'         => 'Add province',
    'edit'           => 'Edit province',
    'manage'         => 'Province',
    'name'           => 'Province name',
    'select_country' => 'Select country'
];