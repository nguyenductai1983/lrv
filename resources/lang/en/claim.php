<?php

return [
    'list-claim' => 'Complaints list',
    'request-code' => 'Request code',
    'title' => 'Title',
    'detail' => 'View detail claim',
    'open' => 'Open claim',
    'complete' => 'Are you sure you want to complete this complaint?'
];