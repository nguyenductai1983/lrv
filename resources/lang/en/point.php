<?php

return [
    'manager-convert'      => 'Convert point',
    'value'        => 'Point',
    'amount'      => 'Amount',
    'is_active'        => 'Status',
    'create' => 'Create',
    'customer_point_log' => 'History point log',
    'create_time' => 'Date time',
    'type' => 'Type',
    'transaction_add' => 'Plus',
    'transaction_sub' => 'Reduced',
    
    'manager-config' => 'Config point',
    'type_config_customer' => 'Guide service',
    'type_config_order' => 'Payment order',
    'is_active' => 'Active',
    'no_active' => 'Deactive',
    'config_type' => 'Type',
    'edit' => 'Edit',
    'manage' => 'History point',
    'use_ref_code' => 'Ref code',
];