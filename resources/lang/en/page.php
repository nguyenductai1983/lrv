<?php

return [
    'create'      => 'Create page',
    'edit'        => 'Edit page',
    'name'        => 'Page name',
    'parent'      => 'Parent page',
    'root'        => 'ROOT PAGE',
    'detail'      => 'Detail',
    'link_to'     => 'Link to',
    'thumnail'    => 'Thumnail',
    'agencies'    => 'Page to Agencies'
];
