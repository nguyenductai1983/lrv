<?php

return [
    'manage'                => 'Manage Log',
    'order_id'                => 'Order ID',
    'transport_id'                  => 'Transport ID',
    'mts_id'                  => 'MTS ID',
    'user_id'                  => 'User ID',
    'body'                => 'Body',
    'created_at'             => 'Create Date'
];