<?php

return [
    'create'         => 'Add agent area',
    'edit'           => 'Edit agent area',
    'manage'         => 'Agent area',
    'code'           => 'Code',
    'name'           => 'Name',
    'address'        => 'Address',
    'phone'          => 'Phone',
    'note'           => 'Note',
    'products_price' => 'Products price'
];