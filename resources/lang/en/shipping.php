<?php

return [
    'method'                => 'Shipping Method',
    'provider'              => 'Carrier',
    'name'                  => 'Name',
    'description'           => 'Description',
    'logo'                  => 'Logo',
    'is_active'             => 'Status',
    'display_order'         => 'Display Order',
    'create'                => 'Add new',
    'edit'                  => 'Edit',
    'is_active_true'        => 'Active',
    'is_active_fail'        => 'Deactive',
    'min_weight'           => 'Min Weight',
    'min_fee'              => 'Min Fee',
];
