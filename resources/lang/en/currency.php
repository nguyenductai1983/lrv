<?php

return [
    'create'       => 'Add currency',
    'edit'         => 'Edit currency',
    'manage'       => 'Currencies',
    'code'         => 'Code',
    'name'         => 'Name',
    'exchange_vnd' => 'Exchange to VND',
    'rate_manage' => 'Rate currency',
    'rate' => 'Rate',
    'created_at' => 'Created at'
];