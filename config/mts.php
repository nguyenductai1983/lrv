<?php

return [
    'transaction_type' => [1, 2, 3],
    'status' => [1, 2, 4],
    'status_full' => [1, 2, 3, 4],
    'payment_by'     => [1, 2, 3],
    'discount_type' => [1, 2],
    'per_agency_fee' => 0.3,
    'per_fee' => 0.02,
    'max_per_fee' => 0.025,
    'province_min_fee' => 205,
    'fix_fee' => 5,
    'mts1500' => 1500,
    'fix_fee1500' => 5,
    'mts3000' => 3000,
    'fix_fee3000' => 10,
    'reason' => ['buy_real_estate','debt_loan_mortgage_payments',
    'donation','education','family_assitance','house_construction','investment',
    'medical_expenses','saving','travel']
];
