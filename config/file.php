<?php

return [
    'invalid'      => 0,
    'valid'        => 1,
    'img_mimes'    => 'jpeg,png,pdf',
    'img_max_size' => 5120
];