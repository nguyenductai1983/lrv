@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default transport-content" ng-app="MtsApp" ng-controller="MtsEditController"
    style="padding: 15px;">
    <div class="transport-content-title panel-heading">
        {{ __('mts.edit') }}
    </div>
    <div class="panel-body ng-cloak">
        <form id="form-edit" ng-submit="updateMts()" novalidate>
            <div class="panel panel-color panel-gray panel-border">
                <div class="panel-body" style="margin: -10px -20px;">
                    <table class="table table-form">
                        <tbody>
                            <tr>
                                <td width="50%" style="border-right: 1px solid #ccc;">
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.sender') }}</h3>

                                    <table class="table table-form">
                                        <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="20%">
                                                    <input type="text" class="form-control" name="customer.first_name"
                                                        ng-model="customer.first_name" ng-disabled="customer.id">
                                                </td>
                                                <td width="15%" class="col-label">
                                                    {{ __('customer.middle_name') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="customer.middle_name"
                                                        ng-model="customer.middle_name" ng-disabled="customer.id">
                                                </td>
                                                <td width="10%" class="col-label">
                                                    {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="customer.last_name"
                                                        ng-model="customer.last_name" ng-disabled="customer.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="customer.address_1"
                                                        ng-model="customer.address_1" ng-disabled="customer.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('customer.email') }}
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="customer.email"
                                                        ng-model="customer.email" ng-disabled="customer.id">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="customer.country_id" class="form-control"
                                                        ng-model="customer.country_id"
                                                        ng-change="getProvincesCustomer()" ng-disabled="customer.id">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">
                                                            @{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="customer.province_id" class="form-control"
                                                        ng-model="customer.province_id" ng-change="getCitiesCustomer()"
                                                        ng-disabled="customer.id">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesCustomer"
                                                            ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="customer.city_id" class="form-control"
                                                        ng-model="customer.city_id" ng-disabled="customer.id">
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesCustomer" ng-value="city.id">
                                                            @{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('customer.postal_code') }}<i
                                                        class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control" name="customer.postal_code"
                                                        ng-model="customer.postal_code" ng-disabled="customer.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="customer.telephone"
                                                        ng-model="customer.telephone" ng-disabled="customer.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="col-label" style="padding-bottom: 10px;">
                                                    <button type="button" class="btn btn-info btn-xs"
                                                        ng-click="showMoreInfoCustomer = !showMoreInfoCustomer">
                                                        <i class="fa fa-plus"></i> {{ __('label.more_info') }}
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.id_card') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="customer.id_card"
                                                        ng-model="customer.id_card" ng-change="checkInfoCustomer()">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.card_expire') }}
                                                </td>
                                                <td>
                                                    <div class="input-group date-picker" data-change-year="true"
                                                        data-change-month="true"
                                                        data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                        <input type="text" class="form-control"
                                                            name="customer.card_expire" ng-model="customer.card_expire"
                                                            ng-change="checkInfoCustomer()">

                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-white">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.birthday') }}
                                                </td>
                                                <td>
                                                    <div class="input-group date-picker" data-change-year="true"
                                                        data-change-month="true"
                                                        data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                                                        <input type="text" class="form-control" name="customer.birthday"
                                                            ng-model="customer.birthday"
                                                            ng-change="checkInfoCustomer()">

                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-white">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.career') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="customer.career"
                                                        ng-model="customer.career" ng-change="checkInfoCustomer()">
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.image_1_file_id') }}
                                                </td>
                                                <td>
                                                    @include('partials.form-controls.image', ['field' =>
                                                    'customer.image_1_file_id', 'file' => null])
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.image_2_file_id') }}
                                                </td>
                                                <td>
                                                    @include('partials.form-controls.image', ['field' =>
                                                    'customer.image_2_file_id', 'file' => null])
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.image_3_file_id') }}
                                                </td>
                                                <td>
                                                    @include('partials.form-controls.image', ['field' =>
                                                    'customer.image_3_file_id', 'file' => null])
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>

                                    <div style="float: right;position: absolute;right: 17px;top: 25px;"
                                        ng-if="addressSearchResult">
                                        <select class="form-control" ng-model="selectedId"
                                            ng-change="pickAddress(selectedId)">
                                            <option value="">{{ __('label.select_address') }}</option>
                                            <option ng-repeat="item in addressSearchResult" ng-value="item.id">
                                                @{{item.last_name}} @{{item.middle_name}} @{{item.first_name}}</option>
                                        </select>
                                    </div>
                                    <table class="table table-form">
                                        <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="20%">
                                                    <input type="text" class="form-control" name="receiver.first_name"
                                                        ng-model="address.first_name" ng-disabled="address.id">
                                                </td>
                                                <td width="15%" class="col-label">
                                                    {{ __('receiver.middle_name') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="receiver.middle_name"
                                                        ng-model="address.middle_name" ng-disabled="address.id">
                                                </td>
                                                <td width="10%" class="col-label">
                                                    {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="receiver.last_name"
                                                        ng-model="address.last_name" ng-disabled="address.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('receiver.address') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="receiver.address"
                                                        ng-model="address.address_1" ng-disabled="address.id">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="receiver.country_id" class="form-control"
                                                        ng-model="address.country_id" ng-change="getCitiesReceiver()"
                                                        ng-disabled="address.id">
                                                        <option value="">{{ __('label.select_address') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">
                                                            @{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="receiver.province_id" class="form-control"
                                                        ng-model="address.province_id"
                                                        ng-change="getProvincesReceiver()" ng-disabled="address.id">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesReceiver"
                                                            ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="receiver.city_id" class="form-control"
                                                        ng-model="address.city_id" ng-change="updateContainers()"
                                                        ng-disabled="address.id">
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesReceiver" ng-value="city.id">
                                                            @{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                                <td>
                                                    <input type="text" class="form-control" name="receiver.post_code"
                                                        ng-model="address.postal_code" ng-disabled="address.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="receiver.telephone"
                                                        ng-model="address.telephone" ng-disabled="address.id">
                                                </td>

                                                <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                                <td>
                                                    <input type="text" class="form-control" name="receiver.cellphone"
                                                        ng-model="address.cellphone" ng-disabled="address.id">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">{{__('receiver.relationship')}}</td>
                                                <td>
                                                    <select id="relationship" class="form-control"
                                                        ng-model="address.relationship"
                                                        ng-change="select(receiver.relationship,'relationship_other')"
                                                        ng-disabled="Updatereciver">
                                                        <option value='{{__('receiver.aunt')}}'>{{__('receiver.aunt')}}
                                                        </option>
                                                        <option value='{{__('receiver.brother')}}'>
                                                            {{__('receiver.brother')}}</option>
                                                        <option value='{{__('receiver.brother_in_law')}}'>
                                                            {{__('receiver.brother_in_law')}}</option>
                                                        <option value='{{__('receiver.cousin')}}'>
                                                            {{__('receiver.cousin')}}</option>
                                                        <option value='{{__('receiver.daughter')}}'>
                                                            {{__('receiver.daughter')}}</option>
                                                        <option value='{{__('receiver.emplyee')}}'>
                                                            {{__('receiver.emplyee')}}</option>
                                                        <option value='{{__('receiver.emplyer')}}'>
                                                            {{__('receiver.emplyer')}}</option>
                                                        <option value='{{__('receiver.father')}}'>
                                                            {{__('receiver.father')}}</option>
                                                        <option value='{{__('receiver.father_in_law')}}'>
                                                            {{__('receiver.father_in_law')}}</option>
                                                        <option value='{{__('receiver.friend')}}'>
                                                            {{__('receiver.friend')}}</option>
                                                        <option value='{{__('receiver.grandfather')}}'>
                                                            {{__('receiver.grandfather')}}</option>
                                                        <option value='{{__('receiver.granmother')}}'>
                                                            {{__('receiver.granmother')}}</option>
                                                        <option value='{{__('receiver.husband')}}'>
                                                            {{__('receiver.husband')}}</option>
                                                        <option value='{{__('receiver.mother')}}'>
                                                            {{__('receiver.mother')}}</option>
                                                        <option value='{{__('receiver.mother_in_law')}}'>
                                                            {{__('receiver.mother_in_law')}}</option>
                                                        <option value='{{__('receiver.nephew')}}'>
                                                            {{__('receiver.nephew')}}</option>
                                                        <option value='{{__('receiver.niece')}}'>
                                                            {{__('receiver.niece')}}</option>
                                                        <option value='{{__('receiver.self')}}'>{{__('receiver.self')}}
                                                        </option>
                                                        <option value='{{__('receiver.sister')}}'>
                                                            {{__('receiver.sister')}}</option>
                                                        <option value='{{__('receiver.sister_in_law')}}'>
                                                            {{__('receiver.sister_in_law')}}</option>
                                                        <option value='{{__('receiver.son')}}'>{{__('receiver.son')}}
                                                        </option>
                                                        <option value='{{__('receiver.uncle')}}'>
                                                            {{__('receiver.uncle')}}</option>
                                                        <option value='{{__('receiver.wife')}}'>{{__('receiver.wife')}}
                                                        </option>
                                                        <option value="">{{__('receiver.other')}}</option>
                                                        <option value="" hidden>{{ __('label.choose') }}</option>
                                                    </select>
                                                </td>
                                                <td colspan="2">
                                                    <input ng-model="address.relationship" class="form-control"
                                                        type="text" id="relationship_other"
                                                        ng-disabled="Updatereciver" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-color panel-gray panel-border">
                <div class="panel-heading">
                    {{ __('mts.details') }}
                </div>
                <div class="panel-body" style="margin: -10px -20px;">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="col-middle text-right">
                                    {{ __('mts.reason') }}:
                                </td>
                                <td colspan="5">
                                    <input type="text" class="form-control" ng-model="container.reason">
                                </td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.date_pay') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control" ng-model="container.pay_date"
                                        disabled="disabled" value="">
                                </td>
                                <td class="col-middle text-right">{{ __('label.currency_pay') }}:</td>
                                <td class="col-middle text-left">
                                    <select class="form-control" ng-model="container.currency">
                                        <option ng-repeat="currency in currencies" ng-value="currency.id">
                                            @{{ currency.name }}</option>
                                    </select>
                                </td>
                                <td class="col-middle text-right">{{ __('mts.transaction_type') }}:</td>
                                <td class="col-middle text-left">
                                    <select class="form-control" ng-model="container.transaction_type">
                                        <option ng-repeat="transaction_type in transaction_types"
                                            ng-value="transaction_type.key">@{{ transaction_type.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                                <td class="col-middle text-left">
                                    <select class="form-control" ng-model="container.payment_by">
                                        <option ng-repeat="payment_by in payment_bys" ng-value="payment_by.key">
                                            @{{ payment_by.name }}</option>
                                    </select>
                                </td>
                                <td class="col-middle text-right">{{ __('label.total_amount_mts') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control" ng-model="container.amount"
                                        ng-change="updateFee(container)">
                                </td>
                                <td class="col-middle text-right">{{ __('label.total_discount') }}:</td>
                                <td class="col-middle text-left">
                                    <select class="form-control" ng-model="container.discount_type"
                                        ng-change="updateFee(container)"
                                        style="display: block;width: 65px;float: left;margin-right: 10px;">
                                        <option ng-repeat="discount_type in discount_types"
                                            ng-value="discount_type.key">@{{ discount_type.name }}</option>
                                    </select>
                                    <input type="text" class="form-control" ng-model="container.discount_number"
                                        style="width:200px;" ng-change="updateFee(container)">
                                </td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.status') }}</td>
                                <td class="col-middle text-left">
                                    <select class="form-control" ng-model="container.status" disabled="disabled">
                                        <option ng-repeat="status in statuses" ng-value="status.key">@{{ status.name }}
                                        </option>
                                    </select>
                                </td>
                                <td class="col-middle text-right">{{ __('mts.transfer_fee') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control" ng-model="container.transfer_fee">
                                </td>
                                <td class="col-middle text-right">{{ __('label.total_pay') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control" ng-model="container.total">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="text-right">
                <a href="{{ route('mts.quote.index') }}" class="btn btn-white">
                    <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                </a>
                <button type="button" class="btn btn-danger" ng-if="bntCancel" ng-click="cancelOrder()">
                    <i class="fa fa-times"></i> {{ __('order.cancel') }} <i class="fa fa-refresh fa-spin"
                        ng-if="submittedCancel"></i>
                </button>
                <button type="submit" class="btn btn-success" ng-if="bntConfirm" ng-click="approve()"
                    ng-disabled="submittedConfirm">
                    <i class="fa fa-check"></i> {{ __('label.approve') }} <i class="fa fa-refresh fa-spin"
                        ng-if="submittedConfirm"></i>
                </button>
                <button type="submit" class="btn btn-success" ng-if="bntPayment" ng-click="payment()"
                    ng-disabled="submittedPayment">
                    <i class="fa fa-money"></i> {{ __('order.payment') }} <i class="fa fa-refresh fa-spin"
                        ng-if="submittedPayment"></i>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script
    src="{{ asset('js/admin/app/mts-quote-edit.js?t=' . File::lastModified(public_path('js/admin/app/mts-quote-edit.js'))) }}">
</script>
<script>
    $(function () {
            var scope = angular.element('#form-edit').scope();
            scope.id = {!! request()->route('id') !!};
            // scope.mts_per_fee = {!! config('mts.per_fee') !!};
            // scope.fix_fee = {!! config('mts.fix_fee') !!};
            scope.vnPriority = {!! json_encode(config('app.vn_priority')) !!};
            // scope.mts_max_per_fee = {!! config('mts.max_per_fee') !!};
            // scope.mts1500 = {!! config('mts.mts1500') !!};
            // scope.fix_fee1500 = {!! config('mts.fix_fee1500') !!};
            // scope.mts3000 = {!! config('mts.mts3000') !!};
            // scope.fix_fee3000 = {!! config('mts.fix_fee3000') !!};
            scope.MTSSurcharge = {!! $MTSSurcharge !!};
            scope.getMts();

            scope.transaction_types = [];
            scope.statuses = [];
            scope.payment_bys = [];
            scope.discount_types = [];

            <?php foreach(config('mts.transaction_type') as $transaction_type): ?>
            scope.transaction_types.push({
                key : parseInt({!! $transaction_type !!}),
                name: '{!! __('label.transaction_type_' . $transaction_type) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.status_full') as $status): ?>
            scope.statuses.push({
                key : parseInt({!! $status !!}),
                name: '{!! __('label.mts_status_' . $status) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.payment_by') as $payment_by): ?>
            scope.payment_bys.push({
                key : parseInt({!! $payment_by !!}),
                name: '{!! __('label.payment_by' . $payment_by) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.discount_type') as $discount_type): ?>
            scope.discount_types.push({
                key : parseInt({!! $discount_type !!}),
                name: '{!! __('label.discount_type_' . $discount_type) !!}'
            });
            <?php endforeach; ?>

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });
        });
</script>
@endsection
