@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default" ng-app="MtsApp" ng-controller="MtsquoteListController">
        <div class="panel-heading">
            {{ __('transport.orders_title')}}
        </div>
        <div class="panel-body">
            <div class="row form-group">
                <div class="col-sm-12">
                    <form action="{{ route('mts.quote.index') }}" method="get" role="form" class="form-inline">
                        <div class="form-group">
                            <input ng-model="from_date" name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}">
                        </div>
                        <div class="form-group">
                            <input ng-model="to_date" name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}">
                        </div>
                        <div class="form-group">
                            <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.tranfer_code') }}">
                        </div>
                        <div class="form-group">
                            <input name="receiver_phone" value="{{ request()->query('receiver_phone')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.receiver_phone') }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-white btn-single">
                                <i class="fa fa-search"></i> {{ __('label.find') }}
                            </button>
                            <a href="{{ route('mts.quote.index') }}" class="btn btn-white btn-single">
                                {{ __('label.clear') }}
                            </a>
                            <a ng-click="Export()" class="btn btn-white btn-single">
                                <i class="fas fa-file-excel"></i> {{ __('label.export_excel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            <form id="shipment-package-form">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" style="min-width: 2500px;">
                        <thead>
                        <tr>
                            <th width="120">{{ __('label.action') }}</th>
                            <th width="120">{{ __('label.tranfer_code') }}</th>
                            <th width="120">{{ __('label.status') }}</th>
                            <th width="120">{{ __('label.customer') }}</th>
                            <th width="120">{{ __('label.receiver') }}</th>
                            <th width="120">{{ __('label.date') }}</th>
                            <th width="150">{{ __('label.amount') }}</th>
                            <th width="150">{{ __('label.currency') }}</th>
                            <th width="150">{{ __('label.transaction_type') }}</th>
                            <th width="150">{{ __('label.fee') }}</th>
                            <th width="150">{{ __('label.total') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                        </thead>
                        <tbody id="shipment-package-items">
                        @if($mts->items() > 0)
                            <?php $no = 1; ?>
                            @foreach($mts as $mt)
                                <tr>
                                    <td>
                                        <a href="{{ route('mts.quote.edit', $mt->id) }}" class="btn btn-xs btn-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td>{{ $mt->code }}</td>
                                    <td>
                                        <span class="{{ $mt->status_label }}">{{ $mt->status_name }}</span>
                                    </td>
                                    <td>{{ $mt->sender_last_name . ' '. $mt->sender_middle_name . ' ' . $mt->sender_first_name }}</td>
                                    <td>{{ $mt->receiver_last_name . ' '. $mt->receiver_middle_name . ' ' . $mt->receiver_first_name }}</td>
                                    <td>{{ $mt->created_date }}</td>
                                    <td>{{ number_format($mt->total_goods, 2) }}</td>
                                    <td>{{ isset($mt->currency->code) ? $mt->currency->code : "unknown" }}</td>
                                    <td>{{ $mt->transaction_type_name }}</td>
                                    <td>{{ number_format($mt->transfer_fee, 2)}}</td>
                                    <td>{{ number_format($mt->total_final, 2)}}</td>
                                    <td>
                                        <a href="{{ route('mts.quote.edit', $mt->id) }}" class="btn btn-xs btn-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">{{ __('label.no_records') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="paginate-single">
                    {{ $mts->appends(request()->query())->links() }}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="/js/admin/app/mts-quote.js"></script>
    <script>
       $(function () {
        if($('#from_date').val()==="")
        {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', -30);
        }
        else
        {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        }
       if($("#to_date").val()==="")
       {
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', 1);
       }
       else
       {
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
       }

    });
    </script>
@endsection
