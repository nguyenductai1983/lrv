@extends('layouts.admin.print')

@section('body')
<form name="form1" method="post" action="" id="form1">
    <div id="pnin">
        <center>
            <span id="ctl01_lbdata">
                <div class="page">
                    <div class="subpage">
                        <div style="border:1px solid black;">
                            <div style="float:left; padding-left:10px">
                                <img src="{{ asset('images/logo.jpg') }}" style="width:180px" height="30px"><br><br>
                                Chuyển tiền nhanh trong ngày
                            </div>
                            <div style="float:right;padding-right:10px">
                                <h3><b>Money Transfer Receipt</b></h3>
                                <b>Control number: {{ $mts->code }}</b>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div style="border:1px solid black;">
                            <table cellspacing="0px" width="100%" cellpadding="0px">
                                <tbody>
                                    <tr align="left">
                                        <td style="border-right:1px solid black;padding:5px;">
                                            <div><b>reason:</b> {{ $mts->reason }}</div>
                                            <div><b>Currency: </b>
                                                @foreach($currency as $cur)
                                                <input type="checkbox" @php if($cur->id == $mts->currency_id){@endphp checked="" @php } @endphp> {{ $cur->code }}
                                                @endforeach
                                            </div>
                                            <div>
                                                <b>Transaction type: </b>
                                                <input type="checkbox" @php if($mts->transaction_type == 1){ @endphp checked="" @php } @endphp> Home delivery
                                                <input type="checkbox" @php if($mts->transaction_type == 2){ @endphp checked="" @php } @endphp> Bank counter
                                                <input type="checkbox" @php if($mts->transaction_type == 3){ @endphp checked="" @php } @endphp> Account deposit
                                            </div>
                                        </td>
                                        <td style="border-right:1px solid black;" align="center">
                                            <b>Amount:</b><br>$ {{ number_format($mts->total_goods, 2) }}
                                        </td>
                                        <td style="padding:5px;">
                                            <b>In words: </b><br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="receviver">
                            <div>
                                <span style="font-size:15px;font-weight:bold">RECEIVER (DATA)</span><br><br>
                                <div>
                                    <b>First name</b>: {{ $mts->receiver_first_name }}
                                    <b>Mid name</b>: {{ $mts->receiver_middle_name }}
                                    <b>Last name</b>: {{ $mts->receiver_last_name }}
                                </div>
                                <div>
                                    <b>Address</b>: {{ $mts->receiver_address }}
                                </div>
                                <div>
                                    <b>City</b>: {{ $mts->receiver_city->name }}
                                </div>
                                <div>
                                    <b>Prov</b>: {{ $mts->receiver_province->name }}
                                </div>
                                <div>
                                    <b>Country</b>: {{ $mts->receiver_country->name }}
                                </div>
                                <div>
                                    <b>Tel</b>: {{ $mts->receiver_phone }}
                                </div>
                            </div>
                            <hr width="100%">
                            <div>
                                <div>
                                    <b>Are you sending money on behalf of third person? </b><br>
                                    <input type="checkbox" name="cad" checked="" id="cad" value="cad"> Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" name="usd" id="usd" value="usd"> No
                                </div>
                                <div>
                                    <b>Name:</b> {{ $mts->third_person_name }}
                                </div>
                                <div>
                                    <b>Address:</b> {{ $mts->third_person_address }}
                                </div>
                                <div>
                                    <b>Tel:</b> {{ $mts->third_person_tel }}
                                </div><br>
                                <div>
                                    <b>Message:</b> {{ $mts->third_person_message }}
                                </div>
                            </div>
                        </div>
                        <div class="sender">
                            <span style="font-size:15px;font-weight:bold">Sender (DATA)</span><br><br>
                            <div>
                                <b>First name</b>: {{ $mts->sender_first_name }}
                                <b>Mid name</b>: {{ $mts->sender_middle_name }}
                                <b>Last name</b>: {{ $mts->sender_last_name }}
                            </div>
                            <div>
                                <b>Address</b>: {{ $mts->sender_address }}
                            </div>
                            <div>
                                <b>City</b>: {{ $mts->sender_city->name }}
                            </div>
                            <div>
                                <b>Prov</b>: {{ $mts->sender_province->name }}
                            </div>
                            <div>
                                <b>Country</b>: {{ $mts->sender_country->name }}
                            </div>
                            <div>
                                <b>Postal code</b>: {{ $mts->sender_post_code }}
                            </div>
                            <div>
                                <b>Tel</b>: {{ $mts->sender_phone }}
                            </div>
                            <div style="font-size:12px;">
                                (Below info. requested for any amount larger than $1,000. See the Instruction for more details)
                            </div>
                            <div>
                                <b>I.D#</b>: {{ $mts->customer->id_card }}
                                <b>Expiry date</b>: {{ $mts->customer->card_expire }}
                            </div>
                            <div>
                                <b>D.O.B</b>: {{ $mts->customer->birthday }}
                                <b>Occupation</b>: {{ $mts->customer->career }}
                            </div>
                            <div>
                                <b>Employer name</b>: {{ $mts->employer_name }}
                                <b>Tel</b>: {{ $mts->employer_tel }}
                            </div>
                            <div>
                                <b>Employer ADD</b>: {{ $mts->employer_address }}
                            </div>
                            <div>
                                <b>Source of fund</b>: {{ $mts->employer_source }}
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div style="margin-top:4px;border:1px solid black;text-align:left">
                            <table celspacing="0px" celpadding="0px">
                                <tbody>
                                    <tr>
                                        <td style="border-right:1px solid black;width:60%">
                                            <div style="font-size:12px">• By applicable laws, iGreenMTS Agents may requires Sender to provide information to be used for helping fight thefunding of terrorism and money-laundering activities. The Canadian government has passed the Proceeds of Crime (Money Laundering) and Terrorist Financing Act, which requires financial institutions, including money services businesses, to obtain, verify and record information that identifies persons who engage in certain transactions with or through our company. Please refer to the iGreenMTS Terms and Conditions for the details.<br>• The rate will be set at the time the transaction is paid, in which case any indication of the payment amount appearing on this form is advisory only.<br>• All complaints must be filled within 30 days from date of transaction.
                                            </div>
                                        </td>
                                        <td style="width:39%">
                                            <span style="font-size:15px;font-weight:bold">AGENT use only</span><br><br>
                                            <div>
                                                <b>Agent name</b>: KHCH iGreenLink
                                            </div>
                                            <div>
                                                <b>ID type</b>: {{ $mts->agency->code }}
                                            </div>
                                            <div>
                                                <b>Payment by</b>:
                                                <input type="checkbox" @php if($mts->payment_by == 1){ @endphp checked="" @php } @endphp> Cash
                                                <input type="checkbox" @php if($mts->payment_by == 2){ @endphp checked="" @php } @endphp> Cheque
                                                <input type="checkbox" @php if($mts->payment_by == 3){ @endphp checked="" @php } @endphp> Transfer
                                            </div>
                                            <div>
                                                <b>Transfer fee</b>: $ {{ number_format($mts->transfer_fee, 2) }}
                                            </div>
                                            <div>
                                                <b>Total amount received</b>: $ {{ number_format($mts->total_final, 2) }}
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="padding:5px;border:1px solid black;font-size:15px;height:60px;font-weight:bold">
                            <div style="float:left;width:33%">Sender signature:</div>
                            <div style="float:left;width:33%">Date: {{ $mts->created_date }}</div>
                            <div style="float:left;width:33%">Agent signature:</div>
                            <div style="clear:both;"></div>
                        </div>
                        <div style="padding:5px;border:1px solid black;font-size:15px;font-weight:bold">
                            <div style="float:left;width:33%">iGreenLink- Công ty chuyển hàng &amp; chuyển tiền</div>
                            <div style="float:left;width:33%">
                                <a href="http://igreencorp.com/">www.iGreenCorp.com</a>
                            </div>
                            <div style="float:left;width:33%">Toll free # 1 (888) 619-6869 / Hotline 1 (647) 887-6869</div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </span>
            <span id="ctl01_lbthongbao"><script language="javascript">window.print();</script></span>
        </center>
    </div>
</form>
@endsection

