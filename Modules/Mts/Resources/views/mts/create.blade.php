@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default" ng-app="MtsApp" ng-controller="MtsCreateController" style="padding: 15px;">
        <div class="panel-heading">
            {{ __('mts.add') }}
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-create" name="createForm" ng-submit="createMts()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i
                                            class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.customer.length || errors.receiver.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.customer.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.customer">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-if="errors.container.length" >
                                        <div class="col-sm-12">
                                            <ul>
                                                <li ng-repeat="error in errors.container">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.system.length">
                                    <div class="row" >
                                        <div class="col-sm-12">
                                            <h4>{{ __('express.system_error') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.system">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading">
                        <div style="margin: -12px 0;width: 80%;float: right;">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" ng-model="customerSearch.telephone"
                                               ng-keypress="$event.keyCode == 13 && searchCustomers()"
                                               placeholder="{{ __('customer.telephone') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-model="customerSearch.code"
                                               ng-keypress="$event.keyCode == 13 && searchCustomers()"
                                               placeholder="{{ __('customer.code') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-model="customerSearch.first_name"
                                               ng-keypress="$event.keyCode == 13 && searchCustomers()"
                                               placeholder="{{ __('customer.first_name') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-model="customerSearch.middle_name"
                                               ng-keypress="$event.keyCode == 13 && searchCustomers()"
                                               placeholder="{{ __('customer.middle_name') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-model="customerSearch.last_name"
                                               ng-keypress="$event.keyCode == 13 && searchCustomers()"
                                               placeholder="{{ __('customer.last_name') }}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-white" ng-click="searchCustomers()"
                                                ng-disabled="submittedSearchCustomers">
                                            <i class="fa fa-search"></i> {{ __('label.find') }}
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-body" style="margin: -10px -20px;">
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td width="50%" style="border-right: 1px solid #ccc;">
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.sender') }} <button type="button" ng-click="EditCustomer();"> <i class="fa fa-edit text-success"></i>
                                        </button> <strong ng-bind="customer.code" class="ng-binding"></strong></h3>
                                    <div class="modal fade custom-width" id="modal-customers-search-result">
                                        <div class="modal-dialog" style="width: 60%;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">{{ __('label.search_result') }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>{{ __('customer.code') }}</th>
                                                                <th>{{ __('customer.first_name') }}</th>
                                                                <th>{{ __('customer.telephone') }}</th>
                                                                <th>{{ __('customer.address_1') }}</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr ng-repeat="customer in customersSearchResult"
                                                                style="cursor: pointer;"
                                                                ng-click="selectCustomer(customer)">
                                                                <td ng-bind="customer.code"></td>
                                                                <td ng-bind="customer.full_name"></td>
                                                                <td ng-bind="customer.telephone"></td>
                                                                <td ng-bind="customer.address_1"></td>
                                                            </tr>
                                                            <tr ng-if="submittedSearchCustomers">
                                                                <td colspan="4">
                                                                    <i class="fa fa-refresh fa-spin"></i>
                                                                </td>
                                                            </tr>
                                                            <tr ng-if="!submittedSearchCustomers && !customersSearchResult.length">
                                                                <td colspan="4">
                                                                    {{ __('label.no_records') }}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">
                                                        {{ __('label.close') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade custom-width" id="modal-address-search-result">
                                        <div class="modal-dialog" style="width: 60%;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">{{ __('label.search_result') }}</h4>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>{{ __('customer.code') }}</th>
                                                                <th>{{ __('customer.first_name') }}</th>
                                                                <th>{{ __('customer.telephone') }}</th>
                                                                <th>{{ __('customer.address_1') }}</th>
                                                                <th>{{ __('customer.address_2') }}</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr ng-repeat="address in addressSearchResult"
                                                                style="cursor: pointer;"
                                                                ng-click="selectAddress(address)">
                                                                <td ng-bind="address.postal_code"></td>
                                                                <td ng-bind="address.email"></td>
                                                                <td ng-bind="address.phone_number"></td>
                                                                <td ng-bind="address.address1"></td>
                                                                <td ng-bind="address.address2"></td>
                                                            </tr>
                                                            <tr ng-if="submittedSearchAddress">
                                                                <td colspan="4">
                                                                    <i class="fa fa-refresh fa-spin"></i>
                                                                </td>
                                                            </tr>
                                                            <tr ng-if="!submittedSearchAddress && !addressSearchResult.length">
                                                                <td colspan="4">
                                                                    {{ __('label.no_records') }}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">
                                                        {{ __('label.close') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="20%">
                                                <input type="text" class="form-control" name="customer.first_name"
                                                       ng-model="customer.first_name" ng-disabled="Updatecustomer">
                                            </td>
                                            <td width="15%" class="col-label">
                                                {{ __('customer.middle_name') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.middle_name"
                                                       ng-model="customer.middle_name" ng-disabled="Updatecustomer">
                                            </td>
                                            <td width="10%" class="col-label">
                                                {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.last_name"
                                                       ng-model="customer.last_name" ng-disabled="Updatecustomer">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="customer.address_1" ng-model="customer.address_1" ng-disabled="Updatecustomer">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.email') }}
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="customer.email" ng-model="customer.email" ng-disabled="customemailedit">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="customer.country_id" class="form-control" ng-disabled="Updatecustomer"
                                                        ng-model="customer.country_id"
                                                        ng-change="getProvincesCustomer()">
                                                    <option value="">{{ __('label.select_country') }}</option>
                                                    <option ng-repeat="country in countries"
                                                            ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="customer.province_id" class="form-control" ng-disabled="Updatecustomer"
                                                        ng-model="customer.province_id" ng-change="getCitiesCustomer()"
                                                        >
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesCustomer"
                                                            ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="customer.city_id" class="form-control"
                                                        ng-model="customer.city_id" ng-change="getPostCodeSender()" ng-disabled="Updatecustomer">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesCustomer"
                                                            ng-value="city.id">@{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label">{{ __('customer.postal_code') }}<i
                                                        class="text-danger">*</i></td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.postal_code" ng-disabled="Updatecustomer"
                                                       ng-model="customer.postal_code">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.telephone" ng-disabled="Updatecustomer"
                                                       ng-model="customer.telephone">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="col-label" style="padding-bottom: 10px;">
                                                <button type="button" class="btn btn-info btn-xs"
                                                        ng-click="showMoreInfoCustomer = !showMoreInfoCustomer">
                                                    <i class="fa fa-plus"></i> {{ __('label.more_info') }}
                                                </button>
                                                <button type="button" ng-click="EditCustomer();"> <i class="fa fa-edit text-success"></i></button>
                                            </td>
                                        </tr>
                                        {{-- đoan này 2020-03-16 --}}
                                        <tr ng-show="showMoreInfoCustomer">
                                            <td class="col-label">
                                                {{ __('customer.id_card') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.id_card"
                                                       ng-model="customer.id_card" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                                            </td>
                                            <td class="col-label">
                                                {{ __('customer.birthday') }}
                                            </td>
                                            <td>
                                                <div class="input-group date-picker" data-change-year="true"
                                                     data-change-month="true"
                                                     data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                                                    <input type="text" class="form-control" name="customer.birthday"
                                                           ng-model="customer.birthday" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">

                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-white">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr ng-show="showMoreInfoCustomer">
                                            <td class="col-label">
                                                {{ __('customer.date_issued') }}
                                            </td>
                                            <td>
                                                <div class="input-group date-picker" data-change-year="true"
                                                     data-change-month="true"
                                                     data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                    <input type="text" class="form-control" name="customer.date_issued"
                                                           ng-model="customer.date_issued"
                                                           ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">

                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-white">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="col-label">
                                                {{ __('customer.card_expire') }}
                                            </td>
                                            <td>
                                                <div class="input-group date-picker" data-change-year="true"
                                                     data-change-month="true"
                                                     data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                    <input type="text" class="form-control" name="customer.card_expire"
                                                           ng-model="customer.card_expire"
                                                           ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">

                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-white">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                            <td class="col-label">
                                                {{ __('career.career') }}
                                            </td>
                                            <td>
                                                <select id="career" ng-disabled="Updatecustomer" class="form-control" ng-model="customer.career" ng-change="select(customer.career,'career_other')">
                                                    <option value='{{__('career.accountant')}}'>{{__('career.accountant')}}</option>
                                                    <option value='{{__('career.building_sector')}}'>{{__('career.building_sector')}}</option>
                                                    <option value=''>{{__('career.businessman')}}</option>
                                                    <option value='{{__('career.carpenter')}}'>{{__('career.carpenter')}}</option>
                                                    <option value='{{__('career.cashier')}}'>{{__('career.cashier')}}</option>
                                                    <option value=''>{{__('career.construction')}}</option>
                                                    <option value=''>{{__('career.consultant')}}</option>
                                                    <option value='{{__('career.cnc_machine_operator')}}'>{{__('career.cnc_machine_operator')}}</option>
                                                    <option value='{{__('career.cnc_operator')}}'>{{__('career.cnc_operator')}}</option>
                                                    <option value='{{__('career.electrical_technician')}}'>{{__('career.electrical_technician')}}</option>
                                                    <option value='{{__('career.executive_manager')}}'>{{__('career.executive_manager')}}</option>
                                                    <option value='{{__('career.factory_labourer')}}'>{{__('career.factory_labourer')}}</option>
                                                    <option value='{{__('career.financial_consultant')}}'>{{__('career.financial_consultant')}}</option>
                                                    <option value='{{__('career.forklift_operator')}}'>{{__('career.forklift_operator')}}</option>
                                                    <option value='{{__('career.grocery_store_manager')}}'>{{__('career.grocery_store_manager')}}</option>
                                                    <option value=''>{{__('career.health_worker')}}</option>
                                                    <option value='{{__('career.house_builder')}}'>{{__('career.house_builder')}}</option>
                                                    <option value=''>{{__('career.import_export')}}</option>
                                                    <option value='{{__('career.laywer')}}'>{{__('career.laywer')}}</option>
                                                    <option value=''>{{__('career.labourer')}}</option>
                                                    <option value='{{__('career.line_operator')}}'>{{__('career.line_operator')}}</option>
                                                    <option value='{{__('career.nail_technician')}}'>{{__('career.nail_technician')}}</option>
                                                    <option value='{{__('career.nurse')}}'>{{__('career.nurse')}}</option>
                                                    <option value='{{__('career.nursing_assistant')}}'>{{__('career.nursing_assistant')}}</option>
                                                    <option value=''>{{__('career.manager')}}</option>
                                                    <option value=''>{{__('career.operator')}}</option>
                                                    <option value='{{__('career.pharmacy')}}'>{{__('career.pharmacy')}}</option>
                                                    <option value='{{__('career.plumber')}}'>{{__('career.plumber')}}</option>
                                                    <option value='{{__('career.retail_sales')}}'>{{__('career.retail_sales')}}</option>
                                                    <option value='{{__('career.secretary')}}'>{{__('career.secretary')}}</option>
                                                    <option value=''>{{__('career.self-employed')}}</option>
                                                    <option value='{{__('career.shop_labourer')}}'>{{__('career.shop_labourer')}}</option>
                                                    <option value='{{__('career.teacher')}}'>{{__('career.teacher')}}</option>
                                                    <option value=''>{{__('career.retired')}}</option>
                                                    <option value=''>{{__('career.other')}}</option>
                                                    <option value='' hidden selected>{{__('label.choose')}}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.career" id="career_other"
                                                       ng-model="customer.career" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                                            </td>
                                        </tr>
                                        {{-- đoan này 2020-03-16 --}}
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}
                                        <strong ng-bind="address.id" class="ng-binding"></strong>
                                    <button type="button" ng-click="Editreceiver()">
                                    <i class="fa fa-edit text-success"></i>
                                    </button>
                                    </h3>
                                    <div style="float: right;position: absolute;right: 17px;top: 63px;"
                                         ng-if="addressSearchResult">
                                        <select class="form-control" ng-model="selectedId"
                                                ng-change="pickAddress(selectedId)">
                                            <option value="">{{ __('label.select_address') }}</option>
                                            <option ng-repeat="item in addressSearchResult"
                                                    ng-value="item.id">@{{item.last_name}} @{{item.middle_name}} @{{item.first_name}}</option>
                                        </select>
                                    </div>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="20%">
                                                <input type="text" class="form-control" name="receiver.first_name"
                                                       ng-model="address.first_name" ng-disabled="Updatereciver">
                                            </td>
                                            <td width="15%" class="col-label">
                                                {{ __('receiver.middle_name') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.middle_name"
                                                       ng-model="address.middle_name" ng-disabled="Updatereciver">
                                            </td>
                                            <td width="10%" class="col-label">
                                                {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.last_name"
                                                       ng-model="address.last_name" ng-disabled="Updatereciver">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.address') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="receiver.address"
                                                       ng-model="address.address_1" ng-disabled="Updatereciver">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="receiver.country_id" class="form-control" ng-disabled="Updatereciver"
                                                        ng-model="address.country_id" ng-change="getProvincesReceiver()">
                                                    <option value="">{{ __('label.select_address') }}</option>
                                                    <option ng-repeat="country in countries"
                                                            ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.province_id" class="form-control" ng-disabled="Updatereciver"
                                                        ng-model="address.province_id"
                                                        ng-change="getCitiesReceiver()">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesReceiver"
                                                            ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.city_id" class="form-control"
                                                        ng-model="address.city_id" ng-change="getWardsReceiver()" ng-disabled="Updatereciver">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesReceiver"
                                                            ng-value="city.id">@{{ city.name }}</option>
                                                </select>
                                            </td>
                                            {{-- ngày 22-04-2020 --}}
                                            <td class="col-label" ng-hide="ward">
                                                {{ __('receiver.ward_id') }}
                                            </td>
                                            <td ng-hide="ward">
                                                <select name="receiver.ward_id" class="form-control" ng-model="address.ward_id"
                                                ng-disabled="Updatereciver && address.ward_id">
                                                    <option value="">{{ __('label.select_ward') }}</option>
                                                    <option ng-repeat="ward in wardsReceiver" ng-value="ward.id">@{{ ward.name }}</option>
                                                </select>
                                            </td>
                                            {{-- het doan them --}}

                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receivertelephone" ng-disabled="Updatereciver"
                                                       ng-model="address.telephone" required ng-maxlength="10" ng-minlength="10" ng-blur="checkreceivercellphone()">
                                            </td>

                                            <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.cellphone" ng-disabled="Updatereciver"
                                                       ng-model="address.cellphone">
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                {{-- doan code kiem tra so dien thoai --}}
                                            <div ng-show="!ward" style="color: red">
                                                <span ng-show="createForm.receivertelephone.$error.maxlength">{{ __('receiver.cellphone_max') }}
                                                    <a target="_blank" style="color: red" href="/helps/change-11-digit-mobile-number-to-10-digit-number-76">{{ __('label.more_info') }}</a></span>
                                                <span ng-show="createForm.receivertelephone.$error.minlength">{{ __('receiver.cellphone_min') }}</span>
                                            </div>
                                            {{-- het doan kiem tra so dt  --}}
                                            </td>
                                        </tr>
                                        {{-- thêm đoạn này 2020-03-16 --}}
                                        <tr>
                                            <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.post_code" ng-disabled="Updatereciver"
                                                       ng-model="address.postal_code">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">{{__('receiver.relationship')}}</td>
                                            <td>
                                            <select id="relationship" class="form-control" ng-model="address.relationship" ng-change="select(address.relationship,'relationship_other')" ng-disabled="Updatereciver">
                                            <option value='{{__('receiver.aunt')}}'>{{__('receiver.aunt')}}</option>
                                            <option value='{{__('receiver.brother')}}'>{{__('receiver.brother')}}</option>
                                            <option value='{{__('receiver.brother_in_law')}}'>{{__('receiver.brother_in_law')}}</option>
                                            <option value='{{__('receiver.cousin')}}'>{{__('receiver.cousin')}}</option>
                                            <option value='{{__('receiver.daughter')}}'>{{__('receiver.daughter')}}</option>
                                            <option value='{{__('receiver.emplyee')}}'>{{__('receiver.emplyee')}}</option>
                                            <option value='{{__('receiver.emplyer')}}'>{{__('receiver.emplyer')}}</option>
                                            <option value='{{__('receiver.father')}}'>{{__('receiver.father')}}</option>
                                            <option value='{{__('receiver.father_in_law')}}'>{{__('receiver.father_in_law')}}</option>
                                            <option value='{{__('receiver.friend')}}'>{{__('receiver.friend')}}</option>
                                            <option value='{{__('receiver.grandfather')}}'>{{__('receiver.grandfather')}}</option>
                                            <option value='{{__('receiver.granmother')}}'>{{__('receiver.granmother')}}</option>
                                            <option value='{{__('receiver.husband')}}'>{{__('receiver.husband')}}</option>
                                            <option value='{{__('receiver.mother')}}'>{{__('receiver.mother')}}</option>
                                            <option value='{{__('receiver.mother_in_law')}}'>{{__('receiver.mother_in_law')}}</option>
                                            <option value='{{__('receiver.nephew')}}'>{{__('receiver.nephew')}}</option>
                                            <option value='{{__('receiver.niece')}}'>{{__('receiver.niece')}}</option>
                                            <option value='{{__('receiver.self')}}'>{{__('receiver.self')}}</option>
                                            <option value='{{__('receiver.sister')}}'>{{__('receiver.sister')}}</option>
                                            <option value='{{__('receiver.sister_in_law')}}'>{{__('receiver.sister_in_law')}}</option>
                                            <option value='{{__('receiver.son')}}'>{{__('receiver.son')}}</option>
                                            <option value='{{__('receiver.uncle')}}'>{{__('receiver.uncle')}}</option>
                                            <option value='{{__('receiver.wife')}}'>{{__('receiver.wife')}}</option>
                                            <option value="">{{__('receiver.other')}}</option>
                                            <option value="" hidden>{{ __('label.choose') }}</option>
                                            </select>
                                        </td>
                                        <td colspan="2">
                                            <input ng-model="address.relationship"  class="form-control" type="text" id="relationship_other" ng-disabled="Updatereciver" />
                                        </td>
                                        </tr>
                                           {{-- thêm đoạn này --}}
                                        <tr>
                            <td colspan="2">
                             <button type="button" class="btn-sm btn-success" ng-click="createContact()" ng-if="SaveContact">{{ __('label.save_contact') }} <i class="fa fa-save"></i></button>
                              <button type="button" class="btn btn-primary btn-sm" ng-if="customer.id" ng-click="removeCustomer()">
                            <i class="fas fa-trash"></i> {{ __('label.clear') }}
                                </button>
                             <div ng-if="contact_save" class='alert alert-success' role="alert"> {{ __('label.save_contact_success')}}
                             </div>
                            </td>
                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
<div class="container-fluid">
<div ng-show="showMoreInfoCustomer" class="row" >
   <div class="col-sm-3 col-xs-4">
    <div class="col-label">
    {{ __('customer.image_1_file_id') }}
    </div>
    @include('partials.form-controls.image', ['field' => 'customer.image_1_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
</div>
    <div class="col-sm-3 col-xs-4">
<div class="col-label">
{{ __('customer.image_2_file_id') }}
</div>
@include('partials.form-controls.image', ['field' => 'customer.image_2_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
</div>
  <div class="col-sm-3 col-xs-4">
    <div class="col-label">
    {{ __('customer.image_3_file_id') }}
    </div>
    @include('partials.form-controls.image', ['field' => 'customer.image_3_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
        </div>
</div>
</div>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading">
                        {{ __('mts.information_advance') }}
                    </div>
                    <div class="panel-body" style="margin: -10px -20px;">
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td width="50%" style="border-right: 1px solid #ccc;">
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('mts.third_person') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('label.name') }}
                                            </td>
                                            <td width="30%">
                                                <input type="text" class="form-control" ng-model="container.thirdperson_name">
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('label.telephone') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" ng-model="container.thirdperson_tel">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('label.address') }}
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" ng-model="container.thirdperson_address">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('label.message') }}
                                            </td>
                                            <td colspan="5">
                                                <textarea type="text" class="form-control" ng-model="container.thirdperson_message"></textarea>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('mts.employer') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('label.name') }}
                                            </td>
                                            <td width="30%">
                                                <input type="text" class="form-control" ng-model="container.employer_name">
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('label.telephone') }}
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" ng-model="container.employer_tel">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('label.address') }}
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" ng-model="container.employer_address">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('mts.source_of_fund') }}
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" ng-model="container.employer_source_of_fund">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading">
                        {{ __('mts.details') }}
                    </div>
                    <div class="panel-body" style="margin: -10px -20px;">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td class="col-middle text-right">
                                        {{ __('mts.reason') }}:
                                    </td>
                                    <td colspan="2">
                                        {{-- thêm đoạn này 2020-03-16--}}
                                        <select id="reason" class="form-control" ng-model="container.reason" ng-change="select(container.reason,'reason_other')">
                                        <option value='{{__('mts.buy_real_estate')}}'>{{__('mts.buy_real_estate')}}</option>
                                        <option value='{{__('mts.debt_loan_mortgage_payments')}}'>{{__('mts.debt_loan_mortgage_payments')}}</option>
                                        <option value='{{__('mts.donation')}}'>{{__('mts.donation')}}</option>
                                        <option value='{{__('mts.education')}}'>{{__('mts.education')}}</option>
                                        <option value='{{__('mts.family_assitance')}}'>{{__('mts.family_assitance')}}</option>
                                        <option value='{{__('mts.house_construction')}}'>{{__('mts.house_construction')}}</option>
                                        <option value='{{__('mts.investment')}}'>{{__('mts.investment')}}</option>
                                        <option value='{{__('mts.medical_expenses')}}'>{{__('mts.medical_expenses')}}</option>
                                        <option value='{{__('mts.saving')}}'>{{__('mts.saving')}}</option>
                                        <option value='{{__('mts.travel')}}'>{{__('mts.travel')}}</option>
                                        <option value=''>{{__('mts.other')}}</option>
                                        <option value='' hidden>{{ __('label.choose') }}</option>
                                        </select>
                                    </td>
                                        <td colspan="3">
                                        <input type="text" class="form-control" ng-model="container.reason" id="reason_other" value="">
                                    </td>
                                    {{--  thêm đoạn này 2020-03-16 --}}
                                </tr>
                                <tr>
                                    <td class="col-middle text-right">{{ __('mts.transaction_type') }}:</td>
                                    <td class="col-middle text-left">
                                        <select class="form-control" ng-model="container.transaction_type"
                                        ng-change="update_transaction_type()">
                                            <option ng-repeat="transaction_type in transaction_types" ng-value="transaction_type.key">@{{ transaction_type.name }}</option>
                                        </select>
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.currency_pay') }}:</td>
                                    <td class="col-middle text-left">
                                        <select class="form-control" ng-model="container.currency">
                                            <option ng-repeat="currency in currencies" ng-value="currency.id">@{{ currency.name }}</option>
                                        </select>
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                                    <td class="col-middle text-left">
                                        <select class="form-control" ng-model="container.payment_by">
                                            <option ng-repeat="payment_by in payment_bys" ng-value="payment_by.key">@{{ payment_by.name }}</option>
                                        </select>
                                    </td>
                                </tr>
                                {{-- them ngay 21-07-2021 --}}
                                <tr ng-if="container.transaction_type===3">
                                    <td colspan="2"></td>
                                    <td class="col-middle text-right">{{ __('mts.bank_name') }}<i class="text-danger">*</i></td>
                                    <td class="col-middle text-left">
                                        <input type="text" class="form-control" ng-model="container.bank_name" id="bank_name" value="">
                                    </td>
                                    <td class="col-middle text-right">{{ __('mts.bank_account') }}<i class="text-danger">*</i></td>
                                    <td class="col-middle text-left">
                                        <input type="text" class="form-control" ng-model="container.bank_account" id="bank_account" value="">
                                    </td>
                                </tr>
                                {{-- them ngay 21-07-2021 --}}
                                <tr>
                                    <td class="col-middle text-right">{{ __('label.total_amount_mts') }}</td>
                                    <td class="col-middle text-left" id ="container.amount">
                                        <input type="text" class="form-control" ng-model="container.amount" ng-change="updateFee(container)">
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.total_discount') }}:</td>
                                    <td class="col-middle text-left inline-block">
                                        <select class="form-control" ng-model="container.discount_type" ng-change="updateFee(container)" style="display: block;width:70px;float: left;">
                                            <option ng-repeat="discount_type in discount_types" ng-value="discount_type.key">@{{ discount_type.name }}</option>
                                        </select>
                                        <input type="text" class="form-control text-right" disabled ng-model="container.discount_number" style="width:50%;text-align: right;" ng-change="updateFee(container)">
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.status') }}</td>
                                    <td class="col-middle text-left">
                                        <select class="form-control" ng-model="container.status">
                                            <option ng-repeat="status in statuses" ng-value="status.key">@{{ status.name }}</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-middle text-right">{{ __('mts.transfer_fee') }}:</td>
                                    <td class="col-middle text-left">
                                        <input type="text" class="form-control" ng-model="container.transfer_fee" disabled="disabled">
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.commission') }} :</td>
                                    <td class="col-middle text-left">
                                        <input type="text" class="form-control" ng-model="container.commission" disabled="disabled">
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.total_pay') }}:</td>
                                    <td class="col-middle text-left">
                                        <input type="text" class="form-control" ng-model="container.total" disabled="disabled">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                 <div ng-if="complete" class='alert alert-success' role="alert">
                    <h3>{{ __('label.complete')}} </h3>
                 </div>
                 <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="inline-block">
                            {{ __('label.date_pay') }} :
                            <input type="text" ng-model="container.pay_date" disabled="disabled" value="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                    <a href="{{ route('mts.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                     <a href="@{{ new_order }}/print_bills" target="_blank" class="btn btn-info" ng-if="complete">
                                   {{ __('label.print_bill')}}  <i class="fa fa-print "></i>
                                </a>
                    <button type="submit" class="btn btn-info" ng-disabled="submitted" ng-if="!complete">
                        <i class="fa fa-check"></i> {{ __('label.add_order') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/admin/app/mts-create.js?t=' . File::lastModified(public_path('js/admin/app/mts-create.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-create').scope();
            scope.agency = {!! auth()->user()->agency_id !!};
            scope.date_now = '{!! date(config('app.date_format')) !!}';
            scope.vnPriority = {!! json_encode(config('app.vn_priority')) !!};
            // scope.mts_per_fee = {!! config('mts.per_fee') !!};
            // scope.mts_max_per_fee = {!! config('mts.max_per_fee') !!};
            // scope.fix_fee = {!! config('mts.fix_fee') !!};
            // scope.fix_fee1500 = {!! config('mts.fix_fee1500') !!};
            // scope.fix_fee3000 = {!! config('mts.fix_fee3000') !!};
            scope.commission = {!! config('mts.per_agency_fee') !!};
            scope.MTSSurcharge = {!! $MTSSurcharge !!};
            scope.init();

            scope.transaction_types = [];
            scope.statuses = [];
            scope.payment_bys = [];
            scope.discount_types = [];

            <?php foreach(config('mts.status') as $status): ?>
            scope.statuses.push({
                key : parseInt({!! $status !!}),
                name: '{!! __('label.mts_status_' . $status) !!}'
            });
            <?php endforeach; ?>
            <?php foreach(config('mts.transaction_type') as $transaction_type): ?>
            scope.transaction_types.push({
                key : parseInt({!! $transaction_type !!}),
                name: '{!! __('label.transaction_type_' . $transaction_type) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.payment_by') as $payment_by): ?>
            scope.payment_bys.push({
                key : parseInt({!! $payment_by !!}),
                name: '{!! __('label.payment_by' . $payment_by) !!}'
            });
            <?php endforeach; ?>

            <?php foreach(config('mts.discount_type') as $discount_type): ?>
            scope.discount_types.push({
                key : parseInt({!! $discount_type !!}),
                name: '{!! __('label.discount_type_' . $discount_type) !!}'
            });
            <?php endforeach; ?>

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });
        });
    </script>
@endsection
