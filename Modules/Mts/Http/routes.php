<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-mts-quote'], 'prefix' => 'admin/mts/quote', 'namespace' => 'Modules\Mts\Http\Controllers'], function() {
    Route::get('/', 'MtsCustomerController@index')->name('mts.quote.index');
    Route::get('/export', 'MtsCustomerController@exportExcel')->name('mts.quote.export');
    Route::get('/{id}', 'MtsCustomerController@show')->name('mts.quote.show');
    Route::get('/{id}/edit', 'MtsCustomerController@edit')->name('mts.quote.edit');
    Route::post('/{id}/approve', 'MtsCustomerController@approve');
    Route::post('/{id}/payment', 'MtsCustomerController@payment');
    Route::post('/{id}/cancel', 'MtsCustomerController@cancel');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-mts'], 'prefix' => 'admin/mts', 'namespace' => 'Modules\Mts\Http\Controllers'], function() {
    Route::get('/', 'MtsController@index')->name('mts.index');
    Route::get('/export', 'MtsController@exportExcel')->name('mts.export');
    Route::get('/create', 'MtsController@create')->name('mts.create');
    Route::post('/store', 'MtsController@store')->name('mts.store');
    Route::put('/{id}/update', 'MtsController@update')->name('mts.update');
    Route::get('/{id}/edit', 'MtsController@edit')->name('mts.edit');
    Route::get('/{id}', 'MtsController@show')->name('mts.show');
    Route::get('/{id}/print_bill', 'MtsController@printBill')->name('mts.print');
    Route::post('/cancel-order', 'MtsController@cancelOrder');
});


