<?php

namespace Modules\Mts\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use App\Mts;
use App\Transaction;
use App\Notification;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
use App\Services\PointService;
use App\Enum\MtsStatusEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\PointEnum;
use App\Exports\ExportQuery;
use App\Services\OrderService;
use App\MTSSurcharge;
use Exception;
class MtsCustomerController extends Controller
{
    private $data;
    private function _validate(Request $request)
    {
        $this->validate($request, [
            'customer' => 'required|array',
            'customer.id' => 'nullable|exists:customers,id',
            'customer.image_1_file_id' => 'nullable|exists:files,id',
            'customer.image_2_file_id' => 'nullable|exists:files,id',
            'customer.image_3_file_id' => 'nullable|exists:files,id',
            'customer.first_name' => 'required|required_without:customer.id|string|max:50',
            'customer.middle_name' => 'nullable|string|max:50',
            'customer.last_name' => 'required|required_without:customer.id|string|max:50',
            'customer.address_1' => 'required|required_without:customer.id|string|max:255',
            'customer.email' => 'nullable|email',
            'customer.telephone' => 'required|required_without:customer.id|string|max:20',
            'customer.cellphone' => 'nullable|string|max:20',
            'customer.postal_code' => 'nullable|required_without:customer.id|string|max:20',
            'customer.id_card' => 'nullable|string|max:20',
            'customer.card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'customer.date_issued' => 'nullable|date_format:' . config('app.date_format'),
            'customer.birthday' => 'nullable|date_format:' . config('app.date_format'),
            'customer.career' => 'nullable|string|max:100',
            'customer.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'customer.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'customer.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:255',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.cellphone' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',
            'receiver.relationship' => 'nullable|string|max:50',

            'container' => 'required|array',
            'container.thirdperson_name' => 'nullable|string',
            'container.thirdperson_tel' => 'nullable|string',
            'container.thirdperson_address' => 'nullable|string',
            'container.thirdperson_message' => 'nullable|string',
            'container.employer_name' => 'nullable|string',
            'container.employer_tel' => 'nullable|string',
            'container.employer_address' => 'nullable|string',
            'container.employer_source_of_fund' => 'nullable|string',
            'container.reason'  => 'required|string',
            'container.pay_date' => 'nullable|date_format:' . config('app.date_format'),
            'container.transaction_type' => 'required|in:' . implode(',', config('mts.transaction_type')),
            'container.status' => 'required|in:' . implode(',', config('mts.status')),
            'container.payment_by' => 'required|in:' . implode(',', config('mts.payment_by')),
            'container.amount' => 'required|numeric|min:0',
            'container.discount_type' => 'required|in:' . implode(',', config('mts.discount_type')),
            'container.discount_number' => 'required|numeric|min:0',
            'container.transfer_fee' => 'required|numeric|min:0',
            'container.total' => 'required|numeric|min:0',
        ], [], [
            'customer.id' => __('label.sender'),
            'customer.image_1_file_id' => __('customer.image_1_file_id'),
            'customer.image_2_file_id' => __('customer.image_2_file_id'),
            'customer.image_3_file_id' => __('customer.image_3_file_id'),
            'customer.first_name' => __('customer.first_name'),
            'customer.middle_name' => __('customer.middle_name'),
            'customer.last_name' => __('customer.last_name'),
            'customer.address_1' => __('customer.address_1'),
            'customer.email' => __('customer.email'),
            'customer.telephone' => __('customer.telephone'),
            'customer.cellphone' => __('customer.cellphone'),
            'customer.postal_code' => __('customer.postal_code'),
            'customer.id_card' => __('customer.id_card'),
            'customer.card_expire' => __('customer.card_expire'),
            'customer.birthday' => __('customer.birthday'),
            'customer.career' => __('customer.career'),
            'customer.country_id' => __('customer.country_id'),
            'customer.province_id' => __('customer.province_id'),
            'customer.city_id' => __('customer.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),
            'receiver.relationship' => __('receiver.relationship'),

            'container.thirdperson_name' => __('mts.thirdperson_name'),
            'container.thirdperson_tel' => __('mts.thirdperson_tel'),
            'container.thirdperson_address' => __('mts.thirdperson_address'),
            'container.thirdperson_message' => __('mts.thirdperson_message'),
            'container.employer_name' => __('mts.employer_name'),
            'container.employer_tel' => __('mts.employer_tel'),
            'container.employer_address' => __('mts.employer_address'),
            'container.employer_source_of_fund' => __('mts.employer_source_of_fund'),
            'container.reason' => __('mts.reason'),
            'container.pay_date' => __('mts.pay_date'),
            'container.status' => __('mts.status'),
            'container.transaction_type' => __('mts.transaction_type'),
            'container.payment_by' => __('mts.payment_by'),
            'container.amount' => __('mts.amount'),
            'container.discount_type' => __('mts.discount_type'),
            'container.discount_number' => __('mts.discount_number'),
            'container.transfer_fee' => __('mts.transfer_fee'),
            'container.total' => __('mts.total'),
        ]);
    }
    public function __construct()
    {
        $this->data = [
            'menu' => '7.2'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Mts::where([]);
        $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['mts'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('mts::quote.index', $this->data);
    }


    public function exportExcel(Request $request)
    {
        $query = Mts::where([]);
        $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('sender_phone', '=', $request->query('sender_phone'));
        }
      $query->orderBy('id', 'desc');
      return (new ExportQuery($query))->download('mts_customer.xlsx');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $mts = Mts::where('id', '=', $id)
            ->with([
                'customer' => function ($query){
                    $query->with('image1');
                    $query->with('image2');
                    $query->with('image3');
                },
                'receiver',
                'currency'
            ])
            ->first();

        if (!$mts) {
            return response('Not Found', 404);
        }
        $mts->receiver->first_name = $mts->receiver_first_name;
        $mts->receiver->middle_name = $mts->receiver_middle_name;
        $mts->receiver->last_name = $mts->receiver_last_name;
        $mts->receiver->address_1 = $mts->receiver_address;
        $mts->receiver->telephone = $mts->receiver_phone;
        $mts->receiver->postal_code = $mts->receiver_post_code;
        $mts->receiver->country_id = $mts->receiver_country_id;
        $mts->receiver->province_id= $mts->receiver_province_id;
        $mts->receiver->city_id = $mts->receiver_city_id;
        if($mts->pay_date){
            $mts->pay_date = ConvertsUtil::strToDate($mts->pay_date);
        }
        if($mts->pay_date){
            $mts->pay_date = ConvertsUtil::strToDate($mts->pay_date);
        }else{
            $mts->pay_date = __('order.payment_status_not_paid');
        }

        return response([
            'mts' => $mts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        $notification_id = $request->get('notification_id', 0);
        if($notification_id > 0){
            Notification::where('id', $notification_id)->update(['is_read' => 1]);
        }
        $MTSSurcharge=MTSSurcharge::get();
        $this->data['MTSSurcharge']=$MTSSurcharge;
        return view('mts::quote.edit', $this->data);
    }

    public function cancel(Request $request) {
        try {
            $mts = Mts::where('id', '=', $request->route('id'))->first();
            if (!$mts) {
                return response('Not Found', 404);
            }

            DB::beginTransaction();
            $data = $request->all();
            if (isset($data) && !empty($data)) {
                $mts->mts_status = MtsStatusEnum::CANCEL;
                $mts->update();

                // Mail::send('email.approved-mts-quote', ['mts' => $mts], function ($message) use ($mts) {
                //     $message->to($mts->sender_email, $mts->sender_email)->subject(__('email.approved-mts-quote'));
                // });
            }else{
                DB::rollBack();
                return response('Không tìm thấy yêu cầu chuyển tiền', 500);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }
    public function approve(Request $request) {
         $mts_fee= 0.025;
        try {
            $mts = Mts::where('id', '=', $request->route('id'))->first();
            if (!$mts) {
                return response('Not Found', 404);
            }

            DB::beginTransaction();
            $data = $request->all();
            if (isset($data) && !empty($data)) {
                $mts->reason = $data['reason'];
                $mts->transaction_type = $data['transaction_type'];
                $mts->payment_by = $data['payment_by'];
                $mts->total_goods = $data['amount'];
                $mts->discount_type = $data['discount_type'];
                $mts->discount_number = $data['discount_number'];
                $mts->mts_status = MtsStatusEnum::PROCESSING;
                $mts->total_discount = 0;
                $surcharge= OrderService::Surcharge($mts->total_goods,$mts->discount_number,$mts->discount_type);
                $mts->discount_agency_amount=$surcharge['commission'];
                $mts->transfer_fee=$surcharge['total_fee'];
                $mts->total_final = $surcharge['total_final'];
                $mts->total_discount = $surcharge['discount'];
                $mts->update();

                Mail::send('email.mts-approved-quote', ['mts' => $mts], function ($message) use ($mts) {
                    $message->to($mts->sender_email, $mts->sender_email)->subject(__('email.mts-approved-quote'));
                });
            }else{
                DB::rollBack();
                return response('Không tìm thấy config giá', 500);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function payment(Request $request)
    {
         $mts_fee= 0.025;
        try {
            $mts = Mts::where('id', '=', $request->route('id'))->first();
            if (!$mts) {
                return response('Not Found', 404);
            }

            DB::beginTransaction();
            $data = $request->all();
            if (isset($data) && !empty($data)) {
                $mts->reason = $data['reason'];
                $mts->transaction_type = $data['transaction_type'];
                $mts->payment_by = $data['payment_by'];
                $mts->total_goods = $data['amount'];
                $mts->discount_type = $data['discount_type'];
                $mts->discount_number = $data['discount_number'];
                $mts->total_paid_amount = $data['total'];
                $mts->total_discount = 0;
                // if($mts->discount_number > 0 && $mts->discount_type == 1){
                //     $mts->total_discount = ($mts->total_goods * $mts->discount_number) / 100;
                // }else if($mts->discount_number > 0 && $mts->discount_type == 2){
                //     $mts->total_discount = $mts->discount_number;
                // }
                // if ($mts->receiver_province_id==config('mts.province_min_fee'))
                // {
                //     $mts_fee=config('mts.per_fee');
                // }
                // else
                // {
                //     $mts_fee=config('mts.max_per_fee');
                // }

                // $mts->transfer_fee = (($mts_fee * $mts->total_goods) + config('mts.fix_fee')) - $mts->total_discount;
                // // số tiền gửi lớn
                //  if($mts->total_goods > config('mts.mts3000'))
                // {
                //     $mts->transfer_fee += config('mts.fix_fee3000');
                // }
                //     else if($mts->total_goods > config('mts.mts1500'))
                //     {
                //        $mts->transfer_fee += config('mts.fix_fee1500');
                //     }
                // $mts->total_final = $mts->total_goods + $mts->transfer_fee;
                $surcharge= OrderService::Surcharge($mts->total_goods,$mts->discount_number,$mts->discount_type);
                $mts->transfer_fee=$surcharge['total_fee'];
                $mts->total_final = $surcharge['total_final'];
                $mts->total_discount = $surcharge['discount'];
                $mts->update();
                $mts->mts_status = MtsStatusEnum::COMPLETE;
                $mts->pay_date = date("Y-m-d H:i:s");
                $mts->update();

                $transactionExist = Transaction::where('mts_id', $mts->id)->first();
                if(empty($transactionExist) && $mts->mts_status == 4){
                    $transaction = new Transaction();
					$transaction->user_id = $mts->user_id;
                    $transaction->customer_id = $mts->customer_id;
                    $transaction->currency_id = $mts->currency_id;
                    $transaction->mts_id = $mts->id;
                    $transaction->code = "PT_" . $mts->code;
                    $transaction->type = TransactionTypeEnum::RECEIPT;
                    $transaction->credit_amount = $mts->total_final;
					$transaction->total_amount = $mts->total_final;
					$transaction->goods_code = $mts->code;
                    $transaction->save();

                    PointService::addPointToCustomer($mts->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);

                    Mail::send('email.mts-payment-quote', ['mts' => $mts], function ($message) use ($mts) {
                        $message->to($mts->sender_email, $mts->sender_email)->subject(__('email.mts-payment-quote'));
                    });
                }
            }else{
                DB::rollBack();
                return response('Không tìm thấy config giá', 500);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }
}
