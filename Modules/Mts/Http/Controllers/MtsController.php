<?php

namespace Modules\Mts\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Agency;
use App\Customer;
use App\Address;
use App\Mts;
use App\Currency;
use App\Transaction;
use App\MTSSurcharge;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
// use App\Services\PointService;
use App\Services\OrderService;
use App\Enum\MtsStatusEnum;
use App\Enum\CustomerGroupEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\PointEnum;
use App\Country;
use App\Exports\ExportQuery;

class MtsController extends Controller
{
    private $data;

    private function _validate(Request $request)
    {
        $this->validate($request, [
            'customer' => 'required|array',
            'customer.id' => 'nullable|exists:customers,id',
            'customer.image_1_file_id' => 'nullable|exists:files,id',
            'customer.image_2_file_id' => 'nullable|exists:files,id',
            'customer.image_3_file_id' => 'nullable|exists:files,id',
            'customer.first_name' => 'required|required_without:customer.id|string|max:50',
            'customer.middle_name' => 'nullable|string|max:50',
            'customer.last_name' => 'required|required_without:customer.id|string|max:50',
            'customer.address_1' => 'required|required_without:customer.id|string|max:255',
            'customer.email' => 'nullable|email',
            'customer.telephone' => 'required|required_without:customer.id|string|max:20',
            'customer.cellphone' => 'nullable|string|max:20',
            'customer.postal_code' => 'nullable|required_without:customer.id|string|max:20',
            'customer.id_card' => 'nullable|string|max:20',
            'customer.card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'customer.date_issued' => 'nullable|date_format:' . config('app.date_format'),
            'customer.birthday' => 'nullable|date_format:' . config('app.date_format'),
            'customer.career' => 'nullable|string|max:100',
            'customer.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'customer.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'customer.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:255',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.cellphone' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',
            'receiver.relationship' => 'nullable|string|max:50',

            'container' => 'required|array',
            'container.thirdperson_name' => 'nullable|string',
            'container.thirdperson_tel' => 'nullable|string',
            'container.thirdperson_address' => 'nullable|string',
            'container.thirdperson_message' => 'nullable|string',
            'container.employer_name' => 'nullable|string',
            'container.employer_tel' => 'nullable|string',
            'container.employer_address' => 'nullable|string',
            'container.employer_source_of_fund' => 'nullable|string',
            'container.reason'  => 'required|string',
            'container.pay_date' => 'nullable|date_format:' . config('app.date_format'),
            'container.transaction_type' => 'required|in:' . implode(',', config('mts.transaction_type')),
            'container.status' => 'required|in:' . implode(',', config('mts.status')),
            'container.payment_by' => 'required|in:' . implode(',', config('mts.payment_by')),
            'container.amount' => 'required|numeric|min:0',
            'container.discount_type' => 'required|in:' . implode(',', config('mts.discount_type')),
            'container.discount_number' => 'required|numeric|min:0',
            'container.transfer_fee' => 'required|numeric|min:0',
            'container.total' => 'required|numeric|min:0',
            'container.bank_name' => 'required_if:container.transaction_type,3',
            'container.bank_account' => 'required_if:container.transaction_type,3',
        ], [], [
            'customer.id' => __('label.sender'),
            'customer.image_1_file_id' => __('customer.image_1_file_id'),
            'customer.image_2_file_id' => __('customer.image_2_file_id'),
            'customer.image_3_file_id' => __('customer.image_3_file_id'),
            'customer.first_name' => __('customer.first_name'),
            'customer.middle_name' => __('customer.middle_name'),
            'customer.last_name' => __('customer.last_name'),
            'customer.address_1' => __('customer.address_1'),
            'customer.email' => __('customer.email'),
            'customer.telephone' => __('customer.telephone'),
            'customer.cellphone' => __('customer.cellphone'),
            'customer.postal_code' => __('customer.postal_code'),
            'customer.id_card' => __('customer.id_card'),
            'customer.card_expire' => __('customer.card_expire'),
            'customer.birthday' => __('customer.birthday'),
            'customer.career' => __('customer.career'),
            'customer.country_id' => __('customer.country_id'),
            'customer.province_id' => __('customer.province_id'),
            'customer.city_id' => __('customer.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),
            'receiver.relationship' => __('receiver.relationship'),

            'container.thirdperson_name' => __('mts.thirdperson_name'),
            'container.thirdperson_tel' => __('mts.thirdperson_tel'),
            'container.thirdperson_address' => __('mts.thirdperson_address'),
            'container.thirdperson_message' => __('mts.thirdperson_message'),
            'container.employer_name' => __('mts.employer_name'),
            'container.employer_tel' => __('mts.employer_tel'),
            'container.employer_address' => __('mts.employer_address'),
            'container.employer_source_of_fund' => __('mts.employer_source_of_fund'),
            'container.reason' => __('mts.reason'),
            'container.pay_date' => __('mts.pay_date'),
            'container.status' => __('mts.status'),
            'container.transaction_type' => __('mts.transaction_type'),
            'container.payment_by' => __('mts.payment_by'),
            'container.amount' => __('mts.amount'),
            'container.discount_type' => __('mts.discount_type'),
            'container.discount_number' => __('mts.discount_number'),
            'container.transfer_fee' => __('mts.transfer_fee'),
            'container.total' => __('mts.total'),
            'container.bank_name' => __('mts.bank_name'),
            'container.bank_account' => __('mts.bank_account'),
        ]);
    }


    public function __construct()
    {
        $this->data = [
            'menu' => '7.1'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Mts::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
             $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['mts'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('mts::mts.index', $this->data);
    }


    public function exportExcel(Request $request)
    {
       $query = Mts::where([]);
       $user = Auth::user();
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
        $query->where('code', 'like', '%'.$request->query('code').'%');
    }
        if (!empty($request->query('sender_phone'))) {
            $query->where('sender_phone', '=', $request->query('sender_phone'));
        }
         $query->orderBy('id', 'desc');
        return (new ExportQuery($query))->download('mts_order.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $MTSSurcharge=MTSSurcharge::get();
        $this->data['MTSSurcharge']=$MTSSurcharge;
        $this->data['language'] = LanguageUtil::getMTSKeyJavascript();
        return view('mts::mts.create', $this->data);
    }

    public function printBill($id){
        $mts = Mts::find($id);
        if (empty($mts)) {
            return redirect()->route('mts.index');
        }
        $this->data['mts'] = $mts;
        $this->data['currency'] = Currency::where('is_active', 1)->get();
        return view('mts::mts.print', $this->data);
    }
    private function Check_info_Invalid($customer,$amount,$relationship)
    {
        $list_errors=[];
        $status=0;
        $arr=['id_card','card_expire','birthday','career'];
            $check_card_expire =false;
            if($customer['card_expire']){
                $today = date('Y-m-d H:i:s');
                $ngay=explode('/',$customer['card_expire']);
                $varDate = Date($ngay[2].'-'.$ngay[1].'-'.$ngay[0].': 0:0:0');
                if($varDate >= $today) {
                    $check_card_expire = true ;}
                }
                foreach ($arr as & $value) {
                    if (!$customer[$value]) {
                        $status=2;
                        $list_errors+=array('customer.'.$value =>[__('customer.check').__('customer.'.$value)]);
                    }
                }
            if(!$check_card_expire){
                $status=2;
                $list_errors+=array('customer.card_expire' => [__('customer.check').__('customer.card_expire')]);
            }
            if(!$relationship)
            {
                $status=2;
                $list_errors+=array('receiver.relationship' => [__('receiver.check').__('receiver.relationship')]);
            }
        if($amount>=3000){
            if (!$customer['image_1_file_id'] && !$customer['image_2_file_id'] && !$customer['image_2_file_id']) {
                $status=1;
                $list_errors+=array('customer.image_1_file_id' => [__('customer.check').__('customer.image_1_file_id')]);
            }
        }
            $result= [$status,$list_errors];
            return $result;
    }
    private function Check_info_amount($amount, $customer,$relationship)
    {
        $result = ['status' => true,''];
        if($amount >= 3000)
            {
                $customer= $this->Check_info_Invalid($customer,$amount,$relationship);
                if($customer[0]>=1){
                return ['status' => false,$customer[1]];
            }
            }elseif($amount >= 850 )
            {
                $customer= $this->Check_info_Invalid($customer,$amount,$relationship);
                if($customer[0]>=2){
                return ['status' => false,$customer[1]];
                }
            }
        return $result;
    }
     /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);
            $data = $request->all();
            $check_info_amount = $this->Check_info_amount($data['container']['amount'], $data['customer'],
            $data['receiver']['relationship']) ;
            if(!$check_info_amount['status'])
            {
                return response(['errors'=>$check_info_amount[0]],422)->header('Content-Type', 'text/plain');
            }
            $customer = null;
            $receiver = null;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            try {
                DB::beginTransaction();
            //kiểm tra khách hàng
            if (isset($data['customer']) && !empty($data['customer'])) {
                $customerExist = null;
                if(!empty($data['customer']['email'])){
                    $customerExist = Customer::where('email', $data['customer']['email'])->first();
                }
                if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                    if(empty($customer)){
                        return response('Internal Server Error', 500)->header('Content-Type', 'text/plain');
                    }
                    if(!empty($customerExist) && $customerExist->id != $customer->id){
                        return response(__('message.cus_email_exist'), 500)->header('Content-Type', 'text/plain');
                    }
                    $customer->update([
                        'id_card' => $data['customer']['id_card'],
                        'card_expire' => $data['customer']['card_expire'],
                        'birthday' => $data['customer']['birthday'],
                        'date_issued' => $data['customer']['date_issued'],
                        'career' => $data['customer']['career'],
                        'email' => $data['customer']['email'],
                        'image_1_file_id' => $data['customer']['image_1_file_id'],
                        'image_2_file_id' => $data['customer']['image_2_file_id'],
                        'image_3_file_id' => $data['customer']['image_3_file_id']
                    ]);
                } else if(empty ($customerExist)){
                    $customer = new Customer();
                    $properties = array_keys($data['customer']);
                    foreach ($properties as $property) {
                        if (isset($data['customer'][$property]) && !empty($data['customer'][$property]))
                            $customer->$property = $data['customer'][$property];
                    }
                    $customer->agency_id = $agency->id;
                    $customer->customer_group_id = CustomerGroupEnum::MD;
                    $customer->code = "KH_";
                    $customer->save();
                    $customer->code = "KH_" . $customer->id;
                    $customer->update();
                } else{
                    return response(__('message.cus_email_exist'), 500)->header('Content-Type', 'text/plain');
                }

                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        //net người nhận chưa có
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }
                        // nếu người nhận đã có update lại
                        else
                        {
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }

                    } else {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    }
                }

            } //hết kiểm tra khách hàng
            if (isset($data['container']) && !empty($data['container'])) {
                 $count_mts= Mts::where('user_id', $user->id)->count() +1;
                $countryreceiver = Country::find($receiver->country_id);
                $countryrcustomer = Country::find($customer->country_id);
                $mts = new Mts();
                $mts->user_id = $user->id;
                $mts->agency_id = $user->agency_id;
                $mts->currency_id =  $data['container']['currency'];
                $mts->customer_id = $customer->id;
                $mts->sender_first_name = $customer->first_name;
                $mts->sender_middle_name = $customer->middle_name;
                $mts->sender_last_name = $customer->last_name;
                $mts->sender_email = $customer->email;
                $mts->sender_phone = $customer->telephone;
                $mts->sender_address = $customer->address_1;
                $mts->sender_country_id = $customer->country_id;
                $mts->sender_province_id = $customer->province_id;
                $mts->sender_city_id = $customer->city_id;
                $mts->sender_post_code = $customer->postal_code;
                $mts->shipping_address_id = $receiver->id;
                $mts->receiver_last_name = $receiver->last_name;
                $mts->receiver_first_name = $receiver->first_name;
                $mts->receiver_middle_name = $receiver->middle_name;
                $mts->receiver_email = $receiver->email;
                $mts->receiver_phone = $receiver->telephone;
                $mts->receiver_address = $receiver->address_1;
                $mts->receiver_country_id = $receiver->country_id;
                $mts->receiver_province_id = $receiver->province_id;
                $mts->receiver_city_id = $receiver->city_id;
                $mts->receiver_post_code = $receiver->postal_code;

                $mts->third_person_name = $data['container']['thirdperson_name'];
                $mts->third_person_tel = $data['container']['thirdperson_tel'];
                $mts->third_person_address = $data['container']['thirdperson_address'];
                $mts->third_person_message = $data['container']['thirdperson_message'];
                $mts->employer_name = $data['container']['employer_name'];
                $mts->employer_tel = $data['container']['employer_tel'];
                $mts->employer_address = $data['container']['employer_address'];
                $mts->employer_source = $data['container']['employer_source_of_fund'];

                $mts->reason = $data['container']['reason'];
                $mts->relationship = $data['receiver']['relationship'];
                $mts->transaction_type = $data['container']['transaction_type'];
                //them ngày 21-07-21
                $mts->bank_name = $data['container']['bank_name'];
                $mts->bank_account = $data['container']['bank_account'];
                //them ngày 21-07-21
                $mts->payment_by = $data['container']['payment_by'];
                $mts->total_goods = $data['container']['amount'];
                $mts->discount_type = $data['container']['discount_type'];
                $mts->discount_number = $data['container']['discount_number'];
                $mts->mts_status = $data['container']['status'];
                if(!empty($data['container']['pay_date'])){
                    $mts->pay_date = ConvertsUtil::dateFormat($data['container']['pay_date'], config('app.date_format'));
                }
                // $mts->total_discount = 0;
                // if($mts->discount_number > 0 && $mts->discount_type == 1){
                //     $mts->total_discount = ($mts->total_goods * $mts->discount_number) / 100;
                // }else if($mts->discount_number > 0 && $mts->discount_type == 2){
                //     $mts->total_discount = $mts->discount_number;
                // }
                // $mts->transfer_fee = ((OrderService::getPerFeeMts($mts->receiver_province_id) * $mts->total_goods)
                // + config('mts.fix_fee')) - $mts->total_discount;
                // kiểm tra huê hồng đại lý
               $surcharge= OrderService::Surcharge($mts->total_goods,$mts->discount_number,$mts->discount_type);
               $mts->discount_agency_amount=$surcharge['commission'];
               $mts->transfer_fee=$surcharge['total_fee'];
               $mts->total_final = $surcharge['total_final'];
               $mts->total_discount = $surcharge['discount'];
                // $mts->discount_agency_amount = config('mts.per_agency_fee') * $mts->transfer_fee;
				// if($mts->discount_agency_amount < 0){
				// 	$mts->discount_agency_amount = 0;
				// }
                // /* bổ sung phi tien gui nhiều*/
                // if($mts->total_goods > config('mts.mts3000'))
                // {
                //     $mts->transfer_fee += config('mts.fix_fee3000');
                // }
                //  else if($mts->total_goods > config('mts.mts1500'))
                //  {
                //        $mts->transfer_fee += config('mts.fix_fee1500');
                //  }
                // $mts->total_final = $mts->total_goods + $mts->transfer_fee;
                $mts->code = $user->code . sprintf('%04d', $count_mts) . "F".$countryrcustomer->code.$countryreceiver->code;
                $mts->save();
                if($data['container']['status'] == 4){
                    $transaction = new Transaction();
					$transaction->user_id = $mts->user_id;
                    $transaction->agency_id = $mts->agency_id;
                    $transaction->customer_id = $mts->customer_id;
                    $transaction->currency_id = $mts->currency_id;
                    $transaction->mts_id = $mts->id;
                    $transaction->code = "PT_" . $mts->code;
                    $transaction->transaction_at = ConvertsUtil::dateFormat($data['container']['pay_date'], config('app.date_format'));
                    $transaction->type = TransactionTypeEnum::RECEIPT;
                    $transaction->credit_amount = $data['container']['total'];
					$transaction->total_amount = $mts->total_final;
					$transaction->total_agency_discount = $mts->discount_agency_amount;
					$transaction->agency_discount = $transaction->credit_amount * ($transaction->total_agency_discount / $transaction->total_amount);
                    $transaction->goods_code = $mts->code;
                    $transaction->save();
                    // PointService::addPointToCustomer($mts->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                }
            }else{
                DB::rollBack();
                return response('Không tìm thấy config giá', 500)->header('Content-Type', 'text/plain');
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500)->header('Content-Type', 'text/plain');
        }

        return response($mts->id)->header('Content-Type', 'text/plain');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $mts = Mts::where('id', '=', $id)
            ->with([
                'customer' => function ($query){
                    $query->with('image1');
                    $query->with('image2');
                    $query->with('image3');
                },
                'receiver',
                'currency'
            ])
            ->first();

        if (!$mts) {
            return response('Not Found', 404)->header('Content-Type', 'text/plain');
        }

        $mts->receiver->first_name = $mts->receiver_first_name;
        $mts->receiver->middle_name = $mts->receiver_middle_name;
        $mts->receiver->last_name = $mts->receiver_last_name;
        $mts->receiver->address_1 = $mts->receiver_address;
        $mts->receiver->telephone = $mts->receiver_phone;
        $mts->receiver->postal_code = $mts->receiver_post_code;
        $mts->receiver->country_id = $mts->receiver_country_id;
        $mts->receiver->province_id= $mts->receiver_province_id;
        $mts->receiver->city_id = $mts->receiver_city_id;
        if($mts->pay_date){
            $mts->pay_date = ConvertsUtil::strToDate($mts->pay_date);
        }

        return response([
            'mts' => $mts
        ])->header('Content-Type', 'text/plain');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        $this->data['language'] = LanguageUtil::getMTSKeyJavascript();
        $MTSSurcharge=MTSSurcharge::get();
        $this->data['MTSSurcharge']=$MTSSurcharge;
        return view('mts::mts.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $check_info_amount = $this->Check_info_amount($data['container']['amount'], $data['customer'],
            $data['receiver']['relationship']) ;
            if(!$check_info_amount['status'])
            {
                return response(['errors'=>$check_info_amount[0]],422)->header('Content-Type', 'text/plain');
            }
        try {
            DB::beginTransaction();
            $customer = null;
            $receiver = null;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            if (isset($data['customer']) && !empty($data['customer'])) {
                $customerExist = null;
                if(!empty($data['customer']['email'])){
                    $customerExist = Customer::where('email', $data['customer']['email'])->first();
                }
                if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                    if(empty($customer)){
                        return response('Internal Server Error', 500)->header('Content-Type', 'text/plain');
                    }
                    if(!empty($customerExist) && $customerExist->id != $customer->id){
                        return response(__('message.cus_email_exist'), 500)->header('Content-Type', 'text/plain');
                    }
                    $customer->update([
                        'id_card' => $data['customer']['id_card'],
                        'card_expire' => $data['customer']['card_expire'],
                        'birthday' => $data['customer']['birthday'],
                        'date_issued' => $data['customer']['date_issued'],
                        'career' => $data['customer']['career'],
                        'email' => $data['customer']['email'],
                        'image_1_file_id' => $data['customer']['image_1_file_id'],
                        'image_2_file_id' => $data['customer']['image_2_file_id'],
                        'image_3_file_id' => $data['customer']['image_3_file_id']
                    ]);
                } else if(empty ($customerExist)){
                    $customer = new Customer();
                    $properties = array_keys($data['customer']);
                    foreach ($properties as $property) {
                        if (isset($data['customer'][$property]) && !empty($data['customer'][$property]))
                            $customer->$property = $data['customer'][$property];
                    }
                    $customer->agency_id = $agency->id;
                    $customer->customer_group_id = CustomerGroupEnum::MD;
                    $customer->code = "KH_";
                    $customer->save();
                    $customer->code = "KH_" . $customer->id;
                    $customer->update();
                } else{
                    return response(__('message.cus_email_exist'), 500)->header('Content-Type', 'text/plain');
                }

                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }
                        else
                        {
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }
                    } else {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    }
                }

            }

            if (isset($data['container']) && !empty($data['container'])) {
                $mts = Mts::find($data['container']['id']);

                $mts->customer_id = $customer->id;

                $mts->sender_first_name = $customer->first_name;
                $mts->sender_middle_name = $customer->middle_name;
                $mts->sender_last_name = $customer->last_name;
                $mts->sender_email = $customer->email;
                $mts->sender_phone = $customer->telephone;
                $mts->sender_address = $customer->address_1;
                $mts->sender_country_id = $customer->country_id;
                $mts->sender_province_id = $customer->province_id;
                $mts->sender_city_id = $customer->city_id;
                $mts->sender_post_code = $customer->postal_code;
                $mts->shipping_address_id = $receiver->id;
                $mts->receiver_last_name = $receiver->last_name;
                $mts->receiver_first_name = $receiver->first_name;
                $mts->receiver_middle_name = $receiver->middle_name;

                $mts->receiver_email = $receiver->email;
                $mts->receiver_phone = $receiver->telephone;
                $mts->receiver_address = $receiver->address_1;
                $mts->receiver_country_id = $receiver->country_id;
                $mts->receiver_province_id = $receiver->province_id;
                $mts->receiver_city_id = $receiver->city_id;
                $mts->receiver_post_code = $receiver->postal_code;

                $mts->third_person_name = $data['container']['thirdperson_name'];
                $mts->third_person_tel = $data['container']['thirdperson_tel'];
                $mts->third_person_address = $data['container']['thirdperson_address'];
                $mts->third_person_message = $data['container']['thirdperson_message'];
                $mts->employer_name = $data['container']['employer_name'];
                $mts->employer_tel = $data['container']['employer_tel'];
                $mts->employer_address = $data['container']['employer_address'];
                $mts->employer_source = $data['container']['employer_source_of_fund'];
                $mts->currency_id = $data['container']['currency'];
                $mts->reason = $data['container']['reason'];
                $mts->transaction_type = $data['container']['transaction_type'];
                // them ngáy 21-07-21
                $mts->bank_account = $data['container']['bank_account'];
                $mts->bank_name = $data['container']['bank_name'];
                // them ngáy 21-07-21
                $mts->payment_by = $data['container']['payment_by'];
                $mts->total_goods = $data['container']['amount'];
                $mts->discount_type = $data['container']['discount_type'];
                $mts->discount_number = $data['container']['discount_number'];
                $mts->mts_status = $data['container']['status'];
                if(!empty($data['container']['pay_date'])){
                    $mts->pay_date = ConvertsUtil::dateFormat($data['container']['pay_date'], config('app.date_format'));
                }
                // $mts->total_discount = 0;
                // if($mts->discount_number > 0 && $mts->discount_type == 1){
                //     $mts->total_discount = ($mts->total_goods * $mts->discount_number) / 100;
                // }else if($mts->discount_number > 0 && $mts->discount_type == 2){
                //     $mts->total_discount = $mts->discount_number;
                // }
                // $mts->transfer_fee = ((OrderService::getPerFeeMts($mts->receiver_province_id) * $mts->total_goods) + config('mts.fix_fee')) - $mts->total_discount;
                // $mts->discount_agency_amount = config('mts.per_agency_fee') * $mts->transfer_fee;
				// if($mts->discount_agency_amount < 0){
				// 	$mts->discount_agency_amount = 0;
				// }
                //  if($mts->total_goods > config('mts.mts3000'))
                // {
                //     $mts->transfer_fee += config('mts.fix_fee3000');
                // }
                //     else if($mts->total_goods > config('mts.mts1500'))
                //     {
                //        $mts->transfer_fee += config('mts.fix_fee1500');
                //     }
                // $mts->total_final = $mts->total_goods + $mts->transfer_fee;
                $surcharge= OrderService::Surcharge($mts->total_goods,$mts->discount_number,$mts->discount_type);
                $mts->discount_agency_amount=$surcharge['commission'];
                $mts->transfer_fee=$surcharge['total_fee'];
                $mts->total_final = $surcharge['total_final'];
                $mts->total_discount = $surcharge['discount'];
                $mts->update();

                $transactionExist = Transaction::where('mts_id', $mts->id)->first();
                if(empty($transactionExist) && $data['container']['status'] == 4){
                    $transaction = new Transaction();
					$transaction->user_id = $mts->user_id;
                    $transaction->agency_id = $mts->agency_id;
                    $transaction->customer_id = $mts->customer_id;
                    $transaction->currency_id = $mts->currency_id;
                    $transaction->mts_id = $mts->id;
                    $transaction->code = "PT_" . $mts->code;
                    if(!empty($data['container']['pay_date'])){
                        $transaction->transaction_at = ConvertsUtil::dateFormat($data['container']['pay_date'], config('app.date_format'));
                    }else{
                        $transaction->transaction_at = date("Y-m-d H:i:s");
                    }

                    $transaction->type = TransactionTypeEnum::RECEIPT;
                    $transaction->credit_amount = $data['container']['total'];
					$transaction->total_amount = $mts->total_final;
					$transaction->total_agency_discount = $mts->discount_agency_amount;
					$transaction->agency_discount = $transaction->credit_amount * ($transaction->total_agency_discount / $transaction->total_amount);
                    $transaction->goods_code = $mts->code;
                    $transaction->save();

                    // PointService::addPointToCustomer($mts->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                }
            }else{
                DB::rollBack();
                return response('Không tìm thấy config giá', 500)->header('Content-Type', 'text/plain');
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500)->header('Content-Type', 'text/plain');
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function cancelOrder(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []])->header('Content-Type', 'text/plain');
        }
        $mts = Mts::find($request->get('id'));
        if(empty($mts)){
            return response(["success" => false, "message" => __('message.err_mts_is_not_valid'), "data" => []])->header('Content-Type', 'text/plain');
        }
        try {
            DB::beginTransaction();

            $mts->mts_status = MtsStatusEnum::CANCEL;
            $mts->update();

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_mts_success'), "data" => ['refUrl' => route('mts.index')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []])->header('Content-Type', 'text/plain');
        }
    }
}
