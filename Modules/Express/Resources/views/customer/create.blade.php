@extends('layouts.admin.index')

@section('body')
    <div class="panel panel-default" id="create-express" style="padding: 15px;" ng-app="ExpressApp" ng-controller="ExpressCreateController">
        <div class="modal fade custom-width" id="modal-errors">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                    </div>
                    <div class="modal-body text-danger">
                        <div class="row" ng-if="errors.addressFrom.length || errors.addressTo.length || errors.packages.others.length || errors.packages.boxes.length|| errors.order.length">
                            <div class="col-sm-12">
                                <div ng-if="errors.order.length">
                                    <h4>{{ __('label.order') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.order">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.addressFrom.length">
                                    <h4>{{ __('label.sender') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.addressFrom">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.addressTo.length">
                                    <h4>{{ __('label.receiver') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.addressTo">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.packages.others.length || errors.packages.boxes.length">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.packages') }}</h4>
                                            <ul ng-if="errors.packages.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="errors.packages.boxes.length">
                                                <li ng-repeat="productErrors in errors.packages.boxes" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-if="errors.quotes.length">
                            <div class="row" ng-repeat="error in errors.quotes" >
                                <div class="col-sm-12">
                                    <h4>{{ __('label.quote') }} @{{ $index + 1 }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.quotes">@{{ error }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">
                            {{ __('label.close') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade custom-width" id="modal-customers-search-result">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">&times;</button>
                        <h4 class="modal-title">{{ __('label.search_result') }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>{{ __('customer.code') }}</th>
                                    <th>{{ __('customer.first_name') }}</th>
                                    <th>{{ __('customer.telephone') }}</th>
                                    <th>{{ __('customer.address_1') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="customer in customersSearchResult"
                                    style="cursor: pointer;"
                                    ng-click="selectCustomer(customer)">
                                    <td ng-bind="customer.code"></td>
                                    <td ng-bind="customer.full_name"></td>
                                    <td ng-bind="customer.telephone"></td>
                                    <td ng-bind="customer.address_1"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">
                            {{ __('label.close') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-heading" style="font-size: 1.6em;">
           {{ __('express.create') }}
        </div>
        <div class="panel-body">
            <form  id="form-create" name="createForm" novalidate>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading">
                        <div style="margin: -12px 0;width: 80%;float: right;">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.telephone" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.telephone') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.code" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.code') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.first_name" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.first_name') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.middle_name" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.middle_name') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.last_name" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.last_name') }}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-white" ng-click="searchCustomers()" ng-disabled="submittedSearchCustomers">
                                            <i class="fa fa-search"></i> {{ __('label.find') }}
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-body" style="margin: -10px -20px;padding-top: 20px">
                        <div class="col-sm-12">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <h3>{{ __('express.customer') }}</h3>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>{{ __('customer.code') }}</th>
                                                <th>{{ __('customer.first_name') }}</th>
                                                <th>{{ __('customer.telephone') }}</th>
                                                <th>{{ __('customer.address_1') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-if="show_customer">
                                                <td ng-bind="customer.code"></td>
                                                <td ng-bind="customer.full_name"></td>
                                                <td ng-bind="customer.telephone"></td>
                                                <td ng-bind="customer.address_1"></td>
                                            </tr>
                                            <tr ng-if="!show_customer">
                                                <td colspan="4"> {{ __('express.no_customer_selected') }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;">
                                        <h3>{{ __('express.ship_from') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.company') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.company" ng-model="addressFrom.company">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.address_1" ng-model="addressFrom.address_1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_2') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.address_2" ng-model="addressFrom.address_2">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="addressFrom.country_id" class="form-control" ng-model="addressFrom.country_id" ng-change="getProvincesFrom()" >
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressFrom.province_id" class="form-control" ng-model="addressFrom.province_id" ng-change="getCitiesFrom()" >
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesFrom" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressFrom.city_id" class="form-control" ng-model="addressFrom.city_id" >
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesFrom" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('express.postal_code') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="addressFrom.postal_code" ng-model="addressFrom.postal_code" ng-blur="addressFrom.postal_code = addressFrom.postal_code.split(' ').join('')">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.telephone" ng-model="addressFrom.telephone" >
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.email') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.email" ng-model="addressFrom.email">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.attention') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.attention" ng-model="addressFrom.attention">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressFrom.confirm_delivery" value="1"  ng-model="addressFrom.confirm_delivery">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.confirm_delivery') }}
                                                </td>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressFrom.residential" value="1" ng-model="addressFrom.residential">
                                                </td>
                                                <td class="col-label" style="text-align: left">
                                                    {{ __('express.residential') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.instruction') }}
                                                </td>
                                                <td colspan="3">
                                                    <textarea class="form-control" name="addressFrom.instruction" ng-model="addressFrom.instruction"></textarea>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <h3>{{ __('express.ship_to') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.company') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.company" ng-model="addressTo.company">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.address_1" ng-model="addressTo.address_1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_2') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.address_2" ng-model="addressTo.address_2">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="addressTo.country_id" class="form-control" ng-model="addressTo.country_id" ng-change="getProvincesTo()">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressTo.province_id" class="form-control" ng-model="addressTo.province_id" ng-change="getCitiesTo()">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesTo" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressTo.city_id" class="form-control" ng-model="addressTo.city_id"  >
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesTo" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('express.postal_code') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.postal_code" ng-model="addressTo.postal_code" ng-blur="addressTo.postal_code = addressTo.postal_code.split(' ').join('')">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.telephone" ng-model="addressTo.telephone" >
                                                </td>

                                                <td class="col-label">{{ __('express.email') }}</td>
                                                <td>
                                                    <input type="text" class="form-control y" name="addressTo.email" ng-model="addressTo.email">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.attention') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.attention" ng-model="addressTo.attention">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressTo.notify_recipient" value="1"  ng-model="addressTo.notify_recipient">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.notify_recipient') }}
                                                </td>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressTo.residential" value="1" ng-model="addressTo.residential">
                                                </td>
                                                <td class="col-label" style="text-align: left">
                                                    {{ __('express.residential') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.instruction') }}
                                                </td>
                                                <td colspan="3">
                                                    <textarea class="form-control" name="addressTo.instruction" ng-model="addressTo.instruction" ></textarea>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="panel panel-color panel-gray panel-border" id="package-info">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-cubes"></i> {{ __('express.boxes') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0">
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;padding-top: 15px">
                                        <h3 >{{ __('express.packaging') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.package_type') }}
                                                </td>
                                                <td width="35%">
                                                    <select name="packages.type" ng-model="packages.type" class="form-control" ng-change="changePackageType()">
                                                        <option ng-repeat="val in packageTypes" ng-value="val.id">@{{ val.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label" width="15%" >
                                                    {{ __('express.package_quantity') }}
                                                </td>
                                                <td width="20%">
                                                    <select name="packages.quantity" id="" class="form-control" ng-model="packages.quantity" ng-change="changeQuantity()" ng-disabled="disable_field">
                                                        <option ng-repeat="val in quantities" ng-value="val">@{{ val }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="padding-top: 15px">
                                        <h3> {{ __('express.reference') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="30%" style="text-align: left">
                                                    {{ __('express.ref_code') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control " name="referenceCode" ng-model="packages.referenceCode" >
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12" style="padding-top: 15px">
                            <table class="table table-form" style="margin-bottom: 0;">
                                <tbody>
                                <tr>
                                    <td>
                                        <h3> {{ __('express.dimension') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td colspan="3">
                                                    <p class="note"><strong> {{ __('express.package_note') }}</strong></p>
                                                </td>
                                            </tr>
                                            <tr class="no-border-top">
                                                <td class="col-label" width="10%" style="    min-width: 100px;">
                                                    {{ __('express.unit') }}
                                                </td>
                                                <td width="15%">
                                                    <select name="packages.dimType" ng-model="packages.dimType" class="form-control" style="min-width: 100px;" ng-change="changeDimType()">
                                                        <option ng-repeat="val in dimTypes" ng-value="val.id" ng-hide="val.id == 2">@{{ val.name }}</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="table-responsive">
                                <table class="table table-form table-container table-bordered">
                                    <thead>
                                    <tr id="productHead">
                                        <th>#</th>
                                        <th id="lengthHeader">{{ __('express.length') }} (in) <i class="text-danger">*</i></th>
                                        <th id="widthHeader">{{ __('express.width') }} (in) <i class="text-danger">*</i></th>
                                        <th id="heightHeader">{{ __('express.height') }} (in) <i class="text-danger">*</i></th>
                                        <th id="weightHeader">{{ __('express.weight') }} (lbs)</th>
                                        <th>{{ __('express.insurance_amount') }} CAD($)</th>
                                        <th ng-if="show_field">{{ __('express.freight_class') }}</th>
                                        <th ng-if="show_field">{{ __('express.nmfc_code') }}</th>
                                        <th>{{ __('express.cod_amount') }} ($)</th>
                                        <th class="nmfc_description">{{ __('express.description') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in packages.boxes">
                                        <td style="padding-top: 14px;" ng-bind="$index + 1"></td>
                                        <td><input class="form-control" type="text" name="product.length" size="5" ng-model="product.length" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.width" size="5" ng-model="product.width" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.height" size="5" ng-model="product.height" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.weight" size="5" ng-model="product.weight" ></td>
                                        <td><input class="form-control" type="text" name="product.insuranceAmount" ng-model="product.insuranceAmount" ng-disabled="disable_field"></td>
                                        <td ng-if="show_field"><input class="form-control" type="text" name="product.freightClass" ng-model="product.freightClass" ng-disabled="disable_field" ></td>
                                        <td ng-if="show_field"><input class="form-control" type="text" name="product.nmfcCode" ng-model="product.nmfcCode" ng-disabled="disable_field" ></td>
                                        <td><input class="form-control" type="text" name="product.codValue" size="5" value="0.0" id="codValue" ng-model="product.codValue" ng-disabled="!services.codPaymentType"></td>
                                        <td class="nmfc_description"><input class="form-control" type="text" name="product.description" maxlength="34" size="40" ng-model="product.description"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border" id="service">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;">{{ __('express.service') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0">
                        <table class="table table-form" style="margin-top: 15px;">
                            <tbody>
                            <tr>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.addition_service') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td class="col-label" width="15%">
                                                {{ __('express.saturday_delivery') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="saturdayDelivery" ng-model="services.saturdayDelivery">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.saturday_pickup') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="saturdayPickup" ng-model="services.saturdayPickup">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.hold_pickup') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="holdForPickup" ng-model="services.holdForPickup">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.docs_only') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="docsOnly" ng-model="services.docsOnly">
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="no-border-top">
                                            <td class="col-label">
                                                {{ __('express.dangerous_good') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="dangerousGoods" class="form-control" ng-model="services.dangerousGoods">
                                                    <option value="" selected="selected">None</option>
                                                    <option value="2">Limited Quantity</option>
                                                    <option value="3">500 Kg Exemption</option>
                                                    <option value="4">Fully Regulated</option>
                                                </select>
                                            </td>
                                            <td class="col-label">
                                                {{ __('express.signature_required') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="signatureRequired" class="form-control" ng-model="services.signatureRequired">
                                                    <option value="" selected="selected">No</option>
                                                    <option value="3">Yes</option>
                                                    <option value="4">Adult Signature</option>
                                                </select>
                                            </td>
                                            <td class="col-label" >
                                                {{ __('express.insurance_type') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="insuranceType" class="form-control" ng-model="services.insuranceType">
                                                    <option value="">None</option>
                                                    <option value="1">CWW</option>
                                                    <option value="2" selected="selected">Carrier</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;"> {{ __('express.cod_service') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td class="col-label" width="15%" style="min-width: 100px">
                                                COD
                                            </td>
                                            <td width="35%" style="min-width: 150px">
                                                <select name="codPaymentType" ng-model="services.codPaymentType" class="form-control">
                                                    <option value="" selected="selected">None</option>
                                                    <option value="1">{{ __('express.check') }}</option>
                                                    <option value="2">{{ __('express.certified_check') }}</option>
                                                </select>
                                            </td>

                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div ng-if="quotes.length" class="panel panel-color panel-gray panel-border" id="service-list">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;">{{ __('express.quote') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0">
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr ng-repeat="quote in quotes.slice(0, 5)">
                                            <td width="10%">
                                                <input type="radio" style="margin-top: 8px;margin-left: 5px" name="serviceId" ng-value="$index+1" ng-click="setService($index)">
                                            </td>
                                            <td class="col-label" width="15%">
                                                @{{ quote.carrierName}}
                                            </td>
                                            <td class="col-label" width="15%">
                                                @{{ quote.serviceName}}
                                            </td>
                                            <td class="col-label" width="25%">
                                                @{{ quote.totalCharge}}@{{  quote.currency }}
                                            </td>
                                            <td >

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <table class="table table-bordered" ng-if="showCreate">
                    <tbody>
                    <tr>
                        <td class="text-right" width="15%">{{ __('express.carrier') }}:</td>
                        <td class="text-left" width="19%"><strong ng-bind="quote.carrierName"></strong></td>
                        <td class="text-right" width="15%">{{ __('express.service') }}:</td>
                        <td class="text-left" width="18%"><strong ng-bind="quote.serviceName"></strong></td>
                        <td class="text-right" width="15%">{{ __('express.est_transitday') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.transitDays"></strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">{{ __('express.delivery_date') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.deliveryDate"></strong></td>
                        <td class="text-right">{{ __('label.currency_pay') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.currency"></strong></td>
                        <td class="text-right">{{ __('express.base_charge') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.baseCharge"></strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">{{ __('express.total_tariff') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.totalTariff"></strong></td>
                        <td class="text-right">{{ __('express.base_charge_tariff') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.baseChargeTariff"></strong></td>
                        <td class="text-right">{{ __('express.fuel_surcharge_tariff') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.fuelSurchargeTariff"></strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">{{ __('express.fuel_surcharge') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.fuelSurcharge"></strong></td>
                        <td class="text-right">{{ __('express.total_charge') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.totalCharge"></strong></td>
                        <td class="text-right">{{ __('express.surcharge_amount') }}:</td>
                        <td class="text-left"><strong ng-bind="quote.surcharge.amount"></strong></td>
                    </tr>
                    <tr>
                        <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                        <td class="text-left" ng-if="methods.length > 0">
                            <select name="currency" id="payMethod" class="form-control" ng-model="order.payment_method">
                                <option value="" selected="selected">Select method</option>
                                <option ng-repeat="method in methods" ng-value="method.code">@{{ method.name }}</option>
                            </select>
                        </td>
                        <td></td>
                        <td></td><td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-right">
                    <a href="javascript:void(0);" class="btn btn-white" ng-click="clearExpress()">
                        <i class="fa fa-arrow-left"></i> {{ __('label.cancel') }}
                    </a> 
                    <button type="button" class="btn btn-info" ng-click="searchExpress()" ng-disabled="submitted">
                        <i class="fa fa-search"></i> {{ __('label.search_express') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
                    </button>
                    <button type="button" class="btn btn-info" ng-if="showCreate" ng-click="createExpress()" ng-disabled="submitted">
                        <i class="fa fa-check"></i> {{ __('label.approve') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer')
    <script src="{{ asset('js/admin/app/express-customer-create.js?t=' . File::lastModified(public_path('js/admin/app/express-customer-create.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-create').scope();
            scope.init();
            scope.initQuantity();
            scope.changePackageType();
        });
    </script>
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/admin/express.css') }}">
@endsection