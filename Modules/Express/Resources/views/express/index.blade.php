@extends('layouts.admin.index')

@section('body')
    <div class="panel panel-default user">
        <div class="panel-heading">
            {{ __('express.orders_title')}}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-1">
                    <div class="form-group">
                        <a href="{{ route('admin.express.create') }}" class="btn btn-success btn-single">
                            <i class="fa fa-plus-circle"></i> {{ __('label.add_order')}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <form action="{{ route('admin.express.list') }}" method="get" role="form" class="form-inline">
                            <div class="form-group">
                                <input name="from_date" autocomplete="off" id="from_date" value="{{ request()->query('from_date')}}"
                                       class="form-control" type="text" style="width: 160px;"
                                       placeholder="{{ __('label.from_date') }}">
                            </div>
                            <div class="form-group">
                                <input name="to_date" autocomplete="off" id="to_date" value="{{ request()->query('to_date')}}"
                                       class="form-control" type="text" style="width: 160px;"
                                       placeholder="{{ __('label.to_date') }}">
                            </div>
                            <div class="form-group">
                                <input name="code" value="{{ request()->query('code')}}" class="form-control"
                                       type="text" style="width: 190px;" placeholder="{{ __('express.order_code') }}">
                            </div>
                            <div class="form-group">
                                <input name="receiver_phone" value="{{ request()->query('receiver_phone')}}"
                                       class="form-control" type="text" style="width: 190px;"
                                       placeholder="{{ __('label.receiver_phone') }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-white btn-single">
                                    <i class="fa fa-search"></i> {{ __('label.find') }}
                                </button>
                                <a href="{{ route('admin.express.list') }}" class="btn btn-white btn-single">
                                    <i class="fas fa-broom"></i> {{ __('label.clear') }}
                                </a>
                                 <button type="button" class="btn btn-white btn-single" onclick="order.export();">
                                    <i class="fas fa-file-excel"></i> {{ __('label.export_excel') }}</i>
                        </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <form id="shipment-package-form">
                  <input class="form-control" id="mySearch" type="text" placeholder= "{{ __('label.filter_list') }}">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" style="min-width: 2500px;">
                        <thead>
                        <tr>
                            <th width="120">{{ __('label.action') }}</th>
                            <th width="120">{{ __('label.form_code') }}</th>
                            <th width="120">{{ __('label.shipping_status') }}
                                <br> {{ __('label.payment_status') }}</th>
                                @if(auth()->user()->role->admin)
                                <th width="120">{{ __('express.eshipper_order_id') }}</th>
                                @endif
                            <th width="120">{{ __('label.pick_scheduled') }}</th>
                            <th width="80">{{ __('label.employee') }}</th>
                            <th width="120">{{ __('label.sender') }}</th>
                            <th width="120">{{ __('label.receiver') }}</th>
                            <th width="120" class="text-center">{{ __('label.date') }}</th>
                            <th width="150">{{ __('label.service_name') }}</th>
                            <th width="120" class="text-right">{{ __('label.total_pay') }} (CAD)</th>
                            <th width="120" class="text-right">{{ __('label.total_weight') }} (Lbs)</th>
                            <th width="110" class="text-right">{{ __('label.pay') }} (CAD)</th>
                            <th width="110" class="text-right">{{ __('label.total_discount') }} (CAD)</th>
                            <th width="110" class="text-right">{{ __('label.commission') }} (CAD)</th>
                            <th width="160">{{ __('label.address') }}</th>
                            <th width="140">{{ __('label.city') }}</th>
                            <th width="140">{{ __('label.province') }}</th>
                            <th width="120">{{ __('label.telephone') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                        </thead>
                        <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                            <?php $no = 1; ?>
                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.express.edit', $order->id) }}" class="btn btn-xs btn-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('admin.express.print', $order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                            <i class="fa fa-print "></i>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-info" onclick="extent.extra({{$order->id}}, '{{ csrf_token() }}');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        <button type="button" class="btn btn-xs btn-info" onclick="extent.refund({{$order->id}}, '{{ csrf_token() }}');">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        <?php if(!empty($order->eshipper_label)){?>
                                        <a href="{{ route('admin.express.pdf', $order->id) }}" class="btn btn-xs btn-primary" download="label_{{ $order->express_order_id }}.pdf">
                                            <i class="fa fa-file-pdf"></i>
                                        </a>
                                        <?php }?>
                                        <?php if(!empty($order->eshipper_custom_label)){?>
                                        <a href="{{ route('admin.express.custominvoicepdf', $order->id) }}" class="btn btn-xs btn-primary" download="custom_label_{{ $order->express_order_id }}.pdf">
                                            <i class="fa fa-anchor "></i>
                                        </a>
                                        <?php }?>
                                        <?php if(!empty($order->express_order_id)){?>
                                        <button type="button" class="btn btn-xs btn-info" onclick="order.viewHistoryEshiper({{ $order->id }})">
                                            <i class="fa fa-truck"></i>
                                        </button>
                                        <?php }?>
                                    </td>
                                    <td>{{ $order->code }}</td>
                                    <td>
                                        <span class="{{ $order->eshipper_status_label }}">{{ $order->eshipper_status_name }}</span>
                                    <br> <span class="{{ $order->payment_status_label }}">{{ $order->payment_status_name }}</span>
                                    </td>
                                    @if(auth()->user()->role->admin)
                                    <td>
                                        {{ $order->express_order_id }}
                                    </td>
                                    @endif
                                    <td>
                                        {{ $order->pickupNumber }}
                                    </td>
                                    <td>{{ isset($order->user->code) ? $order->user->code : 'ADM' }}</td>
                                    <td>{{ $order->sender_full_name }}</td>
                                    <td>{{ $order->receive_full_name }}</td>
                                    <td class="text-center">{{ $order->created_date }}</td>
                                    <td>
                                         @if ($order->eshiper_service)
                                        {{ $order->eshiper_service['carrierName'] }} {{ $order->eshiper_service['serviceName'] }}
                                        @endif
                                    </td>
                                    <td class="text-right">{{ number_format($order->total_final, 2) }}</td>
                                    <td class="text-right">{{ number_format($order->total_weight, 2) }}</td>
                                    <td class="text-right">{{ number_format($order->total_paid_amount, 2) }}</td>
                                    <td class="text-right">{{ number_format($order->eshiper_total_discount, 2) }}</td>
                                    <td class="text-right">{{ number_format($order->agency_discount, 2) }}</td>
                                    <td>{{ $order->receiver_address }}</td>
                                    <td>{{ $order->receiver_city->name }}</td>
                                    <td>{{ $order->receiver_province->name }}</td>
                                    <td>
                                        {{ $order->receiver_phone }}<br>
                                        {{ $order->receiver_cellphone }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.express.edit', $order->id) }}" class="btn btn-xs btn-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('admin.express.print', $order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                            <i class="fa fa-print "></i>
                                        </a>
                                        @if($order->payment_status == 2 || $order->payment_status == 3)
                                        <button type="button" class="btn btn-xs btn-info" onclick="extent.extra({{$order->id}}, '{{ csrf_token() }}');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        <button type="button" class="btn btn-xs btn-info" onclick="extent.refund({{$order->id}}, '{{ csrf_token() }}');">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        @endif
                                        <?php if(!empty($order->eshipper_label)){?>
                                        <a href="{{ route('admin.express.pdf', $order->id) }}" class="btn btn-xs btn-primary" download="label.pdf">
                                            <i class="fa fa-file-pdf"></i>
                                        </a>
                                        <?php }?>
                                        <?php if(!empty($order->eshipper_custom_label)){?>
                                        <a href="{{ route('admin.express.custominvoicepdf', $order->id) }}" class="btn btn-xs btn-primary" download="custom_label_{{ $order->express_order_id }}.pdf">
                                            <i class="fa fa-anchor "></i>
                                        </a>
                                        <?php }?>
                                        <?php if(!empty($order->express_order_id)){?>
                                        <button type="button" class="btn btn-xs btn-info" onclick="order.viewHistoryEshiper({{ $order->id }})">
                                            <i class="fa fa-truck"></i>
                                        </button>
                                        <?php }?>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">{{ __('label.no_records') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="paginate-single">
                    {{ $orders->appends(request()->query())->links() }}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="/js/admin/app/extent.js"></script>
    <script src="/js/express/order.js"></script>
    <script>
      $(function () {
        if($('#from_date').val()==="")
        {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', -30);
        }
        else
        {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        }
       if($("#to_date").val()==="")
       {
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', 1);
       }
       else
       {
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
       }
    });
    </script>
@endsection
