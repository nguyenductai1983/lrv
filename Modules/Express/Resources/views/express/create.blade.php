@extends('layouts.admin.index')

@section('body')
    <div class="panel panel-default" id="create-express" style="padding: 15px;" ng-app="ExpressApp" ng-controller="ExpressCreateController">
        <!-- thông báo lỗi -->
        <div class="modal fade custom-width" id="modal-errors">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                    </div>
                    <div class="modal-body text-danger">
                        <div class="row" ng-if="errors.addressFrom.length || errors.addressTo.length || errors.pickups.length || errors.packages.others.length || errors.packages.boxes.length || errors.order.length || errors.custom.length">
                            <div class="col-sm-12">
                                <div ng-if="errors.order.length">
                                    <h4>{{ __('label.order') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.order">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.addressFrom.length">
                                    <h4>{{ __('label.sender') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.addressFrom">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.addressTo.length">
                                    <h4>{{ __('label.receiver') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.addressTo">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.pickups.length">
                                    <h4>{{ __('express.pickup_info') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.pickups">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.custom.length">
                                    <h4>{{ __('express.custom_invoice') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.custom">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.packages.others.length || errors.packages.boxes.length">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.packages') }}</h4>
                                            <ul ng-if="errors.packages.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="errors.packages.boxes.length">
                                                <li ng-repeat="productErrors in errors.packages.boxes" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-if="errors.quotes.length">
                            <div class="row" ng-repeat="error in errors.quotes" >
                                <div class="col-sm-12">
                                    <h4>{{ __('label.quote') }} @{{ $index + 1 }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.quotes">@{{ error }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div ng-if="errors.system.length">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <h4>{{ __('express.system_error') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.system">@{{ error }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">
                            {{ __('label.close') }}
                        </button>
                        <button type="button" class="btn btn-info" ng-click="saveExpress()" ng-if="save_order" ng-disabled="save_order_wait">
                        <i class="fa fa-check"></i> {{ __('label.save_order') }} <i class="fa fa-refresh fa-spin" ng-if="save_order_wait"></i>
                    </button>
                    </div>
                </div>
            </div>
        </div>
<!--hết thông báo lỗi -->

        <div class="modal fade custom-width" id="modal-customers-search-result">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">&times;</button>
                        <h4 class="modal-title">{{ __('label.search_result') }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>{{ __('customer.code') }}</th>
                                    <th>{{ __('customer.first_name') }}</th>
                                    <th>{{ __('customer.telephone') }}</th>
                                    <th>{{ __('customer.address_1') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="customer in customersSearchResult"
                                    style="cursor: pointer;"
                                    ng-click="selectCustomer(customer)">
                                    <td ng-bind="customer.code"></td>
                                    <td ng-bind="customer.full_name"></td>
                                    <td ng-bind="customer.telephone"></td>
                                    <td ng-bind="customer.address_1"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">
                            {{ __('label.close') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-heading" style="font-size: 1.6em;">
           {{ __('express.create') }}
        </div>
        <div class="panel-body" style="padding: 0;">
            <form  id="form-create" name="createForm" novalidate>
                <div class="panel panel-color panel-gray panel-border" style="padding:0">
                    <div class="panel-heading" style="margin: 0">
                        <div style="margin: -12px 0;width: 80%;float: right;">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.telephone" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.telephone') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.code" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.code') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.first_name" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.first_name') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.middle_name" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.middle_name') }}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="customerSearch.last_name" ng-keypress="$event.keyCode == 13 &amp;&amp; searchCustomers()" placeholder="{{ __('customer.last_name') }}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-white" ng-click="searchCustomers()" ng-disabled="submittedSearchCustomers">
                                            <i class="fa fa-search"></i> {{ __('label.find') }}
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-body" style="margin: -10px -20px;padding-top: 20px">
                        <div class="col-sm-6" style="padding-right: 0px">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;">
                                        <h3>{{ __('express.ship_from') }} <strong ng-bind="addressFrom.code" class="ng-binding"></strong> <button type="button" ng-click="EditCustomer();">
                                            <i class="fa fa-edit text-success"></i>
                                        </button></h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.first_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.first_name" ng-model="addressFrom.first_name" ng-disabled="Updatecustomer">
                                                </td>
                                                <td width="15%" class="col-label">
                                                    {{ __('express.middle_name') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.middle_name" ng-model="addressFrom.middle_name" ng-disabled="Updatecustomer">
                                                </td>
                                                <td width="10%" class="col-label">
                                                    {{ __('express.last_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.last_name" ng-model="addressFrom.last_name" ng-disabled="Updatecustomer">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="4">
                                                    <input type="text" class="form-control" name="addressFrom.address_1" ng-model="addressFrom.address_1" ng-disabled="Updatecustomer">
                                                </td>
                                                 <td class="col-label">
                                                <button type="button" class="btn btn-info btn-xs" ng-click="showAddress2(1)">{{ __('express.address_2') }}</button>
                                            </td>
                                            </tr>
                                            <tr ng-show="customeraddress2">
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_2') }}
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="addressFrom.address_2" ng-model="addressFrom.address_2" ng-disabled="Updatecustomer">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="addressFrom.country_id" class="form-control" ng-model="addressFrom.country_id" ng-change="getProvincesFrom()"  ng-disabled="Updatecustomer">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressFrom.province_id" class="form-control" ng-model="addressFrom.province_id" ng-change="getCitiesFrom()"  ng-disabled="Updatecustomer">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesFrom" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressFrom.city_id" class="form-control" ng-model="addressFrom.city_id"  ng-change="getPostCodeFrom()" ng-disabled="Updatecustomer">
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesFrom" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('express.postal_code') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="addressFrom.postal_code" ng-model="addressFrom.postal_code" ng-disabled="Updatecustomer" ng-blur="addressFrom.postal_code = addressFrom.postal_code.split(' ').join('')">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.telephone" ng-model="addressFrom.telephone"  ng-disabled="Updatecustomer">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.cellphone') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.cellphone" ng-model="addressFrom.cellphone" ng-disabled="Updatecustomer">
                                                </td>
                                                 </tr>
                                                 <tr>
                                                    <td class="col-label">{{ __('express.email') }} </td>
                                                <td colspan="3">
                                                    <input type="text" class="form-control" name="addressFrom.email" ng-model="addressFrom.email" autocomplete="off">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                                <td colspan="2" class="col-label" style="padding-bottom: 10px;">
                                                    <button type="button" class="btn btn-info btn-xs" ng-click="showMoreInfoCustomer = !showMoreInfoCustomer">
                                                        <i class="fa fa-plus"></i> {{ __('label.more_info') }}
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.id_card') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.id_card" ng-model="addressFrom.id_card" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.card_expire') }}
                                                </td>
                                                <td>
                                                    <div class="input-group date-picker" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                        <input type="text" class="form-control" name="addressFrom.card_expire" ng-model="addressFrom.card_expire" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-white">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-show="showMoreInfoCustomer">
                                                <td class="col-label">
                                                    {{ __('customer.birthday') }}
                                                </td>
                                                <td>
                                                    <div class="input-group date-picker" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                                                        <input type="text" class="form-control" name="addressFrom.birthday" ng-model="addressFrom.birthday" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-white">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="col-label">
                                                    {{ __('customer.career') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.career" ng-model="addressFrom.career" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <h3>{{ __('express.ship_to') }} <strong ng-bind="addressTo.id" class="ng-binding"></strong>  <button type="button" ng-click="Editreceiver()"><i class="fa fa-edit text-success"></i></h3>
                                        <div style="float: right;position: absolute;right: 24px;top: 4px;" ng-if="addressSearchResult">
                                            <select class="form-control" ng-model="selectedId"  ng-change="pickAddress(selectedId)">
                                                <option value="" selected="selected">{{ __('label.select_address') }}</option>
                                                <option ng-if="addressSearchResult" ng-repeat="item in addressSearchResult" ng-value="item.id">@{{item.last_name}} @{{item.middle_name}} @{{item.first_name}}</option>
                                            </select>
                                        </div>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.first_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.first_name" ng-model="addressTo.first_name" ng-disabled="Updatereciver">
                                                </td>
                                                <td width="15%" class="col-label">
                                                    {{ __('express.middle_name') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.middle_name" ng-model="addressTo.middle_name" ng-disabled="Updatereciver">
                                                </td>
                                                <td width="10%" class="col-label">
                                                    {{ __('express.last_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.last_name" ng-model="addressTo.last_name" ng-disabled="Updatereciver">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="4">
                                                    <input type="text" class="form-control" name="addressTo.address_1" ng-model="addressTo.address_1" ng-disabled="Updatereciver">
                                                </td>
                                                 <td class="col-label">
                                                <button type="button" class="btn btn-info btn-xs" ng-click="showAddress2(2)">{{ __('express.address_2') }}</button>
                                            </td>
                                            </tr>
                                            <tr ng-show="reciveraddress2">
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_2') }}
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="addressTo.address_2" ng-model="addressTo.address_2" ng-disabled="Updatereciver">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="addressTo.country_id" class="form-control" ng-model="addressTo.country_id" ng-change="getProvincesTo()" ng-disabled="Updatereciver">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressTo.province_id" class="form-control" ng-model="addressTo.province_id" ng-change="getCitiesTo()" ng-disabled="Updatereciver">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesTo" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressTo.city_id" class="form-control" ng-model="addressTo.city_id" ng-change="getPostCodeTo()" ng-disabled="Updatereciver" >
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesTo" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('express.postal_code') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.postal_code" ng-model="addressTo.postal_code" ng-disabled="Updatereciver" ng-blur="addressTo.postal_code = addressTo.postal_code.split(' ').join('')">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.telephone" ng-model="addressTo.telephone"  ng-disabled="Updatereciver">
                                                </td>
                                                 <td class="col-label">
                                                    {{ __('express.cellphone') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.cellphone" ng-model="addressTo.cellphone"  ng-disabled="Updatereciver">
                                                </td>
                                                </tr>
                                                <tr>
                                                <td class="col-label">{{ __('express.email') }}</td>
                                                <td colspan="3">
                                                    <input type="text" class="form-control y" name="addressTo.email" ng-model="addressTo.email" ng-disabled="Updatereciver">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn-sm btn-success" ng-click="createContact()" ng-if="SaveContact">{{ __('label.save_contact') }} <i class="fa fa-save"></i></button>
                                        <button type="button" class="btn btn-primary btn-sm" ng-if="addressFrom.id" ng-click="removeCustomer()">
                                                    <i class="fas fa-trash"></i> {{ __('label.clear') }}
                                                </button>
                                       <div ng-if="contact_save" class='alert alert-success' role="alert"> {{ __('label.save_contact_success')}}
                             </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
<div class="container-fluid">
<div ng-show="showMoreInfoCustomer" class="row" >
   <div class="col-sm-3 col-xs-4">
    <div class="col-label">
        {{ __('customer.image_1_file_id') }}
    </div>
    @include('partials.form-controls.image', ['field' => 'addressFrom.image_1_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
</div>
    <div class="col-sm-3 col-xs-4">
<div class="col-label">
    {{ __('customer.image_2_file_id') }}
</div>
@include('partials.form-controls.image', ['field' => 'addressFrom.image_2_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
</div>
  <div class="col-sm-3 col-xs-4">
    <div class="col-label">
        {{ __('customer.image_3_file_id') }}
    </div>
    @include('partials.form-controls.image', ['field' => 'addressFrom.image_3_file_id', 'file' => null, 'Update' => 'Updatecustomer'])
        </div>
</div>
    </div>
<!-- bo sung ngay 25-12-2019-->
 <div class="panel panel-color panel-gray panel-border" id="package-info">
                   <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;">
                            <i class="fa fa-cube"> </i> {{ __('express.delivery_address') }}
                        </h3>
                   </div>
                <div class="panel-body" style="margin: -10px -20px;padding-top: 20px">
                               <div class="col-sm-6" style="padding-right: 0px">
                                <table class="table table-form" style="border-right: 1px solid #ccc;">
                                    <tr>
                                        <td style="border-right: 1px solid #ccc;" colspan="4">
                                              <h3>{{ __('express.ship_from') }} </h3>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
                                <input type="radio" name="addressFrom.pickup_type" value="1"  ng-model="addressFrom.pickup_type">
                                 {{ __('express.delivery_address_agent') }}
                                    </td>
                                   <td colspan="2">
                                <input type="radio" name="addressFrom.pickup_type" value="2"  ng-model="addressFrom.pickup_type">
                               {{ __('express.delivery_address_customer') }}
                                   </td>
                                    </tr>
                                    <tr>
                                     <td class="col-label">
                                        {{ __('express.ship_date') }} <i class="text-danger">*</i>
                                      </td>
                                      <td>
                                            <div class="input-group date-picker" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') }}:{{ date('Y') + 5 }}">
                                            <input autocomplete="off" type="text" class="form-control" name="addressFrom.shipping_date" ng-model="addressFrom.shipping_date" >
                                               <div class="input-group-btn">
                                                  <button type="button" class="btn btn-white">
                                                    <i class="fa fa-calendar"></i>
                                                  </button>
                                               </div>
                                            </div>
                                      </td>
                                      <td class="col-label">
                                        {{ __('express.attention') }} <i class="text-danger">*</i>
                                      </td>
                                      <td>
                                        <input type="text" class="form-control" name="addressFrom.attention" ng-model="addressFrom.attention">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="col-label">
                                        {{ __('express.instruction') }}
                                      </td>
                                     <td colspan="3">
                                        <textarea class="form-control" name="addressFrom.instruction" ng-model="addressFrom.instruction"></textarea>
                                     </td>
                                    </tr>
                                     <tr>
                                       <td colspan="2">
                                       <input type="checkbox" name="addressFrom.confirm_delivery" value="1"  ng-model="addressFrom.confirm_delivery">
                                       {{ __('express.confirm_delivery') }}
                                     </td>
                                     <td colspan="2">
                                       <input type="checkbox" name="addressFrom.residential" value="1" ng-model="addressFrom.residential" ng-disabled="disable_field">
                                       {{ __('express.residential') }}
                                    </td>
                                     </tr>
                                </table>
                                     </div>

                               <div class="col-sm-6" style="padding-right: 0px">
                                <table class="table table-form">
                                    <tr>
                                        <td style="border-right: 1px solid #ccc;" colspan="4">
                                            <h3>{{ __('express.ship_to') }}</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                   <td class="col-label">
                                        {{ __('express.attention') }} <i class="text-danger">*</i>
                                   </td>
                                   <td colspan="3">
                                    <input type="text" class="form-control" name="addressTo.attention" ng-model="addressTo.attention" >
                                    </td>
                                   </tr>
                                   <tr>
                                     <td class="col-label">
                                     {{ __('express.instruction') }}
                                     </td>
                                     <td colspan="3">
                                      <textarea class="form-control" name="addressTo.instruction" ng-model="addressTo.instruction" ></textarea>
                                     </td>
                                   </tr>
                                   <tr width="100%">
                                       <td  colspan="1">
                                      <input type="checkbox" name="addressTo.residential" value="1" ng-model="addressTo.residential" >
                                      {{ __('express.residential') }}
                                     </td>
                                       <td colspan="3">
                                       <input type="checkbox" name="addressTo.notify_recipient" value="1"  ng-model="addressTo.notify_recipient" >
                                       {{ __('express.notify_recipient') }}
                                     </td>
                                   </tr>
                                </table>
                               </div>
                     </div>
                 </div>
<!-- -->
                <div class="panel panel-color panel-gray panel-border" id="package-info">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-cubes"></i> {{ __('express.boxes') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0">
                        <div class="col-sm-6" style="padding-right: 0px;padding-left: 0px">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;padding-top: 15px">
                                        <h3 >{{ __('express.packaging') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.package_type') }}
                                                </td>
                                                <td width="35%">
                                                    <select name="packages.type" ng-model="packages.type" class="form-control" ng-change="changePackageType()">
                                                        <option ng-repeat="val in packageTypes" ng-value="val.id">@{{ val.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label" width="15%" >
                                                    {{ __('express.package_quantity') }}
                                                </td>
                                                <td width="20%">
                                                    <select name="packages.quantity" id="" class="form-control" ng-model="packages.quantity" ng-change="changeQuantity()" ng-disabled="disable_field">
                                                        <option ng-repeat="val in quantities" ng-value="val">@{{ val }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6" style="padding-right: 0px;">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="padding-top: 15px">
                                        <h3> {{ __('express.reference') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="30%" style="text-align: left">
                                                    {{ __('express.ref_code') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control " name="referenceCode" ng-model="packages.referenceCode" >
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12" style="padding-top: 15px;padding-right: 0px;padding-left: 0px">
                            <table class="table table-form" style="margin-bottom: 0;">
                                <tbody>
                                <tr>
                                    <td>
                                        <h3> {{ __('express.dimension') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td colspan="3">
                                                    <p class="note"><strong> {{ __('express.package_note') }}</strong></p>
                                                </td>
                                            </tr>
                                            <tr class="no-border-top">
                                                <td class="col-label" width="10%" style="    min-width: 100px;">
                                                    {{ __('express.unit') }}
                                                </td>
                                                <td width="15%">
                                                    <select name="packages.dimType" ng-model="packages.dimType" class="form-control" style="min-width: 100px;" ng-change="changeDimType()">
                                                        <option ng-repeat="val in dimTypes" ng-value="val.id" ng-hide="val.id <= 2">@{{ val.name }}</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="table-responsive">
                                <table class="table table-form table-container table-bordered">
                                    <thead>
                                    <tr id="productHead">
                                        <th>#</th>
                                        <th id="lengthHeader">{{ __('express.length') }} (in) <i class="text-danger">*</i></th>
                                        <th id="widthHeader">{{ __('express.width') }} (in) <i class="text-danger">*</i></th>
                                        <th id="heightHeader">{{ __('express.height') }} (in) <i class="text-danger">*</i></th>
                                        <th id="weightHeader">{{ __('label.volume') }} (Lb)</th>
                                        <th id="weightHeader">{{ __('express.weight') }} (Lb)</th>
                                        <th>{{ __('express.insurance_amount') }} CAD($)</th>
                                        <th ng-if="show_field">{{ __('express.freight_class') }}</th>
                                        <th ng-if="show_field">{{ __('express.nmfc_code') }}</th>
                                        <th>{{ __('express.cod_amount') }} ($)</th>
                                        <th>{{ __('label.discount_short') }}(%)</th>
                                        <th class="nmfc_description">{{ __('express.description') }}</th>
                                        <th>{{ __('label.action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in packages.boxes">
                                        <td style="padding-top: 14px;" ng-bind="$index + 1"></td>
                                        <td><input class="form-control" type="text" name="product.length" size="5" ng-model="product.length" ng-change="updateVolume(product)" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.width" size="5" ng-model="product.width" ng-change="updateVolume(product)" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.height" size="5" ng-model="product.height" ng-change="updateVolume(product)" ng-disabled="disable_field"></td>
                                        <td><span ng-bind="product.volume.toFixed(2)" class="form-control"></span></td>
                                        <td><input class="form-control" type="text" name="product.weight" size="5" ng-model="product.weight" ></td>
                                        <td><input class="form-control" type="text" name="product.insuranceAmount" ng-model="product.insuranceAmount" ng-disabled="disable_field"></td>
                                        <td ng-if="show_field"><input class="form-control" type="text" name="product.freightClass" ng-model="product.freightClass" ng-disabled="disable_field" ></td>
                                        <td ng-if="show_field"><input class="form-control" type="text" name="product.nmfcCode" ng-model="product.nmfcCode" ng-disabled="disable_field" ></td>
                                        <td><input class="form-control" type="text" name="product.codValue" size="5" value="0.0" id="codValue" ng-model="product.codValue" ng-disabled="!services.codPaymentType"></td>
                                        <td><input class="form-control" type="text" name="product.per_discount" ng-model="product.per_discount" ng-disabled="disable_field"></td>
                                        <td class="nmfc_description"><input class="form-control" type="text" name="product.description" maxlength="34" size="40" ng-model="product.description"></td>
                                        <td class="col-middle">
                                            <button type="button" class="btn btn-xs btn-danger" ng-click="remove($index)">
                                                <i class="fas fa-window-close"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border" id="service">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;">{{ __('express.service') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0">
                        <table class="table table-form" style="margin-top: 15px;">
                            <tbody>
                            <tr>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.addition_service') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td class="col-label" width="15%">
                                                {{ __('express.saturday_delivery') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="saturdayDelivery" ng-model="services.saturdayDelivery">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.saturday_pickup') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="saturdayPickup" ng-model="services.saturdayPickup">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.hold_pickup') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="holdForPickup" ng-model="services.holdForPickup">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.docs_only') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="docsOnly" ng-model="services.docsOnly">
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="no-border-top">
                                            <td class="col-label">
                                                {{ __('express.dangerous_good') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="dangerousGoods" class="form-control" ng-model="services.dangerousGoods">
                                                    <option value="" selected="selected">None</option>
                                                    <option value="2">Limited Quantity</option>
                                                    <option value="3">500 Kg Exemption</option>
                                                    <option value="4">Fully Regulated</option>
                                                </select>
                                            </td>
                                            <td class="col-label">
                                                {{ __('express.signature_required') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="signatureRequired" class="form-control" ng-model="services.signatureRequired">
                                                    <option value="" selected="selected">No</option>
                                                    <option value="3">Yes</option>
                                                    <option value="4">Adult Signature</option>
                                                </select>
                                            </td>
                                            <td class="col-label" >
                                                {{ __('express.insurance_type') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="insuranceType" class="form-control" ng-model="services.insuranceType">
                                                    <option value="">None</option>
                                                    <option value="1">CWW</option>
                                                    <option value="2" selected="selected">Carrier</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;"> {{ __('express.cod_service') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td class="col-label" width="15%" style="min-width: 100px">
                                                COD
                                            </td>
                                            <td width="35%" style="min-width: 150px">
                                                <select name="codPaymentType" ng-model="services.codPaymentType" class="form-control">
                                                    <option value="" selected="selected">None</option>
                                                    <option value="1">{{ __('express.check') }}</option>
                                                    <option value="2">{{ __('express.certified_check') }}</option>
                                                </select>
                                            </td>

                                            <td class="col-label" width="35%" style="min-width: 100px">
                                                {{ __('express.schedule_pickup') }}
                                            </td>
                                            <td width="15%" style="min-width: 150px">
                                                <select ng-model="pickup.is_schedule" ng-change="changeSchedulePickup();" class="form-control">
                                                    <option value="1" selected="selected">{{ __('label.no') }}</option>
                                                    <option value="2">{{ __('label.yes') }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="pickup.is_schedule == 2" class="panel panel-color panel-gray panel-border" style="">
                    <div class="panel-heading" >
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-shopping-basket"></i> {{ __('express.pickup_info') }}</h3>
                    </div>
                    <div class="panel-body" style="padding:15px ;margin: 0px -30px;">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="10%" class="text-right">{{ __('express.contact_name') }}<i class="text-danger">*</i></td>
                                <td width="20%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.contact_name">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.phone_number') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.phone_number">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.pickup_location') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <select class="form-control" ng-model="pickup.location">
                                        <option ng-repeat="location in pickupLocation" ng-value="location.id">@{{ location.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.pickup_date') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <div class="input-group date-picker" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') }}:{{ date('Y') + 5 }}">
                                        <input autocomplete="off" type="text" class="form-control" name="pickup.date_time" ng-model="pickup.date_time" >
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-white">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">{{ __('express.pickup_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.start_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>
                                    <select class="form-control time-option" ng-model="pickup.start_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                                <td class="text-right">{{ __('express.closing_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.closing_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>:
                                    <select class="form-control time-option" ng-model="pickup.closing_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="outside_canada" class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading" >
                        <h3 class="panel-title" style="font-size: 1.6em;">{{ __('express.custom_invoice') }}</h3>
                    </div>
                    <!-- <div class="panel-body" style="margin: -10px -20px;padding-top: 20px">
                        <div class="col-sm-6" style="padding-right: 0px">
                            <h3>{{ __('express.bill_to') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;">

                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.company') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.company" ng-model="custom.billTo.company">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.name" ng-model="custom.billTo.name">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.address_1" ng-model="custom.billTo.address_1">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="custom.billTo.country_id" class="form-control" ng-model="custom.billTo.country_id" ng-change="getProvincesCustom()">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="custom.billTo.province_id" class="form-control" ng-model="custom.billTo.province_id" ng-change="getCitiesCustom()">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesCustom" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="custom.billTo.city_id" class="form-control" ng-model="custom.billTo.city_id" ng-change="getPostCodeCustom()">
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesCustom" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.postal_code') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.postal_code" ng-model="custom.billTo.postal_code">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <h3>{{ __('express.duties_tax') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>

                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%">
                                                    {{ __('express.dutiable') }}
                                                </td>
                                                <td colspan="1">
                                                    <select class="form-control" ng-model="custom.dutiesTaxes.dutiable">
                                                        <option value="false">{{ __('label.no') }}</option>
                                                        <option value="true">{{ __('label.yes') }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <input type="radio" ng-model="custom.dutiesTaxes.billTo" value="receiver" style="margin-top: 5px;margin-left: 5px">
                                                </td>
                                                <td colspan="3">
                                                    {{ __('express.receiver') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <input type="radio" ng-model="custom.dutiesTaxes.billTo" value="shipper" style="margin-top: 5px;margin-left: 5px">
                                                </td>
                                                <td colspan="3">
                                                    {{ __('express.shipper') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <input type="radio" ng-model="custom.dutiesTaxes.billTo" value="consignee account" style="margin-top: 8px;margin-left: 5px">
                                                </td>
                                                <td>
                                                    <div class="consignee-account-item">
                                                        <span class="consignee-account-item-title">{{ __('express.consignee_account') }}</span>
                                                        <span class="consignee-account-item-content">
                                                            <input type="text" class="form-control" ng-model="custom.dutiesTaxes.consigneeAccount">
                                                        </span>
                                                    </div>
                                                    <div class="consignee-account-item">
                                                        <span class="consignee-account-item-title">{{ __('express.sed_number') }}</span>
                                                        <span class="consignee-account-item-content">
                                                            <input type="text" class="form-control" ng-model="custom.dutiesTaxes.sedNumber">
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> -->
                    <div class="panel-body" style="margin: -10px -20px;padding-top: 20px">
                        <div class="col-sm-12" style="padding-right: 0px">
                            <h3>{{ __('express.custom_product') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td width="15%">
                                        {{ __('express.description') }}<i class="text-danger">*</i>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-model="item.description" required>
                                    </td>
                                    <td width="15%"> {{ __('express.code') }}
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-model="item.code" >
                                    </td>
                                    <td width="15%">
                                        {{ __('express.origin') }}<i class="text-danger">*</i>
                                    </td>
                                    <td>
                                        <select class="form-control" ng-model="item.country_id" required>
                                            <option value="">{{ __('label.select_country') }}</option>
                                            <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        {{ __('express.quantity') }}<i class="text-danger">*</i>
                                    </td>
                                    <td>
                                        <input type="number" min="1" class="form-control" ng-model="item.quantity" required>
                                    </td>
                                    <td width="15%">
                                        {{ __('express.unit') }}<i class="text-danger">*</i>
                                    </td>
                                    <td>
                                        <input type="number" min="0" class="form-control" ng-model="item.unitPrice" required>
                                    </td>
                                    <td width="15%">
                                        <button type="button" class="btn btn-success" ng-click="addItem()" required>
                                            {{ __('label.add_invoice') }}
                                        </button>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="15%">
                                        {{ __('express.delete') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.description') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.code') }}
                                    </th>
                                    <th width="10%">
                                        {{ __('express.origin') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.quantity') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.unit_price') }}
                                    </th>
                                    <th width="10%">
                                        {{ __('express.ext_price') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in custom.items">
                                    <td>
                                        <button type="button" ng-click="removeItem($index)" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                    <td >
                                        @{{ item.description}}
                                    </td>
                                    <td >
                                        @{{ item.code}}
                                    </td>
                                    <td>
                                        @{{ item.country_code}}
                                    </td>
                                    <td >
                                        @{{ item.quantity}}
                                    </td>
                                    <td >
                                        @{{ item.unitPrice}}
                                    </td>
                                    <td >
                                        @{{ item.subPrice}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <span style="float: right">{{ __('express.total') }}: <strong>@{{ custom.total}}</strong> CAD</span>
                        </div>
                    </div>
                    <!-- <div class="panel-body" style="margin: -10px -20px;padding-top: 20px">
                        <div class="col-sm-12" style="padding-right: 0px">
                            <h3>{{ __('express.custom_info') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="col-sm-6" style="padding-right: 0px">
                                            <table class="table table-form">
                                                <tbody>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.company') }}<i class="text-danger">*</i>
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.company">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.name') }}<i class="text-danger">*</i>
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.name">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.broker_name') }}
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.brokerName">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-6" style="padding-right: 0px">
                                            <table class="table table-form">
                                                <tbody>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.tax_id') }}
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.taxId">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.phone') }}<i class="text-danger">*</i>
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.phone">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.receipts_tax_id') }}
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.receiptsTaxId">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> -->
                </div>

                <div ng-if="quotes.length" class="panel panel-color panel-gray panel-border" id="service-list">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;padding: 15px 15px;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-truck"></i> {{ __('express.quote') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0;margin: 0 -15px;">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="15%">
                                    {{ __('express.carrier') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.service') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.est_transitday') }}
                                </th>
                                <th width="10%">
                                    {{ __('express.base_charge') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.orther_surcharge') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.fuel_surcharge') }}
                                </th>
                                <th width="10%">
                                    {{ __('express.total') }}
                                </th>
                                <th>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="quote in quotes.slice(0, 5)">
                                <td>
                                    @{{ quote.carrierName}}
                                </td>
                                <td >
                                    @{{ quote.serviceName}}
                                </td>
                                <td >
                                    @{{ quote.transitDays}}
                                </td>
                                <td>
                                    @{{ quote.baseCharge | number : 2 }} @{{  quote.currency }}
                                </td>
                                <td >
                                    <p ng-if=" quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}} @{{  quote.currency }}
                                    </p>
                                </td>
                                <td >
                                    @{{ quote.fuelSurcharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    @{{ quote.totalCharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    <input type="radio" style="margin-top: 8px;margin-left: 5px" name="serviceId" ng-value="$index+1" ng-click="setService($index)">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="chooseCarrer" class="panel panel-color panel-gray panel-border" style="">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;padding: 15px 15px;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-money"></i> {{ __('express.quote_temp') }}</h3>
                    </div>
                    <div class="panel-body" style="padding:15px ;margin: 0px -30px;">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="text-right" width="15%">{{ __('express.carrier') }}:</td>
                                <td class="text-left" width="19%"><strong ng-bind="quote.carrierName"></strong></td>
                                <td class="text-right" width="15%">{{ __('express.service') }}:</td>
                                <td class="text-left" width="18%"><strong ng-bind="quote.serviceName"></strong></td>
                                <td class="text-right" width="15%">{{ __('express.est_transitday') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.transitDays"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.delivery_date') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.receiver_date"></strong></td>
                                <td class="text-right">{{ __('label.currency_pay') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.currency"></strong></td>
                                <td class="text-right">{{ __('express.base_charge') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.baseCharge | number : 2"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.fuel_surcharge') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.fuelSurcharge | number : 2"></strong></td>
                                <td class="text-right">{{ __('express.surcharge_amount') }}:</td>
                                <td class="text-left">
                                    <p ng-if=" quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}}
                                    </p>
                                </td>
                                <td class="text-right">{{ __('express.total_charge') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.totalCharge | number : 2"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('label.total_discount') }}:</td>
                                <td class="text-left">
                                    <select class="form-control" style="display: block;width: 65px;float: left;margin-right: 10px;">
                                        <option>%</option>
                                    </select>
                                    <input type="text" class="form-control" ng-model="quote.discount_number" style="width:50px;" ng-change="updateFee()">
                                   <!-- <input type="text" class="form-control" ng-model="quote.discount_number" style="width:150px;" ng-if="!isEditDiscount" ng-click="showPopupVip()" readonly>-->
                                </td>
                                <td class="text-right">
                                    {{ __('label.total_amount') }}:
                                    <strong ng-bind="quote.total | number : 2"></strong>
                                </td>
                                <td class="text-left">
                                    {{ __('label.pay') }}:
                                </td>
                                <td class="col-middle text-right">
                                    <input type="text" class="form-control text-right" ng-model="order.total_pay"  ng-change="updatepay()" >
                                    </td>
                                    <td class="col-middle text-right">
                                        {{ __('label.debt') }}:
                                    <strong ng-bind="order.debt.toFixed(2)"></strong>
                                    </td>
                            </tr>
                            </tbody>
                        </table>
                         {!! isset($termagree->translate->detail) ? $termagree->translate->detail : '' !!}
                        <br>
                        <h3><input type="checkbox" name="term_agree" ng-click="setagree(agree)" ng-model="agree">
                            {{ __('label.ok_Terms_Conditions') }} </h3>
                    </div>
                </div>

                <div class="text-right">
                    <a href="javascript:void(0);" class="btn btn-white" ng-click="clearExpress()">
                        <i class="fa fa-arrow-left"></i> {{ __('label.cancel') }}
                    </a>
                    <button type="button" class="btn btn-info" ng-if="bntSearch" ng-click="scanSearchExpress()" ng-disabled="submittedSearch">
                        <i class="fa fa-search"></i> {{ __('label.search_express') }} <i class="fa fa-refresh fa-spin" ng-if="submittedSearch"></i>
                    </button>
                    <button type="button" class="btn btn-info" ng-if="bntConfirm" ng-click="createExpress()" ng-disabled="submittedConfirm">
                        <i class="fa fa-check"></i> {{ __('label.approve') }} <i class="fa fa-refresh fa-spin" ng-if="submittedConfirm"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer')
    <script src="{{ asset('js/admin/app/express-create.js?t=' . File::lastModified(public_path('js/admin/app/express-create.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-create').scope();
            scope.agency = {!! auth()->user()->agency_id !!};
            scope.sourceCounty = 14;
            scope.initQuantity();
            scope.changePackageType();
            scope.init();
        });
    </script>
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/admin/express.css') }}">
@endsection
