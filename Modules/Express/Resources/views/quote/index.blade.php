@extends('layouts.home.user')

@section('content')
    <div class="panel panel-default user">
        <div class="panel-heading">
            {{ __('express.orders_title')}}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-1">
                    <div class="form-group">
                        <a href="{{ route('express.create') }}" class="btn btn-success btn-single">
                            <i class="fa fa-plus-circle"></i> {{ __('label.add_order')}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <form action="{{ route('express.list') }}" method="get" role="form" class="form-inline">
                            <div class="form-group">
                                <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}"
                                       class="form-control" type="text" style="width: 160px;"
                                       placeholder="{{ __('label.from_date') }}">
                            </div>
                            <div class="form-group">
                                <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}"
                                       class="form-control" type="text" style="width: 160px;"
                                       placeholder="{{ __('label.to_date') }}">
                            </div>
                            <div class="form-group">
                                <input name="code" value="{{ request()->query('code')}}" class="form-control"
                                       type="text" style="width: 190px;" placeholder="{{ __('express.order_code') }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-white btn-single">
                                    <i class="fa fa-search"></i> {{ __('label.find') }}
                                </button>
                                <a href="{{ route('express.list') }}" class="btn btn-white btn-single">
                                    {{ __('label.clear') }}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <form id="shipment-package-form">
                 <input class="form-control" id="mySearch" type="text" placeholder= "{{ __('label.filter_list') }}">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="20" class="text-center">#</th>
                            <th width="60">{{ __('label.form_code') }}</th>
                            <th width="60">{{ __('label.service_name') }}</th>
                            <th width="60">{{ __('label.total_weight') }} (Lbs)</th>
                            <th width="60">{{ __('label.amount') }} (CAD)</th>
                            <th width="60" class="text-center">{{ __('label.date') }}</th>
                            <th width="60" class="text-center">{{ __('label.status') }}</th>
                            <th width="60">{{ __('label.action') }}</th>
                        </tr>
                        </thead>
                        <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                            <?php $no = 1; ?>
                            @foreach($orders as $order)
                                <tr>
                                    <td class="text-center">
                                        {{ $no }}
                                    </td>
                                    <td>{{ $order->code }}</td>
                                    <td>{{ isset($order->eshiper_service['carrierName']) ? $order->eshiper_service['carrierName'] : '' }} - {{ isset($order->eshiper_service['serviceName']) ? $order->eshiper_service['serviceName'] : '' }}</td>
                                    <td>{{ number_format($order->total_weight, 2) }}</td>
                                    <td>{{ number_format($order->total_final, 2) }}</td>
                                    <td class="text-center">{{ $order->created_date }}</td>
                                    <td >
                                        {{ __('label.order_status_short') }}: <span class="{{ $order->status_label }}" style="margin-bottom: 10px;">{{ $order->status_name }}</span></br>
                                        {{ __('label.shipping_status_short') }}: <span class="{{ $order->eshipper_status_label }}" style="margin-bottom: 10px;">{{ $order->eshipper_status_name }}</span></br>
                                        {{ __('label.payment_status_short') }}: <span class="{{ $order->payment_status_label }}" style="margin-bottom: 10px;">{{ $order->payment_status_name }}</span></br>
                                    </td>
                                    <td>
                                        <a href="{{ route('express.edit', $order->id) }}" class="btn btn-xs btn-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        @if($order->payment_status > 1)
                                        <button type="button" class="btn btn-xs btn-warning" onclick="claim.open({{$order->id}}, '{{ csrf_token() }}');">
                                            <i class="fa fa-bullhorn"></i>
                                        </button>
                                        @endif
                                        <?php if(!empty($order->eshipper_label)){?>
                                        <a href="{{ route('express.pdf', $order->id) }}" class="btn btn-xs btn-primary" download="label_{{ $order->express_order_id }}.pdf">
                                            <i class="fa fa-file-pdf"></i>
                                        </a>
                                        <?php }?>
                                        <?php if(!empty($order->eshipper_custom_label)){?>
                                        <a href="{{ route('express.custominvoicepdf', $order->id) }}" class="btn btn-xs btn-primary" download="custom_label_{{ $order->express_order_id }}.pdf">
                                            <i class="fa fa-anchor "></i>
                                        </a>
                                        <?php }?>
                                        <?php if(!empty($order->express_order_id)){?>
                                        <button type="button" class="btn btn-xs btn-info" onclick="order.quoteViewHistoryEshiper({{ $order->id }}, '{{ csrf_token() }}')">
                                            <i class="fa fa-truck"></i>
                                        </button>
                                        <?php }?>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">{{ __('label.no_records') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="paginate-single">
                    {{ $orders->appends(request()->query())->links() }}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="/js/home/claim.js"></script>
    <script src="/js/express/order.js"></script>
    <script src="/js/admin/app/extent.js"></script>

    <script>
        $(function () {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
            $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        });
    </script>
@endsection
