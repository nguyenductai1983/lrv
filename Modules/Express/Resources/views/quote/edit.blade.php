@extends('layouts.home.user')

@section('content')
    <div class="panel panel-default" id="edit-express" style="padding: 15px;" ng-app="ExpressApp" ng-controller="ExpressEditController">
        <div class="modal fade custom-width" id="modal-errors">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                    </div>
                    <div class="modal-body text-danger">
                        <div class="row" ng-if="errors.addressFrom.length || errors.addressTo.length || errors.packages.others.length || errors.packages.boxes.length">
                            <div class="col-sm-12">
                                <div ng-if="errors.addressFrom.length">
                                    <h4>{{ __('label.sender') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.addressFrom">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.addressTo.length">
                                    <h4>{{ __('label.receiver') }}</h4>
                                    <ul>
                                        <li ng-repeat="error in errors.addressTo">@{{ error }}</li>
                                    </ul>
                                </div>
                                <div ng-if="errors.packages.others.length || errors.packages.boxes.length">
                                    <div class="row" >
                                        <div class="col-sm-12">
                                            <h4>{{ __('express.package') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="errors.packages.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="errors.packages.boxes.length">
                                                <li ng-repeat="productErrors in errors.packages.boxes" ng-if="productErrors.length">
                                                    {{ __('express.package_product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-if="errors.quotes.length">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <h4>{{ __('express.quote') }}</h4>
                                    <ul>
                                        <li ng-repeat="errorq in errors.quotes">@{{ errorq }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div ng-if="errors.system.length">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <h4>{{ __('express.system_error') }}</h4>
                                    <ul>
                                        <li ng-repeat="errorq in errors.system">@{{ errorq }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">
                            {{ __('label.close') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-heading" style="font-size: 1.6em;">
            {{ __('express.edit') }} {{ $express->code }}
        </div>
        <div class="panel-body">
            <form  id="form-create" name="updateForm" novalidate>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;border-top: none;">
                                        <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.ship_from') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.first_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.first_name" ng-model="addressFrom.first_name" ng-disabled="disable_from">
                                                </td>
                                                <td width="15%" class="col-label">
                                                    {{ __('express.middle_name') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.middle_name" ng-model="addressFrom.middle_name" ng-disabled="disable_from">
                                                </td>
                                                <td width="10%" class="col-label">
                                                    {{ __('express.last_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.last_name" ng-model="addressFrom.last_name" ng-disabled="disable_from">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="addressFrom.address_1" ng-model="addressFrom.address_1" ng-disabled="disable_from">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_2') }}
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="addressFrom.address_2" ng-model="addressFrom.address_2" ng-disabled="disable_from">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="addressFrom.country_id" class="form-control" ng-model="addressFrom.country_id" ng-change="getProvincesFrom()"  ng-disabled="disable_from">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressFrom.province_id" class="form-control" ng-model="addressFrom.province_id" ng-change="getCitiesFrom()"  ng-disabled="disable_from">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesFrom" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressFrom.city_id" class="form-control" ng-model="addressFrom.city_id"  ng-disabled="disable_from">
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesFrom" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('express.postal_code') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="addressFrom.postal_code" ng-model="addressFrom.postal_code" ng-disabled="disable_from">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.telephone" ng-model="addressFrom.telephone"  ng-disabled="disable_from">
                                                </td>
                                                <td class="col-label">{{ __('express.email') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.email" ng-model="addressFrom.email"  ng-disabled="disable_from">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.attention') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressFrom.attention" ng-model="addressFrom.attention">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.ship_date') }}
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="shipping-date" name="addressFrom.shipping_date" ng-model="addressFrom.shipping_date" readonly style="background-color: white;cursor: pointer">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressFrom.confirm_delivery" value="1"  ng-model="addressFrom.confirm_delivery">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.confirm_delivery') }}
                                                </td>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressFrom.residential" value="1" ng-model="addressFrom.residential" ng-disabled="disable_field">
                                                </td>
                                                <td class="col-label" style="text-align: left">
                                                    {{ __('express.residential') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.instruction') }}
                                                </td>
                                                <td colspan="3">
                                                    <textarea class="form-control" name="addressFrom.instruction" ng-model="addressFrom.instruction"></textarea>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-top: none;">
                                        <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.ship_to') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.first_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.first_name" ng-model="addressTo.first_name" ng-disabled="disable_to">
                                                </td>
                                                <td width="15%" class="col-label">
                                                    {{ __('express.middle_name') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.middle_name" ng-model="addressTo.middle_name" ng-disabled="disable_to">
                                                </td>
                                                <td width="10%" class="col-label">
                                                    {{ __('express.last_name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.last_name" ng-model="addressTo.last_name" ng-disabled="disable_to">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="addressTo.address_1" ng-model="addressTo.address_1" ng-disabled="disable_to">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_2') }}
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="addressTo.address_2" ng-model="addressTo.address_2" ng-disabled="disable_to">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="addressTo.country_id" class="form-control" ng-model="addressTo.country_id" ng-change="getProvincesTo()" ng-disabled="disable_to">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressTo.province_id" class="form-control" ng-model="addressTo.province_id" ng-change="getCitiesTo()" ng-disabled="disable_to">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesTo" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="addressTo.city_id" class="form-control" ng-model="addressTo.city_id"  ng-disabled="disable_to" >
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesTo" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">{{ __('express.postal_code') }}<i class="text-danger">*</i></td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.postal_code" ng-model="addressTo.postal_code" ng-disabled="disable_to">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.telephone') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.telephone" ng-model="addressTo.telephone" ng-disabled="disable_to" >
                                                </td>

                                                <td class="col-label">{{ __('express.email') }}</td>
                                                <td>
                                                    <input type="text" class="form-control y" name="addressTo.email" ng-model="addressTo.email" ng-disabled="disable_to">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.attention') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="addressTo.attention" ng-model="addressTo.attention">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressTo.notify_recipient" value="1"  ng-model="addressTo.notify_recipient">
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.notify_recipient') }}
                                                </td>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="addressTo.residential" value="1" ng-model="addressTo.residential">
                                                </td>
                                                <td class="col-label" style="text-align: left">
                                                    {{ __('express.residential') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.instruction') }}
                                                </td>
                                                <td colspan="3">
                                                    <textarea class="form-control" name="addressTo.instruction" ng-model="addressTo.instruction" ></textarea>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="panel panel-color panel-gray panel-border" id="package-info">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-cubes"></i>{{ __('express.boxes') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 0; padding-right: 0">
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc; border-top: none;">
                                        <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.packaging') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="15%">
                                                    {{ __('express.package_type') }}
                                                </td>
                                                <td width="35%">
                                                    <select name="packages.type" ng-model="packages.type" class="form-control" ng-change="changePackageType()">
                                                        <option ng-repeat="val in packageTypes" ng-value="val.id">@{{ val.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label" width="15%" >
                                                    {{ __('express.package_quantity') }}
                                                </td>
                                                <td width="35%">
                                                    <select name="packages.quantity" id="" class="form-control" ng-model="packages.quantity" ng-change="changeQuantity()" ng-disabled="disable_field">
                                                        <option ng-repeat="val in quantities" ng-value="val">@{{ val }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style=" border-top: none;">
                                        <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.reference') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="30%">
                                                    {{ __('express.ref_code') }}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control " name="referenceCode" ng-model="packages.referenceCode" >
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12">
                            <table class="table table-form" style="margin-bottom: 0;">
                                <tbody>
                                <tr>
                                    <td>
                                        <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.dimension') }}</h3>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td colspan="3">
                                                    <p class="note"><strong>{{ __('express.package_note') }}</strong></p>
                                                </td>
                                            </tr>
                                            <tr class="no-border-top">
                                                <td class="col-label" width="10%" style="    min-width: 100px;">
                                                    {{ __('express.choose_unit') }}
                                                </td>
                                                <td width="15%">
                                                    <select name="packages.dimType" ng-model="packages.dimType" class="form-control" style="min-width: 100px;" ng-change="changeDimType()">
                                                        <option ng-repeat="val in dimTypes" ng-value="val.id" ng-hide="val.id == 2">@{{ val.name }}</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="table-responsive">
                                <table class="table table-form table-container table-bordered">
                                    <thead>
                                    <tr id="productHead">
                                        <th>#</th>
                                        <th id="lengthHeader">{{ __('express.length') }} (in) <i class="text-danger">*</i></th>
                                        <th id="widthHeader">{{ __('express.width') }} (in) <i class="text-danger">*</i></th>
                                        <th id="heightHeader">{{ __('express.height') }} (in) <i class="text-danger">*</i></th>
                                        <th id="weightHeader">{{ __('express.weight') }} (lbs)</th>
                                        <th>{{ __('express.insurance_amount') }} CAD($)</th>
                                        <th ng-if="show_field">{{ __('express.freight_class') }}</th>
                                        <th ng-if="show_field">{{ __('express.nmfc_code') }}</th>
                                        <th>{{ __('express.cod_amount') }} ($)</th>
                                        <th class="nmfc_description">{{ __('express.description') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in packages.boxes">
                                        <td style="padding-top: 14px;" ng-bind="$index + 1"></td>
                                        <td><input class="form-control" type="text" name="product.length" size="5" ng-model="product.length" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.width" size="5" ng-model="product.width" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.height" size="5" ng-model="product.height" ng-disabled="disable_field"></td>
                                        <td><input class="form-control" type="text" name="product.weight" size="5" ng-model="product.weight" ></td>
                                        <td><input class="form-control" type="text" name="product.insuranceAmount" ng-model="product.insuranceAmount" ng-disabled="disable_field"></td>
                                        <td ng-if="show_field"><input class="form-control" type="text" name="product.freightClass" ng-model="product.freightClass" ng-disabled="disable_field" ></td>
                                        <td ng-if="show_field"><input class="form-control" type="text" name="product.nmfcCode" ng-model="product.nmfcCode" ng-disabled="disable_field" ></td>
                                        <td><input class="form-control" type="text" name="product.codValue" size="5" value="0.0" id="codValue" ng-model="product.codValue" ng-disabled="!services.codPaymentType"></td>
                                        <td class="nmfc_description"><input class="form-control" type="text" name="product.description" maxlength="34" size="40" ng-model="product.description"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border" id="service">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;">{{ __('express.service') }}</h3>
                    </div>
                    <div class="panel-body" style="padding-left: 15px; padding-right: 15px">
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.addition_service') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td class="col-label" width="15%">
                                                {{ __('express.saturday_delivery') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="saturdayDelivery" ng-model="services.saturdayDelivery">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.saturday_pickup') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="saturdayPickup" ng-model="services.saturdayPickup">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.hold_pickup') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="holdForPickup" ng-model="services.holdForPickup">
                                            </td>
                                            <td class="col-label" width="15%">
                                                {{ __('express.docs_only') }}
                                            </td>
                                            <td width="10%">
                                                <input type="checkbox" style="margin-top: 8px;margin-left: 5px" name="docsOnly" ng-model="services.docsOnly">
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="no-border-top">
                                            <td class="col-label">
                                                {{ __('express.dangerous_good') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="dangerousGoods" class="form-control" ng-model="services.dangerousGoods">
                                                    <option value="" selected="selected">None</option>
                                                    <option value="2">Limited Quantity</option>
                                                    <option value="3">500 Kg Exemption</option>
                                                    <option value="4">Fully Regulated</option>
                                                </select>
                                            </td>
                                            <td class="col-label">
                                                {{ __('express.signature_required') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="signatureRequired" class="form-control" ng-model="services.signatureRequired">
                                                    <option value="" selected="selected">No</option>
                                                    <option value="3">Yes</option>
                                                    <option value="4">Adult Signature</option>
                                                </select>
                                            </td>
                                            <td class="col-label" >
                                                {{ __('express.insurance_type') }}
                                            </td>
                                            <td  colspan="2">
                                                <select name="insuranceType" class="form-control" ng-model="services.insuranceType">
                                                    <option value="">None</option>
                                                    <option value="1">CWW</option>
                                                    <option value="2" selected="selected">Carrier</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('express.cod_service') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td class="col-label" width="15%" style="min-width: 100px">
                                                COD
                                            </td>
                                            <td width="35%" style="min-width: 150px">
                                                <select name="codPaymentType" ng-model="services.codPaymentType" class="form-control">
                                                    <option value="" selected="selected">None</option>
                                                    <option value="1">{{ __('express.check') }}</option>
                                                    <option value="2">{{ __('express.certified_check') }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label" width="35%" style="min-width: 100px">
                                                {{ __('express.schedule_pickup') }}
                                            </td>
                                            <td width="15%" style="min-width: 150px">
                                                <select ng-model="pickup.is_schedule" ng-change="changeSchedulePickup();" class="form-control">
                                                    <option value="1" selected="selected">{{ __('label.no') }}</option>
                                                    <option value="2">{{ __('label.yes') }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div ng-if="pickup.is_schedule == 2" class="panel panel-color panel-gray panel-border" style="">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;padding: 15px 15px;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-shopping-basket"></i> {{ __('express.pickup_info') }}</h3>
                    </div>
                    <div class="panel-body" style="padding:15px ;margin: 0px -30px;">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="10%" class="text-right">{{ __('express.contact_name') }}<i class="text-danger">*</i></td>
                                <td width="20%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.contact_name">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.phone_number') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.phone_number">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.pickup_location') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <select class="form-control" ng-model="pickup.location">
                                        <option ng-repeat="location in pickupLocation" ng-value="location.id">@{{ location.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.pickup_date') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <div class="input-group date-picker" data-change-year="true" data-change-month="true" data-year-range="{{ date('Y') }}:{{ date('Y') + 5 }}">
                                        <input autocomplete="off" type="text" class="form-control" name="pickup.date_time" ng-model="pickup.date_time" >
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-white">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">{{ __('express.pickup_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.start_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>
                                    <select class="form-control time-option" ng-model="pickup.start_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                                <td class="text-right">{{ __('express.closing_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.closing_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>:
                                    <select class="form-control time-option" ng-model="pickup.closing_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="outside_canada == 2" class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;">{{ __('express.custom_invoice') }}</h3>
                    </div>
                    <!-- <div class="panel-body" style="padding-top: 20px">
                        <div class="col-sm-6" style="padding-right: 0px">
                            <h3>{{ __('express.bill_to') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td style="border-right: 1px solid #ccc;">

                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.company') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.company" ng-model="custom.billTo.company">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.name') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.name" ng-model="custom.billTo.name">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label" width="20%">
                                                    {{ __('express.address_1') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.address_1" ng-model="custom.billTo.address_1">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.country_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td width="30%">
                                                    <select name="custom.billTo.country_id" class="form-control" ng-model="custom.billTo.country_id" ng-change="getProvincesCustom()">
                                                        <option value="">{{ __('label.select_country') }}</option>
                                                        <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                    </select>
                                                </td>
                                                <td width="20%" class="col-label">
                                                    {{ __('express.province_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="custom.billTo.province_id" class="form-control" ng-model="custom.billTo.province_id" ng-change="getCitiesCustom()">
                                                        <option value="">{{ __('label.select_province') }}</option>
                                                        <option ng-repeat="province in provincesCustom" ng-value="province.id">@{{ province.name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-label">
                                                    {{ __('express.city_id') }}<i class="text-danger">*</i>
                                                </td>
                                                <td>
                                                    <select name="custom.billTo.city_id" class="form-control" ng-model="custom.billTo.city_id" ng-change="getPostCodeCustom()">
                                                        <option value="">{{ __('label.select_city') }}</option>
                                                        <option ng-repeat="city in citiesCustom" ng-value="city.id">@{{ city.name }}</option>
                                                    </select>
                                                </td>
                                                <td class="col-label">
                                                    {{ __('express.postal_code') }}<i class="text-danger">*</i>
                                                </td>
                                                <td colspan="5">
                                                    <input type="text" class="form-control" name="custom.billTo.postal_code" ng-model="custom.billTo.postal_code">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <h3>{{ __('express.duties_tax') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>

                                        <table class="table table-form">
                                            <tbody>
                                            <tr>
                                                <td width="20%">
                                                    {{ __('express.dutiable') }}
                                                </td>
                                                <td colspan="1">
                                                    <select class="form-control" ng-model="custom.dutiesTaxes.dutiable">
                                                        <option value="false">{{ __('label.no') }}</option>
                                                        <option value="true">{{ __('label.yes') }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <input type="radio" ng-model="custom.dutiesTaxes.billTo" value="receiver" style="margin-top: 5px;margin-left: 5px">
                                                </td>
                                                <td colspan="3">
                                                    {{ __('express.receiver') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <input type="radio" ng-model="custom.dutiesTaxes.billTo" value="shipper" style="margin-top: 5px;margin-left: 5px">
                                                </td>
                                                <td colspan="3">
                                                    {{ __('express.shipper') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <input type="radio" ng-model="custom.dutiesTaxes.billTo" value="consignee account" style="margin-top: 8px;margin-left: 5px">
                                                </td>
                                                <td>
                                                    <div class="consignee-account-item">
                                                        <span class="consignee-account-item-title">{{ __('express.consignee_account') }}</span>
                                                        <span class="consignee-account-item-content">
                                                            <input type="text" class="form-control" ng-model="custom.dutiesTaxes.consigneeAccount">
                                                        </span>
                                                    </div>
                                                    <div class="consignee-account-item">
                                                        <span class="consignee-account-item-title">{{ __('express.sed_number') }}</span>
                                                        <span class="consignee-account-item-content">
                                                            <input type="text" class="form-control" ng-model="custom.dutiesTaxes.sedNumber">
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> -->
                    <div class="panel-body" style="padding-top: 20px">
                        <div class="col-sm-12" style="padding-right: 0px">
                            <h3>{{ __('express.custom_product') }}</h3>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="15%">
                                        {{ __('express.desctiption') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.code') }}
                                    </th>
                                    <th width="10%">
                                        {{ __('express.origin') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.quantity') }}
                                    </th>
                                    <th width="15%">
                                        {{ __('express.unit_price') }}
                                    </th>
                                    <th width="10%">
                                        {{ __('express.ext_price') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in custom.items">
                                    <td >
                                        @{{ item.description}}
                                    </td>
                                    <td >
                                        @{{ item.code}}
                                    </td>
                                    <td>
                                        @{{ item.country_code}}
                                    </td>
                                    <td >
                                        @{{ item.quantity}}
                                    </td>
                                    <td >
                                        @{{ item.unitPrice}}
                                    </td>
                                    <td >
                                        @{{ item.subPrice}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <span style="float: right">{{ __('express.total') }}: <strong>@{{ custom.total}}</strong> CAD</span>
                        </div>
                    </div>
                    <!-- <div class="panel-body" style="padding-top: 20px">
                        <div class="col-sm-12" style="padding-right: 0px">
                            <h3>{{ __('express.custom_info') }}</h3>
                            <table class="table table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="col-sm-6" style="padding-right: 0px">
                                            <table class="table table-form">
                                                <tbody>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.company') }}<i class="text-danger">*</i>
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.company">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.name') }}<i class="text-danger">*</i>
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.name">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.broker_name') }}
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.brokerName">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-6" style="padding-right: 0px">
                                            <table class="table table-form">
                                                <tbody>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.tax_id') }}
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.taxId">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.phone') }}<i class="text-danger">*</i>
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.phone">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-label" width="20%">
                                                        {{ __('express.receipts_tax_id') }}
                                                    </td>
                                                    <td colspan="5">
                                                        <input type="text" class="form-control" ng-model="custom.receiptsTaxId">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> -->
                </div>

                <div ng-if="quotes.length" class="panel panel-color panel-gray panel-border" id="service-list">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-truck"></i> {{ __('express.quote') }}</h3>
                    </div>
                    <div class="panel-body" style="padding: 15px;">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="15%">
                                    {{ __('express.carrier') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.service') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.est_transitday') }}
                                </th>
                                <th width="10%">
                                    {{ __('express.base_charge') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.orther_surcharge') }}
                                </th>
                                <th width="15%">
                                    {{ __('express.fuel_surcharge') }}
                                </th>
                                <th width="10%">
                                    {{ __('express.total') }}
                                </th>
                                <th>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="quote in quotes.slice(0, 5)">
                                <td>
                                    @{{ quote.carrierName}}
                                </td>
                                <td >
                                    @{{ quote.serviceName}}
                                </td>
                                <td >
                                    @{{ quote.transitDays}}
                                </td>
                                <td>
                                    @{{ quote.oldBaseCharge ? quote.oldBaseCharge : quote.baseCharge | number : 2 }} @{{  quote.currency }}
                                </td>
                                <td >
                                    <p ng-if=" quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}} @{{  quote.currency }}
                                    </p>
                                </td>
                                <td >
                                    @{{ quote.fuelSurcharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    @{{ quote.oldTotalCharge ? quote.oldTotalCharge : quote.totalCharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    <input type="radio" style="margin-top: 8px;margin-left: 5px" name="serviceId" ng-value="$index+1" ng-checked="@{{quote.serviceId == selectQuote.serviceId}}">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading" style="border-bottom: 1px solid #cab5b547;">
                        <h3 class="panel-title" style="font-size: 1.6em;"><i class="fa fa-money"></i> {{ __('express.quote_temp') }}</h3>
                    </div>
                    <div class="panel-body" style="padding:15px">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="text-right" width="15%"></td>
                                <td class="text-left" width="19%"></td>
                                <td class="col-middle text-right" width="15%">{{ __('label.coupon_code') }}:</td>
                                <td class="text-left">
                                    <strong ng-bind="quote.coupon_code"></strong>
                                </td>
                                <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                <td class="text-left">
                                    <strong ng-bind="quote.coupon_amount.toFixed(2)"></strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right" width="15%">{{ __('express.carrier') }}:</td>
                                <td class="text-left" width="19%"><strong ng-bind="quote.carrierName"></strong></td>
                                <td class="text-right" width="15%">{{ __('express.service') }}:</td>
                                <td class="text-left" width="18%"><strong ng-bind="quote.serviceName"></strong></td>
                                <td class="text-right" width="15%">{{ __('express.est_transitday') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.transitDays"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.delivery_date') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.receiver_date"></strong></td>
                                <td class="text-right">{{ __('label.currency_pay') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.currency"></strong></td>
                                <td class="text-right">{{ __('express.base_charge') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.baseCharge | number : 2"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.fuel_surcharge') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.fuelSurcharge | number : 2"></strong></td>
                                <td class="text-right">{{ __('express.surcharge_amount') }}:</td>
                                <td class="text-left">
                                    <p ng-if="quote.surcharge.length" ng-repeat="surcharge in quote.surcharge" style="color:black;">
                                        @{{ surcharge.name }} : @{{  surcharge.amount | number : 2}}
                                    </p>
                                </td>
                                <td class="text-right">{{ __('express.total_charge') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.totalCharge | number : 2"></strong></td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('label.discount_short') }} %:</td>
                                <td>
                                     <strong ng-bind="quote.per_discount | number : 2"></strong>
                                </td>
                                <td class="text-right">{{ __('label.total_discount') }}:</td>
                                <td ><strong ng-bind="quote.total_discount | number : 2"></strong></td>
                                <td class="text-right">{{ __('label.total_amount') }}:</td>
                                <td class="text-left"><strong ng-bind="quote.total | number : 2"></strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-right">
                    <a href="{{ route('express.list') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer')
    <script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
    <script src="{{ asset('js/express/express-edit.js?t=' . File::lastModified(public_path('js/express/express-edit.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-create').scope();
            scope.init();
            scope.initQuantity();
            scope.id = {!! request()->route('id') !!};
            scope.customer_id = '{{ auth()->id() }}';
            scope.getExpress();
        });
        $(function () {
            $("#shipping-date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        });
    </script>
@endsection
