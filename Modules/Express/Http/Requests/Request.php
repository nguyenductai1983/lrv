<?php
/**
 * Created by IntelliJ IDEA.
 * User: DatLK
 * Date: 4/23/2018
 * Time: 5:00 PM
 */

namespace Modules\Express\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    //
}