<?php

namespace Modules\Express\Http\Requests;


class PaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'order.payment_method' => 'required|exists:tbl_payment_method,code',
            'order.date_pay' => 'required|string',
            'order.last_pay_amount' => 'required|numeric|min:0'
        ];
    }

    public function messages() {
        return [];
    }
    public function attributes()
    {
        return[
            'order.payment_method' =>  __('label.payment_method'),
            'order.date_pay' => __('label.date_pay'),
            'order.last_pay_amount' => __('label.last_pay_amount'),
        ];
    }
}
