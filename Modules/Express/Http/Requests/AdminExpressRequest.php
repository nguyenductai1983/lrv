<?php

namespace Modules\Express\Http\Requests;


class AdminExpressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'order.customer_id' => 'nullable|exists:customers,id',
            'order.payment_method' => 'nullable|exists:tbl_payment_method,code',

            // 'is_custom' => 'required|numeric|in:1,2',

            // 'custom' => 'nullable|required_if:is_custom,2|array',
            // 'custom.company' => 'nullable|required_if:is_custom,2|string',
            // 'custom.name' => 'nullable|required_if:is_custom,2|string',
            // 'custom.brokerName' => 'nullable|string',
            // 'custom.taxId' => 'nullable|string',
            // 'custom.phone' => 'nullable|required_if:is_custom,2|string',
            // 'custom.receiptsTaxId' => 'nullable|string',

            // 'custom.billTo.company' => 'nullable|required_if:is_custom,2|string',
            // 'custom.billTo.name' => 'nullable|required_if:is_custom,2|string',
            // 'custom.billTo.address_1' => 'nullable|required_if:is_custom,2|string',
            // 'custom.billTo.country_id' => 'nullable|required_if:is_custom,2|exists:countries,id',
            // 'custom.billTo.city_id' => 'nullable|required_if:is_custom,2|exists:cities,id',
            // 'custom.billTo.province_id' => 'nullable|required_if:is_custom,2|exists:provinces,id',
            // 'custom.billTo.postal_code' => 'nullable|required_if:is_custom,2|string|max:20',

            // 'custom.dutiesTaxes.dutiable' => 'nullable|required_if:is_custom,2|string|in:false,true',
            // 'custom.dutiesTaxes.billTo' => 'nullable|required_if:is_custom,2|string|in:receiver,shipper,consignee account',
            // 'custom.dutiesTaxes.consigneeAccount' => 'nullable|required_if:custom.dutiesTaxes.billTo,consignee account|string',
            // 'custom.dutiesTaxes.sedNumber' => 'nullable|string',

           'custom.items' => 'nullable|required_if:outside_canada,true|array',
           'custom.items.*.description' => 'nullable|required_if:outside_canada,true|string',
           'custom.items.*.code' => 'nullable|string',
           'custom.items.*.country_id' => 'nullable|required_if:outside_canada,true|exists:countries,id',
           'custom.items.*.quantity' => 'nullable|required_if:outside_canada,true|numeric|min:0',
           'custom.items.*.unitPrice' => 'nullable|required_if:outside_canada,true|numeric|min:0',

            'pickup' => 'required|array',
            'pickup.is_schedule' => 'required|string|in:1,2',
            'pickup.contact_name' => 'required_if:pickup.is_schedule,2',
            'pickup.phone_number' => 'required_if:pickup.is_schedule,2',
            'pickup.location' => 'required_if:pickup.is_schedule,2',
            'pickup.date_time' => 'required_if:pickup.is_schedule,2',
            'pickup.start_hour_time' => 'required_if:pickup.is_schedule,2',
            'pickup.start_minute_time' => 'required_if:pickup.is_schedule,2',
            'pickup.closing_hour_time' => 'required_if:pickup.is_schedule,2',
            'pickup.closing_minute_time' => 'required_if:pickup.is_schedule,2',

            'addressFrom' => 'required|array',
            'addressFrom.first_name' => 'required|string|max:100',
            'addressFrom.middle_name' => 'nullable|string|max:100',
            'addressFrom.last_name' => 'required|string|max:100',
            'addressFrom.address_1' => 'required|string|max:35',
            'addressFrom.address_2' => 'nullable|string|max:35',
            'addressFrom.country_id' => 'required|exists:countries,id',
            'addressFrom.city_id' => 'required|exists:cities,id',
            'addressFrom.province_id' => 'required|exists:provinces,id',
            'addressFrom.postal_code' => 'required|string|max:20',
            'addressFrom.telephone' => 'required|string',
            'addressFrom.cellphone' => 'nullable|string',
            'addressFrom.email' => 'nullable|email',
            'addressFrom.attention' => 'required|string|max:100',
            'addressFrom.instruction' => 'nullable|string|max:500',
            'addressFrom.confirm_delivery' => 'nullable|boolean',
            'addressFrom.residential' => 'nullable|boolean',
            'addressFrom.shipping_date' => 'required|date_format:' . config('app.date_format'),

            'addressTo' => 'required|array',
            'addressTo.id' => 'nullable|exists:tbl_address,id',
            'addressTo.first_name' => 'required|string|max:100',
            'addressTo.middle_name' => 'nullable|string|max:100',
            'addressTo.last_name' => 'required|string|max:100',
            'addressTo.address_1' => 'required|string|max:35',
            'addressTo.address_2' => 'nullable|required_with:services.codPaymentType|string|max:35',
            'addressTo.country_id' => 'required|exists:countries,id',
            'addressTo.city_id' => 'required|exists:cities,id',
            'addressTo.province_id' => 'required|exists:provinces,id',
            'addressTo.postal_code' => 'required|string|max:20',
            'addressTo.telephone' => 'required|string|max:20',
            'addressTo.cellphone' => 'nullable|string|max:20',
            'addressTo.email' => 'nullable|string|max:100',
            'addressTo.attention' => 'required|string|max:100',
            'addressTo.instruction' => 'nullable|string|max:500',
            'addressTo.notify_recipient' => 'nullable|boolean',
            'addressTo.residential' => 'nullable|boolean',

            'packages' => 'required|array',
            'packages.type' => 'required|numeric|min:1|max:6',
            'packages.quantity' => 'required|numeric|min:1|max:50',
            'packages.referenceCode' => 'nullable|string',
            'packages.dimType' => 'required|numeric|in:1,3',
            'packages.boxes' => 'required|array',
            'packages.boxes.*.length' => 'required|numeric',
            'packages.boxes.*.width' => 'required|numeric',
            'packages.boxes.*.height' => 'required|numeric',
            'packages.boxes.*.weight' => 'required|numeric',
            'packages.boxes.*.codValue' => 'required|numeric|min:0',
            'packages.boxes.*.description' => 'nullable|string|max:255',
            'packages.boxes.*.insuranceAmount' => 'nullable|numeric|min:0',
            'packages.boxes.*.freightClass' => 'nullable|required_if:packages.type,4|numeric|min:50',
            'packages.boxes.*.nmfcCode' => 'nullable|required_if:packages.type,4|string|max:100',
            'quote' => 'nullable|array',

            'services' => 'nullable|array',
            'services.saturdayDelivery' => 'nullable|boolean',
            'services.saturdayPickup' => 'nullable|boolean',
            'services.holdForPickup' => 'nullable|boolean',
            'services.docsOnly' => 'nullable|boolean',
            'services.dangerousGoods' => 'nullable|numeric',
            'services.signatureRequired' => 'nullable|numeric',
            'services.insuranceType' => 'nullable|numeric',
            'services.codPaymentType' => 'nullable|numeric',
        ];
    }

    public function messages() {
        return [];
    }
    public function attributes()
    {
        return[
            'customer_id' => __('customer.id'),

            // 'is_custom' => __('express.is_custom'),

            // 'custom' => __('express.custom'),
            // 'custom.company' => __('express.custom_company'),
            // 'custom.name' => __('express.custom_name'),
            // 'custom.brokerName' => __('express.custom_brokerName'),
            // 'custom.taxId' => __('express.custom_taxId'),
            // 'custom.phone' => __('express.custom_phone'),
            // 'custom.receiptsTaxId' => __('express.custom_receiptsTaxId'),

            // 'custom.billTo.company' => __('express.custom_billTo_company'),
            // 'custom.billTo.name' => __('express.custom_billTo_name'),
            // 'custom.billTo.address_1' => __('express.custom_billTo_address_1'),
            // 'custom.billTo.country_id' => __('express.custom_billTo_country_id'),
            // 'custom.billTo.city_id' => __('express.custom_billTo_city_id'),
            // 'custom.billTo.province_id' => __('express.custom_billTo_province_id'),
            // 'custom.billTo.postal_code' => __('express.custom_billTo_postal_code'),

            // 'custom.dutiesTaxes.dutiable' => __('express.custom_dutiesTaxes_dutiable'),
            // 'custom.dutiesTaxes.billTo' => __('express.custom_dutiesTaxes_billTo'),
            // 'custom.dutiesTaxes.consigneeAccount' => __('express.custom_dutiesTaxes_consigneeAccount'),
            // 'custom.dutiesTaxes.sedNumber' => __('express.custom_dutiesTaxes_sedNumber'),

           'custom.items' => __('express.custom_items'),
           'custom.items.*.description' => __('express.custom_items_description'),
           'custom.items.*.code' => __('express.custom_items_code'),
           'custom.items.*.country_id' => __('express.custom_items_country_id'),
           'custom.items.*.quantity' => __('express.custom_items_quantity'),
           'custom.items.*.unitPrice' => __('express.custom_items_unitPrice'),

            'addressFrom' => __('label.address'),
            'addressFrom.first_name' =>  __('express.first_name'),
            'addressFrom.middle_name' =>  __('express.middle_name'),
            'addressFrom.last_name' =>  __('express.last_name'),
            'addressFrom.address_1' => __('customer.address_1'),
            'addressFrom.address_2' => __('customer.address_2'),
            'addressFrom.country_id' => __('customer.country_id'),
            'addressFrom.city_id' => __('customer.city_id'),
            'addressFrom.province_id' => __('customer.province_id'),
            'addressFrom.postal_code' => __('customer.postal_code'),
            'addressFrom.telephone' => __('customer.telephone'),
            'addressFrom.cellphone' => __('customer.cellphone'),
            'addressFrom.email' => __('customer.email'),
            'addressFrom.attention' =>  __('label.attention'),
            'addressFrom.instruction' =>  __('label.instruction'),

            'pickup' => __('express.pickup_info'),
            'pickup.contact_name' =>  __('express.contact_name'),
            'pickup.phone_number' => __('express.phone_number'),
            'pickup.location' => __('express.pickup_location'),
            'pickup.date_time' => __('express.pickup_date'),
            'pickup.start_hour_time' => __('express.pickup_hour'),
            'pickup.start_minute_time' => __('express.pickup_minute'),
            'pickup.closing_hour_time' => __('express.closing_hour'),
            'pickup.closing_minute_time' => __('express.closing_minute'),

            'addressTo' => __('label.address'),
            'addressTo.first_name' =>  __('express.first_name'),
            'addressTo.middle_name' =>  __('express.middle_name'),
            'addressTo.last_name' =>  __('express.last_name'),
            'addressTo.address_1' => __('customer.address_1'),
            'addressTo.address_2' => __('customer.address_2'),
            'addressTo.country_id' => __('customer.country_id'),
            'addressTo.city_id' => __('customer.city_id'),
            'addressTo.province_id' => __('customer.province_id'),
            'addressTo.postal_code' => __('customer.postal_code'),
            'addressTo.telephone' => __('customer.telephone'),
            'addressTo.cellphone' => __('customer.cellphone'),
            'addressTo.email' => __('customer.email'),
            'addressTo.attention' =>  __('label.attention'),
            'addressTo.instruction' =>  __('label.instruction'),

            'packages' => __('label.packages'),
            'packages.type' => __('packages.type'),
            'packages.quantity' =>  __('packages.quantity'),
            'packages.referenceCode' =>  __('packages.referenceCode'),
            'packages.dimType' =>  __('packages.dimType'),
            'packages.boxes' => __('packages.boxes'),
            'packages.boxes.*.length' =>  __('packages.boxes_length'),
            'packages.boxes.*.width' =>  __('packages.boxes_width'),
            'packages.boxes.*.height' =>  __('packages.boxes_height'),
            'packages.boxes.*.weight' =>  __('packages.boxes_weight'),
            'packages.boxes.*.codValue' =>  __('packages.boxes_cod_value'),
            'packages.boxes.*.description' =>  __('packages.description'),
            'packages.boxes.*.insuranceAmount' => __('packages.insurance_amount'),
            'packages.boxes.*.freightClass' => __('packages.freight_class'),
            'packages.boxes.*.nmfcCode' => __('packages.nmfc_code'),
            'quote' => __('label.quote'),
            'services' => __('label.services'),
            'services.codPaymentType' => __('express.cod_service'),
        ];
    }
}
