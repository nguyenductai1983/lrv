<?php

namespace Modules\Express\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Exception;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Modules\Express\Http\Requests\ExpressRequest;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Components\RealtimeUtil;
use App\Services\EshiperService;
// use App\Services\PointService;
use App\Services\CampaignService;
use App\Enum\EshiperEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
// use App\Enum\PointEnum;
use App\Address;
use App\City;
use App\Currency;
use App\Order;
use App\OrderItem;
use App\Province;
use App\Country;
use App\Customer;
use App\Page;
use App\Notification;
use App\User;
use App\CampaignLog;
use App\Campaign;

class ExpressController extends Controller {

    protected $realtimeUtil;

    public function __construct(RealtimeUtil $realtimeUtil) {
        $this->realtimeUtil = $realtimeUtil;
        $this->data = [
            'menu' => '3.0'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::EXPRESS);
        $query->where('customer_id', '=', auth()->id());
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('express::quote.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $this->data['customer'] = auth()->user();
        $slug=68;
        if (!$page = Page::with('translate')->where('id', '=', $slug)->first()) {
            $page='';
        }
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['termagree'] = $page;
        return view('express::quote.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ExpressRequest $request) {
        $param = $request->all();
        $addressFrom = $param['addressFrom'];
        $addressTo = $param['addressTo'];
        $eshiperService = $param['quote'];
        $eshiperServiceList = $param['quotes'];
        $packages = $param['packages'];
        $services = $param['services'];
        $code_custom='VAD';
        try {
            DB::beginTransaction();
            $customer = auth()->user();
            $this->updateInfoCustmer($customer, $addressFrom);
            if (isset($addressTo) && !empty($addressTo)) {
                if (isset($addressTo['id']) && !empty($addressTo['id'])) {
                    $receiver = Address::where('id', $addressTo['id'])->first();
                    if (empty($receiver)) {
                        $receiver = new Address();
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]))
                                $receiver->$property = $addressTo[$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    } else {
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]))
                                $receiver->$property = $addressTo[$property];
                        }
                        $receiver->update();
                    }
                } else {
                    $receiver = new Address();
                    $properties = array_keys($addressTo);
                    foreach ($properties as $property) {
                        if (isset($addressTo[$property]) && !empty($addressTo[$property]))
                            $receiver->$property = $addressTo[$property];
                    }
                    $receiver->customer_id = $customer->id;
                    $receiver->save();
                }
            }
            $order = new Order();
            $order->customer_id = $customer->id;
            $order->sender_first_name = trim($addressFrom['first_name']);
            $order->sender_middle_name = trim($addressFrom['middle_name']);
            $order->sender_last_name = trim($addressFrom['last_name']);
            $order->sender_email = $addressFrom['email'];
            $order->sender_phone = $addressFrom['telephone'];
            $order->sender_address = $addressFrom['address_1'];
            $order->sender_country_id = $addressFrom['country_id'];
            $order->sender_province_id = $addressFrom['province_id'];
            $order->sender_city_id = $addressFrom['city_id'];
            $order->sender_post_code = $addressFrom['postal_code'];
            $order->sender_attention = $addressFrom['attention'];
            $order->sender_instruction = $addressFrom['instruction'];
            $order->sender_address_2 = $addressFrom['address_2'];
             $order->shipping_date = !empty($addressFrom['shipping_date']) ? ConvertsUtil::dateFormat($addressFrom['shipping_date'], config('app.date_format')) : null;
            $order->sender_confirm_delivery = isset($addressFrom['confirm_delivery']) && $addressFrom['confirm_delivery'] ? 1 : 0;
            $order->sender_residential = isset($addressFrom['residential']) && $addressFrom['residential'] ? 1 : 0;

            $order->receive_first_name = trim($addressTo['first_name']);
            $order->receiver_middle_name = trim($addressTo['middle_name']);
            $order->receive_last_name = trim($addressTo['last_name']);
            $order->receiver_email = $addressTo['email'];
            $order->receiver_phone = $addressTo['telephone'];
            $order->receiver_address = $addressTo['address_1'];
            $order->receiver_country_id = $addressTo['country_id'];
            $order->receiver_province_id = $addressTo['province_id'];
            $order->receiver_city_id = $addressTo['city_id'];
            $order->receiver_post_code = $addressTo['postal_code'];
            $order->receiver_attention = $addressTo['attention'];
            $order->receiver_instruction = $addressTo['instruction'];
            $order->receiver_address_2 = $addressTo['address_2'];
            $order->receiver_notify_recipient = isset($addressTo['notify_recipient']) && $addressTo['notify_recipient'] ? 1 : 0;
            $order->receiver_residential = isset($addressTo['residential']) && $addressTo['residential'] ? 1 : 0;
            if(isset($eshiperService['currency']) && !empty($eshiperService['currency'])){
                $order->currency_id = Currency::where('code', $eshiperService['currency'])->first()->id;
            }
            $order->dimension_unit_id = $packages['dimType'];
            $order->weight_unit_id = $packages['dimType'];
            $order->package_type = $packages['type'];
            $order->package_quantity = $packages['quantity'];
            $order->ref_code = $packages['referenceCode'];
            $order->addtional_service = $services;
            $order->eshiper_service = $eshiperService;
            $order->total_discount=$eshiperService['total_discount'];
            $order->per_discount=isset($eshiperService['per_discount'])?$eshiperService['per_discount'] :0;
            $order->eshiper_service_list = $eshiperServiceList;
            $order->type = OrderTypeEnum::EXPRESS;
            $order->pickup = json_encode($param['pickup']);
            $order->eshipper_custom = json_encode($param['custom']);
            $order->save();
            $receiverCountry = Country::find($addressTo['country_id']);
            $receiverCountryName = isset($receiverCountry->code) ? $receiverCountry->code : '';
            $order->code = $order->id .'E' . $receiverCountryName;

            $total_weight = 0;
            foreach ($packages['boxes'] as $key => $product) {
                $order_item = new OrderItem();
                $order_item->order_id = $order->id;
                $order_item->name = 'EXPRESS_' . $order->id . '-PRODUCT_' . ($key + 1);
                $order_item->description = $product['description'];
                $order_item->weight = $product['weight'];
                $order_item->length = $product['length'];
                $order_item->width = $product['width'];
                $order_item->height = $product['height'];
                $volume = ($order_item->width * $order_item->length * $order_item->height) / config('express.volumeweight');
                if($volume > $order_item->weight){
                    $order_item->weight = $volume;
                }
                $total_weight += $order_item->weight;

                $order_item->freight_class = $product['freightClass'];
                $order_item->nmfc_code = $product['nmfcCode'];
                $order_item->sub_total_insurrance_fee = $product['insuranceAmount'];
                $order_item->cod_fee = $product['codValue'];
                $order_item->save();
            }
            $order->total_weight = $total_weight;
            // Kiểm tra mã coupon
            if(!empty($eshiperService['coupon_code'])){
                $amount = CampaignService::getAmountCoupon($eshiperService['coupon_code'], $order->customer_id, $order->total_weight);
                if($amount > 0){
                    $order->coupon_code = $eshiperService['coupon_code'];
                    $order->coupon_amount = $amount;
                    $order->coupon_time = date("Y-m-d H:i:s");

                    $campaignLog = new CampaignLog();
                    $campaignLog->customer_id = $order->customer_id;
                    $campaignLog->order_id = $order->id;
                    $campaignLog->coupon_code = $order->coupon_code;
                    $campaignLog->coupon_amount = $amount;
                    $campaignLog->weight = $order->total_weight;
                    $campaignLog->save();

                    $campaign = Campaign::where('code', '=', $eshiperService['coupon_code'])->first();
                    if(!empty($campaign)){
                        $numberUse = Order::where('coupon_code', '=', $eshiperService['coupon_code'])->count();
                        if($numberUse + 1 >= $campaign->num_of_use){
                            $campaign->is_used = 1;
                            $campaign->update();
                        }
                    }
                }
            }

            $order->update();
            //Đẩy vào notifi
            $users = User::where('role_id', '=', 1)->get();
            if(!empty($users)){
                $this->realtimeUtil->registerChannel('notification');
                foreach($users as $user){
                    $notification = new Notification();
                    $notification->to_user_id = $user->id;
                    $notification->name = $order->code;
                    $notification->is_read = 0;
                    $notification->save();

                    $notification->path = route('customer.express.edit', $order->id) . '?notification_id='.$notification->id;
                    $notification->update();
                    // Đẩy vào 1 channel có sẵn
                    $this->realtimeUtil->publish(['name' => $notification->name, 'to_user_id' => $notification->to_user_id, 'path' => $notification->path], 'notification');
                }
            }

            DB::commit();
            $order->email_send=config('express.email');
            Mail::send('email.admin-create-express', ['order' => $order], function ($message) use ($order) {
                $message->to($order->email_send,$order->email_send)->subject(__('email.create-express-quote'));
            });
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500)->header('Content-Type', 'text/plain');;
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        $express = Order::where('id', '=', $id)->where('customer_id', auth()->id())->first();
        if (!$express) {
            return response('Not Found', 404);
        }

        $data = $this->convertExpress($express);

        return response([
            'express' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request) {
        $express = Order::where('id', $request->id)->where('customer_id', auth()->id())->first();
        if (empty($express)) {
            return redirect()->route('express.list');
        }
        $this->data['express'] = $express;
        return view('express::quote.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(ExpressRequest $request) {
        $param = $request->all();
        $unset_list = array('attention', 'instruction', 'shipping_date', 'confirm_delivery', 'residential', 'notify_recipient');
        $addressFrom = $param['addressFrom'];
        $addressTo = $param['addressTo'];
        $eshiperService = $param['quote'];
        $packages = $param['packages'];
        $services = $param['services'];
        $orderInfo = $param['order_info'];
        try {
            DB::beginTransaction();
            $order = Order::where('id', $orderInfo['order_id'])->where('customer_id', auth()->id())->first();
            if (!$order) {
                return response('Not found', 404);
            }
            $customer = auth()->user();
            if (isset($addressTo) && !empty($addressTo)) {
                if (isset($addressTo['id']) && !empty($addressTo['id'])) {
                    $receiver = Address::where('id', $addressTo['id'])->first();
                    if (empty($receiver)) {
                        $receiver = new Address();
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list))
                                $receiver->$property = $addressTo[$property];
                        }
                        $receiver->type = 2;
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    } else {
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list))
                                $receiver->$property = $addressTo[$property];
                        }
                        $receiver->update();
                    }
                } else {
                    $receiver = new Address();
                    $properties = array_keys($addressTo);
                    foreach ($properties as $property) {
                        if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list))
                            $receiver->$property = $addressTo[$property];
                    }
                    $receiver->customer_id = $customer->id;
                    $receiver->type = 2;
                    $receiver->save();
                }
            }
            if ($order->shipping_status <= OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN && $order->order_status <= OrderStatusEnum::STATUS_PROCESSING) {
                $order->sender_first_name = trim($addressFrom['first_name']);
                $order->sender_middle_name = trim($addressFrom['middle_name']);
                $order->sender_last_name = trim($addressFrom['last_name']);
                $order->sender_email = $addressFrom['email'];
                $order->sender_phone = $addressFrom['telephone'];
                $order->sender_address = $addressFrom['address_1'];
                $order->sender_country_id = $addressFrom['country_id'];
                $order->sender_province_id = $addressFrom['province_id'];
                $order->sender_city_id = $addressFrom['city_id'];
                $order->sender_post_code = $addressFrom['postal_code'];
                $order->sender_attention = $addressFrom['attention'];
                $order->sender_instruction = $addressFrom['instruction'];
                $order->sender_address_2 = $addressFrom['address_2'];
                $order->shipping_date = !empty($addressFrom['shipping_date']) ? date('Y-m-d', strtotime(date(config('app.date_format'), strtotime($addressFrom['shipping_date'])))) : null;
                $order->sender_confirm_delivery = isset($addressFrom['confirm_delivery']) && $addressFrom['confirm_delivery'] ? 1 : 0;
                $order->sender_residential = isset($addressFrom['residential']) && $addressFrom['residential'] ? 1 : 0;

                $order->receive_first_name = trim($addressTo['first_name']);
                $order->receiver_middle_name = trim($addressTo['middle_name']);
                $order->receive_last_name = trim($addressTo['last_name']);
                $order->receiver_email = $addressTo['email'];
                $order->receiver_phone = $addressTo['telephone'];
                $order->receiver_address = $addressTo['address_1'];
                $order->receiver_country_id = $addressTo['country_id'];
                $order->receiver_province_id = $addressTo['province_id'];
                $order->receiver_city_id = $addressTo['city_id'];
                $order->receiver_post_code = $addressTo['postal_code'];
                $order->receiver_attention = $addressTo['attention'];
                $order->receiver_instruction = $addressTo['instruction'];
                $order->receiver_address_2 = $addressTo['address_2'];
                $order->receiver_notify_recipient = isset($addressTo['notify_recipient']) && $addressTo['notify_recipient'] ? 1 : 0;
                $order->receiver_residential = isset($addressTo['residential']) && $addressTo['residential'] ? 1 : 0;

                $order->dimension_unit_id = $packages['dimType'];
                $order->weight_unit_id = $packages['dimType'];
                $order->package_type = $packages['type'];
                $order->package_quantity = $packages['quantity'];
                $order->ref_code = $packages['referenceCode'];
                $order->addtional_service = $services;
                $order->eshiper_service = $eshiperService;
                $order->update();
                $total_weight = 0;
                $total_insurance = 0;
                $editIds = [];
                foreach ($packages['boxes'] as $key => $product) {
                    if (!isset($product['id']) || empty($product['id'])) {
                        $editIds[] = $product['id'];
                    }
                }
                $order->order_items()->whereIn('id', $editIds)->update(['is_delete' => 1]);
                foreach ($packages['boxes'] as $key => $product) {
                    if (isset($product['id']) && !empty($product['id'])) {
                        $orderItem = OrderItem::find($product['id']);
                    } else {
                        $orderItem = new OrderItem();
                    }
                    $orderItem->order_id = $order->id;
                    $orderItem->name = 'EXPRESS_' . $order->id . '- PRODUCT_' . ($key + 1);
                    $orderItem->description = $product['description'];
                    $orderItem->weight = $product['weight'];
                    $orderItem->length = $product['length'];
                    $orderItem->width = $product['width'];
                    $orderItem->height = $product['height'];
                    $orderItem->freight_class = $product['freightClass'];
                    $orderItem->nmfc_code = $product['nmfcCode'];
                    $orderItem->sub_total_insurrance_fee = $product['insuranceAmount'];
                    $orderItem->cod_fee = $product['codValue'];
                    $orderItem->save();

                    $total_weight += $orderItem->weight;
                    $total_insurance += $orderItem->sub_total_insurrance_fee;
                }
                $order->total_weight = $total_weight;
                $order->total_insurrance_fee = $total_insurance;
                $order->update();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response(
                'Ok'
        );
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function cancelOrder(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }

        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $order = Order::find($request->get('id'));
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        if ($order->order_status == OrderStatusEnum::STATUS_CANCEL || $order->shipping_status != OrderShippingStatusEnum::SHIPPING_STATUS_NEW) {
            return response(["success" => false, "message" => __('message.order_no_allow_cancel'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $order->order_status = OrderStatusEnum::STATUS_CANCEL;
            $order->update();
            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_order_success'), "data" => ['refUrl' => route('express.list')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function getQuotes(ExpressRequest $request) {
        $params = $this->buildParams($request->all());
        $result = EshiperService::getQuoteEshiper($params);
        if (!$result["success"]) {
            return response($result['message'], 500);
        }
        $list_carrier =[];
        if (!empty($params['addressFrom']['shipping_date'])) {
            foreach ($result['data'] as $key => $val) {
                $result['data'][$key]['receiver_date'] = ConvertsUtil::dateAddDay($params['addressFrom']['shipping_date'], $val['transitDays'], 'd/m/Y', 'd/m/Y');
                if($val['carrierName']!=="Canpar"){
                    array_push($list_carrier,$result['data'][$key]);
            }
            }
        } else {
            foreach ($result['data'] as $key => $val) {
                $result['data'][$key]['receiver_date'] = ConvertsUtil::dateAddDay(date('d/m/Y'), $val['transitDays'], 'd/m/Y', 'd/m/Y');
                if($val['carrierName']!=="Canpar"){
                    array_push($list_carrier,$result['data'][$key]);
            }
            }
        }
        $result['data']=$list_carrier;
        return response(
                $result
        );
    }

    public function getPdf($id) {
        $order = Order::where('id', '=', $id)->first();
        if (empty($order) || empty($order->eshipper_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $order->eshipper_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    public function viewHistoryEshipper($id) {
        $order = Order::where('id', '=', $id)->first();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_sys'), []]);
        }
        if (!empty($order->express_order_id)) {
            $eshiperResponse = EshiperService::getOrder($order->express_order_id);
            if ($eshiperResponse['success']) {
                $order->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                if ($order->eshiper_shipping_status == EshiperEnum::DELIVERED) {
                    $order->eshiper_charge_order = json_encode($eshiperResponse['data']);
                    $order->total_discount = 0;
                    if($order->per_discount > 0){
                        $order->total_discount = ($eshiperResponse['data']['baseCharge'] * $order->per_discount) / 100;
                    }
                    $order->charge_total_final = $eshiperResponse['data']['totalCharge'];
                    $order->eshiper_total_discount = 0.4 * $eshiperResponse['data']['baseCharge'];
                    $order->agency_discount = $order->eshiper_total_discount;

                    // Tặng điểm
                    // PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);
                }
                if(isset($eshiperResponse['data']['customLabel']) && !empty($eshiperResponse['data']['customLabel'])){
                    $order->eshipper_custom_label = $eshiperResponse['data']['customLabel'];
                }
                $order->update();
            } else {
                return response(["success" => false, "message" => $eshiperResponse['message'], []]);
            }
            return response(["success" => true, "message" => __('message.get_data_success'), "data" => $eshiperResponse['data']['history']]);
        }
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => []]);
    }

    private function buildParams($params) {
        $countries = Country::where("is_active", 1)->pluck('code', 'id');
        $provinces = Province::where("is_active", 1)->pluck('code', 'id');
        $cities = City::where("is_active", 1)->pluck('name', 'id');
        $params['typeAttrs'] = ['serviceId' => 0, 'stackable' => true];
        if((isset($params['services']['saturdayDelivery']) && $params['services']['saturdayDelivery']) || (isset($params['services']['saturdayPickup']) && $params['services']['saturdayPickup'])){
            $params['typeAttrs']['isSaturdayService'] = "true";
        }else{
            $params['typeAttrs']['isSaturdayService'] = "false";
        }
        if(isset($params['services']['holdForPickup']) && $params['services']['holdForPickup']){
            $params['typeAttrs']['holdForPickupRequired'] = "true";
        }else{
            $params['typeAttrs']['holdForPickupRequired'] = "false";
        }
        if(isset($params['services']['docsOnly']) && $params['services']['docsOnly']){
            $params['typeAttrs']['docsOnly'] = "true";
        }else{
            $params['typeAttrs']['docsOnly'] = "false";
        }
        if(isset($params['services']['dangerousGoods']) && !empty($params['services']['dangerousGoods'])){
            $params['typeAttrs']['dangerousGoodsType'] = $params['services']['dangerousGoods'];
        }
        if(isset($params['services']['signatureRequired']) && !empty($params['services']['signatureRequired'])){
            $params['typeAttrs']['signatureRequired'] = "Yes";
        }else{
            $params['typeAttrs']['signatureRequired'] = "No";
        }
        if(isset($params['services']['insuranceType']) && !empty($params['services']['insuranceType'])){
            $params['typeAttrs']['insuranceType'] = $params['services']['insuranceType'];
        }
        $params['addressFrom']['company'] = $params['addressFrom']['last_name'] . ' ' . $params['addressFrom']['middle_name'] . ' ' . $params['addressFrom']['first_name'];
        $params['addressFrom']['state'] = isset($provinces[$params['addressFrom']['province_id']]) ? $provinces[$params['addressFrom']['province_id']] : '';
        $params['addressFrom']['country'] = isset($countries[$params['addressFrom']['country_id']]) ? $countries[$params['addressFrom']['country_id']] : '';
        $params['addressFrom']['city'] = isset($cities[$params['addressFrom']['city_id']]) ? $cities[$params['addressFrom']['city_id']] : '';
        $params['addressTo']['company'] = $params['addressTo']['last_name'] . ' ' . $params['addressTo']['middle_name'] . ' ' . $params['addressTo']['first_name'];
        $params['addressTo']['state'] = isset($provinces[$params['addressTo']['province_id']]) ? $provinces[$params['addressTo']['province_id']] : '';
        $params['addressTo']['country'] = isset($countries[$params['addressTo']['country_id']]) ? $countries[$params['addressTo']['country_id']] : '';
        $params['addressTo']['city'] = isset($cities[$params['addressTo']['city_id']]) ? $cities[$params['addressTo']['city_id']] : '';
        if (!empty($params['services']['codPaymentType'])) {
            $params['cod']['payment_type'] = EshiperEnum::COD_PAYMENT_TYPE[$params['services']['codPaymentType']];
            $params['cod']['cod_data'] = $params['addressTo'];
        }
        if (isset($params['pickup']['is_schedule']) && $params['pickup']['is_schedule'] == EshiperEnum::IS_PICKUP) {
            $params['pickup']['pickup_date'] = ConvertsUtil::strToDateFormat($params['pickup']['date_time']);
            $params['pickup']['pickup_time'] = $params['pickup']['start_hour_time'] . ':' . $params['pickup']['start_minute_time'];
            $params['pickup']['closing_time'] = $params['pickup']['closing_hour_time'] . ':' . $params['pickup']['closing_minute_time'];
        }
        return $params;
    }

    private function convertExpress($express) {
        $data = [];
        $data['addressFrom'] = array(
            'id' => $express->sender_address_id,
            'first_name' => $express->sender_first_name,
            'middle_name' => $express->sender_middle_name,
            'last_name' => $express->sender_last_name,
            'email' => $express->sender_email,
            'telephone' => $express->sender_phone,
            'address_1' => $express->sender_address,
            'country_id' => $express->sender_country_id,
            'province_id' => $express->sender_province_id,
            'city_id' => $express->sender_city_id,
            'postal_code' => $express->sender_post_code,
            'attention' => $express->sender_attention,
            'instruction' => $express->sender_instruction,
            'address_2' => $express->sender_address_2,
            'shipping_date' => !empty($express->shipping_date) ? date(config('app.date_format'), strtotime($express->shipping_date)) : '',
            'confirm_delivery' => $express->sender_confirm_delivery ? true : false,
            'residential' => $express->sender_residential ? true : false,
        );
        $data['addressTo'] = array(
            'id' => $express->shipping_address_id,
            'first_name' => $express->receive_first_name,
            'middle_name' => $express->receiver_middle_name,
            'last_name' => $express->receive_last_name,
            'email' => $express->receiver_email,
            'telephone' => $express->receiver_phone,
            'address_1' => $express->receiver_address,
            'country_id' => $express->receiver_country_id,
            'province_id' => $express->receiver_province_id,
            'city_id' => $express->receiver_city_id,
            'postal_code' => $express->receiver_post_code,
            'attention' => $express->receiver_attention,
            'instruction' => $express->receiver_instruction,
            'address_2' => $express->receiver_address_2,
            'notify_recipient' => $express->receiver_notify_recipient ? true : false,
            'residential' => $express->receiver_residential ? true : false,
        );
        $data['pickup'] = json_decode($express->pickup, true);
        $data['custom'] = json_decode($express->eshipper_custom, true);
        $data['outside_canada'] = false;
        if(!empty($data['custom'])){
            $data['outside_canada'] = true;
        }
        $boxes = [];
        $order_items = $express->order_items()->where('is_delete', 0)->get();
        foreach ($order_items as $order_item) {
            $boxes[] = array(
                'id' => $order_item->id,
                'weight' => $order_item->weight,
                'description' => $order_item->description,
                'length' => $order_item->length,
                'width' => $order_item->width,
                'height' => $order_item->height,
                'insuranceAmount' => $order_item->sub_total_insurrance_fee,
                'codValue' => $order_item->cod_fee,
                'freightClass' => $order_item->freight_class,
                'nmfcCode' => $order_item->nmfc_code
            );
        }
        $data['packages'] = array(
            'dimType' => $express->dimension_unit_id,
            'type' => $express->package_type,
            'quantity' => $express->package_quantity,
            'referenceCode' => $express->ref_code,
            'boxes' => $boxes,
        );
        $data['services'] = $express->addtional_service;
        $data['quote'] = $express->eshiper_service;
        $data['quotes'] = $express->eshiper_service_list;
        $data['order_info'] = array(
            'order_id' => $express->id,
            'created_at' => date('d/m/Y', strtotime($express->created_at)),
            $total_pay = !empty($express->total_final) ? $express->total_final : $data['quote']['totalCharge'],
            'order_status' => $express->order_status,
            'payment_method' => $express->payment_method,
            'payment_status' => $express->payment_status,
            'total_declare_price' => $express->total_declare_price,
            'total_final' => $express->total_final,
            'total_goods_fee' => $express->total_goods_fee,
            'total_insurrance_fee' => $express->total_insurrance_fee,
            'total_paid_amount' => $express->total_paid_amount,
            'total_remain_amount' => $express->total_remain_amount,
            'total_shipping_fee' => $express->total_shipping_fee,
            'total_weight' => $express->total_weight,
            'total_surcharge_fee' => $express->total_surcharge_fee,
            //'total_discount' => !empty($express->total_discount) ? $express->total_discount : 0
             $discount_number = !empty($express->total_discount) ? $express->total_discount : 0  ,
            'discount_number'=>$discount_number,
            'total_pay'=>$total_pay - (($discount_number*$total_pay)/100),
        );
        return $data;
    }

    private function updateInfoCustmer(Customer $customer, $addressFrom){
        $isCustomerUpadte = false;
        if(empty($customer->first_name)){
            $customer->first_name = trim($addressFrom['first_name']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->middle_name)){
            $customer->middle_name = trim($addressFrom['middle_name']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->last_name)){
            $customer->last_name = trim($addressFrom['last_name']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->address_1)){
            $customer->address_1 = trim($addressFrom['address_1']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->country_id)){
            $customer->country_id = trim($addressFrom['country_id']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->province_id)){
            $customer->province_id = trim($addressFrom['province_id']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->city_id)){
            $customer->city_id = trim($addressFrom['city_id']);
            $isCustomerUpadte = true;
        }
        if(empty($customer->postal_code)){
            $customer->postal_code = trim($addressFrom['postal_code']);
            $isCustomerUpadte = true;
        }
        if($isCustomerUpadte){
            $customer->update();
        }
    }
}
