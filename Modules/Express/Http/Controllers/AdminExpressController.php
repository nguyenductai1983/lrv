<?php

namespace Modules\Express\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Exception;
// use Illuminate\Support\Facades\Input;
use App\Address;
use App\Agency;
use App\City;
use App\Currency;
use App\Order;
use App\OrderItem;
use App\Province;
use App\Country;
use App\Customer;
use App\Transaction;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Enum\EshiperEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Enum\CustomerGroupEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\PointEnum;
use App\Services\EshiperService;
// use App\Services\PointService;
use Modules\Express\Http\Requests\AdminExpressRequest;
use Modules\Express\Http\Requests\ExpressRequest;
use Modules\Express\Http\Requests\PaymentRequest;
use App\Page;
use App\Exports\ExportQuery;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Mime\Message;

class AdminExpressController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '5.1'
        ];
    }

    public function index(Request $request) {
        $query = Order::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::EXPRESS);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('express::express.index', $this->data);
    }

public function exportExcel(Request $request)
    {
     $query = Order::where([]);
        $user = Auth::user();
        $query->select('tbl_order.code','tbl_order.created_at', 'customer_note', 'user_note', 'description', 'sender_first_name',
                'sender_middle_name', 'sender_last_name', 'sender_phone', 'sender_cellphone', 'sender_address',
                'sender_post_code', 'receive_first_name', 'nuoc_gui.name as sender_countrie','tinh_gui.name as sender_province',
                'thanh_pho_gui.name as sender_city','receiver_middle_name', 'receive_last_name', 'receiver_email', 'receiver_phone',
                'receiver_cellphone', 'receiver_address', 'receiver_post_code','nuoc_nhan.name as receiver_countrie',
                'tinh_nhan.name as receiver_province','thanh_pho_nhan.name as receiver_city', 'is_crr', 'total_weight', 'total_final',
                'total_paid_amount', 'total_surcharge_fee', 'total_insurrance_fee','total_remain_amount', 'total_declare_price',
                'agency_discount', 'order_status', 'shipping_status','payment_status', 'receive_status');
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::EXPRESS);
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_order.created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_order.created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id' );
        // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id' );
        // lấy thông tin tỉnh gửi
         $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id' );
         // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id' );
        // lấy thông tin thành phố gửi
         $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id' );
         // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id' );
        $query->orderBy('tbl_order.id', 'desc');
        return (new ExportQuery($query))->download('express_agency.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $slug=68;
        if (!$page = Page::with('translate')->where('id', '=', $slug)->first()) {
            $page='';
        }
        $this->data['termagree']=$page;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('express::express.create', $this->data);
    }

    public function printBill($id)
    {
       $order = Order::find($id);
        if (empty($order)) {
            return redirect()->route('admin.express.list');
        }
        $this->data['order'] = $order;
        return view('express::express.print', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function buildOrder($order,$addressFrom, $addressTo,$eshiperServiceList,$eshiperService,
    $packages ,$order_data, $services,$param )
    {
        try{
            $order->sender_first_name = trim($addressFrom['first_name']);
            $order->sender_middle_name = trim($addressFrom['middle_name']);
            $order->sender_last_name = trim($addressFrom['last_name']);
            $order->sender_email = $addressFrom['email'];
            $order->sender_phone = $addressFrom['telephone'];
            $order->sender_address = $addressFrom['address_1'];
            $order->sender_country_id = $addressFrom['country_id'];
            $order->sender_province_id = $addressFrom['province_id'];
            $order->sender_city_id = $addressFrom['city_id'];
            $order->sender_post_code = $addressFrom['postal_code'];
            $order->sender_attention = $addressFrom['attention'];
            try{
                if(isset($addressFrom['instruction'])){
            $instruction = $addressFrom['instruction'];
            $instruction = str_replace("\"","",$instruction);
            $instruction = str_replace(";","",$instruction);
            $instruction = substr($instruction,0,250);
            $instruction = $instruction.'.';
            $order->sender_instruction = $instruction;
                }
           }
           catch (Exception $e)
           {
               Log::error($e.$addressFrom['instruction'] );
           }
            $order->sender_address_2 = $addressFrom['address_2'];
            $order->shipping_date = !empty($addressFrom['shipping_date']) ? ConvertsUtil::dateFormat($addressFrom['shipping_date'], config('app.date_format')) : null;
            $order->sender_confirm_delivery = isset($addressFrom['confirm_delivery']) && $addressFrom['confirm_delivery'] ? 1 : 0;
            $order->sender_residential = isset($addressFrom['residential']) && $addressFrom['residential'] ? 1 : 0;
            $order->pickup_type = isset($addressFrom['pickup_type']) ? $addressFrom['pickup_type'] : 2;
            $order->receive_first_name = trim($addressTo['first_name']);
            $order->receiver_middle_name = trim($addressTo['middle_name']);
            $order->receive_last_name = trim($addressTo['last_name']);
            $order->receiver_email = $addressTo['email'];
            $order->receiver_phone = $addressTo['telephone'];
            $order->receiver_address = $addressTo['address_1'];
            $order->receiver_country_id = $addressTo['country_id'];
            $order->receiver_province_id = $addressTo['province_id'];
            $order->receiver_city_id = $addressTo['city_id'];
            $order->receiver_post_code = $addressTo['postal_code'];
            $order->receiver_attention = $addressTo['attention'];
            $order->receiver_instruction = $addressTo['instruction'];
            $order->receiver_address_2 = $addressTo['address_2'];
            $order->receiver_notify_recipient = isset($addressTo['notify_recipient']) && $addressTo['notify_recipient'] ? 1 : 0;
            $order->receiver_residential = isset($addressTo['residential']) && $addressTo['residential'] ? 1 : 0;

//            $order->currency_id = Currency::where('code', $eshiperService['currency'])->first()->id;
            $order->eshiper_total_charge = $eshiperService['totalCharge'];
            $order->total_final = $order->eshiper_total_charge - $order->total_discount;
            $order->total_final =  round($order->total_final, 2);
            $order->dimension_unit_id = $packages['dimType'];
            $order->weight_unit_id = $packages['dimType'];
            $order->package_type = $packages['type'];
            $order->package_quantity = $packages['quantity'];
            $order->ref_code = $packages['referenceCode'];
            $order->eshiper_service = $eshiperService;
            $order->eshiper_service_list = $eshiperServiceList;
            $order->eshiper_base_charge = $eshiperService['baseCharge'];
            $order->eshiper_fuel_surcharge = $eshiperService['fuelSurcharge'];
            $order->eshiper_other_surcharge = $eshiperService['totalCharge'] - $eshiperService['baseCharge'] - $eshiperService['fuelSurcharge'];
            $order->eshiper_total_discount = 0.15 * $eshiperService['totalCharge'];
            $order->total_discount = 0;
            if(isset($eshiperService['discount_number']) && (float) $eshiperService['discount_number'] > 0){
                $order->per_discount = (float) $eshiperService['discount_number'];
                $order->total_discount = ($order->eshiper_base_charge * $order->per_discount) / 100;
            }
            $order->payment_method = $order_data['payment_method'];
            $order->total_paid_amount = $order_data['total_pay'];
            $order->addtional_service = $services;
            $order->agency_discount = $order->eshiper_total_discount;
            $order->pickup = json_encode($param['pickup']);
            $order->eshipper_custom = json_encode($param['custom']);
            if(isset($order_data['last_pay_amount']))
            {
            $order->total_paid_amount += $order_data['last_pay_amount'];
            }
            $datetmp=getdate();
            $datetmp=$datetmp['mday'].'/'.$datetmp['mon'].'/'.$datetmp['year'];
            $order->last_payment_at = ConvertsUtil::dateFormat($datetmp, config('app.date_format'));
            $order->total_remain_amount = $order->total_final - $order->total_paid_amount > 0 ? $order->total_final - $order->total_paid_amount : 0;
            if($order->total_remain_amount == $order->total_final)
            {
                $order->payment_status = OrderPaymentStatusEnum::NOT_PAID;
            }
           else if ($order->total_remain_amount == 0) {
                $order->payment_status = OrderPaymentStatusEnum::FULL_PAID;
            } else {
                $order->payment_status = OrderPaymentStatusEnum::PART_PAID;
            }
            return $order;
        }
        catch(Exception $e)
        {
            Log::error($e);
            return false;
        }
    }
    // hàm này hiện tại chưa dùng nữa chờ kiểm tra
    public function store(AdminExpressRequest $request) {
        $unset_list = array('shipping_date');
        $use_list = array('attention', 'instruction', 'confirm_delivery', 'residential', 'notify_recipient');
        $param = $request->all();
        $addressFrom = $param['addressFrom'];
        $addressTo = $param['addressTo'];
        $eshiperService = $param['quote'];
        $eshiperServiceList = $param['quotes'];
        $packages = $param['packages'];
        $services = $param['services'];
        $order_data = $param['order'];;
        $order_save=false;
        if(isset($param['save']) && !empty($param['save'])) {
            $order_save = $param['save'];
        }
        $user = auth()->guard('admin')->user();
        $agency = Agency::find($user->agency_id);
        try {
            $isCreateCustomer = true;
            DB::beginTransaction();
            if (isset($order_data['customer_id']) && !empty($order_data['customer_id'])) {
                $customer = Customer::where('id', $order_data['customer_id'])->first();
                if (!empty($customer)) {
                    $isCreateCustomer = false;
                    $customer->update([
                        'id_card' => $addressFrom['id_card'],
                        'card_expire' => $addressFrom['card_expire'],
                        'birthday' => $addressFrom['birthday'],
                        'career' => $addressFrom['career'],
                        'email' => $addressFrom['email'],
                        'image_1_file_id' => $addressFrom['image_1_file_id'],
                        'image_2_file_id' => $addressFrom['image_2_file_id'],
                        'image_3_file_id' => $addressFrom['image_3_file_id']
                    ]);
                }
            }
            if ($isCreateCustomer) {
                $customerExist = null;
                if(!empty($addressFrom['email'])){
                    $customerExist = Customer::where('email', $addressFrom['email'])->first();
                }
                if(!empty($customerExist)){
                    return response(__('message.cus_email_exist'), 500);
                }
                $customer = new Customer();
                $customer->agency_id = $agency->id;
                $customer->country_id = $addressFrom["country_id"];
                $customer->province_id = $addressFrom["province_id"];
                $customer->city_id = $addressFrom["city_id"];
                $customer->address_1 = $addressFrom["address_1"];
                $customer->address_2 = $addressFrom["address_2"];
                $customer->telephone = $addressFrom["telephone"];
                $customer->cellphone = $addressFrom["cellphone"];
                $customer->postal_code = $addressFrom["postal_code"];
                $customer->first_name = $addressFrom["first_name"];
                $customer->middle_name = $addressFrom["middle_name"];
                $customer->last_name = $addressFrom["last_name"];
                $customer->customer_group_id = CustomerGroupEnum::CUS_SHP;
                $customer->code = "KH_";
                $customer->save();
                $customer->code = "KH_" . $customer->id;
                $customer->update();
                $addressFrom['id']=$customer->id;
            }
            if (isset($addressTo) && !empty($addressTo)) {
                if (isset($addressTo['id']) && !empty($addressTo['id'])) {
                    $receiver = Address::where('id', $addressTo['id'])->where('customer_id', $customer->id)->first();
                    if (empty($receiver)) {
                        unset($addressTo['id']);
                        $receiver = new Address();
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list)){
                                $receiver->$property = $addressTo[$property];
                            }
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                        $addressTo['id']=$receiver->id;
                    } else {
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $use_list)){
                                $receiver->$property = $addressTo[$property];
                            }
                        }
                        $receiver->update();
                    }
                } else {
                    $receiver = new Address();
                    $properties = array_keys($addressTo);
                    foreach ($properties as $property) {
                        if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list))
                            $receiver->$property = $addressTo[$property];
                    }
                    $receiver->customer_id = $customer->id;
                    $receiver->save();
                    $addressTo['id']=$receiver->id;
                }
            }
            $count_transport= Order::where('user_id', $user->id)->where('type',OrderTypeEnum::EXPRESS) ->count() +1;
            $countryreceiver = Country::find($receiver->country_id);
            $countryCode = isset($countryreceiver->code) && !empty($countryreceiver->code) ? $countryreceiver->code : 'VN';
            $order = new Order();
            $order=$this->buildOrder($order,$addressFrom, $addressTo,$eshiperServiceList,$eshiperService,
            $packages ,$order_data, $services,$param);
            $order->customer_id = $customer->id;
            $order->user_id = $user->id;
            $order->agency_id = $user->agency_id;
            // $order->sender_address_id = $addressFrom['id'];
            $order->shipping_address_id = $addressTo['id'];
            $order->currency_id = Currency::where('code', $eshiperService['currency'])->first()->id;
            $order->eshiper_shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_NEW;
            $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
            $order->type = OrderTypeEnum::EXPRESS;
            $order->code = $user->code . sprintf('%04d',$count_transport) .'X' . $countryCode;
            $order->save();
            $total_weight = 0;
             $count_package= count($packages['boxes']);
            foreach ($packages['boxes'] as $key => $product) {
                $order_item = new OrderItem();
                $order_item->order_id = $order->id;
                $order_item->name = $order->code . ($key + 1).$count_package;
                $order_item->description = $product['description'];
                $order_item->length = $product['length'];
                $order_item->height = $product['height'];
                $order_item->width = $product['width'];
                $order_item->weight = $product['weight'];
                $volume = ($order_item->width * $order_item->length * $order_item->height) / config('express.volumeweight');
                if($volume > $order_item->weight){
                    $order_item->weight = $volume;
                }
                $order_item->freight_class = $product['freightClass'];
                $order_item->nmfc_code = $product['nmfcCode'];
                $order_item->sub_total_insurrance_fee = $product['insuranceAmount'];
                $order_item->cod_fee = $product['codValue'];
                $order_item->save();
                $total_weight += $order_item->weight;
            }
            $param['reference']['code']=$order->code;
            $param['reference']['name']='iGreenLink';
            if (!$order_save)
            {
            $paramsShippingRequest = $this->buildParams($param, true);
            $shipment = EshiperService::getShippingEshiper($paramsShippingRequest);
            if ($shipment['success'] && !empty($shipment['data']['orderId'])) {
                $order->eshiper_shipping_status = !empty($shipment['data']['statusId']) ? $shipment['data']['statusId'] : EshiperEnum::READY_FOR_SHIPPING;
                $order->express_order_id = $shipment['data']['orderId'];
                $order->express_tracking_url = $shipment['data']['trackingUrl'];
                $order->pickupNumber = $shipment['data']['pickupNumber'];
                $order->express_order = $shipment['data'];
                $order->total_weight = $total_weight;
                $order->eshipper_label = $shipment['data']['label'];
                if(isset($shipment['data']['customLabel']) && !empty($shipment['data']['customLabel'])){
                    $order->eshipper_custom_label = $shipment['data']['customLabel'];
                }
                $order->update();
            } else {
                DB::rollBack();
                return response(!empty($shipment['message']) ? $shipment['message'] : 'Not create order', 500);
            }
        }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response('Internal Server Error', 500);
        }
        return response('Ok',200)->header('Content-Type', 'text/plain');
    }

    public function storeupdate(AdminExpressRequest $request) {
        $unset_list = array('shipping_date');
        $use_list = array('attention', 'instruction', 'confirm_delivery', 'residential', 'notify_recipient');
        $param = $request->all();
        $addressFrom = $param['addressFrom'];
        $addressTo = $param['addressTo'];
        $eshiperService = $param['quote'];
        $eshiperServiceList = $param['quotes'];
        $packages = $param['packages'];
        $services = $param['services'];
        $order_data = $param['order'];
        $user = auth()->guard('admin')->user();
        $agency = Agency::find($user->agency_id);
        try {
            $isCreateCustomer = true;
            DB::beginTransaction();
            if (isset($order_data['customer_id']) && !empty($order_data['customer_id'])) {
                $customer = Customer::where('id', $order_data['customer_id'])->first();
                if (!empty($customer)) {
                    $isCreateCustomer = false;
                     $customer->update([
                        'id_card' => $addressFrom['id_card'],
                        'card_expire' => $addressFrom['card_expire'],
                        'birthday' => $addressFrom['birthday'],
                        'career' => $addressFrom['career'],
                        'email' => $addressFrom['email'],
                        'image_1_file_id' => $addressFrom['image1']['id'],
                        'image_2_file_id' => $addressFrom['image2']['id'],
                        'image_3_file_id' => $addressFrom['image3']['id']
                    ]);
                }
            }
            if ($isCreateCustomer) {
                $customerExist = null;
                if(!empty($addressFrom['email'])){
                    $customerExist = Customer::where('email', $addressFrom['email'])->first();
                }
                if(!empty($customerExist)){
                    return response(__('message.cus_email_exist'), 500);
                }
                $customer = new Customer();
                $customer->agency_id = $agency->id;
                $customer->country_id = $addressFrom["country_id"];
                $customer->province_id = $addressFrom["province_id"];
                $customer->city_id = $addressFrom["city_id"];
                $customer->address_1 = $addressFrom["address_1"];
                $customer->address_2 = $addressFrom["address_2"];
                $customer->telephone = $addressFrom["telephone"];
                $customer->cellphone = $addressFrom["cellphone"];
                $customer->postal_code = $addressFrom["postal_code"];
                $customer->first_name = $addressFrom["first_name"];
                $customer->middle_name = $addressFrom["middle_name"];
                $customer->last_name = $addressFrom["last_name"];
                $customer->customer_group_id = CustomerGroupEnum::CUS_SHP;
                $customer->code = "KH_";
                $customer->save();
                $customer->code = "KH_" . $customer->id;
                $customer->update();
            }
            if (isset($addressTo) && !empty($addressTo)) {
                if (isset($addressTo['id']) && !empty($addressTo['id'])) {
                    $receiver = Address::where('id', $addressTo['id'])->where('customer_id', $customer->id)->first();
                    if (empty($receiver)) {
                        unset($addressTo['id']);
                        $receiver = new Address();
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list)){
                                $receiver->$property = $addressTo[$property];
                            }
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    } else {
                        $properties = array_keys($addressTo);
                        foreach ($properties as $property) {
                            if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $use_list)){
                                $receiver->$property = $addressTo[$property];
                            }
                        }
                        $receiver->update();
                    }
                } else {
                    $receiver = new Address();
                    $properties = array_keys($addressTo);
                    foreach ($properties as $property) {
                        if (isset($addressTo[$property]) && !empty($addressTo[$property]) && !in_array($property, $unset_list))
                            $receiver->$property = $addressTo[$property];
                    }
                    $receiver->customer_id = $customer->id;
                    $receiver->save();
                }
            }
            $order= Order::find($order_data['order_id']);
            $order=$this->buildOrder($order,$addressFrom, $addressTo,$eshiperServiceList,$eshiperService,
            $packages ,$order_data, $services,$param);
            $order->order_status  = OrderStatusEnum::STATUS_PROCESSING;
            $order->eshiper_shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_NEW;
            $order->type = OrderTypeEnum::EXPRESS;
            $order->update();
            $total_weight = 0;
             $count_package= count($packages['boxes']);
            foreach ($packages['boxes'] as $key => $product) {
                if (isset($product['id']) && !empty($product['id'])) {
                    $order_item = OrderItem::find($product['id']);
                } else {
                    $order_item = new OrderItem();
                     $order_item->order_id = $order->id;
                     $order_item->name = $order->code . ($key + 1).$count_package;
                       }
                $order_item->description = $product['description'];
                $order_item->length = $product['length'];
                $order_item->height = $product['height'];
                $order_item->width = $product['width'];
                $order_item->weight = $product['weight'];
                $volume = ($order_item->width * $order_item->length * $order_item->height) / config('express.volumeweight');
                if($volume > $order_item->weight){
                    $order_item->weight = $volume;
                }
                $order_item->freight_class = $product['freightClass'];
                $order_item->nmfc_code = $product['nmfcCode'];
                $order_item->sub_total_insurrance_fee = $product['insuranceAmount'];
                $order_item->cod_fee = $product['codValue'];
                $order_item->update();
                $total_weight += $order_item->weight;
            }
            $param['reference']['code']=$order->code;
            $param['reference']['name']='iGreenLink';
            $paramsShippingRequest = $this->buildParams($param, true);
            $shipment = EshiperService::getShippingEshiper($paramsShippingRequest);
            if ($shipment['success'] && !empty($shipment['data']['orderId'])) {
                $order->eshiper_shipping_status = !empty($shipment['data']['statusId']) ? $shipment['data']['statusId'] : EshiperEnum::READY_FOR_SHIPPING;
                $order->express_order_id = $shipment['data']['orderId'];
                $order->express_tracking_url = $shipment['data']['trackingUrl'];
                $order->pickupNumber = $shipment['data']['pickupNumber'];
                $order->express_order = $shipment['data'];
                $order->total_weight = $total_weight;
                $order->eshipper_label = $shipment['data']['label'];
                if(isset($shipment['data']['customLabel']) && !empty($shipment['data']['customLabel'])){
                    $order->eshipper_custom_label = $shipment['data']['customLabel'];
                }
                $order->update();
            } else {
                DB::rollBack();
                return response(!empty($shipment['message']) ? $shipment['message'] : 'Not create order', 500);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('Ok',200)->header('Content-Type', 'text/plain');
    }
 public function update(Request $request) {
        $param = $request->all();
        $order_info = $param['order'];
        try {
            DB::beginTransaction();
            $order = Order::where('id', $order_info['order_id'])->whereNotNull('agency_id')->first();
            if (!$order) {
                return response('Not found', 404);
            }
            if (!empty($order->express_order_id)) {
                $eshiperResponse = EshiperService::getOrder($order->express_order_id);
                if (!$eshiperResponse['success']) {
                    return response($eshiperResponse['message'], 500);
                }
                $order->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                if(isset($eshiperResponse['data']['customLabel']) && !empty($eshiperResponse['data']['customLabel'])){
                    $order->eshipper_custom_label = $eshiperResponse['data']['customLabel'];
                }
                if ($order->eshiper_shipping_status == EshiperEnum::READY_FOR_SHIPPING) {
                    $order->eshiper_charge_order = json_encode($eshiperResponse['data']);
                    $order->total_discount = 0;
                    if($order->per_discount > 0){
                        $order->total_discount = ($eshiperResponse['data']['baseCharge'] * $order->per_discount) / 100;
                    }
                    $order->charge_total_final = $eshiperResponse['data']['totalCharge'] - $order->total_discount;
                    $order->eshiper_total_discount = 0.15 * $eshiperResponse['data']['totalCharge'];
                    $order->agency_discount = $order->eshiper_total_discount;

                    // Tặng điểm
                    // PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);
                }
                $order->update();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('Ok',200)->header('Content-Type', 'text/plain');
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        $express = Order::where('id', '=', $id)->first();
        if (!$express) {
            return response('Not Found', 404);
        }

        $data = $this->convertExpress($express);
        return response([
            'express' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request) {
        $express = Order::where('id', $request->id)->whereNotNull('agency_id')->first();
        if (!$express) {
            return redirect()->route('admin.express.list');
        }
        $this->data['express'] = $express;
        return view('express::express.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */


    public function payment(PaymentRequest $request) {
        $param = $request->all();
        $data = $param['order'];
        $order = Order::where('id', $data['order_id'])->whereNotNull('agency_id')->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();

            if ($data['last_pay_amount'] > 0 && !empty($data['date_pay'])) {
                $count = Transaction::where('order_id', $order->id)->count();

                $transaction = new Transaction();
				$transaction->user_id = $order->user_id;
                $transaction->customer_id = $order->customer_id;
                $transaction->order_id = $order->id;
                $transaction->currency_id = $order->currency_id;
                $transaction->code = "PT_" . $order->code . "_{$count}";
                $transaction->transaction_at = ConvertsUtil::dateFormat($data['date_pay'], config('app.date_format'));
                $transaction->type = TransactionTypeEnum::RECEIPT;
                $transaction->credit_amount = $data['last_pay_amount'];
				$transaction->total_amount = $order->total_final;
				$transaction->total_agency_discount = $order->agency_discount;
				$transaction->agency_discount = $transaction->credit_amount * ($transaction->total_agency_discount / $transaction->total_amount);
                $transaction->goods_code = $order->code;
                $transaction->save();

                $order->payment_method = $data['payment_method'];
                $order->total_paid_amount += $data['last_pay_amount'];
                $order->total_remain_amount = $order->total_final - $order->total_paid_amount > 0 ? $order->total_final - $order->total_paid_amount : 0;
                if ($order->total_remain_amount == 0) {
                    $order->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                } else {
                    $order->payment_status = OrderPaymentStatusEnum::PART_PAID;
                }
                $order->last_payment_at = ConvertsUtil::dateFormat($data['date_pay'], config('app.date_format'));

                $order->update();
                // ngày 25-12-2020 không sử dụng tích điểm cho đại lý
                // PointService::addPointToCustomer($order->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                // PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function cancelOrder(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response($validator->errors()->first(), 500);
        }

        $user = Auth::user();
        if (empty($user)) {
            return response(__('message.err_user_not_login'), 500);
        }
        $order = Order::find($request->get('id'));
        if (empty($order)) {
            return response(__('message.err_order_is_not_valid'), 500);
        }
        if ($order->eshiper_shipping_status == EshiperEnum::CANCELLED) {
            return response(__('message.order_no_allow_cancel'), 500);
        }
        if(empty($order->express_order_id))
        {
            try {
                DB::beginTransaction();
             $order->order_status = OrderStatusEnum::STATUS_CANCEL;
             $order->eshiper_shipping_status = 4;
                $order->update();
                DB::commit();
                return response('OK');
              } catch (Exception $e) {
                DB::rollBack();
                return response($e->getMessage(), 500);
            }
        }
        $eshiperCancelResult = EshiperService::cancelOrder($order->express_order_id);
        if ($eshiperCancelResult["success"] && isset($eshiperCancelResult['data']['orderStatus']) && $eshiperCancelResult['data']['orderStatus'] == EshiperEnum::CANCELLED) {
            try {
                DB::beginTransaction();
                $order->eshiper_shipping_status = $eshiperCancelResult['data']['orderStatus'];
                $order->order_status = OrderStatusEnum::STATUS_CANCEL;
                $order->update();
                DB::commit();

                return response('OK', 200)->header('Content-Type', 'text/plain');
            } catch (Exception $e) {
                DB::rollBack();
                return response($e->getMessage(), 500);
            }
        }
        return response($eshiperCancelResult['message'], 500);
    }

    public function getQuotes(ExpressRequest $request) {
        $params = $this->buildParams($request->all());
        $result = EshiperService::getQuoteEshiper($params);
        if (!$result["success"]) {
            if(empty($result['message']))
            {
                $result['message']='Don\'t connect';
            }
            Log::info('Admin Express '.$result['message']);
            return response($result['message'],500)->header('Content-Type', 'text/plain');
        }
        $list_carrier =[];
        if (!empty($params['addressFrom']['shipping_date'])) {
            foreach ($result['data'] as $key => $val) {
                $result['data'][$key]['receiver_date'] = ConvertsUtil::dateAddDay($params['addressFrom']['shipping_date'], $val['transitDays'], 'd/m/Y', 'd/m/Y');
                if($val['carrierName']!=="Canpar"){
                    array_push($list_carrier,$result['data'][$key]);
            }
        }
        } else {
            foreach ($result['data'] as $key => $val) {
                $result['data'][$key]['receiver_date'] = ConvertsUtil::dateAddDay(date('d/m/Y'), $val['transitDays'], 'd/m/Y', 'd/m/Y');
                if($val['carrierName']!=="Canpar"){
                    array_push($list_carrier,$result['data'][$key]);
            }
        }
        }
        $result['data']=$list_carrier;
        return response($result);
    }

    public function getPdf($id) {
        $order = Order::where('id', '=', $id)->first();
        if (empty($order) || empty($order->eshipper_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $order->eshipper_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    public function getCustomInvoicePdf($id) {
        $order = Order::where('id', '=', $id)->first();
        if (empty($order) || empty($order->eshipper_custom_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $order->eshipper_custom_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    public function viewHistoryEshipper(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $order = Order::find($request->get('id'));
        if(empty($order)){
            return response(["success" => false, "message" => __('message.err_sys'), []]);
        }
        if(!empty($order->express_order_id)){
            $eshiperResponse = EshiperService::getOrder($order->express_order_id);
            if($eshiperResponse['success']){
                $order->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                if(isset($eshiperResponse['data']['customLabel']) && !empty($eshiperResponse['data']['customLabel'])){
                    $order->eshipper_custom_label = $eshiperResponse['data']['customLabel'];
                }
                $order->update();
            }else{
                return response(["success" => false, "message" => $eshiperResponse['message'], []]);
            }
            return response(["success" => true, "message" => __('message.get_data_success'), "data" => $eshiperResponse['data']['history']]);
        }
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => []]);
    }

    private function buildParams($params, $isShip = false) {
        $countries = Country::where("is_active", 1)->pluck('code', 'id');
        $provinces = Province::where("is_active", 1)->pluck('name', 'id');
        $cities = City::where("is_active", 1)->pluck('name', 'id');
        $params['countries'] = $countries;
        $params['provinces'] = $provinces;
        $params['cities'] = $cities;
        $params['typeAttrs'] = ['serviceId' => 0, 'stackable' => true];
        if((isset($params['services']['saturdayDelivery']) && $params['services']['saturdayDelivery']) || (isset($params['services']['saturdayPickup']) && $params['services']['saturdayPickup'])){
            $params['typeAttrs']['isSaturdayService'] = "true";
        }else{
            $params['typeAttrs']['isSaturdayService'] = "false";
        }
        if(isset($params['services']['holdForPickup']) && $params['services']['holdForPickup']){
            $params['typeAttrs']['holdForPickupRequired'] = "true";
        }else{
            $params['typeAttrs']['holdForPickupRequired'] = "false";
        }
        if(isset($params['services']['docsOnly']) && $params['services']['docsOnly']){
            $params['typeAttrs']['docsOnly'] = "true";
        }else{
            $params['typeAttrs']['docsOnly'] = "false";
        }
        if(isset($params['services']['dangerousGoods']) && !empty($params['services']['dangerousGoods'])){
            $params['typeAttrs']['dangerousGoodsType'] = $params['services']['dangerousGoods'];
        }
        if(isset($params['services']['signatureRequired']) && !empty($params['services']['signatureRequired'])){
            $params['typeAttrs']['signatureRequired'] = "Yes";
        }else{
            $params['typeAttrs']['signatureRequired'] = "No";
        }
        if(isset($params['services']['insuranceType']) && !empty($params['services']['insuranceType'])){
            $params['typeAttrs']['insuranceType'] = $params['services']['insuranceType'];
        }
        if ($isShip) {
            $params['typeAttrs']['serviceId'] = $params['quote']['serviceId'];
            if(!empty($params['addressFrom']['shipping_date'])){
                $params['typeAttrs']['scheduledShipDate'] = ConvertsUtil::strToDateFormat($params['addressFrom']['shipping_date']);
            }
            $params['typeAttrs']['stackable'] = true;
            $params['paymentType'] = $params['order']['payment_method'];
        }
        if(isset($params['addressFrom']['pickup_type']) && $params['addressFrom']['pickup_type'] == '1'){
            $user = Auth::user();
            $fullname='';
            if(!empty($user->first_name))
            { $fullname = $user->first_name. ' '; }
            if(!empty($user->middle_name))
            { $fullname .= $user->middle_name. ' '; }
            if(!empty($user->last_name))
            { $fullname .= $user->last_name. ' '; }
            if(!empty($user->code))
            { $fullname .= '('. $user->code. ')'; }
            $params['addressFrom']['company'] = $fullname;
            $params['addressFrom']['state'] = isset($provinces[$user->province_id]) ? $provinces[$user->province_id] : '';
            $params['addressFrom']['country'] = isset($countries[$user->country_id]) ? $countries[$user->country_id] : '';
            $params['addressFrom']['city'] = isset($cities[$user->city_id]) ? $cities[$user->city_id] : '';
            $params['addressFrom']['postal_code'] = $user->postal_code;
            $params['addressFrom']['address_1'] = $user->address;
        }else{
            if(isset($params['addressFrom']['code'])){
                $params['addressFrom']['company'] = $params['addressFrom']['last_name'] . ' ' . $params['addressFrom']['middle_name'] . ' ' . $params['addressFrom']['first_name'].' ('. $params['addressFrom']['code'].')' ;
            }else{
                $params['addressFrom']['company'] = $params['addressFrom']['last_name'] . ' ' . $params['addressFrom']['middle_name'] . ' ' . $params['addressFrom']['first_name'] ;
            }

            $params['addressFrom']['state'] = isset($provinces[$params['addressFrom']['province_id']]) ? $provinces[$params['addressFrom']['province_id']] : '';
            $params['addressFrom']['country'] = isset($countries[$params['addressFrom']['country_id']]) ? $countries[$params['addressFrom']['country_id']] : '';
            $params['addressFrom']['city'] = isset($cities[$params['addressFrom']['city_id']]) ? $cities[$params['addressFrom']['city_id']] : '';
        }
        $params['addressTo']['company'] = $params['addressTo']['last_name'] . ' ' . $params['addressTo']['middle_name'] . ' ' . $params['addressTo']['first_name'];
        $params['addressTo']['state'] = isset($provinces[$params['addressTo']['province_id']]) ? $provinces[$params['addressTo']['province_id']] : '';
        $params['addressTo']['country'] = isset($countries[$params['addressTo']['country_id']]) ? $countries[$params['addressTo']['country_id']] : '';
        $params['addressTo']['city'] = isset($cities[$params['addressTo']['city_id']]) ? $cities[$params['addressTo']['city_id']] : '';

        if (!empty($params['services']['codPaymentType'])) {
            $params['cod']['payment_type'] = EshiperEnum::COD_PAYMENT_TYPE[$params['services']['codPaymentType']];
            $params['cod']['cod_data'] = $params['addressTo'];
        }
        if(isset($params['pickup']['is_schedule']) && $params['pickup']['is_schedule'] == EshiperEnum::IS_PICKUP){
            $params['pickup']['pickup_date'] = ConvertsUtil::strToDateFormat($params['pickup']['date_time']);
            $params['pickup']['pickup_time'] = $params['pickup']['start_hour_time'] .':'. $params['pickup']['start_minute_time'];
            $params['pickup']['closing_time'] = $params['pickup']['closing_hour_time'] .':'. $params['pickup']['closing_minute_time'];
        }
        return $params;
    }

    private function convertExpress($express) {
        $data = [];
        $data['addressFrom'] = array(
            'id' => $express->sender_address_id,
            'first_name' => $express->sender_first_name,
            'middle_name' => $express->sender_middle_name,
            'last_name' => $express->sender_last_name,
            'email' => $express->sender_email,
            'telephone' => $express->sender_phone,
            'address_1' => $express->sender_address,
            'country_id' => $express->sender_country_id,
            'province_id' => $express->sender_province_id,
            'city_id' => $express->sender_city_id,
            'postal_code' => $express->sender_post_code,
            'attention' => $express->sender_attention,
            'instruction' => $express->sender_instruction,
            'address_2' => $express->sender_address_2,
            'shipping_date' => !empty($express->shipping_date) ? date(config('app.date_format'), strtotime($express->shipping_date)) : '',
            'confirm_delivery' => $express->sender_confirm_delivery ? true : false,
            'residential' => $express->sender_residential ? true : false,
            'pickup_type' => $express->pickup_type ? "{$express->pickup_type}" : "2",
            'id_card' => $express->customer->id_card,
            'card_expire' => $express->customer->card_expire,
            'birthday' => $express->customer->birthday,
            'career' => $express->customer->career,
            'image1' => $express->customer->image1,
            'image2' => $express->customer->image2,
            'image3' => $express->customer->image3
        );
        $data['addressTo'] = array(
            'id' => $express->shipping_address_id,
            'first_name' => $express->receive_first_name,
            'middle_name' => $express->receiver_middle_name,
            'last_name' => $express->receive_last_name,
            'email' => $express->receiver_email,
            'telephone' => $express->receiver_phone,
            'address_1' => $express->receiver_address,
            'country_id' => $express->receiver_country_id,
            'province_id' => $express->receiver_province_id,
            'city_id' => $express->receiver_city_id,
            'postal_code' => $express->receiver_post_code,
            'attention' => $express->receiver_attention,
            'instruction' => $express->receiver_instruction,
            'address_2' => $express->receiver_address_2,
            'notify_recipient' => $express->receiver_notify_recipient ? true : false,
            'residential' => $express->receiver_residential ? true : false,
        );
        $data['pickup'] = json_decode($express->pickup, true);
        $data['custom'] = json_decode($express->eshipper_custom, true);
        $data['outside_canada'] = 1;
        if(!empty($data['custom'])){
            $data['outside_canada'] = 2;
        }
        $boxes = [];
        $order_items = $express->order_items()->where('is_delete', 0)->get();
        foreach ($order_items as $order_item) {
            $boxes[] = array(
                'id' => $order_item->id,
                'weight' => $order_item->weight,
                'description' => $order_item->description,
                'length' => $order_item->length,
                'width' => $order_item->width,
                'height' => $order_item->height,
                'insuranceAmount' => $order_item->sub_total_insurrance_fee,
                'codValue' => $order_item->cod_fee,
                'freightClass' => $order_item->freight_class,
                'nmfcCode' => $order_item->nmfc_code
            );
        }
        $data['packages'] = array(
            'dimType' => $express->dimension_unit_id,
            'type' => $express->package_type,
            'quantity' => $express->package_quantity,
            'referenceCode' => $express->ref_code,
            'boxes' => $boxes,
        );
        $data['services'] = $express->addtional_service;
        $data['quote'] = $express->eshiper_service;
        $data['quotes'] = $express->eshiper_service_list;
        $customer = $express->customer;
        $data['customer'] = array(
            'id' => $customer->id,
            'code' => $customer->code,
            'full_name' => $customer->full_name,
            'first_name' => $customer->first_name,
            'middle_name' => $customer->middle_name,
            'last_name' => $customer->last_name,
            'address_1' => $customer->address_1,
            'telephone' => $customer->telephone
        );
        $data['order'] = array(
            'customer_id' => $customer->id,
            'payment_method' => !empty($express->payment_method) ? $express->payment_method : 'CK',
            'order_id' => $express->id,
            'total_pay' => $express->total_final,
            'pay' => $express->total_paid_amount,
            'debt' => $express->total_final - $express->total_paid_amount,
            'total_weight' => $express->total_weight,
            'express_order_id' => $express->express_order_id,
            'eshiper_base_charge' => $express->eshiper_base_charge,
            'eshiper_fuel_surcharge' => $express->eshiper_fuel_surcharge,
            'eshiper_other_surcharge' => $express->eshiper_other_surcharge,
            'eshiper_total_charge' => $express->eshiper_total_charge,
            'order_status_name' => $express->status_name,
            'order_status' => $express->order_status,
            'eshiper_shipping_status' => $express->eshiper_shipping_status,
            'eshiper_status_name' => $express->eshipper_status_name,
            'payment_status' => $express->payment_status_name,
            'last_payment_at' => !empty($express->last_payment_at) ? date(config('app.date_format'), strtotime($express->last_payment_at)) : null,
            'date_pay' => null,
            'last_pay_amount' => 0,
            'discount_number' => !empty($express->per_discount) ? $express->per_discount : 0
        );
        return $data;
    }

}
