<?php

namespace Modules\Express\Http\Controllers;

use Auth;
use Exception;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
// use App\Components\TextUtil;
use App\Enum\EshiperEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\PointEnum;
use App\Order;
// use App\OrderItem;
use App\Province;
use App\Transaction;
use App\Country;
// use App\Currency;
use App\City;
use App\Customer;
use App\Notification;
use App\Services\EshiperService;
use App\Services\PointService;
use Modules\Express\Http\Requests\ExpressRequest;
use Modules\Express\Http\Requests\PaymentRequest;
use App\Exports\ExportQuery;
class CustomerExpressController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '5.2'
        ];
    }

    public function index(Request $request) {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::EXPRESS);
        $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('express::customer.index', $this->data);
    }
    public function exportExcel(Request $request)
    {
      $query = Order::where([]);
        $query->select('tbl_order.code','tbl_order.created_at', 'customer_note', 'user_note', 'description', 'sender_first_name',
                'sender_middle_name', 'sender_last_name', 'sender_phone', 'sender_cellphone', 'sender_address',
                'sender_post_code', 'receive_first_name', 'nuoc_gui.name as sender_countrie','tinh_gui.name as sender_province',
                'thanh_pho_gui.name as sender_city','receiver_middle_name', 'receive_last_name', 'receiver_email', 'receiver_phone',
                'receiver_cellphone', 'receiver_address', 'receiver_post_code','nuoc_nhan.name as receiver_countrie',
                'tinh_nhan.name as receiver_province','thanh_pho_nhan.name as receiver_city', 'is_crr', 'total_weight', 'total_final',
                'total_paid_amount', 'total_surcharge_fee', 'total_insurrance_fee','total_remain_amount', 'total_declare_price',
                'agency_discount', 'order_status', 'shipping_status','payment_status', 'receive_status');
                $query->where('type', '=', OrderTypeEnum::EXPRESS);
                $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_order.created_at', '>=', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_order.created_at', '<=', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id' );
        // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id' );
        // lấy thông tin tỉnh gửi
         $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id' );
         // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id' );
        // lấy thông tin thành phố gửi
         $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id' );
         // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id' );
        $query->orderBy('tbl_order.id', 'desc');
        return (new ExportQuery($query))->download('ex_customer.xlsx');
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        $express = Order::where('id', '=', $id)->whereNull('agency_id')->first();
        if (!$express) {
            return response('Not Found', 404);
        }

        $data = $this->convertExpress($express);
        return response([
            'express' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request) {
        $notification_id = $request->get('notification_id', 0);
        if($notification_id > 0){
            Notification::where('id', $notification_id)->update(['is_read' => 1]);
        }
        $express = Order::where('id', $request->id)->whereNull('agency_id')->first();
        if (!$express) {
            return redirect()->route('customer.express.list');
        }
        $this->data['express'] = $express;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('express::customer.edit', $this->data);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function cancelOrder(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response($validator->errors()->first(), 500);
        }
        $order = Order::find($request->get('id'));
        if (empty($order)) {
            return response(__('message.err_order_is_not_valid'), 500);
        }
        if ($order->eshiper_shipping_status == EshiperEnum::CANCELLED || $order->order_status == OrderStatusEnum::STATUS_CANCEL) {
            return response(__('message.order_no_allow_cancel'), 500);
        }
        if(!empty($order->express_order_id)){
            $eshiperCancelResult = EshiperService::cancelOrder($order->express_order_id);
            if ($eshiperCancelResult["success"] && isset($eshiperCancelResult['data']['orderStatus']) && $eshiperCancelResult['data']['orderStatus'] == EshiperEnum::CANCELLED) {
                try {
                    DB::beginTransaction();
                    $order->order_status = OrderStatusEnum::STATUS_CANCEL;
                    $order->eshiper_shipping_status = $eshiperCancelResult['data']['orderStatus'];
                    $order->update();
                    DB::commit();

                    return response('OK', 200)->header('Content-Type', 'text/plain');
                } catch (Exception $e) {
                    DB::rollBack();
                    return response($e->getMessage(), 500);
                }
            }
            return response($eshiperCancelResult['message'], 500);
        }else{
            try {
                DB::beginTransaction();
                $order->order_status = OrderStatusEnum::STATUS_CANCEL;
                $order->update();
                DB::commit();

                return response('OK', 200)->header('Content-Type', 'text/plain');
            } catch (Exception $e) {
                DB::rollBack();
                return response($e->getMessage(), 500);
            }
        }
    }

    public function approve(Request $request) {
        $param = $request->all();
        $order_id = $param['id'];
        $addressFrom = $param['addressFrom'];
        $eshiperService = $param['quote'];
        $eshiperServiceList = $param['quotes'];

        $order = Order::where('id', '=', $order_id)->whereNull('agency_id')->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();
            if (!empty($order->customer_id)) {
                $customer = Customer::where('id', $order->customer_id)->first();
                if (!empty($customer)) {
                    $customer->update([
                        'id_card' => $addressFrom['id_card'],
                        'card_expire' => $addressFrom['card_expire'],
                        'birthday' => $addressFrom['birthday'],
                        'career' => $addressFrom['career'],
                        'email' => $addressFrom['email'],
                        'image_1_file_id' => isset($addressFrom['image_1_file_id']) ? $addressFrom['image_1_file_id'] : null,
                        'image_2_file_id' => isset($addressFrom['image_2_file_id']) ? $addressFrom['image_2_file_id'] : null,
                        'image_3_file_id' => isset($addressFrom['image_3_file_id']) ? $addressFrom['image_3_file_id'] : null,
                    ]);
                }
            }
            $order->eshiper_service = $eshiperService;
            $order->eshiper_service_list = $eshiperServiceList;
            $order->eshiper_base_charge = $eshiperService['baseCharge'];
            $order->eshiper_fuel_surcharge = $eshiperService['fuelSurcharge'];
            $order->eshiper_total_charge = $eshiperService['totalCharge'];
            $order->eshiper_other_surcharge = $eshiperService['totalCharge'] - $eshiperService['baseCharge'] - $eshiperService['fuelSurcharge'];
            $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
            $order->total_discount = 0;
            if(isset($eshiperService['discount']) && (float) $eshiperService['discount'] > 0){
                $order->per_discount = (float) $eshiperService['discount'];
                $order->total_discount = ($order->eshiper_base_charge * $order->per_discount) / 100;
            }
            $order->total_final = $order->eshiper_total_charge - $order->total_discount;
            // if (!empty($order->eshiper_service)) {
            //     $params = $this->eshipBuildParams($order);
            //     $eshipRespose = EshiperService::getShippingEshiper($params);
            //     if ($eshipRespose['success'] && !empty($eshipRespose['data']['orderId'])) {
            //         $order->eshiper_shipping_status = !empty($shipment['data']['statusId']) ? $shipment['data']['statusId'] : EshiperEnum::READY_FOR_SHIPPING;
            //         $order->express_order_id = $eshipRespose['data']['orderId'];
            //         $order->express_tracking_url = $eshipRespose['data']['trackingUrl'];
            //         $order->express_order = $eshipRespose['data'];
            //         $order->eshipper_label = $eshipRespose['data']['label'];
            //         if(isset($eshipRespose['data']['customLabel']) && !empty($eshipRespose['data']['customLabel'])){
            //             $order->eshipper_custom_label = $eshipRespose['data']['customLabel'];
            //         }
            //     } else {
            //         return response(!empty($eshipRespose['message']) ? $eshipRespose['message'] : 'Not create order Carrier', 500);
            //     }
            // }
            $order->update();

            Mail::send('email.create-express-quote', ['order' => $order], function ($message) use ($order) {
                $message->to($order->sender_email)->subject(__('email.create-express-quote'));
            });
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request) {
        $param = $request->all();
        $data = $param['order'];
        try {
            DB::beginTransaction();
            $order = Order::where('id', $data['order_id'])->whereNull('agency_id')->first();
            if (!$order) {
                return response('Not found', 404);
            }
            if (!empty($order->express_order_id)) {
                $eshiperResponse = EshiperService::getOrder($order->express_order_id);
                if (!$eshiperResponse['success']) {
                    return response($eshiperResponse['message'], 500);
                }
                $order->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                if ($order->eshiper_shipping_status == EshiperEnum::DELIVERED) {
                    $order->eshiper_charge_order = json_encode($eshiperResponse['data']);
                    $order->total_discount = 0;
                    if($order->per_discount > 0){
                        $order->total_discount = ($eshiperResponse['data']['baseCharge'] * $order->per_discount) / 100;
                    }
                    $order->charge_total_final = $eshiperResponse['data']['totalCharge'];
                    $order->eshiper_total_discount = 0.4 * $eshiperResponse['data']['baseCharge'];
                    $order->agency_discount = $order->eshiper_total_discount;

                    // Tặng điểm
                    // PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);
                }
                if(isset($eshiperResponse['data']['customLabel']) && !empty($eshiperResponse['data']['customLabel'])){
                    $order->eshipper_custom_label = $eshiperResponse['data']['customLabel'];
                }
                $order->update();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response(
                'Ok'
        );
    }

    public function getQuotes(ExpressRequest $request) {
        $params = $this->buildParams($request->all());
        $result = EshiperService::getQuoteEshiper($params);
        if (!$result["success"]) {
            return response($result['message'], 500);
        }
        $list_carrier =[];
        if (!empty($params['addressFrom']['shipping_date'])) {
            foreach ($result['data'] as $key => $val) {
                $result['data'][$key]['receiver_date'] = ConvertsUtil::dateAddDay($params['addressFrom']['shipping_date'], $val['transitDays'], 'd/m/Y', 'd/m/Y');
                if($val['carrierName']!=="Canpar"){
                    array_push($list_carrier,$result['data'][$key]);
            }
            }
        } else {
            foreach ($result['data'] as $key => $val) {
                $result['data'][$key]['receiver_date'] = ConvertsUtil::dateAddDay(date('d/m/Y'), $val['transitDays'], 'd/m/Y', 'd/m/Y');
                if($val['carrierName']!=="Canpar"){
                    array_push($list_carrier,$result['data'][$key]);
            }
            }
        }
        $result['data']=$list_carrier;
        return response(
                $result
        );
    }

    private function buildParams($params) {
        $countries = Country::where("is_active", 1)->pluck('code', 'id');
        $provinces = Province::where("is_active", 1)->pluck('code', 'id');
        $cities = City::where("is_active", 1)->pluck('name', 'id');
        $params['typeAttrs'] = ['serviceId' => 0, 'stackable' => true];
        if((isset($params['services']['saturdayDelivery']) && $params['services']['saturdayDelivery']) || (isset($params['services']['saturdayPickup']) && $params['services']['saturdayPickup'])){
            $params['typeAttrs']['isSaturdayService'] = "true";
        }else{
            $params['typeAttrs']['isSaturdayService'] = "false";
        }
        if(isset($params['services']['holdForPickup']) && $params['services']['holdForPickup']){
            $params['typeAttrs']['holdForPickupRequired'] = "true";
        }else{
            $params['typeAttrs']['holdForPickupRequired'] = "false";
        }
        if(isset($params['services']['docsOnly']) && $params['services']['docsOnly']){
            $params['typeAttrs']['docsOnly'] = "true";
        }else{
            $params['typeAttrs']['docsOnly'] = "false";
        }
        if(isset($params['services']['dangerousGoods']) && !empty($params['services']['dangerousGoods'])){
            $params['typeAttrs']['dangerousGoodsType'] = $params['services']['dangerousGoods'];
        }
        if(isset($params['services']['signatureRequired']) && !empty($params['services']['signatureRequired'])){
            $params['typeAttrs']['signatureRequired'] = "Yes";
        }else{
            $params['typeAttrs']['signatureRequired'] = "No";
        }
        if(isset($params['services']['insuranceType']) && !empty($params['services']['insuranceType'])){
            $params['typeAttrs']['insuranceType'] = $params['services']['insuranceType'];
        }
        $params['addressFrom']['company'] = $params['addressFrom']['last_name'] . ' ' . $params['addressFrom']['middle_name'] . ' ' . $params['addressFrom']['first_name'];
        $params['addressFrom']['state'] = $provinces[$params['addressFrom']['province_id']];
        $params['addressFrom']['country'] = $countries[$params['addressFrom']['country_id']];
        $params['addressFrom']['city'] = $cities[$params['addressFrom']['city_id']];
        $params['addressTo']['company'] = $params['addressTo']['last_name'] . ' ' . $params['addressTo']['middle_name'] . ' ' . $params['addressTo']['first_name'];
        $params['addressTo']['state'] = $provinces[$params['addressTo']['province_id']];
        $params['addressTo']['country'] = $countries[$params['addressTo']['country_id']];
        $params['addressTo']['city'] = $cities[$params['addressTo']['city_id']];
        if (!empty($params['services']['codPaymentType'])) {
            $params['cod']['payment_type'] = EshiperEnum::COD_PAYMENT_TYPE[$params['services']['codPaymentType']];
            $params['cod']['cod_data'] = $params['addressTo'];
        }
        if (isset($params['pickup']['is_schedule']) && $params['pickup']['is_schedule'] == EshiperEnum::IS_PICKUP) {
            $params['pickup']['pickup_date'] = ConvertsUtil::strToDateFormat($params['pickup']['date_time']);
            $params['pickup']['pickup_time'] = $params['pickup']['start_hour_time'] . ':' . $params['pickup']['start_minute_time'];
            $params['pickup']['closing_time'] = $params['pickup']['closing_hour_time'] . ':' . $params['pickup']['closing_minute_time'];
        }
        return $params;
    }

    public function payment(PaymentRequest $request) {
        $param = $request->all();
        $data = $param['order'];
        $order = Order::where('id', $data['order_id'])->whereNull('agency_id')->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();

            if ($data['last_pay_amount'] > 0 && !empty($data['date_pay'])) {
                $count = Transaction::where('order_id', $order->id)->count();

                $transaction = new Transaction();
                $transaction->customer_id = $order->customer_id;
                $transaction->order_id = $order->id;
                $transaction->currency_id = $order->currency_id;
                $transaction->code = "PT_" . $order->code . "_{$count}";
                $transaction->transaction_at = ConvertsUtil::dateFormat($data['date_pay'], config('app.date_format'));
                $transaction->type = TransactionTypeEnum::RECEIPT;
                $transaction->credit_amount = $data['last_pay_amount'];
                $transaction->goods_code = $order->code;
                $transaction->save();

                $order->payment_method = $data['payment_method'];
                $order->total_paid_amount += $data['last_pay_amount'];
                $order->total_remain_amount = $order->charge_total_final - $order->total_paid_amount > 0 ? $order->charge_total_final - $order->total_paid_amount : 0;
                if ($order->total_remain_amount == 0) {
                    $order->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                } else {
                    $order->payment_status = OrderPaymentStatusEnum::PART_PAID;
                }
                $order->last_payment_at = ConvertsUtil::dateFormat($data['date_pay'], config('app.date_format'));

                if (!empty($order->eshiper_service)) {
                    $param['reference']['code']=$order->code;
                    $param['reference']['name']=null;
                    $params = $this->eshipBuildParams($order);
                    $eshipRespose = EshiperService::getShippingEshiper($params);
                    if ($eshipRespose['success'] && !empty($eshipRespose['data']['orderId'])) {
                        $order->eshiper_shipping_status = !empty($eshipRespose['data']['statusId']) ? $eshipRespose['data']['statusId'] : EshiperEnum::READY_FOR_SHIPPING;
                        $order->express_order_id = $eshipRespose['data']['orderId'];
                        $order->express_tracking_url = $eshipRespose['data']['trackingUrl'];
                        $order->pickupNumber = $eshipRespose['data']['pickupNumber'];
                        $order->express_order = $eshipRespose['data'];
                        $order->eshipper_label = $eshipRespose['data']['label'];
                        if(isset($eshipRespose['data']['customLabel']) && !empty($eshipRespose['data']['customLabel'])){
                            $order->eshipper_custom_label = $eshipRespose['data']['customLabel'];
                        }
                    } else {
                        return response(!empty($eshipRespose['message']) ? $eshipRespose['message'] : 'Not create order Carrier', 500);
                    }
                }

                $order->update();

                PointService::addPointToCustomer($order->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);

                PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);

                Mail::send('email.payment-express-quote', ['order' => $order, 'amount' => $data['last_pay_amount'], 'last_payment_at' => $data['date_pay']], function ($message) use ($order) {
                    $message->to($order->sender_email, $order->sender_email)->subject(__('email.payment-express-quote'));
                });
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function getPdf($id) {
        $order = Order::where('id', '=', $id)->first();
        if (empty($order) || empty($order->eshipper_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $order->eshipper_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    public function getCustomInvoicePdf($id) {
        $order = Order::where('id', '=', $id)->first();
        if (empty($order) || empty($order->eshipper_custom_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $order->eshipper_custom_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    public function viewHistoryEshipper(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $order = Order::find($request->get('id'));
        if(empty($order)){
            return response(["success" => false, "message" => __('message.err_sys'), []]);
        }
        if(!empty($order->express_order_id)){
            $eshiperResponse = EshiperService::getOrder($order->express_order_id);
            if($eshiperResponse['success']){
                $order->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                if ($order->eshiper_shipping_status == EshiperEnum::DELIVERED) {
                    $order->eshiper_charge_order = json_encode($eshiperResponse['data']);
                    $order->total_discount = 0;
                    if($order->per_discount > 0){
                        $order->total_discount = ($eshiperResponse['data']['baseCharge'] * $order->per_discount) / 100;
                    }
                    $order->charge_total_final = $eshiperResponse['data']['totalCharge'];
                    $order->eshiper_total_discount = 0.4 * $eshiperResponse['data']['baseCharge'];
                    $order->agency_discount = $order->eshiper_total_discount;

                    // Tặng điểm
                    // PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);
                }
                if(isset($eshiperResponse['data']['customLabel']) && !empty($eshiperResponse['data']['customLabel'])){
                    $order->eshipper_custom_label = $eshiperResponse['data']['customLabel'];
                }
                $order->update();
            }else{
                return response(["success" => false, "message" => $eshiperResponse['message'], []]);
            }
            return response(["success" => true, "message" => __('message.get_data_success'), "data" => $eshiperResponse['data']['history']]);
        }
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => []]);
    }

    private function eshipBuildParams(Order $order) {
        $countries = Country::where("is_active", 1)->pluck('code', 'id');
        $provinces = Province::where("is_active", 1)->pluck('name', 'id');
        $cities = City::where("is_active", 1)->pluck('name', 'id');
        $params['countries'] = $countries;
        $params['provinces'] = $provinces;
        $params['cities'] = $cities;
        $quote = $order->eshiper_service;
        $services = $order->services;

        $params['typeAttrs']['serviceId'] = $quote['serviceId'];
        if(!empty($order->shipping_date)){
            $params['typeAttrs']['scheduledShipDate'] = ConvertsUtil::strToDateFormat($order->shipping_date, 'Y-m-d', 'Y-m-d');
        }
        $params['typeAttrs']['stackable'] = true;

        $params['typeAttrs']['serviceId'] = $quote['serviceId'];
        $params['typeAttrs']['stackable'] = true;
        $params['addressFrom']['company'] = $order->sender_full_name;
        $params['addressFrom']['address_1'] = $order->sender_address;
        $params['addressFrom']['city'] = isset($cities[$order->sender_city_id]) ? $cities[$order->sender_city_id] : '';
        $params['addressFrom']['state'] = isset($provinces[$order->sender_province_id]) ? $provinces[$order->sender_province_id] : '';
        $params['addressFrom']['postal_code'] = $order->sender_post_code;
        $params['addressFrom']['country'] = isset($countries[$order->sender_country_id]) ? $countries[$order->sender_country_id] : '';
        $params['addressFrom']['telephone'] = $order->sender_phone;
        $params['addressFrom']['attention'] = $order->sender_attention;
        $params['addressFrom']['email'] = $order->sender_email;
        $params['addressFrom']['residential'] = $order->sender_residential;
        $params['addressFrom']['confirm_delivery'] = $order->sender_confirm_delivery;

        $params['addressTo']['company'] = $order->receive_full_name;
        $params['addressTo']['address_1'] = $order->receiver_address;
        $params['addressTo']['address_2'] = $order->receiver_address_2;
        $params['addressTo']['city'] = isset($cities[$order->receiver_city_id]) ? $cities[$order->receiver_city_id] : '';
        $params['addressTo']['state'] = isset($provinces[$order->receiver_province_id]) ? $provinces[$order->receiver_province_id] : '';
        $params['addressTo']['postal_code'] = $order->receiver_post_code;
        $params['addressTo']['country'] = isset($countries[$order->receiver_country_id]) ? $countries[$order->receiver_country_id] : '';
        $params['addressTo']['telephone'] = $order->receiver_phone;
        $params['addressTo']['attention'] = $order->receiver_attention;
        $params['addressTo']['email'] = $order->receiver_email;
        $params['addressTo']['notify_recipient'] = $order->receiver_notify_recipient;
        $params['addressTo']['residential'] = $order->receiver_residential;
        $params['paymentType'] = '3rd Party';
        $boxes = [];
        $order_items = $order->order_items()->where('is_delete', 0)->get();
        if ($order_items) {
            foreach ($order_items as $item) {
                $box['length'] = $item->length;
                $box['width'] = $item->width;
                $box['height'] = $item->height;
                $box['weight'] = $item->weight;
                $box['insuranceAmount'] = $item->sub_total_insurrance_fee;
                $box['freightClass'] = $item->freight_class;
                $box['nmfcCode'] = $item->nmfc_code;
                $box['description'] = $item->description;
                $boxes[] = $box;
            }
        }
        $params['packages']['dimType'] = $order->dimension_unit_id;
        $params['packages']['type'] = $order->package_type;
        $params['packages']['quantity'] = $order->package_quantity;
        $params['packages']['referenceCode'] = $order->ref_code;
        $params['packages']['boxes'] = $boxes;
        if (!empty($services['codPaymentType'])) {
            $params['cod']['payment_type'] = EshiperEnum::COD_PAYMENT_TYPE[$services['codPaymentType']];
            $params['cod']['cod_data'] = $params['addressTo'];
        }
        $pickup = json_decode($order->pickup, true);
        if(isset($pickup['is_schedule']) && $pickup['is_schedule'] == EshiperEnum::IS_PICKUP){
            $params['pickup'] = $pickup;
            $params['pickup']['pickup_date'] = ConvertsUtil::strToDateFormat($pickup['date_time']);
            $params['pickup']['pickup_time'] = $pickup['start_hour_time'] .':'. $pickup['start_minute_time'];
            $params['pickup']['closing_time'] = $pickup['closing_hour_time'] .':'. $pickup['closing_minute_time'];
        }
        $params['custom'] = json_decode($order->eshipper_custom, true);
        return $params;
    }

    private function convertExpress($express) {
        $data = [];
        $data['addressFrom'] = array(
            'id' => $express->sender_address_id,
            'first_name' => $express->sender_first_name,
            'middle_name' => $express->sender_middle_name,
            'last_name' => $express->sender_last_name,
            'email' => $express->sender_email,
            'telephone' => $express->sender_phone,
            'address_1' => $express->sender_address,
            'country_id' => $express->sender_country_id,
            'province_id' => $express->sender_province_id,
            'city_id' => $express->sender_city_id,
            'postal_code' => $express->sender_post_code,
            'attention' => $express->sender_attention,
            'instruction' => $express->sender_instruction,
            'address_2' => $express->sender_address_2,
            'shipping_date' => !empty($express->shipping_date) ? date(config('app.date_format'), strtotime($express->shipping_date)) : '',
            'confirm_delivery' => $express->sender_confirm_delivery ? true : false,
            'residential' => $express->sender_residential ? true : false,
            'id_card' => $express->customer->id_card,
            'card_expire' => $express->customer->card_expire,
            'birthday' => $express->customer->birthday,
            'career' => $express->customer->career,
            'image1' => $express->customer->image1,
            'image2' => $express->customer->image2,
            'image3' => $express->customer->image3,
            'image_1_file_id' => $express->customer->image_1_file_id,
            'image_2_file_id' => $express->customer->image_2_file_id,
            'image_3_file_ids' => $express->customer->image_3_file_id
        );
        $data['addressTo'] = array(
            'id' => $express->shipping_address_id,
            'first_name' => $express->receive_first_name,
            'middle_name' => $express->receiver_middle_name,
            'last_name' => $express->receive_last_name,
            'email' => $express->receiver_email,
            'telephone' => $express->receiver_phone,
            'address_1' => $express->receiver_address,
            'country_id' => $express->receiver_country_id,
            'province_id' => $express->receiver_province_id,
            'city_id' => $express->receiver_city_id,
            'postal_code' => $express->receiver_post_code,
            'attention' => $express->receiver_attention,
            'instruction' => $express->receiver_instruction,
            'address_2' => $express->receiver_address_2,
            'notify_recipient' => $express->receiver_notify_recipient ? true : false,
            'residential' => $express->receiver_residential ? true : false,
        );
        $data['pickup'] = json_decode($express->pickup, true);
        $data['custom'] = json_decode($express->eshipper_custom, true);
        $data['outside_canada'] = false;
        if(!empty($data['custom'])){
            $data['outside_canada'] = true;
        }
        $boxes = [];
        $order_items = $express->order_items()->where('is_delete', 0)->get();
        foreach ($order_items as $order_item) {
            $boxes[] = array(
                'id' => $order_item->id,
                'weight' => $order_item->weight,
                'description' => $order_item->description,
                'length' => $order_item->length,
                'width' => $order_item->width,
                'height' => $order_item->height,
                'insuranceAmount' => $order_item->sub_total_insurrance_fee,
                'codValue' => $order_item->cod_fee,
                'freightClass' => $order_item->freight_class,
                'nmfcCode' => $order_item->nmfc_code
            );
        }
        $data['packages'] = array(
            'dimType' => $express->dimension_unit_id,
            'type' => $express->package_type,
            'quantity' => $express->package_quantity,
            'referenceCode' => $express->ref_code,
            'boxes' => $boxes,
        );
        $data['services'] = $express->addtional_service;
        $data['quote'] = $express->eshiper_service;
        if(!isset($data['quote']['totalCharge'])){
            $data['quote']['totalCharge'] = 0;
        }
        $data['quotes'] = $express->eshiper_service_list;
        $customer = $express->customer;
        $data['customer'] = array(
            'id' => $customer->id,
            'code' => $customer->code,
            'full_name' => $customer->full_name,
            'first_name' => $customer->first_name,
            'middle_name' => $customer->middle_name,
            'last_name' => $customer->last_name,
            'address_1' => $customer->address_1,
            'telephone' => $customer->telephone
        );
        $data['order'] = array(
            'customer_id' => $customer->id,
            'payment_method' => !empty($express->payment_method) ? $express->payment_method : 'CK',
            'order_id' => $express->id,
            $total_pay = !empty($express->total_final) ? $express->total_final : $data['quote']['totalCharge'],
            'total_pay'=>$total_pay,
            'pay' => $express->total_paid_amount,
            'total_weight' => $express->total_weight,
            'express_order_id' => $express->express_order_id,
            'eshiper_base_charge' => $express->eshiper_base_charge,
            'eshiper_fuel_surcharge' => $express->eshiper_fuel_surcharge,
            'eshiper_other_surcharge' => $express->eshiper_other_surcharge,
            'eshiper_total_charge' => $express->eshiper_total_charge,
            'order_status_name' => $express->status_name,
            'order_status' => $express->order_status,
            'eshiper_shipping_status' => $express->eshipper_status_name,
            'payment_status' => $express->payment_status_name,
            'last_payment_at' => !empty($express->last_payment_at) ? date(config('app.date_format'), strtotime($express->last_payment_at)) : null,
            'date_pay' => null,
            'last_pay_amount' => 0,
            'charge_total_final' => $express->charge_total_final,
            //'discount_number' => !empty($express->per_discount) ? $express->per_discount : 0
            $discount_number = !empty($express->total_discount) ? $express->total_discount : 0  ,
            'discount_number'=>$discount_number,
            'total_pay'=>$total_pay - (($discount_number*$total_pay)/100),
        );
        $data['order']['debt'] = $data['order']['total_pay'] - $data['order']['pay'];
        $data['order']['last_pay_amount'] = $data['order']['debt'];
        return $data;
    }

}
