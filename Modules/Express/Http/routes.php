<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => ['web','auth'], 'prefix' => 'express', 'namespace' => 'Modules\Express\Http\Controllers'], function()
{
    Route::get('/create', 'ExpressController@create')->name('express.create');
    Route::post('/get-quote', 'ExpressController@getQuotes');
    Route::post('/store', 'ExpressController@store');
    Route::post('/update', 'ExpressController@update');
    Route::get('/list', 'ExpressController@index')->name('express.list');
    Route::post('/cancel-order', 'ExpressController@cancelOrder')->name('express.cancel');
    Route::get('/{id}/edit', 'ExpressController@edit')->name('express.edit');
    Route::get('/{id}', 'ExpressController@show')->name('express.show');
    Route::get('/{id}/get-pdf', 'ExpressController@getPdf')->name('express.pdf');
    Route::get('/{id}/get-custom-invoice-pdf', 'ExpressController@getCustomInvoicePdf')->name('express.custominvoicepdf');
    Route::get('/{id}/view-history', 'ExpressController@viewHistoryEshipper');
});
Route::group(['middleware' => ['web','auth:admin', 'can:manage-express-agency'], 'prefix' => 'admin/express', 'namespace' => 'Modules\Express\Http\Controllers'], function()
{
    Route::get('/create', 'AdminExpressController@create')->name('admin.express.create');
    Route::post('/get-quote', 'AdminExpressController@getQuotes');
    Route::post('/store', 'AdminExpressController@store');
    Route::post('/update', 'AdminExpressController@update');
    Route::post('/payment', 'AdminExpressController@payment');
    Route::get('/list', 'AdminExpressController@index')->name('admin.express.list');
    Route::get('/export', 'AdminExpressController@exportExcel')->name('admin.express.export');
    Route::post('/cancel-order', 'AdminExpressController@cancelOrder')->name('admin.express.cancel');
    Route::get('/{id}/edit', 'AdminExpressController@edit')->name('admin.express.edit');
    Route::post('/storeupdate', 'AdminExpressController@storeupdate')->name('admin.express.storeupdate');
    Route::get('/{id}', 'AdminExpressController@show')->name('admin.express.show');
    Route::get('/{id}/get-pdf', 'AdminExpressController@getPdf')->name('admin.express.pdf');
    Route::get('/{id}/get-custom-invoice-pdf', 'AdminExpressController@getCustomInvoicePdf')->name('admin.express.custominvoicepdf');
    Route::post('/view-history-eshipper', 'AdminExpressController@viewHistoryEshipper');
    Route::get('/{id}/print_bill', 'AdminExpressController@printBill')->name('admin.express.print');
});
Route::group(['middleware' => ['web','auth:admin', 'can:manage-express-customer'], 'prefix' => 'admin/customer/express', 'namespace' => 'Modules\Express\Http\Controllers'], function()
{
    Route::post('/get-quote', 'CustomerExpressController@getQuotes');
    Route::post('/store', 'CustomerExpressController@store');
    Route::post('/approve', 'CustomerExpressController@approve');
    Route::post('/update', 'CustomerExpressController@update');
    Route::post('/payment', 'CustomerExpressController@payment');
    Route::get('/list', 'CustomerExpressController@index')->name('customer.express.list');
    Route::get('/', 'CustomerExpressController@index')->name('customer.express.index');
    Route::get('/export', 'CustomerExpressController@exportExcel')->name('admin.customer.express.export');
    Route::post('/cancel-order', 'CustomerExpressController@cancelOrder')->name('customer.express.cancel');
    Route::get('/{id}/edit', 'CustomerExpressController@edit')->name('customer.express.edit');
    Route::get('/{id}', 'CustomerExpressController@show')->name('customer.express.show');
    Route::get('/{id}/get-pdf', 'CustomerExpressController@getPdf')->name('customer.express.pdf');
    Route::get('/{id}/get-custom-invoice-pdf', 'CustomerExpressController@getCustomInvoicePdf')->name('customer.express.custominvoicepdf');
    Route::post('/view-history-eshipper', 'CustomerExpressController@viewHistoryEshipper');
});
