<?php

namespace Modules\Yhl\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstockCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id'=>'required|string',
            'products.*.quantity' => 'required|numeric|min:1',
            //
        ];
    }
    public function attributes() {
        return[
            'warehouse_id' => __('label.warehouse_id'),
        ];
    }
}
