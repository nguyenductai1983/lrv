<?php

namespace Modules\Yhl\Http\Controllers;

use App\Import_good_details;
use Illuminate\Http\Request;

class ImportGoodDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function __construct() {
        $this->data = [
            'menu' => '6.8'
        ];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Import_good_details  $import_good_details
     * @return \Illuminate\Http\Response
     */
    public function show(Import_good_details $import_good_details)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Import_good_details  $import_good_details
     * @return \Illuminate\Http\Response
     */
    public function edit(Import_good_details $import_good_details)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Import_good_details  $import_good_details
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Import_good_details $import_good_details)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Import_good_details  $import_good_details
     * @return \Illuminate\Http\Response
     */
    public function destroy(Import_good_details $import_good_details)
    {
        //
    }
}
