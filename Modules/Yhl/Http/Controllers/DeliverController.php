<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Components\VNPostClient;
use App\Components\LanguageUtil;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\ShippingMethodProviderEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
use App\ShippingPackageLocal;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use App\Services\giaohangtietkiem;
use Exception;
use App\Components\ConvertsUtil;
use App\ListShippingPackage;
use App\Picksession;
class DeliverController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '6.4'
        ];
    }
    public function listghtk(Request $request)
    {
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $query = ListShippingPackage::where([]);

        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        $query->where('type', '=', OrderTypeEnum::YHL);
        $query->where('shipping_provider_id', '=', ShippingMethodProviderEnum::SHIPPING_PROVIDER_GHTK);
        $this->data['list_shipping_package'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::deliver.giaohangtietkiem', $this->data);
    }
    public function printlistghtk($id)
    {
        $columns = [
            'tbl_order.id',
            'tbl_order.code',
            'tbl_shipping_package_local.tracking_code',
            'tbl_shipping_package_local.carrier_fee',
            'tbl_order.receive_last_name',
            'tbl_order.receiver_middle_name',
            'tbl_order.receive_first_name',
            'tbl_order.receiver_address',
            'tbl_order.receiver_ward_name',
            'tbl_order.receiver_city_name',
            'tbl_order.receiver_province_name',
            'tbl_order.receiver_phone',
        ];
        $ListShippingPackage = ListShippingPackage::find($id);
        if (empty($ListShippingPackage)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $query = Order::where([]);
        $query->leftJoin('tbl_shipping_package_local', 'tbl_order.id', '=', 'tbl_shipping_package_local.order_id');
        $orders = $query->where('tbl_shipping_package_local.list_id', '=', $ListShippingPackage->id)->select($columns)->get();
        $this->data['ListShippingPackage'] = $ListShippingPackage;
        $this->data['orders'] = $orders;
        return view('yhl::deliver.ghtkprints', $this->data);
    }
    public function editlistghtk($id)
    {
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $ListShippingPackage = ListShippingPackage::find($id);
        if (empty($ListShippingPackage)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }

        $query = ShippingPackageLocal::where([]);
        $query->where('list_id', '=', $ListShippingPackage->id);
        // $query->where('status', '=', ShippingPackageLocalEnum::APPROVED);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_ONLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        $this->data['Picksessions'] = Picksession::get();
        $this->data['list_shipping_package'] = $ListShippingPackage;
        $this->data['messeger']='';
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::deliver.ghtkedit', $this->data);
    }
    public function addtolistghtk(Request $request)
    {
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $ListShippingPackage = $request->input('list_shipping_packages');

        if (empty($ListShippingPackage)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::YHL);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        if (!empty($request->input('order_code'))) {
            $query->where('code', '=', $request->input('order_code'));
        }
            $query->where('shipping_status', '=', OrderShippingStatusEnum::SHIPPING_STATUS_REVICER);
        $order = $query->get();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        $shippingMethodProvider = ShippingMethodProviderEnum::GHTK;
        $pick_session = $request->input('pick_session');
        CarrierController::addgiaohangtietkiem($order, $shippingMethodProvider, $user, $pick_session, $ListShippingPackage);
         return redirect()->route('yhl.deliver.editlistghtk', [$ListShippingPackage]);
    }
    public function approved(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::APPROVED);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_ONLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::deliver.approved', $this->data);
    }
    public function cancel(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::CANCEL);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_ONLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }

        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::deliver.cancel', $this->data);
    }
    public function getpdfghtk($id)
    {
        $shipping_package_local = ShippingPackageLocal::find($id);
        $response = giaohangtietkiem::getLabel($shipping_package_local->tracking_code);
        $name = $shipping_package_local->order_code;
        return (new Response($response, 200))->header('ContentType', 'application/pdf')
            ->header('Content-Transfer-Encoding', 'binary')
            ->header('Content-Disposition', 'attachment; filename=' . $name . '_GHTK.pdf');
    }
    public function carrierPickup(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::DELIVERY_CARRIER);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_ONLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::deliver.carrierpickup', $this->data);
    }

    public function done(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_ONLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }

        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('yhl::deliver.done', $this->data);
    }

    public function cancelRequest(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                if (!empty($shippingPackageLocal->tracking_code) && ($shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_FAST || $shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_SLOW)) {
                    $response = VNPostClient::cancelOrder($shippingPackageLocal->shipping_code);
                    if(empty($response->CancelOrderResult)){
                        DB::rollBack();
                        return response(["success" => false, "message" => $response->sE, "data" => []]);
                    }
                }
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CANCEL;
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                $orderData->update();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_REVICER;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_req_delivery_success'), "data" => ['refUrl' => route('yhl.deliver.cancel')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function pickup(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                $shippingPackageLocal->status = ShippingPackageLocalEnum::DELIVERY_CARRIER;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
                $orderTracking->order_code = $shippingPackageLocal->order_code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_CARRIER;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_CARRIER;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_pickup_success'), "data" => ['refUrl' => route('yhl.deliver.carrierpickup')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function customerReceiver(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
                $orderData->order_status = OrderStatusEnum::STATUS_COMPLETE;
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
                $orderTracking->order_code = $shippingPackageLocal->order_code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_DONE;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_DONE;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_delivery_success'), "data" => ['refUrl' => route('yhl.deliver.done')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
