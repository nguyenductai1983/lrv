<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
use App\Enum\WarehouseEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Enum\OrderStatusEnum;
use App\Order;
use App\Warehouse;
use App\Product;
use App\OrderTracking;
use App\OrderLog;
use Exception;
use App\WarehousePackage;
use App\Enum\WarehousePackageEnum;
use League\CommonMark\Inline\Element\Code;

class StockoutController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '6.2'
        ];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function index(Request $request) {
        $user = Auth::user();

        $columns = [
            'tbl_warehouse_package.id',
            'tbl_warehouse_package.code',
            'tbl_warehouse_package.created_at',
            'tbl_warehouse_package.stockin_at',
            'tbl_warehouse_package.status',
            'users.first_name',
            'users.middle_name',
            'users.last_name'
        ];
        $query = WarehousePackage::join('users', 'tbl_warehouse_package.create_user_id', '=', 'users.id');
        if(!$user->role->admin){
            $query->where('tbl_warehouse_package.create_user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_warehouse_package.stockin_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_warehouse_package.stockin_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('status'))) {
            $query->where('tbl_warehouse_package.status', '=', $request->query('status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_warehouse_package.code', 'like', '%'.$request->query('code').'%');
        }
        $query->where('tbl_warehouse_package.type', '=', OrderTypeEnum::YHL);
        $this->data['warehousePackage'] = $query->orderBy('tbl_warehouse_package.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::stockout.index', $this->data);
    }
    public function print($id)
    {
        $stockout = WarehousePackage::find($id);
        if (empty($stockout)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $orders = Order::where('warehouse_packge_id', '=', $stockout->id)->get();
       $this->data['stockout'] = $stockout;
       $this->data['orders'] = $orders;
        return view('yhl::stockout.prints', $this->data);
    }
    public function create(Request $request) {
        $user = Auth::user();
        if(strtoupper($user->role->name) == "ADMIN"){
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_LOCAL, 'is_deleted' => 0])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_LOCAL, 'id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::YHL);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('code', '=', $request->query('order_code'));
        }
        $query->where('shipping_status', '<=', OrderShippingStatusEnum::SHIPPING_STATUS_REVICER);
        $query->where('payment_status', '<=', OrderShippingStatusEnum::SHIPPING_STATUS_REVICER);
        $this->data['orders'] = $query->orderBy('id', 'desc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['status_message']=0;
        return view('yhl::stockout.create', $this->data);
    }
    public function view($id) {
        $this->data['warehousePackage'] = WarehousePackage::find($id);
        if(empty($this->data['warehousePackage']) || $this->data['warehousePackage']->status == WarehousePackageEnum::STATUS_CANCEL){
            return redirect()->route('yhl.stockout.index');
        }
        $user = Auth::user();
        if($user->role->admin) {
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'is_deleted' => 0])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        $this->data['orders'] = Order::where([['warehouse_packge_id', '=', $id]])->orderBy('id', 'desc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::stockout.view', $this->data);
    }
public function update_product()
{
    $warehouse=Warehouse::find(1);
    $stockin_product_warehouse=$warehouse->product()->where('product_id', 7)->get();
    $stockin_product_warehouse=$stockin_product_warehouse->amount-1;
    $warehouse->product()->syncWithoutDetaching ([8=>['amount' => 19]]);
    // $warehouse->product()->save(6,['amount' => 15]);
    // $warehouse->product()->attach(7);
    // $warehouse->product()->updateExistingPivot(5,['amount' => 20]);


}
    public function pushItems(Request $request) {
        $validator = Validator::make($request->all(), [
            'from_warehouse_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $orderIds = $request->get('order_ids');
        if (empty($orderIds)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        try {
            DB::beginTransaction();
            $warehouse_id=$request->get('from_warehouse_id');
            $warehouse=Warehouse::find($warehouse_id);

            $warehousePackage = new WarehousePackage();
            $warehousePackage->create_user_id = $user->id;
            $warehousePackage->from_warehouse_id = $warehouse->id;
            $warehousePackage->type = OrderTypeEnum::YHL;
            $strdate = date('md');
            $count_stockin= WarehousePackage::where('create_user_id', $user->id)->where('type',OrderTypeEnum::YHL) ->count() +1;
            $code = "Y".$user->code. $warehouse->code. $strdate . sprintf('%03d', $count_stockin) ;
            $warehousePackage->code =  $code ;
            $warehousePackage->save();

            $messge="";
            foreach ($orderData as $order) {
                $order->from_warehouse_id = $warehouse_id;
                $order->warehouse_packge_id = $warehousePackage->id;
                $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
                $order->order_status = OrderStatusEnum::STATUS_PROCESSING;

                $order->update();
                $list_items=$order->order_items()->get();

                foreach($list_items as $item)
                {
                    $product=Product::find($item->product_id);
                    $product_instock=$product->stockinwarehouse($warehouse_id);

                    $product->decreaseStock($item->quantity,[
                        'description' => $order->code,
                        'reference' => $order,'warehouse' => $warehouse,]);
                        if($product_instock < $item->quantity)
                        {
                            $messge="<p style='color:red; font-size:12pt'>".$order->code."->".$product->name."(".$product->code.") in ".$warehouse->name."  stockin = ".$product_instock."</p>";
                        }
                        else if ($product_instock<20)
                        {
                            $messge="<p style='color:DarkOrange; font-size:12pt'>".$order->code."->".$product->name." in ".$warehouse->name."  stockin = ".$product_instock."</p>";
                        }
                }
                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $order->code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_READY_TO_SHIP;

                $orderLog->save();
            }

            DB::commit();
            $mess=__('message.stockout_success').$messge;
            // return response()->json(['status_message' => $warehousePackage->id]);
            //return response($warehousePackage->id);
            return response(["success" => true, "message" => $mess, "data" => ['refUrl' => route('yhl.stockout.view',$warehousePackage->id)]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
