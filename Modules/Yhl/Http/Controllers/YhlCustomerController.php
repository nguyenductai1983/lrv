<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Enum\OrderTypeEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\PointEnum;
use App\Services\PointService;
//use App\Services\CampaignService;
use App\Order;
use App\OrderItem;
use App\Config;
use App\Transaction;
use App\Product;
//use App\CampaignLog;
use App\Notification;
use App\Address;
use App\Customer;
use App\Ward;
use App\Exports\ExportQuery;
use Exception;

class YhlCustomerController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    public function __construct()
    {
        $this->data = [
            'menu' => '6.7'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $query->whereNull('agency_id');
        $query->where('type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('code'))&& strlen($request->query('code'))>6)
        {
            $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        else
        {
            if(!empty($request->query('code')))
            {
            $query->where('code', 'like', '%'.$request->query('code').'%');
            }
            if (!empty($request->query('from_date'))) {
                $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
            }
            if (!empty($request->query('to_date'))) {
                $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
            }
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::customer.index', $this->data);
    }

     public function exportExcel(Request $request)
    {
        $query = Order::where([]);
        $query->select('tbl_order.id','tbl_order.code', 'tbl_order.created_at','customer_note', 'user_note', 'description','package_quantity','total_weight','total_final','agency_discount',
            'total_paid_amount', 'total_remain_amount', 'payment_status',
            DB::raw("CONCAT(sender_first_name,sender_middle_name,sender_last_name) as sender_full_name"),
                'nuoc_gui.name as sender_countrie','tinh_gui.name as sender_province','thanh_pho_gui.name as sender_city', 'sender_address',
               'sender_phone', 'sender_cellphone', 'sender_email', 'sender_post_code',
                DB::raw("CONCAT(receive_first_name,receiver_middle_name,receive_last_name) as receive_full_name"),
                'nuoc_nhan.name as receiver_countrie', 'tinh_nhan.name as receiver_province','thanh_pho_nhan.name as receiver_city', 'receiver_address',
                 'receiver_phone', 'receiver_cellphone', 'receiver_email',
                 'receiver_post_code', 'is_crr',
                'order_status', 'shipping_status',  'receive_status',
                'sender_first_name', 'sender_middle_name', 'sender_last_name',
                'receive_first_name', 'receiver_middle_name', 'receive_last_name'
            );
        // lấy thông tin nước gửiprovinces
        $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id' );
        // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id' );
        // lấy thông tin tỉnh gửi
         $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id' );
         // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id' );
        // lấy thông tin thành phố gửi
         $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id' );
         // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id' );
        $query->whereNull('agency_id');
        $query->where('type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_order.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_order.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('shipping_status', '=', $request->query('shipping_status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('sender_phone', '=', $request->query('sender_phone'));
        }
      $query->orderBy('tbl_order.id', 'desc')->get()->toArray();
        return (new ExportQuery($query))->download('yhl_customer.xlsx');
    }

    public function printBill($id){
        $order = Order::find($id);
        if (empty($order)) {
            return redirect()->route('yhl.home.index');
        }
        $this->data['order'] = $order;
        return view('yhl::customer.print', $this->data);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $order = Order::where('id', '=', $id)->first();

        $order->last_payment_at = $order->last_payment_date;
        $order->last_paid_amount = 0;
        $order->last_date_pay = null;
        if (!$order) {
            return response('Not Found', 404);
        }

        return response([
            'transport' => $this->convertYhl($order)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        $notification_id = $request->get('notification_id', 0);
        if($notification_id > 0){
            Notification::where('id', $notification_id)->update(['is_read' => 1]);
        }
        $order = Order::where('id', '=', $request->id)->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        $this->data['order'] = $order;
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));

        return view('yhl::customer.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            // cập nhật người nhận
                if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                    if(empty($customer)){
                        return response('Internal Server Error', 500);
                    }
                    else
                    {
                        // kiểm tra tín hiệu cập nhật người gửi
                        if (isset($data['Updatecustomer']) && !($data['Updatecustomer'])) {
                         $customer->update([
                        'first_name'  => $data['customer']['first_name'],
                        'middle_name'  => $data['customer']['middle_name'],
                        'last_name'  => $data['customer']['last_name'],
                        'address_1'  => $data['customer']['address_1'],
                        'address_2'  => $data['customer']['address_2'],
                        'telephone'  => $data['customer']['telephone'],
                        'cellphone'  => $data['customer']['cellphone'],
                        'postal_code'  => $data['customer']['postal_code'],
                        'city_id'  => $data['customer']['city_id'],
                        'province_id'  => $data['customer']['province_id'],
                        'country_id' => $data['customer']['country_id'],
                        // 'id_card' => $data['customer']['id_card'],
                        // 'card_expire' => $data['customer']['card_expire'],
                        // 'birthday' => $data['customer']['birthday'],
                        // 'career' => $data['customer']['career'],
                        // 'image_1_file_id' => $data['customer']['image_1_file_id'],
                        // 'image_2_file_id' => $data['customer']['image_2_file_id'],
                        // 'image_3_file_id' => $data['customer']['image_3_file_id'],

                    ]);
                   }
                    }
                }
               if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                   $id_address=$data['receiver']['id'];
                        $receiver = Address::find($id_address);
                if (isset($data['Updatereciver']) && !($data['Updatereciver'])) {
                       $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                {
                                    $receiver->$property = $data['receiver'][$property];
                                }
                            }
                            $receiver->save();
                        }
                        // them ngay 24-04-2020
                        else if($data['receiver']['ward_id']!==$receiver['ward_id'])
                        {
                            $receiver->ward_id = $data['receiver']['ward_id'];
                            $receiver->save();
                        }
                        // them ngay 24-04-2020
                    }

            // hết cập nhật người nhận
            if (isset($data['containers']) && !empty($data['containers'])) {
                if (isset($data['containers'][0])
                    && !empty($data['containers'][0])
                    && isset($data['containers'][0]['id'])
                    && !empty($data['containers'][0]['id'])
                ) {
                    $con = $data['containers'][0];
                    $order = Order::find($data['containers'][0]['id']);
                    $total_weight = 0;
                    $total_goods = 0;
                    $total_discount = 0;
                    $total_final = 0;
                    $description = "";
                    $product_codes = "";
                    $user_notes = "";
                    foreach ($con['products'] as $product) {
                        if (isset($product['product']) && !empty($product['product'])) {
                            $quantity = 0;
                            $source = Product::where('id', '=', $product['product_id'])->first();
                            if(empty($source)){
                                continue;
                            }
                            if (isset($product['id']) && !empty($product['id'])) {
                                $orderItem = OrderItem::find($product['id']);
                            } else {
                                $orderItem = new OrderItem();
                            }
                            $orderItem->name = $product['product']['name'];
                            $orderItem->description = $product['product']['description'];
                            $orderItem->order_id = $order->id;
                            $orderItem->product_id = $product['product_id'];
                            $orderItem->quantity = $product['quantity'];
                            if(!empty($product['user_note'])){
                                $orderItem->user_note = $product['user_note'];
                                $user_notes .= $orderItem->user_note . '; ';
                            }
                            $orderItem->unit_goods_fee = isset($product['product']['sale_price']) && !empty($product['product']['sale_price']) ? $product['product']['sale_price'] : $product['unit_goods_fee'];
                            $total_weight += $orderItem->sub_total_weight = $product['sub_total_weight'];
                            if (isset($product['product']['by_weight']) && $product['product']['by_weight'] == 1) {
                                $quantity = $product['sub_total_weight'];
                            } else {
                                $quantity = $product['quantity'];
                            }
                            $total_goods += $orderItem->sub_total_goods = $orderItem->unit_goods_fee * $quantity;
                            $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                            $orderItem->per_discount = $discount;
                            $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);
                            $total_final += $orderItem->sub_total_final = $orderItem->sub_total_goods - $orderItem->sub_total_discount;
                            $description .= $product['quantity'] . ' ' . $product['product']['name'] . '; ';
                            $product_codes .= $product['product']['code'] . '; ';

                            $orderItem->messure_unit_id = $source->messure_unit_id;
                            $orderItem->save();
                        }
                    }
                    $orderAfter = Order::find($order->id);
                    $orderAfter->description = $description;
                    $orderAfter->products = $product_codes;
                    $orderAfter->length = $con['length'];
                    $orderAfter->width = $con['width'];
                    $orderAfter->height = $con['height'];
                    $orderAfter->receive_status = $con['receive_status'];
                    $orderAfter->total_weight = $con['total_weight'];
                    $orderAfter->shipping_fee = $con['shipping_fee'];
                    $orderAfter->total_shipping_fee = $orderAfter->total_weight * $orderAfter->shipping_fee;
                    $orderAfter->total_goods_fee = $total_goods;
                    $orderAfter->total_final = $total_final + $orderAfter->total_shipping_fee;
                    if ((isset($data['Updatereciver']) && !($data['Updatereciver'])) || ($data['receiver']['ward_id']!==$receiver['ward_id'])) {
                        $orderAfter->receiver_country_id=$data['receiver']['country_id'];
                        $orderAfter->receiver_province_id=$data['receiver']['province_id'];
                        $orderAfter->receiver_city_id=$data['receiver']['city_id'];
                        $orderAfter->receiver_ward_id = $data['receiver']['ward_id'];
                        $receiverWard = Ward::where('id', $orderAfter->receiver_ward_id)->first();
                        $orderAfter->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
                    }
                    // Kiểm tra mã coupon
                    if(!empty($orderAfter->coupon_code)){
                        $orderAfter->total_final -= $orderAfter->coupon_amount;
                        if($orderAfter->total_final < 0){
                            $orderAfter->total_final = 0;
                        }
                    }
                    $orderAfter->user_note = $user_notes;
                    if($con['last_paid_amount'] > 0 && !empty($con['last_date_pay'])){
                        $count = Transaction::where('order_id', $orderAfter->id)->count();

                        $transaction = new Transaction();
                        $transaction->agency_id = $orderAfter->agency_id;
                        $transaction->customer_id = $orderAfter->customer_id;
                        $transaction->transport_id = $orderAfter->transport_id;
                        $transaction->order_id = $orderAfter->id;
                        $transaction->currency_id = $orderAfter->currency_id;
                        $transaction->code = "PT_" . $orderAfter->code . "_{$count}";
                        $transaction->transaction_at = ConvertsUtil::dateFormat($con['last_date_pay'], config('app.date_format'));
                        $transaction->type = TransactionTypeEnum::RECEIPT;
                        $transaction->credit_amount = $con['last_paid_amount'];
                        $transaction->goods_code = $orderAfter->code;
                        $transaction->save();

                        $orderAfter->total_paid_amount += $con['last_paid_amount'];
                        $orderAfter->total_remain_amount = $orderAfter->total_final - $orderAfter->total_paid_amount > 0 ? $orderAfter->total_final - $orderAfter->total_paid_amount : 0;
                        if($orderAfter->total_remain_amount == 0){
                            $orderAfter->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                        }else{
                            $orderAfter->payment_status = OrderPaymentStatusEnum::PART_PAID;
                        }
                        $orderAfter->last_payment_at = ConvertsUtil::dateFormat($con['last_date_pay'], config('app.date_format'));
                        $orderAfter->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                        PointService::addPointToCustomer($orderAfter->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                    }
                    $orderAfter->total_discount = $total_discount;
                    $orderAfter->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    private function convertYhl (Order $order) {
        $data = $order->getAttributes();
        $data['customer'] = array(
            'id' => $order->customer_id,
            'first_name' => $order->sender_first_name,
            'email' => $order->receiver_email,
            'address_1' => $order->sender_address,
            'telephone' => $order->sender_phone,
            'country_id' => $order->sender_country_id,
            'province_id' => $order->sender_province_id,
            'city_id' => $order->sender_city_id,
            'postal_code' => $order->sender_post_code,
        );
        $data['receiver'] = array(
            'id' => $order->shipping_address_id,
            'first_name' => $order->receive_first_name,
            'email' => $order->receiver_email,
            'telephone' => $order->receiver_phone,
            'address_1' => $order->receiver_address,
            'country_id' => $order->receiver_country_id,
            'province_id' => $order->receiver_province_id,
            'city_id' => $order->receiver_city_id,
            'ward_id' => $order->receiver_ward_id,
            'postal_code' => $order->receiver_post_code,
        );
        $data['order_items'] = $order->order_items()->where('is_delete', 0)->with(['messure', 'product'])->get();
        return $data;
    }
}
