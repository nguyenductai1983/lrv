<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Exports\ExportQuery;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Enum\CustomerGroupEnum;
use App\Enum\OrderTypeEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\PointEnum;
use App\Components\LanguageUtil;
use App\Components\ConvertsUtil;
// use App\Services\PointService;
use App\Services\CampaignService;
use App\Address;
use App\Agency;
use App\Customer;
use App\Order;
use App\OrderItem;
use App\Transport;
use App\Product;
use App\Transaction;
use App\CampaignLog;
use App\Country;
use App\Province;
use App\City;
use App\Ward;
use App\Page;
use App\ActionLogs;
class YhlController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '6.1'
        ];
    }

    private function _validate(Request $request)
    {
        $this->validate($request, [
            'customer' => 'required|array',
            'customer.id' => 'nullable|exists:customers,id',
            'customer.image_1_file_id' => 'nullable|exists:files,id',
            'customer.image_2_file_id' => 'nullable|exists:files,id',
            'customer.image_3_file_id' => 'nullable|exists:files,id',
            'customer.first_name' => 'required|required_without:customer.id|string|max:50',
            'customer.middle_name' => 'nullable|string|max:50',
            'customer.last_name' => 'required|required_without:customer.id|string|max:50',
            'customer.address_1' => 'required|required_without:customer.id|string|max:255',
            'customer.email' => 'nullable|email',
            'customer.telephone' => 'required|required_without:customer.id|string|max:20',
            'customer.cellphone' => 'nullable|string|max:20',
            'customer.postal_code' => 'nullable|required_without:customer.id|string|max:20',
            'customer.id_card' => 'nullable|string|max:20',
            'customer.card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'customer.birthday' => 'nullable|date_format:' . config('app.date_format'),
            'customer.career' => 'nullable|string|max:100',
            'customer.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'customer.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'customer.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:255',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',
            'receiver.ward_id' => 'requiredIf:receiver.country_id,91',
            'receiver.telephone' => 'required|string|exclude_unless:receiver.country_id,91|min:10|max:10',
            'receiver.cellphone' => 'nullable|string|max:20',

            'containers' => 'required|array',
            'containers.*.length' => 'nullable|numeric|min:0',
            'containers.*.width' => 'nullable|numeric|min:0',
            'containers.*.height' => 'nullable|numeric|min:0',
            'containers.*.volume' => 'nullable|numeric|min:0',
            'containers.*.yhl_fee' => 'nullable|numeric|min:0',
            'containers.*.total_weight' => 'required|numeric|min:0',
            'containers.*.total_amount' => 'required|numeric|min:0',
            'containers.*.total_discount' => 'required|numeric|min:0',
            'containers.*.total' => 'required|numeric|min:0',
            'containers.*.date_pay' => 'nullable|date_format:' . config('app.date_format'),
            'containers.*.pay' => 'required|numeric|min:0',
            'containers.*.debt' => 'required|numeric|min:0',
            'containers.*.currency_id' => 'required|exists:currencies,id',
            'containers.*.pay_method' => 'required|string',
            'containers.*.status' => 'required|in:' . implode(',', config('transport.statues')),
            'containers.*.products' => 'required|array',
            'containers.*.products.*.code' => 'required|string',
            'containers.*.products.*.name' => 'required|string',
            'containers.*.products.*.by_weight' => 'required|in:0,1',
            'containers.*.products.*.quantity' => 'required|numeric|min:0',
            'containers.*.products.*.weight' => 'required|numeric|min:0',
            'containers.*.products.*.price' => 'required|numeric|min:0',
            'containers.*.products.*.discount' => 'nullable|numeric|min:0',
            'containers.*.products.*.amount' => 'required|numeric|min:0',
            'containers.*.products.*.total' => 'required|numeric|min:0',
            'containers.*.products.*.note' => 'nullable|string|max:255'
        ], [], [
            'customer.id' => __('label.sender'),
            'customer.image_1_file_id' => __('customer.image_1_file_id'),
            'customer.image_2_file_id' => __('customer.image_2_file_id'),
            'customer.image_3_file_id' => __('customer.image_3_file_id'),
            'customer.first_name' => __('customer.first_name'),
            'customer.middle_name' => __('customer.middle_name'),
            'customer.last_name' => __('customer.last_name'),
            'customer.address_1' => __('customer.address_1'),
            'customer.email' => __('customer.email'),
            'customer.telephone' => __('customer.telephone'),
            'customer.cellphone' => __('customer.cellphone'),
            'customer.postal_code' => __('customer.postal_code'),
            'customer.id_card' => __('customer.id_card'),
            'customer.card_expire' => __('customer.card_expire'),
            'customer.birthday' => __('customer.birthday'),
            'customer.career' => __('customer.career'),
            'customer.country_id' => __('customer.country_id'),
            'customer.province_id' => __('customer.province_id'),
            'customer.city_id' => __('customer.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),
            'receiver.ward_id' => __('receiver.ward_id'),

            'containers' => __('label.containers'),
            'containers.*.length' => __('label.length'),
            'containers.*.width' => __('label.width'),
            'containers.*.height' => __('label.height'),
            'containers.*.volume' => __('label.volume'),
            'containers.*.yhl_fee' => __('label.yhl_fee'),
            'containers.*.total_weight' => __('label.total_weight'),
            'containers.*.total_amount' => __('label.total_amount'),
            'containers.*.total_discount' => __('label.total_discount'),
            'containers.*.total' => __('label.total'),
            'containers.*.date_pay' => __('label.date_pay'),
            'containers.*.pay' => __('label.total_pay'),
            'containers.*.debt' => __('label.debt'),
            'containers.*.currency_id' => __('label.currency_pay'),
            'containers.*.pay_method' => __('label.pay_method'),
            'containers.*.status' => __('label.status'),

            'containers.*.products' => __('label.products'),
            'containers.*.products.*.product_id' => __('label.product_id'),
            'containers.*.products.*.code' => __('product.code'),
            'containers.*.products.*.name' => __('product.name'),
            'containers.*.products.*.by_weight' => __('product.by_weight'),
            'containers.*.products.*.quantity' => __('label.quantity_short'),
            'containers.*.products.*.weight' => __('label.weight_short'),
            'containers.*.products.*.price' => __('label.price'),
            'containers.*.products.*.discount' => __('label.discount_short'),
            'containers.*.products.*.unit' => __('label.unit_short'),
            'containers.*.products.*.amount' => __('label.amount'),
            'containers.*.products.*.total' => __('label.total'),
            'containers.*.products.*.note' => __('label.note')
        ]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('code'))&& strlen($request->query('code'))>6)
        {
            $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        else
        {
            if(!empty($request->query('code')))
            {
            $query->where('code', 'like', '%'.$request->query('code').'%');
            }
            if (!empty($request->query('from_date'))) {
                $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
            }
            if (!empty($request->query('to_date'))) {
                $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
            }
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('shipping_status', '=', $request->query('shipping_status'));
        }
        if (!empty($request->query('receiver_phone'))) {
            $query->where('receiver_phone', '=', $request->query('receiver_phone'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::home.index', $this->data);
    }


   public function exportExcel(Request $request)
    {
        $query = Order::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        $query->select('tbl_order.id','tbl_order.code', 'tbl_order.created_at', 'customer_note', 'user_note', 'description','package_quantity','total_weight','total_final','agency_discount',
            'total_paid_amount', 'total_remain_amount', 'payment_status',
            DB::raw("CONCAT(sender_first_name,sender_middle_name,sender_last_name) as sender_full_name"),
                'nuoc_gui.name as sender_countrie','tinh_gui.name as sender_province','thanh_pho_gui.name as sender_city', 'sender_address',
               'sender_phone', 'sender_cellphone', 'sender_email', 'sender_post_code',
                DB::raw("CONCAT(receive_first_name,receiver_middle_name,receive_last_name) as receive_full_name"),
                'nuoc_nhan.name as receiver_countrie', 'tinh_nhan.name as receiver_province','thanh_pho_nhan.name as receiver_city', 'receiver_address',
                 'receiver_phone', 'receiver_cellphone', 'receiver_email',
                 'receiver_post_code', 'is_crr',
                'order_status', 'shipping_status',  'receive_status',
                'sender_first_name', 'sender_middle_name', 'sender_last_name',
                'receive_first_name', 'receiver_middle_name', 'receive_last_name'
            );
         // lấy thông tin nước gửiprovinces
        $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id' );
        // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id' );
        // lấy thông tin tỉnh gửi
         $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id' );
         // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id' );
        // lấy thông tin thành phố gửi
         $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id' );
         // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id' );
        $query->where('type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_order.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_order.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('shipping_status', '=', $request->query('shipping_status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('sender_phone', '=', $request->query('sender_phone'));
        }
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        return (new ExportQuery($query))->download('yhl_order.xlsx');
    }

    public function printBill($id){
        $order = Order::find($id);
        if (empty($order)) {
            return redirect()->route('yhl.home.index');
        }
        $this->data['order'] = $order;
        return view('yhl::home.print', $this->data);
    }
    public function printBills($id){
         $currOrder = Order::find($id);
        if (empty($currOrder)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $orders = Order::where('transport_id', '=', $currOrder->transport_id)->get();
       $this->data['orders'] = $orders;
        return view('yhl::home.prints', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $slug=66;
        if (!$page = Page::with('translate')->where('id', '=', $slug)->first()) {
            $page='';
        }
        $this->data['termagree']=$page;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('yhl::home.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = null;
            $receiver = null;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            if (isset($data['customer']) && !empty($data['customer'])) {
                $customerExist = null;
                if(!empty($data['customer']['email'])){
                    $customerExist = Customer::where('email', $data['customer']['email'])->first();
                }
                if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                    if(!empty($customerExist) && $customerExist->id != $customer->id){
                        return response(__('message.cus_email_exist'), 500);
                    }
                    if(empty($customer)){
                        return response('Internal Server Error', 500);
                    }
                    else
                    {
                        if (isset($data['Updatecustomer']) && !($data['Updatecustomer'])) {
                         $customer->update([
                        'first_name'  => $data['customer']['first_name'],
                        'middle_name'  => $data['customer']['middle_name'],
                        'last_name'  => $data['customer']['last_name'],
                        'address_1'  => $data['customer']['address_1'],
                        'address_2'  => $data['customer']['address_2'],
                        'telephone'  => $data['customer']['telephone'],
                        'cellphone'  => $data['customer']['cellphone'],
                        'postal_code'  => $data['customer']['postal_code'],
                        'city_id'  => $data['customer']['city_id'],
                        'province_id'  => $data['customer']['province_id'],
                        'country_id' => $data['customer']['country_id'],
                        'id_card' => $data['customer']['id_card'],
                        'card_expire' => $data['customer']['card_expire'],
                        'birthday' => $data['customer']['birthday'],
                        'career' => $data['customer']['career'],
                        'image_1_file_id' => $data['customer']['image_1_file_id'],
                        'image_2_file_id' => $data['customer']['image_2_file_id'],
                        'image_3_file_id' => $data['customer']['image_3_file_id'],
                        'email' => $data['customer']['email'],
                    ]);
                         if(empty($customerExist)) {
                             $customer->update([
                        'email' => $data['customer']['email'],
                    ]);
                         }
                   }
                    }
                } else if(empty ($customerExist)){
                    $customer = new Customer();
                    $properties = array_keys($data['customer']);
                    foreach ($properties as $property) {
                        if (isset($data['customer'][$property]) && !empty($data['customer'][$property]))
                            $customer->$property = $data['customer'][$property];
                    }
                    $customer->agency_id = $agency->id;
                    $customer->customer_group_id = CustomerGroupEnum::CUS_SHP;
                    $customer->save();
                    $customer->code = "KH_" . $customer->id;
                    $customer->update();
                }else{
                    return response(__('message.cus_email_exist'), 500);
                }

                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        if (isset($data['Updatereciver']) && !($data['Updatereciver'])) {
                        // nếu dữ liệu người nhận không tồn tại thì tạo mới
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $receiver->customer_id = $customer->id;
                        }
                        // ngược lại nếu đã tòn tại
                        $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                {
                                    // $receiver[$property] = $data['receiver'][$property];
                                    $receiver->$property = $data['receiver'][$property];
                                }
                            }
                            $receiver->save();
                        }
                        // them ngay 24-04-2020
                        else if($data['receiver']['ward_id']!==$receiver['ward_id'])
                        {
                            $receiver['ward_id']=$data['receiver']['ward_id'];
                            $receiver->save();
                        }
                        // them ngay 24-04-2020
                    }
                    else
                    {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                            {
                            $receiver->$property = $data['receiver'][$property];
                            }
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                }
                }
            }


            if (isset($data['containers']) && !empty($data['containers'])) {
                $count_transport= Transport::where('user_id', $user->id)->where('type',OrderTypeEnum::YHL) ->count() +1;
                $countryreceiver = Country::find($receiver->country_id);
                $cityreceiver = City::find($receiver->city_id);
                $countryCode = isset($countryreceiver->code) && !empty($countryreceiver->code) ? $countryreceiver->code : 'VN';
                $transport = new Transport();
                $transport->user_id = $user->id;
                $transport->agency_id = $user->agency_id;
                $transport->customer_id = $customer->id;
                $transport->weight_unit_id = $agency->weight_unit_id;
                $transport->currency_id = $agency->currency_id;
                $transport->code = $user->code . sprintf('%04d', $count_transport) . "Y". $countryCode;
                $transport->type = OrderTypeEnum::YHL;
                $transport->save();

                $count_container = 1;
                foreach ($data['containers'] as $con) {
                    $order = new Order();
                    $order->user_id = $user->id;
                    $order->agency_id = $user->agency_id;
                    $order->payment_method = $con['pay_method'];
                    $order->customer_id = $customer->id;
                    $order=self::buid_Order($order,$customer,$receiver,$agency);
                    $order->transport_id = $transport->id;
                    $order->type = OrderTypeEnum::YHL;
                    $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                    $order->code = $user->code . sprintf('%04d', $count_transport) . "Y". $countryCode . $count_container . count($data['containers']);
                    $order->save();
                    $total_weight = 0;
                    $total_goods = 0;
                    $total_discount = 0;
                    $total_final = 0;
                    $total_agency_price = 0;
                    $description = "";
                    $products_code = "";
                    $user_note = "";
                    $count_products= count($con['products']);
                    $count_pro=1;
                    foreach ($con['products'] as $product) {
                        $source = Product::where('id', '=', $product['product_id'])->first();
                        if(empty($source)){
                            continue;
                        }
                        $agency_sub_total_goods = 0;
                        $quantity = 0;
                        $orderItem = new OrderItem();
                        $orderItem->name = $product['name'];
                        $orderItem->description = $order->code."-".$count_pro.$count_products;
                        $orderItem->order_id = $order->id;
                        $orderItem->product_id = $product['product_id'];
                        $orderItem->quantity = $product['quantity'];
                        if(!empty($product['note'])){
                            $orderItem->user_note = $product['note'];
                            $user_note .= $orderItem->user_note . '; ';
                        }
                        $orderItem->unit_goods_fee = $product['price'];
                        $total_weight += $orderItem->sub_total_weight = $product['weight']*$product['quantity'];
                        if (isset($product['by_weight']) && $product['by_weight'] == 1) {
                            $quantity = $product['weight'];
                        } else {
                            $quantity = $product['quantity'];
                        }
                        $agency_sub_total_goods =  $source->sale_price * $quantity;
                        $total_goods += $orderItem->sub_total_goods = $product['price'] * $quantity;
                        $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                        $orderItem->per_discount = $discount;
                        $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);
                        $total_final += $orderItem->sub_total_final = $orderItem->sub_total_goods - $orderItem->sub_total_discount;
                        $description .= $product['quantity'] . ' ' . $product['name'] . '; ';
                        $products_code .= $product['quantity'].' '. $product['code'] . '; '; // add quantity
                        $orderItem->messure_unit_id = $source->messure_unit_id;
                        $orderItem->agency_discount = 0;
                        if($orderItem->per_discount < $source->quota_discount){
                            $orderItem->agency_discount = ((($agency_sub_total_goods - $orderItem->sub_total_discount) * $source->commission_percent) / 100) + ($source->commission_amount*$quantity);
                        }else{
                            $orderItem->agency_discount = ((($agency_sub_total_goods - $orderItem->sub_total_discount) * $source->commission_quota_percent) / 100) + ($source->commission_quota_amount*$quantity);
                        }
                        $total_agency_price += $orderItem->agency_discount;
                        $orderItem->save();
                          $count_pro ++;

                    }
                    $orderAfter = Order::find($order->id);

                    $orderAfter->description = $description;
                    $orderAfter->user_note = $user_note;
                    $orderAfter->products = $products_code;
                    $orderAfter->length = $con['length'];
                    $orderAfter->width = $con['width'];
                    $orderAfter->height = $con['height'];
                    $orderAfter->receive_status = $con['status'];
                    $orderAfter->total_weight = $con['total_weight'];
                    $orderAfter->package_quantity = $con['total_quantity'];
                    $orderAfter->shipping_fee = $cityreceiver->yhl_fee;
                    $orderAfter->total_shipping_fee = $orderAfter->total_weight * $orderAfter->shipping_fee;
                    $orderAfter->total_goods_fee = $total_goods;
                    $orderAfter->total_final = $total_final + $orderAfter->total_shipping_fee;
                    // Kiểm tra mã coupon
                    if(!empty($con['coupon_code'])){
                        $amount = CampaignService::getAmountCoupon($con['coupon_code'], $orderAfter->customer_id, $orderAfter->total_weight);
                        if($amount > 0){
                            $orderAfter->coupon_code = $con['coupon_code'];
                            $orderAfter->coupon_amount = $amount;
                            $orderAfter->coupon_time = date("Y-m-d H:i:s");

                            $orderAfter->total_final -= $orderAfter->coupon_amount;
                            if($orderAfter->total_final < 0){
                                $orderAfter->total_final = 0;
                            }
                            $campaignLog = new CampaignLog();
                            $campaignLog->customer_id = $orderAfter->customer_id;
                            $campaignLog->order_id = $orderAfter->id;
                            $campaignLog->coupon_code = $orderAfter->coupon_code;
                            $campaignLog->coupon_amount = $amount;
                            $campaignLog->weight = $orderAfter->total_weight;
                            $campaignLog->save();
                        }
                    }
                    $orderAfter->total_paid_amount = $con['pay'];
                    $orderAfter->total_discount = $total_discount;
                    $orderAfter->agency_discount = $total_agency_price;
					if($orderAfter->agency_discount < 0){
						$orderAfter->agency_discount = 0;
					}
                    $orderAfter->total_paid_amount = 0;
                    if($con['pay'] > 0 && !empty($con['date_pay'])){
                        $count = Transaction::where('order_id', $orderAfter->id)->count();

                        $transaction = new Transaction();
						$transaction->user_id = $orderAfter->user_id;
                        $transaction->agency_id = $orderAfter->agency_id;
                        $transaction->customer_id = $orderAfter->customer_id;
                        $transaction->transport_id = $orderAfter->transport_id;
                        $transaction->order_id = $orderAfter->id;
                        $transaction->currency_id = $orderAfter->currency_id;
                        $transaction->code = "PT_" . $orderAfter->code . "_{$count}";
                        $transaction->transaction_at = ConvertsUtil::dateFormat($con['date_pay'], config('app.date_format'));
                        $transaction->type = TransactionTypeEnum::RECEIPT;
                        $transaction->credit_amount = $con['pay'];
						$transaction->total_amount = $orderAfter->total_final;
						$transaction->total_agency_discount = $orderAfter->agency_discount;
						$transaction->agency_discount = $transaction->credit_amount * ($transaction->total_agency_discount / $transaction->total_amount);
                        $transaction->goods_code = $orderAfter->code;
                        $transaction->save();

                        $orderAfter->total_paid_amount += $con['pay'];
                        $orderAfter->total_remain_amount = $orderAfter->total_final - $orderAfter->total_paid_amount > 0 ? $orderAfter->total_final - $orderAfter->total_paid_amount : 0;
                        if($orderAfter->total_remain_amount == 0){
                            $orderAfter->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                        }else{
                            $orderAfter->payment_status = OrderPaymentStatusEnum::PART_PAID;
                        }
                        $orderAfter->last_payment_at = ConvertsUtil::dateFormat($con['date_pay'], config('app.date_format'));

                        // PointService::addPointToCustomer($orderAfter->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                    }
                    $orderAfter->box_number = $count_container;
                    $orderAfter->save();
                    $count_container++;
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response($orderAfter->id);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    private static function buid_receiver($order,$receiver)
    {
        $order->shipping_address_id = $receiver->id;
        $order->receive_last_name = $receiver->last_name;
        $order->receive_first_name = $receiver->first_name;
        $order->receiver_middle_name = $receiver->middle_name;
        $order->receiver_email = $receiver->email;
        $order->receiver_phone = $receiver->telephone;
        $order->receiver_address = $receiver->address_1;
        $order->receiver_address_2 = $receiver->address_2;
        $order->receiver_country_id = $receiver->country_id;
        $order->receiver_province_id = $receiver->province_id;
        $order->receiver_city_id = $receiver->city_id;
        $order->receiver_ward_id = $receiver->ward_id;
        $order->receiver_cellphone = $receiver->cellphone;
        $order->receiver_post_code = $receiver->postal_code;
        $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
        $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
        $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
        $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
        $receiverCity = City::where('id', $order->receiver_city_id)->first();
        $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';
        $receiverWard = Ward::where('id', $order->receiver_ward_id)->first();
        $order->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
        return $order;
    }
    private static function  buid_Order($order,$customer,$receiver,$agency)
    {
        $order->sender_first_name = $customer->first_name;
        $order->sender_middle_name = $customer->middle_name;
        $order->sender_last_name = $customer->last_name;
        $order->sender_email = $customer->email;
        $order->sender_phone = $customer->telephone;
        $order->sender_address = $customer->address_1;
        $order->sender_address_2 = $customer->address_2;
        $order->sender_country_id = $customer->country_id;
        $order->sender_province_id = $customer->province_id;
        $order->sender_city_id = $customer->city_id;
        $order->sender_post_code = $customer->postal_code;
        $order->sender_cellphone = $customer->cellphone;
        $senderCountry = Country::where('id', $order->sender_country_id)->first();
        $order->sender_country_name = isset($senderCountry->name) ? $senderCountry->name : '';
        $senderProvince = Province::where('id', $order->sender_province_id)->first();
        $order->sender_province_name = isset($senderProvince->name) ? $senderProvince->name : '';
        $senderCity = City::where('id', $order->sender_city_id)->first();
        $order->sender_city_name = isset($senderCity->name) ? $senderCity->name : '';
        $order->shipping_address_id = $receiver->id;
        $order->receive_last_name = $receiver->last_name;
        $order->receive_first_name = $receiver->first_name;
        $order->receiver_middle_name = $receiver->middle_name;
        $order->receiver_email = $receiver->email;
        $order->receiver_phone = $receiver->telephone;
        $order->receiver_address = $receiver->address_1;
        $order->receiver_address_2 = $receiver->address_2;
        $order->receiver_country_id = $receiver->country_id;
        $order->receiver_province_id = $receiver->province_id;
        $order->receiver_city_id = $receiver->city_id;
        $order->receiver_ward_id = $receiver->ward_id;
        $order->receiver_cellphone = $receiver->cellphone;
        $order->receiver_post_code = $receiver->postal_code;
        $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
        $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
        $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
        $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
        $receiverCity = City::where('id', $order->receiver_city_id)->first();
        $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';
        $receiverWard = Ward::where('id', $order->receiver_ward_id)->first();
        $order->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
        $order->currency_id = $agency->currency_id;
        $order->dimension_unit_id = $agency->dimension_unit_id;
        $order->weight_unit_id = $agency->weight_unit_id;
        return $order;
    }
    public function show($id)
    {
        $transport = Order::where('id', '=', $id)
            ->with([
                'customer' => function ($query){
                    $query->with('image1');
                    $query->with('image2');
                    $query->with('image3');
                },
                'receiver',
                'order_items' => function ($query) {
                    $query->with('messure');
                    $query->with('product');
                }
            ])
            ->first();
        $transport->last_payment_at = $transport->last_payment_date;
        $transport->last_paid_amount = 0;
        $transport->last_date_pay = null;
        if (!$transport) {
            return response('Not Found', 404);
        }

        return response([
            'transport' => $transport
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        return view('yhl::home.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function updatereceiver(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $receiver = null;
            $order =null;
            $user = auth()->user();
           $body='';
        if (isset($data['receiver']['id']) && !empty($data['receiver']['id']))
                {
               $receiver = Address::where('id', $data['receiver']['id'])->first();
                  }
         if (isset($data['Updatereciver']) && !($data['Updatereciver'])) {
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                            {
                                if(!($receiver->$property === $data['receiver'][$property] || $property==='created_at' || $property==='updated_at'))
                                {
                                    $body.= $property .' : '. $receiver->$property .'-'. $data['receiver'][$property];
                                }
                                $receiver->$property = $data['receiver'][$property];
                            }

                        }
                        $receiver->update();
                        $order = Order::find($data['order']);
         $order=self::buid_receiver($order,$receiver);
         $order->save();
         $action = new ActionLogs();
         $action->user_id=$user->id;
         $action->order_id=$order->id;
         $action->body=$body;
         $action->save();
         DB::commit();
        }

        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = null;
            $receiver = null;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                     $customemail=$customer['email'];
                    if(empty($customer)){
                        return response('Internal Server Error', 500);
                    }
                }
                    if (isset($data['Updatecustomer']) && !($data['Updatecustomer']))
                    {
                         $customer->update([
                        'first_name'  => $data['customer']['first_name'],
                        'middle_name'  => $data['customer']['middle_name'],
                        'last_name'  => $data['customer']['last_name'],
                        'address_1'  => $data['customer']['address_1'],
                        'address_1'  => $data['customer']['address_1'],
                        'telephone'  => $data['customer']['telephone'],
                        'cellphone'  => $data['customer']['cellphone'],
                        'postal_code'  => $data['customer']['postal_code'],
                        'city_id'  => $data['customer']['city_id'],
                        'province_id'  => $data['customer']['province_id'],
                        'country_id' => $data['customer']['country_id'],
                        'id_card' => $data['customer']['id_card'],
                        'card_expire' => $data['customer']['card_expire'],
                        'birthday' => $data['customer']['birthday'],
                        'career' => $data['customer']['career'],
                        'image_1_file_id' => $data['customer']['image_1_file_id'],
                        'image_2_file_id' => $data['customer']['image_2_file_id'],
                        'image_3_file_id' => $data['customer']['image_3_file_id'],
                    ]);
                         if(empty($customemail) && isset($data['customer']['email'])) {
                              $customerExist = Customer::where('email', $data['customer']['email'])->first();
                             if(!empty($customerExist)){
                             $customer->update([
                                 'email' => $data['customer']['email'],
                                 ]);
                         }
                         else{
                    return response(__('message.cus_email_exist'), 500);
                            }
                   }
                  }
        if (isset($data['receiver']['id']) && !empty($data['receiver']['id']))
                {
               $receiver = Address::where('id', $data['receiver']['id'])->first();
                  }
         if (isset($data['Updatereciver']) && !($data['Updatereciver'])) {
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->customer_id = $customer->id;
                            $receiver->save();
                    } else {
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->update();
                    }
                }

            if (isset($data['containers']) && !empty($data['containers'])) {
                if (isset($data['containers'][0])
                    && !empty($data['containers'][0])
                    && isset($data['containers'][0]['id'])
                    && !empty($data['containers'][0]['id'])
                ) {
                    $con = $data['containers'][0];
                    $order = Order::find($data['containers'][0]['id']);
                    if ((int)$order->shipping_status<8){
                    $order=self::buid_Order($order,$customer,$receiver,$agency);
                    $order->save();
                    $total_weight = 0;
                    $total_goods = 0;
                    $total_discount = 0;
                    $total_final = 0;
                    $total_agency_price = 0;
                    $description = "";
                    $product_codes = "";
                    $user_notes = "";
                    foreach ($con['products'] as $product) {
                        if (isset($product['product']) && !empty($product['product'])) {
                            $agency_sub_total_goods = 0;
                            $quantity = 0;
                            $source = Product::where('id', '=', $product['product_id'])->first();
                            if(empty($source)){
                                continue;
                            }
                            if (isset($product['id']) && !empty($product['id'])) {
                                $orderItem = OrderItem::find($product['id']);
                            } else {
                                $orderItem = new OrderItem();
                            }
                            $orderItem->name = $product['product']['name'];
                            $orderItem->description = $product['product']['description'];
                            $orderItem->order_id = $order->id;
                            $orderItem->product_id = $product['product_id'];
                            $orderItem->quantity = $product['quantity'];
                            if(!empty($product['user_note'])){
                                $orderItem->user_note = $product['user_note'];
                                $user_notes .= $orderItem->user_note . '; ';
                            }
                            $orderItem->unit_goods_fee = isset($product['product']['pickup_fee']) ? $product['product']['sale_price'] + $product['product']['pickup_fee'] : $product['unit_goods_fee'];
                            $total_weight += $orderItem->sub_total_weight = $product['sub_total_weight'];
                            if (isset($product['product']['by_weight']) && $product['product']['by_weight'] == 1) {
                                $quantity = $product['sub_total_weight'];
                            } else {
                                $quantity = $product['quantity'];
                            }
                            $agency_sub_total_goods = $source->sale_price * $quantity;
                            $total_goods += $orderItem->sub_total_goods = $orderItem->unit_goods_fee * $quantity;
                            $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                            $orderItem->per_discount = $discount;
                            $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);
                            $total_final += $orderItem->sub_total_final = $orderItem->sub_total_goods - $orderItem->sub_total_discount;
                            $description .= $product['quantity'] . ' ' . $product['product']['name'] . '; ';
                            $product_codes .= $product['quantity'].' '.$product['product']['code'] . '; ';

                            $orderItem->messure_unit_id = $source->messure_unit_id;

                            $orderItem->agency_discount = 0;
                            if($orderItem->per_discount < $source->quota_discount){
                                $orderItem->agency_discount = ((($agency_sub_total_goods - $orderItem->sub_total_discount) * $source->commission_percent) / 100) + ($source->commission_amount * $quantity);
                            }else{
                                $orderItem->agency_discount = ((($agency_sub_total_goods - $orderItem->sub_total_discount) * $source->commission_quota_percent) / 100) + ($source->commission_quota_amount * $quantity);
                            }
                            $total_agency_price += $orderItem->agency_discount;
                            $orderItem->save();
                        }
                    }
                    $orderAfter = Order::find($order->id);
                    $orderAfter->description = $description;
                    $orderAfter->products = $product_codes;
                    $orderAfter->length = $con['length'];
                    $orderAfter->width = $con['width'];
                    $orderAfter->height = $con['height'];
                    $orderAfter->receive_status = $con['receive_status'];
                    $orderAfter->total_weight = $con['total_weight'];
                    $orderAfter->package_quantity = $con['total_quantity'];
                    $cityreceiver = City::find($receiver->city_id);
                    $orderAfter->shipping_fee = $cityreceiver->yhl_fee;
                    $orderAfter->total_shipping_fee = $orderAfter->total_weight * $orderAfter->shipping_fee;
                    $orderAfter->total_goods_fee = $total_goods;
                    $orderAfter->total_final = $total_final + $orderAfter->total_shipping_fee;
                    // Kiểm tra mã coupon
                    if(!empty($con['coupon_code'])){
                        $amount = CampaignService::getAmountCoupon($con['coupon_code'], $orderAfter->customer_id, $orderAfter->total_weight);
                        if($amount > 0){
                            $orderAfter->coupon_code = $con['coupon_code'];
                            $orderAfter->coupon_amount = $amount;
                            $orderAfter->coupon_time = date("Y-m-d H:i:s");

                            $orderAfter->total_final -= $orderAfter->coupon_amount;
                            if($orderAfter->total_final < 0){
                                $orderAfter->total_final = 0;
                            }
                            $campaignLog = new CampaignLog();
                            $campaignLog->customer_id = $orderAfter->customer_id;
                            $campaignLog->order_id = $orderAfter->id;
                            $campaignLog->coupon_code = $orderAfter->coupon_code;
                            $campaignLog->coupon_amount = $amount;
                            $campaignLog->weight = $orderAfter->total_weight;
                            $campaignLog->save();
                        }
                    }else{
                        $orderAfter->coupon_code = null;
                        $orderAfter->coupon_amount = null;
                        $orderAfter->coupon_time = null;

                        CampaignLog::where('order_id', '=', $orderAfter->id)->delete();
                    }
                    $orderAfter->agency_discount = $total_agency_price;
					if($orderAfter->agency_discount < 0){
						$orderAfter->agency_discount = 0;
					}
                    $orderAfter->user_note = $user_notes;
                    if($con['last_paid_amount'] > 0 && !empty($con['last_date_pay'])){
                        $count = Transaction::where('order_id', $orderAfter->id)->count();

                        $transaction = new Transaction();
						$transaction->user_id = $orderAfter->user_id;
                        $transaction->agency_id = $orderAfter->agency_id;
                        $transaction->customer_id = $orderAfter->customer_id;
                        $transaction->transport_id = $orderAfter->transport_id;
                        $transaction->order_id = $orderAfter->id;
                        $transaction->currency_id = $orderAfter->currency_id;
                        $transaction->code = "PT_" . $order->code . "_{$count}";
                        $transaction->transaction_at = ConvertsUtil::dateFormat($con['last_date_pay'], config('app.date_format'));
                        $transaction->type = TransactionTypeEnum::RECEIPT;
                        $transaction->credit_amount = $con['last_paid_amount'];
						$transaction->total_amount = $orderAfter->total_final;
						$transaction->total_agency_discount = $orderAfter->agency_discount;
						$transaction->agency_discount = $transaction->credit_amount * ($transaction->total_agency_discount / $transaction->total_amount);
                        $transaction->goods_code = $orderAfter->code;
                        $transaction->save();

                        $orderAfter->total_paid_amount += $con['last_paid_amount'];
                        $orderAfter->total_remain_amount = $orderAfter->total_final - $orderAfter->total_paid_amount > 0 ? $orderAfter->total_final - $orderAfter->total_paid_amount : 0;
                        if($orderAfter->total_remain_amount == 0){
                            $orderAfter->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                        }else{
                            $orderAfter->payment_status = OrderPaymentStatusEnum::PART_PAID;
                        }
                        $orderAfter->last_payment_at = ConvertsUtil::dateFormat($con['last_date_pay'], config('app.date_format'));

                        // PointService::addPointToCustomer($orderAfter->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                    }
                    $orderAfter->total_discount = $total_discount;
                    $orderAfter->save();
                    }
                    else
                    {
                        DB::rollBack();
                        return response('Orders in progress cannot be changed, contact the company for advice', 500);
                    }
            }
                // het update don hang
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
//            LogService::error($e);
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function print_invoice($id)
    {
        $currOrder = Order::find($id);
        if (empty($currOrder)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $orders = Order::where('transport_id', '=', $currOrder->transport_id)->get();
        $this->data['orders'] = $orders;
        return view('prints.print_invoice100x75', $this->data);
    }

}
