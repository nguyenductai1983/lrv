<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Components\VNPostClient;
use App\Components\LanguageUtil;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\ShippingMethodProviderEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
use App\ShippingPackageLocal;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use App\ListShippingPackage;
use App\Components\ConvertsUtil;
use Exception;
class CompleteController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '6.5'
        ];
    }
    public function index(Request $request) {
        $user = Auth::user();
        $query = ListShippingPackage::where([]);

        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        $query->where('shipping_provider_id', '=', ShippingMethodProviderEnum::SHIPPING_PROVIDER_GMN);
        $this->data['list_shipping_package'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::complete.approved.index', $this->data);
    }
    public function create(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::APPROVED);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::complete.approved.create', $this->data);
    }
    public function edit($id) {
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $ListShippingPackage = ListShippingPackage::find($id);
        if (empty($ListShippingPackage)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $query = ShippingPackageLocal::where([]);
        $query->where('list_id', '=',$ListShippingPackage->id);
        // $query->where('status', '=', ShippingPackageLocalEnum::APPROVED);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        $this->data['list_shipping_package']= $ListShippingPackage ;
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::complete.approved.edit', $this->data);
    }
    public function print($id)
    {
        $ListShippingPackage = ListShippingPackage::find($id);
        if (empty($ListShippingPackage)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $query = Order::where([]);
        $query->leftJoin('tbl_shipping_package_local as package_local', 'tbl_order.id', '=', 'package_local.order_id' );
        $orders=$query->where('package_local.list_id', '=', $ListShippingPackage->id)->get();
       $this->data['ListShippingPackage'] = $ListShippingPackage;
       $this->data['orders'] = $orders;
        return view('yhl::complete.approved.prints', $this->data);
    }

    // het doan approved
    public function cancel(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::CANCEL);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }

        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::complete.cancel', $this->data);
    }

    public function carrierPickup(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::DELIVERY_CARRIER);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::complete.carrierpickup', $this->data);
    }

    public function done(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        $query->where('order_type', '=', OrderTypeEnum::YHL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }

        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('yhl::complete.done', $this->data);
    }

    public function cancelRequest(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                if (!empty($shippingPackageLocal->tracking_code) && ($shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_FAST || $shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_SLOW)) {
                    $response = VNPostClient::cancelOrder($shippingPackageLocal->shipping_code);
                    if(empty($response->CancelOrderResult)){
                        DB::rollBack();
                        return response(["success" => false, "message" => $response->sE, "data" => []]);
                    }
                }
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CANCEL;
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                $orderData->update();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_REVICER;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_req_delivery_success'), "data" => ['refUrl' => route('yhl.complete.cancel')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    public function cancelOrder(Request $request) {
        $shippingPackageLocalId = $request;
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalId);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        try {
            DB::beginTransaction();
                if (!empty($shippingPackageLocalId->tracking_code) && ($shippingPackageLocalId->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_FAST || $shippingPackageLocalId->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_SLOW)) {
                    $response = VNPostClient::cancelOrder($shippingPackageLocalId->shipping_code);
                    if(empty($response->CancelOrderResult)){
                        DB::rollBack();
                        return response(["success" => false, "message" => $response->sE, "data" => []]);
                    }
                }
                $shippingPackageLocalId->status = ShippingPackageLocalEnum::CANCEL;
                $shippingPackageLocalId->update();
                $orderData = Order::find($shippingPackageLocalId->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                $orderData->update();
                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_REVICER;
                $orderLog->save();
            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_req_delivery_success'), "data" => ['refUrl' => route('yhl.complete.cancel')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    // public function pickup(Request $request) {
    //     $shippingPackageLocalIds = $request->get('shipping_package_locals');
    //     if (empty($shippingPackageLocalIds)) {
    //         return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
    //     }
    //     $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
    //     if (empty($shippingPackageLocals)) {
    //         return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
    //     }
    //     $user = Auth::user();
    //     if (empty($user)) {
    //         return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
    //     }
    //     try {
    //         DB::beginTransaction();

    //         foreach ($shippingPackageLocals as $shippingPackageLocal) {
    //             $shippingPackageLocal->status = ShippingPackageLocalEnum::DELIVERY_CARRIER;
    //             $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
    //             $shippingPackageLocal->update();

    //             $orderData = Order::find($shippingPackageLocal->order_id);
    //             if (empty($orderData)) {
    //                 DB::rollBack();
    //                 return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
    //             }
    //             $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
    //             $orderData->update();

    //             $orderTracking = new OrderTracking();
    //             $orderTracking->order_id = $orderData->id;
    //             $orderTracking->create_user_id = $user->id;
    //             $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
    //             $orderTracking->order_code = $shippingPackageLocal->order_code;
    //             $orderTracking->status = OrderTrackingStatusEnum::STATUS_CARRIER;

    //             $orderTracking->save();

    //             $orderLog = new OrderLog();
    //             $orderLog->order_id = $orderData->id;
    //             $orderLog->create_user_id = $user->id;
    //             $orderLog->status = OrderLogStatusEnum::STATUS_CARRIER;

    //             $orderLog->save();
    //         }

    //         DB::commit();

    //         return response(["success" => true, "message" => __('message.carrier_pickup_success'), "data" => ['refUrl' => route('yhl.complete.carrierpickup')]]);
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
    //     }
    // }
    public function store(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();
            $ListShippingPackage = new ListShippingPackage();
            $ListShippingPackage->user_id=$user->id;
            $count_list= ListShippingPackage::where('user_id', $user->id)->count() +1;
            $strdate = date('md');
            $code = "List".$user->code. $strdate . sprintf('%03d', $count_list) ;
            $ListShippingPackage->code =  $code ;
            $ListShippingPackage->type = OrderTypeEnum::YHL;
            $ListShippingPackage->shipping_provider_id = ShippingMethodProviderEnum::SHIPPING_PROVIDER_GMN;
            $ListShippingPackage->save();
            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                $shippingPackageLocal->status = ShippingPackageLocalEnum::DELIVERY_CARRIER;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->	list_id=$ListShippingPackage->id;
                $shippingPackageLocal->update();
                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
                $orderTracking->order_code = $shippingPackageLocal->order_code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_CARRIER;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_CARRIER;

                $orderLog->save();
            }

            DB::commit();
            return redirect()->route('yhl.complete.approved.edit',$ListShippingPackage->id);
            // return response(["success" => true, "message" => __('message.carrier_pickup_success'), "data" => ['refUrl' => route('yhl.complete.carrierpickup')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    public function customerReceiver(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
                $orderData->order_status = OrderStatusEnum::STATUS_COMPLETE;
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
                $orderTracking->order_code = $shippingPackageLocal->order_code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_DONE;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_DONE;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_delivery_success'), "data" => ['refUrl' => route('yhl.complete.done')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
