<?php

namespace Modules\Yhl\Http\Controllers;

use App\Import_goods;
use App\Import_good_details;
use Illuminate\Http\Request;
use App\Product_suppliers;
use App\Warehouse;
use Illuminate\Support\Facades\DB;
use Exception;

class ImportGoodsController extends Controller
{
    private function _validate(Request $request)
    {
        $this->validate($request, [
            'list_products' => 'required|array',
            'list_warehouse' => 'nullable|exists:tbl_warehouse,id',
            'list_supplies' => 'required|exists:product_suppliers,id'

        ], [], [
            'list_products' => __('label.list_products'),
            'list_warehouse' => __('label.list_warehouse'),
            'list_supplies' => __('label.list_supplies')
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Import_goods::where([]);
        $query->where('is_active', '=', 1);
        $this->data['lists'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        return view('yhl::Import_goods.index', $this->data);
        //
    }
    public function __construct() {
        $this->data = [
            'menu' => '6.8'
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_supplies = Product_suppliers::get();
        $list_warehouse = Warehouse::get();
        $this->data['list_supplies']=$list_supplies;
        $this->data['list_warehouse']=$list_warehouse;
        return view('yhl::Import_goods.create', $this->data);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->_validate($request);
        try {
            DB::beginTransaction();
            $data = $request->all();
            $user = auth()->user();
            $import_good = new Import_goods();
           $import_good->user_id=$user->id;
           $import_good->warehouse_id=$data['list_warehouse'];
           $import_good->product_supplier_id=$data['list_supplies'];
           $import_good->description = $data['description'];
           $import_good->is_active=1;
           $import_good->save();

           $list_products = $data['list_products'];
           foreach ($list_products as $product)
           {
            $import_good_details = new Import_good_details();
            $import_good_details->product_id=$product['product_id'];
            $import_good_details->import_good_id=$import_good->id;
            $import_good_details->quantity=$product['quantity'];
            $import_good_details->description=$product['note'];
            $import_good_details->is_active=1;
            $import_good_details->save();
           }
           DB::commit();
        }
        catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response($import_good->id);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Import_goods  $import_goods
     * @return \Illuminate\Http\Response
     */
    public function show(Import_goods $import_goods)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Import_goods  $import_goods
     * @return \Illuminate\Http\Response
     */
    public function edit($import_goods)
    {
        $transport = Import_goods::where('id', '=', $import_goods)
        ->first();
        $this->data['lists'] = $transport;
        $list_supplies = Product_suppliers::get();
        $list_warehouse = Warehouse::get();
        $this->data['list_supplies']=$list_supplies;
        $this->data['list_warehouse']=$list_warehouse;
        return view('yhl::Import_goods.edit', $this->data);

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Import_goods  $import_goods
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Import_goods $import_goods)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Import_goods  $import_goods
     * @return \Illuminate\Http\Response
     */
    public function destroy(Import_goods $import_goods)
    {
        //
    }
}
