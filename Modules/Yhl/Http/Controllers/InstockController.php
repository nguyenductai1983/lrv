<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Instock;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\LanguageUtil;
// use App\Http\Controllers\Admin\stockMutations\Stockable;
use App\Product;
use App\Stockables;
use App\Warehouse;
use App\ProductGroup;
use Illuminate\Support\Facades\DB;
// use DateTimeInterface;
// use Illuminate\Support\Arr;
// use Illuminate\Support\Carbon;
// use App\Enum\OrderTypeEnum;
// use Illuminate\Database\Eloquent\Relations\morphMany;
use Modules\Yhl\Http\Requests\InstockCreateRequest;
use Exception;

class InstockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '2.6'
        ];
    }
    public function index()
    {
        $query = Instock::where([]);
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        //
        return view('yhl::instock.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function warehousecreate($warehouse = null)
    {

        $this->data['warehouses'] = Warehouse::where(['id' => $warehouse])->get();
        // $this->data['products'] =$products ;
        return view('yhl::instock.create',   $this->data);
    }
    public function create()
    {
        $user = Auth::user();
        $list_warehouse = null;
        if ($user->role->admin) {
            $list_warehouse =  Warehouse::where(['is_deleted' => 0])->get();
        } else {
            $list_warehouse =  Warehouse::where(['id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        $this->data['warehouses'] = $list_warehouse;
        // $this->data['products'] =$products ;
        return view('yhl::instock.create',   $this->data);
    }
    public function printbill()
    {
        $this->data['warehouses'] = 1;
        return view('yhl::instock.prints',  $this->data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(InstockCreateRequest $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $list_product = $data['products'];
        $warehouse = $data['warehouse_id'];
        try {
            DB::beginTransaction();
            $Instock = new Instock();
            $Instock->user_id = $user->id;
            $Instock->warehouse_id = $warehouse;
            $Instock->save();
            $Instock->getMorphClass();
            $warehouse_class = Warehouse::find($data['warehouse_id']);
            $warehouse_class->getMorphClass();
            foreach ($list_product as $product) {
                if (!empty($product['product_id'])) {
                    $product_update = Product::find($product['product_id']);
                    $product_update->increaseStock($product['quantity'], [
                        'description' => $product['note'],
                        'reference' => $Instock, 'warehouse' => $warehouse_class,
                    ]);
                    //     $this->increaseStock($product_update,$product['quantity'],[
                    // 'description' => 'This is a description',
                    // 'reference' => $Instock,]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response($e->getMessage(), 500);
        }

        return response($Instock->id);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $columns = [
            'stockables.id as id',
            'products.code',
            'products.name',
            'stockables.description as note',
            'stockables.amount',

        ];
        $instock = Instock::where('id', '=', $id)->first();
        $this->data['instock'] = $instock;
        $this->data['warehouses'] = Warehouse::find($instock->warehouse_id);
        $query_stock = Stockables::where('reference_id', '=', $instock->id);
        $query_stock->leftJoin('products', 'stockable_id', '=', 'products.id');
        $query_stock->select($columns);
        $this->data['products'] = $query_stock->get();
        return view('yhl::instock.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InstockCreateRequest $request)
    {
        $data = $request->all();
        $id = $data['id'];
        if (!$instock = Instock::find($id)) {
            abort(404);
        }
        $list_product_instock = $data['product'];
        try {
            DB::beginTransaction();
            foreach ($list_product_instock as $stockable_item) {
                if (!empty($stockable_item['id'])) {
                    $stockable = Stockables::find($stockable_item['id']);
                    $stockable->amount = $stockable_item['amount'];
                    $stockable->update();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response($e->getMessage(), 500);
        }

        return redirect()->route('yhl.instock.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getStockAttribute($product)
    {
        return $this->stock($product);
    }
}
