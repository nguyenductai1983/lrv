<?php

namespace Modules\Yhl\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
//use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Enum\VnPostEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\ShippingMethodProviderEnum;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Components\ConvertsUtil;
use App\Components\VNPostClient;
use App\Components\LanguageUtil;
use App\Order;
use App\OrderTracking;
use App\Warehouse;
use App\Currency;
use App\DimensionUnit;
use App\WeightUnit;
use App\ShippingMethodProvider;
use App\ShippingPackageLocal;
use App\Address;
use App\Picksession;
use App\Services\giaohangtietkiem;
use App\CurrencyRate;
use App\OrderLog;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Services\myVNPost;
use App\ListShippingPackage;

class CarrierController extends Controller
{

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '6.3'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::YHL);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('code', '=', $request->query('order_code'));
        }
        // $query->where('shipping_status', '>=', OrderShippingStatusEnum::SHIPPING_STATUS_REVICER);
        $query->where('shipping_status', '=', OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP);
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['Picksessions'] = Picksession::get();
        $this->data['shipping_providers'] = ShippingMethodProvider::where([['is_active', '=', 1]])->orderBy('display_order', 'asc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('yhl::carrier.index', $this->data);
    }

    public function pushItems(Request $request)
    {
        $shippingMethodProvider = ShippingMethodProvider::find($request->route('id'));
        if (empty($shippingMethodProvider)) {
            return response(["success" => false, "message" => __('message.carrier_is_not_valid'), "data" => []]);
        }
        $orderIds = $request->get('order_ids');
        if (empty($orderIds)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $pick_session = $request->get('pick_session');
        if ($shippingMethodProvider->id == ShippingMethodProviderEnum::DROPOFF) {
            return $this->receiveDropOff($orderData, $shippingMethodProvider, $user);
        } else if ($shippingMethodProvider->id == ShippingMethodProviderEnum::ABS) {
            return $this->iGreenAbs($orderData, $shippingMethodProvider, $user);
        } else if ($shippingMethodProvider->id == ShippingMethodProviderEnum::VNPOST_FAST) {
            return $this->myVNPost($orderData, $shippingMethodProvider, $user, VnPostEnum::SERVICE_NAME_EMS);
        } else if ($shippingMethodProvider->id == ShippingMethodProviderEnum::VNPOST_SLOW) {
            return $this->myVNPost($orderData, $shippingMethodProvider, $user, VnPostEnum::SERVICE_NAME_BK);
        } else if ($shippingMethodProvider->id == ShippingMethodProviderEnum::GHTK) {
            return $this->giaohangtietkiem($orderData, $shippingMethodProvider, $user, $pick_session);
        }
    }

    private function receiveDropOff($orderData, $shippingMethodProvider, $user)
    {
        try {
            DB::beginTransaction();
            foreach ($orderData as $order) {
                $shippingPackageLocalExist = ShippingPackageLocal::where([
                    ['order_id', '=', $order->id],
                    ['status', '!=', ShippingPackageLocalEnum::CANCEL]
                ])->orderBy('id', 'desc')->get()->first();
                if (!empty($shippingPackageLocalExist)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-exist-shipping-local'), "data" => []]);
                }
                $warehouse = Warehouse::find($order->from_warehouse_id);
                if (empty($warehouse)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.warehouse-not-exist'), "data" => []]);
                }
                $currency = Currency::find($warehouse->currency_id);
                if (empty($currency)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.currency-not-exist'), "data" => []]);
                }
                $weightUnit = WeightUnit::find($warehouse->weight_unit_id);
                if (empty($weightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-not-exist'), "data" => []]);
                }
                $dimensionUnit = DimensionUnit::find($warehouse->dimension_unit_id);
                if (empty($dimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-not-exist'), "data" => []]);
                }
                $fromWeightUnit = WeightUnit::find($order->weight_unit_id);
                if (empty($fromWeightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-from-not-exist'), "data" => []]);
                }
                $fromDimensionUnit = DimensionUnit::find($order->dimension_unit_id);
                if (empty($fromDimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-from-not-exist'), "data" => []]);
                }
                $currencyRate = CurrencyRate::where([
                    ['currency_from_id', '=', $order->currency_id],
                    ['currency_to_id', '=', $warehouse->currency_id]
                ])->orderBy('id', 'desc')->get()->first();
                $rate = 1;
                if (!empty($currencyRate)) {
                    $rate = $currencyRate->rate;
                }
                $total_quantity_item = $order->order_items->sum('quantity');
                if ($total_quantity_item <= 0) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-no-item'), "data" => []]);
                }
                $shippingPackageLocal = new ShippingPackageLocal();
                //   $shippingPackageLocal->description = $order->description;
                $shippingPackageLocal->description = $order->user_note;
                $shippingPackageLocal->create_user_id = $user->id;
                $shippingPackageLocal->order_id = $order->id;
                $shippingPackageLocal->shipping_method_provider_id = $shippingMethodProvider->id;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
                $shippingPackageLocal->currency_id = $warehouse->currency_id;
                $shippingPackageLocal->weight_unit_id = $warehouse->weight_unit_id;
                $shippingPackageLocal->dimension_unit_id = $warehouse->dimension_unit_id;
                $shippingPackageLocal->warehouse_id = $warehouse->id;
                $shippingPackageLocal->shipping_address_id = $order->shipping_address_id;
                $shippingPackageLocal->shipping_method_provider_name = $shippingMethodProvider->name;
                $shippingPackageLocal->order_code = $order->code;
                $shippingPackageLocal->currency_code = $currency->code;
                $shippingPackageLocal->weight_unit_code = $weightUnit->code;
                $shippingPackageLocal->dimension_unit_code = $dimensionUnit->code;
                $shippingPackageLocal->shipping_address = $order->shipping_address;
                $shippingPackageLocal->sender_address = $warehouse->warehouse_address;
                $shippingPackageLocal->weight = ConvertsUtil::getWeight($order->total_weight, $fromWeightUnit, $weightUnit);
                $shippingPackageLocal->height = ConvertsUtil::getDimension($order->height, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->length = ConvertsUtil::getDimension($order->length, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->width = ConvertsUtil::getDimension($order->width, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->currency_rate = $rate;
                $shippingPackageLocal->total_declare_goods = ($order->total_declare_price + $order->total_final) * $rate;
                $shippingPackageLocal->total_quantity_item = $total_quantity_item;
                $shippingPackageLocal->is_channel_off = ShippingPackageLocalEnum::CHANNEL_OFFLINE;
                $shippingPackageLocal->order_type = OrderTypeEnum::YHL;

                $shippingPackageLocal->save();

                $num = ShippingPackageLocal::where('order_id', '=', $order->id)->count();
                if ($num > 1) {
                    $shippingPackageLocal->shipping_code = $order->code . "_" . $num;
                } else {
                    $shippingPackageLocal->shipping_code = $order->code;
                }
                $shippingPackageLocal->update();

                $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
                $order->order_status = OrderStatusEnum::STATUS_COMPLETE;
                $order->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->shipping_code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_DONE;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_DONE;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_delivery_success'), "data" => ['refUrl' => route('yhl.tracking.index')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    public static function addgiaohangtietkiem($orderData, $shippingMethodProvider, $user, $pick_session, $list)
    {
        $shippingMethodProvider = ShippingMethodProvider::find($shippingMethodProvider);
        $result = self::giaohangtietkiem($orderData, $shippingMethodProvider, $user, $pick_session, $list);
        return $result;
    }
    private static function giaohangtietkiem($orderData, $shippingMethodProvider, $user, $pick_session, $list = null)
    {
        try {
            DB::beginTransaction();
            if ($list == null) {
                $ListShippingPackage = new ListShippingPackage();
                $ListShippingPackage->user_id = $user->id;
                $count_list = ListShippingPackage::where('user_id', $user->id)->count() + 1;
                $strdate = date('md');
                $code = "GHTK" . $user->code . $strdate . sprintf('%03d', $count_list);
                $ListShippingPackage->code =  $code;
                $ListShippingPackage->type = OrderTypeEnum::YHL;
                $ListShippingPackage->shipping_provider_id = ShippingMethodProviderEnum::SHIPPING_PROVIDER_GHTK;
                $ListShippingPackage->save();
            } else {
                $ListShippingPackage = ListShippingPackage::find($list);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
        DB::commit();
        foreach ($orderData as $order) {
            DB::beginTransaction();
            try {
                $shippingPackageLocalExist = ShippingPackageLocal::where([
                    ['order_id', '=', $order->id],
                    ['status', '!=', ShippingPackageLocalEnum::CANCEL]
                ])->orderBy('id', 'desc')->get()->first();
                if (!empty($shippingPackageLocalExist)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-exist-shipping-local'), "data" => []]);
                }
                //$warehouse = Warehouse::find($order->from_warehouse_id);
                //tạm thời để bưu điện phú thọ nhận id=6
                $warehouse = Warehouse::find(2);
                if (empty($warehouse)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.warehouse-not-exist'), "data" => []]);
                }
                $currency = Currency::find($warehouse->currency_id);
                if (empty($currency)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.currency-not-exist'), "data" => []]);
                }
                $weightUnit = WeightUnit::find($warehouse->weight_unit_id);
                if (empty($weightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-not-exist'), "data" => []]);
                }
                $dimensionUnit = DimensionUnit::find($warehouse->dimension_unit_id);
                if (empty($dimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-not-exist'), "data" => []]);
                }
                $fromWeightUnit = WeightUnit::find($order->weight_unit_id);
                if (empty($fromWeightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-from-not-exist'), "data" => []]);
                }
                $fromDimensionUnit = DimensionUnit::find($order->dimension_unit_id);
                if (empty($fromDimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-from-not-exist'), "data" => []]);
                }
                $currencyRate = CurrencyRate::where([
                    ['currency_from_id', '=', $order->currency_id],
                    ['currency_to_id', '=', $warehouse->currency_id]
                ])->orderBy('id', 'desc')->get()->first();
                $rate = 1;
                if (!empty($currencyRate)) {
                    $rate = $currencyRate->rate;
                }
                $total_quantity_item = $order->order_items->sum('quantity');
                if ($total_quantity_item <= 0) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-no-item'), "data" => []]);
                }
                $shippingPackageLocal = new ShippingPackageLocal();
                // $shippingPackageLocal->description = $order->description;
                $shippingPackageLocal->create_user_id = $user->id;
                $shippingPackageLocal->order_id = $order->id;
                $shippingPackageLocal->shipping_method_provider_id = $shippingMethodProvider->id;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->currency_id = $warehouse->currency_id;
                $shippingPackageLocal->weight_unit_id = $warehouse->weight_unit_id;
                $shippingPackageLocal->dimension_unit_id = $warehouse->dimension_unit_id;
                $shippingPackageLocal->warehouse_id = $warehouse->id;
                $shippingPackageLocal->shipping_address_id = $order->shipping_address_id;
                $shippingPackageLocal->shipping_method_provider_name = $shippingMethodProvider->name;
                $shippingPackageLocal->order_code = $order->code;
                $shippingPackageLocal->currency_code = $currency->code;
                $shippingPackageLocal->weight_unit_code = $weightUnit->code;
                $shippingPackageLocal->dimension_unit_code = $dimensionUnit->code;
                $shippingPackageLocal->shipping_address = $order->shipping_address;
                $shippingPackageLocal->sender_address = $warehouse->warehouse_address;
                $shippingPackageLocal->weight = ConvertsUtil::getWeight($order->total_weight, $fromWeightUnit, $weightUnit);
                $shippingPackageLocal->height = ConvertsUtil::getDimension($order->height, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->length = ConvertsUtil::getDimension($order->length, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->width = ConvertsUtil::getDimension($order->width, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->currency_rate = $rate;
                $shippingPackageLocal->total_declare_goods = ($order->total_declare_price + $order->total_final) * $rate;
                $shippingPackageLocal->total_quantity_item = $total_quantity_item;
                $shippingPackageLocal->order_type = OrderTypeEnum::YHL;
                $shippingPackageLocal->is_channel_off = ShippingPackageLocalEnum::CHANNEL_ONLINE;
                $user_note = !empty($order->user_note) ? $order->user_note : $order->description;
                $user_note = str_replace("\"", "", $user_note);
                $user_note = str_replace(";", "", $user_note);
                $shippingPackageLocal->description = substr($user_note, 0, 252);
                $shippingPackageLocal->list_id = $ListShippingPackage->id;
                $shippingPackageLocal->save();

                $num = ShippingPackageLocal::where('order_id', '=', $order->id)->count();
                if ($num > 1) {
                    $shippingPackageLocal->shipping_code = $order->code . "0" . $num;
                } else {
                    $shippingPackageLocal->shipping_code = $order->code;
                }
                // cập nhật 17-04-2020
                if (empty($order->total_goods_fee <= 1)) {
                    $order->total_declare_price = $order->total_goods_fee * 10000;
                }
                $response = giaohangtietkiem::createOrder($pick_session, $shippingPackageLocal, $order, $warehouse);
                $info = json_decode($response);
                $message = preg_replace('/\\\u([0-9a-z]{4})/', '&#x$1;', $info->message);
                Log::info($message);
                // kiểm tra kết quả
                if ($info->success) {
                    // kiểm tra kết quả
                    $shippingPackageLocal->status = ShippingPackageLocalEnum::APPROVED;
                    $shippingPackageLocal->tracking_code = $info->order->label;
                    $shippingPackageLocal->carrier_fee = $info->order->fee;
                    $shippingPackageLocal->update();
                    $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
                    $order->update();

                    $orderTracking = new OrderTracking();
                    $orderTracking->order_id = $order->id;
                    $orderTracking->create_user_id = $user->id;
                    $orderTracking->tracking_code = $shippingPackageLocal->shipping_code;
                    $orderTracking->order_code = $order->code;
                    $orderTracking->status = OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;
                    $orderTracking->save();

                    $orderLog = new OrderLog();
                    $orderLog->order_id = $order->id;
                    $orderLog->create_user_id = $user->id;
                    $orderLog->status = OrderLogStatusEnum::STATUS_READY_TO_SHIP;

                    $orderLog->save();
                }
                // ket quả không thành công
                else {
                    DB::rollBack();
                    // cập nhật 17-04-2020
                    return response(["success" => false, "message" => isset($message) && !empty($message) ? $order->code . ':' . $message : 'Hãng vận chuyển không trả về kết quả. Liên hệ kỹ thuật.', "data" => []]);
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
            }
            DB::commit();
        }
        // hết vòng lặp
        if (empty($list)) {
            return response(["success" => true, "message" => $message, "data" => ['refUrl' => route('yhl.deliver.approved')]]);
        } else {
            return $message;
        }
    }
    private function iGreenAbs($orderData, $shippingMethodProvider, $user)
    {
        try {
            DB::beginTransaction();
            foreach ($orderData as $order) {
                $shippingPackageLocalExist = ShippingPackageLocal::where([
                    ['order_id', '=', $order->id],
                    ['status', '!=', ShippingPackageLocalEnum::CANCEL]
                ])->orderBy('id', 'desc')->get()->first();
                if (!empty($shippingPackageLocalExist)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-exist-shipping-local'), "data" => []]);
                }
                $warehouse = Warehouse::find($order->from_warehouse_id);
                if (empty($warehouse)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.warehouse-not-exist'), "data" => []]);
                }
                $currency = Currency::find($warehouse->currency_id);
                if (empty($currency)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.currency-not-exist'), "data" => []]);
                }
                $weightUnit = WeightUnit::find($warehouse->weight_unit_id);
                if (empty($weightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-not-exist'), "data" => []]);
                }
                $dimensionUnit = DimensionUnit::find($warehouse->dimension_unit_id);
                if (empty($dimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-not-exist'), "data" => []]);
                }
                $fromWeightUnit = WeightUnit::find($order->weight_unit_id);
                if (empty($fromWeightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-from-not-exist'), "data" => []]);
                }
                $fromDimensionUnit = DimensionUnit::find($order->dimension_unit_id);
                if (empty($fromDimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-from-not-exist'), "data" => []]);
                }
                $currencyRate = CurrencyRate::where([
                    ['currency_from_id', '=', $order->currency_id],
                    ['currency_to_id', '=', $warehouse->currency_id]
                ])->orderBy('id', 'desc')->get()->first();
                $rate = 1;
                if (!empty($currencyRate)) {
                    $rate = $currencyRate->rate;
                }
                $total_quantity_item = $order->order_items->sum('quantity');
                if ($total_quantity_item <= 0) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-no-item'), "data" => []]);
                }
                $shippingPackageLocal = new ShippingPackageLocal();
                // $shippingPackageLocal->description = $order->description;
                $shippingPackageLocal->description = $order->user_note;
                $shippingPackageLocal->create_user_id = $user->id;
                $shippingPackageLocal->order_id = $order->id;
                $shippingPackageLocal->shipping_method_provider_id = $shippingMethodProvider->id;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->status = ShippingPackageLocalEnum::APPROVED;
                $shippingPackageLocal->currency_id = $warehouse->currency_id;
                $shippingPackageLocal->weight_unit_id = $warehouse->weight_unit_id;
                $shippingPackageLocal->dimension_unit_id = $warehouse->dimension_unit_id;
                $shippingPackageLocal->warehouse_id = $warehouse->id;
                $shippingPackageLocal->shipping_address_id = $order->shipping_address_id;
                $shippingPackageLocal->shipping_method_provider_name = $shippingMethodProvider->name;
                $shippingPackageLocal->order_code = $order->code;
                $shippingPackageLocal->currency_code = $currency->code;
                $shippingPackageLocal->weight_unit_code = $weightUnit->code;
                $shippingPackageLocal->dimension_unit_code = $dimensionUnit->code;
                $shippingPackageLocal->shipping_address = $order->shipping_address;
                $shippingPackageLocal->sender_address = $warehouse->warehouse_address;
                $shippingPackageLocal->weight = ConvertsUtil::getWeight($order->total_weight, $fromWeightUnit, $weightUnit);
                $shippingPackageLocal->height = ConvertsUtil::getDimension($order->height, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->length = ConvertsUtil::getDimension($order->length, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->width = ConvertsUtil::getDimension($order->width, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->currency_rate = $rate;
                $shippingPackageLocal->total_declare_goods = ($order->total_declare_price + $order->total_final) * $rate;
                $shippingPackageLocal->total_quantity_item = $total_quantity_item;
                $shippingPackageLocal->is_channel_off = ShippingPackageLocalEnum::CHANNEL_OFFLINE;
                $shippingPackageLocal->order_type = OrderTypeEnum::YHL;

                $shippingPackageLocal->save();

                $num = ShippingPackageLocal::where('order_id', '=', $order->id)->count();
                if ($num > 1) {
                    $shippingPackageLocal->shipping_code = $order->code . "_" . $num;
                } else {
                    $shippingPackageLocal->shipping_code = $order->code;
                }
                $shippingPackageLocal->update();

                $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
                $order->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->shipping_code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_READY_TO_SHIP;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_delivery_success'), "data" => ['refUrl' => route('yhl.complete.approved')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    // cập nhật 17-04-2020
    private function myVNPost($orderData, $shippingMethodProvider, $user, $servicename)
    {
        try {
            foreach ($orderData as $order) {
                DB::beginTransaction();
                $shippingPackageLocalExist = ShippingPackageLocal::where([
                    ['order_id', '=', $order->id],
                    ['status', '!=', ShippingPackageLocalEnum::CANCEL]
                ])->orderBy('id', 'desc')->get()->first();
                if (!empty($shippingPackageLocalExist)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-exist-shipping-local'), "data" => []]);
                }
                $warehouse = Warehouse::find($order->from_warehouse_id);
                if (empty($warehouse)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.warehouse-not-exist'), "data" => []]);
                }
                $currency = Currency::find($warehouse->currency_id);
                if (empty($currency)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.currency-not-exist'), "data" => []]);
                }
                $weightUnit = WeightUnit::find($warehouse->weight_unit_id);
                if (empty($weightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-not-exist'), "data" => []]);
                }
                $dimensionUnit = DimensionUnit::find($warehouse->dimension_unit_id);
                if (empty($dimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-not-exist'), "data" => []]);
                }
                $fromWeightUnit = WeightUnit::find($order->weight_unit_id);
                if (empty($fromWeightUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.weight-from-not-exist'), "data" => []]);
                }
                $fromDimensionUnit = DimensionUnit::find($order->dimension_unit_id);
                if (empty($fromDimensionUnit)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.dimession-from-not-exist'), "data" => []]);
                }
                $currencyRate = CurrencyRate::where([
                    ['currency_from_id', '=', $order->currency_id],
                    ['currency_to_id', '=', $warehouse->currency_id]
                ])->orderBy('id', 'desc')->get()->first();
                $rate = 1;
                if (!empty($currencyRate)) {
                    $rate = $currencyRate->rate;
                }
                $total_quantity_item = $order->order_items->sum('quantity');
                if ($total_quantity_item <= 0) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.order-no-item'), "data" => []]);
                }
                $shippingPackageLocal = new ShippingPackageLocal();
                // $shippingPackageLocal->description = $order->description;
                $shippingPackageLocal->description = $order->user_note;
                $shippingPackageLocal->create_user_id = $user->id;
                $shippingPackageLocal->order_id = $order->id;
                $shippingPackageLocal->shipping_method_provider_id = $shippingMethodProvider->id;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->currency_id = $warehouse->currency_id;
                $shippingPackageLocal->weight_unit_id = $warehouse->weight_unit_id;
                $shippingPackageLocal->dimension_unit_id = $warehouse->dimension_unit_id;
                $shippingPackageLocal->warehouse_id = $warehouse->id;
                $shippingPackageLocal->shipping_address_id = $order->shipping_address_id;
                $shippingPackageLocal->shipping_method_provider_name = $shippingMethodProvider->name;
                $shippingPackageLocal->order_code = $order->code;
                $shippingPackageLocal->currency_code = $currency->code;
                $shippingPackageLocal->weight_unit_code = $weightUnit->code;
                $shippingPackageLocal->dimension_unit_code = $dimensionUnit->code;
                $shippingPackageLocal->shipping_address = $order->shipping_address;
                $shippingPackageLocal->sender_address = $warehouse->warehouse_address;
                $shippingPackageLocal->weight = ConvertsUtil::getWeight($order->total_weight, $fromWeightUnit, $weightUnit);
                $shippingPackageLocal->height = ConvertsUtil::getDimension(5, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->length = ConvertsUtil::getDimension(5, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->width = ConvertsUtil::getDimension(5, $fromDimensionUnit, $dimensionUnit);
                $shippingPackageLocal->currency_rate = $rate;
                $shippingPackageLocal->total_declare_goods = ($order->total_declare_price + $order->total_final) * $rate;
                $shippingPackageLocal->total_quantity_item = $total_quantity_item;
                $shippingPackageLocal->is_channel_off = ShippingPackageLocalEnum::CHANNEL_ONLINE;
                $shippingPackageLocal->order_type = OrderTypeEnum::YHL;

                $shippingPackageLocal->save();

                $num = ShippingPackageLocal::where('order_id', '=', $order->id)->count();
                if ($num > 1) {
                    $shippingPackageLocal->shipping_code = $order->code . "0" . $num;
                } else {
                    $shippingPackageLocal->shipping_code = $order->code;
                }
                $shippingPackageLocal->update();
                // cập nhật 17-04-2020
                $response = myVNPost::createOrder($servicename, $shippingPackageLocal, $order, $warehouse);
                Log::info(json_encode($response));
                $check_ketqua = false;
                if (!empty($response->ItemCode)) {
                    $check_ketqua = true;
                    $vnpt_code = $response->ItemCode;
                } else {
                    $vnpt_code = explode(' ', $response)[0];
                    $vnpt_code = explode('|', $vnpt_code);
                    if (count($vnpt_code) > 1) {
                        $check_ketqua = true;
                        $vnpt_code = $vnpt_code[1];
                    } else {
                        $check_ketqua = false;
                        $vnpt_code = null;
                    }
                }
                if ($check_ketqua) {
                    $shippingPackageLocal->status = ShippingPackageLocalEnum::APPROVED;
                    $shippingPackageLocal->tracking_code = $vnpt_code;
                    // cập nhật 17-04-2020
                    $shippingPackageLocal->update();

                    $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP;
                    $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
                    $order->update();

                    $orderTracking = new OrderTracking();
                    $orderTracking->order_id = $order->id;
                    $orderTracking->create_user_id = $user->id;
                    $orderTracking->tracking_code = $shippingPackageLocal->shipping_code;
                    $orderTracking->order_code = $order->code;
                    $orderTracking->status = OrderTrackingStatusEnum::STATUS_READY_TO_SHIP;

                    $orderTracking->save();

                    $orderLog = new OrderLog();
                    $orderLog->order_id = $order->id;
                    $orderLog->create_user_id = $user->id;
                    $orderLog->status = OrderLogStatusEnum::STATUS_READY_TO_SHIP;

                    $orderLog->save();
                } else {
                    // cập nhật 17-04-2020
                    return response(["success" => false, "message" => isset($response) ? $order->code . ':' . $response : 'Hãng vận chuyển không trả về kết quả. Liên hệ kỹ thuật.', "data" => []]);
                    DB::rollBack();
                }
                DB::commit();
            }
            return response(["success" => true, "message" => __('message.create_request_delivery_success'), "data" => ['refUrl' => route('yhl.deliver.approved')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
}
