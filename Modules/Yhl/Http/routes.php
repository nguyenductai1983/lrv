<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl'], 'prefix' => 'admin/yhls', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/', 'YhlController@index')->name('yhl.index');
    Route::get('/export', 'YhlController@exportExcel')->name('yhl.export');
    Route::get('/create', 'YhlController@create')->name('yhl.create');
    Route::post('/store', 'YhlController@store');
    Route::get('/{id}', 'YhlController@show')->name('yhl.show');
    Route::put('/{id}/update', 'YhlController@update')->name('yhl.update');
    Route::post('/{id}/updatereceiver', 'YhlController@updatereceiver')->name('yhl.updatereceiver');
    Route::get('/{id}/edit', 'YhlController@edit')->name('yhl.edit');
    Route::get('/{id}/print_bill', 'YhlController@printBill')->name('yhl.print');
    Route::get('/{id}/print_bills', 'YhlController@printBills')->name('yhl.prints');
    Route::get('/{id}/print_invoice', 'YhlController@print_invoice')->name('yhl.print_invoice');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl-customer'], 'prefix' => 'admin/yhl/customer', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/', 'YhlCustomerController@index')->name('yhl.customer.index');
    Route::get('/{id}', 'YhlCustomerController@show')->name('yhl.customer.show');
    Route::put('/{id}/update', 'YhlCustomerController@update')->name('yhl.customer.update');
    Route::get('/{id}/edit', 'YhlCustomerController@edit')->name('yhl.customer.edit');
    Route::get('/{id}/print_bill', 'YhlCustomerController@printBill')->name('yhl.customer.print');
    Route::get('/export', 'YhlCustomerController@exportExcel')->name('yhl.customer.export');
});
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl-stockout'], 'prefix' => 'admin/yhl/stockout', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/', 'StockoutController@index')->name('yhl.stockout.index');
    Route::get('/create', 'StockoutController@create')->name('yhl.stockout.create');
    Route::get('/{id}', 'StockoutController@view')->name('yhl.stockout.view');
    Route::get('/print/{id}', 'StockoutController@print')->name('yhl.stockout.print');
    Route::post('/push-items', 'StockoutController@pushItems');
    Route::get('/update_product', 'StockoutController@update_product')->name('yhl.stockout.update_product');;
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl-carrier'], 'prefix' => 'admin/yhl/carrier', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/', 'CarrierController@index')->name('yhl.carrier.index');
    Route::post('/{id}/push-items', 'CarrierController@pushItems');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl-deliver'], 'prefix' => 'admin/yhl/deliver', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/approved', 'DeliverController@approved')->name('yhl.deliver.approved');
    Route::get('/cancel', 'DeliverController@cancel')->name('yhl.deliver.cancel');
    Route::get('/carrier-pickup', 'DeliverController@carrierPickup')->name('yhl.deliver.carrierpickup');
    Route::get('/done', 'DeliverController@done')->name('yhl.deliver.done');
    Route::get('/{id}/getpdfghtk', 'DeliverController@getpdfghtk')->name('yhl.deliver.getpdfghtk');
    Route::get('/listghtk', 'DeliverController@listghtk')->name('yhl.deliver.listghtk');
    Route::get('/{id}/listghtk', 'DeliverController@editlistghtk')->name('yhl.deliver.editlistghtk');
    Route::get('/{id}/printlistghtk', 'DeliverController@printlistghtk')->name('yhl.deliver.printlistghtk');
    Route::post('/{id}/listghtk', 'DeliverController@addtolistghtk')->name('yhl.deliver.editlistghtk');
    // Service
    Route::post('/cancel-request', 'DeliverController@cancelRequest');
    Route::post('/approve-request', 'DeliverController@approveRequest');
    Route::post('/pickup', 'DeliverController@pickup');
    Route::post('/customer-receiver', 'DeliverController@customerReceiver');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl-complete'], 'prefix' => 'admin/yhl/complete', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/approved', 'CompleteController@index')->name('yhl.complete.approved');
    Route::get('/approved/{id}/print', 'CompleteController@print')->name('yhl.complete.approved.print');
    Route::get('/approved/create', 'CompleteController@create')->name('yhl.complete.approved.create');
    Route::get('/approved/excel', 'CompleteController@index')->name('yhl.complete.approved.excel');
    Route::get('/approved/{id}/edit', 'CompleteController@edit')->name('yhl.complete.approved.edit');
    Route::get('/cancel', 'CompleteController@cancel')->name('yhl.complete.cancel');
    Route::get('/carrier-pickup', 'CompleteController@carrierPickup')->name('yhl.complete.carrierpickup');
    Route::get('/done', 'CompleteController@done')->name('yhl.complete.done');

    // Service
    Route::post('/cancel-request', 'CompleteController@cancelRequest');
    Route::post('/cancelorder', 'CompleteController@cancelOrder')->name('yhl.complete.cancelOrder');;
    Route::post('/approve-request', 'CompleteController@approveRequest');
    Route::post('/pickup', 'CompleteController@pickup');
    Route::post('/customer-receiver', 'CompleteController@customerReceiver');
    Route::post('/approved/store', 'CompleteController@store')->name('yhl.complete.approved.store');

});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl'], 'prefix' => 'admin/yhl/tracking', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/', 'TrackingController@index')->name('yhl.tracking.index');
    Route::post('/view-history', 'TrackingController@viewHistory');
    Route::post('/cancel-order', 'TrackingController@cancelOrder');
    Route::post('/view-log', 'TrackingController@viewLog');
    Route::get('/export', 'TrackingController@exportExcel')->name('yhl.tracking.export');
    Route::get('/{id}/showinfo', 'TrackingController@showinfo')->name('yhl.tracking.showinfo');
    Route::post('/updateinfo', 'TrackingController@updateinfo')->name('yhl.tracking.updateinfo');
    Route::post('/findtracking', 'TrackingController@findtracking')->name('yhl.tracking.findtracking');
    Route::post('/updatestatus', 'TrackingController@updatestatus')->name('yhl.tracking.updatestatus');
    Route::get('/{id}/delete', 'TrackingController@delete')->name('yhl.tracking.delete');
});
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-yhl'], 'prefix' => 'admin/yhl/instock', 'namespace' => 'Modules\Yhl\Http\Controllers'], function() {
    Route::get('/{id}/warehousecreate', 'InstockController@warehousecreate')->name('yhl.instock.warehousecreate');
    Route::get('/', 'InstockController@index')->name('yhl.instock.index');
    Route::get('/create',  'InstockController@create')->name('yhl.instock.create');
    Route::post('/store', 'InstockController@store')->name('yhl.instock.store');
    Route::get('/{id}/edit', 'InstockController@edit')->name('yhl.instock.edit');
    Route::put('/{id}/update', 'InstockController@update')->name('yhl.instock.update');
    Route::get('/{id}', 'InstockController@show')->name('yhl.instock.show');
});
