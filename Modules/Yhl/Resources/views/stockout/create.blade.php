@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('yhl.list_waiting_stockout') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-12">
          <form action="{{ route('yhl.stockout.index') }}" method="get" role="form" class="form-inline">
            <div class="form-group">
              <input name="from_date" id="from_date" value="{{ request()->query('from_date') }}" class="form-control"
                type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
            </div>
            <div class="form-group">
              <input name="to_date" id="to_date" value="{{ request()->query('to_date') }}" class="form-control"
                type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
            </div>

            <div class="form-group">
              <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;"
                placeholder="{{ __('label.order_code') }}">
                <button type="button" class="btn btn-white btn-single" id="bt_switch_barcode" onclick="switch_barcode()"
                id="toggle-on">
                <i class="fas fa-barcode" id="switch_barcode"></i>
              </button>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-white btn-single">
                <i class="fa fa-search"></i> {{ __('label.find') }}
              </button>
              <a href="{{ route('yhl.stockout.index') }}" class="btn btn-white btn-single">
                {{ __('label.clear') }}
              </a>
            </div>
          </form>
          <p class="text-info">{{ __('label.barcode_switch') }}</p>
            <p class="text-danger" id="error_alert"></p>
        </div>
      </div>
      <form id="shipment-package-form">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>
                  <input type="checkbox" id="select-all">
                </th>
                <th width="13%">{{ __('label.order_code') }}</th>
                <th>{{ __('label.goods_info') }}</th>
                <th>{{ __('label.description') }}</th>
                <th>{{ __('label.note') }}</th>
                <th>{{ __('label.status') }}</th>
              </tr>
            </thead>
            <tbody id="shipment-package-items">
              @if ($orders->count() > 0)
                <?php $no = 1; ?>
                @foreach ($orders as $order)
                  <tr>
                    <td>{{ $no }}</td>
                    <td>
                      @if ($order->shipping_status == $order->shipping_status_revicer || $order->shipping_status === 5)
                        <input type="checkbox" name="order_ids[]" value="{{ $order->id }}"  id="{{ $order->code }}">
                      @endif
                    </td>
                    <td>{{ $order->code }}
                      <br>
                      {{ date_format($order->created_at, 'd/m/Y') }}
                    </td>
                    <td>{{ $order->products }}</td>
                    <td>{{ $order->description }}</td>
                    <td>{{ $order->user_note }}</td>
                    <td>
                      <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                    </td>
                  </tr>
                  <?php $no++; ?>
                @endforeach
              @else
                <tr>
                  <td colspan="7">{{ __('label.no_records') }}</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
        <div class="text-right">
          <div class="form-group bottom-group-form-left">
            <label for="from_warehouse_id">{{ __('yhl.from-warehouse') }}</label>
            <select name="from_warehouse_id" id="from_warehouse_id" class="form-control">
              @if ($warehouses->count() > 0)
                @foreach ($warehouses as $warehouse)
                  <option value="{{ $warehouse->id }}" @if (request()->query('to_warehouse_id') == $warehouse->id) selected @endif>
                    {{ $warehouse->name }}
                  </option>
                @endforeach
              @endif
            </select>
          </div>
          <button type="button" class="btn btn-info" onclick="stockout.pushItems();">
            <i class="fa fa-check"></i> {{ __('label.stock_out') }}
          </button>

        </div>
      </form>
      @if ($status_message != 0)
        {{ $status_message }}
        <div class='alert alert-success' role="alert">
          {{ __('label.complete') }}
        </div>
      @endif

    </div>
  </div>
@endsection

@section('footer')
  <script src="/js/yhl/stockout.js"></script>
  <script>
    $(function() {
      $("#from_date").datepicker({
        dateFormat: "{{ config('app.date_format_js') }}"
      });
      $("#to_date").datepicker({
        dateFormat: "{{ config('app.date_format_js') }}"
      });

    });

    $('#select-all').click(function(event) {
      if (this.checked) {
        $('#shipment-package-items input[type=checkbox]').each(function() {
          this.checked = true;
        });
      } else {
        $('#shipment-package-items input[type=checkbox]').each(function() {
          this.checked = false;
        });
      }
    });
    // xu ly nhận tín hiệu enter
   let barcode = false;
    let count;
    $("#order_code").on('keypress', function(event) {
      if (event.which === 13) {
        stockout.checkItem(document.getElementById('order_code').value);
        event.preventDefault();
        document.getElementById('order_code').value = '';
        return false;
      }
    });
    function switch_barcode() {
      var btn_x = document.getElementById("bt_switch_barcode");
      if (barcode === false) {
        barcode = true;
        // x.style.display = "inline";
        btn_x.className = "btn btn-info btn-single";
        btn_x.style.fontSize = '18pt';
        count = setInterval(timer, 100)
      } else {
        barcode = false;
        // x.style.display = "none";
        btn_x.style.fontSize = '';
        btn_x.className = "btn btn-white btn-single";
        clearInterval(count)
      }
    }
    // xu ly mở tắt barcode
    function timer() {
      var focusbox = document.getElementById("order_code");
      focusbox.focus();
    }
    // xu ly nhận tín hiệu enter
  </script>
@endsection
