@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default" ng-app="YhlApp" ng-controller="YhlEditController" style="padding: 15px;">
        <div class="panel-heading">
          {{ __('label.edit') }} Yhl <strong>{{ $order->code }}</strong>
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-edit" ng-submit="updateYhl()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.customer.length || errors.receiver.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.customer.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.customer">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}
                                        </h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border">
                    <div class="panel-heading">
                        <div style="margin: -12px 0;width: 80%;float: right;">
                        </div>
                    </div>
                    <div class="panel-body" style="margin: -10px -20px;">
                        <table class="table table-form">
                            <tbody>
                            <tr>
                                <td width="50%" style="border-right: 1px solid #ccc;">
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.sender') }}</h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="customer.first_name" ng-model="customer.first_name" ng-disabled="customer.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.email') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="customer.email" ng-model="customer.email" ng-disabled="customer.id">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="customer.address_1" ng-model="customer.address_1" ng-disabled="customer.id">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="customer.country_id" class="form-control" ng-model="customer.country_id" ng-change="getProvincesCustomer()" ng-disabled="customer.id">
                                                    <option value="">{{ __('label.select_country') }}</option>
                                                    <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="customer.province_id" class="form-control" ng-model="customer.province_id" ng-change="getCitiesCustomer()" ng-disabled="customer.id">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesCustomer" ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="customer.city_id" class="form-control" ng-model="customer.city_id" ng-disabled="customer.id">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesCustomer" ng-value="city.id">@{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label">
                                                {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="customer.telephone" ng-model="customer.telephone" ng-disabled="customer.id">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}
                                        <button type="button" ng-click="Editreceiver()">
                                            <i class="fa fa-edit alert-success"></i>
                                        </button></h3>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" ng-disabled="Updatereciver">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('customer.email') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="receiver.email" ng-model="receiver.email" ng-disabled="Updatereciver">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.address') }}<i class="text-danger">*</i>
                                            </td>
                                            <td colspan="5">
                                                <input type="text" class="form-control" name="receiver.address_1" ng-model="receiver.address_1" ng-disabled="Updatereciver">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-form">
                                        <tbody>
                                        <tr>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td width="30%">
                                                <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" ng-disabled="Updatereciver">
                                                    <option value="">{{ __('label.select_country') }}</option>
                                                    <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                                </select>
                                            </td>
                                            <td width="20%" class="col-label">
                                                {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" ng-disabled="Updatereciver">
                                                    <option value="">{{ __('label.select_province') }}</option>
                                                    <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="getWardsReceiver()" ng-disabled="Updatereciver">
                                                    <option value="">{{ __('label.select_city') }}</option>
                                                    <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                                </select>
                                            </td>
                                            <td class="col-label" ng-hide="ward">
                                                {{ __('receiver.ward_id') }}<i class="text-danger">*</i>
                                            </td>
                                            <td ng-hide="ward">
                                                <select name="receiver.ward_id" class="form-control" ng-model="receiver.ward_id" ng-disabled="Updatereciver && receiver.ward_id">
                                                    <option value="">{{ __('label.select_ward') }}</option>
                                                    <option ng-repeat="ward in wardsReceiver" ng-value="ward.id">@{{ ward.name }}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-label">
                                                {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone"  ng-disabled="Updatereciver">
                                            </td>
                                            <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                            <td>
                                                <input type="text" class="form-control" name="receiver.cellphone"
                                                       ng-model="receiver.cellphone" ng-disabled="Updatereciver">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-color panel-gray panel-border" style="padding-bottom: 0;margin-bottom: -2px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-cubes"></i> {{ __('label.goods') }}</h3>
                    </div>
                </div>
                <div id="container" class="tabs-border">
                    <div class="tab-content" style="padding: 15px;">
                        <div ng-repeat="container in containers" class="tab-pane" ng-class="{'active' : $first}" id="container-@{{ $index + 1 }}">
                            <div class="form-group">
                                <table class="table table-form table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="150" class="text-right col-middle">{{ __('label.total_weight') }}:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.total_weight.toFixed(2)"></strong>
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.length') }}:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.length" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.width') }}:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.width" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.height') }}:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.height" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="100" class="text-right col-middle">{{ __('label.volume') }}:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.volume.toFixed(2)"></strong>
                                        </td>
                                        <td class="col-middle">
                                            <button type="button" class="btn btn-info btn-xs" ng-click="setVolume(container)">
                                                <i class="fa fa-check"></i> {{ __('label.select') }}
                                            </button>
                                        </td>
                                        <td ng-if="containers.length > 1" class="col-middle">
                                            <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#modal-delete-container-@{{ $index }}">
                                                <i class="fas fa-trash"></i> {{ __('label.delete_container') }} @{{ $index + 1 }}
                                            </button>
                                            <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! __('label.confirm_delete_msg') !!}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger disabled-submit" ng-click="deleteContainer($index)">
                                                                <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                            </button>
                                                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-container" style="max-width: 120%;width: 120%;">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th width="130">{{ __('product.code') }}</th>
                                        <th width="153">{{ __('product.name') }}</th>
                                        <th width="60" class="text-center">{{ __('label.quantity_short') }}</th>
                                        <th width="60" class="text-center">{{ __('label.weight_short') }}</th>
                                        <th width="60" class="text-right">{{ __('label.price') }}</th>
                                        <th width="60" class="text-right">{{ __('label.discount_short') }}(%)</th>
                                        <th width="60" class="text-center">{{ __('label.unit_short') }}</th>
                                        <th width="80" class="text-right">{{ __('label.amount') }}</th>
                                        <th width="81" class="text-right">{{ __('label.total') }}</th>
                                        <th>{{ __('label.note') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in container.order_items">

                                        <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.product.code"
                                                   ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')" ng-disabled="container.shipping_status > 5">
                                            <div id="products-@{{ $parent.$index + '-' + $index }}-dropdown"   class="dropdown products-dropdown">
                                                <button class="btn btn-primary dropdown-toggle hidden" type="button" data-toggle="dropdown"></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <table class="table table-bordered table-hover">
                                                            <tbody>
                                                            <tr ng-repeat="item in products" ng-style="!item.show && {display: 'none'}" ng-click="selectProduct(product, item)">
                                                                <td ng-bind="item.code"></td>
                                                                <td ng-bind="item.name"></td>
                                                                <td class="text-right" ng-bind="item.sale_price"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control input-readonly" ng-model="product.product.name" readonly="readonly" ng-disabled="container.shipping_status > 5">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.quantity" ng-change="updateContainers()" ng-disabled="container.shipping_status > 5">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.sub_total_weight" ng-change="updateContainers()" ng-disabled="container.shipping_status > 5">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.unit_goods_fee"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center"
                                                   ng-model="product.per_discount" ng-change="updateContainers()"
                                                   ng-disabled="container.shipping_status > 5">
                                        </td>
                                        <td class="text-center col-middle" ng-bind="product.messure.code"></td>
                                        <td class="text-right col-middle" ng-bind="product.sub_total_goods.toFixed(2)"></td>
                                        <td class="text-right col-middle" ng-bind="product.total.toFixed(2)"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.user_note" ng-disabled="container.shipping_status > 5">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td class="text-right" width="15%"></td>
                                    <td class="text-left" width="19%"></td>
                                    <td class="col-middle text-right" width="15%">{{ __('label.coupon_code') }}:</td>
                                    <td class="text-left" width="18%">
                                        <div class="input-group">
                                            <input type="text" class="form-control" ng-model="container.coupon_code">
                                        </div>
                                    </td>
                                    <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                    <td class="text-left">
                                        <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" width="15%">{{ __('label.shipping_fee') }}:</td>
                                    <td class="text-left" width="19%"><strong
                                                ng-bind="container.shipping_fee.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_weight') }}:</td>
                                    <td class="text-left" width="18%"><strong
                                                ng-bind="container.total_weight.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_shipping_fee') }}:</td>
                                    <td class="text-left"><strong
                                                ng-bind="container.total_shipping_fee.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_amount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_amount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_discount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_discount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_pay') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="col-middle text-right">{{ __('label.date_pay') }}:</td>
                                    <td class="col-middle text-left">
                                        <div class="input-group date-picker" data-change-year="true"
                                             data-change-month="true"
                                             data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                            <input type="text" class="form-control" name="datePay"
                                                   ng-model="container.last_date_pay">

                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-white">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.pay') }}:</td>
                                    <td class="col-middle text-left">
                                        <input type="text" class="form-control text-right"
                                               ng-model="container.last_paid_amount" ng-change="updateContainers()">
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.debt') }}:</td>
                                    <td class="col-middle text-left"><strong
                                                ng-bind="container.debt.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="col-middle text-right">{{ __('label.paid') }}:</td>
                                    <td class="text-left" style="padding-top:18px">
                                        <strong class="ng-binding" ng-bind="container.total_paid_amount"></strong>
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.last_paid_time') }}:</td>
                                    <td class="text-left" style="padding-top:18px">
                                        <strong class="ng-binding" ng-bind="container.last_payment_at"></strong>
                                    </td>
                                    <td class="col-middle text-right">{{ __('label.currency_pay') }}:</td>
                                    <td class="text-left" style="padding-top:18px">
                                        <strong class="ng-binding">{{ auth()->user()->agency->currency->code }}</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-middle text-right"></td>
                                    <td class="text-left" style="padding-top:18px">
                                        <strong class="ng-binding"></strong>
                                    </td>
                                    <td class="col-middle text-right"></td>
                                    <td class="text-left">

                                    </td>
                                    <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                                    <td class="text-left">
                                        <select id="payMethod" class="form-control"
                                                ng-model="container.payment_method">
                                            <option ng-repeat="method in methods"
                                                    ng-value="method.code">@{{ method.name }}</option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="text-right" style="padding-top: 30px;">
                    <a href="{{ route('yhl.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                    <button type="submit" class="btn btn-info" ng-disabled="submitted || containers[0].shipping_status > 5">
                        <i class="fa fa-check"></i> {{ __('label.update') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/admin/app/yhl-customer-edit.js?t=' . File::lastModified(public_path('js/admin/app/yhl-customer-edit.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-edit').scope();

            scope.agency = {!! auth()->user()->agency_id !!};
            scope.id = {!! request()->route('id') !!};

            scope.getYhl();

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
