@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <ul class="nav nav-pills">
        <li >
            <a href="{{ route('yhl.deliver.approved') }}">{{ __('yhl.ship_local_status_approve')}}</a>
        </li>
        <li class="active">
            <a href="{{ route('yhl.deliver.listghtk') }}">GHTK {{ __('yhl.ship_local_status_approve')}}</a>
        </li>
        <li>
            <a href="{{ route('yhl.deliver.carrierpickup') }}">{{ __('yhl.ship_local_status_carrier')}}</a>
        </li>
        <li>
            <a href="{{ route('yhl.deliver.done') }}">{{ __('yhl.ship_local_status_done')}}</a>
        </li>
        <li>
            <a href="{{ route('yhl.deliver.cancel') }}">{{ __('yhl.ship_local_status_reject')}}</a>
        </li>
    </ul>
    <div class="panel-heading">
        {{-- {{ __('label.approved_list') }} --}}
        GHTK
    </div>
    <div class="panel-body">

        {{-- <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.deliver.listghtk') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.shipping_package_code') }}">
                    </div>
                    <div class="form-group">
                        <select name="status" id="country_id" class="form-control">
                            <option value="">{{ __('label.status_all') }}</option>
                            <option value="1" @php if(request()->query('status') == 1){@endphp selected @php }@endphp>{{ __('label.status_normal') }}</option>
                            <option value="2" @php if(request()->query('status') == 2){@endphp selected @php }@endphp>{{ __('label.status_cancel') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.deliver.listghtk') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                        <a href="{{ route('yhl.complete.approved.excel') }}" class="btn btn-white btn-single">
                            {{ __('label.export_excel') }}
                        </a>
                    </div>
                </form>
            </div>
        </div> --}}
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('label.shipment_code') }}</th>
                        <th>{{ __('label.name') }}</th>
                        <th>{{ __('label.create_time') }}</th>
                        <th>{{ __('label.status') }}</th>
                        <th>{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($list_shipping_package->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($list_shipping_package as $shipping_package)
                    <tr>
                        <td>{{ $no }}</td>
                        <td><a href="{{ route('yhl.complete.approved.edit', $shipping_package->id) }}">
                            {{ $shipping_package->code }}
                        </a>
                        </td>
                        <td>{{ $shipping_package->user->name }}</td>
                        <td>{{ $shipping_package->created_at }}</td>
                        <td>
                            @if(empty($shipping_package->deleted_at))
                            <span class="label label-success">{{ __('label.status_normal') }}</span>
                            @else
                            <span class="label label-danger">{{ __('label.status_cancel') }}</span>
                            @endif
                        </td>
                        <td>
                            @if(empty($shipping_package->deleted_at))
                            <a href="{{ route('yhl.deliver.editlistghtk', $shipping_package->id) }}" class="btn btn-xs btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ route('yhl.deliver.printlistghtk', $shipping_package->id) }}" target="_blank" class="btn btn-xs btn-info">
                                <i class="fa fa-print" aria-hidden="true"></i> {{ __('label.print_bill')}}
                            </a>
                            @endif
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $list_shipping_package->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
