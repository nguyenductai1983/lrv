@extends('layouts.admin.index', ['menu' => $menu])
@section('head')
<link rel="stylesheet"
    href="{{ asset('css/admin/tracking.css?t=' . File::lastModified(public_path('css/admin/tracking.css'))) }}">
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        <form action="{{ route('yhl.tracking.findtracking') }}" method="post" role="form" class="form-inline">
            @csrf
            <div class="form-group">
                <input name="code" class="form-control" type="text" value="{{ $order->code }}" style="width: 190px;"
                    placeholder="{{ __('label.order_code') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-white btn-single">
                    <i class="fa fa-search"></i> {{ __('label.find') }}
                </button>
            </div>
        </form>
        {{ __('label.order_code') }}:
        <a href="{{ route('yhl.edit', $order->id) }}">
            {{ $order->code }} <i class="fa fa-eye"></i>
        </a>
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">

            </div>
        </div>
        <form role="form" id="form-edit" action="{{ route('yhl.tracking.updateinfo') }}" method="post">
            @csrf
            <input type="hidden" name="order_id" value="{{ $order->id }}" />
            <input type="hidden" name="address_id" value="{{ $order->shipping_address_id }}" />
            <div class="row form-group">
                <div class="col-sm-3">
                    <h4> {{ __('label.receiver') }} : {{ $order->receive_full_name }}</h5>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-1">
                    {{ __('customer.first_name') }}
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="address.first_name"
                        value="{{ $order->receive_first_name }}">
                </div>

                <div class="col-sm-1">
                    {{ __('customer.middle_name') }}
                </div>
                <div class="col-sm-3"><input type="text" class="form-control" name="address.middle_name"
                        value="{{ $order->receiver_middle_name }}"></div>

                <div class="col-sm-1">
                    {{ __('customer.last_name') }}
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="address.last_name"
                        value="{{ $order->receive_last_name }}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-1">
                    {{ __('receiver.telephone') }} <i class="fa fa-phone"></i><i class="text-danger">*</i>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="address.telephone"
                        value="{{ $order->receiver_phone }}">
                </div>
                <div class="col-sm-1">
                    {{ __('receiver.cellphone') }} <i class="fa fa-phone"></i>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="address.cellphone"
                        value="{{ $order->receiver_cellphone }}">
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-info">
                        <i class="fa fa-check"></i> {{ __('label.update') }}
                    </button class="btn btn-info"></div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            @if (Session::has('complete'))
            <div class='alert alert-success' role="alert">
                {{ __('label.complete') }}
            </div>
            @endif
        </form>
        <hr>
        <article class="card">
            <div class="track">
                <div class="step active"> <span class="icon"> <i class="fas fa-warehouse"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_stockin') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=3) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fas fa-pallet"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_shipment') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=4) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-plane"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_stockout') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=6) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-th-large"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_local') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=7) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fab fa-stack-overflow"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_carrier') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=9) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-truck"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_ready') }}
                    </span>
                </div>
                <div class="step {{ ($order->shipping_status >=10) ? 'active' : '' }}"> <span class="icon"> <i
                            class="fa fa-check"></i> </span>
                    <span class="text">
                        {{ __('order.shipping_status_done') }}
                    </span>
                </div>
            </div>
            <hr>
            <form role="form" id="form-edit" action="{{ route('yhl.tracking.updatestatus') }}" method="post">
                @csrf
                <input type="hidden" name="order_id" value="{{ $order->id }}" />
                <div class="row form-group">
                    <div class="col-sm-2">
                        <label>{{ __('label.order_status') }}</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="order_status">
                            @foreach ($list_order_status as $key => $value)
                            <option class="form-control ng-pristine ng-valid ng-empty ng-touched" value="{{ $key }}"
                                {{ ( $key ==$order->order_status) ? 'selected' : '' }}>
                                {{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label>{{ __('label.shipping_status') }}</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="shipping_status">
                            @foreach ($list_shingping_status as $key => $value)
                            <option class="form-control ng-pristine ng-valid ng-empty ng-touched" value="{{ $key }}"
                                {{ ( $key ==$order->shipping_status) ? 'selected' : '' }}>
                                {{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-check"></i> {{ __('label.update') }}
                        </button class="btn btn-info">
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                @if (Session::has('complete_status'))
                <div class='alert alert-success' role="alert">
                    {{ __('label.complete') }}
                </div>
                @endif
            </form>
        </article>
        <br>
        <div class="row">
            <div class="col-sm-6">
                Tracking
                <ul class="timeline">
                    @foreach ($order_trackings as $order_tracking)
                    <li>
                        {{ $order_tracking->created_at }} -
                        {{ isset($order_tracking->user->code) ? $order_tracking->user->code : '' }}
                        : {{ $order_tracking->status }} - {{ $list_shingping_status[$order_tracking->status] }}
                        <a href="{{ route('yhl.tracking.delete', $order_tracking->id) }}" class="btn btn-xs btn-info">
                            <i class="fas fa-window-close"></i>
                        </a>
                        <p>{{ $order_tracking->note }}
                        </p>

                    </li>
                    @endforeach
                </ul>
                @if (Session::has('tracking_status'))
                <div class='alert alert-success' role="alert">
                    {{ __('label.complete') }}
                </div>
                @endif
            </div>
            <div class="col-sm-6">
                Order Log
                <ul class="timeline">
                    @foreach ($ordersLog as $orderLog)
                    <li>
                        {{ $orderLog->created_at }} -
                        {{ isset($orderLog->user->code) ? $orderLog->user->code : '' }}
                        : {{ $orderLog->status }} - {{ $status_log[$orderLog->status] }}
                        <p>{{ $orderLog->description }} </p>
                    </li>
                    @endforeach
                </ul>
            </div>

        </div>
        <div class="text-left" style="padding-top: 30px;"> <a href="{{ route('yhl.tracking.index') }}"
                class="btn btn-white">
                <i class="fa fa-chevron-left"></i> {{ __('label.back') }}
            </a>
        </div>
    </div>
</div>

@endsection
@section('footer')
@endsection
