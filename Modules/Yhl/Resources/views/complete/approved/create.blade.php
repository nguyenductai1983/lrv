@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
    <div class="panel panel-default">
        @include('yhl::layouts.complete.approved')
        <div class="panel-heading">
            {{ __('label.approved_create') }}
        </div>
        <div class="panel-body">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-group">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <form action="{{ route('yhl.complete.approved') }}" method="get" role="form"
                                        class="form-inline">
                                        <div class="form-group">
                                            <input name="from_date" id="from_date"
                                                value="{{ request()->query('from_date') }}" class="form-control" type="text"
                                                style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                                        </div>
                                        <div class="form-group">
                                            <input name="to_date" id="to_date" value="{{ request()->query('to_date') }}"
                                                class="form-control" type="text" style="width: 190px;"
                                                placeholder="{{ __('label.to_date') }}">
                                        </div>
                                        <div class="form-group">
                                            <input name="order_code" id="order_code"
                                                value="{{ request()->query('order_code') }}" class="form-control"
                                                type="text" style="width: 190px;"
                                                placeholder="{{ __('label.order_code') }}">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-white btn-single">
                                                <i class="fa fa-search"></i> {{ __('label.find') }}
                                            </button>
                                            <a href="{{ route('yhl.complete.approved') }}" class="btn btn-white btn-single">
                                                {{ __('label.clear') }}
                                            </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <form id="shipping-package-local-form" action="{{ route('yhl.complete.approved.store') }}" method="post" role="form"
                            class="form-inline">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="paginate-single">
                                    {{ $shipping_package_locals->appends(request()->query())->links() }}
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered" style="max-width: 100%;width: 100%;">
                                        <thead>
                                            <tr>
                                                <th width="3%">#</th>
                                                <th width="3%">
                                                    <input type="checkbox" id="select-all" checked="">
                                                </th>
                                                <th width="10%">{{ __('label.order_code') }}</th>
                                                <th width="15%">{{ __('label.receiver') }}</th>
                                                <th width="15%">{{ __('label.transporter') }}</th>
                                                <th width="15%">{{ __('label.transport_time') }}</th>
                                                <th width="10%">{{ __('label.partner') }}</th>
                                                <th width="10%">{{ __('label.note') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list_package_select">
                                            @if ($shipping_package_locals->count() > 0)
                                                <?php $no = 1; ?>
                                                @foreach ($shipping_package_locals as $shipping_package_local)
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        <td>
                                                            <input type="checkbox" name="shipping_package_locals[]"
                                                                value="{{ $shipping_package_local->id }}" checked="">
                                                        </td>
                                                        <td>{{ $shipping_package_local->order_code }}</td>
                                                        <td>{{ $shipping_package_local->shipping_address }}</td>
                                                        <td>{{ $shipping_package_local->user->full_name }}</td>
                                                        <td>{{ $shipping_package_local->created_date }}</td>
                                                        <td>
                                                            {{ $shipping_package_local->shipping_method_provider_name }}
                                                        </td>
                                                        <td>{{ $shipping_package_local->note }}</td>
                                                    </tr>
                                                    <?php $no++; ?>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7">{{ __('label.no_records') }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <a href="{{ route('yhl.complete.approved') }}" class="btn btn-white">
                                    <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
                                </a>
                                <button type="submit" class="btn btn-info">
                                    <i class="fa fa-check"></i> {{ __('label.stock_out') }}
                                </button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('footer')
    <script src="/js/yhl/complete.js"></script>
    <script src="/js/common.js"></script>
@endsection --}}
