@extends('layouts.admin.index', ['menu' => $menu ?? ''])

@section('body')
    <div class="panel panel-default" ng-app="InstockApp" ng-controller="InstockController" style="padding: 15px;">
        <div class="panel-heading">
            {{ __('label.create') }} Instock
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-create" name="createForm" ng-submit="createinstock()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                  <div ng-if="errors.length">
                                        <div class="col-sm-12">
                                                <li ng-repeat="error in errors">@{{ error }}</li>
                                        </div>
                                </div>
                                <div ng-if="errors.system.length">
                                    <div class="row" >
                                        <div class="col-sm-12">
                                            <h4>{{ __('express.system_error') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.system">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-2">
                        {{ __('label.warehouse_manage') }}
                    </div>
                    <div class="col col-lg-6">
                        @if($warehouses->count() > 0)
                        <select name="from_warehouse_id" ng-model="warehouse_id" class="form-control">
                            <option value="">Select</option>
                            @foreach($warehouses as $warehouse)
                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                </div>
                <div class="panel panel-color panel-gray panel-border" style="padding-bottom: 0;margin-bottom: -2px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-cubes"></i> {{ __('label.goods') }}</h3>
                    </div>
                </div>
                <div id="container" class="tabs-border">
                    <div class="tab-content" style="padding: 15px;">
                           <!--  xoa trong luong vat ly -->
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-container">
                                    <thead>
                                    <tr>
                                        <th width="50" class="text-center">#</th>
                                        <th width="150">{{ __('product.code') }}</th>
                                        <th width="150">{{ __('product.name') }}</th>
                                        <th width="10" class="text-center">{{ __('label.amount') }}</th>
                                        <th width="200" class="text-center">{{ __('label.note') }}</th>
                                        <th width="100" class="text-center">{{ __('label.action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in list_products">
                                        <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.code"
                                                   ng-change="searchProducts(product, 'list_products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-focus="searchProducts(product, 'list_products-' + $parent.$index + '-' + $index + '-dropdown')">
                                            <div id="list_products-@{{ $parent.$index + '-' + $index }}-dropdown" class="dropdown list_products-dropdown">
                                                <button class="btn btn-primary dropdown-toggle hidden" type="button" data-toggle="dropdown"></button>
                                                <ul class="dropdown-menu" style="position: inherit;">
                                                    <li>
                                                        <table class="table table-bordered table-hover" style="max-width: 200%; width: 200%;">
                                                            <tbody>
                                                            <tr ng-repeat="item in product.product_list" ng-style="!item.show && {display: 'none'}" ng-click="selectProduct(product, item)">
                                                                <td ng-bind="item.name"></td>
                                                                <td ng-bind="item.code"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control input-readonly" ng-model="product.name" readonly="readonly">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.quantity" >
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.note" >
                                        </td>
                                        <td class="col-middle">
                                            <button type="button" class="btn btn-xs btn-danger" ng-click="remove($parent.$index, $index)">
                                                <i class="fas fa-window-close"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <button type="button" class="btn btn-info" ng-click="addproduct()">
                                                <i class="fa fa-plus-square"></i> {{ __('label.add_row') }}
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <div ng-if="complete" class='alert alert-success' role="alert">
                    <h3>{{ __('label.complete')}} </h3>
                 </div>
                <div class="text-right">
                    <a href="/admin/yhl/instock" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                    <button type="submit" class="btn btn-info">
                        <i class="fa fa-check"></i> {{ __('label.save') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
                    </button>
                     <a href="@{{ new_order }}/print_bills" target="_blank" class="btn btn-info" ng-if="complete">
                                   {{ __('label.print_bill')}}   <i class="fa fa-print "></i>
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/admin/instock/instock-create.js?t=' . File::lastModified(public_path('js/admin/instock/instock-create.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-create').scope();

            scope.agency = {!! auth()->user()->agency_id !!};

            scope.init();
            scope.statues = [];

            <?php foreach(config('yhl.statues') as $status): ?>
            scope.statues.push({
                        key : parseInt({!! $status !!}),
                        name: '{!! __('label.reciver_status_' . $status) !!}'
                    });
            <?php endforeach; ?>

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr    = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
