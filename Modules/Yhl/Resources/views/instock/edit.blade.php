@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  {{-- <div class="panel panel-default" ng-app="InstockApp" ng-controller="InstockController" style="padding: 15px;"> --}}
    <div class="panel panel-default">

      <form action="{{ route('yhl.instock.update', $instock->id) }}" method="post" role="form">

        <div class="row">
          <div class="col col-lg-6">
            <input type="hidden" name="warehouse_id" value="{{ $warehouses->id }}">
            <input type="hidden" name="id" value="{{ $instock->id }}">
            <input type="text" name="warehouse_name" value="{{ $warehouses->name }}" readonly="readonly" type="text"
              class="form-control input-readonly">
          </div>
        </div>
        <div class="panel panel-color panel-gray panel-border" style="padding-bottom: 0;margin-bottom: -2px;">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cubes"></i> {{ __('label.goods') }}</h3>
          </div>
        </div>
        <div id="container" class="tabs-border">
          <div class="tab-content" style="padding: 15px;">
            <div class="table-responsive">
              <table class="table table-hover table-bordered table-container">
                <thead>
                  <tr>
                    <th width="20" class="text-center">#</th>
                    <th width="130">{{ __('product.code') }}</th>
                    <th width="153">{{ __('product.name') }}</th>
                    <th width="60" class="text-center">{{ __('label.quantity_short') }}</th>
                    <th width="200" class="text-center">{{ __('label.note') }}</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $number = 1;
                  @endphp
                  @foreach ($products as $item)

                    <tr>
                      <td class="col-middle">
                        <label>{{ $number }}</label>
                        <input type="hidden" name="product[{{ $number }}][id]" value="{{ $item->id }}">
                      </td>
                      <td>
                        <label class="text-center">{{ $item->code }} </label>
                      </td>
                      <td class="col-middle">
                        <label class="text-center">{{ $item->name }} </label>
                      </td>
                      <td class="col-middle">
                        <input type="text" name="product[{{ $number }}][amount]" value="{{ $item->amount }}"
                          class="form-control text-center">
                      </td>
                      <td class="col-middle">
                        <input type="text" name="product[{{ $number }}][note]" value="{{ $item->note }}"
                          class="form-control text-center">
                      </td>
                    </tr>
                    <?php $number++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="text-right" style="padding-top: 30px;">
          <a href="{{ route('yhl.index') }}" class="btn btn-white">
            <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
          </a>
          <button type="submit" class="btn btn-info">
            <i class="fa fa-check"></i>
            {{ __('label.update') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
          </button>
          <a href="print_bills" target="_blank" class="btn btn-info">
            Print Bill <i class="fa fa-print "></i>
          </a>
        </div>
        {{ csrf_field() }}
        {{ method_field('put') }}
      </form>
    </div>
  </div>
@endsection

@section('footer')

@endsection
