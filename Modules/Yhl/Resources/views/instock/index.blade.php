@extends('layouts.admin.index', ['menu' => $menu ?? ''])

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ __('yhl.orders_title') }}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-1">
                    <div class="form-group">
                        <a href="{{ route('yhl.instock.create') }}" class="btn btn-success btn-single">
                            <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12">
                    <form action="{{ route('yhl.index') }}" method="get" role="form" class="form-inline">
                        <div class="form-group">
                            <input name="from_date" autocomplete="off" id="from_date"
                                value="{{ request()->query('from_date') }}" class="form-control" type="text"
                                style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                        </div>
                        <div class="form-group">
                            <input name="to_date" autocomplete="off" id="to_date" value="{{ request()->query('to_date') }}"
                                class="form-control" type="text" style="width: 190px;"
                                placeholder="{{ __('label.to_date') }}">
                        </div>
                        <div class="form-group">
                            <input name="code" value="{{ request()->query('code') }}" class="form-control" type="text"
                                style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                        </div>
                        <div class="form-group">
                            <input name="receiver_phone" value="{{ request()->query('receiver_phone') }}"
                                class="form-control" type="text" style="width: 190px;"
                                placeholder="{{ __('label.receiver_phone') }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-white btn-single">
                                <i class="fa fa-search"></i> {{ __('label.find') }}
                            </button>
                            <a href="{{ route('yhl.index') }}" class="btn btn-white btn-single">
                                {{ __('label.clear') }}
                            </a>
                            <button type="button" class="btn btn-white btn-single" onclick="order.export();">
                                {{ __('label.export_excel') }} <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <form id="shipment-package-form">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" style="min-width: 800px;">
                        <thead>
                            <tr>
                                <th width="80">{{ __('label.action') }}</th>
                                <th width="50">{{ __('label.form_code') }}
                                </th>
                                <th width="150">{{ __('label.date') }} </th>
                                <th width="80">{{ __('label.employee') }}</th>

                                <th width="120">{{ __('label.warehouse_stockout') }}</th>
                                <th width="120">{{ __('label.note') }}</th>
                            </tr>
                        </thead>
                        <tbody id="shipment-package-items">
                            @if ($orders->count() > 0)
                                <?php $no = 1; ?>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>
                                            <a href="{{ route('yhl.instock.edit', $order->id) }}" class="btn btn-xs btn-info">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('yhl.instock.show', $order->id) }}" target="_blank"
                                                class="btn btn-xs btn-info">
                                                <i class="fa fa-print "></i>
                                            </a>
                                            @if ($order->payment_status == 2 || $order->payment_status == 3)
                                                <button type="button" class="btn btn-xs btn-info"
                                                    onclick="extent.extra({{ $order->id }}, '{{ csrf_token() }}');">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-info"
                                                    onclick="extent.refund({{ $order->id }}, '{{ csrf_token() }}');">
                                                    <i class="fa fa-reply"></i>
                                                </button>
                                            @endif
                                            @if ($order->order_status != $order->status_cancel && $order->shipping_status == $order->shipping_status_revicer)
                                                <button type="button" class="btn btn-xs btn-danger"
                                                    onclick="order.cancel({{ $order->id }});">
                                                    <i class="fa fa-close"></i>
                                                </button>
                                            @endif
                                        </td>
                                        <td class="text-center">{{ $order->id }} </td>
                                        <td class="text-center">{{ $order->created_date }} </br>
                                            {{ date_format($order->updated_at, 'd/m/Y') }}</td>
                                        <td>{{ isset($order->user->code) ? $order->user->code : 'ADM' }}</td>
                                        <td>{{ $order->from_warehouse_name }}</td>
                                        <td>{{ $order->user_note }}</td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">{{ __('label.no_records') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="paginate-single">
                    {{ $orders->appends(request()->query())->links() }}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="/js/admin/app/extent.js"></script>
    <script src="/js/yhl/tracking.js"></script>
    <script src="/js/yhl/order.js"></script>
    <script>
        $(function() {
            $("#from_date").datepicker({
                dateFormat: "{{ config('app.date_format_js') }}"
            }).datepicker('setDate', -30);
            $("#to_date").datepicker({
                dateFormat: "{{ config('app.date_format_js') }}"
            }).datepicker('setDate', +1);
        });

    </script>
@endsection
