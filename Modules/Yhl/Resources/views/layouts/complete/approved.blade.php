                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="{{ route('yhl.complete.approved') }}">{{ __('yhl.shipping_list') }} GMN</a>
                    </li>
                    <li>
                        <a href="{{ route('yhl.complete.carrierpickup') }}">{{ __('yhl.order_completion_confirmation') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('yhl.complete.done') }}">{{ __('yhl.ship_local_status_done') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('yhl.complete.cancel') }}">{{ __('yhl.ship_local_status_reject') }}</a>
                    </li>
                </ul>
