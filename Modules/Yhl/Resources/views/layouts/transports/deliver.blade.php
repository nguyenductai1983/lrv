                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="{{ route('yhl.complete.approved') }}">{{ __('yhl.ship_local_status_approve') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('yhl.complete.carrierpickup') }}">{{ __('label.stock_out') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('yhl.complete.done') }}">{{ __('yhl.ship_local_status_done') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('yhl.complete.cancel') }}">{{ __('yhl.ship_local_status_reject') }}</a>
                    </li>
                </ul>
