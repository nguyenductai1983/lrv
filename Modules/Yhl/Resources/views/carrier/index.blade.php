@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('yhl.goods_title')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('yhl.carrier.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('yhl.carrier.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="paginate-single">
                {{ $orders->appends(request()->query())->links() }}
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered" style="min-width: 120%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                <input type="checkbox" id="select-all">
                            </th>
                            <th>{{ __('label.order_code') }}</th>
                            <th class="text-center">{{ __('label.date') }}</th>
                            <th>{{ __('label.goods_info') }}</th>
                            <th>{{ __('warehouse.manage') }}</th>
                            <th>{{ __('label.note') }}</th>
                            <th>{{ __('label.province') }}</th>
                            <th>{{ __('label.city') }}</th>
                            <th width="120">{{ __('label.status') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        @if($order->shipping_status == $order->shipping_status_ready_to_ship && !$order->is_delivery)
                        <tr>
                            <td>{{ $no }}</td>
                            <td>
                                <input type="checkbox" name="order_ids[]" value="{{ $order->id }}">
                            </td>
                            <td>{{ $order->code }}</td>
                            <td>{{ $order->created_date }}</td>
                            <td>{{ $order->description }}</td>
                            <td>{{ isset($order->warehouse->name) ? $order->warehouse->name : '' }}</td>
                            <td>{{ $order->user_note }}</td>
                            <td>{{ $order->receiver_province_name }}</td>
                            <td>{{ $order->receiver_city_name }}</td>
                            <td>
                                <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if($Picksessions->count() > 0)
            <div class="text-right">
              <label> GHTK Pickup Session : <label>
            <select name="pick_session" class="btn btn-success" id="pick_session">
                @foreach($Picksessions as $Picksession)
                <option value="{{ $Picksession->code }}">{{ $Picksession->name }}</option>
                @endforeach
            </select>
            </div>
            @endif
        </form>
        <div class="text-right">
            @if($shipping_providers->count() > 0)
            @foreach($shipping_providers as $shipping_provider)
            <button type="button" class="btn btn-info" onclick="carrier.pushItems({{ $shipping_provider->id }});">
                <img height="50" src={{ isset($shipping_provider->file) ? $shipping_provider->file->path : '' }}>
                <i class="fa fa-check"></i> {{ $shipping_provider->name }}
            </button>
            @endforeach
            @endif


        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/yhl/carrier.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    language = JSON.parse(language);
    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
