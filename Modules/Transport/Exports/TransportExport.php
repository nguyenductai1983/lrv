<?php

namespace Modules\Transport\Exports;

use Auth;
use App\User;
use App\Order;
use App\Enum\OrderTypeEnum;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class TransportExport implements FromQuery, WithHeadings
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function headings(): array
    {
        return [
            'code',
            'created_at',
            'sender_full_name',
            'customer_note',
            'user_note',
            'description',
            'sender_email',
            'sender_phone',
            'sender_cellphone',
            'sender_address',
            'sender_post_code',
            'sender_countrie',
            'sender_province',
            'sender_city',
            'receiver_email',
            'receive_full_name',
            'receiver_cellphone',
            'receiver_phone',
            'receiver_address',
            'receiver_post_code',
            'receiver_countrie',
            'receiver_province',
            'receiver_city',
            'is_crr',
            'total_weight',
            'total_final',
            'total_paid_amount',
            'total_surcharge_fee',
            'total_insurrance_fee',
            'total_remain_amount',
            'total_declare_price',
            'agency_discount',
            'order_status',
            'shipping_status',
            'payment_status',
            'receive_status',
            'sender_first_name',
            'sender_middle_name',
            'sender_last_name',
            'receive_first_name',
            'receiver_middle_name',
            'receive_last_name'
        ];
    }
    public function query()
    {

        $query = Order::where([]);
        $query->select('tbl_order.code','tbl_order.created_at',
            DB::raw("CONCAT(sender_first_name,sender_middle_name,sender_last_name) as sender_full_name"),
            'customer_note', 'user_note', 'description', 'sender_email', 'sender_phone', 'sender_cellphone', 'sender_address', 'sender_post_code',
                'nuoc_gui.name as sender_countrie','tinh_gui.name as sender_province',
                'thanh_pho_gui.name as sender_city','receiver_email',
            DB::raw("CONCAT(receive_first_name,receiver_middle_name,receive_last_name) as receive_full_name"),
             'receiver_cellphone', 'receiver_phone', 'receiver_address', 'receiver_post_code','nuoc_nhan.name as receiver_countrie', 'tinh_nhan.name as receiver_province','thanh_pho_nhan.name as receiver_city', 'is_crr', 'total_weight', 'total_final', 'total_paid_amount', 'total_surcharge_fee', 'total_insurrance_fee','total_remain_amount', 'total_declare_price', 'agency_discount', 'order_status', 'shipping_status','payment_status', 'receive_status',
            'sender_first_name', 'sender_middle_name', 'sender_last_name',
            'receive_first_name', 'receiver_middle_name', 'receive_last_name' );
        // lấy thông tin nước gửiprovinces
        $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id' );
        // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id' );
        // lấy thông tin tỉnh gửi
         $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id' );
         // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id' );
        // lấy thông tin thành phố gửi
         $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id' );
         // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id' );
        $user = Auth::user();
        $request= $this->request;
         if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('tbl_order.agency_id');

        }else{
            $query->where('tbl_order.user_id', '=', $user->id);
        }
        $query->where('tbl_order.type', '=', OrderTypeEnum::TRANSPORT);
        if (!empty($request->query('from_date'))) {
          $arrday=  explode('/', $request->query('from_date'));
          $tr=$arrday[2]."-" .$arrday[1]."-".$arrday[0]." 00:00:00";
            $query->where('tbl_order.created_at', '>',$tr);
        }
        if (!empty($request->query('to_date'))) {
            $arrday=  explode('/', $request->query('to_date'));
            $query->where('tbl_order.created_at', '<', $arrday[2]."-" .$arrday[1]."-".$arrday[0]. " 23:59:59");
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('tbl_order.shipping_status', '=', $request->query('tbl_order.shipping_status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_order.code', 'like', '%'.$request->query('code').'%');
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('tbl_order.sender_phone', '=', $request->query('sender_phone'));
        }
        $query->get();
        return $query;
    }
}
