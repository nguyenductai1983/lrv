<?php

namespace Modules\Transport\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
class ExportTable implements FromQuery, WithHeadings
{
    use Exportable;
    public function __construct($request, $header)
    {
        $this->request = $request;
        $this->header = $header;
    }
    public function headings(): array
    {
        $header2 =array_keys($this->header);
        return [$header2];
    }

    public function query()
    {
        $query=$this->request ;
        $query->get();
        return $query;
    }
}
