<?php

namespace Modules\Transport\Exports;

use Auth;
use App\User;
use App\Order;
use App\Enum\OrderTypeEnum;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ExportView implements FromView
{
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function view(): View
    {
        $query = Order::where([]);
        $user = Auth::user();
        $request= $this->request;
         if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('tbl_order.agency_id');

        }else{
            $query->where('tbl_order.user_id', '=', $user->id);
        }
        $query->where('tbl_order.type', '=', OrderTypeEnum::TRANSPORT);
        if (!empty($request->query('from_date'))) {
          $arrday=  explode('/', $request->query('from_date'));
          $tr=$arrday[2]."-" .$arrday[1]."-".$arrday[0]." 00:00:00";
            $query->where('tbl_order.created_at', '>',$tr);
        }
        if (!empty($request->query('to_date'))) {
            $arrday=  explode('/', $request->query('to_date'));
            $query->where('tbl_order.created_at', '<', $arrday[2]."-" .$arrday[1]."-".$arrday[0]. " 23:59:59");
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('tbl_order.shipping_status', '=', $request->query('tbl_order.shipping_status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_order.code', 'like', '%'.$request->query('code').'%');
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('tbl_order.sender_phone', '=', $request->query('sender_phone'));
        }
        $test= $query->get();
    return view('transport::home.viewlist', [
        'users' => $test
    ]);
    }
}
