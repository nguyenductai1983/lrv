@extends('layouts.admin.index', ['menu' => $menu])
@section('head')
<link rel="stylesheet" href="{{ asset('css/admin/user.css') }}">
@endsection
@section('body')
    <div class="panel panel-default transport-content" ng-app="TransportApp" ng-controller="TransportEditController">
        <div class="transport-content-title panel-heading">
           {{ __('transport.send-to-vn') }} <strong>{{ $transport->code }}</strong>
        </div>
        <div class="panel-body ng-cloak">
            <form id="form-edit" name="editForm" ng-submit="editTransport()" novalidate>
                <div class="modal fade custom-width" id="modal-errors">
                    <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i> {{ __('label.error') }}</h4>
                            </div>
                            <div class="modal-body text-danger">
                                <div class="row" ng-if="errors.sender.length || errors.receiver.length">
                                    <div class="col-sm-12">
                                        <div ng-if="errors.sender.length">
                                            <h4>{{ __('label.sender') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.sender">@{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div ng-if="errors.receiver.length">
                                            <h4>{{ __('label.receiver') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.container.length">
                                    <div class="row" ng-repeat="error in errors.container" ng-if="error.others.length || error.products.length">
                                        <div class="col-sm-12">
                                            <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                                            <ul ng-if="error.others.length">
                                                <li ng-repeat="other in error.others">@{{ other }}</li>
                                            </ul>
                                            <ul ng-if="error.products.length">
                                                <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                                                    {{ __('label.product') }} @{{ $index + 1 }}
                                                    <ul>
                                                        <li ng-repeat="err in productErrors">@{{ err }}</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if="errors.system.length">
                                    <div class="row" >
                                        <div class="col-sm-12">
                                            <h4>{{ __('express.system_error') }}</h4>
                                            <ul>
                                                <li ng-repeat="error in errors.system">@{{ error }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">
                                    {{ __('label.close') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-gray">
                    <table class="transport-content-shipping table table-form">
                        <tbody>
                        <tr>
                            <td class="transport-content-sender">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.sender') }} ({{ $transport->customer_id }})</h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="sender.first_name" ng-model="sender.first_name">
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('customer.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.middle_name" ng-model="sender.middle_name">
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('customer.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.last_name" ng-model="sender.last_name">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_1" ng-model="sender.address_1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.address_2" ng-model="sender.address_2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.email') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="sender.email" ng-model="sender.email" readonly="">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="sender.country_id" class="form-control" ng-model="sender.country_id" ng-change="getProvincesSender()">
                                                <option value="">{{ __('label.select_country') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.province_id" class="form-control" ng-model="sender.province_id" ng-change="getCitiesSender()">
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesSender" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="sender.city_id" class="form-control" ng-model="sender.city_id" ng-change="updateContainers(false)">
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesSender" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.postal_code" ng-model="sender.postal_code" ng-change="updateContainers(false)" ng-blur="sender.postal_code = sender.postal_code.split(' ').join('')">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="sender.telephone" ng-model="sender.telephone">
                                        </td>
                                        <td class="col-label">
                                            {{ __('express.ship_date') }}
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="shipping-date" name="sender.shipping_date" ng-model="sender.shipping_date">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2" class="col-label" style="padding-bottom: 10px;">
                                            <button type="button" class="btn btn-info btn-xs"
                                                    ng-click="showMoreInfoCustomer = !showMoreInfoCustomer">
                                                <i class="fa fa-plus"></i> {{ __('label.more_info') }}
                                            </button>
                                        </td>
                                    </tr>
                                    <tr ng-show="showMoreInfoCustomer">
                                        <td class="col-label">
                                            {{ __('customer.id_card') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="customer.id_card"
                                                ng-model="customer.id_card" ng-change="checkInfoCustomer()">
                                        </td>
                                        <td class="col-label">
                                            {{ __('customer.card_expire') }}
                                        </td>
                                        <td>
                                            <div class="input-group date-picker" data-change-year="true"
                                                data-change-month="true"
                                                data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                <input type="text" class="form-control" name="customer.card_expire"
                                                    ng-model="customer.card_expire"
                                                    ng-change="checkInfoCustomer()">

                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-white">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr ng-show="showMoreInfoCustomer">
                                        <td class="col-label">
                                            {{ __('customer.birthday') }}
                                        </td>
                                        <td>
                                            <div class="input-group date-picker" data-change-year="true"
                                                data-change-month="true"
                                                data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                                                <input type="text" class="form-control" name="customer.birthday"
                                                    ng-model="customer.birthday" ng-change="checkInfoCustomer()">

                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-white">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="col-label">
                                            {{ __('customer.career') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="customer.career"
                                                ng-model="customer.career" ng-change="checkInfoCustomer()">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="transport-content-receiver">
                                <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }}</h3>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="20%">
                                            <input type="text" class="form-control" name="receiver.first_name" ng-model="receiver.first_name" >
                                        </td>
                                        <td width="15%" class="col-label">
                                            {{ __('receiver.middle_name') }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.middle_name" ng-model="receiver.middle_name" >
                                        </td>
                                        <td width="10%" class="col-label">
                                            {{ __('receiver.last_name') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.last_name" ng-model="receiver.last_name" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.address') }}<i class="text-danger">*</i>
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address" ng-model="receiver.address_1" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('customer.address_2') }}
                                        </td>
                                        <td colspan="5">
                                            <input type="text" class="form-control" name="receiver.address_2" ng-model="receiver.address_2" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-form">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td width="30%">
                                            <select name="receiver.country_id" class="form-control" ng-model="receiver.country_id" ng-change="getProvincesReceiver()" >
                                                <option value="">{{ __('label.select_address') }}</option>
                                                <option ng-repeat="country in countries" ng-value="country.id">@{{ country.code + '-' + country.name }}</option>
                                            </select>
                                        </td>
                                        <td width="20%" class="col-label">
                                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.province_id" class="form-control" ng-model="receiver.province_id" ng-change="getCitiesReceiver()" >
                                                <option value="">{{ __('label.select_province') }}</option>
                                                <option ng-repeat="province in provincesReceiver" ng-value="province.id">@{{ province.name }}</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <select name="receiver.city_id" class="form-control" ng-model="receiver.city_id" ng-change="getWardsReceiver()" >
                                                <option value="">{{ __('label.select_city') }}</option>
                                                <option ng-repeat="city in citiesReceiver" ng-value="city.id">@{{ city.name }}</option>
                                            </select>
                                        </td>
                                        {{-- ngày 22-04-2020 --}}
                                        <td class="col-label" ng-hide="ward">
                                            {{ __('receiver.ward_id') }}<i class="text-danger">*</i>
                                        </td>
                                        <td ng-hide="ward">
                                            <select name="receiver.ward_id" class="form-control" ng-model="receiver.ward_id" ng-change="btnEditset()"
                                            ng-disabled="Updatereciver && receiver.ward_id">
                                                <option value="">{{ __('label.select_ward') }}</option>
                                                <option ng-repeat="ward in wardsReceiver" ng-value="ward.id">@{{ ward.name }}</option>
                                            </select>
                                        </td>
                                        {{-- het doan them --}}
                                    </tr>
                                    <tr>
                                        <td class="col-label">
                                            {{ __('receiver.telephone') }}<i class="text-danger">*</i>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.telephone" ng-model="receiver.telephone" >
                                        </td>

                                        <td class="col-label">{{ __('receiver.cellphone') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="receiver.cellphone" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-label">{{ __('receiver.postal_code') }}</td>
                                        <td>
                                            <input type="text" class="form-control" name="receiver.post_code" ng-model="receiver.postal_code" ng-blur="receiver.postal_code = receiver.postal_code.split(' ').join('')" >
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="container-fluid">
                    <div ng-show="showMoreInfoCustomer" class="row">
                        <div class="col-sm-3 col-xs-4">
                            <div class="col-label">
                                {{ __('customer.image_1_file_id') }}
                            </div>
                            @include('partials.form-controls.image', ['field' => 'customer.image_1_file_id', 'file' => null,
                            'Update' => 'Updatecustomer'])
                        </div>
                        <div class="col-sm-3 col-xs-4">
                            <div class="col-label">
                                {{ __('customer.image_2_file_id') }}
                            </div>
                            @include('partials.form-controls.image', ['field' => 'customer.image_2_file_id', 'file' => null,
                            'Update' => 'Updatecustomer'])
                        </div>
                        <div class="col-sm-3 col-xs-4">
                            <div class="col-label">
                                {{ __('customer.image_3_file_id') }}
                            </div>
                            @include('partials.form-controls.image', ['field' => 'customer.image_3_file_id', 'file' => null,
                            'Update' => 'Updatecustomer'])
                        </div>
                    </div>
                </div>
                <h3 class="transport-content-package-title">
                    <i class="fa fa-cubes"></i> {{ __('label.goods') }}
                </h3>

                <div id="container" class="tabs-border transport-content-package">
                    <ul class="nav nav-tabs tabs-border">
                        <li ng-repeat="container in transport.containers" class="tab" ng-class="{'active' : $first}">
                            <a href="#container-@{{ $index + 1 }}" data-toggle="tab" target="_self">
                                <span>{{ __('label.container') }} @{{ $index + 1 }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript://" ng-click="addContainer()">
                                <span><i class="fa fa-plus"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="padding: 15px;">
                        <div ng-repeat="container in transport.containers" class="tab-pane" ng-class="{'active' : $first}" id="container-@{{ $index + 1 }}">
                            <div class="form-group">
                                <table class="table table-form table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="150" class="text-right col-middle">{{ __('label.total_weight') }}:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.total_weight.toFixed(2)"></strong>
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.length') }}:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.length" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.width') }}:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.width" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="70" class="text-right col-middle">{{ __('label.height') }}:</td>
                                        <td width="80">
                                            <input type="text" class="form-control text-center" ng-model="container.height" ng-change="updateVolume(container)">
                                        </td>
                                        <td width="100" class="text-right col-middle">{{ __('label.volume') }}:</td>
                                        <td width="70" class="col-middle">
                                            <strong ng-bind="container.volume.toFixed(2)"></strong>
                                        </td>
                                        <td class="col-middle">
                                            <button type="button" class="btn btn-info btn-xs" ng-click="setVolume(container)">
                                                <i class="fa fa-check"></i> {{ __('label.select') }}
                                            </button>
                                        </td>
                                        <td ng-if="containers.length > 1" class="col-middle">
                                            <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#modal-delete-container-@{{ $index }}">
                                                <i class="fas fa-trash"></i> {{ __('label.delete_container') }} @{{ $index + 1 }}
                                            </button>
                                            <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">{{ __('label.confirm_delete') }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! __('label.confirm_delete_msg') !!}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger disabled-submit" ng-click="deleteContainer($index)">
                                                                <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                                            </button>
                                                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('label.cancel') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-container" style="max-width: 120%;width: 120%;">
                                    <thead>
                                    <tr>
                                        <th width="30" class="text-center">#</th>
                                        <th width="100">{{ __('product.code') }}</th>
                                        <th width="100">{{ __('product.name') }}</th>
                                        <th width="30" class="text-center">{{ __('label.quantity') }}</th>
                                        <th width="30" class="text-center">{{ __('label.weight') }}</th>
                                        <th width="60" class="text-right">{{ __('label.price') }}</th>
                                        <th width="60" class="text-right">{{ __('label.discount_short') }}(%)</th>
                                        <th width="60" class="text-center">{{ __('label.unit_short') }}</th>
                                        <th width="30" class="text-right">{{ __('label.declared_value_short') }}</th>
                                        <th width="60" class="text-right">{{ __('label.surcharge') }}</th>
                                        <th width="60" class="text-center">{{ __('label.insurance_short') }}</th>
                                        <th width="60" class="text-right">{{ __('label.total') }}</th>
                                        <th width="150">{{ __('label.note') }}</th>
                                        <th width="50" class="text-right">{{ __('label.action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="product in container.products">
                                        <td class="col-middle text-center" ng-bind="$index + 1"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.code"
                                                   ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown')"
                                                   ng-disabled="container.shipping_status > 1">

                                            <div id="products-@{{ $parent.$index + '-' + $index }}-dropdown"
                                                 class="dropdown products-dropdown">
                                                <button class="btn btn-primary dropdown-toggle hidden" type="button"
                                                        data-toggle="dropdown"></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <table class="table table-bordered table-hover">
                                                            <tbody>
                                                            <tr ng-repeat="item in products"
                                                                ng-style="!item.show && {display: 'none'}"
                                                                ng-click="selectProduct(product, item)">
                                                                <td ng-bind="item.name"></td>
                                                                <td ng-bind="item.code"></td>
                                                                <td class="text-right" ng-bind="item.price"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        {{-- <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.code">
                                        </td> --}}
                                        <td class="col-middle">
                                            <input type="text" class="form-control input-readonly" ng-model="product.name" readonly="readonly">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.quantity" ng-change="updateContainers(false)">
                                        </td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.weight" ng-change="updateContainers(false)">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.unit_goods_fee"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-center" ng-model="product.per_discount" ng-if="isEditDiscount" ng-change="updateContainers(false)" ng-disabled="container.shipping_status > 1">
                                            <input type="text" class="form-control text-center" ng-model="product.per_discount" ng-if="!isEditDiscount" ng-click="showPopupVip()" ng-disabled="container.shipping_status > 1" readonly>
                                        </td>
                                        <td class="text-center col-middle" ng-bind="product.unit"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control text-right" ng-model="product.declared_value" ng-change="updateContainers(false)">
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.surcharge.toFixed(2)"></td>
                                        <td class="text-center col-middle">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" class="iswitch iswitch-info" style="margin: 0;" ng-model="product.is_insurance" ng-checked="product.is_insurance" ng-change="updateContainers(false)">
                                                    </td>
                                                    <td ng-bind="product.insurance.toFixed(2)"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="text-right col-middle" ng-bind="product.total.toFixed(2)"></td>
                                        <td class="col-middle">
                                            <input type="text" class="form-control" ng-model="product.note">
                                        </td>
                                        <td class="col-middle">
                                            <button type="button" class="btn btn-xs btn-danger" ng-click="remove($parent.$index, $index)">
                                            <i class="fas fa-window-close"></i> </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <button type="button" class="btn btn-info" ng-click="addproduct($index)">
                                        <i class="fa fa-plus-square"></i> {{ __('label.add_row') }}
                                        </button>
                                    </tr>
                                <tr>
                                    <td class="text-right" width="15%"></td>
                                    <td class="text-left" width="19%"></td>
                                    <td class="col-middle text-right" width="15%">{{ __('label.coupon_code') }}:</td>
                                    <td class="text-left" width="18%">
                                        <div class="input-group">
                                            <input type="text" class="form-control" ng-model="container.coupon_code" readonly>
                                        </div>
                                    </td>
                                    <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                    <td class="text-left">
                                        <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-right" width="15%">{{ __('label.shipping_fee') }}:</td>
                                    <td class="text-left" width="19%"><strong ng-bind="container.shipping_fee.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_weight') }}:</td>
                                    <td class="text-left" width="18%"><strong ng-bind="container.total_weight.toFixed(2)"></strong></td>
                                    <td class="text-right" width="15%">{{ __('label.total_shipping_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_shipping_fee.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_declared_value') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_declared_value.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_surcharge') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_insurance') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_insurance.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_amount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_amount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_fee.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.min_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.min_fee.toFixed(2)"></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-right">{{ __('label.total_charge_fee') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_charge_fee.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_discount') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total_discount.toFixed(2)"></strong></td>
                                    <td class="text-right">{{ __('label.total_pay') }}:</td>
                                    <td class="text-left"><strong ng-bind="container.total.toFixed(2)"></strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-map-marker"></i> {{ __('transport.point-receiver') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-form">
                            <tbody>
                                <tr>
                                    <td colspan="1">
                                        <strong>{{ __('label.choose-option') }}:</strong>
                                    </td>
                                </tr>
                                <tr ng-repeat="warehouse in warehouses">
                                    <td class="col-label" width="90%">
                                        <label>
                                            <input type="radio" name="warehouseId" ng-model="transport.warehouse_id" ng-value="warehouse.id" ng-click="updateContainers(false)" ng-checked="@{{warehouse.id == transport.warehouse_id}}">
                                            @{{ warehouse.name}}
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-label">
                                        <label style="display:  block;float:  left;">{{ __('express.schedule_pickup') }}</label>
                                        <select ng-model="pickup.is_schedule" ng-change="changeSchedulePickup();" class="form-control" style="display:  block;width: 150px;margin-left:  10px;float: left;">
                                            <option value="1">{{ __('label.no') }}</option>
                                            <option value="2">{{ __('label.yes') }}</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="pickup.is_schedule == 2" class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-shopping-cart"></i> {{ __('express.pickup_info') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="10%" class="text-right">{{ __('express.contact_name') }}<i class="text-danger">*</i></td>
                                <td width="20%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.contact_name">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.phone_number') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <input type="text" class="form-control" ng-model="pickup.phone_number">
                                </td>
                                <td width="10%" class="text-right">{{ __('express.pickup_location') }}<i class="text-danger">*</i></td>
                                <td width="25%" class="text-left">
                                    <select class="form-control" ng-model="pickup.location">
                                        <option ng-repeat="location in pickupLocation" ng-value="location.id">@{{ location.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">{{ __('express.pickup_date') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <div class="input-group date-picker" data-change-year="true"
                                         data-change-month="true"
                                         data-year-range="{{ date('Y') }}:{{ date('Y') + 10 }}">
                                        <input type="text" class="form-control" ng-model="pickup.date_time">

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-white">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">{{ __('express.pickup_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.start_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>
                                    <select class="form-control time-option" ng-model="pickup.start_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                                <td class="text-right">{{ __('express.closing_time') }}<i class="text-danger">*</i></td>
                                <td class="text-left">
                                    <select class="form-control time-option" ng-model="pickup.closing_hour_time">
                                        <option ng-repeat="hourTime in hourTimes" ng-value="hourTime.id">@{{ hourTime.name }}</option>
                                    </select>:
                                    <select class="form-control time-option" ng-model="pickup.closing_minute_time">
                                        <option ng-repeat="minuteTime in minuteTimes" ng-value="minuteTime.id">@{{ minuteTime.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div ng-if="quotes.length" class="transport-carrer">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-truck"></i> {{ __('transport.carrer-local') }}
                    </h3>
                    <div class="quote-eshipper-list panel-body">
                        <table class="table table-border">
                            <thead>
                            <tr>
                                <th></th>
                                <th width="15%">{{ __('express.carrier') }}</th>
                                <th width="15%">{{ __('express.service') }}</th>
                                <th width="15%">{{ __('express.est_transitday') }}</th>
                                <th width="10%">{{ __('express.base_charge') }}</th>
                                <th width="15%">{{ __('express.orther_surcharge') }}</th>
                                <th width="15%">{{ __('express.fuel_surcharge') }}</th>
                                <th width="10%">{{ __('express.total') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="quote in quotes.slice(0, 5)">
                                <td >
                                    <input type="radio" name="serviceId" ng-value="$index+1" ng-click="setCarrer($index)" ng-checked="@{{quote.serviceId == selectQuote.serviceId}}">
                                </td>
                                <td>
                                    @{{ quote.carrierName}}
                                </td>
                                <td >
                                    @{{ quote.serviceName}}
                                </td>
                                <td >
                                    @{{ quote.transitDays}}
                                </td>
                                <td>
                                    @{{ quote.baseCharge | number : 2 }} @{{  quote.currency }}
                                </td>
                                <td>
                                    <div ng-repeat="surcharge in quote.surcharge">
                                        <span class="text-left">@{{ surcharge.name }}</span>
                                        <span class="text-right">@{{ surcharge.amount | number : 2 }} @{{  quote.currency }}</span>
                                    </div>
                                </td>
                                <td >
                                    @{{ quote.fuelSurcharge | number : 2}} @{{  quote.currency }}
                                </td>
                                <td >
                                    @{{ quote.totalCharge | number : 2}} @{{  quote.currency }}
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <input type="radio" name="serviceId" ng-click="setNoCarrer()" ng-checked="@{{!selectQuote}}">
                                </td>
                                <td colspan="7">
                                    {{ __('transport.auto-shipper') }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div ng-if="chooseCarrer" class="transport-quote">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-money"></i> {{ __('transport.quote-cal') }}
                    </h3>
                    <div class="quote-shipping-result">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.date-shipping') }}</strong></td>
                                <td colspan="2" class="text-center" width="50%"><strong>{{ __('transport.service-fee') }}</strong></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-right"></td>
                                <td>{{ __('transport.shipping-discount-local-fee') }}:</td>
                                <td class="text-right">
                                    <input type="text" class="form-control text-right" ng-if="transport.transport_status == 1" ng-model="transport.discount_international_fee" ng-change="updateContainers()">
                                    <strong ng-if="transport.transport_status != 1" ng-bind="transport.discount_international_fee.toFixed(2)"></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_sender"></strong></td>
                                <td>{{ __('transport.shipping-local-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_local.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.igreen-receiver') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.date_receiver"></strong></td>
                                <td>{{ __('transport.send-tovn-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping_international.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('transport.date-send-vn') }}:</td>
                                <td class="text-right">{{ __('transport.to') }} <strong ng-bind="transport.date_from_shipping"></strong> - <strong ng-bind="transport.date_to_shipping"></strong></td>
                                <td>{{ __('transport.total-fee') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.total_shipping.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td>{{ __('label.shipping_status') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.eshiper_status_name"></strong></td>
                                <td>{{ __('label.payment_status') }}:</td>
                                <td class="text-right"><strong ng-bind="transport.payment_status"></strong></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-left"></td>
                                <td>{{ __('label.status') }}:</td>
                                <td class="text-right">
                                    <select class="form-control" ng-model="transport.receive_status">
                                        <option ng-repeat="status in statues" ng-value="status.key">@{{ status.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td> User node:
                                </td>
                                <td colspan="3">
                                    <textarea class="form-control" type="text" ng-model="customer_note"></textarea>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div ng-if="transport.transport_status == 2" class="transport-quote">
                    <h3 class="transport-content-package-title">
                        <i class="fa fa-money"></i> {{ __('transport.charge-fee') }}
                    </h3>
                    <div class="quote-shipping-result">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="text-right">{{ __('transport.shipping-local-fee') }}:</td>
                                <td class="text-left"><strong ng-bind="transport.charge_foreign_fee.toFixed(2)"></strong></td>
                                <td class="text-right">{{ __('transport.send-tovn-fee') }}:</td>
                                <td class="text-left"><strong ng-bind="transport.total_shipping_international.toFixed(2)"></strong></td>
                                <td class="text-right">{{ __('transport.total-fee') }}:</td>
                                <td class="text-left"><strong ng-bind="transport.charge_total_final.toFixed(2)"></strong></td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.date_pay') }}:</td>
                                <td class="col-middle text-left">
                                    <div class="input-group date-picker" data-change-year="true"
                                         data-change-month="true"
                                         data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                        <input type="text" class="form-control" ng-model="transport.last_date_pay">

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-white">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-middle text-right">{{ __('label.pay') }}:</td>
                                <td class="col-middle text-left">
                                    <input type="text" class="form-control text-right"
                                           ng-model="transport.last_paid_amount">
                                </td>
                                <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                                <td class="col-middle text-left">
                                    <select id="payMethod" class="form-control"
                                            ng-model="transport.pay_method"
                                            ng-disabled="container.shipping_status > 1">
                                        <option ng-repeat="method in methods"
                                                ng-value="method.code">@{{ method.name }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-middle text-right">{{ __('label.paid') }}:</td>
                                <td class="text-left" style="padding-top:18px">
                                    <strong class="ng-binding" ng-bind="transport.total_paid_amount"></strong>
                                </td>
                                <td class="col-middle text-right">{{ __('label.last_paid_time') }}:</td>
                                <td class="text-left" style="padding-top:18px">
                                    <strong class="ng-binding" ng-bind="transport.last_payment_at"></strong>
                                </td>
                                <td class="col-middle text-right">{{ __('label.currency_pay') }}:</td>
                                <td class="text-left" style="padding-top:18px">
                                    <strong class="ng-binding">{{ auth()->user()->agency->currency->code }}</strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-right">
                    <a href="{{ route('transport.quote.index') }}" class="btn btn-white m-3">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                    <button type="button" class="btn btn-info" ng-if="bntSearch" ng-click="searchExpress()" ng-disabled="submittedSearch">
                        <i class="fa fa-search"></i> {{ __('label.search') }} <i class="fa fa-refresh fa-spin" ng-if="submittedSearch"></i>
                    </button>
                    <button type="button" class="btn btn-primary" ng-if="bntConfirm" ng-click="editTransport()" ng-disabled="submittedEdit">
                        <i class="fa fa-edit text-success"></i> {{ __('label.edit') }} <i class="fa fa-refresh fa-spin" ng-if="submittedEdit"></i>
                    </button>
                    <button type="button" class="btn btn-danger" ng-if="bntCancel" ng-click="cancelOrder()">
                        <i class="fa fa-times"></i> {{ __('order.cancel') }} <i class="fa fa-refresh fa-spin" ng-if="submittedCancel"></i>
                    </button>
                    <button type="button" class="btn btn-success" ng-if="bntConfirm" ng-click="approveTransport()" ng-disabled="submittedConfirm">
                        <i class="fa fa-check"></i> {{ __('label.approve') }} <i class="fa fa-refresh fa-spin" ng-if="submittedConfirm"></i>
                    </button>
                    <button type="button" class="btn btn-success" ng-if="bntGetShipping" ng-click="getShipping()" ng-disabled="submittedGetShipping">
                        <i class="fa fa-check"></i> {{ __('order.get_shipping_status') }} <i class="fa fa-refresh fa-spin" ng-if="submittedGetShipping"></i>
                    </button>
                    <button type="button" class="btn btn-success" ng-if="bntPayment" ng-click="paymentTransport()" ng-disabled="submittedPayment">
                        <i class="fa fa-check"></i> {{ __('order.payment') }} <i class="fa fa-refresh fa-spin" ng-if="submittedPayment"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/admin/app/transport-customer-edit.js?t=' . File::lastModified(public_path('js/admin/app/transport-customer-edit.js'))) }}"></script>
    <script>
        $(function () {
            var scope = angular.element('#form-edit').scope();

            scope.configSurcharge = {!! $configSurcharge->toJson() !!};
            scope.configInsurance = {!! $configInsurance->toJson() !!};
            scope.caPriority = {!! json_encode($caPriority) !!};
            scope.vnPriority = {!! json_encode($vnPriority) !!};
            scope.caVnPriorityFee = {!! $caVnPriorityFee !!};
            scope.caVnPriorityDefaultFee = {!! $caVnPriorityDefaultFee !!};
            scope.caVnDefaultPriorityFee = {!! $caVnDefaultPriorityFee !!};
            scope.caVnDefaultFee = {!! $caVnDefaultFee !!};
            scope.discountLevel = {!! json_encode($discountLevel) !!};
            scope.id = {!! request()->route('id') !!};

            scope.getTransport();

            scope.statues = [];
            <?php foreach(config('transport.statues') as $status): ?>
            scope.statues.push({
                key: parseInt({!! $status !!}),
                name: '{!! __('label.reciver_status_' . $status) !!}'
            });
            <?php endforeach; ?>

            if (!scope.$$phase) {
                scope.$apply();
            }

            $(document).on('hidden.bs.dropdown', '.dropdown', function () {
                $(this).find('.table tbody tr').removeClass('hover');
            });

            $(document).on('mouseenter', '.products-dropdown .table tbody tr', function () {
                $('.products-dropdown .table tbody tr').removeClass('hover');
                $(this).addClass('hover');
            });

            $(document).on('keydown', function (event) {
                if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
                    var table = $(this).find('.products-dropdown .table'),
                            tr    = table.find('tbody tr.hover');
                    if (tr.length === 0) {
                        return true;
                    }
                    event.preventDefault();
                    if (event.keyCode === 40) {
                        tr = table.find('tbody tr.hover').next(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').first();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 38) {
                        tr = table.find('tbody tr.hover').prev(':visible');
                        if (tr.length === 0) {
                            tr = table.find('tbody tr:visible').last();
                        }
                        tr.trigger('mouseenter');
                    } else if (event.keyCode === 13) {
                        tr.trigger('click');
                    }
                }
            });
        });
    </script>
@endsection
