@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default user">
    <div class="panel-heading">
        {{ __('transport.orders_title')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.quote.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.quote.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
              <input class="form-control" id="mySearch" type="text" placeholder= "{{ __('label.filter_list') }}">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" style="min-width: 2500px;">
                    <thead>
                        <tr>
                          <th width="150">{{ __('label.form_code') }} / {{ __('label.action') }}</th>
                            <th width="120">{{ __('label.order_status') }}</th>
                            <th width="120">{{ __('label.payment_status') }}</th>
                            <th width="180">{{ __('label.sender') }}</th>
                            <th width="180">{{ __('label.receiver') }}</th>
                             <th width="200" class="text-center">{{ __('label.date') }}</th>
                            <th width="160" class="text-center">{{ __('label.total_weight') }}(Lbs)</th>
                            <th width="160" class="text-center">{{ __('label.amount') }}(CAD)</th>
                            <th width="110" class="text-center">{{ __('label.total_discount') }}</th>
                            <th width="110" class="text-center">{{ __('label.pay') }}</th>
                            <th width="180" class="text-center">{{ __('label.partner') }}</th>
                            <th width="120">{{ __('express.eshipper_order_id') }}</th>
                            <th width="420">{{ __('label.address') }}</th>
                            <th width="250">{{ __('label.telephone') }}</th>
                            <th width="120">{{ __('customer.code') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($transports->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($transports as $transport)
                        <tr>
                            <td>  {{ $transport->code }} </br>
                                <a href="{{ route('transport.quote.edit', $transport->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @php if(!empty($transport->eshiper_order_id)){ @endphp
                                <button type="button" class="btn btn-xs btn-info" onclick="order.viewHistoryEshiper({{ $transport->id }})">
                                    <i class="fa fa-truck"></i>
                                </button>
                                @php } @endphp
                                <?php if(!empty($transport->eshipper_label)){?>
                                <a href="{{ route('transport.quote.pdf', $transport->id) }}" class="btn btn-xs btn-primary" download="label.pdf">
                                    <i class="fa fa-file-pdf"></i>
                                </a>
                                <?php }?>
                            </td>
                            <td>
                                <span class="{{ $transport->status_label }}">{{ $transport->status_name }}</span>
                                 <span class="{{ $transport->eshipper_status_label }}">{{ $transport->eshipper_status_name }}</span>
                            </td>
                            <td>
                                <span class="{{ $transport->payment_status_label }}">{{ $transport->payment_status_name }}</span>
                            </td>
                            <td class="text-center">{{ $transport->sender_first_name }} {{ $transport->sender_middle_name }}
                            {{ $transport->sender_last_name }}</td>
                             <td class="text-center">{{ $transport->receive_first_name }} {{ $transport->receiver_middle_name }}
                            {{ $transport->receive_last_name }}</td>
                             <td class="text-center">{{ $transport->created_at }}</td>
                            <td class="text-center">{{ number_format($transport->total_weight, 2) }}</td>
                            <td class="text-center">{{ number_format($transport->total_final, 2) }}</td>
                            <td class="text-center">{{ number_format($transport->total_discount, 2) }}
                                       @if($transport->total_final >0)
                                <br>
                           ({{ number_format($transport->total_discount/$transport->total_final, 3)*100 }}%)
                            @endif
                            </td>
                            <td class="text-center">{{ number_format($transport->total_paid_amount, 2) }}</td>
                            <td class="text-center">{{ $transport->partner_name }}</td>
                            <td> {{ $transport->eshiper_order_id }} </td>
                              <td>{{ $transport->receiver_address }} - {{ $transport->cities_name }} - {{ $transport->provinces_name }} - {{ $transport->countries_name }}</td>
                              <td>{{ $transport->receiver_phone }}</td>
                              <td class="text-center">{{ $transport->customer_id }}</td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $transports->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/order.js"></script>
<script src="/js/admin/app/extent.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
