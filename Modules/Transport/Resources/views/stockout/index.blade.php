@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.shipment_title') }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.stockout.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.shipment_code') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.stockout.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('label.shipment_code') }}</th>
                        <th>{{ __('label.employee') }}</th>
                        <th>{{ __('label.create_time') }}</th>
                        <th>{{ __('label.estimate_delivery') }}</th>
                        <th>{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($shipments->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($shipments as $shipment)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $shipment->code }}</td>
                        <td>{{ $shipment->first_name ? $shipment->first_name : '' }} {{ $shipment->middle_name ? $shipment->middle_name : '' }} {{ $shipment->last_name ? $shipment->last_name : '' }}</td>
                        <td>{{ $shipment->created_at }}</td>
                        <td>{{ $shipment->estimate_delivery }}</td>
                        <td>
                            <a href="{{ route('transport.stockout.view', $shipment->id) }}" class="btn btn-xs btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $shipments->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection