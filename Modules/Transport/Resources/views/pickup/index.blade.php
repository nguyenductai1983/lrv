@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.pickup.index') }}" method="get" role="form" class="form-inline">

                <div class="form-group">
                    <input name="order_code" id="order_code" value="{{ request()->query('order_code') }}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code')}}">
                    <button type="reset" class="form-control btn-clear"><i class="fa fa-times" aria-hidden="true"></i></button>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-white btn-single">
                        <i class="fa fa-search"></i> {{ __('label.find') }}
                    </button>
                    <input type="reset" class="btn btn-white btn-single" value="{{ __('label.clear') }}">
                </div>
            </form>
        </div>
    </div>
    <form id="pickup-package-form">
        <div class="table-responsive">
            <table class="table table-hover table-bordered" style="min-width: 120%;">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th><input type="checkbox" id="select-all"></th>
                        <th width="15%">{{ __('label.order_code')}}</th>
                        <th width="20%">{{ __('label.customer') }}</th>
                        <th width="15%" class="text-center">{{ __('label.date') }}</th>
                        <th width="20%">{{ __('label.goods_info')}}</th>
                        <th width="35%">{{ __('label.note')}}</th>
                        <th width="10%">{{ __('label.status')}}</th>
                    </tr>
                </thead>
                <tbody id="pickup-items">
                    @if($orders->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>
                            @if($order->order_status == 1)
                            <input type="checkbox" name="order_ids[]" value="{{ $order->id }}" checked="">
                            @elseif($order->order_status == $order->order_status)
                            <input type="checkbox" name="order_ids[]" value="{{ $order->id }}">
                            @endif
                        </td>
                        <td>{{ $order->code }}</td>
                        <td>{{ $order->sender_full_name }}</td>
                        <td class="text-center">{{ $order->created_date }}</td>
                        <td>{{ $order->products }}</td>
                        <td>{{ $order->user_note }}</td>
                        <td>
                            <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $orders->appends(request()->query())->links() }}
        </div>
    </form>
    <div class="text-right">
        <a href="{{ route('transport.pickup.index') }}" class="btn btn-white">
            <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
        </a>
        <button type="button" class="btn btn-info" onclick="stockout.pushItems();">
            <i class="fa fa-check"></i> {{ __('transport.menu_pick_up')}}
        </button>
    </div>
</div>
</div>
@endsection

@section('footer')
<script src="/js/transport/pickup.js"></script>
<script src="/js/transport/tracking.js"></script>
<script>
    $("#order_code").on('keypress', function (event) {
        if (event.which === 13) {
        }
    });

    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#pickup-items input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#pickup-items input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
