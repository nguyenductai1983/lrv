@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.goods_title')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.carrier.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}"
                            class="form-control" type="text" style="width: 190px;"
                            placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control"
                            type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;"
                            placeholder="{{ __('label.order_code') }}">
                        <button type="reset" class="form-control btn-clear"><i class="fa fa-times"
                                aria-hidden="true"></i></button>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.carrier.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="paginate-single">
                {{ $orders->appends(request()->query())->links() }}
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                <input type="checkbox" id="select-all">
                            </th>
                            <th>{{ __('label.order_code') }}</th>
                            <th width="15%">{{ __('label.goods_info') }}</th>
                            <th>{{ __('label.province') }}</th>
                            <th>{{ __('label.city') }}</th>
                            <th>{{ __('label.shipment_code') }}</th>
                            <th>{{ __('label.warehouse_manage') }}</th>
                            <!--   <th width="25%">{{ __('label.note') }}</th>  -->
                            <th>{{ __('label.status') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        @if($order->shipping_status == $order->shipping_status_revicer)
                        <tr>

                            <td>{{ $no }}</td>
                            <td>
                                <input type="checkbox" name="order_ids[]" value="{{ $order->id }}">
                            </td>
                            <td>
                                <a href="{{ route('transport.tracking.showphone',$order->id) }}" target="_blank">
                                    {{ $order->code }} <i class="fa fa-phone-square" aria-hidden="true"
                                        class="btn btn-info"></i>
                                </a>

                                <span class="text-info {{ ($order->shipping_fast > 0) ? '' : 'hidden' }}"> <br><i class="fas fa-fast-forward"></i>
                                    {{ __('label.fast_shipping') }} </span>

                                <span class="text-danger {{ ($order->danger_type > 0) ? '' : 'hidden' }}"> <br><i class="fab fa-gripfire"></i>
                                    {{ __('label.flammable_goods') }}</span>

                                <span class="text-warning {{ ((int)$order->total_remain_amount>0) ? '' : 'hidden' }}"> <br><i class="fas fa-hand-holding-usd"></i>
                                    {{ __('label.cod') }} </span>

                            </td>
                            <td>{{ $order->products }}</td>
                            <td>{{ $order->receiver_province_name }}</td>
                            <td>{{ $order->receiver_city_name }}</td>
                            <td>{{ isset($order->shipment->code) ? $order->shipment->code : '' }}</td>
                            <td>{{ isset($order->shipment_warehouse_name) ? $order->shipment_warehouse_name : '' }}
                            </td>
                            <!--  <td>{{ $order->user_note }}</td> -->
                            <td>

                                <span
                                    class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if($Picksessions->count() > 0)
            <div class="text-right">
                <label> GHTK Pickup Session : <label>
                        <select name="pick_session" class="btn btn-success" id="pick_session">
                            @foreach($Picksessions as $Picksession)
                            <option value="{{ $Picksession->code }}">{{ $Picksession->name }}</option>
                            @endforeach
                        </select>
            </div>
            @endif
        </form>
        <div class="text-right">
            @if($shipping_providers->count() > 0)
            @foreach($shipping_providers as $shipping_provider)
            <button type="button" class="btn btn-info" onclick="carrier.pushItems({{ $shipping_provider->id }});">
                <img height="50" src={{ isset($shipping_provider->file) ? $shipping_provider->file->path : '' }}> <i
                    class="fa fa-check"></i> {{ $shipping_provider->name }}
            </button>
            @endforeach
            @endif
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/carrier.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    language = JSON.parse(language);
    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
