@extends('layouts.admin.printlandscape')
@section('body')
<form name="form_print" method="post" action="" id="form_print" style="page-break-after: auto;">
    <div id="pnin">
        <center>
            <span id="ctl01_lbdata">
                <div class="page" style="height:205mm;width: 295mm;">
                    <div class="subpage" style="padding:0px">
                        <div>
                            <div class="head">
                                <div class="a" style="float:left;width:55%;text-align:left">
                                    <img src="{{ asset('images/logo.jpg') }}" style="width:120px" height="30px"><br>
                                    Toll free: 1 (888) 619-6869<br>
                                    Web: www.igreencorp.com
                                </div>
                                <div style="float:Right;width:44%;text-align:left;font-size:20pt;">
                                    PHIẾU XUẤT HÀNG GMN<br>
                                    <span style="font-size:15px">Mã phiếu #: {{ $ListShippingPackage->code }}</span><br>
                                    <span style="font-size:15px">Ngày: {{ $ListShippingPackage->created_at }}</span>
                                </div>
                                <div style="clear:both"></div>
                            </div>

                            <div style="clear:both"></div>
                            <table border="1px" cellspacing="0px" cellpadding="5px" width="100%"
                                style="margin-top:10px">
                                <tbody>
                                    <tr align="center" style="background:#F1F1F1">
                                        <th>STT</th>
                                        <th>Mã hàng</th>
                                        <th>Người nhận</th>
                                        <th>Địa chỉ</th>
                                        <th>Điện thoại</th>
                                        <th>Sản phẩm <br>(Description)</th>
                                        <th>Phí</th>
                                        <th width='10%'>Kí nhận</th>
                                    </tr>
                                    @php $no = 1; @endphp
                                    @foreach ($orders as $order)
                                        <tr align="center">
                                            <td>{{ $no }}</td>
                                            <td>{{ $order->code }} <br>
                                                {{ $order->tracking_code }}

                                                {{-- {{ $order->shipping_method_provider_name }} --}}
                                            </td>
                                            <td>{{ $order->receive_last_name }} {{ $order->receive_middle_name }} {{ $order->receive_first_name }}</td>
                                            <td>{{ $order->receiver_address }},
                                                @php if(!empty($order->receiver_ward->name)){ @endphp
                                                {{ $order->receiver_ward->name }} @php } @endphp
                                                , {{ $order->receiver_city->name }}, {{ $order->receiver_province->name }}</td>
                                            <td>{{ $order->receiver_phone }} </td>

                                            <td align="left">
                                                @foreach ($order->order_items as $order_item)
                                               <strong> {{ number_format($order_item->quantity, 0) }} </strong>
                                                {{ $order_item->name }}. <br/>
                                                @endforeach
                                            </td>
                                            <td>{{ number_format($order->carrier_fee,0) }} vnd</td>
                                            <td></td>
                                        </tr>
                                        @php $no++; @endphp
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </span>
            <span id="ctl01_lbthongbao">
                <script language="javascript">
                    window.print();

                </script>
            </span>
        </center>
    </div>
    <span id="lbthongbao"></span>
</form>
@endsection
