@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.goods_title')}}
    </div>
    <div class="panel-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-pills">
                <li >
                    <a href="{{ route('transport.deliver.approved') }}">{{ __('transport.ship_local_status_approve')}}</a>
                </li>
                <li>
                    <a href="{{ route('transport.deliver.listghtk') }}">{{ __('transport.ship_local_status_approve')}} GHTK</a>
                </li>
                <li class="active">
                    <a href="{{ route('transport.deliver.carrierpickup') }}">{{ __('transport.ship_local_status_carrier')}}</a>
                </li>
                <li>
                    <a href="{{ route('transport.deliver.done') }}">{{ __('transport.ship_local_status_done')}}</a>
                </li>
                <li>
                    <a href="{{ route('transport.deliver.cancel') }}">{{ __('transport.ship_local_status_reject')}}</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="panel-group">
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <form action="{{ route('transport.deliver.carrierpickup') }}" method="get" role="form" class="form-inline">
                                    <div class="form-group">
                                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                                    </div>
                                    <div class="form-group">
                                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                                    </div>
                                    <div class="form-group">
                                        <input name="order_code" id="order_code" value="{{ request()->query('order_code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-white btn-single">
                                            <i class="fa fa-search"></i> {{ __('label.find') }}
                                        </button>
                                        <a href="{{ route('transport.deliver.carrierpickup') }}" class="btn btn-white btn-single">
                                            {{ __('label.clear') }}
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <form id="shipping-package-local-form">
                            <div class="paginate-single">
                                {{ $shipping_package_locals->appends(request()->query())->links() }}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" style="max-width: 180%;width: 180%;">
                                    <thead>
                                        <tr>
                                            <th width="3%">#</th>
                                            <th width="3%">
                                                <input type="checkbox" id="select-all" checked="">
                                            </th>
                                            <th width="8%">{{ __('label.partner') }}</th>
                                            {{-- <th width="8%">{{ __('label.order_code') }}</th> --}}
                                            <th width="8%">{{ __('label.note') }}</th>
                                            <th width="8%">{{ __('label.shipping_code') }}</th>
                                            <th width="8%">{{ __('label.shipment_code') }}</th>
                                            {{-- <th width="8%">{{ __('label.tracking_code') }}</th> --}}
                                            <th width="10%">{{ __('label.receiver') }}</th>
                                            <th width="10%">{{ __('label.sender') }}</th>
                                            <th width="5%">{{ __('label.stock_out_time') }}</th>
                                            <th width="10%">{{ __('label.goods_info') }}</th>
                                            {{-- <th width="5%">{{ __('label.note') }}</th> --}}
                                            {{-- <th width="20%">{{ __('label.api_response') }}</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody id="shipment-package-items">
                                        @if($shipping_package_locals->count() > 0)
                                        <?php $no = 1; ?>
                                        @foreach($shipping_package_locals as $shipping_package_local)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>
                                                <input type="checkbox" name="shipping_package_locals[0][]" value="{{ $shipping_package_local->id }}" checked="">
                                            </td>
                                            <td>
                                                {{ $shipping_package_local->shipping_method_provider_name }}</br>
                                                {{ $shipping_package_local->tracking_code}}
                                            </td>
                                            {{-- <td>{{ $shipping_package_local->order_code }}</td> --}}
                                            <td>
                                                <input name="shipping_package_locals[1][]"  class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.note') }}">
                                            </td>
                                            <td>{{ $shipping_package_local->shipping_code }}
                                                <br>
                                                <span class="{{ $shipping_package_local->order->status_label }}">
                                                    {{ $shipping_package_local->order->status_name }}
                                                </span>
                                                <span class="{{ $shipping_package_local->order->shipping_status_label }}">
                                                    {{ $shipping_package_local->order->shipping_status_name }}
                                                </span>
                                            </td>
                                            <td>{{ $shipping_package_local->shipment_code }}</td>
                                            {{-- <td>{{ $shipping_package_local->tracking_code }}</td> --}}
                                            <td>{{ $shipping_package_local->shipping_address }}</td>
                                            <td>{{ $shipping_package_local->sender_address }}</td>
                                            <td>{{ $shipping_package_local->sent_date }}</td>
                                            <td>
                                                <strong>{{ __('label.weight')}}:</strong> {{ number_format($shipping_package_local->weight, 0) }} {{ $shipping_package_local->weight_unit_code }} </br>
                                                <strong>{{ __('label.dimension')}}:</strong> {{ number_format($shipping_package_local->height, 0) }} x {{ number_format($shipping_package_local->length, 0) }} x {{ number_format($shipping_package_local->width, 0) }} {{ $shipping_package_local->dimension_unit_code }} </br>
                                                <strong>{{ __('label.goods_info')}}:</strong> {{ number_format($shipping_package_local->total_declare_goods, 0) }} {{ $shipping_package_local->currency_code }} </br>
                                                <strong>{{ __('label.shipping_fee')}}:</strong> {{ number_format($shipping_package_local->carrier_fee, 2) }} {{ $shipping_package_local->currency_code }} </br>
                                            </td>
                                            {{-- <td>{{ $shipping_package_local->note }}</td> --}}
                                            {{-- <td>{{ $shipping_package_local->last_response_api }}</td> --}}
                                        </tr>
                                        <?php $no++; ?>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="7">{{ __('label.no_records') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class="text-right">
                            <button type="button" class="btn btn-info" onclick="deliver.customerReceiver();">
                                <i class="fa fa-check"></i> {{ __('transport.ship_local_status_done')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/deliver.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    language = JSON.parse(language);
    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
