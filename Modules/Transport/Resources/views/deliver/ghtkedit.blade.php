@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <ul class="nav nav-pills">
        <li >
            <a href="{{ route('transport.deliver.approved') }}">{{ __('transport.ship_local_status_approve')}}</a>
        </li>
        <li class="active">
            <a href="{{ route('transport.deliver.listghtk') }}">{{ __('transport.ship_local_status_approve')}} GHTK</a>
        </li>
        <li>
            <a href="{{ route('transport.deliver.carrierpickup') }}">{{ __('transport.ship_local_status_carrier')}}</a>
        </li>
        <li>
            <a href="{{ route('transport.deliver.done') }}">{{ __('transport.ship_local_status_done')}}</a>
        </li>
        <li>
            <a href="{{ route('transport.deliver.cancel') }}">{{ __('transport.ship_local_status_reject')}}</a>
        </li>
    </ul>
    <div class="panel-heading">
        {{ __('label.approved_view') }} {{ $list_shipping_package->code }}
    </div>
    <div class="panel-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <form id="package-info-form" method="POST" class="form-inline"
                    action="{{ route('transport.deliver.editlistghtk',$list_shipping_package->id) }}">
                    <div class="form-group">
                        <input name="list_shipping_packages" id="list_shipping_packages" type="hidden"
                            value="{{ $list_shipping_package->id }}">
                        <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;"
                            placeholder="{{ __('label.order_code')}}">
                        @if($Picksessions->count() > 0)
                        <select name="pick_session" class="btn btn-success" id="pick_session">
                            @foreach($Picksessions as $Picksession)
                            <option value="{{ $Picksession->code }}">{{ $Picksession->name }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find')}}
                        </button>
                    </div>
                    {{ csrf_field() }}
                </form>
                <div class="tab-pane active">
                    <div class="panel-group">
                        <form id="shipping-package-local-form"
                            method="post" role="form" class="form-inline">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="paginate-single">
                                {{ $shipping_package_locals->appends(request()->query())->links() }}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" style="max-width: 100%;width: 100%;">
                                    <thead>
                                        <tr>
                                            <th width="3%">#</th>
                                            {{-- <th width="3%">
                                                    <input type="checkbox" id="select-all" checked="">
                                                </th> --}}
                                            <th width="10%">{{ __('label.order_code') }}</th>
                                            <th width="15%">{{ __('label.receiver') }}</th>
                                            <th width="15%">{{ __('label.transporter') }}</th>
                                            <th width="15%">{{ __('label.transport_time') }}</th>
                                            <th width="10%">{{ __('label.partner') }}</th>
                                            <th width="10%">{{ __('label.note') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list_package_select">
                                        @if ($shipping_package_locals->count() > 0)
                                        <?php $no = 1; ?>
                                        @foreach ($shipping_package_locals as $shipping_package_local)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            {{-- <td>
                                                            <input type="checkbox" name="shipping_package_locals[]"
                                                                value="{{ $shipping_package_local->id }}" checked="">
                                            </td> --}}
                                            <td>{{ $shipping_package_local->order_code }}</td>
                                            <td>{{ $shipping_package_local->shipping_address }}</td>
                                            <td>{{ $shipping_package_local->user->full_name }}</td>
                                            <td>{{ $shipping_package_local->created_date }}</td>
                                            <td>
                                                {{ $shipping_package_local->shipping_method_provider_name }}
                                            </td>
                                            <td>{{ $shipping_package_local->note }}</td>
                                        </tr>
                                        <?php $no++; ?>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="7">{{ __('label.no_records') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-right">
                                <a href="{{ route('transport.deliver.listghtk') }}" class="btn btn-white">
                                    <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
                                </a>
                                <a href="{{ route('transport.deliver.printlistghtk', $list_shipping_package->id) }}"
                                    target="_blank" class="btn btn-info">
                                    <i class="fa fa-print" aria-hidden="true"></i> {{ __('label.print_bill')}}
                                </a>
                                {{-- <button type="submit" class="btn btn-info">
                                    <i class="fa fa-check"></i> {{ __('label.update') }}
                                </button> --}}
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/ghtkedit.js"></script>

{{-- <script src="/js/common.js"></script> --}}
@endsection
