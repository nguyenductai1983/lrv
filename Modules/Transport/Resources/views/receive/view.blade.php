@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('transport.shipment_view') }}: {{ $shipment->code }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-12">
          <form action="{{ route('transport.receive.view', $shipment->id) }}" method="get" role="form"
            class="form-inline" id="form">
            <div class="form-group">
              <input name="estimate_delivery" value="{{ $shipment->estimate_delivery }}" class="form-control" type="text"
                style="width: 190px;" placeholder="{{ __('label.date') }}" readonly="">
            </div>
            <div class="form-group">
              <select name="to_warehouse_id" class="form-control" readonly="">
                @if ($warehouses->count() > 0)
                  @foreach ($warehouses as $warehouse)
                    <option value="{{ $warehouse->id }}"
                      {{ $shipment->to_warehouse_id == $warehouse->id ? 'selected' : '' }}>
                      {{ $warehouse->name }}
                    </option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;"
                placeholder="{{ __('label.order_code') }}">
                <button type="button" class="btn btn-white btn-single" id="bt_switch_barcode" onclick="switch_barcode()"
                id="toggle-on">
                <i class="fas fa-barcode" id="switch_barcode"></i>
              </button>
              <button type="submit" class="btn btn-white btn-single">
                <i class="fa fa-search"></i> {{ __('label.find') }}
              </button>
              <a href="{{ route('transport.receive.view', $shipment->id) }}" class="btn btn-white btn-single">
                {{ __('label.clear') }}
              </a>
            </div>
          </form>
          <div>
            <p class="text-info">{{ __('label.barcode_switch') }}</p>
            <p class="text-danger" id="error_alert"></p>
        </div>
        </div>
      </div>
      <form id="shipment-package-form">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>
                  <input type="checkbox" id="select-all">
                </th>
                <th width="13%">{{ __('label.order_code') }}</th>
                <th>{{ __('label.goods_info') }}</th>
                <th>{{ __('label.note') }}</th>
                <th>{{ __('label.status') }}</th>
              </tr>
            </thead>
            <tbody id="shipment-package-items">
              @if ($orders->count() > 0)
                <?php $no = 1; ?>
                @foreach ($orders as $order)
                  <tr>
                    <td>{{ $no }}</td>
                    <td>
                      @if ($order->shipping_status == $order->shipping_status_revicer)
                        <input type="checkbox" name="order_ids[]" value="{{ $order->id }}" id="{{ $order->code }}"
                          checked="">
                      @elseif($order->shipping_status == $order->shipping_status_stock_out)
                        <input type="checkbox" name="order_ids[]" value="{{ $order->id }}" id="{{ $order->code }}">
                      @endif
                    </td>
                    <td>{{ $order->code }}</td>
                    <td>{{ $order->products }}</td>
                    <td>{{ $order->user_note }}</td>
                    <td>
                      <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                    </td>
                  </tr>
                  <?php $no++; ?>
                @endforeach
              @else
                <tr>
                  <td colspan="7">{{ __('label.no_records') }}</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </form>
      <div class="text-right">
        <a href="{{ route('transport.receive.index') }}" class="btn btn-white">
          <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
        </a>
        <button type="button" class="btn btn-info" onclick="receive.pushItems({{ $shipment->id }});">
          <i class="fa fa-check"></i> {{ __('label.receiver') }}
        </button>
      </div>
    </div>
  </div>
@endsection

@section('footer')
  <script src="/js/transport/receive.js"></script>
  <script>
     // xu ly nhận tín hiệu enter
   let barcode = false;
    let count;
    $("#order_code").on('keypress', function(event) {
      if (event.which === 13) {
        receive.checkItem(document.getElementById('order_code').value);
        event.preventDefault();
        document.getElementById('order_code').value = '';
        return false;
      }
    });
    function switch_barcode() {
      var btn_x = document.getElementById("bt_switch_barcode");
      if (barcode === false) {
        barcode = true;
        // x.style.display = "inline";
        btn_x.className = "btn btn-info btn-single";
        btn_x.style.fontSize = '18pt';
        count = setInterval(timer, 100)
      } else {
        barcode = false;
        // x.style.display = "none";
        btn_x.style.fontSize = '';
        btn_x.className = "btn btn-white btn-single";
        clearInterval(count)
      }
    }
    // xu ly mở tắt barcode
    function timer() {
      var focusbox = document.getElementById("order_code");
      focusbox.focus();
    }
    // xu ly nhận tín hiệu enter
    $('#select-all').click(function(event) {
      if (this.checked) {
        $('#shipment-package-items input[type=checkbox]').each(function() {
          this.checked = true;
        });
      } else {
        $('#shipment-package-items input[type=checkbox]').each(function() {
          this.checked = false;
        });
      }
    });
  </script>
@endsection
