@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.orders_title')}}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <a href="{{ route('transport.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add')}}
                    </a>
                </div>
            </div>
            <div class="col-sm-6 text-right">
                <div class="form-group">
                    <a href="https://form.jotform.com/60794463126257" class="btn btn-warning btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.pick_up_online')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.from_date') }}" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 160px;" placeholder="{{ __('label.to_date') }}" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input name="code" id="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                        </div>
                    <div class="form-group">
                        <input name="receiver_phone" value="{{ request()->query('receiver_phone')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.receiver_phone') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.index') }}" class="btn btn-white btn-single">
                            <i class="fas fa-broom"></i> {{ __('label.clear') }}
                        </a>
                         <button type="button" class="btn btn-white btn-single" onclick="order.export();">
                            <i class="fas fa-file-excel"></i> {{ __('label.export_excel') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
              <input class="form-control" id="mySearch" type="text" placeholder= "{{ __('label.filter_list') }}">
            <div class="table-responsive" id="table_transport">
                <table class="table table-hover table-bordered" style="min-width: 2700px;">
                    <thead>
                        <tr>
                             <th width="50">#</th>
                            <th width="120">{{ __('label.action') }}</th>
                            <th width="150">{{ __('label.form_code') }}</th>
                            <th width="120">{{ __('label.status') }} <br> {{ __('label.shipping_status') }}</th>
                           <!-- <th width="70">{{ __('label.shipment_code') }}</th> -->
                            <th width="120">{{ __('label.customer') }}</th>
                            <th width="120">{{ __('label.receiver') }}</th>
                            <th width="100" class="text-center">{{ __('label.date') }}</th>
                             <th width="120">{{ __('label.product_id') }}</th>
                            <th width="120">{{ __('label.note') }}</th>
                            <th width="120" class="text-right">{{ __('label.total_pay') }} (CAD)</th>
                            <th width="120" class="text-right">{{ __('label.total_weight') }} (Lbs)</th>
                            <th width="110" class="text-right">{{ __('label.pay') }} (CAD)</th>
                            <th width="110" class="text-right">{{ __('label.total_discount') }} (CAD)</th>
                            <th width="110" class="text-right">{{ __('label.commission') }} (CAD)</th>
                             <th width="100" class="text-right">{{ __('label.coupon_code') }}</th>
                            <th width="100" class="text-right">{{ __('label.coupon_amount') }} (CAD)</th>
                            <th width="110">{{ __('label.partner') }}</th>
                            <th width="160">{{ __('label.address') }}</th>
                            <th width="140">{{ __('label.city') }}</th>
                            <th width="140">{{ __('label.province') }}</th>
                            <th width="120">{{ __('label.telephone') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr {{ ($order->shipping_fast) ? 'class=bg-success text-white' : '' }}>
                             <td>
                                {{ $no }}
                            </td>
                            <td>
                                @if(auth()->user()->role->admin)
                                <a title="{{ __('title.order_detail') }}" href="{{ route('transport.detail', $order->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                                @endif
                                <a title="{{ __('title.order_edit') }}" href="{{ route('transport.edit', $order->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a title="{{ __('title.print_label_receiver') }}" href="{{ route('transport.print_invoice',$order->id) }}" target="_blank"
                                    class="btn btn-xs btn-info">
                                    <i class="fas fa-file-invoice" ></i>
                                </a>

                                <button title="{{ __('title.order_tracking') }}" type="button" class="btn btn-xs btn-info" onclick="tracking.viewHistory({{ $order->id }});">
                                    <i class="fa fa-truck"></i>
                                </button>
                                @if($order->payment_status == 2 || $order->payment_status == 3)
                                <a title="{{ __('title.extra_fee') }}" href="extrafee/{{$order->id}}/list" class="btn btn-xs btn-info">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <button title="{{ __('title.refund_fee') }}" type="button" class="btn btn-xs btn-info" onclick="extent.refund({{$order->id}}, '{{ csrf_token() }}');">
                                    <i class="fa fa-reply"></i>
                                </button>
                                @endif
                                @if($order->order_status != $order->status_cancel && $order->shipping_status == $order->shipping_status_new)
                                <button title="{{ __('title.order_cancel') }}" type="button" class="btn btn-xs btn-danger" onclick="order.cancel({{ $order->id }});">
                                    <i class="fas fa-window-close"></i>
                                </button>
                                @endif
                                @if($order->order_status == $order->status_cancel && $order->shipping_status == $order->shipping_status_new)
                                <button title="{{ __('title.order_restore') }}" type="button" class="btn btn-xs btn-success" onclick="order.restore({{ $order->id }});">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                </button>
                                @endif
                                <a title="{{ __('title.print_bill') }}" href="{{ route('transport.prints',$order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                    <i class="fa fa-print"></i>
                                </a>
                            </td>
                             <td>{{ $order->code }} <br/>
                                @if(!isset($order->shipment))
                                {{ isset($order->warehouse_package->code) ? $order->warehouse_package->code : '' }}
                                @endif
                                 {{ isset($order->shipment->name) ? $order->shipment->name : '' }} <br>
                                 {{ isset($order->voucher->code) ? $order->voucher->code : '' }}
                             </td>
                            <td>
                                <span class="{{ $order->status_label }}">{{ $order->status_name }}</span>
                                 <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                           <!-- <td>{{ isset($order->user->code) ? $order->user->code : 'ADM' }}</td>-->
                            <td>{{ $order->sender_full_name }}</td>
                            <td>{{ $order->receive_full_name }}</td>
                            <td class="text-center">{{ $order->created_date }}</td>
                             <td>{{ $order->products }}</td>
                            <td>{{ $order->user_note }}</td>
                            <td class="text-right">{{ number_format($order->total_final, 2) }}</td>
                            <td class="text-right">{{ number_format($order->total_weight, 2) }}</td>
                            <td class="text-right">{{ number_format($order->total_paid_amount, 2) }}</td>
                            <td class="text-right">{{ number_format($order->total_discount, 2) }}
                            @if($order->total_goods_fee >0)
                                <br>
                           ({{ number_format($order->total_discount/$order->total_goods_fee, 3)*100 }}%)
                            @endif
                            </td>
                            <td class="text-right">{{ number_format($order->agency_discount, 2) }}
                            </td>
                             <td>{{ $order->coupon_code }}</td>
                            <td class="text-right">{{ number_format($order->coupon_amount, 2) }}
                            </td>
                            <td>{{ $order->shipping_method_name }}</td>
                            <td>{{ $order->receiver_address }}</td>
                            <td>{{ $order->receiver_city_name }}</td>
                            <td>{{ $order->receiver_province_name }}</td>
                            <td>
                                {{ $order->receiver_phone }}<br>
                                {{ $order->receiver_cellphone }}
                            </td>
                            <td>
                                <a href="{{ route('transport.edit', $order->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <button type="button" onclick="order.print({{ $order->id }});" class="btn btn-xs btn-info">
                                    <i class="fa fa-print "></i>
                                </button>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.viewHistory({{ $order->id }});">
                                    <i class="fa fa-truck"></i>
                                </button>
                                @if($order->payment_status == 2 || $order->payment_status == 3)
                                <a href="extrafee/{{$order->id}}/list" class="btn btn-xs btn-info">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <button type="button" class="btn btn-xs btn-info" onclick="extent.refund({{$order->id}}, '{{ csrf_token() }}');">
                                    <i class="fa fa-reply"></i>
                                </button>
                                @endif
                                @if($order->order_status != $order->status_cancel && $order->shipping_status == $order->shipping_status_new)
                                <button type="button" class="btn btn-xs btn-danger" onclick="order.cancel({{ $order->id }});">
                                    <i class="fas fa-window-close"></i>
                                </button>
                                @endif
                                @if($order->order_status == $order->status_cancel && $order->shipping_status == $order->shipping_status_new)
                                <button type="button" class="btn btn-xs btn-success" onclick="order.restore({{ $order->id }});">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                </button>
                                @endif
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $orders->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/admin/app/extent.js"></script>
<script src="/js/transport/tracking.js"></script>
<script src="/js/transport/order.js"></script>
<script>
    $(function () {
        if($('#from_date').val()==="")
        {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', -30);
        }
        else
        {
            $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        }
       if($("#to_date").val()==="")
       {
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"}).datepicker('setDate', 1);
       }
       else
       {
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
       }

    });

    // hàm xử lý filter
// $scope.language = {!! $language !!};
// language = JSON.parse(language);
$(document).ready(function(){
  $("#mySearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#shipment-package-items tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection
