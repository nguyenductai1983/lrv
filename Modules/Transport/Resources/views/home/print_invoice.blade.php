@extends('layouts.admin.print2021')
@section('header')
<style>
   @page {
    size: A5 landscape;
    margin: 10px 0 0 0;
}
.page {
    width: 205mm;
    height: 140mm;
}
}
</style>
@endsection
@section('body')
  @foreach ($orders as $order)
    <div id="print">
      <center>
        <div class="page">
          <div class="subpage">
            <table class="footer" border="1px" cellspacing="0px" cellpadding="2px" width="100%">
              <tbody>
                <tr>
                  <td colspan="2" width="50%">
                    Thành phố/ Tỉnh nơi đến: <br>
                    {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }}

                  </td>
                  <td colspan="2" width="50%">
                    Mã phiếu #: {{ $order->code }}<br>
                    NGƯỜI GỬI: {{ $order->sender_full_name }}
                  </td>

                </tr>
                <tr>
                  <td width="40%">
                    NGƯỜI NHẬN:
                  </td>
                  <td colspan="3">
                    {{ $order->receive_full_name }}
                  </td>
                </tr>
                <tr>
                  <td>
                    ĐỊA CHỈ:
                  </td>
                  <td colspan="3">
                    {{ $order->receiver_address }} {{ $order->receiver_address_2 }} ,
                    @if (!empty($order->receiver_ward->name))
                      {{ $order->receiver_ward->name }} @endif
                    , {{ $order->receiver_city->name }},
                    {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }}
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    ĐIỆN THOẠI:
                  </td>
                  <td colspan="3">
                    {{ $order->receiver_phone }}
                    @if (!empty($order->receiver_cellphone))
                      , {{ $order->receiver_cellphone }}
                    @endif
                  </td>
                </tr>
                <tr style="vertical-align: top;">
                  <td width="25%" align="center">
                    <span>Nội dung hàng hóa</span><br>
                    <span>QUÀ TẶNG</span>
                  </td>
                  <td colspan="2" style="border-right: 0">
                    <svg id="barcode{{ $order->code }}"></svg>
                  </td>
                  <td style="border-left: 0 ; text-align: right">
                    <div style="font-size:68px;font-weight:bold">
                      @if ($order->danger_type > 2)
                        <i class="fa fa-tint"></i> <i class="fa fa-battery-empty"></i>
                      @elseif ($order->danger_type>1)
                        <i class="fa fa-tint"></i>
                      @elseif ($order->danger_type>0)
                        <i class="fa fa-battery-empty"></i>
                      @endif
                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <div style="font-size:9pt;">
                      <b>Lưu ý:</b> Người nhận có thể từ chối hoặc yêu cầu cùng kiểm hàng nếu phát
                      hiện kiện hàng không còn nguyên đai, nguyên kiện. Công ty sẽ không chịu
                      trách nhiệm nội dung hàng bên trong kiện nếu đã có chữ ký nhận hàng.
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
        <span id="ctl01_lbthongbao">
          <script language="javascript">
            window.print();

          </script>
        </span>
      </center>
    </div>
    <span id="lbthongbao"></span>
    <script>
      JsBarcode("#barcode{{ $order->code }}", "{{ $order->code }}", {
        format: "CODE128",
        displayValue: false,
        fontSize: 24,
        height: 60,
        width: 2
      });

    </script>
  @endforeach
@endsection
