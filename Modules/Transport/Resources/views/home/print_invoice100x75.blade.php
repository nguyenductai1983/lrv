@extends('layouts.admin.print2021')
@section('header')
  <style>
    /* kich thuoc trang giay */
    @page {
      size: 120mm 90mm;
      margin-top: 1mm;
    }

    /* nội dung tren web va noi dung in*/
    .page {
      width: 118mm;
      height: 88mm;
    }

  </style>
@endsection
@section('body')
  @foreach ($orders as $order)
    <div id="print">
      <center>
        <div class="page">
          <div class="subpage">
            <table border="1px" cellspacing="0px" width="100%" style="font-size:10pt; font-weight: bold;">
              <tbody>
                <tr>
                  <td width="30%">Mã phiếu #:</td>
                  <td>{{ $order->code }}</td>
                </tr>
                <tr>
                  <td>NGƯỜI GỬI:</td>
                  <td>{{ $order->sender_full_name }}</td>
                </tr>
                <tr>
                  <td>NGƯỜI NHẬN:</td>
                  <td>{{ $order->receive_full_name }}</td>
                </tr>
                <tr>
                  <td>ĐIỆN THOẠI:</td>
                  <td> {{ $order->receiver_phone }}
                    @if (!empty($order->receiver_cellphone))
                      , {{ $order->receiver_cellphone }}
                    @endif
                  </td>
                </tr>
                <tr>
                  <td> ĐỊA CHỈ:</td>
                  <td> {{ $order->receiver_address }} {{ $order->receiver_address_2 }} ,
                    @if (!empty($order->receiver_ward->name))
                      {{ $order->receiver_ward->name }} @endif
                    , {{ $order->receiver_city->name }},
                    {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }}
                  </td>
                </tr>
                <tr style="text-align: center;">
                  <td colspan="2">
                    <svg id="barcode{{ $order->code }}"></svg>
                  </td>

                </tr>
                <tr>
                  <td colspan="2">
                    <div style="font-size:6pt;">
                      <b>Lưu ý:</b> Người nhận có thể từ chối hoặc yêu cầu cùng kiểm hàng nếu phát
                      hiện kiện hàng không còn nguyên đai, nguyên kiện. Công ty sẽ không chịu
                      trách nhiệm nội dung hàng bên trong kiện nếu đã có chữ ký nhận hàng. Vui lòng liên hệ 1900-055-509 khi cần thêm thông tin.
                    </div>
                  </td>
                </tr>
                <tr style="text-align: right;">
                  <td colspan="2">
                    @php
                      $dangers = strrev(decbin($order->danger_type));
                    @endphp
                    @for ($i = strlen($dangers)-1; $i >= 0; $i--)
                      @if ($i === 2 && $dangers[$i])
                      <i class="fas fa-notes-medical" style="font-size:48px"></i>
                      @endif
                      @if ($i === 1 && $dangers[$i])
                        <i class="fa fa-tint" style="font-size:48px"></i>
                      @endif
                      @if ($i === 0 && $dangers[$i])
                        <i class="fa fa-battery-empty" style="font-size:48px"></i>
                      @endif
                    @endfor
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
        <span id="ctl01_lbthongbao">
          <script language="javascript">
            window.print();
          </script>
        </span>
      </center>
    </div>
    <span id="lbthongbao"></span>
    <script>
      JsBarcode("#barcode{{ $order->code }}", "{{ $order->code }}", {
        format: "CODE128",
        displayValue: false,
        fontSize: 12,
        height: 50,
        width: 2
      });
    </script>
  @endforeach
@endsection
