@extends('layouts.admin.print2021')
@section('header')
  <style>
    @page {
      size: A4;
      margin: 10px 0 0 0;
    }

    .page {
      width: 21cm;
      min-height: 29.7cm;
    }
    }

  </style>
@endsection
@section('body')
  @foreach ($orders as $order)
    <div id="print">
      <center>
        <div class="page">
          <div class="subpage">
            <table class="footer" border="1px" cellspacing="0px" cellpadding="2px" width="100%">
              <tbody>
                <tr>
                  <td colspan="2" width="50%">
                    Thành phố/ Tỉnh nơi đến: <br>
                    {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }}

                  </td>
                  <td colspan="2" width="50%">
                    Mã phiếu #: {{ $order->code }}<br>
                    NGƯỜI GỬI: {{ $order->sender_full_name }}
                  </td>

                </tr>
                <tr>
                  <td width="40%">
                    NGƯỜI NHẬN:
                  </td>
                  <td colspan="3">
                    {{ $order->receive_full_name }}
                  </td>
                </tr>
                <tr>
                  <td>
                    ĐỊA CHỈ:
                  </td>
                  <td colspan="3">
                    {{ $order->receiver_address }} {{ $order->receiver_address_2 }} ,
                    @if (!empty($order->receiver_ward->name))
                      {{ $order->receiver_ward->name }} @endif
                    , {{ $order->receiver_city->name }},
                    {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }}
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    ĐIỆN THOẠI:
                  </td>
                  <td colspan="3">
                    {{ $order->receiver_phone }}
                    @if (!empty($order->receiver_cellphone))
                      , {{ $order->receiver_cellphone }}
                    @endif
                  </td>
                </tr>
                <tr style="vertical-align: top;">
                  <td width="25%" align="center">
                    <span>Nội dung hàng hóa</span><br>
                    <span>QUÀ TẶNG</span>
                  </td>
                  <td colspan="2" style="border-right: 0">
                    <svg id="barcode{{ $order->code }}"></svg>
                  </td>
                  <td style="border-left: 0 ; text-align: right">
                    <div style="font-size:68px;font-weight:bold">
                        @php
                        $dangers = strrev(decbin($order->danger_type));
                      @endphp
                      @for ($i = strlen($dangers)-1; $i >= 0; $i--)
                        @if ($i === 2 && $dangers[$i])
                        <i class="fas fa-notes-medical" style="font-size:48px"></i>
                        @endif
                        @if ($i === 1 && $dangers[$i])
                          <i class="fa fa-tint" style="font-size:48px"></i>
                        @endif
                        @if ($i === 0 && $dangers[$i])
                          <i class="fa fa-battery-empty" style="font-size:48px"></i>
                        @endif
                      @endfor

                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <div style="font-size:9pt;">
                      <b>Lưu ý:</b> Người nhận có thể từ chối hoặc yêu cầu cùng kiểm hàng nếu phát
                      hiện kiện hàng không còn nguyên đai, nguyên kiện. Công ty sẽ không chịu
                      trách nhiệm nội dung hàng bên trong kiện nếu đã có chữ ký nhận hàng.
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <hr style="border-top: 1px dashed;" />
          <div class="subpage">
            <div class="head">
              <table>
                <tr>
                  <td class="left">
                    <img src="{{ asset('images/logoigreen.png') }}" style="width:180px"><br>
                    Toll free: 1 (888) 619-6869<br>
                  </td>
                  <td class="title">
                    Phiếu Gửi Hàng
                    <br />
                    Shipping Order Slip
                  </td>
                  <td class="right">
                    <span>Mã ĐH (Slip Order)#: <br> {{ $order->code }}</span><br>
                    <span>Ngày (Date): {{ $order->created_date }}</span>
                  </td>
                </tr>
              </table>
            </div>
            {{-- //test border="1px" cellspacing="0px" cellpadding="2px"
                        width="100%" style="margin-top:5px" --}}
            <table class="info" border="1px" cellspacing="0px" cellpadding="2px" width="100%" style="margin-top:5px">
              <tr>
                <td style="text-align:left">
                  <b>NGƯỜI GỬI:</b> {{ $order->sender_full_name }}
                  <br>
                  <b>Địa chỉ:</b> {{ $order->sender_address }},
                  {{ $order->sender_city->name }}
                  @if (!empty($order->sender_address_2))
                    <div>
                      <b>Địa chỉ 2:</b> {{ $order->sender_address_2 }}
                    </div>
                  @endif
                  <br>
                  <b>Tỉnh/ Thành:</b> {{ $order->sender_province->name }} - <b>Tel:</b>
                  {{ $order->sender_phone }}
                </td>
                <td style="width: 1%; background:white; border: 0">
                </td>
                <td style="text-align:left; vertical-align:right">

                  <b>NGƯỜI NHẬN:</b> {{ $order->receive_full_name }}
                  <br>
                  <b>Địa chỉ:</b> {{ $order->receiver_address }},
                  {{ $order->receiver_address_2 }}
                  @if (!empty($order->receiver_ward->name))
                    {{ $order->receiver_ward->name }} @endif
                  , {{ $order->receiver_city->name }}
                  <br>
                  <b>Tỉnh/ Thành:</b> {{ $order->receiver_province->name }} -
                  <b>Tel:</b> {{ $order->receiver_phone }}
                  @if (!empty($order->receiver_cellphone))
                    -{{ $order->receiver_cellphone }}
                  @endif
                </td>
              </tr>
            </table>
            {{-- //test --}}
            <table border="1px" cellspacing="0px" cellpadding="2px" width="100%" style="margin-top:5px">
              <tbody>
                <tr align="center" style="background:#F1F1F1">
                  <th>#</th>
                  <th>DESCRIPTION</th>
                  <th>QTY</th>
                  <th>Weight ({{ auth()->user()->agency->weight_unit->code }})</th>
                  <th>Declared values ({{ auth()->user()->agency->currency->code }})</th>
                  <th>Unit rate ({{ auth()->user()->agency->currency->code }})</th>
                  <th>Discount (% OFF) ({{ auth()->user()->agency->currency->code }})</th>
                  <th>Amount ({{ auth()->user()->agency->currency->code }})</th>
                  <th>Sur- charge Tax ({{ auth()->user()->agency->currency->code }})</th>
                  <th>Insu- rance ({{ auth()->user()->agency->currency->code }})</th>
                  <th>NOTE</th>
                </tr>
                @php
                  $no = 1;
                  $soluong = 0;
                @endphp
                @foreach ($order->order_items as $order_item)
                  <tr align="center">
                    <td>{{ $no }}</td>
                    <td>{{ $order_item->name }}</td>
                    <td>{{ number_format($order_item->quantity, 0) }}</td>
                    <td>{{ number_format($order_item->sub_total_weight, 2) }}</td>
                    <td>{{ number_format($order_item->sub_total_declare_price, 2) }}</td>
                    <td>{{ number_format($order_item->unit_goods_fee, 2) }}</td>
                    <td>{{ number_format($order_item->sub_total_discount, 2) }}</td>
                    <td>{{ number_format($order_item->sub_total_goods, 2) }}</td>
                    <td>{{ number_format($order_item->sub_total_surcharge_fee, 2) }}</td>
                    <td>{{ number_format($order_item->sub_total_insurrance_fee, 2) }}</td>
                    <td>{{ $order_item->user_note }}</td>
                  </tr>
                  @php
                    $no++;
                    $soluong += $order_item->quantity;
                  @endphp
                @endforeach
                @php
                  $min_fee = $order->total_goods_fee;
                  $total_fee = $order->total_goods_fee + $order->total_shipping_fee + $order->total_surcharge_fee + $order->total_shipping_fee + $order->total_insurrance_fee - $order->total_discount;
                @endphp
                {{-- total_goods_fee là tổng tiền hàng
                                total_shipping_fee tổng phí vận chuyển
                                total_final chi phí phải thanh toán --}}
                @if ($order->total_shipping_fee > 0)
                  <tr align="center" style="height:25px">
                    <td colspan="2">Ship Fee / phí vận chuyển</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ number_format($order->total_shipping_fee, 2) }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                @endif
                @if ($total_fee < $order->total_final)
                  @php
                    $min_fee = $order->total_final;
                  @endphp
                  <tr align="center" style="height:25px">
                    <td colspan="2">Min Fee / phí tối thiều</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ number_format($min_fee, 2) }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                @else
                  @php
                    $min_fee = $min_fee + $order->total_shipping_fee;
                  @endphp

                @endif
                <tr align="center">
                  <td colspan="2"><b>TOTAL/ TỔNG </b></td>
                  <td colspan="1"> {{ number_format($soluong, 2) }} </td>
                  <td colspan="1">{{ number_format($order->total_weight, 2) }}</td>
                  <td>
                    {{ number_format($order->total_declare_price, 2) }}
                  </td>
                  <td>
                  </td>
                  <td colspan="1"> {{ number_format($order->total_discount, 2) }}</td>
                  <td colspan="1" class="total">
                    {{ number_format($min_fee, 2) }}
                  </td>
                  <td colspan="1" class="total">
                    {{ number_format($order->total_surcharge_fee, 2) }}</td>
                  <td colspan="1" class="total">
                    {{ number_format($order->total_insurrance_fee, 2) }}</td>
                  <td colspan="1" class="total">
                  </td>
                </tr>
              </tbody>
            </table>
            <span class="total_center">
              - Tổng trọng lượng (Total weight): {{ number_format($order->total_weight, 2) }}
            </span>
            <span class="total_center" style=" border-left: 2px solid black;">
              - Tổng tiền (Total Amount): {{ number_format($order->total_final, 2) }}
            </span>
            <table border="1px" cellspacing="0px" cellpadding="2px" width="100%">
              <tbody>
                <tr>
                  <td style="width:85%;" class="label_center">
                    KHÔNG CÓ PIN: <input type="checkbox" class="stick"> KHÔNG CÓ NƯỚC HOA: <input type="checkbox"
                      class="stick">
                    KHÔNG CÓ HÀNG CHÁY NỔ: <input type="checkbox" class="stick">
                  </td>
                  <td rowspan="2"
                    style="height:105px;background:#F1F1F1;font-size:10pt;font-family:calibri;font-weight:bold;">
                    <table style="height: 100%">
                      <tr style="height: 30%;">
                        <td style="vertical-align: top;">
                          Tôi đã đọc, hiểu & đồng ý các ĐIỀU KIỆN GỬI HÀNG iGreenLink
                          <input type="checkbox" class="stick">
                        </td>
                      </tr>
                      <tr>
                        <td style="vertical-align: top;">
                          <hr style=" border-top: 3px solid black;" />
                          Chữ ký người gửi
                        </td>
                      </tr>
                      <tr>
                        <td style="vertical-align: bottom;">

                          <div style="font-size:10pt;">{{ $order->sender_full_name }}</div>
                        </td>
                      </tr>
                    </table>

                  </td>
                </tr>
                <tr>
                  <td>
                    <div style="text-align: center"><b> <u>ĐIỀU KIỆN GỬI HÀNG: </u></b></div>
                    <b>Tôi xác nhận thông tin hàng hóa nêu trên là đúng sự thật và hợp pháp, đồng thời
                      đồng ý các Điều kiện gửi hàng sau:</b> <br>
                    <b>KHÔNG NHẬN: hàng nguy hiểm, cháy nổ, vũ khí, chất gây nghiện</b> (ma túy,
                    heroin…), <b>thuốc tây</b>, hàng hóa <b>không bao bì, nhãn mác</b>
                    (cây cỏ khô, hàng tự chế biến) hoặc hàng hóa có bao bì không còn nguyên dấu niêm
                    phong của nhà sản xuất.<br>
                    <b>KIỂM HÀNG 100%: iGreen giữ quyền kiểm tra tất cả hàng hóa</b> kể cả hàng hóa có
                    bao bì đóng gói bên ngoài, và chịu trách nhiệm đảm
                    bảo không làm hư hỏng chất lượng hàng hóa bên trong. <br>
                    <b>ĐÓNG GÓI HÀNG: kiểm và đóng hàng tại nơi gửi hàng. Không dùng giấy bạc, giấy
                      than</b> hoặc các loại vật liệu mà máy soi không thấy
                    được hàng hóa bên trong.<br>
                    <b>TRỌNG LƯỢNG CÂN NẶNG & TRỌNG LƯỢNG THỂ TÍCH: iGreen sẽ tính theo giá trị nào lớn
                      hơn</b> (tham khảo Đại lý cách tính) <br>
                    <b>GIAO HÀNG: ước tính từ 7~10 ngày làm việc</b> tính từ ngày có chuyến bay hàng
                    tuần.<br>
                    <div style="text-align: right"><b> * Bản quyền thuộc về iGreenLink. Mọi sự in ấn,
                        sao chép đều là trái pháp luật.</b></div>
                  </td>

                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <span id="ctl01_lbthongbao">
          <script language="javascript">
            window.print();
          </script>
        </span>
      </center>
    </div>
    <span id="lbthongbao"></span>
    <script>
      JsBarcode("#barcode{{ $order->code }}", "{{ $order->code }}", {
        format: "CODE128",
        displayValue: false,
        fontSize: 24,
        height: 60,
        width: 2
      });
    </script>
  @endforeach
@endsection
