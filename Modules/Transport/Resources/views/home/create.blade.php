@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
  <div class="panel panel-default" ng-app="TransportApp" ng-controller="TransportCreateController" style="padding: 15px;">
    <div class="panel-heading">
      {{ __('label.create') }}
    </div>
    <div class="panel-body ng-cloak">
      <form id="form-create" name="createForm" ng-submit="createTransport()" novalidate>
        <div class="modal fade custom-width" id="modal-errors">
          <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-danger"><i class="fa fa-times-circle"></i>
                  {{ __('label.error') }}</h4>
              </div>
              <div class="modal-body text-danger">
                <div class="row" ng-if="errors.customer.length || errors.receiver.length">
                  <div class="col-sm-12">
                    <div ng-if="errors.customer.length">
                      <h4>{{ __('label.sender') }}</h4>
                      <ul>
                        <li ng-repeat="error in errors.customer">@{{ error }}</li>
                      </ul>
                    </div>
                    <div ng-if="errors.receiver.length">
                      <h4>{{ __('label.receiver') }}</h4>
                      <ul>
                        <li ng-repeat="error in errors.receiver">@{{ error }}</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div ng-if="errors.container.length">
                  <div class="row" ng-repeat="error in errors.container"
                    ng-if="error.others.length || error.products.length">
                    <div class="col-sm-12">
                      <h4>{{ __('label.container') }} @{{ $index + 1 }}</h4>
                      <ul ng-if="error.others.length">
                        <li ng-repeat="other in error.others">@{{ other }}</li>
                      </ul>
                      <ul ng-if="error.products.length">
                        <li ng-repeat="productErrors in error.products" ng-if="productErrors.length">
                          {{ __('label.product') }} @{{ $index + 1 }}
                          <ul>
                            <li ng-repeat="err in productErrors">@{{ err }}</li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div ng-if="errors.system.length">
                  <div class="row">
                    <div class="col-sm-12">
                      <h4>{{ __('express.system_error') }}</h4>
                      <ul>
                        <li ng-repeat="error in errors.system">@{{ error }}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">
                  {{ __('label.close') }}
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div>
              <ul>
                <li ng-repeat="error in errors.customer">@{{ error }}</li>
              </ul>
            </div>
            <div ng-if="customer.is_lock" class="alert alert-warning">
              <h4>{{ __('label.customer_lock') }}</h4>
            </div>
          </div>
        </div>
        <div class="panel panel-color panel-gray panel-border">
          <div class="panel-heading">
            <div style="margin: -12px 0;width: 80%;float: right;">
              <table class="table table-form">
                <tbody>
                  <tr>
                    <td>
                      <input type="text" class="form-control" ng-model="customerSearch.telephone"
                        ng-keypress="$event.keyCode == 13 && searchCustomers()"
                        placeholder="{{ __('customer.telephone') }}">
                    </td>
                    <td>
                      <input type="text" class="form-control" ng-model="customerSearch.code"
                        ng-keypress="$event.keyCode == 13 && searchCustomers()"
                        placeholder="{{ __('customer.code') }}">
                    </td>
                    <td>
                      <input type="text" class="form-control" ng-model="customerSearch.first_name"
                        ng-keypress="$event.keyCode == 13 && searchCustomers()"
                        placeholder="{{ __('customer.first_name') }}">
                    </td>
                    <td>
                      <input type="text" class="form-control" ng-model="customerSearch.middle_name"
                        ng-keypress="$event.keyCode == 13 && searchCustomers()"
                        placeholder="{{ __('customer.middle_name') }}">
                    </td>
                    <td>
                      <input type="text" class="form-control" ng-model="customerSearch.last_name"
                        ng-keypress="$event.keyCode == 13 && searchCustomers()"
                        placeholder="{{ __('customer.last_name') }}">
                    </td>
                    <td>
                      <button type="button" class="btn btn-white" ng-click="searchCustomers()"
                        ng-disabled="submittedSearchCustomers">
                        <i class="fa fa-search"></i> {{ __('label.find') }}
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="panel-body" style="margin: -10px -20px;">
            <table class="table table-form">
              <tbody>
                <tr>
                  <td width="50%" style="border-right: 1px solid #ccc;">
                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.sender') }} <strong
                        ng-bind="customer.code" class="ng-binding"></strong>
                      <button type="button" ng-click="EditCustomer();">
                        <i class="fa fa-edit text-success"></i>
                      </button>
                    </h3>
                    <h5 ng-show="!Updatecustomer">{{ __('label.customer_edit') }}</h5>

                    <div class="modal fade custom-width" id="modal-customers-search-result">
                      <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.search_result') }}</h4>
                          </div>
                          <div class="modal-body">
                            <div class="table-responsive">
                              <table class="table table-hover table-bordered">
                                <thead>
                                  <tr>
                                    <th>{{ __('customer.code') }}</th>
                                    <th>{{ __('customer.first_name') }}</th>
                                    <th>{{ __('customer.telephone') }}</th>
                                    <th>{{ __('customer.address_1') }}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr ng-repeat="customer in customersSearchResult" style="cursor: pointer;"
                                    ng-click="selectCustomer(customer)">
                                    <td ng-bind="customer.code"></td>
                                    <td ng-bind="customer.first_name +' '+customer.last_name">
                                    </td>
                                    <td ng-bind="customer.telephone"></td>
                                    <td ng-bind="customer.address_1"></td>
                                  </tr>
                                  <tr ng-if="submittedSearchCustomers">
                                    <td colspan="4">
                                      <i class="fa fa-refresh fa-spin"></i>
                                    </td>
                                  </tr>
                                  <tr ng-if="!submittedSearchCustomers && !customersSearchResult.length">
                                    <td colspan="4">
                                      {{ __('label.no_records') }}
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">
                              {{ __('label.close') }}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade custom-width" id="modal-address-search-result">
                      <div class="modal-dialog" style="width: 60%;">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ __('label.search_result') }}</h4>

                          </div>
                          <div class="modal-body">
                            <div class="table-responsive">
                              <table class="table table-hover table-bordered">
                                <thead>
                                  <tr>
                                    <th>{{ __('customer.code') }}</th>
                                    <th>{{ __('customer.first_name') }}</th>
                                    <th>{{ __('customer.telephone') }}</th>
                                    <th>{{ __('customer.address_1') }}</th>
                                    <th>{{ __('customer.address_2') }}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr ng-repeat="address in addressSearchResult" style="cursor: pointer;"
                                    ng-click="selectAddress(address)">
                                    <td ng-bind="address.postal_code"></td>
                                    <td ng-bind="address.email"></td>
                                    <td ng-bind="address.phone_number"></td>
                                    <td ng-bind="address.address1"></td>
                                    <td ng-bind="address.address2"></td>
                                  </tr>
                                  <tr ng-if="submittedSearchAddress">
                                    <td colspan="4">
                                      <i class="fa fa-refresh fa-spin"></i>
                                    </td>
                                  </tr>
                                  <tr ng-if="!submittedSearchAddress && !addressSearchResult.length">
                                    <td colspan="4">
                                      {{ __('label.no_records') }}
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">
                              {{ __('label.close') }}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <table class="table table-form">
                      <tbody>
                        <tr>
                          <td width="20%" class="col-label">
                            {{ __('customer.first_name') }}<i class="text-danger">*</i>
                          </td>
                          <td width="20%">
                            <input type="text" class="form-control" name="customer.first_name"
                              ng-model="customer.first_name" ng-disabled="Updatecustomer">
                          </td>
                          <td width="15%" class="col-label">
                            {{ __('customer.middle_name') }}
                          </td>
                          <td>
                            <input type="text" class="form-control" name="customer.middle_name"
                              ng-model="customer.middle_name" ng-disabled="Updatecustomer">
                          </td>
                          <td width="10%" class="col-label">
                            {{ __('customer.last_name') }}
                          </td>
                          <td>
                            <input type="text" class="form-control" name="customer.last_name"
                              ng-model="customer.last_name" ng-disabled="Updatecustomer">
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                          </td>
                          <td colspan="4">
                            <input type="text" class="form-control" name="customer.address_1"
                              ng-model="customer.address_1" ng-disabled="Updatecustomer">
                          </td>
                          <td class="col-label">
                            <button type="button" class="btn btn-info btn-xs"
                              ng-click="showAddress2(1)">{{ __('customer.address_2') }}</button>
                          </td>
                        </tr>
                        <tr ng-show="(customeraddress2) || (customer.address_2)">
                          <td class="col-label">
                            {{ __('customer.address_2') }}
                          </td>
                          <td colspan="5">
                            <input type="text" class="form-control" name="customer.address_2"
                              ng-model="customer.address_2" ng-disabled="Updatecustomer">
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('customer.email') }}
                          </td>
                          <td colspan="5">
                            <input type="text" class="form-control" name="customer.email" ng-model="customer.email"
                              ng-disabled="customemailedit">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table class="table table-form">
                      <tbody>
                        <tr>
                          <td width="20%" class="col-label">
                            {{ __('customer.country_id') }}<i class="text-danger">*</i>
                          </td>
                          <td width="30%">
                            <select name="customer.country_id" class="form-control" ng-model="customer.country_id"
                              ng-change="getProvincesCustomer()" ng-disabled="Updatecustomer">
                              <option value="">{{ __('label.select_country') }}</option>
                              <option ng-repeat="country in countries" ng-value="country.id">
                                @{{ country . code + '-' + country . name }}</option>
                            </select>
                          </td>
                          <td width="20%" class="col-label">
                            {{ __('customer.province_id') }}<i class="text-danger">*</i>
                          </td>
                          <td>
                            <select name="customer.province_id" class="form-control" ng-model="customer.province_id"
                              ng-change="getCitiesCustomer()" ng-disabled="Updatecustomer">
                              <option value="">{{ __('label.select_province') }}</option>
                              <option ng-repeat="province in provincesCustomer" ng-value="province.id">
                                @{{ province . name }}</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('customer.city_id') }}<i class="text-danger">*</i>
                          </td>
                          <td>
                            <select name="customer.city_id" class="form-control" ng-model="customer.city_id"
                              ng-change="getPostCodeCustomer()" ng-disabled="Updatecustomer">
                              <option value="">{{ __('label.select_city') }}</option>
                              <option ng-repeat="city in citiesCustomer" ng-value="city.id">
                                @{{ city . name }}</option>
                            </select>
                          </td>
                          <td class="col-label">{{ __('customer.postal_code') }}<i class="text-danger">*</i></td>
                          <td>
                            <input type="text" class="form-control" name="customer.postal_code"
                              ng-model="customer.postal_code"
                              ng-blur="customer.postal_code = customer.postal_code.split(' ').join('')"
                              ng-disabled="Updatecustomer">
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('customer.telephone') }}<i class="text-danger">*</i>
                          </td>
                          <td>
                            <input type="text" class="form-control" name="customer.telephone"
                              ng-model="customer.telephone" ng-disabled="Updatecustomer">
                          </td>
                          <td class="col-label">{{ __('customer.cellphone') }}</td>
                          <td>
                            <input type="text" class="form-control" name="customer.cellphone"
                              ng-model="customer.cellphone" ng-disabled="Updatecustomer">
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                          </td>
                          <td>
                            <label style="position: relative;margin-top: 5px;">
                              <input type="checkbox" ng-model="isCrrCheckbox"
                                style="position: absolute;left: -15px;top: -1px;">
                              CRR
                            </label>
                          </td>
                          <td colspan="2" class="col-label" style="padding-bottom: 10px;">
                            <button type="button" class="btn btn-info btn-xs"
                              ng-click="showMoreInfoCustomer = !showMoreInfoCustomer">
                              <i class="fa fa-plus"></i> {{ __('label.more_info') }}
                            </button>
                            <button type="button" ng-click="EditCustomer();"> <i
                                class="fa fa-edit text-success"></i></button>
                          </td>
                        </tr>
                        <tr ng-show="showMoreInfoCustomer">
                          <td class="col-label">
                            {{ __('customer.id_card') }}

                          </td>
                          <td>
                            <input type="text" class="form-control" name="customer.id_card" ng-model="customer.id_card"
                              ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                          </td>
                          <td class="col-label">
                            {{ __('customer.card_expire') }}
                          </td>
                          <td>
                            <div class="input-group date-picker" data-change-year="true" data-change-month="true"
                              data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                              <input type="text" class="form-control" name="customer.card_expire"
                                ng-model="customer.card_expire" ng-change="checkInfoCustomer()"
                                ng-disabled="Updatecustomer">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-white">
                                  <i class="fa fa-calendar"></i>
                                </button>
                              </div>
                            </div>
                          </td>
                        </tr>
                        {{-- them thong bao ngay het han id --}}
                        <tr ng-show="showMoreInfoCustomer">
                          <td colspan="2" ng-show="errors.customer.id_card_required">
                            <div class="text-danger text-right">
                              {{ __('customer.id_card_required') }}
                            </div>
                          </td>
                          <td colspan="2" ng-show="errors.customer.id_card_required || errors.customer.card_expire">
                            <div class="text-danger text-right">
                              {{ __('customer.card_is_expire') }}
                            </div>
                          </td>
                        </tr>
                        {{-- them thong bao ngay het han id --}}
                        <tr ng-show="showMoreInfoCustomer">
                          <td class="col-label">
                            {{ __('customer.birthday') }}
                          </td>
                          <td>
                            <div class="input-group date-picker" data-change-year="true" data-change-month="true"
                              data-year-range="{{ date('Y') - 100 }}:{{ date('Y') }}">
                              <input type="text" class="form-control" name="customer.birthday"
                                ng-model="customer.birthday" ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-white">
                                  <i class="fa fa-calendar"></i>
                                </button>
                              </div>
                            </div>
                          </td>
                          <td class="col-label">
                            {{ __('customer.career') }}
                          </td>
                          <td>
                            <input type="text" class="form-control" name="customer.career" ng-model="customer.career"
                              ng-change="checkInfoCustomer()" ng-disabled="Updatecustomer">
                          </td>
                        </tr>
                        {{-- them thong bao ngay sinh và nghể nghiệp --}}
                        <tr ng-show="showMoreInfoCustomer">
                          <td colspan="2" ng-show="errors.customer.birthday">
                            <div class="text-danger text-right">
                              {{ __('customer.birthday') }}
                            </div>
                          </td>
                          <td colspan="2" ng-show="errors.customer.career">
                            <div class="text-danger text-right">
                              {{ __('customer.career') }}
                            </div>
                          </td>
                        </tr>
                        {{-- them thong bao ngay sinh và nghể nghiệp --}}
                      </tbody>
                    </table>
                  </td>
                  <td>
                    <h3 style="margin-top: 0;margin-bottom: 15px;">{{ __('label.receiver') }} <strong
                        ng-bind="address.id" class="ng-binding"></strong>
                      <button type="button" ng-click="Editreceiver()">
                        <i class="fa fa-edit text-success"></i>
                      </button>
                    </h3>
                    <h5 ng-show="!Updatecustomer">{{ __('label.receiver_edit') }}</h5>
                    <div style="float: right;position: absolute;right: 17px;top: 63px;" ng-if="addressSearchResult">
                      <select class="form-control" ng-model="selectedId" ng-change="pickAddress(selectedId)">
                        <option value="" selected="selected">{{ __('label.select_address') }}
                        </option>
                        <option ng-if="addressSearchResult" ng-repeat="item in addressSearchResult" ng-value="item.id">
                          @{{ item . last_name }} @{{ item . middle_name }}
                          @{{ item . first_name }}</option>
                      </select>
                    </div>
                    <table class="table table-form">
                      <tbody>
                        <tr>
                          <td width="20%" class="col-label">
                            {{ __('receiver.first_name') }}<i class="text-danger">*</i>
                          </td>
                          <td width="20%">
                            <input type="text" class="form-control" name="receiver.first_name"
                              ng-model="address.first_name" ng-disabled="Updatereciver">
                          </td>
                          <td width="15%" class="col-label">
                            {{ __('receiver.middle_name') }}
                          </td>
                          <td>
                            <input type="text" class="form-control" name="receiver.middle_name"
                              ng-model="address.middle_name" ng-disabled="Updatereciver">
                          </td>
                          <td width="10%" class="col-label">
                            {{ __('receiver.last_name') }}
                          </td>
                          <td>
                            <input type="text" class="form-control" name="receiver.last_name" ng-model="address.last_name"
                              ng-disabled="Updatereciver">
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('customer.address_1') }}<i class="text-danger">*</i>
                          </td>
                          <td colspan="4">
                            <input type="text" class="form-control" name="receiver.address" ng-model="address.address_1"
                              ng-disabled="Updatereciver">
                          </td>
                          <td class="col-label">
                            <button type="button" class="btn btn-info btn-xs"
                              ng-click="showAddress2(2)">{{ __('customer.address_2') }}</button>
                          </td>
                        </tr>
                        <tr ng-show="(reciveraddress2) || (address.address_2)">
                          <td class="col-label">
                            {{ __('customer.address_2') }}
                          </td>
                          <td colspan="5">
                            <input type="text" class="form-control" name="receiver.address2" ng-model="address.address_2"
                              ng-disabled="Updatereciver">
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('customer.email') }}
                          </td>
                          <td colspan="5">
                            <input type="text" class="form-control" name="receiver.email" ng-model="receiver.email"
                              ng-disabled="Updatereciver">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table class="table table-form">
                      <tbody>
                        <tr>
                          <td width="20%" class="col-label">
                            {{ __('receiver.country_id') }}<i class="text-danger">*</i>
                          </td>
                          <td width="30%">
                            <select name="receiver.country_id" class="form-control" ng-model="address.country_id"
                              ng-change="getProvincesReceiver()" ng-disabled="Updatereciver">
                              <option value="">{{ __('label.select_address') }}</option>
                              <option ng-repeat="country in countries" ng-value="country.id">
                                @{{ country . code + '-' + country . name }}</option>
                            </select>
                          </td>
                          <td width="20%" class="col-label">
                            {{ __('receiver.province_id') }}<i class="text-danger">*</i>
                          </td>
                          <td>
                            <select name="receiver.province_id" class="form-control" ng-model="address.province_id"
                              ng-change="getCitiesReceiver()" ng-disabled="Updatereciver">
                              <option value="">{{ __('label.select_province') }}</option>
                              <option ng-repeat="province in provincesReceiver" ng-value="province.id">
                                @{{ province . name }}</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('receiver.city_id') }}<i class="text-danger">*</i>
                          </td>
                          <td>
                            <select name="receiver.city_id" class="form-control" ng-model="address.city_id"
                              ng-change="getWardsReceiver()" ng-disabled="Updatereciver">
                              <option value="">{{ __('label.select_city') }}</option>
                              <option ng-repeat="city in citiesReceiver" ng-value="city.id">
                                @{{ city . name }}</option>
                            </select>
                          </td>
                          {{-- ngày 22-04-2020 --}}
                          <td class="col-label" ng-hide="ward">
                            {{ __('receiver.ward_id') }}
                          </td>
                          <td ng-hide="ward">
                            <select name="receiver.ward_id" class="form-control" ng-model="address.ward_id"
                              ng-disabled="Updatereciver && address.ward_id">
                              <option value="">{{ __('label.select_ward') }}</option>
                              <option ng-repeat="ward in wardsReceiver" ng-value="ward.id">
                                @{{ ward . name }}</option>
                            </select>
                          </td>
                          {{-- het doan them --}}
                        </tr>
                        <tr>
                          <td class="col-label">
                            {{ __('receiver.cellphone') }} <span ng-show="!ward"> +84</span><i
                              class="text-danger">*</i>
                          </td>
                          <td>
                            <input type="text" class="form-control" name="receivertelephone" ng-model="address.telephone"
                              ng-disabled="Updatereciver" required ng-maxlength="10" ng-minlength="10"
                              ng-blur="checkreceivercellphone()">
                          </td>

                          <td class="col-label">{{ __('receiver.telephone') }} <span ng-show="!ward"> +84</span></td>
                          <td>
                            <input type="text" class="form-control" name="receiver.cellphone" ng-model="address.cellphone"
                              ng-disabled="Updatereciver">
                          </td>
                        </tr>
                        <tr>
                          <td colspan="4">
                            {{-- doan code kiem tra so dien thoai --}}
                            <div ng-show="!ward" style="color: red">
                              <span
                                ng-show="createForm.receivertelephone.$error.maxlength">{{ __('receiver.cellphone_max') }}
                                <a target="_blank" style="color: red"
                                  href="/helps/change-11-digit-mobile-number-to-10-digit-number-76">{{ __('label.more_info') }}</a></span>
                              <span
                                ng-show="createForm.receivertelephone.$error.minlength">{{ __('receiver.cellphone_min') }}</span>
                            </div>
                            {{-- het doan kiem tra so dt --}}
                          </td>
                        </tr>

                        <tr>
                          <td class="col-label">{{ __('receiver.postal_code') }}</td>
                          <td>
                            <input type="text" class="form-control" name="receiver.post_code"
                              ng-model="address.postal_code"
                              ng-blur="address.postal_code = address.postal_code.split(' ').join('')"
                              ng-disabled="Updatereciver">
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <button type="button" class="btn-sm btn-success" ng-click="createContact()"
                              ng-if="SaveContact">{{ __('label.save_contact') }} <i class="fa fa-save"></i></button>
                            <button type="button" class="btn btn-primary btn-sm" ng-if="customer.id"
                              ng-click="removeCustomer()">
                              <i class="fas fa-trash"></i> {{ __('label.clear') }}
                            </button>
                            <div ng-if="contact_save" class='alert alert-success' role="alert">
                              {{ __('label.save_contact_success') }}
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!--
        -->
        <div class="container-fluid">
          <div ng-show="showMoreInfoCustomer" class="row">
            <div class="col-sm-3 col-xs-4">
              <div class="col-label">
                {{ __('customer.image_1_file_id') }}
              </div>
              @include('partials.form-controls.image', ['field' => 'customer.image_1_file_id', 'file' => null,
              'Update' => 'Updatecustomer'])
            </div>
            <div class="col-sm-3 col-xs-4">
              <div class="col-label">
                {{ __('customer.image_2_file_id') }}
              </div>
              @include('partials.form-controls.image', ['field' => 'customer.image_2_file_id', 'file' => null,
              'Update' => 'Updatecustomer'])
            </div>
            <div class="col-sm-3 col-xs-4">
              <div class="col-label">
                {{ __('customer.image_3_file_id') }}
              </div>
              @include('partials.form-controls.image', ['field' => 'customer.image_3_file_id', 'file' => null,
              'Update' => 'Updatecustomer'])
            </div>
          </div>
        </div>
        <!--
        -->
        <div class="panel panel-color panel-gray panel-border" style="padding-bottom: 0;margin-bottom: -2px;">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cubes"></i> {{ __('label.goods') }}</h3>
          </div>
        </div>
        <div id="container" class="tabs-border">
          <ul class="nav nav-tabs tabs-border">
            <li ng-repeat="container in containers" class="tab" ng-class="{'active' : $first}">
              <a href="#container-@{{ $index + 1 }}" data-toggle="tab" target="_self">
                <span>{{ __('label.container') }} @{{ $index + 1 }}</span>
              </a>
            </li>
            <li>
              <a href="javascript://" ng-click="addContainer()">
                <span><i class="fa fa-plus"></i></span>
              </a>
            </li>
          </ul>
          <div class="tab-content" style="padding: 15px;">
            <div ng-repeat="container in containers" class="tab-pane" ng-class="{'active' : $first}"
              id="container-@{{ $index + 1 }}">
              <div class="form-group">
                <table class="table table-form table-bordered">
                  <tbody>
                    <tr>
                      <td width="250" class="text-right col-middle">
                        <strong> {{ __('label.Measure_container') }}
                        </strong>
                      </td>
                      <td width="70" class="text-right col-middle">{{ __('label.length') }} <i
                          class="text-danger">*</i>:</td>
                      <td width="80">
                        <input type="text" class="form-control text-center" ng-model="container.length"
                          ng-change="updateVolume(container)">
                      </td>
                      <td width="70" class="text-right col-middle">{{ __('label.width') }} <i
                          class="text-danger">*</i>:</td>
                      <td width="80">
                        <input type="text" class="form-control text-center" ng-model="container.width"
                          ng-change="updateVolume(container)">
                      </td>
                      <td width="70" class="text-right col-middle">{{ __('label.height') }} <i
                          class="text-danger">*</i>:</td>
                      <td width="80">
                        <input type="text" class="form-control text-center" ng-model="container.height"
                          ng-change="updateVolume(container)">
                      </td>
                      <td width="100" class="text-right col-middle">{{ __('label.volume') }}:</td>
                      <td width="70" class="col-middle">
                        <strong ng-bind="container.volume"></strong>
                      </td>
                      <td class="col-middle">
                        <button type="button" class="btn btn-info btn-xs" ng-click="setVolume(container)">
                          <i class="fa fa-check"></i> {{ __('label.select') }}
                        </button>
                      </td>
                      <td ng-if="containers.length > 1" class="col-middle">
                        <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal"
                          data-target="#modal-delete-container-@{{ $index }}">
                          <i class="fas fa-trash"></i> {{ __('label.delete_container') }}
                          @{{ $index + 1 }}
                        </button>
                        <div class="modal fade" id="modal-delete-container-@{{ $index }}">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                  aria-hidden="true">&times;</button>
                                <h4 class="modal-title">{{ __('label.confirm_delete') }}
                                </h4>
                              </div>
                              <div class="modal-body">
                                {!! __('label.confirm_delete_msg') !!}
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger disabled-submit"
                                  ng-click="deleteContainer($index)">
                                  <i class="fas fa-trash"></i> {{ __('label.delete') }}
                                </button>
                                <button type="button" class="btn btn-white"
                                  data-dismiss="modal">{{ __('label.cancel') }}</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="table-responsive">
                <table class="table table-hover table-bordered table-container" style="max-width: 120%;width: 120%;">
                  <thead>
                    <tr>
                      <th width="20" class="text-center">#</th>
                      <th width="130">{{ __('product.code') }}</th>
                      <th width="150">{{ __('product.name') }}</th>
                      <th width="60" class="text-center">{{ __('label.quantity_short') }}</th>
                      <th width="60" class="text-center">{{ __('label.physical_weight') }}</th>
                      <th width="60" class="text-right">{{ __('label.price') }}</th>
                      <th width="60" class="text-right">{{ __('label.discount_short') }} (%)</th>
                      <th width="60" class="text-center">{{ __('label.unit_short') }}</th>
                      <th width="80" class="text-right">{{ __('label.amount') }}</th>
                      <th width="70" class="text-right">{{ __('label.declared_value_short') }}</th>
                      <th width="70" class="text-right">{{ __('label.surcharge') }}</th>
                      <th width="80" class="text-center">{{ __('label.insurance_short') }}</th>
                      <th width="81" class="text-right">{{ __('label.total') }}</th>
                      <th width="200">{{ __('label.note') }}</th>
                      <th width="70">{{ __('label.action') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="product in container.products">
                      <td class="col-middle text-center" ng-bind="$index + 1"></td>
                      <td class="col-middle">
                        <input type="text" class="form-control" ng-model="product.code"
                          ng-change="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown',$parent.$index)"
                          ng-focus="searchProducts(product, 'products-' + $parent.$index + '-' + $index + '-dropdown',$parent.$index)">
                        {{-- 02-07-2021 thêm tín hie65ui vị trí container --}}
                        <div id="products-@{{ $parent . $index + '-' + $index }}-dropdown"
                          class="dropdown products-dropdown">
                          <button class="btn btn-primary dropdown-toggle hidden" type="button"
                            data-toggle="dropdown"></button>
                          <ul class="dropdown-menu">
                            <li>
                              <table class="table table-bordered table-hover">
                                <tbody>
                                  <tr ng-repeat="item in product.product_list" ng-style="!item.show && {display: 'none'}"
                                    ng-click="selectProduct(product, item)">
                                    <td ng-if="item.separate" ng-bind="item.name"
                                      style="height: 60px; border-bottom: 2px dotted green;"></td>
                                    <td ng-if="!item.separate" ng-bind="item.name"></td>
                                    {{-- <td ng-bind="item.code"></td>
                                                                        <td class="text-right" ng-bind="item.sale_price">
                                                                        </td>
                                                                        <td class="text-right" ng-bind="item.pickup_fee">
                                                                        </td> --}}
                                  </tr>
                                </tbody>
                              </table>
                            </li>
                          </ul>
                        </div>
                      </td>
                      <td class="col-middle">
                        <input type="text" class="form-control input-readonly" ng-model="product.name"
                          readonly="readonly">
                      </td>
                      <td class="col-middle">
                        <input type="text" class="form-control text-center" ng-model="product.quantity"
                          ng-change="updateContainers()">
                      </td>
                      <td class="col-middle">
                        <input type="text" class="form-control text-center" ng-model="product.weight"
                          ng-change="updateContainers()">
                          <span ng-show="product.check_weight" class="text-warning">{{ __('label.limit_weight') }} <strong ng-bind="product.weight"></strong></span>
                      </td>
                      <td class="text-right col-middle" ng-bind="product.price.toFixed(2)"></td>
                      <td class="col-middle">
                        <input type="text" class="form-control text-right" ng-model="product.per_discount"
                          ng-if="isEditDiscount" ng-change="updateContainers()">
                        <input type="text" class="form-control text-right" ng-model="product.per_discount"
                          ng-if="!isEditDiscount" ng-click="showPopupVip()" readonly>
                      </td>
                      <td class="text-center col-middle" ng-bind="product.unit"></td>
                      <td class="text-right col-middle" ng-bind="product.amount.toFixed(2)"></td>
                      <td class="col-middle">
                        <input type="text" class="form-control text-right" ng-model="product.declared_value"
                          ng-change="updateContainers()">
                      </td>
                      <td class="text-right col-middle" ng-bind="product.surcharge.toFixed(2)"></td>
                      <td class="text-center col-middle">
                        <table width="100%">
                          <tr>
                            <td>
                              <input type="checkbox" class="iswitch iswitch-info" style="margin: 0;"
                                ng-model="product.is_insurance" ng-change="updateContainers()">
                            </td>
                            <td ng-bind="product.insurance.toFixed(2)"></td>
                          </tr>
                        </table>
                      </td>
                      <td class="text-right col-middle" ng-bind="product.total.toFixed(2)"></td>
                      <td class="col-middle">
                        <input type="text" class="form-control" ng-model="product.note">
                      </td>
                      <td class="col-middle">
                        <button type="button" class="btn btn-xs btn-danger" ng-click="remove($parent.$index, $index)">
                          <i class="fas fa-window-close"></i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <button type="button" class="btn btn-info" ng-click="addproduct($index)">
                          <i class="fa fa-plus-square"></i> {{ __('label.add_row') }}
                        </button>
                      </td>
                      <td colspan="10">
                        <h4>
                          <label class="text-info" ng-if="container.danger===1">{{ __('label.box_has_battery') }}
                            {{ __('label.battery_perfume') }}
                            <a class='text-success' href="javascript://" ng-click="addContainer()">
                              {{ __('label.click_here') }}</a> {{ __('label.perfume_2') }}</label>
                          <label class="text-info"
                            ng-if="container.danger===2">{{ __('label.box_has_perfume') }}{{ __('label.battery_perfume') }}
                            <a class='text-success' href="javascript://" ng-click="addContainer()">
                              {{ __('label.click_here') }}</a> {{ __('label.battery_2') }}</label>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>

              <table class="table table-bordered">
                <tbody>
                  {{-- <tr>
                                        <td class="text-right" width="15%"></td>
                                        <td class="text-left" width="19%"></td>
                                        <td class="col-middle text-right" width="15%">{{ __('label.coupon_code') }}:</td>
                                        <td class="text-left" width="18%">
                                            <div class="input-group">
                                                <input type="text" class="form-control" ng-model="container.coupon_code"
                                                    ng-disabled="isUseCoupon">
                                                <div class="input-group-btn" ng-show="!isUseCoupon">
                                                    <button type="button" class="btn btn-info"
                                                        ng-click="applyCoupon(container)">
                                                        <i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                                <div class="input-group-btn" ng-show="isUseCoupon">
                                                    <button type="button" class="btn btn-danger"
                                                        ng-click="removeCoupon(container)">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right" width="15%">{{ __('label.coupon_amount') }}:</td>
                                        <td class="text-left">
                                            <strong ng-bind="container.coupon_amount.toFixed(2)"></strong>
                                        </td>
                                    </tr> --}}
                  <tr>
                    <td class="text-right" width="15%">{{ __('label.shipping_fee') }}:</td>
                    <td class="text-left" width="19%"><strong ng-bind="container.shipping_fee.toFixed(2)"></strong></td>
                    <td class="text-right" width="15%">{{ __('label.total_weight') }}:</td>
                    <td class="text-left" width="18%"><strong ng-bind="container.total_weight"></strong>
                    </td>
                    <td class="text-right" width="15%">{{ __('label.total_shipping_fee') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_shipping_fee.toFixed(2)"></strong></td>
                  </tr>
                  <tr>
                    <td class="text-right">{{ __('label.total_declared_value') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_declared_value.toFixed(2)"></strong></td>
                    <td class="text-right">{{ __('label.total_surcharge') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_surcharge.toFixed(2)"></strong></td>
                    <td class="text-right">{{ __('label.total_insurance') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_insurance.toFixed(2)"></strong></td>
                  </tr>
                  <tr>
                    <td class="text-right">{{ __('label.total_amount') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_amount.toFixed(2)"></strong>
                    </td>
                    <td class="text-right">{{ __('label.total_fee') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_fee.toFixed(2)"></strong>
                    </td>
                    <td class="text-right">{{ __('label.min_fee') }}:</td>
                    <td class="text-left"><strong ng-bind="container.min_fee.toFixed(2)"></strong></td>
                  </tr>
                  <tr>
                    <td class="text-right">{{ __('label.total_charge_fee') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_charge_fee.toFixed(2)"></strong></td>
                    <td class="text-right">{{ __('label.total_discount') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total_discount.toFixed(2)"></strong></td>
                    <td class="text-right">{{ __('label.total_pay') }}:</td>
                    <td class="text-left"><strong ng-bind="container.total.toFixed(2)"></strong></td>
                  </tr>
                  <tr>
                    {{-- <td class="col-middle text-right">{{ __('label.date_pay') }}:</td>
                                        <td class="col-middle text-left">
                                            <div class="input-group date-picker" data-change-year="true"
                                                data-change-month="true"
                                                data-year-range="{{ date('Y') - 10 }}:{{ date('Y') + 10 }}">
                                                <input type="text" class="form-control" ng-model="container.date_pay"
                                                    ng-init="container.date_pay = '<?php echo date(config('app.date_format')); ?>'">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-white">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </td> --}}
                    <td class="text-center">
                      {{ __('label.cod') }}
                    </td>
                    <td class="text-center">
                      <input type="checkbox" ng-model="container.cod_type" class="iswitch iswitch-info" name="cod_type">
                    </td>
                    <td ng-show="container.cod_type" class="col-middle text-right">{{ __('label.customer_payment') }}:</td>
                    <td ng-show="container.cod_type" class="col-middle text-left">
                      <input type="text" class="form-control text-right" ng-model="container.pay" ng-change="updatepay()">
                    </td>
                    <td ng-show="container.cod_type" class="col-middle text-right">{{ __('label.receiver_payment') }}:</td>
                    <td ng-show="container.cod_type" class="col-middle text-left"><strong
                        ng-bind="container.debt.toFixed(2)"></strong></td>
                  </tr>
                  <tr ng-show="container.cod_type">
                    <td class="col-middle text-right">{{ __('label.currency_pay') }}:</td>
                    <td class="text-left" style="padding-top:18px">
                      <strong class="ng-binding">{{ auth()->user()->agency->currency->code }}</strong>
                    </td>
                    <td class="col-middle text-right">{{ __('label.pay_method') }}:</td>
                    <td class="text-left" ng-if="methods.length > 0">
                      <select name="currency" id="payMethod" class="form-control" ng-model="container.pay_method">
                        <option ng-repeat="method in methods" ng-value="method.code">
                          @{{ method . name }}</option>
                      </select>
                    </td>
                    <td class="col-middle text-right">{{ __('label.status') }}:</td>
                    <td class="text-left">
                      <select name="currency" id="paystatus" class="form-control" ng-model="container.status">
                        <option ng-repeat="status in statues" ng-value="status.key">
                          @{{ status . name }}</option>
                      </select>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <br>
        {!! isset($termagree->translate->detail) ? $termagree->translate->detail : '' !!}
        <br>
        <h3><input type="checkbox" name="term_agree" ng-click="setagree(agree)" ng-model="agree">
          {{ __('label.ok_Terms_Conditions') }} </h3>
        <div class="text-right">
          <a href="{{ route('transport.index') }}" class="btn btn-white">
            <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
          </a>
          <button type="button" class="btn btn-info" ng-if="create && !customer.is_lock" ng-disabled="submitted"
            ng-click="scanCreateTransport()">
            <i class="fa fa-check"></i> {{ __('label.save') }} <i class="fa fa-refresh fa-spin" ng-if="submitted"></i>
          </button>
          <a href="@{{ new_order }}/print_invoice" target="_blank" class="btn btn-info" ng-if="complete">
            {{ __('title.print_label_receiver') }} <i class="fas fa-file-invoice"></i>
          </a>
          <a href="@{{ new_order }}/print_bills" target="_blank" class="btn btn-info" ng-if="complete">
            {{ __('label.print_bill') }} <i class="fa fa-print "></i>
          </a>
        </div>
        <div ng-if="complete" class='alert alert-success' role="alert">
          {{ __('label.complete') }}
        </div>
      </form>
    </div>
  </div>
@endsection

@section('footer')
  <script
    src="{{ asset('js/admin/app/transport-create.js?t=' . File::lastModified(public_path('js/admin/app/transport-create.js'))) }}">
  </script>
  <script>
    $(function() {
      var scope = angular.element('#form-create').scope();
      scope.agency = {!! auth()->user()->agency_id !!};
      scope.configSurcharge = {!! $configSurcharge->toJson() !!};
      scope.configInsurance = {!! $configInsurance->toJson() !!};
      scope.vnPriority = {!! json_encode($vnPriority) !!};
      scope.senderCountryId = {!! $senderCountryId !!};
      scope.receiverCountryId = {!! $receiverCountryId !!};
      scope.payMethod = '{!! $payMethod !!}';
      scope.discountLevel = {!! json_encode($discountLevel) !!};
      scope.vn_hcm_priority = {!! $vn_hcm_priority !!};
      scope.minFee = {!! $minFee !!};
      scope.init();
      scope.statues = [];

      <?php foreach (config('transport.statues') as $status): ?>
      scope.statues.push({
        key: parseInt({!! $status !!}),
        name: '{!! __('label.reciver_status_' . $status) !!}'
      });
      <?php endforeach; ?>

      if (!scope.$$phase) {
        scope.$apply();
      }

      $(document).on('hidden.bs.dropdown', '.dropdown', function() {
        $(this).find('.table tbody tr').removeClass('hover');
      });

      $(document).on('mouseenter', '.products-dropdown .table tbody tr', function() {
        $('.products-dropdown .table tbody tr').removeClass('hover');
        $(this).addClass('hover');
      });

      $(document).on('keydown', function(event) {
        if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
          var table = $(this).find('.products-dropdown .table'),
            tr = table.find('tbody tr.hover');
          if (tr.length === 0) {
            return true;
          }
          event.preventDefault();
          if (event.keyCode === 40) {
            tr = table.find('tbody tr.hover').next(':visible');
            if (tr.length === 0) {
              tr = table.find('tbody tr:visible').first();
            }
            tr.trigger('mouseenter');
          } else if (event.keyCode === 38) {
            tr = table.find('tbody tr.hover').prev(':visible');
            if (tr.length === 0) {
              tr = table.find('tbody tr:visible').last();
            }
            tr.trigger('mouseenter');
          } else if (event.keyCode === 13) {
            tr.trigger('click');
          }
        }
      });
    });
  </script>
@endsection
