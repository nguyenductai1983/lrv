<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role name</th>
            <th>Shipment name</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->user_id }}</td>
                <td>{{ $user->code }}</td>
                <td>
                    {{ isset($order->shipment->name) ? $order->shipment->name : '' }}
                </td>
                <td>{{ $user->shipping_status_name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
