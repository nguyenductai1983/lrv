@extends('layouts.admin.print')

@section('body')
<form name="form1" method="post" action="" id="form1">
    <div id="pnin">
        <center>
            <span id="ctl01_lbdata">
                <div class="page" style="height:297mm;width: 210mm;">
                    <div class="subpage" style="padding:15px">
                        <div>
                            <div class="head">
                                <div class="a" style="float:left;width:55%;text-align:left">
                                    <img src="{{ asset('images/logo.jpg') }}" style="width:120px" height="30px"><br>
                                    Toll free: 1 (888) 619-6869<br>
                                    Web: www.igreencorp.com
                                </div>
                                <div style="float:Right;width:44%;text-align:left;font-size:20pt;">
                                    PHIẾU GỬI HÀNG<br>
                                    <span style="font-size:15px">Mã phiếu #: {{ $order->code }}</span><br>
                                    <span style="font-size:15px">Ngày: {{ $order->created_date }}</span>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="float:left;border:1px solid black;width:49%;padding:6px;text-align:left;min-height:80px;background:#F1F1F1">
                                <div>
                                    <b>NGƯỜI GỬI:</b> {{ $order->sender_full_name }}
                                </div>
                                <div style="margin-top:5px">
                                    <b>Địa chỉ:</b> {{ $order->sender_address }}, {{ $order->sender_city->name }}
                                </div>
                                @php if(!empty($order->sender_address_2))  {   @endphp                             
                                <div style="margin-top:5px">
                                    <b>Địa chỉ 2:</b> {{ $order->sender_address_2 }}
                                </div>
                                 @php }  @endphp 
                                <div style="margin-top:5px">
                                    <b>Tỉnh/ Thành:</b> {{ $order->sender_province->name }}
                                    <b>Tel:</b> {{ $order->sender_phone }}
                                </div>
                            </div>
                            <div style="min-height:80px;float:right;border:1px solid black;width:49%;padding:6px;text-align:left;background:#F1F1F1">
                                <div>
                                    <b>NGƯỜI NHẬN:</b> {{ $order->receive_full_name }}
                                </div>
                                <div style="margin-top:5px">
                                    <b>Địa chỉ:</b> {{ $order->receiver_address }}, {{ $order->receiver_city->name }}
                                </div>
                                 @php if(!empty($order->receiver_address_2))  {   @endphp                             
                                <div style="margin-top:5px">
                                    <b>Địa chỉ 2:</b> {{ $order->receiver_address_2 }}
                                </div>
                                 @php }  @endphp 
                                <div style="margin-top:5px">
                                    <b>Tỉnh/ Thành:</b> {{ $order->receiver_province->name }}
                                    <b>Tel:</b> {{ $order->receiver_phone }} @php if(!empty($order->receiver_cellphone)){ @endphp - {{ $order->receiver_cellphone }} @php }@endphp
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <table border="1px" cellspacing="0px" cellpadding="5px" width="100%" style="margin-top:10px">
                                <tbody>
                                    <tr align="center" style="background:#F1F1F1">
                                        <th style="width:20px">STT</th>
                                        <th style="width:200px">MÔ TẢ HÀNG HÓA<br>(Description)</th>
                                        <th>SỐ LƯỢNG<br>(Quantity)</th>
                                        <th>TRỌNG LƯỢNG<br>(Weight)({{ auth()->user()->agency->weight_unit->code }})</th>
                                        <th>KHAI GIÁ<br>(D.V)({{ auth()->user()->agency->currency->code }})</th>
                                        <th>ĐƠN GIÁ<br>(U.P) ({{ auth()->user()->agency->currency->code }})</th>
                                        <th>THÀNH TIỀN<br>(Amount)({{ auth()->user()->agency->currency->code }})</th>
                                        <th>Phụ thu ({{ auth()->user()->agency->currency->code }})</th>
                                        <th>Bảo hiểm ({{ auth()->user()->agency->currency->code }})</th>
                                        <th>Chiết khấu ({{ auth()->user()->agency->currency->code }})</th>
                                        <th style="width: 145px;">Ghi chú</th>
                                    </tr>
                                    @php 
                                    $no = 1;
                                    $soluong = 0;
                                    @endphp
                                    @foreach($order->order_items as $order_item)
                                    <tr align="center">
                                        <td>{{ $no }}</td>
                                        <td>{{ $order_item->name}}</td>
                                        <td>{{ number_format($order_item->quantity, 0) }}</td>
                                        <td>{{ number_format($order_item->sub_total_weight, 2) }}</td>
                                        <td>{{ number_format($order_item->sub_total_declare_price, 2) }}</td>
                                        <td>{{ number_format($order_item->unit_goods_fee, 2) }}</td>
                                        <td>{{ number_format($order_item->sub_total_goods, 2) }}</td>
                                        <td>{{ number_format($order_item->sub_total_surcharge_fee, 2) }}</td>
                                        <td>{{ number_format($order_item->sub_total_insurrance_fee, 2) }}</td>
                                        <td>{{ number_format($order_item->sub_total_discount, 2) }}</td>
                                        <td>{{ $order_item->user_note}}</td>
                                    </tr>
                                    @php $no++;
                                    $soluong+=$order_item->quantity; 
                                    @endphp
                                    @endforeach
                                    @php 
                                    $min_fee=0;
                                    $tong_tam = $order->total_shipping_fee + $order->total_goods_fee + $order->total_insurrance_fee + $order->total_surcharge_fee - $order->total_discount;
                                    if ($tong_tam<$order->total_final)
                                    {
                                    $thanh_tien=($order->total_final + $order->total_discount)-($order->total_insurrance_fee + $order->total_surcharge_fee+$order->total_shipping_fee);
                                    $min_fee=$thanh_tien;
                                    }
                                    else
                                    {
                                    $thanh_tien=$order->total_goods_fee;
                                    }                                    
                                    @endphp
                                    <tr align="center" style="height:25px">
                                        <td colspan="2">Min Fee / phí tối thiều</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td> 
                                        <td>{{ number_format($min_fee, 2) }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="2"><b>TOTAL/ TỔNG </b></td>                                         
                                          <td colspan="1"> {{ number_format($soluong, 2) }} </td>
                                         <td colspan="1">{{ number_format($order->total_weight, 2) }} </td>                                         
                                        <td colspan="2"> {{ number_format($order->total_shipping_fee, 2) }} </td>
                                        <td colspan="1" style="background:#F1F1F1">{{ number_format($thanh_tien, 2) }}</td>
                                        <td colspan="1" style="background:#F1F1F1">{{ number_format($order->total_surcharge_fee, 2) }}</td>
                                        <td colspan="1" style="background:#F1F1F1">{{ number_format($order->total_insurrance_fee, 2) }}</td>
                                        <td colspan="1" style="background:#F1F1F1">{{ number_format($order->total_discount, 2) }}</td>
                                        <td colspan="1" style="background:#F1F1F1">{{ number_format($order->total_final, 2) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="1px" cellspacing="0px" cellpadding="5px" width="100%"> 
                                <tbody>
                                    <tr>
                                        <td style="width:78%;font-size:12pt;font-family:calibri;font-weight:bold;">
                                            <b>Tôi xác nhận các thông tin khai báo hàng hóa trên là hợp pháp, đúng sự thật, đồng thời hiểu rõ các <i>"Điều kiện Gửi hàng iGreenLink"</i>. Bằng việc ký tên (hoặc có dấu "X") vào ô bên cạnh, tôi sẽ chịu mọi trách nhiệm về nội dung thùng hàng gửi trước các cơ  quan chức năng nếu có bất cứ điều gì vi phạm pháp luật.</b>
                                        </td>
                                        <td style="height:105px;background:#F1F1F1;font-size:10pt;font-family:calibri;font-weight:bold;">
                                            Tôi đã đọc, hiểu, đồng ý <i>"Điều kiện Gửi hàng iGreenLink"</i>
                                            <div style="margin-top:15px;margin-right:20px;border:2px solid #000; width:20px; height:20px; float:right;text-align:center;font-weight:bold;font-size:10pt;">X</div><br><br><br>
                                            <span style="font-size:10pt;">{{ $order->sender_full_name }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="border:3px solid black;margin-top:15px;text-align:left;">
                                <table cellspacing="0px" cellpadding="5px" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="padding:5px; border-right:1px solid black;" width="50%">
                                                <span style="font-size:20px;font-weight:bold">NGƯỜI GỬI: <br>
                                                    <span style="text-align:center">{{ $order->sender_full_name }}</span>
                                                </span>
                                            </td>
                                            <td></td>
                                            <td style="padding:5px; vertical-align: top;">
                                                <span style="font-size:20px;font-weight:bold">THÀNH PHỐ: <br>
                                                    {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }} 
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="border-top:1px solid black" cellspacing="0px" cellpadding="5px" width="100%">
                                    <tbody>
                                        <tr style="border-top:1px solid black">
                                            <td width="25%" style="border-right:1px solid black">
                                                <span style="font-size:20px;font-weight:bold">NGƯỜI NHẬN: </span>
                                            </td>
                                            <td colspan="2" style="padding:5px;font-size:20px;font-weight:bold">
                                                {{ $order->receive_full_name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="border-right:1px solid black">
                                                <span style="font-size:20px;font-weight:bold">ĐỊA CHỈ: </span>
                                            </td>
                                            <td colspan="2" style="padding:5px;font-size:20px;font-weight:bold">
                                                {{ $order->receiver_address }} {{ $order->receiver_address_2 }} , {{ $order->receiver_city->name }}, {{ $order->receiver_province->name }}, {{ $order->receiver_country->name }} 
                                            </td> 
                                        </tr> 
                                        <tr>
                                            <td width="25%" style="border-right:1px solid black">
                                                <span style="font-size:20px;font-weight:bold">ĐIỆN THOẠI: </span>
                                            </td>
                                            <td colspan="2" style="padding:5px;font-size:20px;font-weight:bold">
                                                {{ $order->receiver_phone }} @php if(!empty($order->receiver_cellphone)){ @endphp - {{ $order->receiver_cellphone }} @php }@endphp
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" align="center" style="border-top:1px solid black;border-bottom:1px solid black;border-right:1px solid black">
                                                <span style="font-size:15px;font-weight:bold">Nội dung hàng hóa</span><br>
                                                <span style="font-size:18px;font-weight:bold">QUÀ TẶNG</span>
                                            </td>
                                            <td colspan="2" style="padding:5px;border-top:1px solid black;border-bottom:1px solid black;">
                                                <svg id="barcode"></svg>
                                            </td>
                                        </tr>
                                        <tr> 
                                            <td colspan="3">
                                                <span style="padding-top:15px;font-size:9pt;">
                                                    <b>Lưu ý:</b> Người nhận có thể từ chối  hoặc yêu cầu cùng kiểm hàng  nếu phát hiện kiện hàng không còn nguyên đai, nguyên kiện. Công ty sẽ không chịu trách nhiệm nội dung hàng bên trong kiện nếu đã có chữ ký nhận hàng.
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>                       
                    </div>
                </div>
            </span>
            <span id="ctl01_lbthongbao"><script language="javascript">window.print();</script></span>
        </center>
    </div>
    <span id="lbthongbao"></span>
</form>
<script>
    JsBarcode("#barcode", "{{ $order->code }}", {
        format:"CODE128",
        displayValue:true,
        fontSize:24,
        height:75,
        width:2
    });
</script>
@endsection

