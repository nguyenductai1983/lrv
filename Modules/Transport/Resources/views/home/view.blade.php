@extends('layouts.admin.index', ['menu' => $menu])

@section('body')

  <div class="panel panel-default" style="padding: 15px;">
    <div class="panel-heading">
      <h3 class="panel-title"> {{ __('label.view') }} {{ $order->code }}</h3>
    </div>
    <div class="panel-body">
      <div class="panel panel-color panel-gray panel-border">
        <div class="row">
          <div class="col-sm-6">
            <h3>{{ __('label.sender') }}</h3>
            <div class="row border-bottom">
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small>{{ __('customer.first_name') }}</small>
                  <h4>{{ $order->sender_first_name }}</h4>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small> {{ __('customer.middle_name') }} </small>
                  <h4>{{ $order->sender_middle_name }}</h4>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small> {{ __('customer.last_name') }} </small>
                  <h4>{{ $order->sender_last_name }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-12">
                <div class="d-inline-block">
                  <small> {{ __('customer.address_1') }} </small>
                  <h4>{{ $order->sender_address }}</h4>
                  <h4>{{ $order->sender_address_2 }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small>{{ __('customer.country_id') }}</small>
                  <h4>{{ $order->sender_country_name }}</h4>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small>{{ __('customer.province_id') }}</small>
                  <h4>{{ $order->sender_province_name }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small>{{ __('customer.city_id') }}</small>
                  <h4>{{ $order->sender_city_name }}</h4>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small> {{ __('customer.postal_code') }} </small>
                  <h4>{{ $order->sender_post_code }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-8">
                <div class="d-inline-block">
                  <small>{{ __('customer.telephone') }}</small>
                  <h4>
                    @if (isset($order->sender_phone)){{ $order->sender_phone }}
                    @endif
                    @if (!empty($order->sender_cellphone)) -
                      {{ $order->sender_cellphone }}
                    @endif

                  </h4>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <h4>CRR
                    <input type="checkbox" class="iswitch iswitch-info" readonly @if ($order->is_crr) checked="checked" @endif>
                  </h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-12">
                <div class="d-inline-block">
                  <small> {{ __('customer.email') }} </small>
                  <h4>{{ $order->sender_email }}</h4>
                </div>
              </div>
            </div>
            {{-- het khung nguoi gui --}}
          </div>

          {{-- thông tin nguoi nhận --}}

          <div class="col-sm-6 border-left">
            <h3>{{ __('label.receiver') }}
            </h3>
            <div class="row border-bottom">
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small>{{ __('receiver.first_name') }}</small>
                  <h4>{{ $order->receive_first_name }}</h4>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small> {{ __('receiver.middle_name') }}</small>
                  <h4>{{ $order->sender_middle_name }}</h4>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small>{{ __('receiver.last_name') }}</small>
                  <h4>{{ $order->sender_last_name }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-12">
                <div class="d-inline-block">
                  <small>{{ __('receiver.address') }}</small>
                  <h4>{{ $order->receiver_address }}
                    @if (isset($order->receiver_address_2)),
                      {{ $order->receiver_address_2 }}
                    @endif
                  </h4>
                </div>
              </div>
            </div>
            {{-- thong tin dia ly --}}
            <div class="row border-bottom">
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small>{{ __('receiver.country_id') }}</small>
                  <h4>{{ $order->receiver_country_name }}</h4>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small>{{ __('receiver.province_id') }}</small>
                  <h4>{{ $order->receiver_province_name }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small>{{ __('receiver.city_id') }}</small>
                  <h4>{{ $order->receiver_city_name }}</h4>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="d-inline-block">
                  <small> {{ __('receiver.ward_id') }} </small>
                  <h4>{{ $order->receiver_ward_name }}</h4>
                </div>
              </div>
            </div>
            <div class="row border-bottom">
              <div class="col-xs-8">
                <div class="d-inline-block">
                  <small>{{ __('receiver.telephone') }}</small>
                  <h4>
                    @if (isset($order->receiver_cellphone))
                      {{ $order->receiver_cellphone }}
                    @endif
                    @if (!empty($order->receiver_phone)) -
                      {{ $order->receiver_phone }}
                    @endif
                  </h4>
                </div>
              </div>
              <div class="col-xs-4">
                <div class="d-inline-block">
                  <small> {{ __('receiver.postal_code') }} </small>
                  <h4>{{ $order->receiver_post_code }}</h4>
                </div>
              </div>
            </div>
            {{-- het thong tin dia ly --}}
          </div>
        </div>
      </div>
      <div class="panel panel-color panel-gray panel-border" style="padding-bottom: 0;margin-bottom: -2px;">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-cubes"></i> {{ __('label.goods') }}</h3>
        </div>
      </div>
      <div class="panel panel-color panel-gray panel-border">
        <div class="row border-bottom">
          <div class="col-xs-3 offset-xs-4">
            <h5> box net weight: </h5>
          </div>
          <div class="col-xs-2">
            <div class="d-inline-block">
              <small>{{ __('label.length') }}</small>
              <h5> {{ $order->length }} </h5>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="d-inline-block">
              <small>{{ __('label.width') }}</small>
              <h5> {{ $order->width }} </h5>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="d-inline-block">
              <small>{{ __('label.height') }}</small>
              <h5> {{ $order->height }} </h5>
            </div>
          </div>
        </div>
        <div class="table-responsive" style="padding-bottom: 0;margin-bottom: 20px;">
          <table class="table table-hover table-bordered table-container" style="max-width: 140%;width: 120%;">
            <thead>
              <tr>
                <th class="text-center">#</th>
                <th width="130">{{ __('product.code') }}</th>
                <th width="153">{{ __('product.name') }}</th>
                <th width="60" class="text-center">{{ __('label.quantity_short') }}</th>
                <th width="60" class="text-center">{{ __('label.weight_short') }}</th>
                <th width="60" class="text-right">{{ __('label.price') }}</th>
                <th width="60" class="text-right">{{ __('label.discount_short') }}(%)</th>
                <th width="60" class="text-center">{{ __('label.unit_short') }}</th>
                <th width="80" class="text-right">{{ __('label.amount') }}</th>
                <th width="70" class="text-right">{{ __('label.declared_value_short') }}</th>
                <th width="70" class="text-right">{{ __('label.surcharge') }}</th>
                <th width="80" class="text-center">{{ __('label.insurance_short') }}</th>
                <th width="81" class="text-right">{{ __('label.total') }}</th>
                <th>{{ __('label.note') }}</th>
              </tr>
            </thead>
            <tbody>
              @php
                $no = 1;
                $total_quantity = 0;
              @endphp
              @foreach ($order_items as $order_item)
                <tr>
                  <td class="col-middle text-center">
                    {{ $no }}</td>
                  <td class="col-middle">
                    {{ $order_item->product->code }}
                  </td>
                  <td class="text-center col-middle">
                    {{ $order_item->name }}
                  </td>
                  <td class="text-center col-middle">
                    {{ $order_item->quantity }}
                  </td>
                  <td class="text-center col-middle">
                    {{ $order_item->sub_total_weight }}
                  </td>
                  <td class="text-center  col-middle">
                    {{ $order_item->unit_goods_fee }}
                  </td>
                  <td class="col-middle">
                    {{ $order_item->sub_total_discount }}
                  </td>
                  <td class="text-center col-middle">
                    {{ $order_item->messure->name }}
                  </td>
                  <td class="text-center  col-middle">
                    {{ $order_item->sub_total_goods }}
                  </td>
                  <td class="text-center col-middle">
                    {{ $order_item->sub_total_declare_price }}
                  </td>
                  <td class="text-center  col-middle">
                    {{ $order_item->sub_total_surcharge_fee }}
                  </td>
                  <td class="text-center col-middle">
                    {{ $order_item->sub_total_insurrance_fee }}
                  </td>

                  <td class="text-center  col-middle">
                    {{ $order_item->sub_total_final }}
                  </td>
                  <td class="text-left col-middle">
                    {{ $order_item->user_note }}
                  </td>
                  <?php
                  $no++;
                  $total_quantity += $order_item->quantity;
                  ?>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <td class="col-middle text-center" colspan="3">
                Total
              </td>
              <td>
                {{ $total_quantity }}
              </td>
              <td class="text-center col-middle padding-0">
                {{ $order->total_weight }}
              </td>
              <td class="text-center  col-middle">

              </td>
              <td class="text-center col-middle padding-0">
                {{ $order->total_discount }}
              </td>
              <td class="text-center col-middle">

              </td>
              <td class="text-center col-middle padding-0">
                {{ $order->total_goods_fee }}
              </td>
              <td class="text-center col-middle padding-0">
                {{ $order->total_declare_price }}
              </td>
              <td class="text-center  col-middle">
                {{ $order->total_surcharge_fee }}
              </td>
              <td class="text-center col-middle">
                {{ $order->total_insurrance_fee }}
              </td>
              <td class="text-center col-middle">
                {{ $order->total_final }}
              </td>
              <td class="col-middle">
              </td>
            </tfoot>
          </table>
          <hr>
        </div>
        {{-- bat dau tong hop --}}
        <h5>
            <div class="row border-secondary" style="padding-top: 10px;">
                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_weight') }} (1)
                    <h4>{{ $order->total_weight }}</h4>
                  </div>
                </div>
                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.shipping_fee') }} (2)
                    <h4>{{ $order->shipping_fee }}</h4>
                  </div>
                </div>
                <div class="col-xs-6 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_shipping_fee') }} (3)=(1)x(2)
                    <h4>{{ $order->total_shipping_fee }}</h4>
                  </div>
                </div>
              </div>
              <div class="row border-secondary">
                               <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_surcharge') }} (4)
                    <h4>{{ $order->total_surcharge }}</h4>
                  </div>
                </div>
                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_insurance') }}(5)
                    <h4>{{ $order->total_insurance }}</h4>
                  </div>
                </div>
                <div class="col-xs-6 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_surcharge') }} (6) = (4)+(5)
                    <h4>{{ $order->total_surcharge + $order->total_insurance }}</h4>
                  </div>
                </div>
              </div>
              <div class="row border-secondary">

                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_charge_fee') }} (7)
                    <h4>{{ $order->total_goods_fee }}</h4>
                  </div>
                </div>
                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_discount') }}(8)
                    <h4>{{ $order->total_discount }}</h4>
                  </div>
                </div>
                <div class="col-xs-6 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_amount') }} (9) = (7)-(8)
                    <h4>{{ $order->total_goods_fee - $order->total_discount }}</h4>
                  </div>
                </div>
              </div>
              <div class="row border-secondary">

                <div class="col-xs-3 border-left">

                </div>
                <div class="col-xs-3 border-left">

                </div>
                <div class="col-xs-6 border-left">
                  <div class="d-inline-block">
                    {{ __('label.total_pay') }}(10) = (3)+(6)+(9)
                    <h4>{{ $order->total_final }}</h4>
                  </div>
                </div>
              </div>
              <div class="row border-secondary">

                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.last_paid_time') }}
                    <h4>{{ $order->last_payment_at }}</h4>
                  </div>
                </div>
                <div class="col-xs-3 border-left">
                  <div class="d-inline-block">
                    {{ __('label.paid') }}:
                    <h4>{{ $order->total_paid_amount }}</h4>
                  </div>
                </div>
                <div class="col-xs-6 border-left">
                  <div class="d-inline-block">
                    {{ __('label.debt') }}
                    <h4>{{ $order->total_remain_amount }}</h4>
                  </div>
                </div>
              </div>
            </h5>
              {{-- het don tong hop --}}
        <div class="text-right" style="padding-top: 10px;">
          <a href="{{ route('transport.index') }}" class="btn btn-white">
            <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
          </a>
          <a href="print_invoice" target="_blank" class="btn btn-info">
            {{ __('title.print_label_receiver') }} <i class="fas fa-file-invoice"></i>
          </a>
          <a href="print_bills" target="_blank" class="btn btn-info">
            {{ __('label.print_bill') }} <i class="fa fa-print "></i>
          </a>
        </div>
      </div>
    </div>
  @endsection

  {{-- @section('footer')
  <script
    src="{{ asset('js/admin/app/transport-edit.js?t=' . File::lastModified(public_path('js/admin/app/transport-edit.js'))) }}">
  </script>
  <script>
    $(function() {
      var scope = angular.element('#form-edit').scope();
      scope.configSurcharge = {!! $configSurcharge->toJson() !!};
      scope.configInsurance = {!! $configInsurance->toJson() !!};
      scope.vnPriority = {!! json_encode($vnPriority) !!};
      scope.discountLevel = {!! json_encode($discountLevel) !!};
      scope.id = {!! request()->route('id') !!};
      scope.vn_hcm_priority = {!! $vn_hcm_priority !!};
      scope.getTransport();
      scope.statues = [];

      if (!scope.$$phase) {
        scope.$apply();
      }

      $(document).on('hidden.bs.dropdown', '.dropdown', function() {
        $(this).find('.table tbody tr').removeClass('hover');
      });

      $(document).on('mouseenter', '.products-dropdown .table tbody tr', function() {
        $('.products-dropdown .table tbody tr').removeClass('hover');
        $(this).addClass('hover');
      });

      $(document).on('keydown', function(event) {
        if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
          var table = $(this).find('.products-dropdown .table'),
            tr = table.find('tbody tr.hover');
          if (tr.length === 0) {
            return true;
          }
          event.preventDefault();
          if (event.keyCode === 40) {
            tr = table.find('tbody tr.hover').next(':visible');
            if (tr.length === 0) {
              tr = table.find('tbody tr:visible').first();
            }
            tr.trigger('mouseenter');
          } else if (event.keyCode === 38) {
            tr = table.find('tbody tr.hover').prev(':visible');
            if (tr.length === 0) {
              tr = table.find('tbody tr:visible').last();
            }
            tr.trigger('mouseenter');
          } else if (event.keyCode === 13) {
            tr.trigger('click');
          }
        }
      });
    });

  </script>
@endsection --}}
