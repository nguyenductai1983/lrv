@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.orders_title')}}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('transport.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
             <table class="table-hover">
                <thead>
                    <tr>
                      <th>Key</th>
                      <th>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($orders as $order => $order_value)
                    <tr>
                      <td><h4>{{ $order}}</h4></td>
                      <td><h4>{{ $order_value }}</h4></td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('transport.index') }}" class="btn btn-white">
                        <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    // hàm xử lý filter
    scope.language = {!! $language !!};
</script>
@endsection
