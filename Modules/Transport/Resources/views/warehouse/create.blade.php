@extends('layouts.admin.index', ['menu' => $menu])
{{-- @extends('layouts.admin.index', ['menu' => $menu, 'language' => $language]) --}}
@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{ __('transport.warehouse_create') }}
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-12">
          <form role="form" class="form-inline">
            <div class="form-group">
              <input name="stockin_at" id="stockin_at" value="{{ date('d/m/Y') }}" class="form-control" type="text"
                style="width: 190px;" placeholder="{{ __('label.date') }}">
            </div>
            <div class="form-group">
              <select name="from_warehouse_id" class="form-control">
                @if ($warehouses->count() > 0)
                  @foreach ($warehouses as $warehouse)
                    <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;"
                placeholder="{{ __('label.order_code') }}" autocomplete="off">
            </div>
            <button type="button" class="btn btn-white btn-single" id="bt_switch_barcode" onclick="switch_barcode()"
              id="toggle-on">
              <i class="fas fa-barcode" id="switch_barcode"></i>
            </button>
            <div class="form-group">
              <button type="button" class="btn btn-white btn-single" onclick="warehouse.addItem();">
                <i class="fa fa-search"></i> {{ __('label.find') }}
              </button>
            </div>
            <div>
              <p class="text-info">{{ __('label.barcode_switch') }}</p>
            </div>
          </form>
        </div>
      </div>
      <form id="warehouse-package-form">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th width="2%">
                  <input type="checkbox" id="select-all" checked="">
                </th>
                <th width="17%">{{ __('label.order_code') }}</th>
                <th width="18%">{{ __('label.goods_info') }}</th>
                <th width="40%">{{ __('label.note') }}</th>
                <th width="10%">{{ __('label.receive_status') }}</th>
                <th width="15%">{{ __('label.action') }}</th>
              </tr>
            </thead>
            <tbody id="warehouse-package-items">
              <tr id="empty-item">
                <td colspan="7">{{ __('label.no_records') }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </form>
      <input name="numItem" type="hidden" value="1">
      <div class="text-right">
        <a href="{{ route('transport.warehouse.index') }}" class="btn btn-white">
          <i class="fa fa-arrow-left"></i> {{ __('label.back') }}
        </a>
        <button type="button" class="btn btn-info"
          onclick="warehouse.addPackage('{{ route('transport.warehouse.index') }}');">
          <i class="fa fa-check"></i> {{ __('label.add') }}
        </button>
      </div>
    </div>
  </div>
@endsection

@section('footer')
  <script src="/js/transport/warehouse.js"></script>
  <script type="text/javascript">
    // xu ly mở tắt barcode
    let barcode = false;
    let count;
    let temp_str = '';

    function switch_barcode() {
      var btn_x = document.getElementById("bt_switch_barcode");
      if (barcode === false) {
        barcode = true;
        // x.style.display = "inline";
        btn_x.className = "btn btn-info btn-single";
        btn_x.style.fontSize = '18pt';
        count = setInterval(timer, 100)
      } else {
        barcode = false;
        // x.style.display = "none";
        btn_x.style.fontSize = '';
        btn_x.className = "btn btn-white btn-single";
        clearInterval(count)
      }
    }

    function timer() {
      var focusbox = document.getElementById("order_code");
      focusbox.focus();
    }
    $("#order_code").on('keypress', function(event) {
      if (event.which === 13) {
        var str_new = document.getElementById('order_code').value;
        str_new = str_new.replace(temp_str, '');
        document.getElementById('order_code').value = str_new;
        warehouse.addItem();
        event.preventDefault();
        temp_str = str_new;
      }
    });
    // xu ly mở tắt barcode
    $(function() {
      $("#stockin_at").datepicker({
        dateFormat: "{{ config('app.date_format_js') }}"
      });
    });
    $('#select-all').click(function(event) {
      if (this.checked) {
        $('#warehouse-package-items input[type=checkbox]').each(function() {
          this.checked = true;
        });
      } else {
        $('#warehouse-package-items input[type=checkbox]').each(function() {
          this.checked = false;
        });
      }
    });
  </script>
@endsection
