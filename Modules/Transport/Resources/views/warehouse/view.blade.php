@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.warehouse_view')}}: {{ $warehousePackage->code }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form id="warehouse-package-info-form" class="form-inline">
                    <input name="id" type="hidden" value="{{ $warehousePackage->id }}">
                    <div class="form-group">
                        <input name="stockin_at" id="stockin_at" value="{{ $warehousePackage->stockin_at}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.date')}}">
                    </div>
                    <div class="form-group">
                        <select name="from_warehouse_id" class="form-control">
                            @if($warehouses->count() > 0)
                                @foreach($warehouses as $warehouse)
                                    <option value="{{ $warehouse->id }}"
                                        {{ ($warehousePackage->from_warehouse_id == $warehouse->id)?
                                        'selected' :'' }}  >
                                        {{ $warehouse->name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code')}}">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-white btn-single" onclick="warehouse.addItemExist({{ $warehousePackage->id}});">
                            <i class="fa fa-search"></i> {{ __('label.find')}}
                        </button>
                        <a href="{{ route('transport.warehouse.exportExcelOrder', $warehousePackage->id) }}" class="btn btn-white btn-single">
                            {{ __('label.export_excel') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="warehouse-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="10%">{{ __('label.order_code')}}</th>
                            <th width="20%">{{ __('label.goods_info')}}</th>
                            <th width="35%">{{ __('label.note')}}</th>
                            <th width="10%">{{ __('label.receive_status')}}</th>
                            <th width="10%">{{ __('label.status')}}</th>
                            <th width="10%">{{ __('label.action')}}</th>
                        </tr>
                    </thead>
                    <tbody id="warehouse-package-items">
                        @if($orders->count() > 0)
                         <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr id="warehouse-package-item-{{ $order->id }}">
                            <td> {{ $no }}</td>
                            <td>{{ $order->code }}</td>
                            <td>{{ $order->products }}</td>
                            <td>{{ $order->user_note }}</td>
                            <td>
                                <span class="{{ $order->receive_status_label }}">{{ $order->receive_status_name }}</span>
                            </td>
                            <td>
                                <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                            <td>
                                @php if($order->shipping_status == $order->shipping_status_stock_in){ @endphp
                                <button type="button" onclick="warehouse.removeItemExist({{ $order->id }}, {{ $warehousePackage->id }})" class="btn btn-xs btn-danger">
                                    <i class="fas fa-trash"></i>
                                </button>
                                @php } @endphp
                            </td>
                        </tr>
                          <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </form>
        <div class="text-right">
            <a href="{{ route('transport.warehouse.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <button type="button" class="btn btn-info" onclick="warehouse.editPackage('{{ route('transport.warehouse.index') }}');">
                <i class="fa fa-check"></i> {{ __('label.update')}}
            </button>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/warehouse.js"></script>
<script>
    $(function () {
        $("#stockin_at").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    $("#order_code").on('keypress', function (event) {
        if (event.which === 13) {
            warehouse.addItemExist({{ $warehousePackage->id}});
        }
    });
</script>
@endsection
