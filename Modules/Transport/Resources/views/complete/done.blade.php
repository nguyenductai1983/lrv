@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.goods_title')}}
    </div>
    <div class="panel-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-pills">
                <li>
                    <a href="{{ route('transport.complete.approved') }}">{{ __('transport.ship_local_status_approve')}}</a>
                </li>
                <li>
                    <a href="{{ route('transport.complete.carrierpickup') }}">{{ __('transport.ship_local_status_carrier')}}</a>
                </li>
                <li class="active">
                    <a href="{{ route('transport.complete.done') }}">{{ __('transport.ship_local_status_done')}}</a>
                </li>
                <li>
                    <a href="{{ route('transport.complete.cancel') }}">{{ __('transport.ship_local_status_reject')}}</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="panel-group">
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <form action="{{ route('transport.complete.done') }}" method="get" role="form" class="form-inline">
                                    <div class="form-group">
                                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                                    </div>
                                    <div class="form-group">
                                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                                    </div>
                                    <div class="form-group">
                                        <input name="order_code" id="order_code" value="{{ request()->query('order_code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-white btn-single">
                                            <i class="fa fa-search"></i> {{ __('label.find') }}
                                        </button>
                                        <a href="{{ route('transport.complete.done') }}" class="btn btn-white btn-single">
                                            {{ __('label.clear') }}
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <form>
                            <div class="paginate-single">
                                {{ $shipping_package_locals->appends(request()->query())->links() }}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" style="max-width: 120%;width: 120%;">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="10%">{{ __('label.order_code') }}</th>
                                            <th width="15%">{{ __('label.receiver') }}</th>
                                            <th width="15%">{{ __('label.transporter') }}</th>
                                            <th width="15%">{{ __('label.customer_received_time') }}</th>
                                            <th width="10%">{{ __('label.partner') }}</th>
                                            <th width="10%">{{ __('label.note') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($shipping_package_locals->count() > 0)
                                        <?php $no = 1; ?>
                                        @foreach($shipping_package_locals as $shipping_package_local)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $shipping_package_local->order_code }}</td>
                                            <td>{{ $shipping_package_local->shipping_address }}</td>
                                            <td>{{ $shipping_package_local->user->full_name }}</td>
                                            <td>{{ $shipping_package_local->customer_received_date }}</td>
                                            <td>
                                                {{ $shipping_package_local->shipping_method_provider_name }}
                                            </td>
                                            <td>{{ $shipping_package_local->note }}</td>
                                        </tr>
                                        <?php $no++; ?>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="7">{{ __('label.no_records') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
