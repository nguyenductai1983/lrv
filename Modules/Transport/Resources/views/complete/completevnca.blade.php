@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.shipment_view')}}:
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.complete.completevnca') }}" method="get" role="form" class="form-inline">

                    <div class="form-group">
                        <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code')}}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>

                    </div>
                </form>
            </div>
        </div>
        <form id="order-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                <input type="checkbox" id="select-all">
                            </th>
                            <th width="13%">{{ __('label.order_code')}}</th>
                            <th>{{ __('label.note')}}</th>
                            <th>{{ __('label.goods_info')}}</th>
                            <th>{{ __('label.note')}}</th>
                            <th>{{ __('label.status')}}</th>
                        </tr>
                    </thead>
                    <tbody id="orderitems">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{ $no }}</td>
                            <td>
                                <input type="checkbox" name="order_id{{ $no }}" value="{{ $order->id }}" checked="">
                            </td>
                            <td>{{ $order->code }}</td>
                            <td>
                                <input type="text" name="note{{ $no }}" value="{{ $order->code }}">

                            </td>
                            <td>{{ $order->products }}</td>
                            <td>{{ $order->user_note }}</td>
                            <td>
                                <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </form>
        <div class="text-right">
            <a href="{{ route('transport.receive.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <button type="button" class="btn btn-info" onclick="complete.vncacomplete();">
                <i class="fa fa-check"></i> {{ __('label.receiver')}}
            </button>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/complete.js"></script>
<script>
    $("#order_code").on('keypress', function (event) {
        if (event.which === 13) {
            shipment.addItemExist();
        }
    });

    $('#select-all').click(function(event) {
        if(this.checked) {
            $('#order-package-form input[type=checkbox]').each(function() {
                this.checked = true;
            });
        }else{
            $('#order-package-form input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });
</script>
@endsection
