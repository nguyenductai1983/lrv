@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.shipment_view')}}: {{ $shipment->code }}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form id="shipment-package-info-form" class="form-inline">
                <div class="form-group">
        <label for="shipment_name" class="control-label">{{ __('label.name')}} Shipment</label>
        <input name="shipment_name" id="shipment_name" type="text" class="form-control"
        value="{{ $shipment->name }}" >
                </div>
                    <input name="id" type="hidden" value="{{ $shipment->id }}">
                    <div class="form-group">
                        <input name="estimate_delivery" id="estimate_delivery" value="{{ $shipment->estimate_delivery}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.date')}}">
                    </div>
                    <br/>
                    <div class="form-group">
                        <select name="from_warehouse_id" class="form-control">
                            @if($warehouses->count() > 0)
                                @foreach($warehouses as $warehouse)
                                    <option value="{{ $warehouse->id }}"
                                        @if($shipment->from_warehouse_id == $warehouse->id)
                                        selected
                                        @endif
                                        >
                                        {{ $warehouse->name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                       <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code')}}">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-white btn-single" onclick="shipment.addItemExist({{ $shipment->id}});">
                            <i class="fa fa-search"></i> {{ __('label.find')}}
                        </button>
                        <a href="{{ route('transport.shipment.exportExcelOrder', $shipment->id) }}" class="btn btn-white btn-single">
                            {{ __('label.export_excel') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form" class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="7%">{{ __('label.no')}}</th>
                            <th width="15%">{{ __('label.order_code')}}</th>
                            <th width="20%">{{ __('label.goods_info')}}</th>
                            <th width="43%">{{ __('label.note')}}</th>
                            <th width="10%">{{ __('label.shipping_status')}}</th>
                            <th width="10%">{{ __('label.action')}}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                          <?php $no = 1; ?>
                        @foreach($orders as $order)
                            <tr id="shipment-package-item-{{ $order->id }}">
                            <td>{{ $no }}</td>
                            <td>{{ $order->code }}</td>
                            <td>{{ $order->products }}</td>
                            <td>{{ $order->user_note }}</td>
                            <td>
                                <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                            <td>
                               {{-- {{ $order->shipping_status }} <br>
                                {{ $order->shipping_status_shipment }} --}}
                                 @php if($order->shipping_status == $order->shipping_status_shipment){ @endphp
                                <button type="button" onclick="shipment.removeItemExist({{ $order->id }}, {{ $shipment->id }})" class="btn btn-xs btn-danger">
                                    <i class="fas fa-trash"></i>
                                </button>
                                @php } @endphp
                            </td>
                        </tr>
                         <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </form>
        <div class="text-right">
            <a href="{{ route('transport.shipment.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <button type="button" class="btn btn-info" onclick="shipment.editPackage('{{ route('transport.shipment.index') }}');">
                <i class="fa fa-check"></i> {{ __('label.update')}}
            </button>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/shipment.js"></script>
<script>
    $(function () {
        $("#estimate_delivery").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    $("#order_code").on('keypress', function (event) {
        if (event.which === 13) {
            shipment.addItemExist({{ $shipment->id}});
        }
    });
</script>
@endsection
