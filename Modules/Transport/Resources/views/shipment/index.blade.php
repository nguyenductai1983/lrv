@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.shipment_title') }}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-1">
                <div class="form-group">
                    <a href="{{ route('transport.shipment.create') }}" class="btn btn-success btn-single">
                        <i class="fa fa-plus-circle"></i> {{ __('label.add') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.shipment.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" id="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.shipment_code') }}">
                        </div>
                    <div class="form-group">
                        <select name="status" id="country_id" class="form-control">
                            <option value="">{{ __('label.status_all') }}</option>
                            <option value="1" @php if(request()->query('status') == 1){@endphp selected @php }@endphp>{{ __('label.status_normal') }}</option>
                            <option value="2" @php if(request()->query('status') == 2){@endphp selected @php }@endphp>{{ __('label.status_cancel') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.shipment.index') }}" class="btn btn-white btn-single">
                            {{ __('label.clear') }}
                        </a>
                        <a href="{{ route('transport.shipment.exportExcel') }}" class="btn btn-white btn-single">
                            {{ __('label.export_excel') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('label.shipment_code') }}</th>
                        <th>{{ __('label.name') }}</th>
                        <th>{{ __('label.employee') }}</th>
                        <th>{{ __('label.create_time') }}</th>
                        <th>{{ __('label.estimate_delivery') }}</th>
                        <th>{{ __('label.status') }}</th>
                        <th>{{ __('label.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($shipments->count() > 0)
                    <?php $no = 1; ?>
                    @foreach($shipments as $shipment)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $shipment->code }}</td>
                        <td>{{ $shipment->name }}</td>
                        <td>{{ $shipment->first_name ? $shipment->first_name : '' }} {{ $shipment->middle_name ? $shipment->middle_name : '' }} {{ $shipment->last_name ? $shipment->last_name : '' }}</td>
                        <td>{{ $shipment->created_at }}</td>
                        <td>{{ $shipment->estimate_delivery }}</td>
                        <td>
                            @if($shipment->status == 1)
                            <span class="label label-success">{{ __('label.status_normal') }}</span>
                            @else
                            <span class="label label-danger">{{ __('label.status_cancel') }}</span>
                            @endif
                        </td>
                        <td>
                            @if($shipment->status == 1)
                            <a href="{{ route('transport.shipment.view', $shipment->id) }}" class="btn btn-xs btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">{{ __('label.no_records') }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="paginate-single">
            {{ $shipments->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
