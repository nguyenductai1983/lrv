@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.shipment_create')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form role="form" class="form-inline" action="{{ route('transport.shipment.create') }}" method="get">
                    <div class="form-group">
                        <input name="estimate_delivery" id="estimate_delivery" value="{{ date('d/m/Y')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.date')}}">
                    </div>
                    <div class="form-group">
                        <select name="from_warehouse_id" class="form-control">
                             <option value="">{{ __('label.select') }}</option>
                            @if($warehouses->count() > 0)
                                @foreach($warehouses as $warehouse)
                                    @if ($warehouse->id == $warehouses_id){
                                    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->name }}</option>
                                    }
                                    @else
                                    {
                                     <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                    }
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <input name="order_code" id="order_code" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code')}}">
                    </div>
                    <div class="form-group">
                        <button type="submit"  class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="form-group">
        <label for="shipment_name" class="control-label col-sm-2">{{ __('label.name')}} Shipment</label>
        <div class="col-sm-4">
        <input name="shipment_name" type="text" class="form-control" value="">
        </div>
            </div>        
        <form id="shipment-package-form" class="col-sm-12">            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="select-all" checked="">
                            </th>
                            <th>{{ __('label.no')}}</th>
                            <th>{{ __('label.order_code')}}</th>
                            <th>{{ __('label.goods_info')}}</th>
                            <th>{{ __('label.note')}}</th>
                            <th>{{ __('label.countries')}}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                         @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr id="receipt-package-create-{{ $order->id }}">
                            <td>
                                <input type="checkbox" name="id" value="{{ $order->id }}" checked=""></td>
                            <td>{{ $no }}</td>
                            <td>{{ $order->code }}</td>
                            <td>{{ $order->products }}</td>
                             <td>{{ $order->note }}</td>   
                             <td>{{ $order->receiver_country_name }}</td>                             
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr id="empty-item">
                            <td colspan="7">{{ __('label.no_records')}}</td>
                        </tr>
                        @endif
                        
                    </tbody>
                </table>
            </div>
        </form>
        <input name="numItem" type="hidden" value="1">
        <div class="text-right">
            <a href="{{ route('transport.shipment.index') }}" class="btn btn-white">
                <i class="fa fa-arrow-left"></i> {{ __('label.back')}}
            </a>
            <button type="button" class="btn btn-info" 
            onclick="shipment.addPackage('{{ route('transport.shipment.index') }}');">
                <i class="fa fa-check"></i> {{ __('label.add')}}
            </button>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/shipment.js"></script>
<script>
    $(function () {
        $("#estimate_delivery").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
    $("#order_code").on('keypress', function (event) {
        if (event.which === 13) {
            shipment.addItem();
        }
    });
    $('#select-all').click(function(event) {   
        if(this.checked) {
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = true;                        
            });
        }else{
            $('#shipment-package-items input[type=checkbox]').each(function() {
                this.checked = false;                        
            });
        }
    });
</script>
@endsection