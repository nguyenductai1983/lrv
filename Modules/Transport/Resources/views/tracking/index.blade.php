@extends('layouts.admin.index', ['menu' => $menu])

@section('body')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ __('transport.tracking_order')}}
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-sm-12">
                <form action="{{ route('transport.tracking.index') }}" method="get" role="form" class="form-inline">
                    <div class="form-group">
                        <input name="from_date" id="from_date" value="{{ request()->query('from_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.from_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="to_date" id="to_date" value="{{ request()->query('to_date')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.to_date') }}">
                    </div>
                    <div class="form-group">
                        <input name="code" value="{{ request()->query('code')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.order_code') }}">
                    </div>
                    <div class="form-group">
                        <input name="receiver_phone" value="{{ request()->query('receiver_phone')}}" class="form-control" type="text" style="width: 190px;" placeholder="{{ __('label.receiver_phone') }}">
                    </div>
                    <div class="form-group">
                       <label>{{ __('label.shipping_status') }}</label>
                        <select class="form-control" name="shipping_status" >
                        @foreach ($list_status as $key => $value)
                        <option class="form-control ng-pristine ng-valid ng-empty ng-touched" value="{{ $key }}" {{ ( $key == request()->query('shipping_status')) ? 'selected' : '' }}>
                            {{ $value }}
                        </option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-white btn-single">
                            <i class="fa fa-search"></i> {{ __('label.find') }}
                        </button>
                        <a href="{{ route('transport.tracking.index') }}" class="btn btn-white btn-single">
                            <i class="fas fa-broom"></i> {{ __('label.clear') }}
                        </a>
                        <button type="button" class="btn btn-white btn-single" onclick="tracking.export();"><i class="fas fa-file-excel"></i>{{ __('label.export_excel') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <form id="shipment-package-form">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" style="min-width: 2500px;">
                    <thead>
                        <tr>
                            <th width="120">{{ __('label.action') }}</th>
                            <th width="120">{{ __('label.status') }} <br>
                                {{ __('label.shipping_status') }}</th>
                            <th width="150">{{ __('label.form_code') }}</th>
                            <th width="120">{{ __('label.employee') }}</th>
                            <th width="150">{{ __('label.customer') }}</th>
                            <th width="150">{{ __('label.receiver') }}</th>
                            <th width="120" class="text-center">{{ __('label.date') }}</th>
                            <th width="150">{{ __('label.product_id') }}</th>
                            <th width="120" class="text-right">{{ __('label.total_pay') }} ({{ auth()->user()->agency->currency->code }})</th>
                            <th width="120" class="text-right">{{ __('label.total_weight') }} ({{ auth()->user()->agency->weight_unit->code }})</th>
                            <th width="110" class="text-right">{{ __('label.pay') }} ({{ auth()->user()->agency->currency->code }})</th>
                            <th width="110" class="text-right">{{ __('label.total_discount') }} ({{ auth()->user()->agency->currency->code }})</th>
                            <th width="160">{{ __('label.address') }}</th>
                            <th width="140">{{ __('label.province') }}</th>
                            <th width="140">{{ __('label.city') }}</th>
                            <th width="120">{{ __('label.telephone') }}</th>
                            <th width="120">{{ __('label.action') }}</th>
                        </tr>
                    </thead>
                    <tbody id="shipment-package-items">
                        @if($orders->count() > 0)
                        <?php $no = 1; ?>
                        @foreach($orders as $order)
                        <tr {{ ($order->shipping_fast) ? 'class=bg-success text-white' : "" }}>
                            <td>
                                <a href="{{ route('transport.print', $order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                    <i class="fa fa-print"></i>
                                </a>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.viewHistory({{ $order->id }});">
                                    <i class="fa fa-truck"></i>
                                </button>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.viewLog({{ $order->id }});">
                                    <i class="fa fa-history"></i>
                                </button>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.reshipOrder({{ $order->id }});">
                                    <i class="fa fa-history"></i>
                                </button>
                                <a href="{{ route('transport.tracking.showphone',$order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                   <i class="fa fa-phone-square" aria-hidden="true" ></i>
                                </a>
                                @if(auth()->user()->role->admin)
                                <a href="{{ route('transport.detail', $order->id) }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                                @endif
                            </td>
                            <td>
                                <span class="{{ $order->status_label }}">{{ $order->status_name }}</span>
                                <br>
                                <span class="{{ $order->shipping_status_label }}">{{ $order->shipping_status_name }}</span>
                            </td>
                            <td>{{ $order->code }}
                                {{ isset($order->warehouse_package->code) ? $order->warehouse_package->code : '' }}
                                {{ isset($order->shipment->code) ? $order->shipment->code : '' }}
                                {{ isset($order->voucher->code) ? $order->voucher->code : '' }}
                            </td>
                            <td>{{ isset($order->user->code) ? $order->user->code : '' }}</td>
                            <td>{{ $order->sender_full_name }}</td>
                            <td>{{ $order->receive_full_name }}</td>
                            <td class="text-center">{{ $order->created_date }}</td>
                            <td>{{ $order->products }}</td>
                            <td class="text-right">{{ number_format($order->total_final, 2) }}</td>
                            <td class="text-right">{{ number_format($order->total_weight, 2) }}</td>
                            <td class="text-right">{{ number_format($order->total_paid_amount, 2) }}</td>
                            <td class="text-right">{{ number_format($order->total_discount, 2) }}</td>
                            <td>{{ $order->receiver_address }}</td>
                            <td>{{ $order->receiver_province->name }}</td>
                            <td>{{ $order->receiver_city->name }}</td>
                            <td>
                                {{ $order->receiver_phone }}<br>
                                {{ $order->receiver_cellphone }}
                            </td>
                            <td>
                                <a href="{{ route('transport.print', $order->id) }}" target="_blank" class="btn btn-xs btn-info">
                                    <i class="fa fa-print "></i>
                                </a>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.viewHistory({{ $order->id }});">
                                    <i class="fa fa-truck"></i>
                                </button>
                                <button type="button" class="btn btn-xs btn-info" onclick="tracking.viewLog({{ $order->id }});">
                                    <i class="fa fa-history"></i>
                                </button>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('label.no_records') }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="paginate-single">
                {{ $orders->appends(request()->query())->links() }}
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="/js/transport/tracking.js"></script>
<script>
    $(function () {
        $("#from_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
        $("#to_date").datepicker({dateFormat: "{{ config('app.date_format_js') }}"});
    });
</script>
@endsection
