@extends('layouts.admin.index', ['menu' => $menu])
@section('head')
  <link rel="stylesheet"
    href="{{ asset('css/admin/tracking.css?t=' . File::lastModified(public_path('css/admin/tracking.css'))) }}">
@endsection
@section('body')
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          {{ __('label.order_code') }}:
          <a href="{{ route('transport.edit', $order->id) }}">
            {{ $order->code }} <i class="fa fa-eye"></i>
          </a>
        </div>
        <div class="col-xs-12 col-md-6 text-right">
          <form action="{{ route('transport.tracking.findtracking') }}" method="post" role="form" class="form-inline">
            @csrf
            <div class="form-group">
              <input name="code" class="form-control" type="text" value="{{ $order->code }}" style="width: 190px;"
                placeholder="{{ __('label.order_code') }}">

              <button type="submit" class="btn btn-white btn-single">
                <i class="fa fa-search"></i> {{ __('label.find') }}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="row form-group">
        <div class="col-sm-12">
          <h3> {{ __('label.update_receiver') }}</h3>
        </div>
      </div>
      <form role="form" id="form-updatephone" action="{{ route('transport.tracking.updatephone') }}" method="post">
        @csrf
        <input type="hidden" name="order_id" value="{{ $order->id }}" />
        <input type="hidden" name="address_id" value="{{ $order->shipping_address_id }}" />
        <div class="row form-group">
          <div class="col-sm-12">
            <h4> {{ __('label.receiver') }} : {{ $order->receive_full_name }}</h5>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-1">
            {{ __('customer.first_name') }}
          </div>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="address.first_name" value="{{ $order->receive_first_name }}">
          </div>

          <div class="col-sm-1">
            {{ __('customer.middle_name') }}
          </div>
          <div class="col-sm-3"><input type="text" class="form-control" name="address.middle_name"
              value="{{ $order->receiver_middle_name }}"></div>

          <div class="col-sm-1">
            {{ __('customer.last_name') }}
          </div>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="address.last_name" value="{{ $order->receive_last_name }}">
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-1">
            {{ __('receiver.telephone') }} <i class="fa fa-phone"></i><i class="text-danger">*</i>
          </div>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="address.telephone" value="{{ $order->receiver_phone }}">
          </div>
          <div class="col-sm-1">
            {{ __('receiver.cellphone') }} <i class="fa fa-phone"></i>
          </div>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="address.cellphone" value="{{ $order->receiver_cellphone }}">
          </div>
          <div class="col-sm-4  text-right">
            <button type="submit" class="btn btn-info">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button class="btn btn-info">
          </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        @if (Session::has('complete'))
          <div class='alert alert-success' role="alert">
            {{ __('label.complete') }}
          </div>
        @endif
      </form>
      <hr style="margin: 30pt">
      <h3> {{ __('label.update_status_shipping') }}  {{ $order->code }}</h3>
      <article class="card">
        <div class="track">
          <div class="step active"> <span class="icon"> <i class="fas fa-warehouse"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_stockin') }}
            </span>
          </div>
          <div class="step {{ $order->shipping_status >= 3 ? 'active' : '' }}"> <span class="icon"> <i
                class="fas fa-pallet"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_shipment') }}
            </span>
          </div>
          <div class="step {{ $order->shipping_status >= 4 ? 'active' : '' }}"> <span class="icon"> <i
                class="fa fa-plane"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_stockout') }}
            </span>
          </div>
          <div class="step {{ $order->shipping_status >= 6 ? 'active' : '' }}"> <span class="icon"> <i
                class="fa fa-th-large"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_local') }}
            </span>
          </div>
          <div class="step {{ $order->shipping_status >= 7 ? 'active' : '' }}"> <span class="icon"> <i
                class="fab fa-stack-overflow"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_carrier') }}
            </span>
          </div>
          <div class="step {{ $order->shipping_status >= 9 ? 'active' : '' }}"> <span class="icon"> <i
                class="fa fa-truck"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_ready') }}
            </span>
          </div>
          <div class="step {{ $order->shipping_status >= 10 ? 'active' : '' }}"> <span class="icon"> <i
                class="fa fa-check"></i> </span>
            <span class="text">
              {{ __('order.shipping_status_done') }}
            </span>
          </div>
        </div>
        <hr>
        <form role="form" id="form-updatestatus" action="{{ route('transport.tracking.updatestatus') }}" method="post">
          @csrf
          <input type="hidden" name="order_id" value="{{ $order->id }}" />
          <div class="row form-group">
            <div class="col-sm-2">
              <label>{{ __('label.order_status') }}</label>
            </div>
            <div class="col-sm-3">
              <select class="form-control" name="order_status">
                @foreach ($list_order_status as $key => $value)
                  <option class="form-control ng-pristine ng-valid ng-empty ng-touched" value="{{ $key }}"
                    {{ $key == $order->order_status ? 'selected' : '' }}>
                    {{ $value }}
                  </option>
                @endforeach
              </select>
            </div>
            <div class="col-sm-2">
              <label>{{ __('label.shipping_status') }}</label>
            </div>
            <div class="col-sm-3">
              <select class="form-control" name="shipping_status">
                @foreach ($list_shingping_status as $key => $value)
                  <option class="form-control ng-pristine ng-valid ng-empty ng-touched" value="{{ $key }}"
                    {{ $key == $order->shipping_status ? 'selected' : '' }}>
                    {{ $value }}
                  </option>
                @endforeach
              </select>
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-info">
                <i class="fa fa-check"></i> {{ __('label.update') }}
              </button class="btn btn-info">
            </div>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          @if (Session::has('complete_status'))
            <div class='alert alert-success' role="alert">
              {{ __('label.complete_update') }}
            </div>
          @endif
        </form>
      </article>
      <br>
      <div class="row">
        <div class="col-sm-6">
          Tracking
          <ul class="timeline">
            @if($order_trackings->count() > 0)
            @foreach ($order_trackings as $order_tracking)
              <li>
                {{ $order_tracking->created_at }} -
                {{ isset($order_tracking->user->code) ? $order_tracking->user->code : '' }}
                : {{ $order_tracking->status }} - {{ $list_shingping_status[$order_tracking->status] }}
                <a href="{{ route('transport.tracking.delete', $order_tracking->id) }}" class="btn btn-xs btn-info">
                  <i class="fas fa-window-close"></i>
                </a>
                <p>{{ $order_tracking->note }}
                </p>

              </li>
            @endforeach
            @endif
          </ul>
          @if (Session::has('tracking_status'))
            <div class='alert alert-success' role="alert">
              {{ __('label.complete_update') }}
            </div>
          @endif
        </div>
        <div class="col-sm-6">
          Order Log
          <ul class="timeline">
            @if($ordersLog->count() > 0)
            @foreach ($ordersLog as $orderLog)
              <li>
                {{ $orderLog->created_at }} -
                {{ isset($orderLog->user->code) ? $orderLog->user->code : '' }}
                : {{ $orderLog->status }} - {{ $status_log[$orderLog->status] }}
                <p>{{ $orderLog->description }} </p>
              </li>
            @endforeach
            @endif
          </ul>
        </div>
      </div>
      {{-- them ngay 21-07-21 --}}
      <hr style="margin: 30pt">
      <h3> {{ __('label.update_date_create') }}  VNA  {{ $order->code }}</h3>
      <form role="form" id="form-date" action="{{ route('transport.tracking.createat') }}" method="post">
        @csrf
        <input type="hidden" name="order_id" value="{{ $order->id }}" />
        <div class="row form-group">
          <div class="col-sm-2">
            <h5> {{ __('label.date_current') }} </h5>
          </div>
          <div class="col-sm-3">
            <h5>
              <input type="text" id="created_at_old" class="form-control"
                value="{{ $order->created_at->format('d/m/Y') }}" disabled>
            </h5>
          </div>
          <div class="col-sm-2">
            <h5> {{ __('label.date_new') }}</h5>
          </div>
          <div class="col-sm-3">
            <h5>
              <div class="input-group date-picker" data-change-year="true" data-change-month="true">
                <input name="created_at" id="created_at" value="{{ $order->created_at->format('d/m/Y') }}"
                  class="form-control" type="text" style="width: 160px;" autocomplete="off">
              </div>
            </h5>
          </div>
          <div class="col-sm-2">
            <button type="submit" class="btn btn-info">
              <i class="fa fa-check"></i> {{ __('label.update') }}
            </button class="btn btn-info">
          </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="row">
          <div class="col-sm-12">
            @if (Session::has('complete_create_at'))
              <div class='alert alert-success' role="alert">
                {{ __('label.data_updated') }}
              </div>
          </div>
          @endif
        </div>
      </form>
      {{-- het them ngay 21-07-21 --}}
      <div class="text-left" style="padding-top: 30px;"> <a href="{{ route('transport.tracking.index') }}"
          class="btn btn-white">
          <i class="fa fa-chevron-left"></i> {{ __('label.back') }}
        </a>
      </div>
    </div>
  </div>

@endsection
@section('footer')
  <script>
    $("#created_at").datepicker({
      dateFormat: "{{ config('app.date_format_js') }}"
    });
  </script>
@endsection
