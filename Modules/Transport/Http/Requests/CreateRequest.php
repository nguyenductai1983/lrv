<?php

namespace Modules\Transport\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'customer' => 'required|array',
            'customer.id' => 'nullable|exists:customers,id',
            'customer.image_1_file_id' => 'nullable|exists:files,id',
            'customer.image_2_file_id' => 'nullable|exists:files,id',
            'customer.image_3_file_id' => 'nullable|exists:files,id',
            'customer.first_name' => 'required|required_without:customer.id|string|max:50',
            'customer.middle_name' => 'nullable|string|max:50',
            'customer.last_name' => 'nullable|required_without:customer.id|string|max:50',
            'customer.address_1' => 'required|required_without:customer.id|string|max:255',
            'customer.address_2' => 'nullable|string|max:250',
            'customer.email' => 'nullable|email',
            'customer.telephone' => 'required|required_without:customer.id|string|max:20',
            'customer.cellphone' => 'nullable|string|max:20',
            'customer.postal_code' => 'required|required_without:customer.id|string|max:20',
            'customer.id_card' => 'nullable|string|max:20',
            'customer.card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'customer.birthday' => 'nullable|date_format:' . config('app.date_format'),
            'customer.career' => 'nullable|string|max:100',
            'customer.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'customer.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'customer.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:255',
            'receiver.address_2' => 'nullable|string|max:250',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.cellphone' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',
            'receiver.ward_id' => 'requiredIf:receiver.country_id,91',

            'containers' => 'required|array',
            'containers.*.length' => 'nullable|numeric|min:0',
            'containers.*.width' => 'nullable|numeric|min:0',
            'containers.*.height' => 'nullable|numeric|min:0',
            'containers.*.volume' => 'nullable|numeric|min:0',
            'containers.*.shipping_fee' => 'nullable|numeric|min:0',
            'containers.*.total_weight' => 'required|numeric|min:0',
            'containers.*.total_shipping_fee' => 'required|numeric|min:0',
            'containers.*.total_declared_value' => 'required|numeric|min:0',
            'containers.*.total_surcharge' => 'required|numeric|min:0',
            'containers.*.total_insurance' => 'required|numeric|min:0',
            'containers.*.total_amount' => 'required|numeric|min:0',
            'containers.*.total_discount' => 'required|numeric|min:0',
            'containers.*.total' => 'required|numeric|min:0',
            'containers.*.date_pay' => 'nullable|date_format:' . config('app.date_format'),
            'containers.*.pay' => 'required|numeric|min:0',
            'containers.*.debt' => 'required|numeric|min:0',
            'containers.*.currency_id' => 'required|exists:currencies,id',
            'containers.*.pay_method' => 'required|string',
            'containers.*.status' => 'required|in:' . implode(',', config('transport.statues')),
            'containers.*.products' => 'required|array',
            'containers.*.products.*.code' => 'required|string',
            'containers.*.products.*.name' => 'required|string',
            'containers.*.products.*.by_weight' => 'required|in:0,1',
            'containers.*.products.*.quantity' => 'required|numeric|min:0',
            'containers.*.products.*.weight' => 'required|numeric|min:0',
            'containers.*.products.*.price' => 'required|numeric|min:0',
            'containers.*.products.*.per_discount' => 'nullable|numeric|min:0',
            'containers.*.products.*.amount' => 'required|numeric|min:0',
            'containers.*.products.*.declared_value' => 'required|numeric|min:0',
            'containers.*.products.*.surcharge' => 'nullable|numeric|min:0',
            'containers.*.products.*.insurance' => 'nullable|numeric|min:0',
            'containers.*.products.*.total' => 'required|numeric|min:0',
            'containers.*.products.*.note' => 'nullable|string|max:255'
        ];
    }

    public function attributes()
    {
        return[
            'customer.id' => __('label.sender'),
            'customer.image_1_file_id' => __('customer.image_1_file_id'),
            'customer.image_2_file_id' => __('customer.image_2_file_id'),
            'customer.image_3_file_id' => __('customer.image_3_file_id'),
            'customer.first_name' => __('customer.first_name'),
            'customer.middle_name' => __('customer.middle_name'),
            'customer.last_name' => __('customer.last_name'),
            'customer.address_1' => __('customer.address_1'),
            'customer.email' => __('customer.email'),
            'customer.telephone' => __('customer.telephone'),
            'customer.cellphone' => __('customer.cellphone'),
            'customer.postal_code' => __('customer.postal_code'),
            'customer.id_card' => __('customer.id_card'),
            'customer.card_expire' => __('customer.card_expire'),
            'customer.birthday' => __('customer.birthday'),
            'customer.career' => __('customer.career'),
            'customer.country_id' => __('customer.country_id'),
            'customer.province_id' => __('customer.province_id'),
            'customer.city_id' => __('customer.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),
            'receiver.ward_id' => __('receiver.ward_id'),

            'containers' => __('label.containers'),
            'containers.*.length' => __('label.length'),
            'containers.*.width' => __('label.width'),
            'containers.*.height' => __('label.height'),
            'containers.*.volume' => __('label.volume'),
            'containers.*.shipping_fee' => __('label.shipping_fee'),
            'containers.*.total_weight' => __('label.total_weight'),
            'containers.*.total_shipping_fee' => __('label.total_shipping_fee'),
            'containers.*.total_declared_value' => __('label.total_declared_value'),
            'containers.*.total_surcharge' => __('label.total_surcharge'),
            'containers.*.total_insurance' => __('label.total_insurance'),
            'containers.*.total_amount' => __('label.total_amount'),
            'containers.*.total_discount' => __('label.total_discount'),
            'containers.*.total' => __('label.total'),
            'containers.*.date_pay' => __('label.date_pay'),
            'containers.*.pay' => __('label.total_pay'),
            'containers.*.debt' => __('label.debt'),
            'containers.*.currency_id' => __('label.currency_pay'),
            'containers.*.pay_method' => __('label.pay_method'),
            'containers.*.status' => __('label.status'),

            'containers.*.products' => __('label.products'),
            'containers.*.products.*.product_id' => __('label.product_id'),
            'containers.*.products.*.code' => __('product.code'),
            'containers.*.products.*.name' => __('product.name'),
            'containers.*.products.*.by_weight' => __('product.by_weight'),
            'containers.*.products.*.quantity' => __('label.quantity_short'),
            'containers.*.products.*.weight' => __('label.weight_short'),
            'containers.*.products.*.price' => __('label.price'),
            'containers.*.products.*.per_discount' => __('label.discount_short'),
            'containers.*.products.*.unit' => __('label.unit_short'),
            'containers.*.products.*.amount' => __('label.amount'),
            'containers.*.products.*.declared_value' => __('label.declared_value_short'),
            'containers.*.products.*.surcharge' => __('label.surcharge'),
            'containers.*.products.*.insurance' => __('label.insurance_short'),
            'containers.*.products.*.total' => __('label.total'),
            'containers.*.products.*.note' => __('label.note')
        ];
    }
}
