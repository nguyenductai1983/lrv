<?php

namespace Modules\Transport\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'customer' => 'required|array',
            'customer.id' => 'required|exists:customers,id',
            'customer.image_1_file_id' => 'nullable|exists:files,id',
            'customer.image_2_file_id' => 'nullable|exists:files,id',
            'customer.image_3_file_id' => 'nullable|exists:files,id',
            'customer.first_name' => 'required|required_without:customer.id|string|max:50',
            'customer.middle_name' => 'nullable|string|max:50',
            'customer.last_name' => 'nullable|required_without:customer.id|string|max:50',
            'customer.address_1' => 'required|required_without:customer.id|string|max:255',
            'customer.email' => 'nullable|email',
            'customer.telephone' => 'required|required_without:customer.id|string|max:20',
            'customer.cellphone' => 'nullable|string|max:20',
            'customer.postal_code' => 'required|required_without:customer.id|string|max:20',
            'customer.id_card' => 'nullable|string|max:20',
            'customer.card_expire' => 'nullable|date_format:' . config('app.date_format'),
            'customer.birthday' => 'nullable|date_format:' . config('app.date_format'),
            'customer.career' => 'nullable|string|max:100',
            'customer.country_id' => 'required|required_without:customer.id|exists:countries,id',
            'customer.province_id' => 'required|required_without:customer.id|exists:provinces,id',
            'customer.city_id' => 'required|required_without:customer.id|exists:cities,id',

            'receiver' => 'required|array',
            'receiver.first_name' => 'required|string|max:50',
            'receiver.middle_name' => 'nullable|string|max:50',
            'receiver.last_name' => 'required|string|max:50',
            'receiver.address_1' => 'required|string|max:255',
            'receiver.postal_code' => 'nullable|string|max:20',
            'receiver.telephone' => 'required|string|max:20',
            'receiver.cellphone' => 'nullable|string|max:20',
            'receiver.country_id' => 'required|exists:countries,id',
            'receiver.province_id' => 'required|exists:provinces,id',
            'receiver.city_id' => 'required|exists:cities,id',
            'receiver.ward_id' => 'requiredIf:receiver.country_id,91',

            'containers' => 'required|array',
            'containers.*.length' => 'nullable|numeric|min:0',
            'containers.*.width' => 'nullable|numeric|min:0',
            'containers.*.height' => 'nullable|numeric|min:0',
            'containers.*.volume' => 'nullable|numeric|min:0',
            'containers.*.shipping_fee' => 'nullable|numeric|min:0',
            'containers.*.total_weight' => 'required|numeric|min:0',
            'containers.*.total_shipping_fee' => 'required|numeric|min:0',
            'containers.*.total_declared_value' => 'required|numeric|min:0',
            'containers.*.total_surcharge' => 'required|numeric|min:0',
            'containers.*.total_insurance' => 'required|numeric|min:0',
            'containers.*.total_amount' => 'required|numeric|min:0',
            'containers.*.total_discount' => 'required|numeric|min:0',
            'containers.*.total' => 'required|numeric|min:0',
            'containers.*.last_date_pay' => 'nullable|date_format:' . config('app.date_format'),
            'containers.*.last_paid_amount' => 'nullable|numeric',
            'containers.*.payment_method' => 'nullable|string',
            'containers.*.receive_status' => 'required|in:' . implode(',', config('transport.statues')),

            'containers.*.products' => 'required|array',
            'containers.*.products.*.product.code' => 'required|string',
            'containers.*.products.*.product.name' => 'required|string',
            'containers.*.products.*.product.by_weight' => 'required|in:0,1',
            'containers.*.products.*.quantity' => 'required|numeric|min:0',
            'containers.*.products.*.sub_total_weight' => 'required|numeric|min:0',
            'containers.*.products.*.unit_goods_fee' => 'required|numeric|min:0',
            'containers.*.products.*.per_discount' => 'nullable|numeric|min:0',
            'containers.*.products.*.sub_total_declare_price' => 'required|numeric|min:0',
            'containers.*.products.*.sub_total_surcharge_fee' => 'nullable|numeric|min:0',
            'containers.*.products.*.sub_total_insurrance_fee' => 'nullable|numeric|min:0',
            'containers.*.products.*.sub_total_goods' => 'required|numeric|min:0',
            'containers.*.products.*.user_note' => 'nullable|string|max:255'
        ];
    }

    public function attributes()
    {
        return[
            'customer.id' => __('label.sender'),
            'customer.image_1_file_id' => __('customer.image_1_file_id'),
            'customer.image_2_file_id' => __('customer.image_2_file_id'),
            'customer.image_3_file_id' => __('customer.image_3_file_id'),
            'customer.first_name' => __('customer.first_name'),
            'customer.middle_name' => __('customer.middle_name'),
            'customer.last_name' => __('customer.last_name'),
            'customer.address_1' => __('customer.address_1'),
            'customer.email' => __('customer.email'),
            'customer.telephone' => __('customer.telephone'),
            'customer.cellphone' => __('customer.cellphone'),
            'customer.postal_code' => __('customer.postal_code'),
            'customer.id_card' => __('customer.id_card'),
            'customer.card_expire' => __('customer.card_expire'),
            'customer.birthday' => __('customer.birthday'),
            'customer.career' => __('customer.career'),
            'customer.country_id' => __('customer.country_id'),
            'customer.province_id' => __('customer.province_id'),
            'customer.city_id' => __('customer.city_id'),

            'receiver.first_name' => __('receiver.first_name'),
            'receiver.middle_name' => __('receiver.middle_name'),
            'receiver.last_name' => __('receiver.last_name'),
            'receiver.address_1' => __('receiver.address'),
            'receiver.postal_code' => __('receiver.postal_code'),
            'receiver.telephone' => __('receiver.telephone'),
            'receiver.cellphone' => __('receiver.cellphone'),
            'receiver.country_id' => __('receiver.country_id'),
            'receiver.province_id' => __('receiver.province_id'),
            'receiver.city_id' => __('receiver.city_id'),
            'receiver.ward_id' => __('receiver.ward_id'),

            'containers' => __('label.containers'),
            'containers.*.length' => __('label.length'),
            'containers.*.width' => __('label.width'),
            'containers.*.height' => __('label.height'),
            'containers.*.volume' => __('label.volume'),
            'containers.*.shipping_fee' => __('label.shipping_fee'),
            'containers.*.total_weight' => __('label.total_weight'),
            'containers.*.total_shipping_fee' => __('label.total_shipping_fee'),
            'containers.*.total_declared_value' => __('label.total_declared_value'),
            'containers.*.total_surcharge' => __('label.total_surcharge'),
            'containers.*.total_insurance' => __('label.total_insurance'),
            'containers.*.total_amount' => __('label.total_amount'),
            'containers.*.total_discount' => __('label.total_discount'),
            'containers.*.total' => __('label.total'),
            'containers.*.last_date_pay' => __('label.date_pay'),
            'containers.*.last_paid_amount' => __('label.total_pay'),
            'containers.*.currency_id' => __('label.currency_pay'),
            'containers.*.payment_method' => __('label.pay_method'),
            'containers.*.receive_status' => __('label.status'),

            'containers.*.products' => __('label.products'),
            'containers.*.products.*.product_id' => __('label.product_id'),
            'containers.*.products.*.product.code' => __('product.code'),
            'containers.*.products.*.product.name' => __('product.name'),
            'containers.*.products.*.product.by_weight' => __('product.by_weight'),
            'containers.*.products.*.quantity' => __('label.quantity_short'),
            'containers.*.products.*.sub_total_weight' => __('label.weight_short'),
            'containers.*.products.*.unit_goods_fee' => __('label.price'),
            'containers.*.products.*.per_discount' => __('label.discount_short'),
            'containers.*.products.*.messure.code' => __('label.unit_short'),
            'containers.*.products.*.sub_total_goods' => __('label.amount'),
            'containers.*.products.*.sub_total_declare_price' => __('label.declared_value_short'),
            'containers.*.products.*.sub_total_surcharge_fee' => __('label.surcharge'),
            'containers.*.products.*.sub_total_insurrance_fee' => __('label.insurance_short'),
            'containers.*.products.*.total' => __('label.total'),
            'containers.*.products.*.user_note' => __('label.note')
        ];
    }
}
