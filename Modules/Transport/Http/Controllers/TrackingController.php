<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use App\Exports\ExportQuery;
use Exception;
use App\Address;

class TrackingController extends Controller
{

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '4.9'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        if (!empty($request->query('code'))) {
            $query->where('code', 'like', '%' . $request->query('code') . '%');
        } else {
            $query->where('order_status', '!=', OrderStatusEnum::STATUS_COMPLETE);
            $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
            if (!empty($request->query('from_date'))) {
                $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
            }
            if (!empty($request->query('to_date'))) {
                $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
            }

            if (!empty($request->query('receiver_phone'))) {
                $query->where('receiver_phone', '=', $request->query('receiver_phone'));
            }
            if (!empty($request->query('shipping_status'))) {
                $query->where('shipping_status', '=', $request->query('shipping_status'));
            }
        }
        $list_status = array(
            null => __('order.status_other'),
            OrderShippingStatusEnum::SHIPPING_STATUS_NEW => __('order.shipping_status_new'),
            OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN => __('order.shipping_status_stockin'),
            OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT => __('order.shipping_status_shipment'),
            OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_OUT => __('order.shipping_status_stockout'),
            OrderShippingStatusEnum::SHIPPING_STATUS_REVICER => __('order.shipping_status_local'),
            OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP => __('order.shipping_status_ready')
        );
        $this->data['list_status'] = $list_status;
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::tracking.index', $this->data);
    }

    public function viewHistory(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $data['tracking'] = [];
        $orderTrackings = OrderTracking::where([
            ['order_id', '=', $request->get('id')]
        ])->orderBy('id', 'asc')->get();
        foreach ($orderTrackings as $orderTracking) {
            $nTracking = [];
            $nTracking['order_code'] = $orderTracking->tracking_code;
            $nTracking['created_at'] = date('d/m/Y', strtotime($orderTracking->created_at));
            $nTracking['status'] = $orderTracking->status;
            $nTracking['user'] = !empty($orderTracking->user->code) ? $orderTracking->user->code : 'System';
            if (empty($orderTracking->note)) {
                $response_api = json_decode($orderTracking->response_api);
                $nTracking['response_api'] = isset($response_api->sGhiChu) ? $response_api->sGhiChu : '';
            } else {
                $nTracking['response_api'] = $orderTracking->note;
            }
            $data['tracking'][] = $nTracking;
        }
        $data['status'] = $this->getOrderTrackingStatus();
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => $data]);
    }
    public function exportExcel(Request $request)
    {
        $query = Order::where([]);
        $query->select(
            'tbl_order.code',
            'tbl_order.created_at',
            DB::raw("CONCAT(sender_first_name,sender_middle_name,sender_last_name) as sender_full_name"),
            'customer_note',
            'user_note',
            'description',
            'sender_email',
            'sender_phone',
            'sender_cellphone',
            'sender_address',
            'sender_post_code',
            'nuoc_gui.name as sender_countrie',
            'tinh_gui.name as sender_province',
            'thanh_pho_gui.name as sender_city',
            'receiver_email',
            DB::raw("CONCAT(receive_first_name,receiver_middle_name,receive_last_name) as receive_full_name"),
            'receiver_cellphone',
            'receiver_phone',
            'receiver_address',
            'receiver_post_code',
            'nuoc_nhan.name as receiver_countrie',
            'tinh_nhan.name as receiver_province',
            'thanh_pho_nhan.name as receiver_city',
            'is_crr',
            'total_weight',
            'total_final',
            'total_paid_amount',
            'total_surcharge_fee',
            'total_insurrance_fee',
            'total_remain_amount',
            'total_declare_price',
            'agency_discount',
            'order_status',
            'shipping_status',
            'payment_status',
            'receive_status',
            'sender_first_name',
            'sender_middle_name',
            'sender_last_name',
            'receive_first_name',
            'receiver_middle_name',
            'receive_last_name'
        );
        // lấy thông tin nước gửiprovinces
        $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id');
        // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id');
        // lấy thông tin tỉnh gửi
        $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id');
        // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id');
        // lấy thông tin thành phố gửi
        $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id');
        // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id');
        $user = Auth::user();
        if (strtoupper($user->role->admin) == 1) {
            $query->whereNotNull('tbl_order.agency_id');
        } else {
            $query->where('tbl_order.user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_COMPLETE);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        $query->where('tbl_order.type', '=', OrderTypeEnum::TRANSPORT);
        $query->orderBy('tbl_order.id', 'desc');
        return (new ExportQuery($query))->download('tracking_transport.xlsx');
    }
    public function viewLog(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $data['log'] = [];
        $orderLogs = OrderLog::where([
            ['order_id', '=', $request->get('id')]
        ])->orderBy('id', 'asc')->get();
        foreach ($orderLogs as $orderLog) {
            $nLog = [];
            $nLog['created_at'] = date('d/m/Y', strtotime($orderLog->created_at));
            $nLog['status'] = $orderLog->status;
            $nLog['user'] = !empty($orderLog->user->code) ? $orderLog->user->code : 'ADM';
            $data['log'][] = $nLog;
        }
        $data['status'] = $this->getOrderLogStatus();
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => $data]);
    }

    public function cancelOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $order = Order::find($request->get('id'));
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        if ($order->order_status == OrderStatusEnum::STATUS_CANCEL || $order->shipping_status != OrderShippingStatusEnum::SHIPPING_STATUS_NEW) {
            return response(["success" => false, "message" => __('message.order_no_allow_cancel'), "data" => []]);
        }
        if ($order->voucher_id != NULL) {
            return response(["success" => false, "message" => __('message.order_exist_voucher'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $order->order_status = OrderStatusEnum::STATUS_CANCEL;
            $order->update();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_CANCEL;

            $orderLog->save();

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_order_success'), "data" => ['refUrl' => route('transport.index')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    public function restoreOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $order = Order::find($request->get('id'));
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        if ($order->order_status == OrderStatusEnum::STATUS_NEW) {
            return response(["success" => false, "message" => __('message.order_no_allow_restore'), "data" => []]);
        }
        if (today()->diff($order->created_at)->days > 7) {
            return response(["success" => false, "message" => __('message.order_no_allow_restore'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $order->order_status = OrderStatusEnum::STATUS_NEW;
            $order->update();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_NEW;

            $orderLog->save();

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_order_success'), "data" => ['refUrl' => route('transport.index')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    public function reshipOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $order = Order::find($request->get('id'));
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        if ($order->order_status == OrderStatusEnum::STATUS_CANCEL || $order->shipping_status != OrderShippingStatusEnum::SHIPPING_STATUS_NEW) {
            return response(["success" => false, "message" => __('message.order_no_allow_cancel'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
            $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
            $order->update();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_REVICER;

            $orderLog->save();

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_order_success'), "data" => ['refUrl' => route('transport.carrier')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    private function getOrderTrackingStatus()
    {
        $data[OrderTrackingStatusEnum::STATUS_NEW] = __('order.shipping_status_new');
        $data[OrderTrackingStatusEnum::STATUS_STOCK_IN] = __('order.shipping_status_stockin');
        $data[OrderTrackingStatusEnum::STATUS_SHIPMENT] = __('order.shipping_status_shipment');
        $data[OrderTrackingStatusEnum::STATUS_STOCK_OUT] = __('order.shipping_status_stockout');
        $data[OrderTrackingStatusEnum::STATUS_CUSTOMS] = __('order.goods_in_the_customs'); //5
        $data[OrderTrackingStatusEnum::STATUS_REVICER] = __('order.shipping_status_local');
        $data[OrderTrackingStatusEnum::STATUS_READY_TO_SHIP] = __('order.shipping_status_ready');
        $data[OrderTrackingStatusEnum::STATUS_DELAY] = __('order.shipping_status_delay'); //8
        $data[OrderTrackingStatusEnum::STATUS_CARRIER] = __('order.shipping_status_carrier');
        $data[OrderTrackingStatusEnum::STATUS_DONE] = __('order.shipping_status_done');
        return $data;
    }

    private function getOrderLogStatus()
    {
        $data[OrderLogStatusEnum::STATUS_CANCEL] = __('order.status_cancel');
        $data[OrderLogStatusEnum::STATUS_NEW] = __('order.shipping_status_new');
        $data[OrderLogStatusEnum::STATUS_STOCK_IN] =  __('order.shipping_status_stockin');
        $data[OrderLogStatusEnum::STATUS_SHIPMENT] = __('order.shipping_status_shipment');
        $data[OrderLogStatusEnum::STATUS_STOCK_OUT] = __('order.shipping_status_stockout');
        $data[OrderLogStatusEnum::STATUS_IN_THE_CUSTOMS] = __('order.goods_in_the_customs');
        $data[OrderLogStatusEnum::STATUS_REVICER] = __('order.shipping_status_local');
        $data[OrderLogStatusEnum::STATUS_READY_TO_SHIP] = __('order.shipping_status_ready');
        $data[OrderLogStatusEnum::STATUS_CARRIER] =  __('order.shipping_status_carrier');
        $data[OrderLogStatusEnum::STATUS_DONE] = __('order.shipping_status_done');

        $data[OrderLogStatusEnum::STATUS_OTHER] = __('order.status_other');
        $data[OrderLogStatusEnum::STATUS_RETURN] = __('order.status_return');
        return $data;
    }
    private function getOrderShippingStatus()
    {
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_NEW] = __('order.shipping_status_new'); //1
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN] = __('order.shipping_status_stockin'); //2
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT] = __('order.shipping_status_shipment'); //3
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_OUT] = __('order.shipping_status_stockout'); //4
        $data[OrderShippingStatusEnum::SHIPPING_IN_THE_CUSTOMS] = __('order.goods_in_the_customs'); //5
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_REVICER] = __('order.shipping_status_local'); //6
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_READY_TO_SHIP] = __('order.shipping_status_ready'); //7
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_DELAY] = substr(__('order.shipping_status_delay'), 0, 50); //8
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER] = __('order.shipping_status_carrier'); //9
        $data[OrderShippingStatusEnum::SHIPPING_STATUS_DONE] = __('order.shipping_status_done'); //10
        return $data;
    }
    public function findtracking(Request $request)
    {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        if (!empty($request->get('code'))) {
            $query->where('code', 'like', '%' . $request->get('code') . '%');
            $order = $query->first();
            return  $this->showphone($order->id);
        }
    }
    public function showphone($id)
    {
        $user = Auth::user();
        if (strtoupper($user->role->admin) == 1) {
            $order = Order::where('id', '=', $id)->first();
            $address_id = $order->shipping_address_id;
            $address = Address::where('id', '=', $address_id)->first();
        }
        $list_shiping_status = $this->getOrderShippingStatus();
        $list_order_status = array(
            OrderStatusEnum::STATUS_NEW => __('order.status_new'),
            OrderStatusEnum::STATUS_PROCESSING => __('order.status_processing'),
            OrderStatusEnum::STATUS_COMPLETE => __('order.status_complete'),
            OrderStatusEnum::STATUS_CANCEL => __('order.status_cancel')
        );
        $orderLogs = OrderLog::where([
            ['order_id', '=', $id]
        ])->orderBy('created_at', 'asc')->get();
        $orderTrackings = OrderTracking::where([
            ['order_id', '=', $order->id]
        ])->orderBy('id', 'asc')->get();
        $this->data['order_trackings'] = $orderTrackings;
        $this->data['list_shingping_status'] = $list_shiping_status;
        $this->data['list_order_status'] = $list_order_status;
        $this->data['ordersLog'] = $orderLogs;
        $this->data['status_log'] = $this->getOrderLogStatus();
        $this->data['order'] = $order;
        $this->data['address'] = $address;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::tracking.updatephone', $this->data);
    }
    public function updatestatus(Request $request)
    {
        $user = Auth::user();
        if (strtoupper($user->role->admin) == 1) {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $order_id = $data['order_id'];
                $order = Order::where('id', '=', $order_id)->first();
                $order->shipping_status = $data['shipping_status'];
                $order->order_status = $data['order_status'];
                $order->update();
                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = $data['shipping_status'];
                $orderLog->save();
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                return response('Internal Server Error', 500);
            }
            return redirect('/admin/transport/tracking/' . $order_id . '/updatephone')->with('complete_status', '1');
        }
    }
    public function updatephone(Request $request)
    {
        $user = Auth::user();
        if ($user->role->admin) {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $order_id = $data['order_id'];
                $address_id = $data['address_id'];
                $address = Address::where('id', '=', $address_id)->first();
                $order = Order::where('id', '=', $order_id)->first();

                $order->receive_first_name = $data['address_first_name'];
                $order->receiver_middle_name = $data['address_middle_name'];
                $order->receive_last_name = $data['address_last_name'];

                $order->receiver_phone = $data['address_telephone'];
                $order->receiver_cellphone = $data['address_cellphone'];
                $order->update();
                $address = Address::where('id', '=', $address_id)->first();
                if (!empty($address)) {
                    $address->first_name = $data['address_first_name'];
                    $address->middle_name = $data['address_middle_name'];
                    $address->last_name = $data['address_last_name'];

                    $address->telephone = $data['address_telephone'];
                    $address->cellphone = $data['address_cellphone'];
                    $address->update();
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                return response('Internal Server Error', 500);
            }
            return redirect('/admin/transport/tracking/' . $order_id . '/updatephone')->with('complete', '1');
        }
    }
    public function updatecreateat(Request $request)
    {
        $user = Auth::user();
        if ($user->role->admin) {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $order_id = $data['order_id'];
                $order = Order::where('id', '=', $order_id)->first();
                $date_tmp = date("H:i:s");
                $old_create_date= $order->created_at;
                $date_create = ConvertsUtil::dateFormat($data['created_at'] . "" .$date_tmp);
                $order->created_at = $date_create;
                $order->update();
                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = $order->shipping_status;
                $orderLog->description =  $old_create_date ." to ". $order->created_at ;
                $orderLog->save();
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                return response('Internal Server Error', 500);
            }
            return redirect('/admin/transport/tracking/' . $order_id . '/updatephone')->with('complete_create_at', '1');
        }
    }
    public function delete(Request $request)
    {
        $user = Auth::user();
        if ($user->role->admin) {
            try {
                $id = $request->id;
                DB::beginTransaction();
                $orderTrackings = OrderTracking::find($id);
                $order_id = $orderTrackings->order_id;
                $orderTrackings->delete();
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                return response('Internal Server Error', 500);
            }
            return redirect('/admin/transport/tracking/' . $order_id . '/updatephone')->with('tracking_status', '1');
        }
    }
}
