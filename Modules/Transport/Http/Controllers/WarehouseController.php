<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Exports\ExportQuery;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Enum\WarehouseEnum;
use App\Enum\WarehousePackageEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use App\Warehouse;
use App\WarehousePackage;
use Exception;
class WarehouseController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '4.2'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $user = Auth::user();

        $columns = [
            'tbl_warehouse_package.id',
            'tbl_warehouse_package.code',
            'tbl_warehouse_package.created_at',
            'tbl_warehouse_package.stockin_at',
            'tbl_warehouse_package.status',
            'users.first_name',
            'users.middle_name',
            'users.last_name'
        ];
        $query = WarehousePackage::join('users', 'tbl_warehouse_package.create_user_id', '=', 'users.id');
        if(!$user->role->admin){
            $query->where('tbl_warehouse_package.create_user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_warehouse_package.stockin_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_warehouse_package.stockin_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('status'))) {
            $query->where('tbl_warehouse_package.status', '=', $request->query('status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_warehouse_package.code', 'like', '%'.$request->query('code').'%');
        }
        $query->where('tbl_warehouse_package.type', '=', OrderTypeEnum::TRANSPORT);
        $this->data['warehousePackage'] = $query->orderBy('tbl_warehouse_package.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::warehouse.index', $this->data);
    }

    public function exportExcel(Request $request) {
        $user = Auth::user();
        $columns = [
            'tbl_warehouse_package.id',
            'tbl_warehouse_package.code',
            'tbl_warehouse_package.created_at',
            'tbl_warehouse_package.stockin_at',
            'tbl_warehouse_package.status',
            'users.username as user_create'
        ];
        $query = WarehousePackage::join('users', 'tbl_warehouse_package.create_user_id', '=', 'users.id');
        if(!$user->role->admin){
            $query->where('tbl_warehouse_package.create_user_id', '=', $user->id);
        }
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_warehouse_package.stockin_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_warehouse_package.stockin_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('status'))) {
            $query->where('tbl_warehouse_package.status', '=', $request->query('status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_warehouse_package.code', '=', $request->query('code'));
        }
         $query->orderBy('tbl_warehouse_package.id', 'desc')->select($columns);
        return (new ExportQuery($query))->download('warehouse.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $user = Auth::user();
        if($user->role->admin){
            $this->data['warehouses'] = Warehouse::where(['is_deleted' => 0,'stockin' => WarehouseEnum::IS_DEFAULT_TRUE])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::warehouse.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function view($id) {
        $this->data['warehousePackage'] = WarehousePackage::find($id);
        if(empty($this->data['warehousePackage']) || $this->data['warehousePackage']->status == WarehousePackageEnum::STATUS_CANCEL){
            return redirect()->route('transport.warehouse.index');
        }
        $user = Auth::user();
        if($user->role->admin){
            $this->data['warehouses'] = Warehouse::where(['is_deleted' => 0])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        $this->data['orders'] = Order::where([['warehouse_packge_id', '=', $id]])->orderBy('id', 'desc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::warehouse.view', $this->data);
    }

    public function exportExcelOrder($id) {
        $columns = [
            'code',
            'description',
            'products',
            'sender_first_name',
            'sender_middle_name',
            'sender_last_name',
            'sender_phone',
            'sender_country_name',
            'sender_province_name',
            'sender_city_name',
            'sender_address',
            'user_note',
            'receive_first_name',
            'receiver_middle_name',
            'receive_last_name',
            'receiver_country_name',
            'receiver_province_name',
            'receiver_city_name'
        ];
        $query = Order::where([]);
        $query->where('warehouse_packge_id', '=', $id);

        $query->orderBy('id', 'desc')->select($columns);
        return (new ExportQuery($query))->download('transport_whp_order.xlsx');
    }

    public function addItem(Request $request) {
        $condition = [];
        $condition[] = ['type', '=', OrderTypeEnum::TRANSPORT];
        $condition[] = ['order_status', '!=', OrderStatusEnum::STATUS_CANCEL];
        if(!empty($request->input('code'))){
            $condition[] = ['code', '=', $request->input('code')];
            $order = Order::where($condition)->whereNull('warehouse_packge_id')->get();
        }
        else
        {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist_or_stockin'), "data" => []]);
        }

        return response(["success" => true, "message" => '', "data" => $order]);
    }

    public function addPackage(Request $request) {
        $validator = Validator::make($request->input(), [
            'from_warehouse_id' => 'required',
            'stockin_at' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $items = $request->input('items');
        if (empty($items)) {
            return response(["success" => false, "message" => __('message.err_package_empty'), "data" => []]);
        }
        $orderIds = [];
        foreach ($items as $item) {
            $orderIds[] = $item['value'];
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $warehouseData = Warehouse::find($request->input('from_warehouse_id'));
        $from_warehouse = isset($warehouseData->id) && !empty($warehouseData->id) ? $warehouseData->id : 0;
        if (empty($warehouseData)) {
            return response(["success" => false, "message" => __('message.err_whp_not_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $warehousePackage = new WarehousePackage();
            $warehousePackage->create_user_id = $user->id;
            $warehousePackage->from_warehouse_id = $warehouseData->id;
            $warehousePackage->type = OrderTypeEnum::TRANSPORT;
            $warehousePackage->stockin_at = ConvertsUtil::dateFormat($request->input('stockin_at') . " 00:00:00");
            $count_transport= WarehousePackage::where('type',OrderTypeEnum::TRANSPORT) ->count()+1;
            $warehousePackage->code = $warehouseData->code."_".$count_transport;
            $warehousePackage->save();
             DB::commit();
             } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
            }
            foreach ($orderData as $order) {
                 try {
                DB::beginTransaction();
                $order->warehouse_packge_id = $warehousePackage->id;
                $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN;
                $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
                $order->from_warehouse_id = $from_warehouse;
                $order->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $order->code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_STOCK_IN;
                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_STOCK_IN;
                $orderLog->save();
                DB::commit();
                } catch (Exception $e) {
                     DB::rollBack();
                    return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
                                        }
            }
            return response(["success" => true, "message" => __('message.add_whp_success'), "data" => []]);
    }

    public function addItemExist(Request $request) {
        $validator = Validator::make($request->Input(), [
                    'code' => 'required',
                    'warehouse_packge_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $warehouseData = WarehousePackage::find($request->Input('warehouse_packge_id'));
        $from_warehouse = isset($warehouseData->from_warehouse_id) && !empty($warehouseData->from_warehouse_id) ? $warehouseData->from_warehouse_id : 0;
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $order = Order::where([
                    ['code', '=', $request->Input('code')],
                    ['type', '=', OrderTypeEnum::TRANSPORT],
                    ['order_status', '!=', OrderStatusEnum::STATUS_CANCEL]
                ])->whereNull('warehouse_packge_id')->get()->first();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist_or_stockin'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $order->warehouse_packge_id = $request->Input('warehouse_packge_id');
            $order->from_warehouse_id = $from_warehouse;
            $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN;
            $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
            $order->update();

            $orderTracking = new OrderTracking();
            $orderTracking->order_id = $order->id;
            $orderTracking->create_user_id = $user->id;
            $orderTracking->tracking_code = $order->code;
            $orderTracking->order_code = $order->code;
            $orderTracking->status = OrderTrackingStatusEnum::STATUS_STOCK_IN;

            $orderTracking->save();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_STOCK_IN;

            $orderLog->save();

            DB::commit();

            $data = $order->getAttributes();
            $data['shipping_status_name'] = $order->shipping_status_name;
            $data['shipping_status_label'] = $order->shipping_status_label;
            $data['receive_status_label'] = $order->receive_status_label;
            $data['receive_status_name'] = $order->receive_status_name;
            return response(["success" => true, "message" => __('message.add_order_to_whp'), "data" => $data]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function removeItemExist(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'warehouse_packge_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $warehousePackage = WarehousePackage::find($request->get('warehouse_packge_id'));
        if (empty($warehousePackage)) {
            return response(["success" => false, "message" => __('message.err_whp_code_not_exist'), "data" => []]);
        }
        $order = Order::where([
                    ['id', '=', $request->get('id')],
                    ['warehouse_packge_id', '=', $request->get('warehouse_packge_id')]
                ])->whereNull('shipment_id')->get()->first();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist_or_shipment'), "data" => []]);
        }
        $count = Order::where([
                    ['warehouse_packge_id', '=', $request->get('warehouse_packge_id')]
                ])->get()->count();
        try {
            DB::beginTransaction();

            $order->warehouse_packge_id = null;
            $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_NEW;
            $order->update();
            if ($count - 1 <= 0) {
                $warehousePackage->status = WarehousePackageEnum::STATUS_CANCEL;
                $warehousePackage->update();
            }

            OrderTracking::where([
                ['order_id', '=', $order->id],
                ['status', '=', OrderTrackingStatusEnum::STATUS_STOCK_IN]
            ])->delete();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_NEW;

            $orderLog->save();

            DB::commit();

            return response(["success" => true, "message" => __('message.del_order_to_whp'), "data" => $warehousePackage]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function editPackage(Request $request) {
        $validator = Validator::make($request->Input(), [
            'from_warehouse_id' => 'required',
            'stockin_at' => 'required',
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $warehousePackage = WarehousePackage::find($request->Input('id'));
        if (empty($warehousePackage)) {
            return response(["success" => false, "message" => __('message.err_whp_code_not_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $warehousePackage->from_warehouse_id = $request->Input('from_warehouse_id');
            $warehousePackage->stockin_at = ConvertsUtil::dateFormat($request->Input('stockin_at') . " 00:00:00");
            $warehousePackage->save();

            DB::commit();

            return response(["success" => true, "message" => __('message.update_whp_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
