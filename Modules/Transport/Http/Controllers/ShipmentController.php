<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Exports\ExportQuery;
// use Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Enum\WarehouseEnum;
use App\Enum\ShipmentEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Enum\OrderStatusEnum;
use App\Order;
use App\Warehouse;
use App\Shipment;
use App\OrderTracking;
use App\OrderLog;
use Exception;
class ShipmentController extends Controller {

    /**
     * Assign data
     */
    private $data;

    public function __construct() {
        $this->data = [
            'menu' => '4.3'
        ];
    }

    public function index(Request $request) {
        $columns = [
            'tbl_shipment.id',
            'tbl_shipment.code',
            'tbl_shipment.name',
            'tbl_shipment.created_at',
            'tbl_shipment.estimate_delivery',
            'tbl_shipment.status',
            'users.first_name',
            'users.middle_name',
            'users.last_name'
        ];
        $query = Shipment::join('users', 'tbl_shipment.create_user_id', '=', 'users.id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_shipment.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_shipment.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('status'))) {
            $query->where('tbl_shipment.status', '=', $request->query('status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_shipment.code', 'like', '%'.$request->query('code').'%');
        }
        $this->data['shipments'] = $query->orderBy('tbl_shipment.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::shipment.index', $this->data);
    }

    public function exportExcel(Request $request) {
        $columns = [
            'tbl_shipment.id',
            'tbl_shipment.code',
            'tbl_shipment.created_at',
            'tbl_shipment.estimate_delivery',
            'tbl_shipment.status',
            'users.username as user_create'
        ];
        $query = Shipment::join('users', 'tbl_shipment.create_user_id', '=', 'users.id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_shipment.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_shipment.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('status'))) {
            $query->where('tbl_shipment.status', '=', $request->query('status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_shipment.code', '=', $request->query('code'));
        }
        $query->orderBy('tbl_shipment.id', 'desc')->select($columns);
        return (new ExportQuery($query))->download('transport_shipment.xlsx');
    }

    public function create(Request $request) {
         $user = Auth::user();
         $query = Order::where([]);
         $this->data['warehouses_id'] = 0;
        if(strtoupper($user->role->name) == "ADMIN"){
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'is_deleted' => 0])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        //kiem tra hang da nhap kho chua
         $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
       $query->where('shipping_status', '=', OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN);
      // $query->leftJoin('tbl_shipping_package_local', 'tbl_order.warehouse_packge_id', '=', 'tbl_shipping_package_local.id');
       $query->whereNotNull('warehouse_packge_id');
         if (!empty($request->query('from_warehouse_id'))) {
          //  $query->where('from_warehouse_id', '=', $request->query('from_warehouse_id'));
             $query->where('from_warehouse_id', '=', $request->query('from_warehouse_id'));
             $this->data['warehouses_id'] = $request->query('from_warehouse_id');
        }
        else
        {
             $query->whereNotNull('from_warehouse_id');
        }
        $this->data['language'] = LanguageUtil::getKeyJavascript();

         $this->data['orders'] = $query->orderBy('tbl_order.id', 'desc')->get();
        return view('transport::shipment.create', $this->data);
    }

    public function view($id) {
        $this->data['shipment'] = Shipment::find($id);
        if(empty($this->data['shipment']) || $this->data['shipment']->status == ShipmentEnum::STATUS_CANCEL){
            return redirect()->route('transport.shipment.index');
        }
        $user = Auth::user();
        if(strtoupper($user->role->name) == "ADMIN"){
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'is_deleted' => 0])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }
        $this->data['orders'] = Order::where([['shipment_id', '=', $id]])->orderBy('id', 'desc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::shipment.view', $this->data);
    }

    public function exportExcelOrder($id) {
        $query = Order::where([]);
        $query->where('shipment_id', '=', $id);

     $query->orderBy('id', 'desc')->select()->get()->toArray();
        return (new ExportQuery($query))->download('transport_shipment_order.xlsx');
    }

    public function addItem( Request $request) {
        $condition = [];
        $condition[] = ['type', '=', OrderTypeEnum::TRANSPORT];
        if(!empty($request->input('code'))){
            $condition[] = ['code', '=', $request->input('code')];
        }
        $order = Order::where($condition)->whereNull('shipment_id')
                ->whereNotNull('warehouse_packge_id')->get();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist_or_shipment'), "data" => []]);
        }
        return response(["success" => true, "message" => '', "data" => $order]);
    }
// hàm thêm mới 1 shipment
     public function addPackage(Request $request ) {
        $validator = Validator::make($request->input(), [
            'from_warehouse_id' => 'required',
            'estimate_delivery' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $shipment_name=$request->input('shipment_name');
        $items = $request->input('items');
        if (empty($items)) {
            return response(["success" => false, "message" => __('message.err_shipment_empty'), "data" => []]);
        }
        $orderIds = [];
        foreach ($items as $item) {
            $orderIds[] = $item['value'];
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.err_order_no_exist'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $warehouseData = Warehouse::find($request->input('from_warehouse_id'));
        if (empty($warehouseData)) {
            return response(["success" => false, "message" =>  __('message.err_whp_not_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $shipment = new Shipment();
            $shipment->create_user_id = $user->id;
            $shipment->name = $shipment_name;
            $shipment->from_warehouse_id = $warehouseData->id;
            $shipment->estimate_delivery = ConvertsUtil::dateFormat($request->input('estimate_delivery') . " 00:00:00");
            $shipment->save();
            $shipment->code = ConvertsUtil::getSPackageCode($warehouseData->code, $shipment->id);
            if(empty($shipment_name))
            {
                $shipment->name = $shipment->code;
            }
            $shipment->update();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
            foreach ($orderData as $order) {
                try {
                DB::beginTransaction();
                $order->shipment_id = $shipment->id;
                $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT;
                $order->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $order->code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_SHIPMENT;
                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_SHIPMENT;
                $orderLog->save();
                DB::commit();
                } catch (Exception $e) {
                 DB::rollBack();
                 return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
                }
            }
            return response(["success" => true, "message" => __('message.add_shipment_success'), "data" => []]);
    }

    public function addItemExist(Request $request) {
        $validator = Validator::make($request->input(), [
                    'code' => 'required',
                    'shipment_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shipment = Shipment::find($request->input('shipment_id'));
        if (empty($shipment)) {
            return response(["success" => false, "message" => __('message.err_shipment_code_not_exist'), "data" => []]);
        }
        $order = Order::where([
                    ['code', '=', $request->input('code')],
                    ['type', '=', OrderTypeEnum::TRANSPORT]
                ])->whereNull('shipment_id')
                ->whereNotNull('warehouse_packge_id')->get()->first();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $order->shipment_id = $request->input('shipment_id');
            $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_SHIPMENT;
            $order->update();

            $orderTracking = new OrderTracking();
            $orderTracking->order_id = $order->id;
            $orderTracking->create_user_id = $user->id;
            $orderTracking->tracking_code = $order->code;
            $orderTracking->order_code = $order->code;
            $orderTracking->status = OrderTrackingStatusEnum::STATUS_SHIPMENT;

            $orderTracking->save();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_SHIPMENT;

            $orderLog->save();

            DB::commit();

            $data = $order->getAttributes();
            $data['shipping_status_name'] = $order->shipping_status_name;
            $data['shipping_status_label'] = $order->shipping_status_label;

            return response(["success" => true, "message" => __('message.add_order_to_shipment'), "data" => $data]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function removeItemExist(Request $request) {
        $validator = Validator::make($request->input(), [
                    'id' => 'required',
                    'shipment_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shipment = Shipment::find($request->input('shipment_id'));
        if (empty($shipment)) {
            return response(["success" => false, "message" => __('message.err_shipment_code_not_exist'), "data" => []]);
        }
        $order = Order::where([
                    ['id', '=', $request->input('id')],
                    ['shipment_id', '=', $request->input('shipment_id')]
                ])->get()->first();
        if (empty($order)) {
            return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
        }
        $count = Order::where([
                    ['shipment_id', '=', $request->input('shipment_id')]
                ])->get()->count();
        try {
            DB::beginTransaction();

            $order->shipment_id = null;
            $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN;
            $order->update();
            if ($count - 1 <= 0) {
                $shipment->status = ShipmentEnum::STATUS_CANCEL;
                $shipment->update();
            }

            OrderTracking::where([
                ['order_id', '=', $order->id],
                ['status', '=', OrderTrackingStatusEnum::STATUS_STOCK_IN]
            ])->delete();

            $orderLog = new OrderLog();
            $orderLog->order_id = $order->id;
            $orderLog->create_user_id = $user->id;
            $orderLog->status = OrderLogStatusEnum::STATUS_STOCK_IN;

            $orderLog->save();

            DB::commit();

            return response(["success" => true, "message" => __('message.del_order_to_shipment'), "data" => $shipment]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function editPackage(Request $request) {
        $validator = Validator::make($request->input(), [
            'from_warehouse_id' => 'required',
            'estimate_delivery' => 'required',
            'id' => 'required',
            'shipment_name' => 'nullable'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $shipment_name = $request->input('shipment_name');
        $shipment = Shipment::find($request->input('id'));
        if (empty($shipment)) {
            return response(["success" => false, "message" => __('message.err_shipment_code_not_exist'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            $shipment->from_warehouse_id = $request->input('from_warehouse_id');
            $shipment->name = $shipment_name;
            $shipment->estimate_delivery = ConvertsUtil::dateFormat($request->input('estimate_delivery') . " 00:00:00");
            $shipment->update();

            DB::commit();

            return response(["success" => true, "message" => __('message.update_shipment_success'), "data" => []]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
