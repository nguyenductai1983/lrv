<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Components\VNPostClient;
use App\Components\LanguageUtil;
use App\Enum\ShippingPackageLocalEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\ShippingMethodProviderEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\OrderTypeEnum;
use App\ShippingPackageLocal;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use App\Services\giaohangtietkiem;
use Exception;
use App\Components\ConvertsUtil;
class CompleteController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '4.8'
        ];
    }
    public function approved(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::APPROVED);
        $query->where('order_type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::complete.approved', $this->data);
    }


    public function cancel(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::CANCEL);
        $query->where('order_type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }

        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::complete.cancel', $this->data);
    }

    public function carrierPickup(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::DELIVERY_CARRIER);
        $query->where('order_type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }
        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::complete.carrierpickup', $this->data);
    }
    public function completevnca(Request $request) {

        $user = Auth::user();

        $query = Order::where([['sender_country_id', '=', 91]]);
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_CANCEL);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_COMPLETE);
        if (!empty($request->query('order_code'))) {
            $query->where('code', '=', $request->query('order_code'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::complete.completevnca', $this->data);
    }
    public function postcompletevnca(Request $request) {
        $Orderlist = $request->get('items');
        if (empty($Orderlist)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($Orderlist as $order) {

                $orderData = Order::find($order->id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
                $orderData->update();
                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $order->code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_CARRIER;
                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_CARRIER;
                $orderLog->save();
            }
            DB::commit();
            return response(["success" => true, "message" => __('message.carrier_pickup_success'), "data" => ['refUrl' => route('transport.complete.completevnca')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }
    public function done(Request $request) {
        $query = ShippingPackageLocal::where([]);
        $query->where('status', '=', ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS);
        $query->where('order_type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('is_channel_off', '=', ShippingPackageLocalEnum::CHANNEL_OFFLINE);
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 00:00:00"));
        }
        if (!empty($request->query('order_code'))) {
            $query->where('order_code', '=', $request->query('order_code'));
        }

        $this->data['shipping_package_locals'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::complete.done', $this->data);
    }

    public function cancelRequest(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }

        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                if (!empty($shippingPackageLocal->tracking_code) && ($shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_FAST || $shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::VNPOST_SLOW)) {
                    $response = VNPostClient::cancelOrder($shippingPackageLocal->shipping_code);
                    if(empty($response->CancelOrderResult)){
                        DB::rollBack();
                        return response(["success" => false, "message" => $response->sE, "data" => []]);
                    }
                }
                else if(!empty($shippingPackageLocal->tracking_code) && ($shippingPackageLocal->shipping_method_provider_id == ShippingMethodProviderEnum::GHTK ) )
                {
                    $response = giaohangtietkiem::cancelOrder($shippingPackageLocal->tracking_code);
                }
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CANCEL;
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_REVICER;
                $orderData->update();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_REVICER;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.cancel_req_delivery_success'), "data" => ['refUrl' => route('transport.complete.cancel')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function pickup(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                $shippingPackageLocal->status = ShippingPackageLocalEnum::DELIVERY_CARRIER;
                $shippingPackageLocal->sent_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_CARRIER;
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
                $orderTracking->order_code = $shippingPackageLocal->order_code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_CARRIER;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_CARRIER;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_pickup_success'), "data" => ['refUrl' => route('transport.complete.carrierpickup')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

    public function customerReceiver(Request $request) {
        $shippingPackageLocalIds = $request->get('shipping_package_locals');
        if (empty($shippingPackageLocalIds)) {
            return response(["success" => false, "message" => __('message.list_req_delivery_empty'), "data" => []]);
        }
        $shippingPackageLocals = ShippingPackageLocal::find($shippingPackageLocalIds);
        if (empty($shippingPackageLocals)) {
            return response(["success" => false, "message" => __('message.req_delivery_is_not_vaild'), "data" => []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($shippingPackageLocals as $shippingPackageLocal) {
                $shippingPackageLocal->status = ShippingPackageLocalEnum::CARRIER_DELIVERY_SUCCESS;
                $shippingPackageLocal->customer_received_at = date('Y-m-d H:i:s');
                $shippingPackageLocal->update();

                $orderData = Order::find($shippingPackageLocal->order_id);
                if (empty($orderData)) {
                    DB::rollBack();
                    return response(["success" => false, "message" => __('message.err_order_is_not_valid'), "data" => []]);
                }
                $orderData->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_DONE;
                $orderData->order_status = OrderStatusEnum::STATUS_COMPLETE;
                $orderData->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $orderData->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $shippingPackageLocal->tracking_code;
                $orderTracking->order_code = $shippingPackageLocal->order_code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_DONE;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $orderData->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_DONE;

                $orderLog->save();
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.carrier_delivery_success'), "data" => ['refUrl' => route('transport.complete.done')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
