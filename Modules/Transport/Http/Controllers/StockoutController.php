<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
// use App\Services\PointService;
use App\Enum\WarehouseEnum;
use App\Enum\ShipmentEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\OrderLogStatusEnum;
// use App\Enum\PointEnum;
use App\Order;
use App\Warehouse;
use App\Shipment;
use App\OrderTracking;
use App\OrderLog;
use Exception;
class StockoutController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '4.4'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $user = Auth::user();
        $columns = [
            'tbl_shipment.id',
            'tbl_shipment.code',
            'tbl_shipment.created_at',
            'tbl_shipment.estimate_delivery',
            'tbl_shipment.status',
            'users.first_name',
            'users.middle_name',
            'users.last_name'
        ];
        $query = Shipment::join('users', 'tbl_shipment.create_user_id', '=', 'users.id');
        $query->where('tbl_shipment.status', '=', ShipmentEnum::STATUS_NORMAL);
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_shipment.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_shipment.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_shipment.code', 'like', '%'.$request->query('code').'%');
        }
        if(!$user->role->admin){
            $query->where('tbl_shipment.from_warehouse_id', '=', $user->warehouse_id);
        }
        $this->data['shipments'] = $query->orderBy('tbl_shipment.id', 'desc')->select($columns)->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::stockout.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function view($id, Request $request) {
        $this->data['shipment'] = Shipment::find($id);
        if (empty($this->data['shipment']) || $this->data['shipment']->status == ShipmentEnum::STATUS_CANCEL) {
            return redirect()->route('transport.stockout.index');
        }
        $user = Auth::user();
        if(strtoupper($user->role->name) == "ADMIN"){
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'is_deleted' => 0])->get();
        }else{
            $this->data['warehouses'] = Warehouse::where(['type' => WarehouseEnum::TYPE_FOREIGN, 'id' => $user->warehouse_id, 'is_deleted' => 0])->get();
        }

        $query = Order::where([['shipment_id', '=', $id]]);
        if (!empty($request->query('order_code'))) {
            $query->where('code', '=', $request->query('order_code'));
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->get();
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::stockout.view', $this->data);
    }

    public function pushItems(Request $request) {
        $validator = Validator::make($request->input(), [
                    'shipment_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $shipment = Shipment::find($request->input('shipment_id'));
        if (empty($shipment)) {
            return response(["success" => false, "message" => __('message.err_shipment_code_not_exist'), "data" => []]);
        }
        $orderIds = $request->input('order_ids');
        if (empty($orderIds)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($orderData as $order) {
                $order->shipping_status = OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_OUT;
                $order->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $order->code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_STOCK_OUT;

                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_STOCK_OUT;

                $orderLog->save();

                // Tặng điểm
                // PointService::addPointOrder($order, PointEnum::TYPE_CONFIG_ORDER);
            }

            DB::commit();

            return response(["success" => true, "message" => __('message.stockout_success'), "data" => ['refUrl' => route('transport.stockout.index')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
