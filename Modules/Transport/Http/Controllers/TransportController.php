<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
//use lluminate\Notifications\Messages\MailMessage;
use App\Enum\CustomerGroupEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Enum\OrderTypeEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\TransactionTypeEnum;
// use App\Enum\PointEnum;
// use App\Enum\ProductEnum;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
// use App\Services\PointService;
use App\Services\CampaignService;
// use App\Services\OrderService;
use App\Config;
use App\User;
use App\Customer;
use App\Address;
use App\Agency;
use App\Order;
use App\OrderItem;
use App\Transport;
use App\Product;
use App\Transaction;
use App\CampaignLog;
use App\Campaign;
use App\Country;
use App\Province;
use App\City;
use App\Ward;
use Modules\Transport\Http\Requests\CreateRequest;
use Modules\Transport\Http\Requests\UpdateRequest;
use App\Exports\ExportQuery;
use App\Page;
use App\Receiver;
// use Maatwebsite\Excel\Concerns\ToArray;

class TransportController extends Controller
{

    /**
     * Assign data
     */
    private $data;

    public function __construct()
    {
        $this->data = [
            'menu' => '4.1'
        ];
    }

    public function index(Request $request)
    {
        $query = Order::where([]);
        $user = Auth::user();
        if (strtoupper($user->role->admin) == 1) {
            $query->whereNotNull('agency_id');
        } else {
            $query->where('user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);

        if (!empty($request->query('shipping_status'))) {
            $query->where('shipping_status', '=', $request->query('shipping_status'));
        }
        //kiểm tra xem có tìm với mã đơn hàng không

        if (!empty($request->query('code'))) {
            $query->where('code', 'like', '%' . $request->query('code') . '%');
        } else
        // nếu không tìm mã đơn hàng lọc với ngày tạo
        {
            if (!empty($request->query('from_date'))) {
                $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
            }
            if (!empty($request->query('to_date'))) {
                $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
            }
            // hàm kiểm tra theo ngày tháng
            if (!empty($request->query('receiver_phone'))) {
                $query->where('receiver_phone', '=', $request->query('receiver_phone'));
            }
        }
        //  $query->leftJoin('tbl_shipment as shipment', 'tbl_order.shipment_id', '=', 'shipment.id' );
        $this->data['orders'] = $query->orderBy('tbl_order.id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        // $request->session()->flash('status', 'Task was successful!');
        // $data = $request->session()->all();
        return view('transport::home.index', $this->data);
    }
    public function detail($id)
    {
        $query = Order::where([]);
        $user = Auth::user();
        if (strtoupper($user->role->admin) == 1) {
            $query->whereNotNull('agency_id');
        } else {
            $query->where('user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('id', '=', $id);
        $order = $query->orderBy('tbl_order.id', 'desc')->first();
        $modelAttributes = $order->attributesToArray();
        $this->data['orders'] = $modelAttributes;
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::home.detail', $this->data);
    }
    // public function exportExcelOK(Request $request)
    // {

    //     return (new TransportExport($request))->download('invoices.xlsx');

    // }
    // public function exportExcel2(Request $request)
    // {
    //     return Excel::download(new ExportView($request), 'invoices.xlsx');
    // }

    public function exportExcel(Request $request)
    {
        $query = Order::where([]);
        $query->select(
            'tbl_order.code',
            'tbl_order.created_at',
            DB::raw("CONCAT(sender_first_name,sender_middle_name,sender_last_name) as sender_full_name"),
            'customer_note',
            'user_note',
            'description',
            'danger_type',
            'shipping_fast',
            'products',
            'sender_country_name',
            'sender_province_name',
            'sender_city_name',
            'sender_address',
            'sender_phone',
            'sender_cellphone',
            'sender_email',
            'sender_post_code',
            // 'nuoc_gui.name as sender_countrie',
            // 'tinh_gui.name as sender_province',
            // 'thanh_pho_gui.name as sender_city',
            'receiver_email',
            DB::raw("CONCAT(receive_first_name,receiver_middle_name,receive_last_name) as receive_full_name"),
            'receiver_country_name',
            'receiver_province_name',
            'receiver_city_name',
            'receiver_ward_name',
            'receiver_address',
            'receiver_cellphone',
            'receiver_phone',
            'receiver_post_code',
            // 'nuoc_nhan.name as receiver_countrie',
            // 'tinh_nhan.name as receiver_province',
            // 'thanh_pho_nhan.name as receiver_city',
            'is_crr',
            'total_weight',
            'total_final',
            'total_discount as VIP_discount',
            'total_paid_amount',
            'total_surcharge_fee',
            'total_insurrance_fee',
            'total_remain_amount',
            'total_declare_price',
            'agency_discount',
            'order_status',
            'shipping_status',
            'payment_status',
            'receive_status',
            'sender_first_name',
            'sender_middle_name',
            'sender_last_name',
            'receive_first_name',
            'receiver_middle_name',
            'receive_last_name'
        );
        // lấy thông tin nước gửiprovinces
        // $query->leftJoin('countries as nuoc_gui', 'tbl_order.sender_country_id', '=', 'nuoc_gui.id');
            // lấy thông tin tỉnh gửi
            // $query->leftJoin('provinces as tinh_gui', 'tbl_order.sender_province_id', '=', 'tinh_gui.id');
            // lấy thông tin thành phố gửi
            // $query->leftJoin('cities as thanh_pho_gui', 'tbl_order.sender_city_id', '=', 'thanh_pho_gui.id');
        // lấy thông tin nước nhận
        // $query->leftJoin('countries as nuoc_nhan', 'tbl_order.receiver_country_id', '=', 'nuoc_nhan.id');
        // lấy thông tin tỉnh nhận
        // $query->leftJoin('provinces as tinh_nhan', 'tbl_order.receiver_province_id', '=', 'tinh_nhan.id');

        // lấy thông tin thành phố nhận
        // $query->leftJoin('cities as thanh_pho_nhan', 'tbl_order.receiver_city_id', '=', 'thanh_pho_nhan.id');
        $user = Auth::user();
        if (strtoupper($user->role->admin) == 1) {
            $query->whereNotNull('tbl_order.agency_id');
        } else {
            $query->where('tbl_order.user_id', '=', $user->id);
        }
        $query->where('tbl_order.type', '=', OrderTypeEnum::TRANSPORT);
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_order.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_order.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('tbl_order.shipping_status', '=', $request->query('tbl_order.shipping_status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_order.code', 'like', '%' . $request->query('code') . '%');
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('tbl_order.sender_phone', '=', $request->query('sender_phone'));
        }
        return (new ExportQuery($query))->download('order_transport'.date('Ymd').'.xlsx');
    }
    public function printBills($id)
    {
        $currOrder = Order::find($id);
        if (empty($currOrder)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $orders = Order::where('transport_id', '=', $currOrder->transport_id)->get();
        $this->data['orders'] = $orders;
        return view('transport::home.prints', $this->data);
    }
    public function print_invoice($id)
    {
        $currOrder = Order::find($id);
        if (empty($currOrder)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $orders = Order::where('transport_id', '=', $currOrder->transport_id)->get();
        $this->data['orders'] = $orders;
        return view('prints.print_invoice100x75', $this->data);
    }

    public function printBill($id)
    {
        $order = Order::find($id);
        if (empty($order)) {
            return redirect()->route('transport.home.index');
        }
        $this->data['order'] = $order;
        return view('transport::home.print', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        // $this->data['caPriority'] = config('app.ca_priority');
        $this->data['vnPriority'] = config('app.vn_priority');
        // $this->data['caVnPriorityFee'] = config('app.ca_priority_vn_priority_fee');
        // $this->data['caVnPriorityDefaultFee'] = config('app.ca_priority_vn_default_fee');
        // $this->data['caVnDefaultPriorityFee'] = config('app.ca_default_vn_priority_fee');
        // $this->data['caVnDefaultFee'] = config('app.ca_default_vn_default_fee');
        $this->data['senderCountryId'] = config('app.sender_country_id');
        $this->data['receiverCountryId'] = config('app.receiver_country_id');
        $this->data['payMethod'] = config('app.pay_method');
        $this->data['discountLevel'] = config('app.discount_level');
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        $this->data['vn_hcm_priority'] = config('app.vn_hcm_priority');
        $minFee = Auth::user()->agency['min_fee'];
        $this->data['minFee'] = $minFee;
        $slug = 66;
        if (!$page = Page::with('translate')->where('id', '=', $slug)->first()) {
            $page = '';
        }
        $this->data['termagree'] = $page;

        return view('transport::home.create', $this->data);
    }


    private static function getMinFeeadmin($Agency, $fromProvinceId, $totalFee, $commission)
    {
        $vnPriority = config('app.vn_priority');
        $vn_hcm_priority = config('app.vn_hcm_priority');
        $vnPriorityFlag = false;
        $agencyfee = Agency::where('id', $Agency)->first(['min_fee']);
        $minFee = $agencyfee['min_fee'];
        if (in_array($fromProvinceId, $vnPriority, true)) {
            $vnPriorityFlag = true;
        }
        if (!$vnPriorityFlag) {
            $minFee = $minFee + $vn_hcm_priority;
        }
        if ($minFee > $totalFee) {
            if ($commission < 5) {
                $commission = 5;
            }
        } else {
            $minFee = $totalFee;
        }
        $result['commission'] = round($commission,2);
        $result['minfee'] = round($minFee,2);
        return $result;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = null;
            $receiver = null;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            if (isset($data['customer']) && !empty($data['customer'])) {
                $customerExist = null;
                if (!empty($data['customer']['email'])) {
                    $customerExist = Customer::where('email', $data['customer']['email'])->first();
                }
                if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                    if (!empty($customerExist) && $customerExist->id != $customer->id) {
                        return response(__('message.cus_email_exist'), 500);
                    }
                    if (empty($customer)) {
                        return response('Internal Server Error', 500);
                    } else {
                        if (isset($data['Updatecustomer']) && !($data['Updatecustomer'])) {
                            $customer->update([
                                'first_name'  => $data['customer']['first_name'],
                                'middle_name'  => $data['customer']['middle_name'],
                                'last_name'  => $data['customer']['last_name'],
                                'address_1'  => $data['customer']['address_1'],
                                'address_2'  => $data['customer']['address_2'],
                                'telephone'  => $data['customer']['telephone'],
                                'cellphone'  => $data['customer']['cellphone'],
                                'postal_code'  => $data['customer']['postal_code'],
                                'city_id'  => $data['customer']['city_id'],
                                'province_id'  => $data['customer']['province_id'],
                                'country_id' => $data['customer']['country_id'],
                                'id_card' => $data['customer']['id_card'],
                                'card_expire' => $data['customer']['card_expire'],
                                'birthday' => $data['customer']['birthday'],
                                'career' => $data['customer']['career'],
                                'image_1_file_id' => $data['customer']['image_1_file_id'],
                                'image_2_file_id' => $data['customer']['image_2_file_id'],
                                'image_3_file_id' => $data['customer']['image_3_file_id'],
                                'email' => $data['customer']['email'],
                            ]);
                            if (empty($customerExist)) {
                                $customer->update([
                                    'email' => $data['customer']['email'],
                                ]);
                            }
                        }
                    }
                } else if (empty($customerExist)) {
                    $customer = new Customer();
                    $properties = array_keys($data['customer']);
                    foreach ($properties as $property) {
                        if (isset($data['customer'][$property]) && !empty($data['customer'][$property]))
                            $customer->$property = $data['customer'][$property];
                    }
                    $customer->agency_id = $agency->id;
                    $customer->customer_group_id = CustomerGroupEnum::CUS_SHP;
                    $customer->save();
                    $customer->code = "KH_" . $customer->id;
                    $customer->update();
                } else {
                    return response(__('message.cus_email_exist'), 500);
                }

                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        if (isset($data['Updatereciver']) && !($data['Updatereciver'])) {
                            // nếu dữ liệu người nhận không tồn tại thì tạo mới
                            if (empty($receiver)) {
                                $receiver = new Address();
                                $receiver->customer_id = $customer->id;
                            }
                            // ngược lại nếu đã tòn tại
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }
                        // them ngay 24-04-2020
                        else if ($data['receiver']['ward_id'] !== $receiver['ward_id']) {
                            $receiver['ward_id'] = $data['receiver']['ward_id'];
                            $receiver->save();
                        }
                        // them ngay 24-04-2020
                    } else {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    }
                }
            }

            if (isset($data['containers']) && !empty($data['containers'])) {
                $configSurcharge = Config::find(config('app.config_surcharge'));
                $configInsurance = Config::find(config('app.config_insurance'));
                $count_transport = Transport::where('user_id', $user->id)->where('type', OrderTypeEnum::TRANSPORT)->count() + 1;
                $countryreceiver = Country::find($receiver->country_id);
                $countryCode = isset($countryreceiver->code) && !empty($countryreceiver->code) ? $countryreceiver->code : 'VN';
                $transport = new Transport();
                $transport->user_id = $user->id;
                $transport->agency_id = $user->agency_id;
                $transport->customer_id = $customer->id;
                $transport->weight_unit_id = $agency->weight_unit_id;
                $transport->currency_id = $agency->currency_id;
                $transport->transport_status = 1;
                $transport->code = $user->code . sprintf('%04d', $count_transport) . "T" . $countryCode;
                $transport->type = OrderTypeEnum::TRANSPORT;
                $transport->save();

                $count_container = 1;
                $shipping_fee_local = ($receiver->city->shipping_fee) > 0 ? $receiver->city->shipping_fee : 0;
                foreach ($data['containers'] as $con) {
                    $order = new Order();
                    $order->user_id = $user->id;
                    $order->agency_id = $user->agency_id;
                    $order->payment_method = $con['pay_method'];
                    $order->customer_id = $customer->id;
                    $order->sender_first_name = $customer->first_name;
                    $order->sender_middle_name = $customer->middle_name;
                    $order->sender_last_name = $customer->last_name;
                    $order->sender_email = $customer->email;
                    $order->sender_phone = $customer->telephone;
                    $order->sender_address = $customer->address_1;
                    $order->sender_address_2 = $customer->address_2;
                    $order->sender_country_id = $customer->country_id;
                    $order->sender_province_id = $customer->province_id;
                    $order->sender_city_id = $customer->city_id;
                    $order->receiver_ward_id = $receiver->ward_id;
                    $senderCountry = Country::where('id', $order->sender_country_id)->first();
                    $order->sender_country_name = isset($senderCountry->name) ? $senderCountry->name : '';
                    $senderProvince = Province::where('id', $order->sender_province_id)->first();
                    $order->sender_province_name = isset($senderProvince->name) ? $senderProvince->name : '';
                    $senderCity = City::where('id', $order->sender_city_id)->first();
                    $order->sender_city_name = isset($senderCity->name) ? $senderCity->name : '';
                    $order->sender_post_code = $customer->postal_code;
                    $order->sender_cellphone = $customer->cellphone;
                    $order->shipping_address_id = $receiver->id;
                    $order->receive_last_name = $receiver->last_name;
                    $order->receive_first_name = $receiver->first_name;
                    $order->receiver_middle_name = $receiver->middle_name;
                    $order->receiver_email = $receiver->email;
                    $order->receiver_phone = $receiver->telephone;
                    $order->receiver_address = $receiver->address_1;
                    $order->receiver_address_2 = $receiver->address_2;
                    $order->receiver_country_id = $receiver->country_id;
                    $order->receiver_province_id = $receiver->province_id;
                    $order->receiver_city_id = $receiver->city_id;
                    $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
                    $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
                    $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
                    $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
                    $receiverCity = City::where('id', $order->receiver_city_id)->first();
                    $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';
                    $receiverWard = Ward::where('id', $order->receiver_ward_id)->first();
                    $order->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
                    $order->receiver_cellphone = $receiver->cellphone;
                    $order->receiver_post_code = $receiver->postal_code;
                    $order->currency_id = $agency->currency_id;
                    $order->dimension_unit_id = $agency->dimension_unit_id;
                    $order->weight_unit_id = $agency->weight_unit_id;
                    $order->transport_id = $transport->id;
                    if(isset($con['cod_type'])){
                        $order->cod_type = $con['cod_type']; //them ngay 21-07-12
                    }
                    else
                    {
                        $order->cod_type = 0;
                    }
                    if ($data['is_crr']) {
                        $order->is_crr = 1;
                    }
                    $order->save();
                    $total_weight = 0;
                    $total_goods = 0;
                    $total_declare_price = 0;
                    $total_discount = 0;
                    $total_surcharge_fee = 0;
                    $total_insurrance_fee = 0;
                    $total_final = 0;
                    $total_agency_price = 0;
                    $description = "";
                    $products_code = "";
                    $user_note = "";
                    // $flag_battery = 0;
                    // $flag_perfuner = 0;
                    $flag_fast = NULL;
                    $bin_danger = 000;
                    foreach ($con['products'] as $product) {
                        $source = Product::where('id', '=', $product['product_id'])->first();
                        if (empty($source)) {
                            continue;
                        }
                        // doạn lý cảnh báo hàng nguy hiểm
                        if ($source->danger > 0) {
                            $bin_danger = $bin_danger | decbin($source->danger);
                        }
                        // hết cảnh báo hàng nguy hiểm
                        if ($source->id == 92) {
                            $flag_fast = 1;
                        }
                        $quantity = 0;
                        $agency_sub_total_goods = 0;
                        $orderItem = new OrderItem();
                        $orderItem->name = $product['name'];
                        $orderItem->description = $product['description'];
                        $orderItem->order_id = $order->id;
                        $orderItem->product_id = $product['product_id'];
                        $orderItem->quantity = $product['quantity'];
                        $orderItem->danger = $source->danger; // thêm tín hiệu nguy hiểm cho từng order item
                        if (!empty($product['note'])) {
                            $orderItem->user_note = $product['note'];
                            $user_note .= $orderItem->user_note . "; ";
                        }
                        $orderItem->unit_goods_fee = $product['price'];
                        $total_weight += $orderItem->sub_total_weight = $product['weight'];
                        if (isset($product['by_weight']) && $product['by_weight'] == 1) {
                            $quantity = $product['weight'];
                        } else {
                            $quantity = $product['quantity'];
                        }
                        $agency_sub_total_goods = $source->sale_price * $quantity;
                        $total_goods += $orderItem->sub_total_goods = round($product['price'] * $quantity,2);
                        $total_declare_price += $orderItem->sub_total_declare_price = $product['declared_value'];
                        $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                        $orderItem->per_discount = $discount;
                        $total_discount += $orderItem->sub_total_discount = round($orderItem->sub_total_goods * ($discount / 100), 2);
                        $quota = $product['declared_value'] / $quantity;
                        if ($quota > $configSurcharge->quota) {
                            $total_surcharge_fee += $orderItem->sub_total_surcharge_fee = $product['declared_value'] * ($configSurcharge->percent / 100);
                        } else {
                            $orderItem->sub_total_surcharge_fee = 0;
                        }
                        if (isset($product['is_insurance']) && $product['is_insurance'] == 1) {
                            $total_insurrance_fee += $orderItem->sub_total_insurrance_fee = $product['declared_value'] * ($configInsurance->percent / 100);
                            $orderItem->is_insurance = 1;
                        } else {
                            $orderItem->sub_total_insurrance_fee = 0;
                        }
                        $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                        $total_final += $orderItem->sub_total_goods - $orderItem->sub_total_discount;

                        $description .= $product['quantity'] . ' ' . $product['name'] . '; ';
                        $products_code .= $product['code'] . ' - ';
                        $orderItem->messure_unit_id = $source->messure_unit_id;
                        $orderItem->agency_discount = 0;
                        // cong thuc tinh hue hong
                        //$orderItem->per_discount nhập từ form, $source->quota_discount định mức chiết khấu từ bảng sản phẩm
                        // cong thuc tinh hue hong
                        $discount_out = $orderItem->per_discount - $source->quota_discount; // 25- 25=0
                        if ($discount_out >= 0) {
                            $commission = ($source->commission_quota_percent - $discount_out); //15-0
                            if ($commission < 0) {
                                $commission = 0;
                            }
                            $commission_amount = $source->commission_quota_amount;
                        } else {
                            $commission = $source->commission_percent;
                            $commission_amount = $source->commission_amount;
                        }
                        // kiem tra crr, neu co CRR thi giam 10%
                        if ($order->is_crr) {
                            $commission =  $commission - 10;
                        }
                        $orderItem->agency_discount = ((($agency_sub_total_goods - $orderItem->sub_total_discount) * $commission) / 100) + $commission_amount + ($orderItem->sub_total_surcharge_fee * ($configSurcharge->agency_percent / 100));
                        $total_agency_price += $orderItem->agency_discount;
                        $orderItem->save();
                    }

                    $orderAfter = Order::find($order->id);
                    $orderAfter->description = $description;
                    $orderAfter->user_note = $user_note;
                    $orderAfter->products = $products_code;
                    $orderAfter->length = $con['length'];
                    $orderAfter->width = $con['width'];
                    $orderAfter->height = $con['height'];
                    $orderAfter->receive_status = $con['status'];
                    // xoa san pham sau khai bao
                    $orderAfter->total_weight = $total_weight;

                    $orderAfter->shipping_fee = $shipping_fee_local;
                    $orderAfter->total_shipping_fee = $orderAfter->total_weight * $orderAfter->shipping_fee;
                    $orderAfter->total_declare_price = $total_declare_price;
                    $orderAfter->total_surcharge_fee = $total_surcharge_fee;
                    $orderAfter->total_insurrance_fee = $total_insurrance_fee;
                    $orderAfter->total_discount = $total_discount;
                    $orderAfter->total_goods_fee = $total_goods;
                    $orderAfter->shipping_fast = $flag_fast;
                    $totalFee = $orderAfter->total_shipping_fee + $orderAfter->total_goods_fee;
                    $getminFee = self::getMinFeeadmin($agency->id, $orderAfter->receiver_province_id, $totalFee, $total_agency_price);
                    $orderAfter->total_final = $getminFee['minfee'] + $orderAfter->total_surcharge_fee + $orderAfter->total_insurrance_fee - $orderAfter->total_discount;
                    $total_agency_price = $getminFee['commission'];
                    //khoa doan nay
                    // $minFee = OrderService::getMinFeeadmin($agency->id, $orderAfter->receiver_province_id);
                    // if($minFee > $totalFee){
                    //     $orderAfter->total_final = $minFee + $orderAfter->total_surcharge_fee + $orderAfter->total_insurrance_fee - $orderAfter->total_discount;
                    //     if($total_agency_price < 5)
                    //     {
                    //         $total_agency_price=5;
                    //     }

                    // }else{
                    //     $orderAfter->total_final = $totalFee + $orderAfter->total_surcharge_fee + $orderAfter->total_insurrance_fee - $orderAfter->total_discount;
                    // }
                    // Kiểm tra mã coupon
                    if (!empty($con['coupon_code'])) {
                        $amount = CampaignService::getAmountCoupon($con['coupon_code'], $orderAfter->customer_id, $orderAfter->total_weight);
                        if ($amount > 0) {
                            $orderAfter->coupon_code = $con['coupon_code'];
                            $orderAfter->coupon_amount = $amount;
                            $orderAfter->coupon_time = date("Y-m-d H:i:s");

                            $orderAfter->total_final -= $orderAfter->coupon_amount;
                            if ($orderAfter->total_final < 0) {
                                $orderAfter->total_final = 0;
                            }
                            $campaignLog = new CampaignLog();
                            $campaignLog->customer_id = $orderAfter->customer_id;
                            $campaignLog->order_id = $orderAfter->id;
                            $campaignLog->coupon_code = $orderAfter->coupon_code;
                            $campaignLog->coupon_amount = $amount;
                            $campaignLog->weight = $orderAfter->total_weight;
                            $campaignLog->save();

                            $campaign = Campaign::where('code', '=', $con['coupon_code'])->first();
                            if (!empty($campaign)) {
                                $numberUse = Order::where('coupon_code', '=', $con['coupon_code'])->count();
                                if ($numberUse + 1 >= $campaign->num_of_use) {
                                    $campaign->is_used = 1;
                                    $campaign->update();
                                }
                            }
                        }
                    }
                    /*  if($orderAfter->is_crr){
                        $orderAfter->agency_discount = 0.9 * $total_agency_price;
                    }else{
                        $orderAfter->agency_discount = $total_agency_price;
                    }
                    if($orderAfter->agency_discount < 0){
                            $orderAfter->agency_discount = 0;
                    }*/
                    $orderAfter->agency_discount = $total_agency_price;
                    $orderAfter->code = $user->code . sprintf('%04d', $count_transport) . "T" . $countryCode . $count_container . count($data['containers']);
                    $orderAfter->total_paid_amount = 0;
                    // ghi nhan so tien thanh toan
                    if ($con['pay'] != 0) {
                        $count = Transaction::where('order_id', $orderAfter->id)->count();

                        $transaction = new Transaction();
                        $transaction->user_id = $orderAfter->user_id;
                        $transaction->agency_id = $orderAfter->agency_id;
                        $transaction->customer_id = $orderAfter->customer_id;
                        $transaction->transport_id = $orderAfter->transport_id;
                        $transaction->order_id = $orderAfter->id;
                        $transaction->currency_id = $orderAfter->currency_id;
                        $transaction->code = "PT_" . $orderAfter->code . "_{$count}";
                        $transaction->transaction_at = now();
                        $transaction->type = TransactionTypeEnum::RECEIPT;
                        $transaction->credit_amount = $con['pay'];
                        $transaction->total_amount = $orderAfter->total_final;
                        $transaction->total_agency_discount = $orderAfter->agency_discount;
                        $transaction->agency_discount = $transaction->credit_amount * ($transaction->total_agency_discount / $transaction->total_amount);
                        $transaction->goods_code = $orderAfter->code;
                        $transaction->save();
                    }
                    // hết ghi nhan so tien thanh toan
                    $orderAfter->total_paid_amount += $con['pay'];
                    $orderAfter->total_remain_amount = $orderAfter->total_final - $orderAfter->total_paid_amount > 0 ? round($orderAfter->total_final, 2) - round($orderAfter->total_paid_amount, 2) : 0;
                    if ($orderAfter->total_remain_amount == 0) {
                        $orderAfter->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                    } else {
                        $orderAfter->payment_status = OrderPaymentStatusEnum::PART_PAID;
                    }
                    $orderAfter->last_payment_at = now();
                    // $orderAfter->last_payment_at = ConvertsUtil::dateFormat($con['date_pay'], config('app.date_format'));
                    // ngày 25-12-2020 không sử dụng tích điểm cho đại lý
                    // PointService::addPointToCustomer($orderAfter->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                    // PointService::addPointOrder($orderAfter, PointEnum::TYPE_CONFIG_ORDER);

                    $orderAfter->box_number = $count_container;
                    $orderAfter->danger_type =  bindec($bin_danger);
                    $orderAfter->save();
                    $count_container++;
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response($orderAfter->id);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $user_login = auth()->user();
        $query = Order::where([]);
        $query->where('id', '=', $id);
        if (!$user_login->role->admin) {
            $query->where('user_id', '=', $user_login->id);
        }
        $transport = $query
            ->with([
                'customer' => function ($query) {
                    $query->with('image1');
                    $query->with('image2');
                    $query->with('image3');
                },
                'receiver',
                'order_items' => function ($query) {
                    $query->with('messure');
                    $query->with('product');
                }
            ])
            ->first();
        if (empty($transport)) {
            return response(["success" => false, "message" => __('message.cart-no-product'), "errors" => [], "data" => []], 401);
        }
        $user = User::where('id', $transport['user_id'])->first('agency_id');
        $agencyfee = Agency::where('id', $user['agency_id'])->first(['id', 'min_fee']);
        $transport->minFee = $agencyfee['min_fee'];
        if (!isset($transport['agency_id'])) {
            $transport->agency = $agencyfee['id'];
        }
        $transport->last_payment_at = $transport->last_payment_date;
        $transport->last_paid_amount = 0;
        $transport->last_date_pay = null;
        $transport->receiver->first_name = $transport->receive_first_name;
        $transport->receiver->middle_name = $transport->receiver_middle_name;
        $transport->receiver->last_name = $transport->receive_last_name;
        $transport->receiver->address_1 = $transport->receiver_address;
        $transport->receiver->country_id = $transport->receiver_country_id;
        $transport->receiver->province_id = $transport->receiver_province_id;
        $transport->receiver->city_id = $transport->receiver_city_id;
        $transport->receiver->postal_code = $transport->receiver_post_code;
        $transport->receiver->telephone = $transport->receiver_phone;
        $transport->receiver->email = $transport->receiver_email;
        $transport->receiver->cellphone = $transport->receiver_cellphone;
        if (!$transport) {
            return response('Not Found', 404);
        }

        return response([
            'transport' => $transport
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $potransport = Order::find($id);
        if ($potransport->order_status > 1) {
            $this->viewpo($id);
            return view('transport::home.view', $this->data);
        } else {
            $this->editpo($id);
            return view('transport::home.edit', $this->data);
        }
    }
    public function editpo($request)
    {
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['vnPriority'] = config('app.vn_priority');
        // $this->data['caPriority'] = config('app.ca_priority');
        // $this->data['caVnPriorityFee'] = config('app.ca_priority_vn_priority_fee');
        // $this->data['caVnPriorityDefaultFee'] = config('app.ca_priority_vn_default_fee');
        // $this->data['caVnDefaultPriorityFee'] = config('app.ca_default_vn_priority_fee');
        // $this->data['caVnDefaultFee'] = config('app.ca_default_vn_default_fee');
        $this->data['vn_hcm_priority'] = config('app.vn_hcm_priority');
        $this->data['discountLevel'] = config('app.discount_level');
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::home.edit', $this->data);
    }
    public  function  viewpo($id)
    {
        $po_order = Order::find($id);
        $this->data['order'] = $po_order;
        $order_items = OrderItem::where('order_id', '=', $po_order['id'])->get();
        $this->data['order_items'] = $order_items;

        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['vnPriority'] = config('app.vn_priority');
        $this->data['vn_hcm_priority'] = config('app.vn_hcm_priority');
        $this->data['discountLevel'] = config('app.discount_level');

        $this->data['language'] = LanguageUtil::getKeyJavascript();
        // return view('transport::home.edit', $this->data);
    }
    public static function  bakedit(Request $request)
    {
        $data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $data['configInsurance'] = Config::find(config('app.config_insurance'));
        $data['vnPriority'] = config('app.vn_priority');
        // $this->data['caPriority'] = config('app.ca_priority');
        // $this->data['caVnPriorityFee'] = config('app.ca_priority_vn_priority_fee');
        // $this->data['caVnPriorityDefaultFee'] = config('app.ca_priority_vn_default_fee');
        // $this->data['caVnDefaultPriorityFee'] = config('app.ca_default_vn_priority_fee');
        // $this->data['caVnDefaultFee'] = config('app.ca_default_vn_default_fee');
        $data['vn_hcm_priority'] = config('app.vn_hcm_priority');
        $data['discountLevel'] = config('app.discount_level');
        $data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::home.edit', $data);
    }
    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = null;
            $receiver = null;
            // $user = auth()->user();
            $customemail = null;
            $agency_id = $data['containers'][0]['agency_id'];
            $agency = Agency::find($agency_id);
            if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                $customer = Customer::where('id', $data['customer']['id'])->first();
                $customemail = $customer['email'];
                if (empty($customer)) {
                    return response('Internal Server Error', 500);
                }
            }
            if (isset($data['Updatecustomer']) && !($data['Updatecustomer'])) {
                $customer->update([
                    'first_name'  => $data['customer']['first_name'],
                    'middle_name'  => $data['customer']['middle_name'],
                    'last_name'  => $data['customer']['last_name'],
                    'address_1'  => $data['customer']['address_1'],
                    'address_1'  => $data['customer']['address_1'],
                    'telephone'  => $data['customer']['telephone'],
                    'cellphone'  => $data['customer']['cellphone'],
                    'postal_code'  => $data['customer']['postal_code'],
                    'city_id'  => $data['customer']['city_id'],
                    'province_id'  => $data['customer']['province_id'],
                    'country_id' => $data['customer']['country_id'],
                    'id_card' => $data['customer']['id_card'],
                    'card_expire' => $data['customer']['card_expire'],
                    'birthday' => $data['customer']['birthday'],
                    'career' => $data['customer']['career'],
                    'image_1_file_id' => $data['customer']['image_1_file_id'],
                    'image_2_file_id' => $data['customer']['image_2_file_id'],
                    'image_3_file_id' => $data['customer']['image_3_file_id'],
                ]);
                if (empty($customemail) && isset($data['customer']['email'])) {
                    $customerExist = Customer::where('email', $data['customer']['email'])->first();
                    if (!empty($customerExist)) {
                        $customer->update([
                            'email' => $data['customer']['email'],
                        ]);
                    } else {
                        return response(__('message.cus_email_exist'), 500);
                    }
                }
            }
            if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                $receiver = Address::where('id', $data['receiver']['id'])->first();
            }
            if (isset($data['Updatereciver']) && !($data['Updatereciver'])) {

                if (empty($receiver)) {
                    $receiver = new Address();
                    $properties = array_keys($data['receiver']);
                    foreach ($properties as $property) {
                        if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                            $receiver->$property = $data['receiver'][$property];
                    }
                    $receiver->customer_id = $customer->id;
                    $receiver->save();
                } else {
                    $properties = array_keys($data['receiver']);
                    foreach ($properties as $property) {
                        if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                            $receiver->$property = $data['receiver'][$property];
                    }
                    $receiver->update();
                }
            }
            // them ngay 24-04-2020
            else if ($data['receiver']['ward_id'] !== $receiver['ward_id']) {
                $receiver['ward_id'] = $data['receiver']['ward_id'];
                $receiver->save();
            }
            // them ngay 24-04-2020
            // fix phí vận chuyển nội địa ngày 25-05-2021

            $shipping_fee_local = ($receiver->city->shipping_fee) > 0 ? $receiver->city->shipping_fee : 0;
            // fix phí vận chuyển nội địa ngày 25-05-2021
            if (isset($data['containers']) && !empty($data['containers'])) {
                $configSurcharge = Config::find(config('app.config_surcharge'));
                $configInsurance = Config::find(config('app.config_insurance'));
                if (
                    isset($data['containers'][0])
                    && !empty($data['containers'][0])
                    && isset($data['containers'][0]['id'])
                    && !empty($data['containers'][0]['id'])
                ) {
                    $con = $data['containers'][0];
                    $order = Order::find($data['containers'][0]['id']);
                    if ($order->shipping_status <= OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN) {
                        $order->customer_id = $customer->id;
                        $order->sender_first_name = $customer->first_name;
                        $order->sender_middle_name = $customer->middle_name;
                        $order->sender_last_name = $customer->last_name;
                        $order->sender_email = $customer->email;
                        $order->sender_phone = $customer->telephone;
                        $order->sender_address = $customer->address_1;
                        $order->sender_address_2 = $customer->address_2;
                        $order->sender_country_id = $customer->country_id;
                        $order->sender_province_id = $customer->province_id;
                        $order->sender_city_id = $customer->city_id;
                        $senderCountry = Country::where('id', $order->sender_country_id)->first();
                        $order->sender_country_name = isset($senderCountry->name) ? $senderCountry->name : '';
                        $senderProvince = Province::where('id', $order->sender_province_id)->first();
                        $order->sender_province_name = isset($senderProvince->name) ? $senderProvince->name : '';
                        $senderCity = City::where('id', $order->sender_city_id)->first();
                        $order->sender_city_name = isset($senderCity->name) ? $senderCity->name : '';
                        $order->sender_post_code = $customer->postal_code;
                        $order->sender_cellphone = $customer->cellphone;
                        $order->shipping_address_id = $receiver->id;
                        $order->receive_last_name = $receiver->last_name;
                        $order->receive_first_name = $receiver->first_name;
                        $order->receiver_middle_name = $receiver->middle_name;
                        $order->receiver_email = $receiver->email;
                        $order->receiver_phone = $receiver->telephone;
                        $order->receiver_address = $receiver->address_1;
                        $order->receiver_address_2 = $receiver->address_2;
                        $order->receiver_country_id = $receiver->country_id;
                        $order->receiver_province_id = $receiver->province_id;
                        $order->receiver_city_id = $receiver->city_id;
                        $order->receiver_ward_id = $receiver->ward_id;
                        $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
                        $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
                        $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
                        $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
                        $receiverCity = City::where('id', $order->receiver_city_id)->first();
                        $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';
                        $receiverWard = Ward::where('id', $order->receiver_ward_id)->first();
                        $order->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
                        $order->receiver_post_code = $receiver->postal_code;
                        $order->receiver_cellphone = $receiver->cellphone;
                        $order->currency_id = $agency->currency_id;
                        $order->dimension_unit_id = $agency->dimension_unit_id;
                        $order->weight_unit_id = $agency->weight_unit_id;
                        $order->cod_type = $con['cod_type']; //them ngay 21-07-12
                        if ($data['is_crr']) {
                            $order->is_crr = 1;
                        } else {
                            $order->is_crr = 0;
                        }
                        $order->save();
                        $total_weight = 0;
                        $total_goods = 0;
                        $total_declare_price = 0;
                        $total_discount = 0;
                        $total_surcharge_fee = 0;
                        $total_insurrance_fee = 0;
                        $total_final = 0;
                        $total_agency_price = 0;
                        $description = "";
                        $product_codes = "";
                        $user_notes = "";
                        $list_order_item = OrderItem::where('order_id', '=', $order['id'])->pluck('id')->toArray();
                        // them ngay 04-07-2021
                        $flag_fast = NULL;
                        $bin_danger = 000;
                        $list_order_use = [];
                        foreach ($con['products'] as $product) {
                            if (isset($product['product']) && !empty($product['product'])) {
                                $quantity = 0;
                                $agency_sub_total_goods = 0;
                                $source = Product::where('id', '=', $product['product_id'])->first();
                                if (empty($source)) {
                                    continue;
                                }
                                if (isset($product['id']) && !empty($product['id'])) {
                                    $orderItem = OrderItem::find($product['id']);
                                    array_push($list_order_use, $orderItem->id);
                                } else {
                                    $orderItem = new OrderItem();
                                }
                                // doạn lý cảnh báo hàng nguy hiểm
                                if ($source->danger > 0) {
                                    $bin_danger = $bin_danger | decbin($source->danger);
                                }
                                // hết cảnh báo hàng nguy hiểm
                                if ($source->id === 92) {
                                    $flag_fast = 1;
                                }
                                $orderItem->name = $product['product']['name'];
                                $orderItem->description = $product['product']['description'];
                                $orderItem->order_id = $order->id;
                                $orderItem->product_id = $product['product_id'];
                                $orderItem->quantity = $product['quantity'];
                                if (!empty($product['user_note'])) {
                                    $orderItem->user_note = $product['user_note'];
                                    $user_notes .= $orderItem->user_note . '; ';
                                }
                                $orderItem->unit_goods_fee = isset($product['product']['pickup_fee']) ? $product['product']['sale_price'] + $product['product']['pickup_fee'] : $product['unit_goods_fee'];
                                $total_weight += $orderItem->sub_total_weight = $product['sub_total_weight'];
                                if (isset($product['product']['by_weight']) && $product['product']['by_weight'] == 1) {
                                    $quantity = $product['sub_total_weight'];
                                } else {
                                    $quantity = $product['quantity'];
                                }
                                $agency_sub_total_goods = round($source->sale_price * $quantity,2);
                                $total_goods += $orderItem->sub_total_goods = round($orderItem->unit_goods_fee * $quantity,2);
                                $total_declare_price += $orderItem->sub_total_declare_price = $product['sub_total_declare_price'];
                                $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                                $orderItem->per_discount = $discount;
                                $total_discount += $orderItem->sub_total_discount = round($orderItem->sub_total_goods * ($discount / 100), 2);
                                $quota = $orderItem->sub_total_declare_price / $quantity;
                                if ($quota > $configSurcharge->quota) {
                                    $total_surcharge_fee += $orderItem->sub_total_surcharge_fee = $orderItem->sub_total_declare_price * ($configSurcharge->percent / 100);
                                } else {
                                    $orderItem->sub_total_surcharge_fee = 0;
                                }

                                if (isset($product['is_insurance']) && !empty($product['is_insurance'])) {
                                    $total_insurrance_fee += $orderItem->sub_total_insurrance_fee = $orderItem->sub_total_declare_price * ($configInsurance->percent / 100);
                                    $orderItem->is_insurance = 1;
                                } else {
                                    $orderItem->is_insurance = 0;
                                    $orderItem->sub_total_insurrance_fee = 0;
                                }
                                // $total_final += $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                                $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                                $total_final += $orderItem->sub_total_goods - $orderItem->sub_total_discount;

                                $description .= $product['quantity'] . ' ' . $product['product']['name'] . '; ';
                                $product_codes .= $product['product']['code'] . '; ';
                                $orderItem->messure_unit_id = $source->messure_unit_id;
                                // cong thuc tinh hue hong
                                //$orderItem->per_discount nhập từ form, $source->quota_discount định mức chiết khấu từ bảng sản phẩm
                                // cong thuc tinh hue hong
                                $discount_out = $orderItem->per_discount - $source->quota_discount;
                                if ($discount_out >= 0) {
                                    $commission = ($source->commission_quota_percent - $discount_out);
                                    if ($commission < 0) {
                                        $commission = 0;
                                    }
                                    $commission_amount = $source->commission_quota_amount;
                                } else {
                                    $commission = $source->commission_percent;
                                    $commission_amount = $source->commission_amount;
                                }
                                if ($order->is_crr) {
                                    $commission =  $commission - 10;
                                }
                                $orderItem->agency_discount = ((($agency_sub_total_goods - $orderItem->sub_total_discount) * $commission) / 100) + $commission_amount + ($orderItem->sub_total_surcharge_fee * ($configSurcharge->agency_percent / 100));
                                $total_agency_price += $orderItem->agency_discount;
                                $orderItem->danger = $source->danger; // thêm tín hiệu nguy hiểm cho từng order item
                                $orderItem->save();
                                // xoa san pham sau khai bao

                            }
                        }
                        // xoa san pham sau khai bao
                        $list_item_deletes = array_diff($list_order_item, $list_order_use);
                        foreach ($list_item_deletes as $list_item_delete) {
                            OrderItem::find($list_item_delete)->delete();
                        }
                        // xoa san pham sau khai bao
                        //  use SoftDeletes;
                        //het vong lap kiem tra san pham
                        $orderAfter = Order::find($order->id);
                        $orderAfter->description = $description;
                        $orderAfter->products = $product_codes;
                        $orderAfter->length = $con['length'];
                        $orderAfter->width = $con['width'];
                        $orderAfter->height = $con['height'];
                        $orderAfter->receive_status = $con['receive_status'];
                        // xoa san pham sau khai bao
                        $orderAfter->total_weight = $total_weight;
                        $orderAfter->shipping_fee = $shipping_fee_local;
                        $orderAfter->total_shipping_fee = $orderAfter->total_weight * $orderAfter->shipping_fee;
                        $orderAfter->total_declare_price = $total_declare_price;
                        $orderAfter->total_surcharge_fee = $total_surcharge_fee;
                        $orderAfter->total_insurrance_fee = $total_insurrance_fee;
                        $orderAfter->total_discount = $total_discount;
                        $orderAfter->total_goods_fee = $total_goods;
                        $totalFee = $orderAfter->total_shipping_fee + $orderAfter->total_goods_fee;
                        $getminFee = self::getMinFeeadmin($agency->id, $orderAfter->receiver_province_id, $totalFee, $total_agency_price);
                        $orderAfter->total_final = $getminFee['minfee'] + $orderAfter->total_surcharge_fee + $orderAfter->total_insurrance_fee - $orderAfter->total_discount;
                        $total_agency_price = $getminFee['commission'];
                        // $minFee = OrderService::getMinFeeadmin($order->agency_id, $orderAfter->receiver_province_id);
                        // if($minFee > $totalFee){
                        //     $orderAfter->total_final = $minFee + $orderAfter->total_surcharge_fee + $orderAfter->total_insurrance_fee - $orderAfter->total_discount;
                        // }else{
                        //     $orderAfter->total_final = $totalFee + $orderAfter->total_surcharge_fee + $orderAfter->total_insurrance_fee - $orderAfter->total_discount;
                        // }
                        // Kiểm tra mã coupon
                        if (!empty($con['coupon_code'])) {
                            $amount = CampaignService::getAmountCoupon($con['coupon_code'], $orderAfter->customer_id, $orderAfter->total_weight);
                            if ($amount > 0) {
                                $orderAfter->coupon_code = $con['coupon_code'];
                                $orderAfter->coupon_amount = $amount;
                                $orderAfter->coupon_time = date("Y-m-d H:i:s");

                                $orderAfter->total_final -= $orderAfter->coupon_amount;
                                if ($orderAfter->total_final < 0) {
                                    $orderAfter->total_final = 0;
                                }
                                $campaignLog = new CampaignLog();
                                $campaignLog->customer_id = $orderAfter->customer_id;
                                $campaignLog->order_id = $orderAfter->id;
                                $campaignLog->coupon_code = $orderAfter->coupon_code;
                                $campaignLog->coupon_amount = $amount;
                                $campaignLog->weight = $orderAfter->total_weight;
                                $campaignLog->save();

                                $campaign = Campaign::where('code', '=', $con['coupon_code'])->first();
                                if (!empty($campaign)) {
                                    $numberUse = Order::where('coupon_code', '=', $con['coupon_code'])->count();
                                    if ($numberUse + 1 >= $campaign->num_of_use) {
                                        $campaign->is_used = 1;
                                        $campaign->update();
                                    }
                                }
                            }
                        } else {
                            $orderAfter->coupon_code = null;
                            $orderAfter->coupon_amount = null;
                            $orderAfter->coupon_time = null;

                            CampaignLog::where('order_id', '=', $orderAfter->id)->delete();
                        }
                        /* if($orderAfter->is_crr){
                            $orderAfter->agency_discount = 0.9 * $total_agency_price;
                        }else{
                            $orderAfter->agency_discount = $total_agency_price;
                        }
                        if($orderAfter->agency_discount < 0){
                            $orderAfter->agency_discount = 0;
                        }*/

                        $orderAfter->user_note = $user_notes;
                        if (($con['last_paid_amount']) != 0) {
                            if ($con['last_paid_amount'] > 0) {
                                $type_transaction = TransactionTypeEnum::RECEIPT;
                            } else {
                                $type_transaction = TransactionTypeEnum::SLIP;
                            }
                            $last_paid_amount = abs($con['last_paid_amount']);
                            $count = Transaction::where('order_id', $orderAfter->id)->count();
                            $transaction = new Transaction();
                            $transaction->user_id = $orderAfter->user_id;
                            $transaction->agency_id = $orderAfter->agency_id;
                            $transaction->customer_id = $orderAfter->customer_id;
                            $transaction->transport_id = $orderAfter->transport_id;
                            $transaction->order_id = $orderAfter->id;
                            $transaction->currency_id = $orderAfter->currency_id;
                            $transaction->code = "PT_" . $orderAfter->code . "_{$count}";
                            $transaction->transaction_at = now();
                            $transaction->type = $type_transaction;
                            $transaction->credit_amount = $last_paid_amount;
                            $transaction->total_amount = $orderAfter->total_final;
                            $transaction->total_agency_discount = $orderAfter->agency_discount;
                            $transaction->agency_discount = abs($orderAfter->agency_discount - $total_agency_price);
                            $transaction->goods_code = $orderAfter->code;
                            $transaction->save();

                            $orderAfter->total_paid_amount += $con['last_paid_amount'];
                            $orderAfter->total_remain_amount = $orderAfter->total_final - $orderAfter->total_paid_amount > 0 ? round($orderAfter->total_final, 2) - round($orderAfter->total_paid_amount, 2) : 0;
                            if ($orderAfter->total_remain_amount == 0) {
                                $orderAfter->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                            } else {
                                $orderAfter->payment_status = OrderPaymentStatusEnum::PART_PAID;
                            }
                            $orderAfter->last_payment_at =  now();
                            // ngày 25-12-2020 không sử dụng tích điểm cho đại lý
                            // PointService::addPointToCustomer($orderAfter->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);
                            // PointService::addPointOrder($orderAfter, PointEnum::TYPE_CONFIG_ORDER);
                        }

                        //ttt
                        $orderAfter->total_paid_amount += $con['last_paid_amount'];
                        $orderAfter->total_remain_amount = $orderAfter->total_final - $orderAfter->total_paid_amount > 0 ? round($orderAfter->total_final, 2) - round($orderAfter->total_paid_amount, 2) : 0;
                        if ($orderAfter->total_remain_amount == 0) {
                            $orderAfter->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                        } else {
                            $orderAfter->payment_status = OrderPaymentStatusEnum::PART_PAID;
                        }
                        $orderAfter->last_payment_at =  now();
                        //ttt


                        // het phan cap nhat giao dich
                        $orderAfter->total_discount = $total_discount;
                        $orderAfter->agency_discount = $total_agency_price;
                        $orderAfter->danger_type =  bindec($bin_danger);
                        $orderAfter->shipping_fast = $flag_fast;
                        $orderAfter->update();
                    } else
                    //  thông báo đơn hàng đã nhập kho không thể xử lý
                    {
                        return response(__('message.err_order_no_exist_or_stockin'), 500);
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function updatecustomer(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = null;
            if (isset($data['id']) && !empty($data['id'])) {
                $customer = Customer::where('id', $data['id'])->first();
                if (empty($customer)) {
                    return response('Internal Server Error', 500);
                }
                $properties = array_keys($data);
                foreach ($properties as $property) {
                    if (isset($data[$property]) && !empty($data[$property]) && $property != 'full_name')
                        $customer->$property = $data[$property];
                }

                $customer->update();
            } else {
                return response(__('message.cus_email_exist'), 500);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }
}
