<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Enum\OrderTypeEnum;
use App\Enum\OrderTrackingStatusEnum;
use App\Enum\OrderLogStatusEnum;
use App\Enum\OrderStatusEnum;
use App\Order;
use App\OrderTracking;
use App\OrderLog;
use Exception;
class PickupController extends Controller {

    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct() {
        $this->data = [
            'menu' => '4.13'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $query = Order::where([]);
        $user = Auth::user();
        if(strtoupper($user->role->admin) == 1){
            $query->whereNotNull('agency_id');
        }else{
            $query->where('user_id', '=', $user->id);
        }
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
            $query->where('order_status', '=', OrderStatusEnum::STATUS_NEW);
        if (!empty($request->query('order_code'))) {
            $query->where('code', 'like', '%'.$request->query('order_code').'%');
        }
        $this->data['orders'] = $query->orderBy('tbl_order.id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();
        return view('transport::pickup.index', $this->data);
    }

    public function pushItems(Request $request) {
        $validator = Validator::make($request->input(), [
            'order_ids' => 'required'
        ],[],[
            'order_ids' => __('message.list_order_empty')
        ]);
        if ($validator->fails()) {
         return response(["success" => false, "message" => $validator->errors()->first(), []]);
            }
        $user = Auth::user();
        if (empty($user)) {
            return response(["success" => false, "message" => __('message.err_user_not_login'), "data" => []]);
        }
        $orderIds = $request->input('order_ids');
        if (empty($orderIds)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        $orderData = Order::find($orderIds);
        if (empty($orderData)) {
            return response(["success" => false, "message" => __('message.list_order_empty'), "data" => []]);
        }
        try {
            DB::beginTransaction();

            foreach ($orderData as $order) {
                $order->order_status = OrderStatusEnum::STATUS_PROCESSING;
                $order->update();

                $orderTracking = new OrderTracking();
                $orderTracking->order_id = $order->id;
                $orderTracking->create_user_id = $user->id;
                $orderTracking->tracking_code = $order->code;
                $orderTracking->order_code = $order->code;
                $orderTracking->status = OrderTrackingStatusEnum::STATUS_NEW;
                $orderTracking->save();

                $orderLog = new OrderLog();
                $orderLog->order_id = $order->id;
                $orderLog->create_user_id = $user->id;
                $orderLog->status = OrderLogStatusEnum::STATUS_NEW;
                $orderLog->save();
            }

            DB::commit();
            return response(["success" => true, "message" => __('message.stockout_success'), "data" => ['refUrl' => route('transport.pickup.index')]]);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["success" => false, "message" => $e->getMessage(), "data" => []]);
        }
    }

}
