<?php

namespace Modules\Transport\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Enum\OrderTypeEnum;
use App\Enum\OrderStatusEnum;
use App\Order;
use App\OrderItem;
use App\Config;
use App\Product;
use App\Country;
use App\Province;
use App\City;
use App\Ward;
use App\Agency;
use App\Address;
use App\Customer;
use App\Enum\CustomerGroupEnum;
use App\Enum\OrderShippingStatusEnum;
use App\Exports\ExportQuery;
use Exception;

class TransportOrderController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    /**
     * AgenciesController constructor.
     */
    public function __construct()
    {
        $this->data = [
            'menu' => '4.12'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Order::where([]);
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_NEW);
        $query->whereNull('agency_id');
        if (!empty($request->query('code'))&& strlen($request->query('code'))>6)
        {
            $query->where('code', 'like', '%'.$request->query('code').'%');
        }
        else
        {
            if(!empty($request->query('code')))
            {
            $query->where('code', 'like', '%'.$request->query('code').'%');
            }
            if (!empty($request->query('from_date'))) {
                $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
            }
            if (!empty($request->query('to_date'))) {
                $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
            }
        }
        $this->data['orders'] = $query->orderBy('id', 'desc')->select()->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::quote.order.index', $this->data);
    }


    public function exportExcel(Request $request)
    {
        $query = Order::where([]);
        $query->select('code', 'customer_note', 'user_note', 'description','created_at', 'sender_first_name', 'sender_middle_name', 'sender_last_name', 'sender_email', 'sender_phone', 'sender_cellphone', 'sender_address', 'sender_post_code', 'receive_first_name', 'receiver_middle_name', 'receive_last_name', 'receiver_email', 'receiver_phone', 'receiver_cellphone', 'receiver_address', 'receiver_post_code', 'is_crr', 'total_weight', 'total_final', 'total_paid_amount', 'total_remain_amount', 'agency_discount', 'order_status', 'shipping_status', 'payment_status', 'receive_status');
        $query->where('type', '=', OrderTypeEnum::TRANSPORT);
        $query->where('order_status', '!=', OrderStatusEnum::STATUS_NEW);
        $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('shipping_status'))) {
            $query->where('shipping_status', '=', $request->query('shipping_status'));
        }
        if (!empty($request->query('code'))) {
            $query->where('code', '=', $request->query('code'));
        }
        if (!empty($request->query('sender_phone'))) {
            $query->where('sender_phone', '=', $request->query('sender_phone'));
        }
        $query->orderBy('id', 'desc')->get()->toArray();
        return (new ExportQuery($query))->download('order_customer.xlsx');
    }

    public function printBill($id)
    {
        $order = Order::find($id);
        if (empty($order)) {
            return redirect()->route('transport.home.index');
        }
        $this->data['order'] = $order;
        return view('transport.print', $this->data);
    }
     public function printBills($id)
    {
        $currOrder = Order::find($id);
        if (empty($currOrder)) {
            return response(["success" => false, "message" => 'Đơn hàng không tồn tại', "data" => []]);
        }
        $orders = Order::where('transport_id', '=', $currOrder->transport_id)->get();
       $this->data['orders'] = $orders;
        return view('transport.prints', $this->data);
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $order = Order::where('id', '=', $id)->first();
        if (!$order) {
            return response('Not Found', 404);
        }
        $data = $this->convertOrder($order);
        return response([
            'order' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['discountLevel'] = config('app.discount_level');
        return view('transport::quote.order.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = null;
            $receiver = null;
            $user = auth()->user();
            $agency = Agency::find($user->agency_id);
            if (isset($data['customer']) && !empty($data['customer'])) {
                if (isset($data['customer']['id']) && !empty($data['customer']['id'])) {
                    $customer = Customer::where('id', $data['customer']['id'])->first();
                    $customer->update([
                        'id_card' => $data['customer']['id_card'],
                        'card_expire' => $data['customer']['card_expire'],
                        'birthday' => $data['customer']['birthday'],
                        'career' => $data['customer']['career'],
                        'image_1_file_id' => $data['customer']['image_1_file_id'],
                        'image_2_file_id' => $data['customer']['image_2_file_id'],
                        'image_3_file_id' => $data['customer']['image_3_file_id']
                    ]);
                    if (empty($customer)) {
                        $customer = new Customer();
                        $properties = array_keys($data['customer']);
                        foreach ($properties as $property) {
                            if (isset($data['customer'][$property]) && !empty($data['customer'][$property]))
                                $customer->$property = $data['customer'][$property];
                        }
                        $customer->customer_group_id = CustomerGroupEnum::CUS_SHP;
                        $customer->code = $agency->code . "_KH_";
                        $customer->save();
                        $customer->code = $agency->code . "_KH_" . $customer->id;
                        $customer->update();
                    }
                } else {
                    $customer = new Customer();
                    $properties = array_keys($data['customer']);
                    foreach ($properties as $property) {
                        if (isset($data['customer'][$property]) && !empty($data['customer'][$property]))
                            $customer->$property = $data['customer'][$property];
                    }
                    $customer->customer_group_id = CustomerGroupEnum::CUS_SHP;
                    $customer->code = $agency->code . "_KH_";
                    $customer->save();
                    $customer->code = $agency->code . "_KH_" . $customer->id;
                    $customer->update();
                }

                if (isset($data['receiver']) && !empty($data['receiver'])) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }
                    } else {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    }
                }
            }
            if (isset($data['containers']) && !empty($data['containers'])) {
                $configSurcharge = Config::find(config('app.config_surcharge'));
                $configInsurance = Config::find(config('app.config_insurance'));
                if (isset($data['containers'][0])
                    && !empty($data['containers'][0])
                    && isset($data['containers'][0]['id'])
                    && !empty($data['containers'][0]['id'])
                ) {
                    $con = $data['containers'][0];
                    $order = Order::find($data['containers'][0]['id']);
                    if ($order->shipping_status <= OrderShippingStatusEnum::SHIPPING_STATUS_STOCK_IN) {
                        $order->customer_id = $customer->id;
                        $order->sender_first_name = $customer->first_name;
                        $order->sender_middle_name = $customer->middle_name;
                        $order->sender_last_name = $customer->last_name;
                        $order->sender_email = $customer->email;
                        $order->sender_phone = $customer->telephone;
                        $order->sender_address = $customer->address_1;
                        $order->sender_country_id = $customer->country_id;
                        $order->sender_province_id = $customer->province_id;
                        $order->sender_city_id = $customer->city_id;
                        $order->sender_post_code = $customer->postal_code;
                        $order->sender_cellphone = $customer->cellphone;

                        $order->receive_last_name = $receiver->last_name;
                        $order->receive_first_name = $receiver->first_name;
                        $order->receiver_middle_name = $receiver->middle_name;
                        $order->receiver_email = $receiver->email;
                        $order->receiver_phone = $receiver->telephone;
                        $order->receiver_address = $receiver->address_1;
                        $order->receiver_country_id = $receiver->country_id;
                        $order->receiver_province_id = $receiver->province_id;
                        $order->receiver_city_id = $receiver->city_id;
                        $order->receiver_ward_id = $receiver->ward_id;
                        $order->receiver_post_code = $receiver->postal_code;
                        $order->receiver_cellphone = $receiver->cellphone;
                        $order->currency_id = $agency->currency_id;
                        $order->dimension_unit_id = $agency->dimension_unit_id;
                        $order->weight_unit_id = $agency->weight_unit_id;
                        $receiverCountry = Country::where('id', $order->receiver_country_id)->first();
                        $order->receiver_country_name = isset($receiverCountry->name) ? $receiverCountry->name : '';
                        $receiverProvince = Province::where('id', $order->receiver_province_id)->first();
                        $order->receiver_province_name = isset($receiverProvince->name) ? $receiverProvince->name : '';
                        $receiverCity = City::where('id', $order->receiver_city_id)->first();
                        $order->receiver_city_name = isset($receiverCity->name) ? $receiverCity->name : '';
                        $receiverWard = Ward::where('id', $order->receiver_ward_id)->first();
                        $order->receiver_ward_name = isset($receiverWard->name) ? $receiverWard->name : '';
                        $order->save();
                        $total_weight = 0;
                        $total_goods = 0;
                        $total_declare_price = 0;
                        $total_discount = 0;
                        $total_surcharge_fee = 0;
                        $total_insurrance_fee = 0;
                        $total_final = 0;
                        $description = "";
                        $product_codes = "";
                        $user_notes = "";

                        foreach ($con['products'] as $product) {
                            if (isset($product['product']) && !empty($product['product'])) {
                                $quantity = 0;
                                if (isset($product['id']) && !empty($product['id'])) {
                                    $orderItem = OrderItem::find($product['id']);
                                } else {
                                    $orderItem = new OrderItem();
                                }
                                $orderItem->name = $product['product']['name'];
                                $orderItem->description = $product['product']['description'];
                                $orderItem->order_id = $order->id;
                                $orderItem->product_id = $product['product_id'];
                                $orderItem->quantity = $product['quantity'];
                                if (!empty($product['user_note'])) {
                                    $orderItem->user_note = $product['user_note'];
                                    $user_notes .= $orderItem->user_note . '; ';
                                }
                                $orderItem->unit_goods_fee = isset($product['product']['sale_price']) && !empty($product['product']['sale_price']) ? $product['product']['sale_price'] : $product['unit_goods_fee'];
                                $total_weight += $orderItem->sub_total_weight = $product['sub_total_weight'];
                                if (isset($product['product']['by_weight']) && $product['product']['by_weight'] == 1) {
                                    $quantity = $product['sub_total_weight'];
                                } else {
                                    $quantity = $product['quantity'];
                                }
                                $total_goods += $orderItem->sub_total_goods = $orderItem->unit_goods_fee * $quantity;
                                $total_declare_price += $orderItem->sub_total_declare_price = $product['sub_total_declare_price'];
                                $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                                $orderItem->per_discount = $discount;
                                $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);

                                $quora = $orderItem->sub_total_declare_price / $quantity;
                                if ($quora > $configSurcharge->quora) {
                                    $total_surcharge_fee += $orderItem->sub_total_surcharge_fee = $orderItem->sub_total_declare_price * ($configSurcharge->percent / 100);
                                } else {
                                    $orderItem->sub_total_surcharge_fee = 0;
                                }

                                if (isset($product['is_insurance']) && !empty($product['is_insurance'])) {
                                    $total_insurrance_fee += $orderItem->sub_total_insurrance_fee = $orderItem->sub_total_declare_price * ($configInsurance->percent / 100);
                                    $orderItem->is_insurance = 1;
                                } else {
                                    $orderItem->is_insurance = 0;
                                    $orderItem->sub_total_insurrance_fee = 0;
                                }
                                $total_final += $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                                $description .= $product['quantity'] . ' ' . $product['product']['name'] . '; ';
                                $product_codes .= $product['product']['code'] . '; ';
                                $orderItem->messure_unit_id = isset($product['product']['agency_unit_id']) && !empty($product['product']['agency_unit_id']) ? $product['product']['agency_unit_id'] : $product['messure_unit_id'];
                                $orderItem->save();
                            }
                        }
                        $orderAfter = Order::find($order->id);
                        $orderAfter->description = $description;
                        $orderAfter->products = $product_codes;
                        $orderAfter->length = $con['length'];
                        $orderAfter->width = $con['width'];
                        $orderAfter->height = $con['height'];
                        $orderAfter->receive_status = $con['receive_status'];
                        $orderAfter->total_weight = $con['total_weight'];
                        $orderAfter->shipping_fee = $con['shipping_fee'];
                        $orderAfter->total_shipping_fee = $orderAfter->total_weight * $orderAfter->shipping_fee;
                        $orderAfter->total_declare_price = $total_declare_price;
                        $orderAfter->total_surcharge_fee = $total_surcharge_fee;
                        $orderAfter->total_insurrance_fee = $total_insurrance_fee;
                        $orderAfter->total_goods_fee = $total_goods;
                        $orderAfter->total_final = $total_final + $orderAfter->total_shipping_fee;
                        $orderAfter->user_note = $user_notes;
                        $orderAfter->total_paid_amount = $con['total_paid_amount'];
                        if (isset($con['last_payment_at']) && !empty($con['last_payment_at'])) {
                            $orderAfter->payment_status = 1;
                            $orderAfter->last_payment_at = ConvertsUtil::dateFormat($con['last_payment_at'], config('app.date_format'));
                        }
                        $orderAfter->total_remain_amount = $orderAfter->total_final - $con['total_paid_amount'] > 0 ? $orderAfter->total_final - $con['total_paid_amount'] : 0;
                        $orderAfter->total_discount = $total_discount;
                        $orderAfter->update();
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    private function convertOrder (Order $order) {
        $order_items = $order->order_items()->where('is_delete', 0)->get();
        $products = [];
        foreach($order_items as $order_item){
            $item = Product::find($order_item->product_id);
            if(empty($item)){
                continue;
            }
            $product['id'] = $order_item->id;
            $product['product_id'] = $order_item->product_id;
            $product['code'] = $order_item->product->code;
            $product['name'] = $order_item->name;
            $product['unit_goods_fee'] = $order_item->unit_goods_fee;
            $product['quantity'] = $order_item->quantity;
            $product['by_weight'] = $item->by_weight;
            $product['per_discount'] = $order_item->per_discount;
            $product['weight'] = $order_item->sub_total_weight;
            $product['declared_value'] = $order_item->sub_total_declare_price;
            $product['surcharge'] = $order_item->sub_total_surcharge_fee;
            $product['is_insurance'] = $order_item->is_insurance == 1 ? true : false;
            $product['insurance'] = $order_item->sub_total_insurrance_fee;
            $product['total'] = $order_item->sub_total_final;
            $product['note'] = !empty($order_item->customer_note) ? $order_item->customer_note : $order_item->user_note;

            $products[] = $product;
        }
        $data = [];
        $data['container'] = array(
            'id' => $order->id,
            'code' => $order->code,
            'length' => $order->length,
            'width' => $order->length,
            'height' => $order->length,
            'shipping_fee'  => $order->shipping_fee,
            'total_weight'  => $order->total_weight,
            'total_shipping_fee'  => $order->total_shipping_fee,
            'total_declared_value' => $order->total_declare_price,
            'total_surcharge' => $order->total_surcharge_fee,
            'total_insurance' => $order->total_insurrance_fee,
            'total' => $order->total_final,
            'coupon_code' => $order->coupon_code,
            'coupon_amount' => !empty($order->coupon_amount) ? $order->coupon_amount : 0,
            'receive_status' => !empty($order->receive_status) ? $order->receive_status : 1,
            'products' => $products
        );

        $data['sender'] = array(
            'code' => isset($order->customer->code) ? $order->customer->code : '',
            'first_name' => $order->sender_first_name,
            'middle_name' => $order->sender_middle_name,
            'last_name' => $order->sender_last_name,
            'telephone' => $order->sender_phone,
            'address_1' => $order->sender_address,
            'country_id' => $order->sender_country_id,
            'province_id' => $order->sender_province_id,
            'city_id' => $order->sender_city_id,
            'postal_code' => $order->sender_post_code,
            'address_2' => $order->sender_address_2,
        );
        $data['receiver'] = array(
            'first_name' => $order->receive_first_name,
            'middle_name' => $order->receiver_middle_name,
            'last_name' => $order->receive_last_name,
            'telephone' => $order->receiver_phone,
            'cellphone' => $order->receiver_cellphone,
            'address_1' => $order->receiver_address,
            'country_id' => $order->receiver_country_id,
            'province_id' => $order->receiver_province_id,
            'city_id' => $order->receiver_city_id,
            'ward_id' => $order->receiver_ward_id,
            'postal_code' => $order->receiver_post_code,
            'address_2' => $order->receiver_address_2,
        );

        return $data;
    }
}
