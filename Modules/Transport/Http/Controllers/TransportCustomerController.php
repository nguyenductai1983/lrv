<?php

namespace Modules\Transport\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ExpressRequest;
use App\Http\Requests\PaymentRequest;
use App\Components\ConvertsUtil;
use App\Components\LanguageUtil;
use App\Services\EshiperService;
use App\Services\PointService;
use App\Services\OrderService;
use App\Enum\TransportStatusEnum;
use App\Enum\CurrencyEnum;
use App\Enum\WeightUnitEnum;
use App\Enum\DimensionUnitEnum;
use App\Enum\OrderStatusEnum;
use App\Enum\EshiperEnum;
use App\Enum\TransactionTypeEnum;
use App\Enum\OrderPaymentStatusEnum;
use App\Enum\PointEnum;
use App\Order;
use App\OrderItem;
use App\Transport;
use App\Config;
use App\Country;
use App\Province;
use App\City;
use App\Warehouse;
use App\Customer;
use App\Transaction;
use App\Address;
use App\Product;
use App\PaymentMethod;
//use App\File;
use App\Notification;
//use App\Exports\ExportQuery;
use Exception;
use Illuminate\Support\Facades\Log;
class TransportCustomerController extends Controller
{
    /**
     * Assign data
     */
    private $data;

    public function __construct()
    {
        $this->data = [
            'menu' => '4.11'
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Transport::where([]);
        $query->whereNull('agency_id');
        if (!empty($request->query('from_date'))) {
            $query->where('tbl_transport.created_at', '>', ConvertsUtil::dateFormat($request->query('from_date') . " 00:00:00"));
        }
        if (!empty($request->query('to_date'))) {
            $query->where('tbl_transport.created_at', '<', ConvertsUtil::dateFormat($request->query('to_date') . " 23:59:59"));
        }
        if (!empty($request->query('code'))) {
            $query->where('tbl_transport.code', '=', $request->query('code'));
        }

        $query->select('tbl_transport.id','tbl_transport.code','eshiper_shipping_status','payment_status','customer_id','eshipper_label',
                'sender_first_name','sender_middle_name','sender_last_name','transport_status',
                'receive_first_name','receiver_middle_name','receive_last_name','tbl_transport.created_at',
                'total_weight','total_final','total_discount','total_paid_amount','carrer_quote',
                'eshiper_order_id','receiver_address','receiver_phone','nuoc_nhan.name as countries_name',
                'tinh_nhan.name as provinces_name','thanh_pho_nhan.name as cities_name'
                );
         // lấy thông tin nước nhận
        $query->leftJoin('countries as nuoc_nhan', 'tbl_transport.receiver_country_id', '=', 'nuoc_nhan.id' );
             // lấy thông tin tỉnh nhận
        $query->leftJoin('provinces as tinh_nhan', 'tbl_transport.receiver_province_id', '=', 'tinh_nhan.id' );
            // lấy thông tin thành phố nhận
        $query->leftJoin('cities as thanh_pho_nhan', 'tbl_transport.receiver_city_id', '=', 'thanh_pho_nhan.id' );

        $this->data['transports'] = $query->orderBy('id', 'desc')->paginate(config('app.items_per_page'));
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::quote.index', $this->data);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $transport = Transport::where('id', '=', $id)->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        $data = $this->convertTransport($transport);
        return response([
            'transport' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, Request $resquest)
    {
        $notification_id = $resquest->Input('notification_id', 0);
        if($notification_id > 0){
            Notification::where('id', $notification_id)->update(['is_read' => 1]);
        }
        $transport = Transport::where('id', '=', $id)->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        $this->data['transport'] = $transport;
        $this->data['configSurcharge'] = Config::find(config('app.config_surcharge'));
        $this->data['configInsurance'] = Config::find(config('app.config_insurance'));
        $this->data['caPriority'] = config('app.ca_priority');
        $this->data['vnPriority'] = config('app.vn_priority');
        $this->data['caVnPriorityFee'] = config('app.ca_priority_vn_priority_fee');
        $this->data['caVnPriorityDefaultFee'] = config('app.ca_priority_vn_default_fee');
        $this->data['caVnDefaultPriorityFee'] = config('app.ca_default_vn_priority_fee');
        $this->data['caVnDefaultFee'] = config('app.ca_default_vn_default_fee');
        $this->data['discountLevel'] = config('app.discount_level');
        $this->data['language'] = LanguageUtil::getKeyJavascript();

        return view('transport::quote.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $transport = Transport::where('id', '=', $request->route('id'))->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();
            $data = $request->all();
            $sender = $data['sender'];
            $receiver = $data['receiver'];
            $customer = Customer::where('id', '=', $transport->customer_id)->first();
            // if (!$customer) {
            //     return response('Not Found', 404);
            // }
            if (isset($data['containers']) && !empty($data['containers'])) {
                $configSurcharge = Config::find(config('app.config_surcharge'));
                $configInsurance = Config::find(config('app.config_insurance'));

                $transport->sender_first_name = $sender['first_name'];
                $transport->sender_middle_name = $sender['middle_name'];
                $transport->sender_last_name = $sender['last_name'];
                $transport->sender_phone = $sender['telephone'];
                $transport->sender_address = $sender['address_1'];
                $transport->sender_address_2 = !empty($sender['address_2']) ? $sender['address_2'] : '' ;
                $transport->sender_email = $sender['email'];
                $transport->sender_post_code = $sender['postal_code'];
                $transport->sender_city_id = $sender['city_id'];
                $transport->sender_province_id = $sender['province_id'];
                $transport->sender_country_id = $sender['country_id'];
                if(!empty($sender['shipping_date'])){
                    $transport->shipping_date = ConvertsUtil::strToDateFormat($sender['shipping_date']);
                }
                if (isset($data['receiver']) && !empty($data['receiver']) && isset($customer->id)) {
                    if (isset($data['receiver']['id']) && !empty($data['receiver']['id'])) {
                        $receiver = Address::where('id', $data['receiver']['id'])->first();
                        if (empty($receiver)) {
                            $receiver = new Address();
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->save();
                        }else{
                            $properties = array_keys($data['receiver']);
                            foreach ($properties as $property) {
                                if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                    $receiver->$property = $data['receiver'][$property];
                            }
                            $receiver->update();
                        }
                    } else {
                        $receiver = new Address();
                        $properties = array_keys($data['receiver']);
                        foreach ($properties as $property) {
                            if (isset($data['receiver'][$property]) && !empty($data['receiver'][$property]))
                                $receiver->$property = $data['receiver'][$property];
                        }
                        $receiver->customer_id = $customer->id;
                        $receiver->save();
                    }
                }
                $transport->receive_first_name = $receiver['first_name'];
                $transport->receiver_middle_name = $receiver['middle_name'];
                $transport->receive_last_name = $receiver['last_name'];
                $transport->receiver_phone = $receiver['telephone'];
                $transport->receiver_cellphone = $receiver['cellphone'];
                $transport->receiver_address = $receiver['address_1'];
                $transport->receiver_address_2 = !empty($receiver['address_2']) ? $receiver['address_2'] : null ;

                $transport->receiver_post_code = $receiver['postal_code'];
                $transport->receiver_ward_id = $receiver['ward_id'];
                $transport->receiver_city_id = $receiver['city_id'];
                $transport->receiver_province_id = $receiver['province_id'];
                $transport->receiver_country_id = $receiver['country_id'];
                $transport->carrer_quotes = json_encode($data['quotes']);
                $transport->carrer_quote = !empty($data['quote']) ? json_encode($data['quote']) : '';
                $transport->discount_international_fee = isset($data['discount_international_fee']) && $data['discount_international_fee'] > 0 ? $data['discount_international_fee']  : 0;
                $transport->foreign_fee = isset($data['total_shipping_local']) && $data['total_shipping_local'] > 0 ? $data['total_shipping_local']  : 0;
                $transport->pickup = json_encode($data['pickup']);
                $transport->sender_at = date('Y-m-d H:i:s');
                $transport->warehouse_id = $data['warehouse_id'];

                if(!empty($data['quote']['transitDays']) && !empty($transport->shipping_date)){
                    $transport->receiver_at = ConvertsUtil::dateAddDay($transport->shipping_date, $data['quote']['transitDays'], 'Y-m-d');
                    $transport->shipping_from_at = ConvertsUtil::dateAddDay($transport->receiver_at, 7);
                    $transport->shipping_to_at = ConvertsUtil::dateAddDay($transport->receiver_at, 14);
                }
                $transport->update();

                $transport_total_weight = 0;
                $transport_total_declare_price = 0;
                $transport_total_surcharge_fee = 0;
                $transport_total_insurrance_fee = 0;
                $transport_total_shipping_fee = 0;
                $transport_total_final = 0;

                $count_container = 1;
                foreach ($data['containers'] as $con) {
                    if(isset($con['id']) && !empty($con['id'])){
                        $order = Order::find($con['id']);
                        if(empty($order)){
                            return response('Internal Server Error', 500);
                        }
                    }else{
                        $order = new Order();
                    }
                    if(isset($customer->id))
                    {
                        $order->customer_id = $customer->id;
                    }
                    $order->currency_id = CurrencyEnum::CAD;
                    $order->dimension_unit_id = DimensionUnitEnum::INCH;
                    $order->weight_unit_id = WeightUnitEnum::LBS;
                    $order->transport_id = $transport->id;

                    $order->sender_first_name = $sender['first_name'];
                    $order->sender_middle_name = $sender['middle_name'];
                    $order->sender_last_name = $sender['last_name'];
                    $order->sender_phone = $sender['telephone'];
                    $order->sender_address = $sender['address_1'];
                    $order->sender_address_2 = !empty($sender['address_2']) ? $sender['address_2']: null;
                    $order->sender_email = $sender['email'];
                    $order->sender_country_id = $sender['country_id'];
                    $order->sender_province_id = $sender['province_id'];
                    $order->sender_city_id = $sender['city_id'];
                    $order->sender_post_code = $sender['postal_code'];

                    $order->receive_last_name = $receiver['last_name'];
                    $order->receive_first_name = $receiver['first_name'];
                    $order->receiver_middle_name = $receiver['middle_name'];
                    $order->receiver_phone = $receiver['telephone'];
                    $order->receiver_address = $receiver['address_1'];
                    $transport->receiver_address_2 = !empty($receiver['address_2']) ?$receiver['address_2']:null;
                    $order->receiver_country_id = $receiver['country_id'];
                    $order->receiver_province_id = $receiver['province_id'];
                    $order->receiver_ward_id = $receiver['ward_id'];
                    $order->receiver_city_id = $receiver['city_id'];
                    $order->receiver_cellphone = $receiver['cellphone'];
                    $order->receiver_post_code = $receiver['postal_code'];

                    $order->save();

                    $total_weight = 0;
                    $total_goods = 0;
                    $total_declare_price = 0;
                    $total_surcharge_fee = 0;
                    $total_insurrance_fee = 0;
                    $total_discount = 0;
                    $total_final = 0;
                    $description = "";
                    $products_code = "";
                    $user_note = "";
                    $order_item_old = OrderItem::where('order_id','=',$order->id)->get();
                    foreach ($con['products'] as $product) {
                        $quantity = 0;
                        $source = Product::where('id', '=', $product['product_id'])->first();
                        if(empty($source)){
                            continue;
                        }
                        if (isset($product['id']) && !empty($product['id'])) {
                            $orderItem = OrderItem::find($product['id']);
                            $order_item_old = $order_item_old->except([$product['id']]);
                        } else {
                            $orderItem = new OrderItem();
                        }
                        $orderItem->name = $product['name'];
                        $orderItem->description = $source->description;
                        $orderItem->order_id = $order->id;
                        $orderItem->product_id = $product['product_id'];
                        $orderItem->quantity = $product['quantity'];
                        if (!empty($product['note'])) {
                            $orderItem->user_note = $product['note'];
                            $user_note .= $orderItem->user_note . "; ";
                        }
                        $orderItem->unit_goods_fee = $source->price;
                        $total_weight += $orderItem->sub_total_weight = $product['weight'];
                        if (isset($product['by_weight']) && $product['by_weight'] == 1) {
                            $quantity = $product['weight'];
                        } else {
                            $quantity = $product['quantity'];
                        }
                        $total_goods += $orderItem->sub_total_goods = $source->price * $quantity;
                        $total_declare_price += $orderItem->sub_total_declare_price = $product['declared_value'];
                        $discount = isset($product['per_discount']) && !empty($product['per_discount']) ? $product['per_discount'] : 0;
                        $orderItem->per_discount = $discount;
                        $total_discount += $orderItem->sub_total_discount = $orderItem->sub_total_goods * ($discount / 100);
                        $quota = $product['declared_value'] / $quantity;
                        if ($quota > $configSurcharge->quota) {
                            $total_surcharge_fee += $orderItem->sub_total_surcharge_fee = $product['declared_value'] * ($configSurcharge->percent / 100);
                        } else {
                            $orderItem->sub_total_surcharge_fee = 0;
                        }
                        if (isset($product['is_insurance']) && $product['is_insurance'] == 1) {
                            $total_insurrance_fee += $orderItem->sub_total_insurrance_fee = $product['declared_value'] * ($configInsurance->percent / 100);
                            $orderItem->is_insurance = 1;
                        } else {
                            $orderItem->sub_total_insurrance_fee = 0;
                        }
                        $total_final += $orderItem->sub_total_final = ($orderItem->sub_total_goods + $orderItem->sub_total_surcharge_fee + $orderItem->sub_total_insurrance_fee) - $orderItem->sub_total_discount;
                        $description .= $product['quantity'] . ' ' . $product['name'] . '; ';
                        $products_code .= $product['code'] . '; ';
                        $orderItem->messure_unit_id = $source->messure_unit_id;
                        $orderItem->save();
                    }
                    foreach ($order_item_old as $player) {
                        $player->is_delete = 1;
                            $player->save();
                    }
                    // kiểm tra danh sách sản phẩm mới có nhiều hơn danh sách hiện tại
                    $order->total_discount = $total_discount;
                    $order->description = $description;
                    $order->user_note = $user_note;
                    $order->products = $products_code;
                    $order->length = $con['length'];
                    $order->width = $con['width'];
                    $order->height = $con['height'];
                    $order->total_weight = $con['total_weight'];
                    $order->shipping_fee = $con['shipping_fee'];
                    $order->total_shipping_fee = $order->total_weight * $order->shipping_fee;
                    $order->total_declare_price = $total_declare_price;
                    $order->total_surcharge_fee = $total_surcharge_fee;
                    $order->total_insurrance_fee = $total_insurrance_fee;
                    $order->total_goods_fee = $total_goods;
                    $totalFee = $order->total_shipping_fee + $order->total_surcharge_fee + $order->total_insurrance_fee + $order->total_goods_fee - $order->total_discount;
                    $minFee = OrderService::getMinFee(true, $order->receiver_province_id);
                    if($minFee > $totalFee){
                        $order->total_final = $minFee;
                    }else{
                        $order->total_final = $totalFee;
                    }
                    $order->code =  $transport->code .  $count_container . count($data['containers']);
                    $order->box_number = $count_container;
                    $order->update();
                    $count_container++;

                    $transport_total_weight += $order->total_weight;
                    $transport_total_declare_price += $order->total_declare_price;
                    $transport_total_surcharge_fee += $order->total_surcharge_fee;
                    $transport_total_insurrance_fee += $order->total_insurrance_fee;
                    $transport_total_shipping_fee += $order->total_shipping_fee;
                    $transport_total_final += $order->total_final;
                }
                $transport->total_weight = $transport_total_weight;
                $transport->total_shipping_fee = $transport_total_shipping_fee;
                $transport->total_declare_price = $transport_total_declare_price;
                $transport->total_surcharge_fee = $transport_total_surcharge_fee;
                $transport->total_insurrance_fee = $transport_total_insurrance_fee;
                $transport->total_goods_fee = 0;
                $transport->international_fee = $transport_total_final;
                $transport->total_final = $transport->international_fee + $transport->foreign_fee;

                $transport->update();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function approve(Request $request){
        $transport = Transport::where('id', '=', $request->route('id'))->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        $data = $request->all();
        $total_shipping_local = isset($data['total_shipping_local']) && $data['total_shipping_local'] > 0 ? $data['total_shipping_local']  : 0;
        $receive_status = $data['receive_status'];
        $status_shipper=null;
        try{
            DB::beginTransaction();
            if(!empty($transport->carrer_quote)){
                $params = $this->eshipBuildParams($transport);
                $eshipRespose = EshiperService::createOrderByPackage($params);
                if ($eshipRespose['success']) {
                    $transport->eshiper_shipping_status = $eshipRespose['data']['statusId'];
                    $transport->eshiper_order_id = $eshipRespose['data']['orderId'];
                    $transport->eshiper_order = json_encode($eshipRespose['data']);
                    $transport->eshipper_label = $eshipRespose['data']['label'];
                     $status_shipper=1;
                }else{
                    DB::rollBack();
                    $this-> Log_file($transport->code.' Eshipper: '.$eshipRespose['message']);
                    return response($eshipRespose['message'], 500);
                }
            }
            $transport->discount_international_fee = isset($data['discount_international_fee']) && $data['discount_international_fee'] > 0 ? $data['discount_international_fee']  : 0;
            $transport->foreign_fee = $total_shipping_local;
            $transport->total_final = $transport->international_fee + $transport->foreign_fee;
            $transport->transport_status = TransportStatusEnum::STATUS_PROCESSING;
            $transport->receive_status = $receive_status;
            $transport->update();

            Order::where('transport_id', '=', $transport->id)->where('order_status', '=', OrderStatusEnum::STATUS_NEW)->update(['order_status' => OrderStatusEnum::STATUS_PROCESSING,'eshiper_shipping_status'=>$status_shipper, 'receive_status' => $receive_status]);

            $payment_methods = PaymentMethod::where(['type' => 2, 'is_active' => 1, 'is_deleted' => 0])->get();
            Mail::send('email.approved-transport-quote', ['transport' => $transport, 'payment_methods' => $payment_methods], function ($message) use ($transport) {
                $message->to($transport->sender_email, $transport->sender_email)->subject(__('email.approved-transport-quote'));
            });
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            return response('Internal Server Error', 500);
        }
        return response('OK', 200)->header('Content-Type', 'text/plain');
    }
    private function Log_file($mess)
    {
        $user = Auth::user();
        $code_user=$user->code;
       Log::error($code_user.' '.$mess);
        return;
    }
    public function cancelEshipper(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response($validator->errors()->first(), 500);
        }
        $transport = Transport::find($request->get('id'));
        if(empty($transport)){
            return response(__('message.err_sys'), 500);
        }
        if(!empty($transport->eshiper_order_id)){
            $eshiperResponse = EshiperService::cancelOrder($transport->eshiper_order_id);
            if($eshiperResponse['success'] && isset($eshiperResponse['data']['orderStatus']) && $eshiperResponse['data']['orderStatus'] == EshiperEnum::CANCELLED){
                Order::where('transport_id', '=', $transport->id)->update(['order_status' => OrderStatusEnum::STATUS_CANCEL]);
                $transport->eshiper_shipping_status = $eshiperResponse['data']['orderStatus'];
                $transport->transport_status = TransportStatusEnum::STATUS_CANCEL;
                $transport->update();
            }else{
                return response($eshiperResponse['message'], 500);
            }
        }else{
            Order::where('transport_id', '=', $transport->id)->update(['order_status' => OrderStatusEnum::STATUS_CANCEL]);

            $transport->transport_status = TransportStatusEnum::STATUS_CANCEL;
            $transport->update();
        }

        return response("OK");
    }

    public function payment(PaymentRequest $request) {
        $data = $request->all();

        $transport = Transport::where('id', '=', $data['transport']['id'])->first();
        if (!$transport) {
            return response('Not Found', 404);
        }
        try {
            DB::beginTransaction();

            if($data['transport']['last_paid_amount'] > 0 && !empty($data['transport']['last_date_pay'])){
                $transaction = new Transaction();
                $transaction->customer_id = $transport->customer_id;
                $transaction->transport_id = $transport->id;
                $transaction->currency_id = $transport->currency_id;
                $transaction->code = "PT_" . $transport->code;
                $transaction->transaction_at = ConvertsUtil::dateFormat($data['transport']['last_date_pay'], config('app.date_format'));
                $transaction->type = TransactionTypeEnum::RECEIPT;
                $transaction->credit_amount = $data['transport']['last_paid_amount'];
                $transaction->goods_code = $transport->code;
                $transaction->save();

                $transport->payment_method = $data['transport']['pay_method'];
                $transport->total_paid_amount += $data['transport']['last_paid_amount'];
                $transport->total_remain_amount = $transport->total_final - $transport->total_paid_amount > 0 ? $transport->total_final - $transport->total_paid_amount : 0;
                if($transport->total_remain_amount == 0){
                    $transport->payment_status = OrderPaymentStatusEnum::FULL_PAID;
                }else{
                    $transport->payment_status = OrderPaymentStatusEnum::PART_PAID;
                }
                $transport->last_payment_at = ConvertsUtil::dateFormat($data['transport']['last_date_pay'], config('app.date_format'));

                $transport->update();

                Order::where('transport_id', '=', $transport->id)->update(['payment_status' => $transport->payment_status, 'last_payment_at' => $transport->last_payment_at]);

                PointService::addPointToCustomer($transport->customer_id, PointEnum::TYPE_CONFIG_CUSTOMER);

                PointService::addPointTransport($transport, PointEnum::TYPE_CONFIG_ORDER);
            }
            Mail::send('email.payment-transport-quote', ['transport' => $transport, 'amount' => $transport->total_paid_amount, 'last_payment_at' => $transport->last_payment_at], function ($message) use ($transport) {
                    $message->to($transport->sender_email, $transport->sender_email)->subject(__('email.payment-express-quote'));
                });
                DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response(['status'=> 200,"success" => false, "message" => $e->getMessage(), "data" => []]);
            //  return response('Internal Server Error', 500);
        }

        return response('OK', 200)->header('Content-Type', 'text/plain');
    }

    public function getShippingEshipper(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response('Not Found', 404);
        }
        $transport = Transport::find($request->get('id'));
        if(empty($transport)){
            return response('Not Found', 404);
        }
        if(!empty($transport->eshiper_order_id)){
            $eshiperResponse = EshiperService::getOrder($transport->eshiper_order_id);
            if(!$eshiperResponse['success']){
                return response($eshiperResponse['message'], 500);
            }
            $transport->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
            if($transport->eshiper_shipping_status == EshiperEnum::DELIVERED){
                $transport->eshiper_charge_order = json_encode($eshiperResponse['data']);
                $transport->charge_foreign_fee = $eshiperResponse['data']['totalCharge'];
                if($transport->discount_international_fee > 0){
                    $transport->charge_foreign_fee = $transport->charge_foreign_fee - $transport->discount_international_fee;
                }
                $transport->charge_total_final = $transport->charge_foreign_fee + $transport->international_fee;
            }
            $transport->update();

            $data = $this->convertTransport($transport);
            return response([
                'transport' => $data
            ]);
        }
        return response('Not Found', 404);
    }

    public function viewHistoryEshipper(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response(["success" => false, "message" => $validator->errors()->first(), []]);
        }
        $transport = Transport::find($request->get('id'));
        if(empty($transport)){
            return response(["success" => false, "message" => __('message.err_sys'), []]);
        }
        if(!empty($transport->eshiper_order_id)){
            $eshiperResponse = EshiperService::getOrder($transport->eshiper_order_id);
            if($eshiperResponse['success']){
                $transport->eshiper_shipping_status = $eshiperResponse['data']['statusId'];
                if($transport->eshiper_shipping_status == EshiperEnum::DELIVERED){
                    $transport->eshiper_charge_order = json_encode($eshiperResponse['data']);
                    $transport->charge_foreign_fee = $eshiperResponse['data']['totalCharge'];
                    if($transport->discount_international_fee > 0){
                        $transport->charge_foreign_fee = $transport->charge_foreign_fee - $transport->discount_international_fee;
                    }
                    $transport->charge_total_final = $transport->charge_foreign_fee + $transport->international_fee;
                }
                $transport->update();
            }else{
                return response(["success" => false, "message" => $eshiperResponse['message'], []]);
            }
            return response(["success" => true, "message" => __('message.get_data_success'), "data" => $eshiperResponse['data']['history']]);
        }
        return response(["success" => true, "message" => __('message.get_data_success'), "data" => []]);
    }

    public function getQuotes(ExpressRequest $request) {
        $params = $this->buildQuoteParams($request->all());
        $result = EshiperService::getQuoteEshiper($params);
        return response($result);
    }

    public function getPdf($id) {
        $transport = Transport::where('id', '=', $id)->first();
        if (empty($transport) || empty($transport->eshipper_label)) {
            return response('Not Found', 404);
        }
        $base64Pdf = $transport->eshipper_label;
        $pdf = base64_decode($base64Pdf);

        return (new Response($pdf, 200))->header('ContentType', 'application/pdf');
    }

    private function eshipBuildParams(Transport $transport) {
        $countries = Country::where("is_active", 1)->pluck('code','id');
        $provinces = Province::where("is_active", 1)->pluck('code','id');
        $cities = City::where("is_active", 1)->pluck('name','id');
        $warehouse = Warehouse::where("id", $transport->warehouse_id)->first();
        $carrer_quote = json_decode($transport->carrer_quote);

        $params['serviceId'] = $carrer_quote->serviceId;
        $params['scheduledShipDate'] = !empty($transport->shipping_date) ? ConvertsUtil::strToDateFormat($transport->shipping_date) : '';
        $params['code'] = $transport->code;
        $params['addressFrom']['company'] = $transport->sender_company_name;
        $params['addressFrom']['address1'] = $transport->sender_address;
        $params['addressFrom']['city'] = $cities[$transport->sender_city_id];
        $params['addressFrom']['state'] = $provinces[$transport->sender_province_id];
        $params['addressFrom']['zip'] = str_replace(" ","",$transport->sender_post_code);
        $params['addressFrom']['country'] = $countries[$transport->sender_country_id];
        $params['addressFrom']['phone'] = $transport->sender_phone;
        $params['addressFrom']['attention'] = $transport->sender_company_name;
        $params['addressFrom']['email'] = $transport->sender_email;
        $params['addressFrom']['residential'] = "true";

        $params['addressTo']['company'] = $warehouse->contact_person;
        $params['addressTo']['address1'] = $warehouse->address;
        $params['addressTo']['city'] = $cities[$warehouse->city_id];
        $params['addressTo']['state'] = $provinces[$warehouse->province_id];
        $params['addressTo']['zip'] = str_replace(" ","",$warehouse->post_code);
        $params['addressTo']['country'] = $countries[$warehouse->country_id];
        $params['addressTo']['phone'] = $warehouse->telephone;
        $params['addressTo']['attention'] = $warehouse->contact_person;
        $params['addressTo']['email'] = $warehouse->email;
        $pickup = json_decode($transport->pickup, true);
        if(isset($pickup['is_schedule']) && $pickup['is_schedule'] == EshiperEnum::IS_PICKUP){
            $params['pickup'] = $pickup;
            $params['pickup']['pickup_date'] = ConvertsUtil::strToDateFormat($params['pickup']['date_time']);
            $params['pickup']['pickup_time'] = $params['pickup']['start_hour_time'] .':'. $params['pickup']['start_minute_time'];
            $params['pickup']['closing_time'] = $params['pickup']['closing_hour_time'] .':'. $params['pickup']['closing_minute_time'];
        }

        $params['packages'] = [];
        if(isset($transport->orders) && !empty($transport->orders)){
            foreach($transport->orders as $order){
                $package['length'] = $order->length;
                $package['width'] = $order->width;
                $package['height'] = $order->height;
                $package['total_weight'] = $order->total_weight;
                $package['total_insurrance_fee'] = $order->total_insurrance_fee;

                $params['packages'][] = $package;
            }
        }
        return $params;
    }

    private function buildQuoteParams($request) {
        $warehouse = Warehouse::where("id", $request['warehouse_id'])->first();
        $countries = Country::where("is_active", 1)->pluck('code','id');
        $provinces = Province::where("is_active", 1)->pluck('code','id');
        $cities = City::where("is_active", 1)->pluck('name','id');
        $params['typeAttrs'] = ['serviceId' => 0, 'stackable' => true ,'holdForPickupRequired' => true];
        $params['addressFrom']['company'] = $request['sender']['last_name'] . ' ' . $request['sender']['middle_name'] . ' ' . $request['sender']['first_name'];
        $params['addressFrom']['address_1'] = $request['sender']['address_1'];
        $params['addressFrom']['email'] = $request['sender']['email'];
        $params['addressFrom']['postal_code'] = $request['sender']['postal_code'];
        $params['addressFrom']['telephone'] = $request['sender']['telephone'];
        $params['addressFrom']['state'] = isset($provinces[ $request['sender']['province_id']]) ? $provinces[ $request['sender']['province_id']] : '';
        $params['addressFrom']['country'] = isset($countries[ $request['sender']['country_id']]) ? $countries[ $request['sender']['country_id']] : '';
        $params['addressFrom']['city'] = isset($cities[ $request['sender']['city_id']]) ? $cities[ $request['sender']['city_id']] : '';

        $params['addressTo']['company'] = $warehouse->contact_person;
        $params['addressTo']['address_1'] = $warehouse->address;
        $params['addressTo']['postal_code'] = $warehouse->post_code;
        $params['addressTo']['telephone'] = $warehouse->telephone;
        $params['addressTo']['state'] = isset($provinces[$warehouse->province_id]) ? $provinces[$warehouse->province_id] : '';
        $params['addressTo']['country'] = isset($countries[$warehouse->country_id]) ? $countries[$warehouse->country_id] : '';
        $params['addressTo']['city'] = isset($cities[$warehouse->city_id]) ? $cities[$warehouse->city_id] : '';

        $params['packages']['type'] = 3;
        $params['packages']['dimType'] = 3;
        $params['packages']['boxes'] = [];
        $quantity = 0;
        foreach($request['containers'] as $container){
            $boxes = [];
            $boxes['length'] = $container['length'];
            $boxes['width'] = $container['width'];
            $boxes['height'] = $container['height'];
            $boxes['weight'] = $container['total_weight'];
            $boxes['insuranceAmount'] = $container['total_insurance'];

            $params['packages']['boxes'][] = $boxes;
            $quantity += 1;
        }
        $params['packages']['quantity'] = $quantity;
        return $params;
    }

    private function convertTransport (Transport $transport) {
        $orders = $transport->orders()->get();
        $container = [];
        foreach ($orders as $order) {
            $order_items = $order->order_items()->where('is_delete', 0)->get();
            $products = [];
            foreach($order_items as $order_item){
                $product['id'] = $order_item->id;
                $product['product_id'] = $order_item->product_id;
                $product['by_weight'] = $order_item->product->by_weight;
                $product['code'] = $order_item->product->code;
                $product['name'] = $order_item->name;
                $product['quantity'] = $order_item->quantity;
                $product['weight'] = $order_item->sub_total_weight;
                $product['unit_goods_fee'] = $order_item->unit_goods_fee;
                $product['unit'] = isset($order_item->messure->code) ? $order_item->messure->code : null;
                $product['declared_value'] = $order_item->sub_total_declare_price;
                $product['surcharge'] = $order_item->sub_total_surcharge_fee;
                $product['is_insurance'] = $order_item->is_insurance == 1 ? true : false;
                $product['insurance'] = $order_item->sub_total_insurrance_fee;
                $product['per_discount'] = $order_item->per_discount;
                $product['total'] = $order_item->sub_total_final;
                $product['note'] = $order_item->user_note;

                $products[] = $product;
            }
            $container[] = array(
                'id' => $order->id,
                'length' => $order->length,
                'width' => $order->width,
                'height' => $order->height,
                'shipping_fee'  => $order->shipping_fee,
                'total_weight'  => $order->total_weight,
                'total_shipping_fee'  => $order->total_shipping_fee,
                'total_declared_value' => $order->total_declare_price,
                'total_surcharge' => $order->total_surcharge_fee,
                'total_insurance' => $order->total_insurrance_fee,
                'total' => $order->total_final,
                'coupon_code' => $order->coupon_code,
                'coupon_amount' => !empty($order->coupon_amount) ? $order->coupon_amount : 0,
                'products' => $products
            );
        }
        $data = [];
        $data['sender'] = array(
            'first_name' => $transport->sender_first_name,
            'middle_name' => $transport->sender_middle_name,
            'last_name' => $transport->sender_last_name,
            'telephone' => $transport->sender_phone,
            'address_1' => $transport->sender_address,
            'country_id' => $transport->sender_country_id,
            'province_id' => $transport->sender_province_id,
            'city_id' => $transport->sender_city_id,
            'postal_code' => $transport->sender_post_code,
            'email' => $transport->sender_email,
            'shipping_date' => !empty($transport->shipping_date) ? date(config('app.date_format'), strtotime($transport->shipping_date)) : '',
        );
        $data['customer'] = Customer::where('id', '=', $transport->customer_id)->with(['image1', 'image2', 'image3'])->first();
        $data['receiver'] = array(
            'first_name' => $transport->receive_first_name,
            'middle_name' => $transport->receiver_middle_name,
            'last_name' => $transport->receive_last_name,
            'telephone' => $transport->receiver_phone,
            'cellphone' => $transport->receiver_cellphone,
            'address_1' => $transport->receiver_address,
            'country_id' => $transport->receiver_country_id,
            'province_id' => $transport->receiver_province_id,
            'city_id' => $transport->receiver_city_id,
            'ward_id' => $transport->receiver_ward_id,
            'postal_code' => $transport->receiver_post_code,
            'address_2' => $transport->receiver_address_2,
        );
        $data['id'] = $transport->id;
        $data['pickup'] = json_decode($transport->pickup, true);
        $data['date_sender'] = $transport->shipping_date;
        $data['date_receiver'] = $transport->receiver_at;
        $data['date_from_shipping'] = $transport->shipping_from_at;
        $data['date_to_shipping'] = $transport->shipping_to_at;
        $data['discount_international_fee'] = floatval($transport->discount_international_fee);
        $data['total_shipping_local'] = $transport->foreign_fee;
        $data['total_shipping_international'] = $transport->international_fee;
        $data['total_shipping'] = $transport->total_final;
        $data['quote'] = json_decode($transport->carrer_quote);
        $data['quotes'] = json_decode($transport->carrer_quotes);
        $data['transport_status'] = $transport->transport_status;
        $data['eshiper_shipping_status'] = $transport->eshiper_shipping_status;
        $data['eshiper_charge_order'] = json_decode($transport->eshiper_charge_order);
        $data['eshiper_order_id'] = $transport->eshiper_order_id;
        $data['eshiper_status_name'] = $transport->eshipper_status_name;
        $data['payment_status'] = $transport->payment_status_name;
        $data['last_payment_at'] = $transport->last_payment_at;
        $data['customer_note'] = $transport->customer_note;
        $data['charge_foreign_fee'] = !empty($transport->charge_foreign_fee) ? $transport->charge_foreign_fee : 0;
        if(empty($data['charge_foreign_fee'])){
            $data['charge_foreign_fee'] = isset($data['quote']->totalCharge) && $data['quote']->totalCharge > 0 ? $data['quote']->totalCharge : 0;
            if($transport->discount_international_fee > 0){
                $data['charge_foreign_fee'] = $data['charge_foreign_fee'] - $transport->discount_international_fee;
            }
        }
        $data['charge_total_final'] = !empty($transport->charge_total_final) ? $transport->charge_total_final : 0;
        if(empty($data['charge_total_final'])){
            $data['charge_total_final'] = $data['charge_foreign_fee'] + $transport->international_fee;
        }
        $data['total_paid_amount'] = $transport->total_paid_amount;
        $data['total_remain_amount'] = $transport->total_remain_amount;
        $data['warehouse_id'] = $transport->warehouse_id;
        $data['receive_status'] = !empty($transport->receive_status) ? $transport->receive_status : 1;
        $data['containers'] = $container;

        return $data;
    }
}
