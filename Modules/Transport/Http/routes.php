<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-quote-order'], 'prefix' => 'admin/transport/quote/order', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'TransportOrderController@index')->name('transport.quote.order.index');
    Route::get('/{id}', 'TransportOrderController@show')->name('transport.quote.order.show');
    Route::post('/get-carrer', 'TransportOrderController@getQuotes');
    Route::post('/{id}/approve', 'TransportOrderController@approve');
    Route::put('/{id}/update', 'TransportOrderController@update')->name('transport.quote.order.update');
    Route::get('/{id}/edit', 'TransportOrderController@edit')->name('transport.quote.order.edit');
});
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-quote-order'], 'prefix' => 'admin/transportquote/order', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/export', 'TransportOrderController@exportExcel')->name('transport.quote.order.export');
});
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-quote'], 'prefix' => 'admin/transports/quote', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'TransportCustomerController@index')->name('transport.quote.index');
    Route::get('/{id}', 'TransportCustomerController@show')->name('transport.quote.show');
    Route::post('/get-carrer', 'TransportCustomerController@getQuotes');
    Route::post('/{id}/approve', 'TransportCustomerController@approve');
    Route::put('/{id}/update', 'TransportCustomerController@update')->name('transport.quote.update');
    Route::get('/{id}/edit', 'TransportCustomerController@edit')->name('transport.quote.edit');
    Route::post('/cancel-eshipper', 'TransportCustomerController@cancelEshipper');
    Route::post('/view-history-eshipper', 'TransportCustomerController@viewHistoryEshipper');
    Route::post('/get-shipping-eshipper', 'TransportCustomerController@getShippingEshipper');
    Route::post('/payment', 'TransportCustomerController@payment');
    Route::get('/{id}/get-pdf', 'TransportCustomerController@getPdf')->name('transport.quote.pdf');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport'], 'prefix' => 'admin/transports', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'TransportController@index')->name('transport.index');
    Route::get('/excel', 'TransportController@exportExcel')->name('transport.export');
    Route::get('/{id}/detail', 'TransportController@detail')->name('transport.detail');
    Route::get('/create', 'TransportController@create')->name('transport.create');
    Route::post('/store', 'TransportController@store');
    Route::get('/{id}', 'TransportController@show')->name('transport.show');
    Route::put('/{id}/update', 'TransportController@update')->name('transport.update');
    Route::put('/{id}/updatecustomer', 'TransportController@updatecustomer')->name('transport.updatecustomer');
    Route::get('/{id}/edit', 'TransportController@edit')->name('transport.edit');
    Route::get('/{id}/print_bill', 'TransportController@printBill')->name('transport.print');
    Route::get('/{id}/print_bills', 'TransportController@printBills')->name('transport.prints');
    Route::get('/{id}/print_invoice', 'TransportController@print_invoice')->name('transport.print_invoice');
    // Route::get('/{id}/print_billold', 'TransportController@printBillold')->name('transport.printold');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-warehouse'], 'prefix' => 'admin/transport/warehouse', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'WarehouseController@index')->name('transport.warehouse.index');
    Route::get('/create', 'WarehouseController@create')->name('transport.warehouse.create');
    Route::get('/exportExcel', 'WarehouseController@exportExcel')->name('transport.warehouse.exportExcel');
    Route::get('/{id}/print_bill', 'WarehouseController@printBill')->name('transport.warehouse.print');
    Route::get('/{id}/view', 'WarehouseController@view')->name('transport.warehouse.view');
    Route::get('/{id}/exportExcelOrder', 'WarehouseController@exportExcelOrder')->name('transport.warehouse.exportExcelOrder');
    Route::post('/add-item', 'WarehouseController@addItem');
    Route::post('/add-package', 'WarehouseController@addPackage');
    Route::post('/add-item-exist', 'WarehouseController@addItemExist');
    Route::post('/remove-item-exist', 'WarehouseController@removeItemExist');
    Route::post('/edit-package', 'WarehouseController@editPackage');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-shipment'], 'prefix' => 'admin/transport/shipment', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'ShipmentController@index')->name('transport.shipment.index');
    Route::get('/create', 'ShipmentController@create')->name('transport.shipment.create');
    Route::get('/{id}/view', 'ShipmentController@view')->name('transport.shipment.view');
    Route::get('/{id}/exportExcelOrder', 'ShipmentController@exportExcelOrder')->name('transport.shipment.exportExcelOrder');
    Route::get('/exportExcel', 'ShipmentController@exportExcel')->name('transport.shipment.exportExcel');
    Route::get('/{id}/print_bill', 'ShipmentController@printBill')->name('transport.shipment.print');
    Route::post('/add-item', 'ShipmentController@addItem');
    Route::post('/add-package', 'ShipmentController@addPackage');
    Route::post('/add-item-exist', 'ShipmentController@addItemExist');
    Route::post('/remove-item-exist', 'ShipmentController@removeItemExist');
    Route::post('/edit-package', 'ShipmentController@editPackage');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-stockout'], 'prefix' => 'admin/transport/stockout', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'StockoutController@index')->name('transport.stockout.index');
    Route::get('/{id}/view', 'StockoutController@view')->name('transport.stockout.view');
    Route::post('/push-items', 'StockoutController@pushItems');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-receive'], 'prefix' => 'admin/transport/receive', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'ReceiveController@index')->name('transport.receive.index');
    Route::get('/{id}/view', 'ReceiveController@view')->name('transport.receive.view');
    Route::post('/push-items', 'ReceiveController@pushItems');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-carrier'], 'prefix' => 'admin/transport/carrier', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'CarrierController@index')->name('transport.carrier.index');
    Route::post('/{id}/push-items', 'CarrierController@pushItems');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-deliver'], 'prefix' => 'admin/transport/deliver', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/approved', 'DeliverController@approved')->name('transport.deliver.approved');
    Route::get('/cancel', 'DeliverController@cancel')->name('transport.deliver.cancel');
    Route::get('/carrier-pickup', 'DeliverController@carrierPickup')->name('transport.deliver.carrierpickup');
    Route::get('/done', 'DeliverController@done')->name('transport.deliver.done');
    Route::get('/{id}/getpdfghtk', 'DeliverController@getpdfghtk')->name('transport.deliver.getpdfghtk');
    Route::get('/listghtk', 'DeliverController@listghtk')->name('transport.deliver.listghtk');
    Route::get('/{id}/listghtk', 'DeliverController@editlistghtk')->name('transport.deliver.editlistghtk');
    Route::get('/{id}/printlistghtk', 'DeliverController@printlistghtk')->name('transport.deliver.printlistghtk');
    Route::post('/{id}/listghtk', 'DeliverController@addtolistghtk')->name('transport.deliver.editlistghtk');
    // Service
    Route::post('/cancel-request', 'DeliverController@cancelRequest');
    Route::post('/approve-request', 'DeliverController@approveRequest');
    Route::post('/pickup', 'DeliverController@pickup');
    Route::post('/customer-receiver', 'DeliverController@customerReceiver');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-complete'], 'prefix' => 'admin/transport/complete', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/approved', 'CompleteController@approved')->name('transport.complete.approved');
    Route::get('/cancel', 'CompleteController@cancel')->name('transport.complete.cancel');
    Route::get('/carrier-pickup', 'CompleteController@carrierPickup')->name('transport.complete.carrierpickup');
    Route::get('/done', 'CompleteController@done')->name('transport.complete.done');
    // Service
    Route::post('/cancel-request', 'CompleteController@cancelRequest');
    Route::post('/approve-request', 'CompleteController@approveRequest');
    Route::post('/pickup', 'CompleteController@pickup');
    Route::post('/customer-receiver', 'CompleteController@customerReceiver');
    Route::get('/completevnca', 'CompleteController@completevnca')->name('transport.complete.completevnca');
    Route::post('/completevnca', 'CompleteController@postcompletevnca')->name('transport.complete.completevnca');
});

Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport'], 'prefix' => 'admin/transport/tracking', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'TrackingController@index')->name('transport.tracking.index');
    Route::get('/{id}/updatephone', 'TrackingController@showphone')->name('transport.tracking.showphone');
    Route::post('/updatephone', 'TrackingController@updatephone')->name('transport.tracking.updatephone');
    Route::post('/findtracking', 'TrackingController@findtracking')->name('transport.tracking.findtracking');
    Route::post('/createat', 'TrackingController@updatecreateat')->name('transport.tracking.createat');
    Route::post('/updatestatus', 'TrackingController@updatestatus')->name('transport.tracking.updatestatus');
    Route::post('/view-history', 'TrackingController@viewHistory');
    Route::get('/{id}/delete', 'TrackingController@delete')->name('transport.tracking.delete');
    Route::post('/cancel-order', 'TrackingController@cancelOrder');
    Route::post('/restoreorder', 'TrackingController@restoreOrder');
    Route::get('/export', 'TrackingController@exportExcel')->name('transport.tracking.export');
    Route::post('/view-log', 'TrackingController@viewLog');
});
Route::group(['middleware' => ['web', 'auth:admin', 'can:manage-transport-stockout'], 'prefix' => 'admin/transport/pickup', 'namespace' => 'Modules\Transport\Http\Controllers'], function() {
    Route::get('/', 'PickupController@index')->name('transport.pickup.index');
    Route::get('/{id}/view', 'PickupController@view')->name('transport.pickup.view');
    Route::post('/push-items', 'PickupController@pushItems');
});
